package com.tbs.brothersgas.haadhir.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tbs.brothersgas.haadhir.Activitys.Cancel_RescheduleActivity;
import com.tbs.brothersgas.haadhir.Activitys.ScheduledCaptureDeliveryActivity;
import com.tbs.brothersgas.haadhir.Model.ReasonDO;
import com.tbs.brothersgas.haadhir.R;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;

import java.util.ArrayList;

public class ReasonListAdapter extends RecyclerView.Adapter<ReasonListAdapter.MyViewHolder> {

    private ArrayList<ReasonDO> reasonDoS;
    private Context context;
    private int type;
    private static int sSelected = -1;

    private static SingleClickListener sClickListener;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tvName;
        private ImageView ivDoneTick;

        public MyViewHolder(View view) {
            super(view);
            tvName = (TextView) view.findViewById(R.id.tvName);
            ivDoneTick = (ImageView) view.findViewById(R.id.ivDoneTick);


        }

        @Override
        public void onClick(View view) {
            sSelected = getAdapterPosition();
            sClickListener.onItemClickListener(getAdapterPosition(), view);
        }
    }

    public void selectedItem() {
        notifyDataSetChanged();
    }

    void setOnItemClickListener(SingleClickListener clickListener) {
        sClickListener = clickListener;
    }

    public ReasonListAdapter(int Type, Context context, ArrayList<ReasonDO> reasonDOS) {
        this.context = context;
        this.reasonDoS = reasonDOS;
        this.type = Type;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.simple_text, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final ReasonDO reasonDO = reasonDoS.get(position);

        holder.tvName.setText("" + reasonDO.reason);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = holder.getAdapterPosition();
                PreferenceUtils preferenceUtils = new PreferenceUtils(context);
                if (type == 3) {
                    if (context instanceof ScheduledCaptureDeliveryActivity) {
                        preferenceUtils.saveString(PreferenceUtils.FOC, String.valueOf(reasonDoS.get(pos).reason));
                        ((ScheduledCaptureDeliveryActivity) context).updateConfirmRequestButtons("FOC", new ArrayList());
                        ((ScheduledCaptureDeliveryActivity) context).dialog.dismiss();
                    }
                } else {
                    preferenceUtils.saveString(PreferenceUtils.Reason_CODE, String.valueOf(reasonDoS.get(pos).code));
                    preferenceUtils.saveString(PreferenceUtils.Reason_Payment, String.valueOf(reasonDoS.get(pos).reason));
                    ((Cancel_RescheduleActivity) context).updateReqApprButton("" + reasonDoS.get(pos).reason);

                }


            }
        });


    }

    @Override
    public int getItemCount() {
        return reasonDoS.size();
    }

    interface SingleClickListener {
        void onItemClickListener(int position, View view);
    }


}
