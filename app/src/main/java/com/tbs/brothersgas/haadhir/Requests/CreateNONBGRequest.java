package com.tbs.brothersgas.haadhir.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.brothersgas.haadhir.Activitys.BaseActivity;
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryDO;
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryMainDO;
import com.tbs.brothersgas.haadhir.Model.CustomerDo;
import com.tbs.brothersgas.haadhir.Model.LoanReturnDO;
import com.tbs.brothersgas.haadhir.Model.LoanReturnMainDO;
import com.tbs.brothersgas.haadhir.R;
import com.tbs.brothersgas.haadhir.database.StorageManager;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;
import com.tbs.brothersgas.haadhir.utils.ProgressTask;
import com.tbs.brothersgas.haadhir.utils.WebServiceConstants;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class CreateNONBGRequest extends AsyncTask<String, Void, Boolean> {

    private LoanReturnMainDO loanReturnMainDO;
    private LoanReturnDO loanReturnDO;
    private Context mContext;
    private String site, customer, vehicleCode;
    private int cylinderType, nonBgType;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;
    ArrayList<ActiveDeliveryDO> customerDos;

    public CreateNONBGRequest(String sitE, String customeR, String vehiclCodE, int cylinderTypE, ArrayList<ActiveDeliveryDO> returndos, Context mContext) {

        this.site = sitE;
        this.customer = customeR;
        this.vehicleCode = vehiclCodE;
        this.cylinderType = cylinderTypE;

        this.customerDos = returndos;

        this.mContext = mContext;


    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        public void onCompleted(boolean isError, LoanReturnMainDO loanReturnMainDO);

    }

    public boolean runRequest() {
        // System.out.println("CUSTOMER ID " + customerId);
        String NAMESPACE = "http://www.adonix.com/WSS";
        String METHOD_NAME = "run";
        String SOAP_ACTION = "CAdxWebServiceXmlCC";
//        String URL = "http://183.82.9.23:8124/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC";
        preferenceUtils = new PreferenceUtils(mContext);
        username = preferenceUtils.getStringFromPreference(PreferenceUtils.A_USER_NAME, "");
        password = preferenceUtils.getStringFromPreference(PreferenceUtils.A_PASSWORD, "");
        ip = preferenceUtils.getStringFromPreference(PreferenceUtils.IP_ADDRESS, "");
        port = preferenceUtils.getStringFromPreference(PreferenceUtils.PORT, "");
        pool = preferenceUtils.getStringFromPreference(PreferenceUtils.ALIAS, "");

        String URL = "http://" + ip + ":" + port + "/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC";
        String site = preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID, "");
        String customer = preferenceUtils.getStringFromPreference(PreferenceUtils.CUSTOMER, "");
        String scheduledID = preferenceUtils.getStringFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID, "");
        String nonScheduledID = preferenceUtils.getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, "");
        String role = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "");
        String id = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "");
        String spotId = preferenceUtils.getStringFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER, "");

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        // Set all input params
        request.addProperty("publicName", WebServiceConstants.CREATE_NON_BG);
        ActiveDeliveryMainDO activeDeliverySavedDo = StorageManager.getInstance(mContext).getActiveDeliveryMainDo(mContext);
        CustomerDo customerDo = StorageManager.getInstance(mContext).getCurrentSpotSalesCustomer(mContext);


        JSONObject jsonObject = new JSONObject();
        try {
            String transactionID = preferenceUtils.getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, "");
            String stransactionID = preferenceUtils.getStringFromPreference(PreferenceUtils.DOC_NUMBER, "");

            String ShipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, mContext.getResources().getString(R.string.checkin_non_scheduled));
            jsonObject.put("I_YCREUSR", role);
            if (ShipmentType.equals(mContext.getResources().getString(R.string.checkin_non_scheduled))) {
                jsonObject.put("I_YSRCNUM", transactionID);

                jsonObject.put("I_YSDHNUM", spotId);
                jsonObject.put("I_YVEHCODE", vehicleCode);
            }else {
                jsonObject.put("I_YSRCNUM", stransactionID);

                jsonObject.put("I_YSDHNUM", activeDeliverySavedDo.shipmentNumber);
                jsonObject.put("I_YVEHCODE", vehicleCode);
            }

            jsonObject.put("I_YVEHCODE", vehicleCode);

            JSONArray jsonArray = new JSONArray();
            if (customerDos != null && customerDos.size() > 0) {
                for (int i = 0; i < customerDos.size(); i++) {
                    JSONObject jsonObject1 = new JSONObject();
                    jsonObject1.put("I_YSDDLIN", customerDos.get(i).linenumber);
                    jsonObject1.put("I_YITMREF", customerDos.get(i).product);
                    jsonObject1.put("I_YQTY", customerDos.get(i).orderedQuantity);
                    jsonArray.put(jsonObject1);
                }

            }
            jsonObject.put("GRP2", jsonArray);

        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        request.addProperty("inputXml", jsonObject.toString());

        SoapObject callcontext = new SoapObject("", "callContext");
        // Set all input params
        callcontext.addProperty("codeLang", "ENG");
        callcontext.addProperty("poolAlias", pool);
        callcontext.addProperty("poolId", "");
        callcontext.addProperty("codeUser", username);
        callcontext.addProperty("password", password);
        callcontext.addProperty("requestConfig", "adxwss.trace.on=off");

        request.addSoapObject(callcontext);


        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.setOutputSoapObject(request);

        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
        androidHttpTransport.debug = true;

        try {
            List<HeaderProperty> headerList = new ArrayList<HeaderProperty>();
            headerList.add(new HeaderProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode((username + ":" + password).getBytes())));


            androidHttpTransport.call(SOAP_ACTION, envelope, headerList);


            SoapObject response = (SoapObject) envelope.getResponse();
            String resultXML = (String) response.getProperty("resultXml");
            if (resultXML != null && resultXML.length() > 0) {
                return parseXML(resultXML);
            } else {
                return false;
            }
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            ProgressTask.getInstance().closeProgress();

            return false;
        } catch (Exception e) {
            ProgressTask.getInstance().closeProgress();

            e.printStackTrace();
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            loanReturnMainDO = new LoanReturnMainDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        loanReturnMainDO.loanReturnDOS = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        loanReturnDO = new LoanReturnDO();

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {

                        if (attribute.equalsIgnoreCase("O_XFLG")) {
                            loanReturnMainDO.flag = Integer.parseInt(text);
                        }
                        else  if (attribute.equalsIgnoreCase("O_YSTATUS")) {
                            if(text.length()>0){
                                loanReturnMainDO.status = Integer.parseInt(text);

                            }
                        }
                        else  if (attribute.equalsIgnoreCase("O_YMESSAGE")) {
                            if(text.length()>0){
                                loanReturnMainDO.message = text;

                            }
                        }
                        text = "";

                    }

                    if (endTag.equalsIgnoreCase("GRP")) {

                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        loanReturnMainDO.loanReturnDOS.add(loanReturnDO);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        ((BaseActivity) mContext).showLoader();

//        ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        ((BaseActivity) mContext).hideLoader();

//        ProgressTask.getInstance().closeProgress();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, loanReturnMainDO);
        }
    }
}