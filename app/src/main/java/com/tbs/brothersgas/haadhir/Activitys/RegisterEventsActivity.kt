package com.tbs.brothersgas.haadhir.Activitys

import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.ScrollView
import com.tbs.brothersgas.haadhir.R

//
class RegisterEventsActivity : BaseActivity() {

    override  protected fun onResume() {
        super.onResume()
    }
    override fun initialize() {
      var  llCategories = getLayoutInflater().inflate(R.layout.fragment_register_events, null) as ScrollView
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()
        hideKeyBoard(llCategories)

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }

    }
    override fun initializeControls() {
        tvScreenTitle.setText(R.string.events)
        val btnCompleted = findViewById(R.id.btnCompleted) as Button

        btnCompleted.setOnClickListener {
            finish()
        }
    }

}