package com.tbs.brothersgas.haadhir.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.tbs.brothersgas.haadhir.common.FireBaseOperations;
import com.tbs.brothersgas.haadhir.utils.AppPrefs;

import java.util.concurrent.TimeUnit;

public class DateChangedReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e("DateChangedReceiver", "Date changed");
        FireBaseOperations.checkingDate(context);
    }


}