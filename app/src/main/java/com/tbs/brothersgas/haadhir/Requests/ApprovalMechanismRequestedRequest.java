package com.tbs.brothersgas.haadhir.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.tbs.brothersgas.haadhir.Activitys.BaseActivity;
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryDO;
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryMainDO;
import com.tbs.brothersgas.haadhir.Model.SuccessDO;
import com.tbs.brothersgas.haadhir.common.AppConstants;
import com.tbs.brothersgas.haadhir.utils.CustomXmlPullParser;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;
import com.tbs.brothersgas.haadhir.utils.WebServiceConstants;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class ApprovalMechanismRequestedRequest extends AsyncTask<String, Void, Boolean> {

    private SuccessDO successDO;
    private Context mContext;
    private String stockTransactionNumber, appUser, customer, site, documentNumber, rescheduleDate, comments;
    private Integer documentType;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;
    private ArrayList<ActiveDeliveryDO> activeDeliveryDOS;
    private int workFlag;

    public ApprovalMechanismRequestedRequest(String StockTransactionNumber, String AppUser, String Customer, String Site,
                                             String DocumentNumber, String RescheduleDate, String Comments, int DocumentType, ArrayList<ActiveDeliveryDO> activeDeliveryDoS,
                                             Context mContext) {

        this.mContext = mContext;
        this.activeDeliveryDOS = activeDeliveryDoS;
        this.stockTransactionNumber = StockTransactionNumber;
        this.appUser = AppUser;
        this.customer = Customer;
        this.site = Site;
        this.documentNumber = DocumentNumber;
        this.comments = Comments;
        this.rescheduleDate = RescheduleDate;
        this.documentType = DocumentType;


    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        public void onCompleted(boolean isError, SuccessDO successDO);

    }

    HttpURLConnection urlConnection;


    public boolean runRequest() {
        // System.out.println("CUSTOMER ID " + customerId);
        String NAMESPACE = "http://www.adonix.com/WSS";
        String METHOD_NAME = "run";
        String SOAP_ACTION = "CAdxWebServiceXmlCC";
        preferenceUtils = new PreferenceUtils(mContext);
        username = preferenceUtils.getStringFromPreference(PreferenceUtils.A_USER_NAME, "");
        password = preferenceUtils.getStringFromPreference(PreferenceUtils.A_PASSWORD, "");
        ip = preferenceUtils.getStringFromPreference(PreferenceUtils.IP_ADDRESS, "");
        port = preferenceUtils.getStringFromPreference(PreferenceUtils.PORT, "");
        pool = preferenceUtils.getStringFromPreference(PreferenceUtils.ALIAS, "");
        String URL = "http://" + ip + ":" + port + "/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC";

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        request.addProperty("publicName", WebServiceConstants.APPROVAL_MECHANISM_REQUEST);
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("I_YSTKVCR", stockTransactionNumber);
            jsonObject.put("I_YAPPUSR", appUser);
            jsonObject.put("I_YBPC", customer);
            jsonObject.put("I_YFCY", site);
            jsonObject.put("I_YDOCTYPE", documentType);
            jsonObject.put("I_YDOCNUM", documentNumber);
            jsonObject.put("I_YRESDATE", rescheduleDate);
            jsonObject.put("I_YREQCMNTS", comments);
            JSONArray jsonArray = new JSONArray();
            if (activeDeliveryDOS != null && activeDeliveryDOS.size() > 0) {
                for (int i = 0; i < activeDeliveryDOS.size(); i++) {
                    JSONObject jsonObject1 = new JSONObject();
                    jsonObject1.put("I_YITMREF", activeDeliveryDOS.get(i).product);
                    jsonObject1.put("I_YOPNQTY", activeDeliveryDOS.get(i).actualQuantity);
                    jsonObject1.put("I_YUPDQTY", activeDeliveryDOS.get(i).orderedQuantity);
                    jsonObject1.put("I_YDOCNUM1", activeDeliveryDOS.get(i).shipmentId);
                    jsonObject1.put("I_YDOCLINE", activeDeliveryDOS.get(i).line);
                    jsonObject1.put("I_YOPNPRI", activeDeliveryDOS.get(i).price);
                    jsonObject1.put("I_YUPDPRI", activeDeliveryDOS.get(i).priceTag);
                    jsonObject1.put("I_YREQCMNTS2", activeDeliveryDOS.get(i).reasonFOC);

                    jsonArray.put(i, jsonObject1);
                }
                jsonObject.put("GRP2", jsonArray);
            }


//            jsonObject.put("I_YREQCMNTS", shipmentID);
//            jsonObject.put("I_YSDHNUM", "SD-U302-19008357");

        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        request.addProperty("inputXml", jsonObject.toString());

        SoapObject callcontext = new SoapObject("", "callContext");
        // Set all input params
        callcontext.addProperty("codeLang", "ENG");
        callcontext.addProperty("poolAlias", pool);
        callcontext.addProperty("poolId", "");
        callcontext.addProperty("codeUser", username);
        callcontext.addProperty("password", password);
        callcontext.addProperty("requestConfig", "adxwss.trace.on=off");

        request.addSoapObject(callcontext);


        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.setOutputSoapObject(request);

        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
        androidHttpTransport.debug = true;

        try {
            List<HeaderProperty> headerList = new ArrayList<HeaderProperty>();
            headerList.add(new HeaderProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode((username + ":" + password).getBytes())));


            androidHttpTransport.call(SOAP_ACTION, envelope, headerList);


            SoapObject response = (SoapObject) envelope.getResponse();
            String resultXML = (String) response.getProperty("resultXml");
            if (resultXML != null && resultXML.length() > 0) {
                return parseXML(resultXML);
            } else {
                return false;
            }
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            successDO = new SuccessDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("LIN")) {

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {


                        if (attribute.equalsIgnoreCase("O_YFLG")) {
                            if (text.length() > 0) {

                                successDO.flag = Integer.parseInt(text);
                            }


                        } else if (attribute.equalsIgnoreCase("O_YMESSAGE")) {
                            if (text.length() > 0) {

                                successDO.message = text;
                            }


                        } else if (attribute.equalsIgnoreCase("O_YREQNO")) {
                            if (text.length() > 0) {

                                successDO.requestedNumber = text;
                            }


                        }


                        text = "";


                    }


                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {

        super.onPreExecute();
        ((BaseActivity) mContext).showLoader();

        // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        ((BaseActivity) mContext).hideLoader();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, successDO);
        }
    }
}