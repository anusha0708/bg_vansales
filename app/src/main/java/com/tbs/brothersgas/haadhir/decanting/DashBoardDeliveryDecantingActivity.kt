package com.tbs.brothersgas.haadhir.decanting

import android.annotation.TargetApi
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.brothersgas.haadhir.Activitys.BaseActivity
import com.tbs.brothersgas.haadhir.Model.CustomerDo
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.delivery_executive.DeliveryExecutiveActivityReportActivity
import com.tbs.brothersgas.haadhir.delivery_executive.DeliveryExecutiveListActivity
import com.tbs.brothersgas.haadhir.utils.Constants
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import kotlinx.android.synthetic.main.activity_dashboard_dcentive.*

class DashBoardDeliveryDecantingActivity : BaseActivity() {
    lateinit var userId: String
    lateinit var orderCode: String
    lateinit var llOrderHistory: View
    lateinit var customerDos: ArrayList<CustomerDo>
    private lateinit var btnAddMore: Button
    lateinit var llSearch: LinearLayout
    lateinit var etSearch: EditText
    var REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 200
    override fun initialize() {
        llOrderHistory = layoutInflater.inflate(R.layout.activity_dashboard_dcentive, null)
        llBody.addView(llOrderHistory, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            finishAffinity()
        }
        disableMenuWithBackButton()
        initializeControls()

        tvScreenTitle.setText("Decanting User")
        ivMenu.visibility = View.VISIBLE

    }

    override fun initializeControls() {
        insertDummyContactWrapper()
        ivMenu.setOnClickListener {
            //
            val popup = PopupMenu(this@DashBoardDeliveryDecantingActivity, ivMenu)
            popup.getMenuInflater().inflate(R.menu.popup_menu_delivery_executive, popup.getMenu())
            popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                override fun onMenuItemClick(item: MenuItem): Boolean {
                    if (item.title.equals("Logout")) {
                        logOut()
                    }

                    return true

                }


            })
            popup.show()//showing popup menu

        }


        val intent = Intent(this, DeliveryDecantingListActivity::class.java)
        llDecanting.setOnClickListener {
            if(preferenceUtils.getStringFromPreference(PreferenceUtils.CHECKIN_DATE,"").isNotEmpty()){
                intent.putExtra(Constants.SCREEN_TYPE, 2)
                startActivity(intent)
            }else{
                showAlert("Kindly do the check-in first to proceed")
            }

        }
        ll_Allocated.setOnClickListener {
            if(preferenceUtils.getStringFromPreference(PreferenceUtils.CHECKIN_DATE,"").isNotEmpty()){
                intent.putExtra(Constants.SCREEN_TYPE, 1)
                startActivity(intent)
            }else{
                showAlert("Kindly do the check-in first to proceed")
            }

        }
        ll_Spot.setOnClickListener {
            if(preferenceUtils.getStringFromPreference(PreferenceUtils.CHECKIN_DATE,"").isNotEmpty()){
                val intent = Intent(this, DecantingSpotSalesCustomerActivity::class.java)
                startActivity(intent)
            }else{
                showAlert("Kindly do the check-in first to proceed")
            }

        }
        ll_Checkin.setOnClickListener {
            val intent = Intent(this, DecantingCheckinActivity::class.java)
            startActivity(intent)
        }
        llCheckout.setOnClickListener {
            if(preferenceUtils.getStringFromPreference(PreferenceUtils.CHECKIN_DATE,"").isNotEmpty()){
                showAppCompatAlert("Alert!", resources.getString(R.string.checkoutconfirm),
                        "OK", "Cancel", "Checkin", true)
            }else{
                showAlert("Kindly do the check-in first to proceed")
            }

        }
    }
    private fun doCheckOut() {

        if (Util.isNetworkAvailable(this)) {
            val request = DecantingCheckinRequest(this,2)
            request.setOnResultListener { isError, modelDO ->
                hideLoader()
                if (isError) {
                    showAlert(resources.getString(R.string.server_error))

                    //invoiceList()
                } else {
                    if (modelDO != null && modelDO.flag == 20) {
                        showToast("Updated Successfully...")
                        preferenceUtils.removeFromPreference(PreferenceUtils.VEHICLE_CODE)
                        preferenceUtils.removeFromPreference(PreferenceUtils.CHECKIN_DATE)

//                        finish()

                    } else {
                        showAlert(resources.getString(R.string.server_error))

                    }

                }
            }
            request.execute()

        } else {
            showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private fun insertDummyContactWrapper() {
        val permissionsNeeded = ArrayList<String>()
        val permissionsList = ArrayList<String>()
        if (!addPermission(permissionsList, android.Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Storage")
        if (permissionsList.size > 0) {
            requestPermissions(permissionsList.toArray(arrayOfNulls<String>(permissionsList.size)),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS)
            return
        }
    }

    private fun addPermission(permissionsList: MutableList<String>, permission: String): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission)
                // Check for Rationale Option
                if (!shouldShowRequestPermissionRationale(permission))
                    return false
            }
        }
        return true
    }

    private fun setIvMenu() {
        val popup = PopupMenu(this, ivMenu)
        popup.getMenuInflater().inflate(R.menu.popup_menu_delivery_executive, popup.getMenu())

        popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
            override fun onMenuItemClick(item: MenuItem): Boolean {
                if (item.title.equals("Logout")) {
                    logOut()
                }
                return true

            }
        })
        popup.show()//showing popup menu

    }
    override fun onButtonYesClick(from: String) {
        super.onButtonYesClick(from)
        if(from.equals("Checkin")){
            doCheckOut()
        }

    }
}

