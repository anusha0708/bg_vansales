package com.tbs.brothersgas.haadhir.history

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import com.google.android.material.tabs.TabLayout
import androidx.viewpager.widget.ViewPager
import androidx.appcompat.widget.SearchView
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.Toast

import com.tbs.brothersgas.haadhir.Activitys.BaseActivity
import com.tbs.brothersgas.haadhir.Adapters.ScheduledProductsAdapter
import com.tbs.brothersgas.haadhir.Model.*
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.ActiveDeliveryRequest
import com.tbs.brothersgas.haadhir.utils.Util

import java.util.ArrayList

class TabListActivity : BaseActivity() {
    private var rlPartyActivity: RelativeLayout? = null
    lateinit var adapter: PagerAdapter
    lateinit var viewPager: ViewPager
    lateinit var tabLayout: TabLayout
    lateinit var customer: String
     var activeDeliveryMainDo: ActiveDeliveryMainDO = ActiveDeliveryMainDO()
     var unPaidInvoiceMainDO: UnPaidInvoiceMainDO  = UnPaidInvoiceMainDO()

    override fun initialize() {
        rlPartyActivity = layoutInflater.inflate(R.layout.activity_tablist, null) as RelativeLayout
        llBody.addView(rlPartyActivity, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {

            finish()
        }
        disableMenuWithBackButton()
        initializeControls()

        if (intent.hasExtra("CODE")) {
            customer = intent.extras!!.getString("CODE")
        } else {
            customer = ""
        }
//        prepareDeliveryData()
//        prepareInvoiceData()

        tabLayout = findViewById<View>(R.id.tab_layout) as TabLayout
        tabLayout.addTab(tabLayout.newTab().setText("Deliveries"))
        tabLayout.addTab(tabLayout.newTab().setText("Invoices"))
        tabLayout.tabGravity = TabLayout.GRAVITY_FILL

        viewPager = findViewById<View>(R.id.pager) as ViewPager

        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        tabLayout.setOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })

        deliveryList()


    }

    fun deliveryList() {
        if (Util.isNetworkAvailable(this)) {
            val driverListRequest = DeliveryListRequest(customer, this@TabListActivity)
            driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                hideLoader()
                if (isError) {

                    invoiceList()
//                    Toast.makeText(this@TabListActivity, R.string.server_error, Toast.LENGTH_SHORT).show()
                } else {
                    if (activeDeliveryDo != null) {

                        activeDeliveryMainDo = activeDeliveryDo
                    }

                    invoiceList()
                }
            }
            driverListRequest.execute()

        } else {
            showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

        }
    }

    fun invoiceList() {
        if (Util.isNetworkAvailable(this)) {
            val driverListRequest = InvoicesListRequest(customer, this@TabListActivity)
            driverListRequest.setOnResultListener { isError, invoiceMainDO ->
                hideLoader()
                if (isError) {

//                    Toast.makeText(this@TabListActivity, R.string.server_error, Toast.LENGTH_SHORT).show()
                } else {
                    if (invoiceMainDO != null) {

                        unPaidInvoiceMainDO = invoiceMainDO
                    }

                }
//                if (activeDeliveryMainDo != null && activeDeliveryMainDo.activeDeliveryDOS != null && activeDeliveryMainDo.activeDeliveryDOS.size > 0
//                        && unPaidInvoiceMainDO != null && unPaidInvoiceMainDO.unPaidInvoiceDOS != null && unPaidInvoiceMainDO.unPaidInvoiceDOS.size > 0) {
//
//                    adapter = PagerAdapter(supportFragmentManager, activeDeliveryMainDo, unPaidInvoiceMainDO)
//                    viewPager.adapter = adapter
//                }
//               else if (activeDeliveryMainDo != null && activeDeliveryMainDo.activeDeliveryDOS != null && activeDeliveryMainDo.activeDeliveryDOS.size > 0) {
//
//                    adapter = PagerAdapter(supportFragmentManager, activeDeliveryMainDo, unPaidInvoiceMainDO)
//                    viewPager.adapter = adapter
//                }
//               else if ( unPaidInvoiceMainDO != null && unPaidInvoiceMainDO.unPaidInvoiceDOS != null && unPaidInvoiceMainDO.unPaidInvoiceDOS.size > 0) {
//
//                    adapter = PagerAdapter(supportFragmentManager, activeDeliveryMainDo, unPaidInvoiceMainDO)
//                    viewPager.adapter = adapter
//                }

                adapter = PagerAdapter(supportFragmentManager, activeDeliveryMainDo, unPaidInvoiceMainDO)
                viewPager.adapter = adapter
            }
            driverListRequest.execute()

        } else {
            showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

        }
    }

    override fun initializeControls() {
        tvScreenTitle.text = "Transactions List"

//        ivSearch.visibility = View.VISIBLE

        ivSearch.setOnClickListener {
            //            svSearch.visibility = View.VISIBLE
//            tvScreenTitle.visibility = View.INVISIBLE
//            val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
//
//            svSearch.setSearchableInfo(searchManager
//                    .getSearchableInfo(componentName))
//            svSearch.maxWidth = Integer.MAX_VALUE
//            svSearch.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
//                override fun onQueryTextSubmit(query: String): Boolean {
//                    adapter.filter.filter(query)
//
//                    return false
//                }
//
//                override fun onQueryTextChange(query: String): Boolean {
//                    // filter recycler view when text is changed
//                    adapter.filter.filter(query)
//                    return false
//                }
//            })
        }

    }
    private fun prepareDeliveryData() {
        var orderRequestsDO = ActiveDeliveryDO()
        activeDeliveryMainDo.activeDeliveryDOS.add(orderRequestsDO)
         orderRequestsDO = ActiveDeliveryDO()
        activeDeliveryMainDo.activeDeliveryDOS.add(orderRequestsDO)
         orderRequestsDO = ActiveDeliveryDO()
        activeDeliveryMainDo.activeDeliveryDOS.add(orderRequestsDO)
         orderRequestsDO = ActiveDeliveryDO()
        activeDeliveryMainDo.activeDeliveryDOS.add(orderRequestsDO)
    }
    private fun prepareInvoiceData() {
        var orderRequestsDO = UnPaidInvoiceDO()
        unPaidInvoiceMainDO.unPaidInvoiceDOS.add(orderRequestsDO)
        orderRequestsDO = UnPaidInvoiceDO()
        unPaidInvoiceMainDO.unPaidInvoiceDOS.add(orderRequestsDO)
        orderRequestsDO = UnPaidInvoiceDO()
        unPaidInvoiceMainDO.unPaidInvoiceDOS.add(orderRequestsDO)
        orderRequestsDO = UnPaidInvoiceDO()
        unPaidInvoiceMainDO.unPaidInvoiceDOS.add(orderRequestsDO)
    }

}