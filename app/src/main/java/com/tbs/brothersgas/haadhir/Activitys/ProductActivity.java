package com.tbs.brothersgas.haadhir.Activitys;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tbs.brothersgas.haadhir.Adapters.ProductAdapter;
import com.tbs.brothersgas.haadhir.Model.ProductDO;
import com.tbs.brothersgas.haadhir.R;
import com.tbs.brothersgas.haadhir.Requests.ProductListRequest;

import java.util.ArrayList;
import java.util.List;

public class ProductActivity extends BaseActivity {
    private String userId,orderCode;
    private RecyclerView recycleview;
    private ProductAdapter productAdapter;
    private LinearLayout llOrderHistory;
    private TextView tvScreenTitles, tvNoOrders;
    private List<ProductDO> productDOS = new ArrayList<>();
    private LinearLayout llSearch;
    private EditText etSearch;
    private ImageView ivClearSearch, ivSearch, ivGoBack;

    @Override
    public void initialize() {
        llOrderHistory = (LinearLayout) getLayoutInflater().inflate(R.layout.site_screen, null);
        llBody.addView(llOrderHistory, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        disableMenuWithBackButton();
        flToolbar.setVisibility(View.GONE);
        initializeControls();
        tvScreenTitles.setVisibility(View.VISIBLE);
        tvScreenTitles.setText("Product List");
        recycleview.setLayoutManager(new LinearLayoutManager(this));

        ivGoBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ivSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if(llSearch.visibility == View.VISIBLE){
//                llSearch.visibility = View.INVISIBLE
//            }
//            else{
//            }
                tvScreenTitles.setVisibility(View.GONE);
                llSearch.setVisibility(View.VISIBLE);
            }
        });

        ivClearSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etSearch.setText("");
                if(productDOS!=null &&productDOS.size()>0){
                    productAdapter = new ProductAdapter(ProductActivity.this, productDOS);
                    recycleview.setAdapter(productAdapter);
                    tvNoOrders.setVisibility(View.GONE);
                    recycleview.setVisibility(View.VISIBLE);
                }
                else{
                    tvNoOrders.setVisibility(View.VISIBLE);
                    recycleview.setVisibility(View.GONE);
                }

            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(etSearch.getText().toString().trim().equalsIgnoreCase("")){
                    if(productDOS!=null &&productDOS.size()>0){
                        productAdapter = new ProductAdapter(ProductActivity.this,productDOS);
                        recycleview.setAdapter(productAdapter);
                        tvNoOrders.setVisibility(View.GONE);
                        recycleview.setVisibility(View.VISIBLE);
                    }
                    else{
                        tvNoOrders.setVisibility(View.VISIBLE);
                        recycleview.setVisibility(View.GONE);
                    }
                }
                else if(etSearch.getText().toString().trim().length()>2){
                    filter(etSearch.getText().toString().trim());
                }
            }
        });

        ProductListRequest siteListRequest = new ProductListRequest(ProductActivity.this);
        showLoader();
        siteListRequest.setOnResultListener(new ProductListRequest.OnResultListener() {
            @Override
            public void onCompleted(boolean isError, List<ProductDO> productDoS) {
                hideLoader();
                productDOS =productDoS;
                if (isError) {
                    Toast.makeText(ProductActivity.this, R.string.error_product_list, Toast.LENGTH_SHORT).show();
                } else {
                    if(productDOS.size()>0){
                        ProductAdapter driverAdapter = new ProductAdapter(ProductActivity.this,productDOS);
                        recycleview.setAdapter(driverAdapter);
                    }
                    else {
                        showAlert(""+R.string.error_NoData);
                    }


                }
            }
        });

        siteListRequest.execute();
        // new CommonBL(OrderHistoryActivity.this, OrderHistoryActivity.this).orderHistory(userId);

    }

    @Override
    public void initializeControls() {
        recycleview             = (RecyclerView) findViewById(R.id.recycleview);
        tvNoOrders              = (TextView) findViewById(R.id.tvNoOrders);
        tvScreenTitles           = (TextView) findViewById(R.id.tvScreenTitles);
        llSearch                = findViewById(R.id.llSearch);
        etSearch                = findViewById(R.id.etSearch);
        ivClearSearch           = findViewById(R.id.ivClearSearch);
        ivGoBack                = findViewById(R.id.ivGoBack);
        ivSearch                = findViewById(R.id.ivSearchs);

    }

    private ArrayList<ProductDO> filter(String filtered) {
        ArrayList<ProductDO> productDOs = new ArrayList();
        for (int i=0; i<productDOS.size(); i++){
            if(productDOS.get(i).productName.contains(filtered) || productDOS.get(i).productId.contains(filtered)){
                productDOs.add(productDOS.get(i));
            }
        }
        if(productDOs.size()>0){
            productAdapter = new ProductAdapter(ProductActivity.this, productDOs);
            recycleview.setAdapter(productAdapter);
//            productAdapter.refreshAdapter(productDOs);
            tvNoOrders.setVisibility(View.GONE);
            recycleview.setVisibility(View.VISIBLE);
        }
        else{
            tvNoOrders.setVisibility(View.VISIBLE);
            recycleview.setVisibility(View.GONE);
        }
        return productDOs;
    }

}
