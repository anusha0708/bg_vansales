package com.tbs.brothersgas.haadhir.pdfs.utils

import android.Manifest
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.ScrollView

import com.github.barteksc.pdfviewer.PDFView
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle
import com.shockwave.pdfium.PdfDocument
import com.tbs.brothersgas.haadhir.Activitys.BaseActivity
import com.tbs.brothersgas.haadhir.Activitys.InvoiceSignatureActivity
import com.tbs.brothersgas.haadhir.Model.CreateInvoiceDO
import com.tbs.brothersgas.haadhir.Model.CreateInvoicePaymentDO
import com.tbs.brothersgas.haadhir.Model.PodDo
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.CreateInvoiceRequest
import com.tbs.brothersgas.haadhir.Requests.CustomerManagementRequest
import com.tbs.brothersgas.haadhir.Requests.InvoiceDetailsRequest
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.listeners.ResultListner
import com.tbs.brothersgas.haadhir.pdfs.BGInvoicePdf
import com.tbs.brothersgas.haadhir.prints.PrinterConnection
import com.tbs.brothersgas.haadhir.prints.PrinterConstants
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util

import java.io.File
import java.util.ArrayList

class PreinvoicePreviewActivity : BaseActivity(), OnPageChangeListener, OnLoadCompleteListener {
    private val permissionRequestCode = 34
    lateinit var createPDFInvoiceDo: CreateInvoicePaymentDO
    lateinit var createInvoiceDO: CreateInvoiceDO
    lateinit var pdfView: PDFView
    lateinit var ll1: LinearLayout
    lateinit var btnInvoice: Button
    lateinit var btnReturn: Button
    internal var pageNumber: Int? = 0
    internal var pdfFileName: String? = null
    internal var TAG = "PdfActivity"
    internal var position = -1
    lateinit var dir: File
    lateinit var podDo: PodDo
    lateinit var data: String
    internal var name: String? = null
    var fromId = 0

    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.pre_invoice_pdf, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        tvScreenTitle.setText(R.string.invoice)
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
        initializeControls()

    }
    override fun initializeControls() {
        init()
        podDo = StorageManager.getInstance(this).getDepartureData(this);

        if (intent.hasExtra("Sales")) {
            fromId = intent.extras.getInt("Sales")
        }
        if (intent.hasExtra("DATA")) {
            data = intent.extras.getString("DATA")
        }

        if (!podDo.invoice.equals("")) {

            btnInvoice.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnInvoice.setClickable(false)
            btnInvoice.setEnabled(false)
            var selectedId = data

            var deliveryId = preferenceUtils.getStringFromPreference(PreferenceUtils.INVOICE_SHIPMENT_ID, "")
            if (deliveryId.length > 0 && selectedId.equals(deliveryId)) {
                btnInvoice.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnInvoice.setClickable(false)
                btnInvoice.setEnabled(false)

            } else {
                btnInvoice.setBackgroundColor(resources.getColor(R.color.md_green))
                btnInvoice.setClickable(true)
                btnInvoice.setEnabled(true)
            }
        } else {
            btnInvoice.setBackgroundColor(resources.getColor(R.color.md_green))
            btnInvoice.setClickable(true)
            btnInvoice.setEnabled(true)
        }
    }
    private fun init() {
        pdfView = findViewById<View>(R.id.pdfView) as PDFView
        ll1 = findViewById<View>(R.id.ll1) as LinearLayout
        btnInvoice = findViewById<View>(R.id.btnInvoice) as Button
        btnReturn = findViewById<View>(R.id.btnReturn) as Button
        btnReturn.setOnClickListener { finish() }
        btnInvoice.setOnClickListener {
            var shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
            var spotID = preferenceUtils.getStringFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER, "")

            if (shipmentId.length > 0) {
                val customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)
                var activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)

                val ShipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, getResources().getString(R.string.checkin_non_scheduled))
                if (ShipmentType.equals(getResources().getString(R.string.checkin_non_scheduled), true)) {

                    customerManagement(customerDo.customerId, 2)
                } else {
                    customerManagement(activeDeliverySavedDo.customer, 2)

                }
            } else if (spotID.length > 0) {
                val customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)
                customerManagement(customerDo.customerId, 2)


            } else {
                showToast("Delivery not Found..")

            }
        }

        //        position = getIntent().getIntExtra("position",-1);
        displayFromSdcard()
    }

    private fun displayFromSdcard() {
        if (intent.hasExtra("KEY")) {
            pdfFileName = intent.extras!!.getString("KEY")
        }
        //        pdfFileName = PDFConstants.CYL_DELIVERY_NOTE_PDF_NAME;
        dir = File(Util.getAppPath(this@PreinvoicePreviewActivity))

        getfile(dir)

        //        File file= new File(String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath().endsWith(pdfFileName)));
        for (j in fileList.indices) {
            if (fileList[j].absoluteFile.name == pdfFileName) {
                pdfView.fromFile(fileList[j].absoluteFile)
                        .defaultPage(pageNumber!!)
                        .enableSwipe(true)

                        .swipeHorizontal(false)
                        .onPageChange(this)
                        .enableAnnotationRendering(true)
                        .onLoad(this)
                        .scrollHandle(DefaultScrollHandle(this))
                        .load()
            }
        }


    }


    fun getfile(dir: File): ArrayList<File> {

        val listFile = dir.listFiles()
        var i = 0
        if (listFile != null && listFile.size > 0) {
            i = 0
            while (i < listFile.size) {

                if (listFile[i].isDirectory) {
                    getfile(listFile[i])

                } else {

                    var booleanpdf = false
                    if (listFile[i].name.endsWith(pdfFileName!!)) {

                        for (j in fileList.indices) {
                            if (fileList[j].name == listFile[i].name) {
                                booleanpdf = true
                            } else {

                            }
                        }

                        if (booleanpdf) {
                            booleanpdf = false
                        } else {
                            fileList.add(listFile[i])

                        }
                    }
                }
                i++
            }
        }
        return fileList
    }

    override fun onPageChanged(page: Int, pageCount: Int) {
        pageNumber = page
        title = String.format("%s %s / %s", pdfFileName, page + 1, pageCount)
    }


    override fun loadComplete(nbPages: Int) {
        val meta = pdfView.documentMeta
        printBookmarksTree(pdfView.tableOfContents, "-")

    }

    fun printBookmarksTree(tree: List<PdfDocument.Bookmark>, sep: String) {
        for (b in tree) {

            Log.e(TAG, String.format("%s %s, p %d", sep, b.title, b.pageIdx))

            if (b.hasChildren()) {
                printBookmarksTree(b.children, "$sep-")
            }
        }
    }

    companion object {
        var fileList = ArrayList<File>()
    }
    private fun customerManagement(id: String, type: Int) {
        if (Util.isNetworkAvailable(this)) {
            val request = CustomerManagementRequest(id, type, this)
            request.setOnResultListener { isError, customerDo ->
                hideLoader()
                if (customerDo != null) {
                    if (isError) {
                        showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
                    } else {
                        if (customerDo.flag.equals("Customer authorized for this transaction", true)) {
//                            invoiceCreation()


                            val intent = Intent(this, InvoiceSignatureActivity::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                            intent.putExtra("KEY", PDFConstants.BG_INVOICE_PDF_NAME)
                            intent.putExtra("DATA", data)

                            startActivityForResult(intent,10)
                        } else {
                            showAppCompatAlert("Alert!", "Customer not authorized for this transaction", "Ok", "", "", false)

                        }
                    }
                } else {
                    showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
                }
            }
            request.execute()
        } else {
            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "FAILURE", false)

        }

    }

    private fun prepareInvoiceCreation() {
        var id = preferenceUtils.getStringFromPreference(PreferenceUtils.INVOICE_ID, "")
//        var id="CDC-U101-19000091"//
        if (id.length > 0) {
            if (Util.isNetworkAvailable(this)) {
                val siteListRequest = InvoiceDetailsRequest(id, this)
                siteListRequest.setOnResultListener { isError, createPDFInvoiceDO ->
                    hideLoader()
                    if (createPDFInvoiceDO != null) {
                        if (isError) {
                            showToast("Unable to send Email");
//                        showAppCompatAlert("Error", "Unable to send Email", "Ok", "", "", false)
                        } else {
                            createPDFInvoiceDo = createPDFInvoiceDO
//                        if(createPDFInvoiceDo.email==10){
//                            createPaymentPDF(createPDFInvoiceDO)
//                        }
//                        if(createPaymentDo.print==10){
//                            printDocument(createPDFpaymentDO, PrinterConstants.PrintPaymentReport)
//                        }
                            if (createInvoiceDO.email == 10) {
                                createInvoicePDF(createPDFInvoiceDO)
                            }
                            if (createInvoiceDO.print == 10) {
                                if (checkFilePermission()) {
                                    printDocument(createPDFInvoiceDO, PrinterConstants.PrintInvoiceReport);
                                }
                            }


                        }
                    } else {
                        showToast("Unable to send Email");

//                    showAppCompatAlert("Error", "Unable to send Email!!", "Ok", "", "", false)
                    }
                }
                siteListRequest.execute()
            } else {
//            showAppCompatAlert("Error", "Unable to send Email", "Ok", "", "Failure", false)
            }
        } else {
            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "",false)

        }


    }

    private fun checkFilePermission(): Boolean {
        if (ContextCompat.checkSelfPermission(this@PreinvoicePreviewActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this@PreinvoicePreviewActivity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), permissionRequestCode)
        } else {
            return true
        }
        return false
    }

    private fun createInvoicePDF(createPDFInvoiceDO: CreateInvoicePaymentDO) {
        captureInfo(createPDFInvoiceDO.invoiceEmail, ResultListner { `object`, isSuccess ->
            if (isSuccess) {
                BGInvoicePdf.getBuilder(this).build(createPDFInvoiceDO, "Email");
            }
        })
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == permissionRequestCode) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                createInvoicePDF(createPDFInvoiceDo)
            } else {
                Log.e(TAG, "WRITE_PERMISSION_DENIED")
            }
        }

    }

    private fun printDocument(obj: Any, from: Int) {
        val printConnection = PrinterConnection.getInstance(this@PreinvoicePreviewActivity)
        if (printConnection.isBluetoothEnabled()) {
            printConnection.connectToPrinter(obj, from)
        } else {
//            PrinterConstants.PRINTER_MAC_ADDRESS = ""
            showToast("Please enable your mobile Bluetooth.")
//            showAppCompatAlert("", "Please enable your mobile Bluetooth.", "Enable", "Cancel", "EnableBluetooth", false)
        }
    }

    private fun pairedDevices() {
        val pairedDevices = BluetoothAdapter.getDefaultAdapter().getBondedDevices()
        if (pairedDevices.size > 0) {
            for (device in pairedDevices) {
                val deviceName = device.getName()
                val mac = device.getAddress() // MAC address
//                if (StorageManager.getInstance(this).getPrinterMac(this).equals("")) {
                StorageManager.getInstance(this).savePrinterMac(this, mac);
//                }
                Log.e("Bluetooth", "Name : " + deviceName + " Mac : " + mac)
                break
            }
        }
    }

    override fun onResume() {
        super.onResume()
        pairedDevices()
    }

    override fun onStart() {
        super.onStart()
        val filter = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        registerReceiver(mReceiver, filter)
//        val filter = IntentFilter(Intent.PAIRIN.ACTION_FOUND Intent. "android.bluetooth.device.action.PAIRING_REQUEST");
//        registerReceiver(mReceiver, filter)
//        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
//        mBluetoothAdapter.startDiscovery()
//        val filter2 = IntentFilter( "android.bluetooth.device.action.PAIRING_REQUEST")
//        registerReceiver(mReceiver, filter2)

    }

    override fun onDestroy() {
        unregisterReceiver(mReceiver)
//        PrinterConstants.PRINTER_MAC_ADDRESS = ""
        super.onDestroy()
    }

    private val mReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            if (BluetoothDevice.ACTION_FOUND == action) {
                val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE);
                PrinterConstants.bluetoothDevices.add(device)
                Log.e("Bluetooth", "Discovered => name : " + device.name + ", Mac : " + device.address)
            } else if (action.equals(BluetoothDevice.ACTION_ACL_CONNECTED)) {
                Log.e("Bluetooth", "status : ACTION_ACL_CONNECTED")
                pairedDevices()
            } else if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                val state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);

                if (state == BluetoothAdapter.STATE_OFF) {
                    Log.e("Bluetooth", "status : STATE_OFF")
                } else if (state == BluetoothAdapter.STATE_TURNING_OFF) {
                    Log.e("Bluetooth", "status : STATE_TURNING_OFF")
                } else if (state == BluetoothAdapter.STATE_ON) {
                    Log.e("Bluetooth", "status : STATE_ON")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_TURNING_ON) {
                    Log.e("Bluetooth", "status : STATE_TURNING_ON")
                } else if (state == BluetoothAdapter.STATE_CONNECTING) {
                    Log.e("Bluetooth", "status : STATE_CONNECTING")
                } else if (state == BluetoothAdapter.STATE_CONNECTED) {
                    Log.e("Bluetooth", "status : STATE_CONNECTED")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_DISCONNECTED) {
                    Log.e("Bluetooth", "status : STATE_DISCONNECTED")
                }
            }
        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 10 && resultCode == 10) {
            btnInvoice.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnInvoice.setClickable(false)
            btnInvoice.setEnabled(false)
            val intent = Intent();
            intent.putExtra("INVOICE", data)
            setResult(10, intent)
            finish()
        }


    }

}
