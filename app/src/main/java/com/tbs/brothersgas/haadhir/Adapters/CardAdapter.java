package com.tbs.brothersgas.haadhir.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.tbs.brothersgas.haadhir.Activitys.CreatePaymentActivity;
import com.tbs.brothersgas.haadhir.Activitys.OnAccountCreatePaymentActivity;
import com.tbs.brothersgas.haadhir.Model.CardDO;
import com.tbs.brothersgas.haadhir.Model.FinantialSiteDO;
import com.tbs.brothersgas.haadhir.R;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;

import java.util.ArrayList;

public class CardAdapter extends RecyclerView.Adapter<CardAdapter.MyViewHolder>  {

    private ArrayList<CardDO> finantialSiteDOS;
    private Context context;
    private String from;





    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName,tvCardChrges;
        private LinearLayout llDetails;

        public MyViewHolder(View view) {
            super(view);
            tvName = (TextView) view.findViewById(R.id.tvName);

            tvCardChrges = (TextView) view.findViewById(R.id.tvCardChrgees);


        }
    }


    public CardAdapter(String From, Context context, ArrayList<CardDO> bankDoS, int typE) {
        this.context = context;
        this.finantialSiteDOS = bankDoS;
        this.from    = From;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_simple_text, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final CardDO finantialSiteDO = finantialSiteDOS.get(position);

        holder.tvName.setText(finantialSiteDO.cardCategory);
        holder.tvCardChrges.setText("Service Charges - "+""+finantialSiteDO.cardCharges);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = holder.getAdapterPosition();
                PreferenceUtils preferenceUtils = new PreferenceUtils(context);
                preferenceUtils.saveString(PreferenceUtils.CARD_TYPE, finantialSiteDOS.get(pos).cardType);
                preferenceUtils.saveDouble(PreferenceUtils.SERVICE_CHARGES, finantialSiteDOS.get(pos).cardCharges);

                if(from.isEmpty()){
                    ((OnAccountCreatePaymentActivity)context).tvCharge.setText("Service Charge("+finantialSiteDOS.get(pos).cardCharges+"%)");

                    ((OnAccountCreatePaymentActivity)context).tvCardSelection.setText(""+finantialSiteDOS.get(pos).cardCategory);
                    ((OnAccountCreatePaymentActivity)context).cardChecks();

                    ((OnAccountCreatePaymentActivity)context).cardDialog.dismiss();

               }else  if(from.equalsIgnoreCase("SINGLE")){
                    ((CreatePaymentActivity)context).tvCharge.setText("Service Charge("+finantialSiteDOS.get(pos).cardCharges+"%)");
                    ((CreatePaymentActivity)context).tvCardSelection.setText(""+finantialSiteDOS.get(pos).cardCategory);
                    ((CreatePaymentActivity)context).cardChecks();

                    ((CreatePaymentActivity)context).cardDialog.dismiss();

//                   ((CreatePaymentActivity)context).tvCardSelection.setText(""+finantialSiteDOS.get(pos).cardType);
//                   ((CreatePaymentActivity)context).cardDialog.dismiss();

               }
                else{
                    ((com.tbs.brothersgas.haadhir.collector.CreatePaymentActivity)context).tvCharge.setText("Service Charge("+finantialSiteDOS.get(pos).cardCharges+"%)");
                    ((com.tbs.brothersgas.haadhir.collector.CreatePaymentActivity)context).tvCardSelection.setText(""+finantialSiteDOS.get(pos).cardCategory);
                    ((com.tbs.brothersgas.haadhir.collector.CreatePaymentActivity)context).cardChecks();

                    ((com.tbs.brothersgas.haadhir.collector.CreatePaymentActivity)context).cardDialog.dismiss();

//                   ((CreatePaymentActivity)context).tvCardSelection.setText(""+finantialSiteDOS.get(pos).cardType);
//                   ((CreatePaymentActivity)context).cardDialog.dismiss();

                }

            }
        });


    }

    @Override
    public int getItemCount() {
        return finantialSiteDOS.size();
    }

}
