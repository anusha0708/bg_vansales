package com.tbs.brothersgas.haadhir.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tbs.brothersgas.haadhir.Activitys.SiteDetailsActivity;
import com.tbs.brothersgas.haadhir.Model.SiteDO;
import com.tbs.brothersgas.haadhir.R;

import java.util.ArrayList;
import java.util.List;

public class SiteAdapter extends RecyclerView.Adapter<SiteAdapter.MyViewHolder>  {

    private List<SiteDO> siteDOS;
    private Context context;

    public void refreshAdapter(ArrayList<SiteDO> siteDOs) {
        this.siteDOS = siteDOs;
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvSiteName, tvSiteId;
        private LinearLayout llDetails;

        public MyViewHolder(View view) {
            super(view);
            llDetails = (LinearLayout) view.findViewById(R.id.llDetails);
            tvSiteName = (TextView) view.findViewById(R.id.tvSiteName);
            tvSiteId = (TextView) view.findViewById(R.id.tvSiteId);



        }
    }


    public SiteAdapter(Context context, List<SiteDO> siteDOS) {
        this.context = context;
        this.siteDOS = siteDOS;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.site_data, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        final SiteDO siteDO = siteDOS.get(position);
        holder.tvSiteName.setText(siteDO.siteName);
        holder.tvSiteId.setText("" + siteDO.siteId);

        holder.llDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, SiteDetailsActivity.class);
                intent.putExtra("Code", siteDO.siteId);
                context.startActivity(intent);


            }
        });


    }

    @Override
    public int getItemCount() {
        return siteDOS.size();
    }

}
