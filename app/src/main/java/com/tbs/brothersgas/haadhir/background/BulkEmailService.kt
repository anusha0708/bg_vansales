package com.tbs.brothersgas.haadhir.background

import android.app.IntentService
import android.content.Context
import android.content.Intent
import android.widget.Toast
import com.google.gson.Gson

import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryMainDO
import com.tbs.brothersgas.haadhir.pdfs.BulkDeliveryNotePDF
import com.tbs.brothersgas.haadhir.pdfs.CYLDeliveryNotePDF

class BulkEmailService : IntentService("EmailService") {
    internal var activeDeliveryMainDO: ActiveDeliveryMainDO = ActiveDeliveryMainDO()
    internal var resutlString = ""
    internal var context: Context = this

    override fun onHandleIntent(intent: Intent?) {
//        Toast.makeText(context, "Started.......", Toast.LENGTH_LONG).show()
        resutlString = intent!!.getStringExtra("object")

        activeDeliveryMainDO = Gson().fromJson(resutlString, ActiveDeliveryMainDO::class.java)

        BulkDeliveryNotePDF(this.applicationContext).createDeliveryPDF(activeDeliveryMainDO, "AUTO").CreateDeliveryPDF().execute()
    }

}
