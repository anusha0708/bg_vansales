package com.tbs.brothersgas.haadhir.Activitys

import android.app.Dialog
import android.content.Intent
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.*
import com.tbs.brothersgas.haadhir.Adapters.*
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryDO
import com.tbs.brothersgas.haadhir.Model.NonScheduledProductMainDO
import com.tbs.brothersgas.haadhir.Model.ProductDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.ActiveDeliveryRequest
import com.tbs.brothersgas.haadhir.Requests.CurrentNonScheduledStockRequest
import com.tbs.brothersgas.haadhir.common.AppConstants
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.listeners.DBProductsListener
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import java.util.ArrayList


class DamageAddProductActivity : BaseActivity(), DBProductsListener {
    override fun updateData() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    lateinit var tvSelection: TextView
    lateinit var tvNoDataFound: TextView
    lateinit var dialog: Dialog
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var llOrderHistory: RelativeLayout
    lateinit var customerId: String
    private lateinit var btnAdd: Button
    //    lateinit var loadStockAdapter: ScheduledProductsAdapter
    lateinit var tvScreenTitles: TextView
    private var tvNoOrders: TextView? = null
    private var loadStockDoS = ArrayList<ActiveDeliveryDO>()
    lateinit var llSearch: LinearLayout
    lateinit var etSearch: EditText
    lateinit var ivClearSearch: ImageView
    lateinit var ivSearchs: ImageView
    lateinit var ivGoBack: ImageView
    lateinit var routeId: String
    lateinit var fromm: String

    private lateinit var loadStockAdapter: DamageProductsAdapter

    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.scheduled_add_product_screen, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        flToolbar.visibility = View.GONE
        disableMenuWithBackButton()
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
        disableMenuWithBackButton()
        routeId = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "")
        if (intent.hasExtra("CustomerId")) {
            customerId = intent.extras.getString("CustomerId")
        }

        initializeControls()
        if (intent.hasExtra("FROM")) {
            fromm = intent.extras.getString("FROM")
        }
        loadStockAdapter = DamageProductsAdapter(this@DamageAddProductActivity, ArrayList(), "NONBG", "")
        recycleview.setAdapter(loadStockAdapter)

//        loadStockAdapter = ScheduledProductsAdapter(this@DamageAddProductActivity, ArrayList(), "AddProducts", "")
//        recycleview.setAdapter(loadStockAdapter)
        loadNonScheduleProducts()
        btnAdd.setOnClickListener {
            var isProductsAdded: Boolean? = false
            if (loadStockAdapter != null && loadStockAdapter.getSelectedLoadStockDOs() != null && loadStockAdapter.getSelectedLoadStockDOs().size > 0) {
                for (i in loadStockAdapter.getSelectedLoadStockDOs().indices) {
                    if (loadStockAdapter.getSelectedLoadStockDOs().get(i).isProductAdded && loadStockAdapter.getSelectedLoadStockDOs().get(i).orderedQuantity > 0) {
                        isProductsAdded = true
                        break
                    }
                }
            }
            if (isProductsAdded!!) {
                showAppCompatAlert("", "you cannot modify products again...", "ADD", "Cancel", "SUCCESS", true)


            } else {
                showToast("Please select atleast one product")
            }
        }

        ivGoBack.setOnClickListener { finish() }


    }


    private fun loadNonScheduleProducts() {
        var shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")

        if (Util.isNetworkAvailable(this)) {

            val loadVanSaleRequest = ActiveDeliveryRequest(shipmentId, this@DamageAddProductActivity)
//        showLoader()
            loadVanSaleRequest.setOnResultListener { isError, loadStockMainDo ->
                hideLoader()
                if (isError) {
                    showToast("No vehicle data found.")
                } else {
                    hideLoader()
                    val previousActiveDeliveryDos: ArrayList<ActiveDeliveryDO> = StorageManager.getInstance(this).getDamageDeliveryItems(this@DamageAddProductActivity)
                    recycleview.visibility = VISIBLE
                    tvNoDataFound.visibility = GONE
                    btnAdd.visibility = VISIBLE
                    if (loadStockMainDo.activeDeliveryDOS != null && loadStockMainDo.activeDeliveryDOS.size > 0) {

                        for (i in loadStockMainDo.activeDeliveryDOS.indices) {
                            for (k in previousActiveDeliveryDos.indices) {
                                if (previousActiveDeliveryDos.get(k).product.equals(loadStockMainDo.activeDeliveryDOS.get(i).product, true)) {
//                                loadStockMainDo.activeDeliveryDOS.get(i).totalQuantity = loadStockMainDo.activeDeliveryDOS.get(i).totalQuantity - previousActiveDeliveryDos.get(k).totalQuantity;
                                    loadStockMainDo.activeDeliveryDOS.get(i).orderedQuantity = loadStockMainDo.activeDeliveryDOS.get(i).orderedQuantity - previousActiveDeliveryDos.get(k).orderedQuantity;
                                }
                            }
                        }
//                        val activeDeliveryDO = ActiveDeliveryDO();
//                        activeDeliveryDO.product = loadStockDO.product
//                        activeDeliveryDO.productDescription = loadStockDO.productDescription
//                        activeDeliveryDO.stockUnit = loadStockDO.stockUnit
//                        activeDeliveryDO.weightUnit = loadStockDO.weightUnit
//                        activeDeliveryDO.quantity = loadStockDO.quantity
//
//                        activeDeliveryDO.totalQuantity = loadStockDO.quantity
//                        activeDeliveryDO.orderedQuantity = loadStockDO.quantity
//                        activeDeliveryDO.shipmentProductType = AppConstants.FixedQuantityProduct
////                                preferenceUtils.saveString(PreferenceUtils.ShipmentProductsType, AppConstants.FixedQuantityProduct)
////                                activeDeliveryDO.status                        = loadStockDO.status
//
//                        loadStockDoS.add(activeDeliveryDO)
//                    }
//                    if(loadStockAdapter != null){
//                        loadStockAdapter = ScheduledProductsAdapter(this@DamageAddProductActivity, loadStockMainDo.activeDeliveryDOS, "AddProducts", "")
//                        recycleview.setAdapter(loadStockAdapter)
//                    }
//                    else{
//                    }
                        loadStockAdapter.refreshAdapter(loadStockMainDo.activeDeliveryDOS)
                    }

                }
            }
            loadVanSaleRequest.execute()

        } else {
            showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

        }

    }

    override fun initializeControls() {

        recycleview = findViewById<View>(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        tvNoDataFound = findViewById<View>(R.id.tvNoDataFound) as TextView
        btnAdd = findViewById<View>(R.id.btnAdd) as Button
        recycleview.setLayoutManager(androidx.recyclerview.widget.LinearLayoutManager(this, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false))
        tvScreenTitles = findViewById<View>(R.id.tvScreenTitles) as TextView
        llSearch = findViewById<LinearLayout>(R.id.llSearch)
        etSearch = findViewById<EditText>(R.id.etSearch)
        ivClearSearch = findViewById<ImageView>(R.id.ivClearSearch)
        ivGoBack = findViewById<ImageView>(R.id.ivGoBack)
        ivSearch = findViewById(R.id.ivSearchs)
        ivSearch.setVisibility(GONE)

    }

    override fun onButtonYesClick(from: String) {

        if ("SUCCESS".equals(from, ignoreCase = true)) {
            val loadStockDos = loadStockAdapter!!.getSelectedLoadStockDOs();// here need add the products, clear data ook
            val previousProductsList = StorageManager.getInstance(this@DamageAddProductActivity).getDamageDeliveryItems(this@DamageAddProductActivity);
            previousProductsList.addAll(loadStockDos)
            StorageManager.getInstance(this@DamageAddProductActivity).saveDamageDeliveryItems(this@DamageAddProductActivity, previousProductsList);
            val intent = Intent();
            intent.putExtra("AddedProducts", loadStockDos);

            setResult(1, intent)
            finish()
        } else
            if ("FAILURE".equals(from, ignoreCase = true)) {


                //finish()
            }

    }
}