package com.tbs.brothersgas.haadhir.Model;

import java.io.Serializable;

public class InvoiceHistoryDO implements Serializable {

    public String invoiceNumber       = "";
    public String reference           = "";
    public String accountingDate      = "";
    public String amount              = "";
    public String customer            = "";
    public String customerDescription = "";
    public String site                = "";
    public String siteDescription     = "";
    public String currency            = "";



}
