package com.tbs.brothersgas.haadhir.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tbs.brothersgas.haadhir.Activitys.CreatePaymentActivity;
import com.tbs.brothersgas.haadhir.Activitys.OnAccountCreatePaymentActivity;
import com.tbs.brothersgas.haadhir.Model.BankDO;
import com.tbs.brothersgas.haadhir.R;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;

import java.util.ArrayList;

public class BankListAdapter extends RecyclerView.Adapter<BankListAdapter.MyViewHolder>  {

    private ArrayList<BankDO> bankDOS;
    private Context context;
    private int type=1;





    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName;
        private LinearLayout llDetails;

        public MyViewHolder(View view) {
            super(view);
            tvName = (TextView) view.findViewById(R.id.tvName);



        }
    }


    public BankListAdapter(Context context, ArrayList<BankDO> bankDoS,int typE) {
        this.context = context;
        this.bankDOS = bankDoS;
        this.type    = typE;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.simple_text, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final BankDO bankDO = bankDOS.get(position);

        holder.tvName.setText(""+bankDO.bankName);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = holder.getAdapterPosition();
                PreferenceUtils preferenceUtils = new PreferenceUtils(context);
                preferenceUtils.saveString(PreferenceUtils.BANK_REASON, String.valueOf(bankDOS.get(pos).bankName));
                if(type==1){
                    ((CreatePaymentActivity)context).tvBankSelection.setText(""+bankDOS.get(pos).bankName);
                    ((CreatePaymentActivity)context).bankDialog.dismiss();
                }else if(type==2){
                    ((com.tbs.brothersgas.haadhir.collector.CreatePaymentActivity)context).tvBankSelection.setText(""+bankDOS.get(pos).bankName);
                    ((com.tbs.brothersgas.haadhir.collector.CreatePaymentActivity)context).bankDialog.dismiss();
                }else {
                    ((OnAccountCreatePaymentActivity)context).tvBankSelection.setText(""+bankDOS.get(pos).bankName);
                    ((OnAccountCreatePaymentActivity)context).bankDialog.dismiss();
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return bankDOS.size();
    }

}
