package com.tbs.brothersgas.haadhir.background

import android.app.IntentService
import android.content.Context
import android.content.Intent
import android.widget.Toast
import com.google.gson.Gson

import com.tbs.brothersgas.haadhir.Model.CreateInvoicePaymentDO
import com.tbs.brothersgas.haadhir.pdfs.BGInvoicePdf

class InvoiceEmailService: IntentService("InvoiceEmailService") {
    internal var invoiceMainDO: CreateInvoicePaymentDO = CreateInvoicePaymentDO()
    internal var context: Context = this
    internal var resutlString = ""
    override fun onHandleIntent(intent: Intent?) {
//        Toast.makeText(context,"Started.......",Toast.LENGTH_LONG).show()
        resutlString = intent!!.getStringExtra("object")

        invoiceMainDO = Gson().fromJson(resutlString, CreateInvoicePaymentDO::class.java)

        BGInvoicePdf.getBuilder(this).build(invoiceMainDO, "AUTO");
    }

}
