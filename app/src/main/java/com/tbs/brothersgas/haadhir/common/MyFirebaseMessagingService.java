package com.tbs.brothersgas.haadhir.common;


import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.tbs.brothersgas.haadhir.Activitys.Cancel_RescheduleActivity;
import com.tbs.brothersgas.haadhir.Activitys.DashBoardActivity;
import com.tbs.brothersgas.haadhir.Activitys.ScheduledCaptureDeliveryActivity;
import com.tbs.brothersgas.haadhir.Activitys.SelectedSalesReturnDetailsActivity;
import com.tbs.brothersgas.haadhir.Activitys.SkipActivity;
import com.tbs.brothersgas.haadhir.R;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.Random;


/**
 * The type My firebase messaging service.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {
    PendingIntent intent;
    private static final String TAG = "MyFirebaseMsgService";

    String body, customerName;
    int type;
    private String requestName;
    int documentType;

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);

    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {


        Map<String, String> data = remoteMessage.getData();

        Gson gson = new Gson();
        String json = gson.toJson(data);

        Log.i(TAG, "json response-->" + remoteMessage.getData());

        try {
            JSONObject objt = new JSONObject(json);
            body = objt.optString("body");
            requestName = objt.optString("Request Name");
            customerName = objt.optString("Customer Name");
            type = objt.optInt("Request Type");
//            body = "You have a new ";


        } catch (JSONException e) {
            e.printStackTrace();
        }
        sendNormalNotification(MyFirebaseMessagingService.this, body, customerName, type);
    }

    public void sendNormalNotification(Context ctx, String msgbody, String customerName, int type) {

        if (ctx == null) {
            return;
        }


        String name = getPackageName();
        String id = "my_package_channel_1"; // The user-visible name of the channel.
        String description = "my_package_first_channel"; // The user-visible description of the channel.
        NotificationChannel mChannel = null;
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (notificationManager != null) {
                mChannel = notificationManager.getNotificationChannel(id);
                if (mChannel == null) {
                    mChannel = new NotificationChannel(id, name, NotificationManager.IMPORTANCE_HIGH);
                    mChannel.setDescription(description);
                    mChannel.enableVibration(true);
                    mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                    notificationManager.createNotificationChannel(mChannel);
                }

            }
        }


        NotificationCompat.Builder notificationBuilder;
        if (mChannel != null) {
            notificationBuilder = new NotificationCompat.Builder(this, mChannel.getId());
        } else {
            notificationBuilder = new NotificationCompat.Builder(this);
        }

        int notificationId = new Random().nextInt(); // just use a counter in some util class...
//        intent = PendingIntent.getActivity(this, 0,
//                new Intent(this, DashBoardActivity.class).
//                        addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP),
//                PendingIntent.FLAG_ONE_SHOT);


//        if (type==1||type==2||type==3||type==8) {
//            intent = PendingIntent.getActivity(this, 0,
//                    new Intent(this, ScheduledCaptureDeliveryActivity.class).
//                            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP),
//                    PendingIntent.FLAG_ONE_SHOT);
//        } else if (type==6||type==7) {
//            intent = PendingIntent.getActivity(this, 0,
//                    new Intent(this, Cancel_RescheduleActivity.class).
//                            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP),
//                    PendingIntent.FLAG_ONE_SHOT);
//        }
//        else if (type==5) {
//            intent = PendingIntent.getActivity(this, 0,
//                    new Intent(this, SkipActivity.class).
//                            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP),
//                    PendingIntent.FLAG_ONE_SHOT);
//        }
//        else if (type==4) {
//            intent = PendingIntent.getActivity(this, 0,
//                    new Intent(this, SelectedSalesReturnDetailsActivity.class).
//                            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP),
//                    PendingIntent.FLAG_ONE_SHOT);
//        }else {
//            intent = PendingIntent.getActivity(this, 0,
//                    new Intent(this, DashBoardActivity.class).
//                            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP),
//                    PendingIntent.FLAG_ONE_SHOT);
//        }
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        notificationBuilder.setSmallIcon(getNotificationIcon());
        notificationBuilder.setColor(ContextCompat.getColor(ctx, R.color.colorPrimary));
        notificationBuilder.setContentTitle("Approval Request");
//        notificationBuilder.setContentText(customerName);
//        notificationBuilder.setContentText(type);
        notificationBuilder.setAutoCancel(true);
        notificationBuilder.setPriority(Notification.PRIORITY_MAX);
        notificationBuilder.setWhen(0);
        notificationBuilder.setSound(defaultSoundUri);
        notificationBuilder.setPriority(Notification.PRIORITY_HIGH);

        notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(msgbody));
        notificationBuilder.setContentText(msgbody);
        notificationBuilder.setContentIntent(intent);
        if (notificationManager != null) {
            notificationManager.notify(notificationId, notificationBuilder.build());
        }
        Intent intent1 = new Intent("custom-event-name");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent1);


    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
//        return useWhiteIcon ? R.drawable.bg_logo : R.drawable.bg_logo;
        return useWhiteIcon ? R.drawable.bg : R.drawable.bg;

    }

}
