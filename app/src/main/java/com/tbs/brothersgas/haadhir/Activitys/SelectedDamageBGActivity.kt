package com.tbs.brothersgas.haadhir.Activitys

import android.content.Intent
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import android.widget.*
import com.tbs.brothersgas.haadhir.Adapters.DamageScheduledProductsAdapter
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryDO
import com.tbs.brothersgas.haadhir.Model.LoadStockDO
import com.tbs.brothersgas.haadhir.Model.LoanReturnDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.*
import com.tbs.brothersgas.haadhir.utils.CalendarUtils
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import kotlin.collections.ArrayList
import kotlin.collections.LinkedHashMap

//
class SelectedDamageBGActivity : BaseActivity() {
    lateinit var invoiceAdapter: DamageScheduledProductsAdapter
    lateinit var loadStockDOs: ArrayList<LoadStockDO>
    lateinit var recycleview : androidx.recyclerview.widget.RecyclerView
    lateinit var tvNoDataFound:TextView
    lateinit var tvBalanceQty:TextView
    lateinit var tvReturnedQty:TextView
    lateinit var tvIssuedQty:TextView
    lateinit var tvOpeningQty:TextView
    lateinit var openingQty:String
    private var issuedQty : Int = 0
    private var type : Int = 1

    private var   returnQty :Double=0.0
    private var balanceQty:Double=0.0
    private lateinit var btnConfirm: Button
    private lateinit var rbEmptyCylinder: RadioButton
    private lateinit var rbSalesReturn: RadioButton
    private var loanReturnDOsMap: LinkedHashMap<String, ArrayList<LoanReturnDO>> = LinkedHashMap()
    private var loanReturnDos: ArrayList<ActiveDeliveryDO> = ArrayList()

    override fun initialize() {
      val  llCategories = getLayoutInflater().inflate(R.layout.selected_nonbg, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
//        ivAdd.setVisibility(View.VISIBLE)

        if (intent.hasExtra("Products")) {
            loanReturnDos = intent.getSerializableExtra("Products") as ArrayList<ActiveDeliveryDO>
        }


        initializeControls()
        invoiceAdapter = DamageScheduledProductsAdapter(this@SelectedDamageBGActivity, loanReturnDos,"","")
        recycleview.adapter = invoiceAdapter


        btnConfirm.setOnClickListener {
            showAppCompatAlert("", "you cannot modify this again..Are You Sure You Want to Create?", "OK", "Cancel", "SUCCESS", true)

        }


    }




    override fun initializeControls() {
        tvScreenTitle.setText("Selected Damage Cylinders")
        recycleview = findViewById(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
        tvNoDataFound = findViewById(R.id.tvNoDataFound) as TextView
        tvOpeningQty = findViewById(R.id.tvOpeningQty) as TextView
        tvIssuedQty = findViewById(R.id.tvIssuedQty) as TextView
        tvReturnedQty = findViewById(R.id.tvReturnedQty) as TextView
        tvBalanceQty = findViewById(R.id.tvBalanceQty) as TextView
        recycleview.setLayoutManager(linearLayoutManager)
        btnConfirm = findViewById(R.id.btnConfirm) as Button


    }

    override fun onButtonYesClick(from: String) {

        if ("SUCCESS".equals(from, ignoreCase = true)) {
            updateDamageCylinders()
        } else
            if ("FAILURE".equals(from, ignoreCase = true)) {


                //finish()
            }

    }
    private fun updateDamageCylinders() {
      var  shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")

        val request = UpdateDamageCylindersRequest(shipmentId, loanReturnDos, this@SelectedDamageBGActivity)
        request.setOnResultListener { isError, approveDO ->
            hideLoader()
            if (approveDO != null) {
                if (isError) {
                    showAppCompatAlert("", " please try again.", "Ok", "Cancel", "", false)

                } else {
                    if (approveDO.flag == 1) {
                        showToast("Updated Successfully")
                        setResult(98, intent)
                        finish()
                    } else {
                        showAppCompatAlert("", " please try again.", "Ok", "Cancel", "", false)

                    }
                }
            }
        }
        request.execute()
    }



}