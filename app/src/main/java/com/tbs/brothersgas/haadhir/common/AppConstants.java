package com.tbs.brothersgas.haadhir.common;


import android.net.Uri;

import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryDO;
import com.tbs.brothersgas.haadhir.Model.LoadStockDO;

import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.util.ArrayList;

public class AppConstants {


    public static ArrayList<LoadStockDO> listStockDO = new ArrayList<>();
    public static ArrayList<ActiveDeliveryDO> productDO = new ArrayList<>();


    public static String DATABASE_NAME = "brothersgas.sqlite";
    public static String DATABASE_PATH = "";
    public static String Trans_From_Date = "";
    public static String Trans_To_Date = "";
    public static String Trans_Date = "";

    public static Uri imageUri = null;
    public static String sdCardPath;
    public static String folderName;
    public static String tempPath;
    public static String message="";
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int SELECT_FILE = 2;
    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 3;

    public static final String MeterReadingProduct = "Bulk";
    public static final String FixedQuantityProduct = "Cylinder";
    public static final String ShipmentListView = "Shipment List View";
    public static String EnableShipments = "";
    public static boolean CapturedReturns = false;
    public static final String Goudy_Old_Style_Bold			= "bgb.ttf";
    public static final String Goudy_Old_Style_Italic			    = "bgi.ttf";
    public static final String Goudy_Old_Style_Normal			    = "bgs.ttf";

    public static final String MONTSERRAT_REGULAR_TYPE_FACE			= "montserrat_regular.ttf";
    public static final String MONTSERRAT_MEDIUM_TYPE_FACE			= "montserrat_medium.ttf";
    public static final String MONTSERRAT_BOLD_TYPE_FACE			= "montserrat_bold.ttf";
    public static final String MONTSERRAT_LIGHT_TYPE_FACE			= "montserrat_light.ttf";
    public static File file2;
    public static File file3;

    public static File file1;
}
