package com.tbs.brothersgas.haadhir.Model;

import java.io.Serializable;

public class VehicleCheckMainDO implements Serializable {

    public String flag = "";
    public String driverId = "";
    public String routingId = "";
    public String vehicleCode = "";
    public String itemsInVehicle = "";
    public String safetyEquipment = "";
    public String vehicleDocument = "";


}
