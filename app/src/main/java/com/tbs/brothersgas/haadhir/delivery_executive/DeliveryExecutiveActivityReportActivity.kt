package com.tbs.brothersgas.haadhir.delivery_executive

import android.app.DatePickerDialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.brothersgas.haadhir.Activitys.BaseActivity
import com.tbs.brothersgas.haadhir.Model.*
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.common.AppConstants
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.pdfs.PaymentReceiptPDF
import com.tbs.brothersgas.haadhir.prints.PrinterConnection
import com.tbs.brothersgas.haadhir.prints.PrinterConstants
import com.tbs.brothersgas.haadhir.utils.CalendarUtils
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import kotlinx.android.synthetic.main.activity_delivery_executive_activity_report.*
import kotlinx.android.synthetic.main.activity_delivery_executive_activity_report.btnSearch
import kotlinx.android.synthetic.main.activity_delivery_executive_activity_report.recycleview
import kotlinx.android.synthetic.main.activity_delivery_executive_activity_report.tvFrom
import kotlinx.android.synthetic.main.activity_delivery_executive_activity_report.tvTO
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


class DeliveryExecutiveActivityReportActivity : BaseActivity() {
    var fromId = 0
    var startDate = ""
    var endDate = ""
    var from = ""
    var to = ""

    private var cday: Int = 0
    private var cmonth: Int = 0
    private var cyear: Int = 0
    lateinit var activeDeliveryMainDO: ActiveDeliveryMainDO

    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.activity_delivery_executive_activity_report, null) as View
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            finish()
        }
        disableMenuWithBackButton()
        initializeControls()
        tvScreenTitle.setText(getString(R.string.activity_list))
    }

    override fun initializeControls() {


        setDatesIntitially()
        setDatePicker()
        dateSeachStuff()

        btnPrint.setOnClickListener {
            if (activeDeliveryMainDO != null
                    && activeDeliveryMainDO.activeDeliveryDOS != null && activeDeliveryMainDO.activeDeliveryDOS.size > 0) {
                prepareActivityReport()
            }
        }
    }

    private fun setDatesIntitially() {
        var from = CalendarUtils.getDate()

        val aMonth = from.substring(4, 6)
        val ayear = from.substring(0, 4)
        val aDate = from.substring(Math.max(from.length - 2, 0))


        tvFrom.setText("" + aDate + " - " + aMonth + " - " + ayear)
        tvTO.setText("" + aDate + " - " + aMonth + " - " + ayear)
        startDate = ayear + aMonth + aDate
        endDate = ayear + aMonth + aDate
        getList()

    }

    private fun dateSeachStuff() {

        btnSearch.setOnClickListener {

            if (!TextUtils.isEmpty(startDate) && !TextUtils.isEmpty(endDate)) {
                val formatter: DateFormat
                formatter = SimpleDateFormat("yyyyMMdd")

//                formatter = SimpleDateFormat("dd-MM-yyyy")
                val startDate = formatter.parse(startDate) as Date
                val endDate = formatter.parse(endDate) as Date
                if (startDate.after(endDate)) {
                    Toast.makeText(this, getString(R.string.select_date), Toast.LENGTH_SHORT).show()
                } else {
                    getList()
                }
            } else {
                if (TextUtils.isEmpty(startDate)) {
                    Toast.makeText(this, getString(R.string.select_start_date), Toast.LENGTH_SHORT).show()
                } else if (!TextUtils.isEmpty(endDate)) {
                    Toast.makeText(this, getString(R.string.select_end_date), Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun getList() {

        if (Util.isNetworkAvailable(this)) {
            val driverListRequest = DeliveryExecutiveListRequest(this,
                    3,
                    startDate,
                    endDate)
            driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                hideLoader()
                if (isError) {

                    //invoiceList()
                } else {
                    if (activeDeliveryDo != null) {
                        if (activeDeliveryDo.activeDeliveryDOS != null && activeDeliveryDo.activeDeliveryDOS.size > 0) {
                            activeDeliveryMainDO = activeDeliveryDo
                            tvNoOrders.visibility = View.GONE
                            recycleview.visibility = View.VISIBLE
                            setDataToViews(activeDeliveryDo.activeDeliveryDOS)
                        } else {
                            tvNoOrders.visibility = View.VISIBLE
                            recycleview.visibility = View.GONE
                            llUserId.visibility = View.GONE
                            llName.visibility = View.GONE
                            llDeliveries.visibility = View.GONE
                            llItem.visibility = View.GONE
                            btnPrint.visibility = View.GONE
                        }
                    }

                }
            }
            driverListRequest.execute()

        } else {
            showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

        }
    }

    private fun setDataToViews(activeDeliveryDOS: java.util.ArrayList<ActiveDeliveryDO>) {

        val name = preferenceUtils.getStringFromPreference(PreferenceUtils.LOGIN_DRIVER_NAME, "")
        val id = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "")

        llUserId.visibility = View.VISIBLE
        tvUserId.setText(id)
        llName.visibility = View.VISIBLE
        tvUserName.setText(name)
        llDeliveries.visibility = View.VISIBLE
        tvDeliveryList.setText(activeDeliveryDOS.size.toString())
        llItem.visibility = View.VISIBLE
        recycleview.isNestedScrollingEnabled = true
        var siteAdapter = DeliveryExecutiveActivityReoprtAdapter(this, activeDeliveryDOS, "")
        recycleview.setAdapter(siteAdapter)
        btnPrint.visibility = View.VISIBLE

    }

    private fun setDatePicker() {

        val c = Calendar.getInstance()
        val cyear = c.get(Calendar.YEAR)
        val cmonth = c.get(Calendar.MONTH)
        val cday = c.get(Calendar.DAY_OF_MONTH)
        tvFrom.setOnClickListener {
            val datePicker = DatePickerDialog(this, datePickerListener, cyear, cmonth, cday)
            datePicker.datePicker.maxDate = Calendar.getInstance().timeInMillis
            datePicker.show()
        }

        tvTO.setOnClickListener {
            val datePicker = DatePickerDialog(this, endDatePickerListener, cyear, cmonth, cday)
            datePicker.datePicker.maxDate = Calendar.getInstance().timeInMillis
            datePicker.show()
        }


    }

    private val datePickerListener: DatePickerDialog.OnDateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        cday = dayOfMonth
        cmonth = monthOfYear + 1
        cyear = year
        var monthString = cmonth.toString()
        if (monthString.length == 1) {
            monthString = "0$monthString"
        } else {
            monthString = cmonth.toString()
        }

        var dayString = cday.toString()
        if (dayString.length == 1) {
            dayString = "0$dayString"
        } else {
            dayString = cday.toString()
        }
        startDate = "" + cyear + monthString + dayString;
        tvFrom.setText("" + dayString + "-" + monthString + "-" + cyear)
//        from = tvFrom.text.toString()

    }
    private val endDatePickerListener: DatePickerDialog.OnDateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        cday = dayOfMonth
        cmonth = monthOfYear + 1
        cyear = year
        var monthString = cmonth.toString()
        if (monthString.length == 1) {
            monthString = "0$monthString"
        } else {
            monthString = cmonth.toString()
        }

        var dayString = cday.toString()
        if (dayString.length == 1) {
            dayString = "0$dayString"
        } else {
            dayString = cday.toString()
        }
        endDate = "" + cyear + monthString + dayString;
        tvTO.setText("" + dayString + "-" + monthString + "-" + cyear)
        to = tvTO.text.toString()

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == 1) {
//            selectInvoiceList()
        }
    }


    private fun prepareActivityReport() {
        AppConstants.Trans_From_Date = tvFrom.text.toString().trim().replace("-", "/");
        AppConstants.Trans_To_Date = tvTO.text.toString().trim().replace("-", "/");
        printDocument(this.activeDeliveryMainDO, PrinterConstants.PrintDelivetryExecutiveReport)

    }


    private fun printDocument(obj: Any, from: Int) {
        val printConnection = PrinterConnection.getInstance(this@DeliveryExecutiveActivityReportActivity)
        if (printConnection.isBluetoothEnabled()) {
            printConnection.connectToPrinter(obj, from)
        } else {
//            PrinterConstants.PRINTER_MAC_ADDRESS = ""
            showToast("Please enable your mobile Bluetooth.")
//            showAppCompatAlert("", "Please enable your mobile Bluetooth.", "Enable", "Cancel", "EnableBluetooth", false)
        }
    }

    private fun pairedDevices() {
        val pairedDevices = BluetoothAdapter.getDefaultAdapter().getBondedDevices()
        if (pairedDevices.size > 0) {
            for (device in pairedDevices) {
                val deviceName = device.getName()
                val mac = device.getAddress() // MAC address
//                if (StorageManager.getInstance(this).getPrinterMac(this).equals("")) {
                StorageManager.getInstance(this).savePrinterMac(this, mac);
//                }
                Log.e("Bluetooth", "Name : " + deviceName + " Mac : " + mac)
                break
            }
        }
    }

    override fun onResume() {
        super.onResume()
        pairedDevices()
    }

    override fun onStart() {
        super.onStart()
        val filter = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        registerReceiver(mReceiver, filter)
//        val filter = IntentFilter(Intent.PAIRIN.ACTION_FOUND Intent. "android.bluetooth.device.action.PAIRING_REQUEST");
//        registerReceiver(mReceiver, filter)
//        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
//        mBluetoothAdapter.startDiscovery()
//        val filter2 = IntentFilter( "android.bluetooth.device.action.PAIRING_REQUEST")
//        registerReceiver(mReceiver, filter2)

    }

    override fun onDestroy() {
        unregisterReceiver(mReceiver)
        super.onDestroy()
    }

    private val mReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            if (BluetoothDevice.ACTION_FOUND == action) {
                val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE);
                PrinterConstants.bluetoothDevices.add(device)
                Log.e("Bluetooth", "Discovered => name : " + device.name + ", Mac : " + device.address)
            } else if (action.equals(BluetoothDevice.ACTION_ACL_CONNECTED)) {
                Log.e("Bluetooth", "status : ACTION_ACL_CONNECTED")
                pairedDevices()
            } else if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                val state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);

                if (state == BluetoothAdapter.STATE_OFF) {
                    Log.e("Bluetooth", "status : STATE_OFF")
                } else if (state == BluetoothAdapter.STATE_TURNING_OFF) {
                    Log.e("Bluetooth", "status : STATE_TURNING_OFF")
                } else if (state == BluetoothAdapter.STATE_ON) {
                    Log.e("Bluetooth", "status : STATE_ON")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_TURNING_ON) {
                    Log.e("Bluetooth", "status : STATE_TURNING_ON")
                } else if (state == BluetoothAdapter.STATE_CONNECTING) {
                    Log.e("Bluetooth", "status : STATE_CONNECTING")
                } else if (state == BluetoothAdapter.STATE_CONNECTED) {
                    Log.e("Bluetooth", "status : STATE_CONNECTED")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_DISCONNECTED) {
                    Log.e("Bluetooth", "status : STATE_DISCONNECTED")
                }
            }
        }
    }

    private fun createPaymentPDF(createPDFInvoiceDO: PaymentPdfDO) {
        if (createPDFInvoiceDO != null) {
            PaymentReceiptPDF(this).createReceiptPDF(createPDFInvoiceDO, "Email")
            return

        } else {
            //Display error message
        }
    }
}