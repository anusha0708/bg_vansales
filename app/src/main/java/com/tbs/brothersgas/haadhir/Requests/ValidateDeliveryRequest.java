package com.tbs.brothersgas.haadhir.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.brothersgas.haadhir.Activitys.BaseActivity;
import com.tbs.brothersgas.haadhir.Activitys.SignatureActivity;
import com.tbs.brothersgas.haadhir.Model.PodDo;
import com.tbs.brothersgas.haadhir.Model.TankDO;
import com.tbs.brothersgas.haadhir.Model.ValidateDeliveryDO;
import com.tbs.brothersgas.haadhir.database.StorageManager;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;
import com.tbs.brothersgas.haadhir.utils.ProgressTask;
import com.tbs.brothersgas.haadhir.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class ValidateDeliveryRequest extends AsyncTask<String, Void, Boolean> {


    private Context mContext;
    private ValidateDeliveryDO validateDeliveryDO;
    private String id, notes, lattitude, longitude, address;
    String username, password, ip, pool, port, signImg;
    PreferenceUtils preferenceUtils;
    TankDO tankDO;
    Double distance;

    public ValidateDeliveryRequest(Double distance,String sign, String note, String id, String lattitudE, String longitudE, String addresS, Context mContext) {

        this.mContext = mContext;
        this.id = id;
        this.notes = note;
        this.lattitude = lattitudE;
        this.longitude = longitudE;
        this.address = addresS;
        this.signImg = sign;
        this.distance = distance;

    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        public void onCompleted(boolean isError, ValidateDeliveryDO validateDeliveryDO);

    }

    public boolean runRequest() {
        // System.out.println("CUSTOMER ID " + customerId);
        String NAMESPACE = "http://www.adonix.com/WSS";
        String METHOD_NAME = "run";
        String SOAP_ACTION = "CAdxWebServiceXmlCC";
        preferenceUtils = new PreferenceUtils(mContext);
        username = preferenceUtils.getStringFromPreference(PreferenceUtils.A_USER_NAME, "");
        password = preferenceUtils.getStringFromPreference(PreferenceUtils.A_PASSWORD, "");
        ip = preferenceUtils.getStringFromPreference(PreferenceUtils.IP_ADDRESS, "");
        port = preferenceUtils.getStringFromPreference(PreferenceUtils.PORT, "");
        pool = preferenceUtils.getStringFromPreference(PreferenceUtils.ALIAS, "");
        String URL = "http://" + ip + ":" + port + "/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC";
        String nonSheduledId = preferenceUtils.getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, "");

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        // Set all input params                         X10CSAINV3
        request.addProperty("publicName", WebServiceConstants.VALIDATION_DELIVERY);
        String SheduledCode = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_CODE, "");
        String nonSheduledCode = preferenceUtils.getStringFromPreference(PreferenceUtils.NON_VEHICLE_CODE, "");
        String role = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "");
        String idd = preferenceUtils.getStringFromPreference(PreferenceUtils.EMIRATES_ID, "");
        String customerName = preferenceUtils.getStringFromPreference(PreferenceUtils.POD_NAME, "");
        String customerNumber = preferenceUtils.getStringFromPreference(PreferenceUtils.POD_NUMBER, "");


        String mrCode = preferenceUtils.getStringFromPreference(PreferenceUtils.MR_CODE, "");

        Double initialReading = preferenceUtils.getDoubleFromPreference(PreferenceUtils.METER_1, 0.0);
        Double endingReading = preferenceUtils.getDoubleFromPreference(PreferenceUtils.METER_2, 0.0);
         tankDO = StorageManager.getInstance(mContext).getTankData(mContext);

        String expiryDate = preferenceUtils.getStringFromPreference(PreferenceUtils.EXPIRY_DATE, "");
        int qsn2 = preferenceUtils.getIntFromPreference(PreferenceUtils.QSN_2, 0);
        int qsn3 = preferenceUtils.getIntFromPreference(PreferenceUtils.QSN_3, 0);
        int qsn4 = preferenceUtils.getIntFromPreference(PreferenceUtils.QSN_4, 0);
        int qsn5 = preferenceUtils.getIntFromPreference(PreferenceUtils.QSN_5, 0);

        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("I_XX10CLAT", lattitude);
            jsonObject.put("I_XX10CLOG", longitude);
            jsonObject.put("I_XX10CADD", address);
            jsonObject.put("I_YSDHNUM", id);
            jsonObject.put("I_XSTKVCR", nonSheduledId);
            jsonObject.put("I_YCREUSR", role);
            jsonObject.put("I_YEMIRATEID", idd);
            jsonObject.put("I_YMSG", notes);
            jsonObject.put("I_YSIGNATURE", signImg);
//            jsonObject.put("I_MAXDISLIM", distance);

            jsonObject.put("I_YCUSNAME", customerName);
            jsonObject.put("I_YCUSMOB", customerNumber);
            jsonObject.put("I_YSMTR", initialReading);
            jsonObject.put("I_YEMTR", endingReading);

            jsonObject.put("I_YSMTR2", tankDO.getTank3());
            jsonObject.put("I_YEMTR2", tankDO.getTank4());
            jsonObject.put("I_YSMTR3", tankDO.getTank5());
            jsonObject.put("I_YEMTR3", tankDO.getTank6());
            jsonObject.put("I_YSMTR4", tankDO.getTank7());
            jsonObject.put("I_YEMTR4", tankDO.getTank8());
            jsonObject.put("I_YSMTR5", tankDO.getTank9());
            jsonObject.put("I_YEMTR5", tankDO.getTank10());
            jsonObject.put("I_YSMTR6", tankDO.getTank11());
            jsonObject.put("I_YEMTR6", tankDO.getTank12());
            jsonObject.put("I_YSMTR7", tankDO.getTank13());
            jsonObject.put("I_YEMTR7", tankDO.getTank14());
            jsonObject.put("I_YSMTR8", tankDO.getTank15());
            jsonObject.put("I_YEMTR8", tankDO.getTank16());
            jsonObject.put("I_YSMTR9", tankDO.getTank17());
            jsonObject.put("I_YEMTR9", tankDO.getTank18());
            jsonObject.put("I_YSMTR10", tankDO.getTank19());
            jsonObject.put("I_YEMTR10", tankDO.getTank20());



            jsonObject.put("I_YMRCODE", mrCode);
            jsonObject.put("I_YCYLEXPDATE", expiryDate);
            jsonObject.put("I_YGASHOSEFLG", qsn2);
            jsonObject.put("I_YGASREGFLG", qsn3);
            jsonObject.put("I_YCLIPHOSFLG", qsn4);
            jsonObject.put("I_YGASLEAKFLG", qsn5);

            if (SheduledCode.length() > 0) {

                jsonObject.put("I_YVEHCODE", SheduledCode);
            } else {

                jsonObject.put("I_YVEHCODE", nonSheduledCode);
            }


        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        request.addProperty("inputXml", jsonObject.toString());

        SoapObject callcontext = new SoapObject("", "callContext");
        // Set all input params
        callcontext.addProperty("codeLang", "ENG");
        callcontext.addProperty("poolAlias", pool);
        callcontext.addProperty("poolId", "");
        callcontext.addProperty("codeUser", username);
        callcontext.addProperty("password", password);
        callcontext.addProperty("requestConfig", "adxwss.trace.on=off");

        request.addSoapObject(callcontext);


        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.setOutputSoapObject(request);

        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL, 180000);
        androidHttpTransport.debug = true;

        try {
            List<HeaderProperty> headerList = new ArrayList<HeaderProperty>();
            headerList.add(new HeaderProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode((username + ":" + password).getBytes())));


            androidHttpTransport.call(SOAP_ACTION, envelope, headerList);


            SoapObject response = (SoapObject) envelope.getResponse();
            String resultXML = (String) response.getProperty("resultXml");
            if (resultXML != null && resultXML.length() > 0) {
                return parseXML(resultXML);
            } else {
                return false;
            }
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("validated xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();

            validateDeliveryDO = new ValidateDeliveryDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {
                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        //    createPaymentDO.customerDetailsDos = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        //      createPaymentDO = new CustomerDetailsDo();

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {


                        if (attribute.equalsIgnoreCase("O_YSDHNUM")) {
                            if (text.length() > 0) {

                                validateDeliveryDO.shipmentId = text;
                            }


                        } else if (attribute.equalsIgnoreCase("O_YFLG")) {
                            if (text.length() > 0) {

                                validateDeliveryDO.flag = Integer.parseInt(text);
                            }
                        } else if (attribute.equalsIgnoreCase("O_YFLG2")) {
                            if (text.length() > 0) {

                                validateDeliveryDO.flag2 = Integer.parseInt(text);
                            }
                        } else if (attribute.equalsIgnoreCase("O_YSTATUS")) {
                            if (text.length() > 0) {

                                validateDeliveryDO.status = Integer.parseInt(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YMESSAGE")) {
                            if (text.length() > 0) {

                                validateDeliveryDO.message = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YCUR")) {
                            if (text.length() > 0) {

                                validateDeliveryDO.currency = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YCYLISPRI")) {
                            if (text.length() > 0) {

                                validateDeliveryDO.cylPrintFlag = Integer.parseInt(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YCYLISMAIL")) {
                            if (text.length() > 0) {

                                validateDeliveryDO.cylEmailFlag = Integer.parseInt(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YDLVPRI")) {
                            if (text.length() > 0) {

                                validateDeliveryDO.delPrintFlag = Integer.parseInt(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YDLVMAIL")) {
                            if (text.length() > 0) {

                                validateDeliveryDO.delEmailFlag = Integer.parseInt(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YINVPRI")) {
                            if (text.length() > 0) {

                                validateDeliveryDO.invPrintFlag = Integer.parseInt(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YINVMAIL")) {
                            if (text.length() > 0) {

                                validateDeliveryDO.invEmailFlag = Integer.parseInt(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSINVNO")) {
                            if (text.length() > 0) {

                                validateDeliveryDO.invoiceNumber = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YAMT")) {
                            if (text.length() > 0) {

                                validateDeliveryDO.amount = text;
                            }

                        }

                        text = "";

                    }


                    if (endTag.equalsIgnoreCase("GRP")) {
                        // customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        // customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (mContext instanceof SignatureActivity) {
            ProgressTask.getInstance().showProgress(mContext, false, "Validating the Delivery...");

        } else {
            ((BaseActivity) mContext).showLoader();

        }
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        if (mContext instanceof SignatureActivity) {
            ProgressTask.getInstance().closeProgress();

        } else {
            ((BaseActivity) mContext).hideLoader();

        }
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, validateDeliveryDO);
        }

    }
}