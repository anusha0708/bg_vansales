package com.tbs.brothersgas.haadhir.utils;

import android.bluetooth.BluetoothDevice;

import java.util.ArrayList;

public class WebServiceConstants {

    public static final String CONFIGURATION = "XX10CCONF";
    public static final String LOGIN = "X10AADMIN";
    public static final String MULTIPLE_LOGIN = "XLMLGNIN";
    public static final String LOGIN_TIME_CAPTURE = "X10CUSRLGN";
    public static final String SCHEDULED_DRIVER_ID = "XX10CDRID1";
    public static final String NON_SCHEDULED_DRIVER_ID = "X10CUNSTK";
    public static final String ACTIVE_DELIVERY = "X10CACVSDH";

    public static final String PENDING_ORDER_ACTIVE_DELIVERY = "Y10CACVSOH";
    public static final String ACTIVE_DELIVERY_TIME_CAPTURE = "X10CUSRACTD";
    public static final String ADDRESS_LIST = "Y10CBPADD";
    public static final String All_UNPAID_INVOICES_LIST = "Y10CGETDUD";
    public static final String BANK_LIST = "X10CSELBAN";
    public static final String FINANCIAL_SITE_LIST = "Y10CFINFCY";
    public static final String CARD_LIST = "Y10CCRDET";

    public static final String MULTIPLE_SITE_LIST = "XMULFCYSEL";
    public static final String CANCEL_RESCHEDULE = "X10CSDHRES";
    public static final String CANCEL_RESCHEDULE_REQUEST_MODIFY = "Y10CSDHREC";
    public static final String CANCEL_RESCHEDULE_APPROVE = "X10CSDHSTA";
    public static final String CHECKIN_TIME_CAPTURE = "X10CUSRCKI";
    public static final String CHECK_STATUS_APPROVAL = "Y10CWRKSTA";
    public static final String CHECK_STATUS_SALES_ORDER = "Y10CSOHSTA";
    public static final String CHECKOUT_TIME_CAPTURE = "X10CUSRCKT";
    public static final String QTY_CONFIRMATION = "Y10CCNFQTY";
    public static final String CREATE_DELIVERY = "X10CRESDH";
    public static final String CREATE_ORDER = "Y10CRESOH";
    public static final String CREATE_INVOICE = "X10CSINV";
    public static final String CREATE_LOAN_RETURN = "X10CEPTYCL";
    public static final String CREATE_NON_BG = "YNONBGMISR";

    public static final String CREATE_PAYMENT_ALL = "YPAYALL";

    public static final String MIS_ON_ACCOUNT = "Y10CMISPAY";
    public static final String CREATE_SINGLE_PAYMENT = "YPAY";
    public static final String COLLECTOR_MUL_PAY = "Y10CMULPAY";


    public static final String CREATE_SALES_RETURN = "Y10CSRH";
    public static final String CURRENT_SHEDULED_STOCK = "XSCHVANSTK";
    public static final String CURRENT_NONSTOCK = "XNONSCHSTK";
    public static final String PENDING_ORDERS_LIST = "Y10CSOLIST";
    public static final String CUSTOMER_DELIVERY_AUTHORIZATION = "YBPCAUT";
    public static final String SPOT_CUSTOMER_DETAILS = "XX10CYBPC";
    public static final String CUSTOMER_AUTHORIZATION = "XX10CCUST";
    public static final String SPOT_CUSTOMER = "XX10CYBPC1";
    public static final String DEC_SPOT_CUSTOMER = "Y10CDCBPC";

    public static final String CUSTOMER_LOCATION = "X10CLATUPD";
    public static final String CYLINDER_ISSUE_DETAILS = "X10CMISRPT";
    public static final String DAMAGE_REASONS_LIST = "XCYLDAM";
    public static final String DELIVERIES_LIST = "X10CVEROU1";
    public static final String INVOICE_DETAILS = "X10CSIHDET";
    public static final String LOAN_RETURN_LIST = "X10CSDHBPC";
    public static final String MULTIPLE_LOGOUT = "XLMLGOUT";
    public static final String LOGOUT_TIMECAPTURE = "X10CUSRLGT";
    public static final String MASTER_CUSTOMER_LIST = "XX10CYBPC3";
    public static final String MR_LOCATION_LIST = "Y10CLOCODE";
    public static final String POD_MR_LOCATION_LIST = "Y10CMLOCOD";
    public static final String PROFILE_LIST = "YVANPROFIL";

    public static final String MR_LOCATION_UPDATE = "Y10CLOCUPD";
    public static final String SCHEDULED_OPENING_STOCK = "XX10CXLVS1";
    public static final String NON_SCHEDULED_OPENING_STOCK = "X10CUNPRO";
    public static final String PAYMENT_CHEQUE_LIST = "Y10CPAYCHQ";
    public static final String PAYMENT_CASH_LIST = "Y10CPAYCSH";
    public static final String PAYMENT_DETAILS = "X10CPAYDET";
    public static final String POD_TIME_CAPTURE = "X10CUSRPOD";
    public static final String PRE_INVOICE_DETAILS = "X10CPRFOR";
    public static final String PRODUCT_MODIFY = "Y10CQTYMOD";
    public static final String CANCEL_RESCHEDULE_REASONS = "X10CREASON";
    public static final String SALES_RETURN_LIST = "Y10CGETSDH";
    public static final String SKIP_SHIPMENT = "Y10CSDHSKP";
    public static final String LEFT_SHIPMENT = "Y10CDOCSKP";
    public static final String LOAN_MODIFY = "Y10CQTYSDL";

    public static final String UNPAID_INVOICE_LIST = "X10CGETDUD";
    public static final String UPDATE_DAMAGE_CYLINDER = "XCLYDAM1";
    public static final String USER_ACTIVITY_REPORT = "Y10CUSRRPT";
    public static final String VALIDATION_DELIVERY = "X10CVALSDH";
    public static final String APPROVAL_MECHANISM_REQUEST = "Y10CREQ";
    public static final String APPROVAL_MECHANISM_CHECK_STATUS_APPROVAL = "YREQSTA";

    public static final String MULTI_OPENING_SHEDULED_STOCK = "XFCYSTKS";
    public static final String MULTI_OPENING_NON_SHEDULED_STOCK = "XFCYSTKN";

    public static final String MULTI_PRODUCT_CURRENT_NONSTOCK = "XFCYNSHSTK";

    //HISTORY
    public static final String HISTORY_DELIVERY_LIST = "XGETSDH";
    public static final String HISTORY_INVOICES_LIST = "XGETSIH";
    public static final String HISTORY_CUSTOMER_LIST = "XBPC1";

    //Collector
//    public static final String COLLECTOR_MUL_PAY                                     = "Y10CMULPAY";
    public static final String COLLECTOR_INVOICE_LIST = "Y10CBPCDUD";

    //Executive
    public static final String DELIVERY_EXECUTIVE_LIST = "XSDHEXEC";
    public static final String VALIDATION_DELIVERY_EXECUTIVE = "XSDHEXECVA";
    public static final String EXECUTIVE_ACTIVE_DELIVERY = "YSDHDELXDS";

    //Decanting
    public static final String DECANTING_LIST = "Y10CDMSEL";
    public static final String DECANTING_DETAILS = "Y10CDMDET";
    public static final String DECANTING_PRODUCT_SELECTION = "Y10CITMSEL";
    public static final String DECANTING_CREATION = "Y10CDMMISC";
    public static final String DECANTING_ADDRESS_LIST = "Y10CBADRSL";
    public static final String DECANTING_SPOT_CREATION = "YSPTSMRCDM";
    public static final String DECANTING_VEHICLE = "Y10CVEHSEL";
    public static final String DECANTING_CHECKIN = "XDECUSRCKI";



    //NOT USING BELOW AS OF NOW / OLD SERVICES
    public static final String CURRENT_VEHICLE_STOCK = "Y10CSTOCK";
    public static final String SIGN_AUTHORIZATION = "XUSRCON";
    public static final String EMPTY_DRIVER_ID = "XX10CDRID";
    public static final String GATE_INSPECTION = "XGATECHKLI";
    public static final String INVOICE_HISTORY = "X10CGETSIH";
    public static final String OLD_STOCK = "XX10CXLVS";
    public static final String IMAGE_CAMERA = "X10CIMAGE";
    public static final String OLD_UPDATE_METER_QTY = "XDELIVMETR";
    public static final String PAYMENT_HISTORY = "X10CGETPAY";
    public static final String PRICE_ITEM_LIST = "XX10CSPL2";
    public static final String PRICE_LIST = "XX10CSPC";
    public static final String PRICE_DETAILS = "XX10CSPLIT";
    public static final String PRODUCT_LIST = "XX10CITM";
    public static final String PRODUCT_DETAILS = "XX10CYITM";
    public static final String RESCHEDULE_SHIPMENT = "X10CDELRES";
    public static final String SIGNATURE = "X10CSIGN";
    public static final String SITE_LIST = "XX10CFCY";
    public static final String SITE_DETAILS = "XX10CYFCY";
    public static final String UN_INVOICED_DELIVERY_LIST = "X10CGETSDH";
    public static final String UNLOAD_STOCK = "X10CSTKCNG";
    public static final String UPDATE_QTY = "XDELIVQTY";
    public static final String VEHICLE_CHECKIN = "XX10CVEHDT";
    public static final String VEHICLE_INSPECTION = "XVEHCHKLIS";
    public static final String VEHICLE_ROUTE_LIST = "XX10CPLC";
    public static final String NON_BG_PRODUCT_LIST = "XNONBGITM";


//IGNORE

//    XUSRCON    -NOT USING
//    XVEHCHKLIS -NOT USING
//    XGATECHKLI -NOT USING
//    X10CSRH    -NOT USINGG
//    XSDR       -NOT USINGG
//    XNONBGITM  -NOT USING
//    XX10CVEHDT -NOT USING
//    XX10CXLVS  -NOT USING
//    XACVSDH    -NOT USING
//    XSIHDETS   -NOT USING
//    X10CIMAGE  -NOT USING
//    X10CSIGN   -NOT USING
//    Y10CSRL1   -NOT USING
//    XDELIVMETR -NOT USING

}
