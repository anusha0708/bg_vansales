package com.tbs.brothersgas.haadhir.Model;

import java.io.Serializable;

public class LoadStockDO implements Serializable {
    public String shipmentNumber            = "";
    public String product            = "";
    public String productDescription = "";
    public String site            = "";
    public String siteDescription = "";
    public String stockUnit          = "";
    public String weightUnit         = "";
    public Double quantity           = 0.0;
    public double productWeight      = 0.0;
    public double productVolume      = 0.0;
    public String totalStockUnit     = "";
    public String totalStockVolume   = "";
    public int itemCount           = 0;
    public int status;
    public int productGroupType           = 0;
    public String productUnit          = "";
    public Double price           = 0.0;

    public double percentage           = 0.0;

    public String other = "";


}
