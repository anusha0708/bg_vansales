package com.tbs.brothersgas.haadhir.fragments;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tbs.brothersgas.haadhir.Activitys.BaseActivity;
import com.tbs.brothersgas.haadhir.Adapters.VehicleRouteAdapter;
import com.tbs.brothersgas.haadhir.Model.PickUpDo;
import com.tbs.brothersgas.haadhir.R;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;

import java.util.ArrayList;

/**
 * Created by sandy on 2/15/2018.
 */

public class RouteListFragmment extends Fragment  {
    private PreferenceUtils preferenceUtils;
    private RecyclerView recycleview;
    private TextView tvNoOrders;
    private ArrayList<PickUpDo> pickUpDos;
    private TextView tvName,tvSiteId;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        savedInstanceState = getArguments();
        pickUpDos = (ArrayList<PickUpDo>) savedInstanceState.getSerializable("RoutList");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.vehicle_screen, container, false);

        recycleview     = (RecyclerView)rootView. findViewById(R.id.recycleview);
        tvNoOrders      = (TextView) rootView.findViewById(R.id.tvNoOrders);
        tvName          = (TextView) rootView.findViewById(R.id.tvName);
        tvSiteId          = (TextView) rootView.findViewById(R.id.tvSiteId);

        recycleview.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        preferenceUtils = ((BaseActivity)getContext()).preferenceUtils;
        tvName.setText(preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_NAME, ""));
        tvSiteId.setText(preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID, ""));

        if(pickUpDos!=null && pickUpDos.size()>0){
               recycleview.setVisibility(View.VISIBLE);
               tvNoOrders.setVisibility(View.GONE);
            ((BaseActivity)getContext()).hideLoader();
               VehicleRouteAdapter vehicleRouteAdapter = new VehicleRouteAdapter(getActivity(), pickUpDos);
               recycleview.setAdapter(vehicleRouteAdapter);
               vehicleRouteAdapter.refreshAdapter(pickUpDos);

           }else {
               recycleview.setVisibility(View.GONE);
               tvNoOrders.setVisibility(View.VISIBLE);
           }

        return rootView;
    }
}