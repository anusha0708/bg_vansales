package com.tbs.brothersgas.haadhir.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.tbs.brothersgas.haadhir.Activitys.OnAccountCreatePaymentActivity;
import com.tbs.brothersgas.haadhir.Model.BankDO;
import com.tbs.brothersgas.haadhir.Model.FinantialSiteDO;
import com.tbs.brothersgas.haadhir.R;
import com.tbs.brothersgas.haadhir.collector.CreatePaymentActivity;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;

import java.util.ArrayList;

public class FinantialSiteListAdapter extends RecyclerView.Adapter<FinantialSiteListAdapter.MyViewHolder>  {

    private ArrayList<FinantialSiteDO> finantialSiteDOS;
    private Context context;
    private String from;





    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName;
        private LinearLayout llDetails;

        public MyViewHolder(View view) {
            super(view);
            tvName = (TextView) view.findViewById(R.id.tvName);



        }
    }


    public FinantialSiteListAdapter(String From,Context context, ArrayList<FinantialSiteDO> bankDoS, int typE) {
        this.context = context;
        this.finantialSiteDOS = bankDoS;
        this.from    = From;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.simple_text, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final FinantialSiteDO finantialSiteDO = finantialSiteDOS.get(position);

        holder.tvName.setText(finantialSiteDO.site+" - "+finantialSiteDO.siteDescription);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = holder.getAdapterPosition();
                PreferenceUtils preferenceUtils = new PreferenceUtils(context);
                preferenceUtils.saveString(PreferenceUtils.FINANTIAL_SITE_ID, String.valueOf(finantialSiteDOS.get(pos).site));
               if(from.isEmpty()){
                   ((OnAccountCreatePaymentActivity)context).tvSiteSelection.setText(""+finantialSiteDOS.get(pos).siteDescription);
                   ((OnAccountCreatePaymentActivity)context).siteDialog.dismiss();

               }else {
                   ((CreatePaymentActivity)context).tvSiteSelection.setText(""+finantialSiteDOS.get(pos).siteDescription);
                   ((CreatePaymentActivity)context).siteDialog.dismiss();

               }

            }
        });


    }

    @Override
    public int getItemCount() {
        return finantialSiteDOS.size();
    }

}
