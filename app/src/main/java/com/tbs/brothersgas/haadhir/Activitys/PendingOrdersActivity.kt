package com.tbs.brothersgas.haadhir.Activitys

import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.brothersgas.haadhir.Adapters.LoadStockAdapter
import com.tbs.brothersgas.haadhir.Adapters.LoadVanSaleStockPagerAdapter
import com.tbs.brothersgas.haadhir.Adapters.PendingOrdersAdapter
import com.tbs.brothersgas.haadhir.Adapters.ScheduledProductsAdapter
import com.tbs.brothersgas.haadhir.Model.LoadStockDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.*
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import java.util.ArrayList

//
class PendingOrdersActivity : BaseActivity() {
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView

    var percentage: Double = 0.0
    lateinit var tvNoOrders: TextView
    override protected fun onResume() {
        loadPendingData()

        super.onResume()
    }

    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.pending_orders, null) as ScrollView
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()
        disableMenuWithBackButton()
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            setResult(14, null)
            finish()
        }
    }

    override fun initializeControls() {
        tvScreenTitle.setText("Pending Orders")
        tvNoOrders = findViewById<View>(R.id.tvNoOrders) as TextView

        recycleview = findViewById<View>(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        recycleview.setLayoutManager(androidx.recyclerview.widget.LinearLayoutManager(this))

    }

    private fun loadPendingData() {
        if (Util.isNetworkAvailable(this)) {
            val loadVanSaleRequest = PendingOrdersListRequest(this@PendingOrdersActivity)
            showLoader()
            loadVanSaleRequest.setOnResultListener { isError, loadStockMainDo ->
                hideLoader()
                if (isError) {
                    showAppCompatAlert("", "Error in Service Response, Please try again", "Ok", "Cancel", "", false)

                } else {
                    hideLoader()

                    if(loadStockMainDo.pendingOrderDOS.size>0){
                        var loadStockAdapter = PendingOrdersAdapter(this@PendingOrdersActivity, loadStockMainDo.pendingOrderDOS, "Shipments")
                        recycleview.setAdapter(loadStockAdapter)
                        recycleview.visibility=View.VISIBLE
                        tvNoOrders.visibility=View.GONE


                    }else{
                        tvNoOrders.visibility=View.VISIBLE
                        recycleview.visibility=View.GONE
                    }

                }
            }
            loadVanSaleRequest.execute()


        } else {
            showToast("No Internet connection, please try again later")
        }
    }
}