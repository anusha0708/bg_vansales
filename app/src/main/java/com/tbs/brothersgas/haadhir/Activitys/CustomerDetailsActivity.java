package com.tbs.brothersgas.haadhir.Activitys;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryMainDO;
import com.tbs.brothersgas.haadhir.Model.CustomerDetailsDo;
import com.tbs.brothersgas.haadhir.Model.CustomerDetailsMainDo;
import com.tbs.brothersgas.haadhir.Model.CustomerDo;
import com.tbs.brothersgas.haadhir.Model.SuccessDO;
import com.tbs.brothersgas.haadhir.R;
import com.tbs.brothersgas.haadhir.Requests.CustomerDetailsRequest;
import com.tbs.brothersgas.haadhir.Requests.CustomersLocationRequest;
import com.tbs.brothersgas.haadhir.database.StorageManager;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;
import com.tbs.brothersgas.haadhir.utils.Util;

import java.util.ArrayList;

public class CustomerDetailsActivity extends BaseActivity implements AdapterView.OnItemClickListener, LocationListener {
    private ScrollView llOrderDetails;
    private TextView tvName;
    private TextView tvDescription;

    ArrayList<CustomerDetailsDo> customerDos;
    private TextView currency;
    private TextView rule;
    private TextView term;
    private TextView category;
    private TextView address;
    private TextView bill;
    private TextView payCustmer;
    private TextView code;
    private TextView payByAddress;
    private TextView billAddress;
    private LocationManager locationManager;
    private Double lat;
    private Double lng;
    PreferenceUtils preferenceUtils;
    String orderCode;
    ListView listView;
    CustomAdapter customAdapter;

    private void captureCustomerLocation() {
         locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        //To request location updates
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1, 1, this);
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            lat = location.getLatitude();
            lng = location.getLongitude();

            String shipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, "");
            if (shipmentType.equals(getResources().getString(R.string.checkin_scheduled))) {
                ActiveDeliveryMainDO activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this);
                captureCustomerLocationRequest(activeDeliverySavedDo.customer, lat, lng, this);

            } else {
                CustomerDo customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this);
                captureCustomerLocationRequest(customerDo.customerId, lat, lng, this);
            }
            locationManager.removeUpdates(this);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void initialize() {
        llOrderDetails = (ScrollView) getLayoutInflater().inflate(R.layout.customer_details, null);
        llBody.addView(llOrderDetails, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        initializeControls();
        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        // llshoppingCartLayout.setVisibility(View.GONE);

        if (getIntent().hasExtra("CODE")) {
            orderCode = getIntent().getExtras().getString("CODE");
        }
        listView = (ListView) findViewById(R.id.lvItems);

        ivCustomerLocation.setVisibility(View.VISIBLE);
        ivCustomerLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
                    mDrawerLayout.closeDrawer(Gravity.LEFT);
                } else {
                    PopupMenu popup = new PopupMenu(getApplicationContext(), ivCustomerLocation);
                    popup.getMenuInflater().inflate(R.menu.location_menu, popup.getMenu());
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            if (item.getTitle().equals("Customer Location")) {
                             //   captureCustomerLocation();
                                Intent intent = new Intent(CustomerDetailsActivity.this,CustomerLocActivty.class);
                                intent.putExtra("CODE",orderCode);
                                startActivityForResult(intent,11);
                            }
                            return true;
                        }


                    });
                    popup.show();
                }
            }
        });


    }
    private void getData(){

        preferenceUtils = new PreferenceUtils(CustomerDetailsActivity.this);
        CustomerDetailsRequest driverListRequest = new CustomerDetailsRequest(orderCode, CustomerDetailsActivity.this);
        driverListRequest.setOnResultListener(new CustomerDetailsRequest.OnResultListener() {
            @Override
            public void onCompleted(boolean isError, CustomerDetailsMainDo customerDetailsMainDo) {
                hideLoader();
                if (isError) {
                    Toast.makeText(CustomerDetailsActivity.this, R.string.error_details_list, Toast.LENGTH_SHORT).show();
                } else {
                    customerDos = customerDetailsMainDo.customerDetailsDos;

                    preferenceUtils.saveString(PreferenceUtils.C_LAT, customerDetailsMainDo.lattitude);
                    preferenceUtils.saveString(PreferenceUtils.C_LONG, customerDetailsMainDo.longitude);

//                    preferenceUtils.getStringFromPreference(PreferenceUtils.PRODUCT_CODE, "");
//                    tvName.setText("" + customerDetailsMainDo.name);
//                    tvDescription.setText("" + customerDetailsMainDo.descrption);
                    currency.setText("Category                 :  " + customerDetailsMainDo.name +"\n"+"\n"
                            +"Description            :  " + customerDetailsMainDo.descrption+"\n"+"\n"
                            +"Currency                 :  " + customerDetailsMainDo.currency +"\n"+"\n"
                            +"Tax Rule                  :  " + customerDetailsMainDo.rule+"\n"+"\n"
                            +"Payment Term      :  " + customerDetailsMainDo.term+"\n" +"\n"
                            +"Bill to Customer   :  " + customerDetailsMainDo.billAddress+"\n"+"\n"
                            +"Pay-by Customer  :  " + customerDetailsMainDo.pay+"\n"+"\n"
                            +"Business line          :  " + customerDetailsMainDo.buisinessLine+"\n"


                    );
//
                    customAdapter = new CustomAdapter(CustomerDetailsActivity.this, customerDos);

                    // Set a adapter object to the listview
                    listView.setAdapter(customAdapter);

                }

            }
        });
        listView.setOnItemClickListener(this);

        driverListRequest.execute();
    }

    class CustomAdapter extends BaseAdapter {
        public Context context;
        ArrayList<CustomerDetailsDo> listOrderHistoryDetailsDO;


        public CustomAdapter(Context context, ArrayList<CustomerDetailsDo> customerDetailsDos) {

            this.context = context;
            this.listOrderHistoryDetailsDO = customerDetailsDos;
        }

        @Override
        public int getCount() {
            // return the all apps count
            if (customerDos.size() > 0)
                return customerDos.size();
            else
                return 0;
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Inflating installedapps_customlayout
            View view = getLayoutInflater().inflate(R.layout.details_list, null);

            TextView addressLine = (TextView) view.findViewById(R.id.addressLine);


            addressLine.setText("Description                :  " + customerDos.get(position).addressDescription
                        +"\n\n"+"Address                     :  " + customerDos.get(position).addressLine
                        +"\n\n"+"Postal code              :  " + customerDos.get(position).postalCode
                       +"\n\n"+ "City                             :  " + customerDos.get(position).city +
                        "\n\n"+ "Telephone                 :  " + customerDos.get(position).telephone +
                       "\n\n" + "Mobile                       :  " + customerDos.get(position).mobile
                      + "\n\n" + "Emirates                    :  " + customerDos.get(position).emirates
            );

//            //   packageNameTextView.setText(customerDos.get(position));
//            +"Currency            : " + customerDetailsMainDo.currency +"\n"+"\n"+"Tax Rule        : " + customerDetailsMainDo.rule+"\n"+"\n"
//                    +"Payment Term     : " + customerDetailsMainDo.term+"\n" +"\n"+"Bill to Customer  : " + customerDetailsMainDo.billAddress+"\n"+"\n"
//                    +"Pay-by Customer : " + customerDetailsMainDo.pay
//            addressDescription.setText("Description  : " + customerDos.get(position).addressDescription);
//            postalCode.setText("Postal code : " + customerDos.get(position).postalCode);
//            city.setText("City  :" + customerDos.get(position).city);
//            telephone.setText("Telephone  :  " + customerDos.get(position).telephone);
//            mobile.setText("Mobile  : " + customerDos.get(position).mobile);

            return view;
        }

    }

    @Override
    public void initializeControls() {
        tvScreenTitle.setText("Customer Details");

        currency = (TextView) findViewById(R.id.currency);
//        tvName = (TextView) findViewById(R.id.tvItemName);
//        tvDescription = (TextView) findViewById(R.id.tvItemDescription);
//        rule = (TextView) findViewById(R.id.rule);
//        term = (TextView) findViewById(R.id.term);
//        address = (TextView) findViewById(R.id.address);
//        bill = (TextView) findViewById(R.id.bill);
//        payCustmer = (TextView) findViewById(R.id.payCustmer);
//        code = (TextView) findViewById(R.id.code);
//        billAddress = (TextView) findViewById(R.id.billAddress);
//
//        payByAddress = (TextView) findViewById(R.id.payByAddress);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    private void captureCustomerLocationRequest(String id, Double lat, Double lng, Context context) {
        if (Util.isNetworkAvailable(this)) {

            CustomersLocationRequest driverListRequest = new CustomersLocationRequest(id, lat, lng, context);
            driverListRequest.setOnResultListener(new CustomersLocationRequest.OnResultListener() {
                @Override
                public void onCompleted(boolean isError, SuccessDO customerDetailsMainDo) {
                    if (isError) {
                        Toast.makeText(CustomerDetailsActivity.this, R.string.error_details_list, Toast.LENGTH_SHORT).show();
                    } else {
                        hideLoader();
                        if (customerDetailsMainDo != null) {
                            if (isError) {
                                showToast("failure");
                            } else {
                                if (customerDetailsMainDo.flag == 2) {
                                    showToast("Location Updated");

                                } else {
                                    showToast("customerDetailsMainDo");

                                }

                            }
                        }

                    }
                }

            });
            driverListRequest.execute();

        } else {
            showAppCompatAlert("Alert!", getResources().getString(R.string.internet_connection), "OK", "", "",false);

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == 11 && resultCode == 11) {
           finish();
        }else {

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Util.isNetworkAvailable(this)) {
            getData();

        } else {
            showAppCompatAlert("Alert!", getResources().getString(R.string.internet_connection), "OK", "", "FAILURE", false);

        }

    }


}
