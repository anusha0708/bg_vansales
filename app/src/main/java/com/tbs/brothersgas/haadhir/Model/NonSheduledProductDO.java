package com.tbs.brothersgas.haadhir.Model;

import java.io.Serializable;

public class NonSheduledProductDO implements Serializable {

    public String item            = "";
    public String itemDescription = "";
    public int quantity           = 0;
    public double productWeight           = 0;


}
