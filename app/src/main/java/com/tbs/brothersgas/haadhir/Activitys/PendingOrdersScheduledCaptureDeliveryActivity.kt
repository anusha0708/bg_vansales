package com.tbs.brothersgas.haadhir.Activitys

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.View
import android.view.View.*
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import com.tbs.brothersgas.haadhir.Adapters.*
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryDO
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryMainDO
import com.tbs.brothersgas.haadhir.Model.CustomerDo
import com.tbs.brothersgas.haadhir.Model.ReasonMainDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.*
import com.tbs.brothersgas.haadhir.common.AppConstants
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.pdfs.BulkDeliveryNotePDF
import com.tbs.brothersgas.haadhir.pdfs.CYLDeliveryNotePDF
import com.tbs.brothersgas.haadhir.utils.CalendarUtils
import com.tbs.brothersgas.haadhir.utils.LogUtils
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import java.util.*


//
class PendingOrdersScheduledCaptureDeliveryActivity : BaseActivity() {


    lateinit var reasonMainDo: ReasonMainDO
    private var shipmentProductsList: ArrayList<ActiveDeliveryDO>? = ArrayList()
    private var cloneShipmentProductsList: ArrayList<ActiveDeliveryDO> = ArrayList()
    private var type: Int = 1


    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var btnConfirm: Button
    lateinit var tvNoDataFound: TextView
    lateinit var tvShipmentId: TextView
    lateinit var tvPaymentTerm: TextView

    lateinit var cbNonBGSelected: CheckBox
    var status = "";
    var customerId = "";
    var foc = "";

    lateinit var rbNormal: RadioButton
    lateinit var rbLoan: RadioButton
    lateinit var rgType: RadioGroup
    lateinit var dialog: Dialog
    private var shipmentId: String = ""
    private var loadStockAdapter: PendingOrderScheduledProductsAdapter? = null
    private var reasonMainDO: ReasonMainDO = ReasonMainDO()
    var nonBgType = 0

    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.pending_scheduled_capture_delivery, null) as LinearLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        disableMenuWithBackButton()
        toolbar.setNavigationIcon(R.drawable.back)
        hideKeyBoard(llBody)
        toolbar.setNavigationOnClickListener {

            if (isSentForApproval) {
                showToast("Please process the order..!")
            } else {

                var intent = Intent()
                var args = Bundle();
                args.putSerializable("List", shipmentProductsList)
                intent.putExtra("BUNDLE", args);
                setResult(56, intent)
                finish()
            }
        }

        initializeControls()


        if (intent.hasExtra("shipmentId")) {
            shipmentId = intent.extras.getString("shipmentId")
        }
        if(shipmentId.isNotEmpty()){
            tvShipmentId.visibility=View.VISIBLE
            tvShipmentId.setText("Order number - "+shipmentId)
        }
        loadScheduleData()

        cbNonBGSelected?.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                nonBgType = 1
            } else {
                nonBgType = 0
            }
        }

        btnConfirm.setOnClickListener {
            createDelivery()
        }
    }

    private fun loadScheduleData() {

        if (Util.isNetworkAvailable(this)) {
            if (shipmentId.length > 0) {
                val driverListRequest = PendingOrdersActiveDeliveryRequest(shipmentId, this)
                driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                    hideLoader()
                    if (isError) {
                        showAppCompatAlert("Info!", "Error in Service Response, Please try again...", "Ok", "", "", false)
                    } else {
                        shipmentProductsList = activeDeliveryDo.activeDeliveryDOS;
                        if (shipmentProductsList != null && shipmentProductsList!!.size > 0) {
                            cloneShipmentProductsList.addAll(shipmentProductsList!!)
                            loadStockAdapter = PendingOrderScheduledProductsAdapter(this, shipmentProductsList, "", "", type)
                            recycleview.adapter = loadStockAdapter
                            recycleview.visibility = VISIBLE
                            tvNoDataFound.visibility = GONE
                            btnConfirm.setVisibility(View.VISIBLE)

                        } else {

                            btnConfirm.setVisibility(View.GONE)
                            recycleview.visibility = GONE
                            tvNoDataFound.visibility = VISIBLE
                            btnConfirm.isEnabled = false
                            btnConfirm.isClickable = false
                            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        }
                    }
                }
                driverListRequest.execute()


            }
        }


//        }
    }


    private fun createDelivery() {


        if (Util.isNetworkAvailable(this)) {

            var createDeliveryRequest = PendingOrdersCreateDeliveryRequest(shipmentId,type, shipmentProductsList, this)

            createDeliveryRequest.setOnResultListener { isError, deliveryMainDo, message ->
                hideLoader()
                var createDeliveryMAinDo = deliveryMainDo
                if (isError) {

                    hideLoader()

                    isSentForApproval = false
                    showAppCompatAlert("Error", "Delivery Not Created  due to " + message, "Ok", "", "SUCCESS", false)
                } else {
                    if (createDeliveryMAinDo.status == 20) {
                        if (createDeliveryMAinDo.deliveryNumber.length > 0) {
                            preferenceUtils.saveString(PreferenceUtils.PENDING_DELIVERY_NUMBER, createDeliveryMAinDo.deliveryNumber)
                            LogUtils.debug("PENDING_DELIVERY_NUMBER",createDeliveryMAinDo.deliveryNumber)
                            if (createDeliveryMAinDo.message.length > 0) {
                                showToast("" + createDeliveryMAinDo.message)
                                updateTheProductsInLocal()
                                if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
                                    val intent = Intent(this@PendingOrdersScheduledCaptureDeliveryActivity, PendingSignatureActivity::class.java);
                                    startActivityForResult(intent, 11)
                                } else {
                                    if (nonBgType == 1) {
                                        val intent = Intent(this@PendingOrdersScheduledCaptureDeliveryActivity, NonBGActivity::class.java);
                                        startActivityForResult(intent, 13)
                                    } else {
                                        val intent = Intent(this@PendingOrdersScheduledCaptureDeliveryActivity, PendingSignatureActivity::class.java);
                                        startActivityForResult(intent, 11)
                                    }
                                }


                            }


//                        updateTheProductsInLocal()

                        } else {
                            if (createDeliveryMAinDo.message.length > 0) {
                                showAppCompatAlert("Error", "" + createDeliveryMAinDo.message, "Ok", "", "SUCCESS", false)

                            } else {
                                showAppCompatAlert("Error", "Delivery Not Created ", "Ok", "", "SUCCESS", false)

                            }
                            isSentForApproval = false

                        }

//
//                          val driverAdapter = CreateDeliveryProductAdapter(this, deliveryMainDo.nonSheduledProductDOS)
//                          recycleview.setAdapter(driverAdapter)
                    } else {
                        hideLoader()
                        if (createDeliveryMAinDo.message.length > 0) {
                            showAppCompatAlert("Error", "" + createDeliveryMAinDo.message, "Ok", "", "SUCCESS", false)

                        } else {
                            showAppCompatAlert("Error", "Delivery Not Created", "Ok", "", "SUCCESS", false)

                        }
                        isSentForApproval = false

                    }
                }
            }
            createDeliveryRequest.execute()
        } else {
            showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

        }

    }




    private fun updateTheProductsInLocal() {

            if (loadStockAdapter != null && loadStockAdapter!!.activeDeliveryDOS != null && loadStockAdapter!!.activeDeliveryDOS.size > 0) {
                val deliveryDos = loadStockAdapter!!.activeDeliveryDOS
                StorageManager.getInstance(this@PendingOrdersScheduledCaptureDeliveryActivity).saveCurrentDeliveryItems(this, deliveryDos)

                val intent = Intent()
                intent.putExtra("DeliveredProducts", loadStockAdapter!!.activeDeliveryDOS)
                setResult(1, intent)
                finish()
//
            } else {
                showToast("There are no products to confirm")
            }

    }




    lateinit var loadStockDO: ActiveDeliveryDO;
    override fun onButtonYesClick(from: String) {
        super.onButtonYesClick(from)

    }





    override fun initializeControls() {
        recycleview = findViewById<View>(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        btnConfirm = findViewById<View>(R.id.btnConfirm) as Button
        tvNoDataFound = findViewById<View>(R.id.tvNoDataFound) as TextView
        tvShipmentId = findViewById<View>(R.id.tvShipmentId) as TextView
        cbNonBGSelected = findViewById<View>(R.id.cbNonBGSelected) as CheckBox
        recycleview.setLayoutManager(androidx.recyclerview.widget.LinearLayoutManager(this, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false))
         tvScreenTitle.setText("Confirm Products")
    }


    private var isSentForApproval: Boolean = false


    override fun onBackPressed() {

        if (isSentForApproval) {
            showToast("Please process the order..!")
        } else {
            LogUtils.debug("PAUSE", "tes")
            var intent = Intent()
            intent.putExtra("List", shipmentProductsList)
            setResult(56, intent)
            finish()
            super.onBackPressed()
        }

    }
    override fun onDestroy() {
        super.onDestroy()
        hideLoader()
    }





}