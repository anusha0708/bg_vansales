package com.tbs.brothersgas.haadhir.Activitys

import android.app.Dialog
import android.content.Intent
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import com.tbs.brothersgas.haadhir.Adapters.CreatePaymentAdapter
import com.tbs.brothersgas.haadhir.Adapters.InvoiceAdapter
import com.tbs.brothersgas.haadhir.Adapters.PendingCreatePaymentAdapter
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryDO
import com.tbs.brothersgas.haadhir.Model.LoadStockDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.AllUnPaidInvoicesListRequest
import com.tbs.brothersgas.haadhir.Requests.InvoiceHistoryRequest
import com.tbs.brothersgas.haadhir.Requests.UnPaidInvoicesListRequest
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import java.util.ArrayList


class PendingInvoiceListActivity : BaseActivity() {
    lateinit var invoiceAdapter: InvoiceAdapter
    lateinit var loadStockDOs: ArrayList<LoadStockDO>
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var tvNoDataFound: TextView
    lateinit var view: LinearLayout
    var fromId = 0
    lateinit var tvNoData: TextView
    lateinit var tvAmount: TextView
    lateinit var tvAmountLabel: TextView

    private lateinit var siteListRequest: UnPaidInvoicesListRequest
    private lateinit var siteListRequest2: AllUnPaidInvoicesListRequest

    override protected fun onResume() {
        super.onResume()
//        selectInvoiceList()
    }

    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.sales_invoice_list, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
    }

    override fun initializeControls() {
        tvScreenTitle.setText("Select Invoice")
        recycleview = findViewById(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        tvNoDataFound = findViewById(R.id.tvNoDataFound) as TextView
        view = findViewById(R.id.view) as LinearLayout

        if (intent.hasExtra("Sales")) {
            fromId = intent.extras.getInt("Sales")
        }
        recycleview.setLayoutManager(linearLayoutManager)
        tvNoData = findViewById(R.id.tvNoDataFound) as TextView
        tvAmount = findViewById(R.id.tvAmount) as TextView
        tvAmountLabel = findViewById(R.id.tvAmountLabel) as TextView


        recycleview.setLayoutManager(androidx.recyclerview.widget.LinearLayoutManager(this));
        ivViewAll.visibility = View.VISIBLE
        ivViewAll.setOnClickListener {
            selectALLInvoiceList()
        }

        selectInvoiceList()

        val btnCreate = findViewById(R.id.btnCreate) as Button

        btnCreate.setOnClickListener {
            val intent = Intent(this@PendingInvoiceListActivity, OnAccountCreatePaymentActivity::class.java);
            intent.putExtra("Sales", 1)
            startActivityForResult(intent, 1)

        }
    }

    private fun selectInvoiceList() {

        val activeDeliverySavedDo = StorageManager.getInstance(this).getORDERActiveDeliveryMainDo(this)

        if (Util.isNetworkAvailable(this)) {

                siteListRequest = UnPaidInvoicesListRequest(activeDeliverySavedDo.customer, this@PendingInvoiceListActivity)

            siteListRequest.setOnResultListener { isError, unPaidInvoiceMainDO ->
                hideLoader()
                if (unPaidInvoiceMainDO != null && unPaidInvoiceMainDO.unPaidInvoiceDOS.size > 0) {

//                    tvInvoiceId.setVisibility(View.VISIBLE)
                    tvAmount.setVisibility(View.VISIBLE)
                    tvAmount.setText("" + String.format("%.2f", unPaidInvoiceMainDO.totalAmount) + " " + unPaidInvoiceMainDO.unPaidInvoiceDOS.get(0).currency)

                    preferenceUtils.saveString(PreferenceUtils.TOTAL_AMOUNT, String.format("%.2f", unPaidInvoiceMainDO.totalAmount))
                    preferenceUtils.saveInt(PreferenceUtils.PAYMENT_TYPE, unPaidInvoiceMainDO.type)


                    tvNoData.setVisibility(View.GONE)
//                unPaidInvoiceMainDO
                    if (isError) {
                        hideLoader()
                        hideLoader()

                        Toast.makeText(this@PendingInvoiceListActivity, resources.getString(R.string.error_NoData), Toast.LENGTH_SHORT).show()
                    } else {
                        hideLoader()
                        hideLoader()

                        tvAmount.setVisibility(View.VISIBLE)
                        view.setVisibility(View.VISIBLE)

                        var siteAdapter = PendingCreatePaymentAdapter(this@PendingInvoiceListActivity, unPaidInvoiceMainDO.unPaidInvoiceDOS)
                        recycleview.setAdapter(siteAdapter)
                        tvAmount.setText("" + String.format("%.2f", unPaidInvoiceMainDO.totalAmount) + " " + unPaidInvoiceMainDO.unPaidInvoiceDOS.get(0).currency)
                        preferenceUtils.saveString(PreferenceUtils.TOTAL_AMOUNT, "" + String.format("%.2f", unPaidInvoiceMainDO.totalAmount))

//                    var siteAdapter = CreatePaymentAdapter(this@CreatePaymentActivity, createPaymentDo)
//                    recyclerView.setAdapter(siteAdapter)f

                    }
                } else {
//                tvInvoiceId.setVisibility(View.VISIBLE)
                    tvAmount.setVisibility(View.GONE)
                    tvAmountLabel.setVisibility(View.GONE)

                    tvNoData.setVisibility(View.VISIBLE)
                    recycleview.setVisibility(View.GONE)
                    view.setVisibility(View.VISIBLE)

                    hideLoader()

                }

            }

            siteListRequest.execute()
        } else {
            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "", false)

        }


    }

    private fun selectALLInvoiceList() {

        val activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)
        val customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)

        if (Util.isNetworkAvailable(this)) {

            val ShipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, getResources().getString(R.string.checkin_non_scheduled))
            if (ShipmentType == getResources().getString(R.string.checkin_non_scheduled)) {

                siteListRequest2 = AllUnPaidInvoicesListRequest(customerDo.customerId, this@PendingInvoiceListActivity)
            } else {
                siteListRequest2 = AllUnPaidInvoicesListRequest(activeDeliverySavedDo.customer, this@PendingInvoiceListActivity)
            }
            siteListRequest2.setOnResultListener { isError, unPaidInvoiceMainDO ->
                hideLoader()
                if (unPaidInvoiceMainDO != null && unPaidInvoiceMainDO.unPaidInvoiceDOS.size > 0) {

//                    tvInvoiceId.setVisibility(View.VISIBLE)
                    tvAmount.setVisibility(View.VISIBLE)
                    tvAmount.setText("" + String.format("%.2f", unPaidInvoiceMainDO.totalAmount) + " " + unPaidInvoiceMainDO.unPaidInvoiceDOS.get(0).currency)

                    preferenceUtils.saveString(PreferenceUtils.TOTAL_AMOUNT, String.format("%.2f", unPaidInvoiceMainDO.totalAmount))
                    preferenceUtils.saveInt(PreferenceUtils.PAYMENT_TYPE, unPaidInvoiceMainDO.type)


                    tvNoData.setVisibility(View.GONE)
//                unPaidInvoiceMainDO
                    if (isError) {
                        hideLoader()
                        hideLoader()

                        Toast.makeText(this@PendingInvoiceListActivity, resources.getString(R.string.error_NoData), Toast.LENGTH_SHORT).show()
                    } else {
                        hideLoader()
                        hideLoader()

                        tvAmount.setVisibility(View.VISIBLE)
                        view.setVisibility(View.VISIBLE)
                        recycleview.setVisibility(View.VISIBLE)

                        var siteAdapter = PendingCreatePaymentAdapter(this@PendingInvoiceListActivity, unPaidInvoiceMainDO.unPaidInvoiceDOS)
                        recycleview.setAdapter(siteAdapter)
                        tvAmount.setText("" + String.format("%.2f", unPaidInvoiceMainDO.totalAmount) + " " + unPaidInvoiceMainDO.unPaidInvoiceDOS.get(0).currency)
                        preferenceUtils.saveString(PreferenceUtils.TOTAL_AMOUNT, "" + String.format("%.2f", unPaidInvoiceMainDO.totalAmount))

//                    var siteAdapter = CreatePaymentAdapter(this@CreatePaymentActivity, createPaymentDo)
//                    recyclerView.setAdapter(siteAdapter)f

                    }
                } else {
//                tvInvoiceId.setVisibility(View.VISIBLE)
                    tvAmount.setVisibility(View.GONE)
                    tvAmountLabel.setVisibility(View.GONE)

                    tvNoData.setVisibility(View.VISIBLE)
                    recycleview.setVisibility(View.GONE)
                    view.setVisibility(View.VISIBLE)

                    hideLoader()

                }

            }

            siteListRequest2.execute()
        } else {
            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "", false)

        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == 1) {
            selectInvoiceList()
        }
    }


}