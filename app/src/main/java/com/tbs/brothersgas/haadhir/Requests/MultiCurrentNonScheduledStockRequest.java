package com.tbs.brothersgas.haadhir.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryMainDO;
import com.tbs.brothersgas.haadhir.Model.CustomerDo;
import com.tbs.brothersgas.haadhir.Model.LoadStockDO;
import com.tbs.brothersgas.haadhir.Model.LoadStockMainDO;
import com.tbs.brothersgas.haadhir.R;
import com.tbs.brothersgas.haadhir.database.StorageManager;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;
import com.tbs.brothersgas.haadhir.utils.ProgressTask;
import com.tbs.brothersgas.haadhir.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class MultiCurrentNonScheduledStockRequest extends AsyncTask<String, Void, Boolean> {

    private LoadStockMainDO loadStockMainDO;
    private LoadStockDO loadStockDO;
    private Context mContext;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;
    String routingId;

    public MultiCurrentNonScheduledStockRequest(String routingId, Context mContext) {
        this.mContext = mContext;
        this.routingId = routingId;
    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        public void onCompleted(boolean isError, LoadStockMainDO loadStockMainDO);

    }

    public boolean runRequest() {
        String NAMESPACE = "http://www.adonix.com/WSS";
        String METHOD_NAME = "run";
        String SOAP_ACTION = "CAdxWebServiceXmlCC";
        preferenceUtils = new PreferenceUtils(mContext);
        username = preferenceUtils.getStringFromPreference(PreferenceUtils.A_USER_NAME, "");
        password = preferenceUtils.getStringFromPreference(PreferenceUtils.A_PASSWORD, "");
        ip       = preferenceUtils.getStringFromPreference(PreferenceUtils.IP_ADDRESS, "");
        port     = preferenceUtils.getStringFromPreference(PreferenceUtils.PORT, "");
        pool     = preferenceUtils.getStringFromPreference(PreferenceUtils.ALIAS, "");
        String site     = preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID, "");

        String URL = "http://" + ip + ":" + port + "/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC";

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        // Set all input params
        request.addProperty("publicName", WebServiceConstants.MULTI_OPENING_NON_SHEDULED_STOCK);
        JSONObject jsonObject = new JSONObject();
        try {
            ActiveDeliveryMainDO activeDeliverySavedDo = StorageManager.getInstance(mContext).getActiveDeliveryMainDo(mContext);
            CustomerDo customerDo = StorageManager.getInstance(mContext).getCurrentSpotSalesCustomer(mContext);

           // jsonObject.put("I_XFCY", site);
            jsonObject.put("I_XSCRNUM", routingId);
            String ShipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, mContext.getResources().getString(R.string.checkin_non_scheduled));

            if (ShipmentType.equals(mContext.getResources().getString(R.string.checkin_non_scheduled))) {

                jsonObject.put("I_XBPC", customerDo.customerId);
            } else {
                jsonObject.put("I_XBPC", activeDeliverySavedDo.customer);
            }


        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        request.addProperty("inputXml", jsonObject.toString());

        SoapObject callcontext = new SoapObject("", "callContext");
        // Set all input params
        callcontext.addProperty("codeLang", "ENG");
        callcontext.addProperty("poolAlias", pool);
        callcontext.addProperty("poolId", "");
        callcontext.addProperty("codeUser", username);
        callcontext.addProperty("password", password);
        callcontext.addProperty("requestConfig", "adxwss.trace.on=off");

        request.addSoapObject(callcontext);


        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.setOutputSoapObject(request);

        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
        androidHttpTransport.debug = true;

        try {
            List<HeaderProperty> headerList = new ArrayList<HeaderProperty>();
            headerList.add(new HeaderProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode((username + ":" + password).getBytes())));


            androidHttpTransport.call(SOAP_ACTION, envelope, headerList);


            SoapObject response = (SoapObject) envelope.getResponse();
            String resultXML = (String) response.getProperty("resultXml");
            if (resultXML != null && resultXML.length() > 0) {
                return parseXML(resultXML);
            } else {
                return false;
            }
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            loadStockMainDO = new LoadStockMainDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        loadStockMainDO.loadStockDOS = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        loadStockDO = new LoadStockDO();

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {

                        if (attribute.equalsIgnoreCase("O_XITMREF")) {
                            loadStockDO.product = text;


                        } else if (attribute.equalsIgnoreCase("O_XITMDES")) {
                            loadStockDO.productDescription = text;


                        }
                        else if (attribute.equalsIgnoreCase("O_XFCY")) {
                            loadStockDO.site = text;


                        } else if (attribute.equalsIgnoreCase("O_XFCYDES")) {
                            loadStockDO.siteDescription = text;


                        }
                        else if (attribute.equalsIgnoreCase("O_XSTU")) {
                            loadStockDO.stockUnit = text;

                        } else if (attribute.equalsIgnoreCase("O_XWU")) {
                            if(text.length()>0){

                                loadStockDO.weightUnit = text;
                            }

                        }

                        else if (attribute.equalsIgnoreCase("O_XSAU")) {
                            if(text.length()>0){

                                loadStockDO.productUnit = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YITMGRPTYP")) {
                            if(text.length()>0){

                                loadStockDO.productGroupType = Integer.parseInt(text);
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_ITMLOADPER")) {
                            if(text.length()>0){

                                loadStockDO.percentage = Double.parseDouble(text);
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_XORIQTY")) {
                            loadStockDO.quantity = Double.parseDouble(text);


                        }
                        else if (attribute.equalsIgnoreCase("O_XPRICE")) {
                            loadStockDO.price = Double.parseDouble(text);


                        }
                        text="";

                    }

                    if (endTag.equalsIgnoreCase("GRP")) {
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        loadStockMainDO.loadStockDOS.add(loadStockDO);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();

       // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        ProgressTask.getInstance().closeProgress();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, loadStockMainDO);
        }
    }
}