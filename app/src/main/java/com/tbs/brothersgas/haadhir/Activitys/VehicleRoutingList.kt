package com.tbs.brothersgas.haadhir.Activitys

import android.app.PendingIntent.getActivity
import android.content.Context
import android.content.Intent
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import com.google.android.material.floatingactionbutton.FloatingActionButton
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.*

import com.tbs.brothersgas.haadhir.Adapters.CustomerAdapter
import com.tbs.brothersgas.haadhir.Adapters.VehicleRouteAdapter
import com.tbs.brothersgas.haadhir.Model.CustomerDo
import com.tbs.brothersgas.haadhir.Model.PickUpDo
import com.tbs.brothersgas.haadhir.Model.RouteDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.DisplayPickupListRequest
import com.tbs.brothersgas.haadhir.common.AppConstants
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util

import java.util.ArrayList

class VehicleRoutingList : BaseActivity() {
    private val userId: String? = null
    private val orderCode: String? = null
    private var recycleview: androidx.recyclerview.widget.RecyclerView? = null
    private val orderHistoryAdapter: CustomerAdapter? = null
    private var llOrderHistory: ScrollView? = null
    private lateinit var driverAdapter: VehicleRouteAdapter
    private var tvName: TextView? = null
    private var tvSiteId: TextView? = null
    private var llDetails: LinearLayout? = null
    private var tvNoData: TextView? = null
    private var tvlName: TextView? = null
    private var tvlSiteId: TextView? = null
    private val customerDos: List<RouteDO>? = null
    private var lllDetails: LinearLayout? = null
    private var view: View? = null

    lateinit var pickUpDos: ArrayList<PickUpDo>


    override fun initialize() {
        llOrderHistory = layoutInflater.inflate(R.layout.vehicle_screen, null) as ScrollView
        llBody.addView(llOrderHistory, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
        //  pickUpDos = (this as VehicleRoutingList).getLoadData()
        tvName!!.setText(preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_NAME, ""))
        tvSiteId!!.setText(preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID, ""))
        tvlName!!.setText(preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_NAME, ""))
        tvlSiteId!!.setText(preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID, ""))
        if (Util.isNetworkAvailable(this)) {
            loadData()
        } else {
            //  showAlert("Please Check Your Internet Connection")
            var shipmentListData = StorageManager.getInstance(this).getShipmentListData(this)
            shipmentListData = saveShipmentListData(shipmentListData)
            driverAdapter = VehicleRouteAdapter(this@VehicleRoutingList, shipmentListData)

            recycleview!!.adapter = driverAdapter
        }
        // new CommonBL(OrderHistoryActivity.this, OrderHistoryActivity.this).orderHistory(userId);

    }

    private fun loadData() {
        if (Util.isNetworkAvailable(this)) {

            showLoader()
            val vehicleRoutId = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "")

            if (vehicleRoutId.length > 0) {
                val driverListRequest = DisplayPickupListRequest(this, vehicleRoutId)
                driverListRequest.setOnResultListener { isError, pods ->
                    hideLoader()
                    if (isError) {
                        showToast("Server Error..please try again")
                    } else {
                        pickUpDos = pods
                        if (pickUpDos.size > 0) {

                            pickUpDos = saveShipmentListData(pods)
                            recycleview!!.setVisibility(View.VISIBLE)
                            tvNoData!!.setVisibility(View.GONE)
                            lllDetails!!.setVisibility(View.VISIBLE)
                            view!!.setVisibility(View.VISIBLE)

                            llDetails!!.setVisibility(View.VISIBLE)
                            driverAdapter = VehicleRouteAdapter(this@VehicleRoutingList, pods)

                            recycleview!!.adapter = driverAdapter
                        } else {
                            lllDetails!!.setVisibility(View.GONE)
                            view!!.setVisibility(View.GONE)

                            llDetails!!.setVisibility(View.GONE)
                            recycleview!!.setVisibility(View.GONE)
                            tvNoData!!.setVisibility(View.VISIBLE)
                        }

                    }
                }
                driverListRequest.execute()
            } else {
                showToast("No Shipments found")

            }

        } else {
            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "",false)

        }

    }

    fun getLoadData(): ArrayList<PickUpDo> {

        return pickUpDos;
    }

    private fun saveShipmentListData(pickUpDos: ArrayList<PickUpDo>): ArrayList<PickUpDo> {
        val completedShipmentsList = StorageManager.getInstance(this).getCompletedShipments(this)
        val skipShipmentList = StorageManager.getInstance(this).getSkipShipmentList(this)

        for (i in pickUpDos.indices) {
            for (j in skipShipmentList.indices) {
                if (pickUpDos.get(i).shipmentNumber.equals(skipShipmentList.get(j), true)) {
                    pickUpDos.get(i).status = "Skip";
                    pickUpDos.get(i).skipFlag == 2


                }
            }
            for (j in completedShipmentsList.indices) {
                if (pickUpDos.get(i).shipmentNumber.equals(completedShipmentsList.get(j).shipmentNumber, ignoreCase = true)) {
                    pickUpDos.get(i).status = "Completed";
                }
            }
        }
        StorageManager.getInstance(this).saveShipmentListData(this, pickUpDos)
        return pickUpDos;
    }

    private fun removePreviousShipmentLocaData() {
        StorageManager.getInstance(this).deleteActiveDeliveryMainDo(this)
        StorageManager.getInstance(this).deleteDepartureData(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        hideLoader()

        if (requestCode == 19 && resultCode == 19) {

            if (AppConstants.CapturedReturns) {
                val vehicleRoutId = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "")
                if (Util.isNetworkAvailable(this)) {

                    val driverListRequest = DisplayPickupListRequest(this, vehicleRoutId)
                    showLoader()
                    driverListRequest.setOnResultListener { isError, pods ->
                        hideLoader()
                        if (isError) {
                            showToast("Server Error..please try again")
                        } else {
                            pickUpDos = pods
                            val cmptdShipments = StorageManager.getInstance(this).getCompletedShipments(this)
                            for (i in pickUpDos.indices) {
                                if (pickUpDos.get(i).shipmentNumber.equals(AppConstants.EnableShipments, ignoreCase = true)) {
                                    pickUpDos.get(i).status = "Completed";
                                    cmptdShipments.add(pickUpDos.get(i))
                                    StorageManager.getInstance(this).saveCompletedShipments(this, cmptdShipments)
                                    break
                                }
                            }
                            removePreviousShipmentLocaData()
                            val completedShipmentsList = StorageManager.getInstance(this).getCompletedShipments(this)
                            for (j in completedShipmentsList.indices) {
                                for (i in pickUpDos.indices) {
                                    if (pickUpDos.get(i).shipmentNumber.equals(completedShipmentsList.get(j).shipmentNumber, ignoreCase = true)) {
                                        pickUpDos.get(i).status = "Completed";
                                    }
                                }
                            }
                            driverAdapter = VehicleRouteAdapter(this, pickUpDos)
                            recycleview!!.setAdapter(driverAdapter)
                        }

                    }
                    driverListRequest.execute()
                } else {
                    showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "",false)

                }

                AppConstants.CapturedReturns = false
            }
        } else if (requestCode == 19 && resultCode == 52) {
            if (Util.isNetworkAvailable(this)) {
                loadData()
            } else {
                //  showAlert("Please Check Your Internet Connection")
                var shipmentListData = StorageManager.getInstance(this).getShipmentListData(this)
                shipmentListData = saveShipmentListData(shipmentListData)
                driverAdapter = VehicleRouteAdapter(this@VehicleRoutingList, shipmentListData)

                recycleview!!.adapter = driverAdapter
            }

        }

    }

    override fun initializeControls() {
        var vehicleCode = preferenceUtils!!.getStringFromPreference(PreferenceUtils.VEHICLE_CODE, "")
        tvScreenTitle.maxLines = 2
        tvScreenTitle.setSingleLine(false)
        tvScreenTitle.setText("Transaction List" + "\n" + vehicleCode);
        recycleview = findViewById(R.id.recycleview)
        recycleview!!.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this@VehicleRoutingList)
        tvName = findViewById(R.id.tvName) as TextView
        tvSiteId = findViewById(R.id.tvSiteId) as TextView
        tvlName = findViewById(R.id.tvlName) as TextView
        tvlSiteId = findViewById(R.id.tvlSiteId) as TextView
        llDetails = findViewById(R.id.llDetails) as LinearLayout
        lllDetails = findViewById(R.id.lllDetails) as LinearLayout
        view = findViewById(R.id.view) as View

        lllDetails!!.setOnClickListener {

            val latitude = 25.379465//"28.7041"
            val longitude = 55.460388//"77.1025"
            val gmmIntentUri = "google.navigation:q=" + latitude + "," + longitude + "&mode=d"
            val mapIntent = Intent(Intent.ACTION_VIEW, Uri.parse(gmmIntentUri))
            mapIntent.setPackage("com.google.android.apps.maps")
            try {
                if (mapIntent.resolveActivity(packageManager) != null) {
                    startActivityForResult(mapIntent, 1542)
                    onUserLeaveHint()
                }
            } catch (e: Exception) {
                Log.e("Map Loading", "onClick: NullPointerException: Couldn't open map." + e.message)
                Toast.makeText(this, "Couldn't open map", Toast.LENGTH_SHORT).show()
            }
        }

        tvNoData = findViewById(R.id.tvNoData) as TextView

    }

    override fun onResume() {

        super.onResume()
    }

    override fun onButtonYesClick(from: String) {
        if (from.equals("PLAYSTORE", true)) {
            val appPackageName = packageName // getPackageName() from Context or Activity object
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")))
        }
    }
}
