package com.tbs.brothersgas.haadhir.utils;

import android.provider.DocumentsContract;
import android.util.Log;
import android.util.Xml;

import org.w3c.dom.Document;
import org.xmlpull.v1.XmlPullParser;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class CustomXmlPullParser {


    static final String RESULTXML = "resultXml";

    static final String STATUS = "Status";


    public static String parse(String rssFeed) {
        String value = null;

        XmlPullParser parser = Xml.newPullParser();
        InputStream stream = null;
        try {
            // auto-detect the encoding from the stream
            parser.setInput(new StringReader(rssFeed));
            int eventType = parser.getEventType();
            boolean done = false;
            while (eventType != XmlPullParser.END_DOCUMENT && !done) {
                String name = null;
                String xmlText = null;
                switch (eventType) {
                    case XmlPullParser.START_DOCUMENT:
                        break;//Try now
                    case XmlPullParser.START_TAG:
                        name = parser.getName();
                        if (name.equalsIgnoreCase(RESULTXML)) {
                            value = parser.nextText();
                        }

                        break;

//                    case XmlPullParser.END_TAG:
//                        name = parser.getName();
//                        Log.i("End tag", name);
//                        if (name.equalsIgnoreCase(ITEM) && ITEM != null) {
//                            Log.i("Added",  parser.nextText());
//                           // list.add(item);
//                        }
//                        break;
                }
                eventType = parser.next();
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return value;
    }
}
