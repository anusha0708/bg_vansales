package com.tbs.brothersgas.haadhir.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.tbs.brothersgas.haadhir.Activitys.PendingOrdersPODActivity;
import com.tbs.brothersgas.haadhir.Activitys.PendingOrdersScheduledCaptureDeliveryActivity;
import com.tbs.brothersgas.haadhir.Activitys.ScheduledCaptureDeliveryActivity;
import com.tbs.brothersgas.haadhir.Model.LoadStockDO;
import com.tbs.brothersgas.haadhir.Model.PendingOrderDO;
import com.tbs.brothersgas.haadhir.R;
import com.tbs.brothersgas.haadhir.common.AppConstants;
import com.tbs.brothersgas.haadhir.listeners.LoadStockListener;

import java.util.ArrayList;

public class PendingOrdersAdapter extends RecyclerView.Adapter<PendingOrdersAdapter.MyViewHolder>{
    private ArrayList<PendingOrderDO> loadStockDOS;
    private Context context;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvProductName, tvDescription;

        public MyViewHolder(View view) {
            super(view);
            tvProductName   = (TextView) view.findViewById(R.id.tvName);
            tvDescription   = (TextView) view.findViewById(R.id.tvDescription);
        }
    }

    public void refreshAdapter(ArrayList<PendingOrderDO> loadStockDoS){
        this.loadStockDOS = loadStockDoS;
        notifyDataSetChanged();

    }
    private String from = "";
    public PendingOrdersAdapter(Context mContext, ArrayList<PendingOrderDO> loadStockDoS, String from) {
        this.context = mContext;
        this.loadStockDOS = loadStockDoS;
        this.from = from;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.pending_orders_data, parent, false);
        return new MyViewHolder(itemView);
    }

    private ArrayList<LoadStockDO> selectedLoadStockDOs = new ArrayList<>();
    public ArrayList<LoadStockDO> getSelectedLoadStockDOs(){
        return selectedLoadStockDOs;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final PendingOrderDO loadStockDO = loadStockDOS.get(position);
        if(loadStockDO.orderDate.length()>0){
            String dMonth = loadStockDO.orderDate.substring(4, 6);
            String dyear = loadStockDO.orderDate.substring(0, 4);
            String dDate = loadStockDO.orderDate.substring(Math.max(loadStockDO.orderDate.length() - 2, 0));

            holder.tvProductName.setText(loadStockDO.salesOrderNumber+"\nDate : "+ dDate + "-" + dMonth + "-" + dyear );
        }else {
            holder.tvProductName.setText(loadStockDO.salesOrderNumber+"\nDate : "+loadStockDO.orderDate );

        }


        holder.tvDescription.setText(loadStockDO.customer +" - "+loadStockDO.customerDescription );
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PendingOrdersPODActivity.class);
                intent.putExtra("shipmentId",loadStockDO.salesOrderNumber);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return loadStockDOS!=null && loadStockDOS.size()>0?loadStockDOS.size():0;
    }



}
