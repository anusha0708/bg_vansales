package com.tbs.brothersgas.haadhir.delivery_executive

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import com.tbs.brothersgas.haadhir.Activitys.BaseActivity
import com.tbs.brothersgas.haadhir.Activitys.SignatureActivity
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryDO
import com.tbs.brothersgas.haadhir.Model.CustomerDo
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.ActiveDeliveryRequest
import com.tbs.brothersgas.haadhir.Requests.ExecutiveActiveDeliveryRequest
import com.tbs.brothersgas.haadhir.history.DeliverysAdapter
import com.tbs.brothersgas.haadhir.listeners.ResultListner
import com.tbs.brothersgas.haadhir.pdfs.CYLDeliveryNotePDF
import com.tbs.brothersgas.haadhir.utils.Constants
import com.tbs.brothersgas.haadhir.utils.Util
import kotlinx.android.synthetic.main.activity_delivery_executive_list.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class DeliveryExecutiveListActivity : BaseActivity() {
    private var screenType: Int = 1
    lateinit var userId: String
    lateinit var orderCode: String
    lateinit var llOrderHistory: View
    lateinit var customerDos: ArrayList<CustomerDo>
    private lateinit var btnAddMore: Button
    lateinit var llSearch: LinearLayout
    lateinit var etSearch: EditText
    private var cday: Int = 0
    private var cmonth: Int = 0
    private var cyear: Int = 0
    var startDate = ""
    var endDate = ""
    var to = ""

    companion object {
        var isRefreshed: Boolean = false
    }

    override fun initialize() {
        llOrderHistory = layoutInflater.inflate(R.layout.activity_delivery_executive_list, null)
        llBody.addView(llOrderHistory, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            val intent = Intent()
            setResult(1, intent)
            finish()
        }
        disableMenuWithBackButton()
        initializeControls()
        screenType = intent.getIntExtra(Constants.SCREEN_TYPE, 0)
        if (screenType == 1) {
            tvScreenTitle.setText(getString(R.string.to_do_list))
            ll_dates.visibility = View.GONE
            getList();
        } else if (screenType == 2) {
            tvScreenTitle.setText(getString(R.string.completed_list))
            ll_dates.visibility = View.VISIBLE
            getList();
            setDatePicker();
        } else if (screenType == 3) {
            tvScreenTitle.setText(getString(R.string.activity_list))
            ll_dates.visibility = View.VISIBLE
            setDatePicker();
        }

        btnSearch.setOnClickListener {

            if (!TextUtils.isEmpty(startDate) && !TextUtils.isEmpty(endDate)) {
                val formatter: DateFormat
                formatter = SimpleDateFormat("yyyyMMdd")

//                formatter = SimpleDateFormat("dd-MM-yyyy")
                val startDate = formatter.parse(startDate) as Date
                val endDate = formatter.parse(endDate) as Date
                if (startDate.after(endDate)) {
                    Toast.makeText(this, getString(R.string.select_date), Toast.LENGTH_SHORT).show()

                } else {
                    getList()
                }
            } else {
                if (TextUtils.isEmpty(startDate)) {
                    Toast.makeText(this, getString(R.string.select_start_date), Toast.LENGTH_SHORT).show()
                } else if (!TextUtils.isEmpty(endDate)) {
                    Toast.makeText(this, getString(R.string.select_end_date), Toast.LENGTH_SHORT).show()
                }
            }
        }


    }

    private fun setDatePicker() {

        val c = Calendar.getInstance()
        val cyear = c.get(Calendar.YEAR)
        val cmonth = c.get(Calendar.MONTH)
        val cday = c.get(Calendar.DAY_OF_MONTH)
        tvFrom.setOnClickListener {
            val datePicker = DatePickerDialog(this, datePickerListener, cyear, cmonth, cday)
            datePicker.datePicker.maxDate = Calendar.getInstance().timeInMillis
            datePicker.show()
        }

        tvTO.setOnClickListener {
            val datePicker = DatePickerDialog(this, endDatePickerListener, cyear, cmonth, cday)
            datePicker.datePicker.maxDate = Calendar.getInstance().timeInMillis
            datePicker.show()
        }


    }

    private val datePickerListener: DatePickerDialog.OnDateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        cday = dayOfMonth
        cmonth = monthOfYear + 1
        cyear = year
        var monthString = cmonth.toString()
        if (monthString.length == 1) {
            monthString = "0$monthString"
        } else {
            monthString = cmonth.toString()
        }

        var dayString = cday.toString()
        if (dayString.length == 1) {
            dayString = "0$dayString"
        } else {
            dayString = cday.toString()
        }
        startDate = "" + cyear + monthString + dayString;
        tvFrom.setText("" + dayString + "-" + monthString + "-" + cyear)
//        from = tvFrom.text.toString()

    }
    private val endDatePickerListener: DatePickerDialog.OnDateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        cday = dayOfMonth
        cmonth = monthOfYear + 1
        cyear = year
        var monthString = cmonth.toString()
        if (monthString.length == 1) {
            monthString = "0$monthString"
        } else {
            monthString = cmonth.toString()
        }

        var dayString = cday.toString()
        if (dayString.length == 1) {
            dayString = "0$dayString"
        } else {
            dayString = cday.toString()
        }
        endDate = "" + cyear + monthString + dayString;
        tvTO.setText("" + dayString + "-" + monthString + "-" + cyear)
        to = tvTO.text.toString()

    }

    private fun getList() {

        if (Util.isNetworkAvailable(this)) {
            val driverListRequest = DeliveryExecutiveListRequest(this,
                    screenType,
                    startDate,
                    endDate)
            driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                hideLoader()
                if (isError) {

                    //invoiceList()
                } else {
                    if (activeDeliveryDo != null) {
                        if (activeDeliveryDo.activeDeliveryDOS != null && activeDeliveryDo.activeDeliveryDOS.size > 0) {
                            tvNoOrders.visibility = View.GONE
                            recycleview.visibility = View.VISIBLE
                            var deliverysAdapter = DeliverysAdapter(this, activeDeliveryDo.activeDeliveryDOS)
                            recycleview!!.adapter = deliverysAdapter
                            deliverysAdapter.deliveryExexutive(true, ResultListner { `object`, isSuccess ->

                                if (screenType == 1) {
                                    val intent = Intent(this@DeliveryExecutiveListActivity, DeliveryExecutiveShipmentDetailsActivity::class.java);
                                    intent.putExtra(Constants.SHIPMENT_ID, (`object` as ActiveDeliveryDO).shipmentId)
                                    intent.putExtra(Constants.CUSTOMER_CODE, (`object` as ActiveDeliveryDO).customer)
                                    startActivityForResult(intent, 11)
                                } else {
                                    prepareDeliveryNoteCreation((`object` as ActiveDeliveryDO).shipmentId)
                                }

                            })
                        } else {
                            tvNoOrders.visibility = View.VISIBLE
                            recycleview.visibility = View.GONE
                        }
                    }

                }
            }
            driverListRequest.execute()

        } else {
            showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

        }
    }


    override fun initializeControls() {
    }
/*
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 11 && resultCode == 11) {
            val intent = Intent();
            prepareDeliveryNoteCreation(intent.getStringExtra(Constants.SHIPMENT_ID))
        }
    }

   */

    private fun prepareDeliveryNoteCreation(id: String) {

        if (id.length > 0) {
            val driverListRequest = ExecutiveActiveDeliveryRequest(id, this)
            driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                if (activeDeliveryDo != null) {
                    if (isError) {
                    } else {
                        val note = CYLDeliveryNotePDF(this).createDeliveryPDF(activeDeliveryDo, "HISTORY");
                        note.CreateHistoryPDF().execute()
                        note.isFromDeliverySignatureToFinishActivity(true)
                    }
                } else {
                }
            }
            driverListRequest.execute()
        }
    }

    override fun onResume() {
        super.onResume()
        if (isRefreshed) {
            isRefreshed = false
            if (screenType == 1) {
                tvScreenTitle.setText(getString(R.string.to_do_list))
                ll_dates.visibility = View.GONE
                getList();
            }
        }
    }

}
