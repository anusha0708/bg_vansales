package com.tbs.brothersgas.haadhir.Model;

import java.io.Serializable;

public class ActiveDeliveryDO implements Serializable {

    public String shipmentId = "";
    public String site = "";
    public String date = "";
    public String accountingDate = "";
    public String createdDate = "";

    public String customer = "";
    public String customerDes = "";

    public String address1 = "";
    public String address2 = "";
    public String address3 = "";
    public String address4 = "";


    public String product = "";
    public String productType = "";
    public String pType = "";

    public String shipmentProductType = "";
    public String productDescription = "";
    public Double totalQuantity = 0.0;
    public Double orderedQuantity = 0.0;
    public String openingQuantity = "";
    public String endingQuantity = "";
    public int count = 1;

    public String openingQuantityLong = "";
    public String endingQuantityLong = "";

    public String stockUnit = "";
    public String weightUnit = "";
    public Double quantity = 0.0;
    public String unit = "";
    public String linenumber = "";
    public Double priceTag = 0.00;
    public Double price = 0.00;
    public Double actualQuantity = 0.0;
    public String reasonFOC = "";
    public int focFlag = 0;

    public double percentage = 0.0;


    public int receivedQuantity = 0;//return qty in cyllinder issue pdf
    public boolean isProductAdded;
    public boolean isScheduled;
    public String reason = "";
    public int reasonId = 0;
    public String productUnit = "";
    public String location = "";
    public int line = 0;
    public String locationType="";
    public String issueNumber="";
    public int issueType=0;
    public int stockin=0;

    @Override
    public String toString() {
        return "ActiveDeliveryDO{" +
                "shipmentId='" + shipmentId + '\'' +
                ", product='" + product + '\'' +
                ", productType='" + productType + '\'' +
                ", shipmentProductType='" + shipmentProductType + '\'' +
                ", productDescription='" + productDescription + '\'' +
                ", totalQuantity=" + totalQuantity +
                ", orderedQuantity=" + orderedQuantity +
                ", unit='" + unit + '\'' +
                ", isProductAdded=" + isProductAdded +
                ", isScheduled=" + isScheduled +
                ", price=" + price +
                ", linenumber=" + linenumber +
                ", receivedQuantity=" + receivedQuantity +
                '}';
    }
}
