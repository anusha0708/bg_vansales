package com.tbs.brothersgas.haadhir.history

import android.app.DatePickerDialog
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryDO

import com.tbs.brothersgas.haadhir.Model.UnPaidInvoiceDO
import com.tbs.brothersgas.haadhir.Model.UnPaidInvoiceMainDO
import com.tbs.brothersgas.haadhir.R
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by sandy on 2/15/2018.
 */

class InvoiceList : Fragment() {
    private var invoicesAdapter: InvoicesAdapter? = null
    private var recyclerView: androidx.recyclerview.widget.RecyclerView? = null
    private var unPaidInvoiceDOS: ArrayList<UnPaidInvoiceDO>? = null
    lateinit var tvNoOrders: TextView

    lateinit var tvFrom: TextView
    lateinit var btnSearch: Button
    private var selectedDateDOs = ArrayList<UnPaidInvoiceDO>()
    var startDate = ""
    var endDate = ""
    lateinit var tvTO: TextView
    private var cday: Int = 0
    private var cmonth: Int = 0
    private var cyear: Int = 0
    var to = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val rootView = inflater.inflate(R.layout.list, container, false)

        recyclerView = rootView.findViewById<View>(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        recyclerView!!.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(activity)
        val nonVegBundle = arguments
        if (nonVegBundle != null) {
            unPaidInvoiceDOS = nonVegBundle.getSerializable("InvoiceList") as ArrayList<UnPaidInvoiceDO>

        }
        btnSearch = rootView.findViewById(R.id.btnSearch) as Button
        tvFrom = rootView.findViewById(R.id.tvFrom) as TextView
        tvTO = rootView.findViewById(R.id.tvTO) as TextView
        btnSearch.setText("Search")
        tvNoOrders = rootView.findViewById(R.id.tvNoOrders) as TextView

        btnSearch.setOnClickListener {
            var title = btnSearch.text.toString()
            if (title.contentEquals("Search")) {
                var frm = tvFrom.text.toString().trim();
                var to = tvTO.text.toString().trim()
                if (frm.isNotEmpty() && to.isNotEmpty()) {
//                selectedDateDOs.removeAll(ArrayList<UnPaidInvoiceDO>())
                    selectedDateDOs.clear()
                    val dates = ArrayList<Date>()

//                val frm = "20190401"
//                val to = "20190701"

                    val formatter: DateFormat
                    formatter = SimpleDateFormat("yyyyMMdd")

//                formatter = SimpleDateFormat("dd-MM-yyyy")
                    val startDate = formatter.parse(startDate) as Date
                    val endDate = formatter.parse(endDate) as Date
                    val interval = (24 * 1000 * 60 * 60).toLong() // 1 hour in millis
                    val endTime = endDate.time // create your endtime here, possibly using Calendar or Date
                    var curTime = startDate.time
                    while (curTime <= endTime) {
                        dates.add(Date(curTime))
                        curTime += interval
                    }
                    for (i in dates.indices) {
                        val ds = formatter.format(dates[i])
                        println(" Date is ...$ds")
                    }
                    var isProductExisted = false;

                    for (i in unPaidInvoiceDOS!!.indices) {
                        for (k in dates.indices) {
                            val ds = formatter.format(dates[k])

                            if (unPaidInvoiceDOS!!.get(i).accountingDate.contains("$ds", true)) {
                                isProductExisted = true

                                break
                            }
                        }
                        if (isProductExisted) {
                            isProductExisted = false
                            selectedDateDOs!!.add(unPaidInvoiceDOS!!.get(i))
                            println(selectedDateDOs)

                            continue
                        } else {

                            println(" Date is ...$selectedDateDOs")

                        }
                    }
                    invoicesAdapter = InvoicesAdapter(context!!, selectedDateDOs)
                    recyclerView!!.adapter = invoicesAdapter
                    btnSearch.setText("Clear")

                } else {
                    Toast.makeText(context,"Please select From and To dates", Toast.LENGTH_SHORT)

//                    btnSearch.setText("Search")

                }
            } else {
                tvFrom.setText("")
                tvTO.setText("")

                tvFrom.setHint("From :")
                tvTO.setHint("To : ")
                btnSearch.setText("Search")
                invoicesAdapter = InvoicesAdapter(context!!, unPaidInvoiceDOS)
                recyclerView!!.adapter = invoicesAdapter
            }

        }

        val c = Calendar.getInstance()
        val cyear = c.get(Calendar.YEAR)
        val cmonth = c.get(Calendar.MONTH)
        val cday = c.get(Calendar.DAY_OF_MONTH)
        tvFrom.setOnClickListener {
            val datePicker = DatePickerDialog(context, datePickerListener, cyear, cmonth, cday)

            datePicker.show()
        }

        tvTO.setOnClickListener {
            val datePicker = DatePickerDialog(context, endDatePickerListener, cyear, cmonth, cday)

            datePicker.show()
        }
        if(unPaidInvoiceDOS!!.size>0){

            invoiceAdapter(activity, unPaidInvoiceDOS)
            tvNoOrders.visibility=View.GONE
            recyclerView!!.visibility=View.VISIBLE
        }else{
            tvNoOrders.visibility=View.VISIBLE
            recyclerView!!.visibility=View.GONE
        }


        return rootView
    }

    private fun invoiceAdapter(context: Context?, unPaidInvoiceDos: ArrayList<UnPaidInvoiceDO>?) {
        invoicesAdapter = InvoicesAdapter(context!!, unPaidInvoiceDos)
        recyclerView!!.adapter = invoicesAdapter

    }
    private val datePickerListener: DatePickerDialog.OnDateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        cday = dayOfMonth
        cmonth = monthOfYear + 1
        cyear = year
        var monthString = cmonth.toString()
        if (monthString.length == 1) {
            monthString = "0$monthString"
        } else {
            monthString = cmonth.toString()
        }

        var dayString = cday.toString()
        if (dayString.length == 1) {
            dayString = "0$dayString"
        } else {
            dayString = cday.toString()
        }
        startDate = "" + cyear + monthString + dayString;
        tvFrom.setText("" + dayString + "-" + monthString + "-" + cyear)
//        from = tvFrom.text.toString()

    }
    private val endDatePickerListener: DatePickerDialog.OnDateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        cday = dayOfMonth
        cmonth = monthOfYear + 1
        cyear = year
        var monthString = cmonth.toString()
        if (monthString.length == 1) {
            monthString = "0$monthString"
        } else {
            monthString = cmonth.toString()
        }

        var dayString = cday.toString()
        if (dayString.length == 1) {
            dayString = "0$dayString"
        } else {
            dayString = cday.toString()
        }
        endDate = "" + cyear + monthString + dayString;
        tvTO.setText("" + dayString + "-" + monthString +"-" + cyear)
        to = tvTO.text.toString()

    }


}