//package com.tbs.brothersgas.haadhir.Adapters;
//
///**
// * Created by sandy on 2/7/2018.
// */
//
//import android.app.Dialog;
//import android.content.Context;
//import android.text.Editable;
//import android.text.InputFilter;
//import android.text.Spanned;
//import android.text.TextWatcher;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.Window;
//import android.view.WindowManager;
//import android.widget.AdapterView;
//import android.widget.BaseAdapter;
//import android.widget.Button;
//import android.widget.CheckBox;
//import android.widget.CompoundButton;
//import android.widget.EditText;
//import android.widget.Filter;
//import android.widget.Filterable;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.ListView;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//import androidx.appcompat.app.AppCompatDialog;
//import androidx.appcompat.widget.SwitchCompat;
//import androidx.recyclerview.widget.RecyclerView;
//
//import com.tbs.brothersgas.haadhir.Activitys.BaseActivity;
//import com.tbs.brothersgas.haadhir.Activitys.ScheduledCaptureDeliveryActivity;
//import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryDO;
//import com.tbs.brothersgas.haadhir.Model.ReasonMainDO;
//import com.tbs.brothersgas.haadhir.R;
//import com.tbs.brothersgas.haadhir.common.AppConstants;
//import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;
//
//import java.math.BigDecimal;
//import java.util.ArrayList;
//
//public class MRScheduledProductsAdapter extends RecyclerView.Adapter<MRScheduledProductsAdapter.MyViewHolder> implements Filterable {
//    public ArrayList<ActiveDeliveryDO> activeDeliveryDOS;
//    ValueFilter valueFilter;
//    private Context context;
//    int type = 0;
//    String unit = "";
//    public boolean isClickable = true;
//    Dialog dialog;
//    ReasonMainDO reasonMAINDO;
//
//    @Override
//    public Filter getFilter() {
//        if (valueFilter == null) {
//            valueFilter = new ValueFilter();
//        }
//        return valueFilter;
//    }
//
//    private class ValueFilter extends Filter {
//        @Override
//        protected FilterResults performFiltering(CharSequence constraint) {
//            FilterResults results = new FilterResults();
//
//            if (constraint != null && constraint.length() > 0) {
//                ArrayList<ActiveDeliveryDO> filterList = new ArrayList<>();
//                for (int i = 0; i < activeDeliveryDOS.size(); i++) {
//                    if ((activeDeliveryDOS.get(i).product.toUpperCase()).contains(constraint.toString().toUpperCase())) {
//                        filterList.add(activeDeliveryDOS.get(i));
//                    }
//                }
//                results.count = filterList.size();
//                results.values = filterList;
//            } else {
//                results.count = activeDeliveryDOS.size();
//                results.values = activeDeliveryDOS;
//            }
//            return results;
//
//        }
//
//        @Override
//        protected void publishResults(CharSequence constraint, FilterResults results) {
//            activeDeliveryDOS = (ArrayList<ActiveDeliveryDO>) results.values;
//            notifyDataSetChanged();
//        }
//
//    }
//
//    public void refreshAdapter(ArrayList<ActiveDeliveryDO> activeDeliveryDOS, int Type) {
//        this.activeDeliveryDOS = activeDeliveryDOS;
//        this.type = Type;
//        notifyDataSetChanged();
//    }
//
//    public ArrayList<ActiveDeliveryDO> selectedactiveDeliveryDOs = new ArrayList<>();
//    private String from = "";
//
//    public ArrayList<ActiveDeliveryDO> getSelectedactiveDeliveryDOs() {
//        return selectedactiveDeliveryDOs;
//    }
//
//    public MRScheduledProductsAdapter(Context context, ArrayList<ActiveDeliveryDO> activeDeliveryDOS, String from, String damagE, int Type) {
//        this.context = context;
//        this.activeDeliveryDOS = activeDeliveryDOS;
//        this.from = from;
//        this.type = Type;
//
//    }
//
//    @Override
//    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.scheduled_load_stock_data, parent, false);
//        return new MyViewHolder(itemView);
//    }
//
//    @Override
//    public void onBindViewHolder(final MyViewHolder holder, final int position) {
//        final ActiveDeliveryDO activeDeliveryDO = activeDeliveryDOS.get(position);
//        AppConstants.productDO = activeDeliveryDOS;
////        final ActiveDeliveryDO productDO = AppConstants.productDO.get(position);
//
//        if (type == 1) {
//            holder.tvSelection.setText("" + activeDeliveryDO.price);
//        }else {
//            holder.tvSelection.setText("" + activeDeliveryDO.price);
//
//        }
//        int param = ((BaseActivity) context).preferenceUtils.getIntFromPreference(PreferenceUtils.UNIT, 0);
//        if (param == 2) {
//            unit = "GALLON";
//        } else {
//            unit = "Litre";
//        }
//        if (from.equalsIgnoreCase("AddProducts")) {
//            holder.tvProductName.setText(activeDeliveryDO.product+"\nSite - "+activeDeliveryDO.site);
//            holder.tvAvailableQty.setVisibility(View.VISIBLE);
//        }else {
//            holder.tvProductName.setText(activeDeliveryDO.product);
//
//        }
//        holder.tvDescription.setText(activeDeliveryDO.productDescription);
//        String shipmentType = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, "");
//        holder.tvAvailableQty.setText("Stock : " + activeDeliveryDO.totalQuantity + " " + activeDeliveryDO.unit);
//
//        if (from.equalsIgnoreCase("AddProducts")) {
//            holder.llFoc.setVisibility(View.GONE);
//            holder.llPriceTag.setVisibility(View.GONE);
//            if (activeDeliveryDO.totalQuantity > 0) {
//                activeDeliveryDO.orderedQuantity = 1.0;
//
//            } else {
//                activeDeliveryDO.orderedQuantity = 0.0;
//
//            }
//            holder.tvNumberET.setText("" + activeDeliveryDO.orderedQuantity);
//            if (activeDeliveryDO.percentage == 0.0) {
//                holder.tvAvailableQty.setText("Stock : " + activeDeliveryDO.totalQuantity + " " + activeDeliveryDO.unit);
//
//            } else {
//                holder.tvAvailableQty.setText("Stock : " + activeDeliveryDO.totalQuantity + " (" + activeDeliveryDO.percentage + "%)");
//            }
//        } else {
//            holder.tvNumberET.setText("" + activeDeliveryDO.orderedQuantity);
//            if (shipmentType.equalsIgnoreCase("Scheduled")) {
//                holder.llPriceTag.setVisibility(View.GONE);
//                holder.llFoc.setVisibility(View.GONE);
//            } else {
//                if (type == 1) {
//                    holder.llFoc.setVisibility(View.VISIBLE);
//                    holder.llPriceTag.setVisibility(View.VISIBLE);
//                    holder.llPriceTag.setClickable(true);
//                    holder.llPriceTag.setEnabled(true);
//                    holder.tvEdit.setVisibility(View.VISIBLE);
//                } else {
//                    holder.llFoc.setVisibility(View.GONE);
//                    holder.llPriceTag.setVisibility(View.VISIBLE);
//                    holder.llPriceTag.setClickable(false);
//                    holder.llPriceTag.setEnabled(false);
//                    holder.tvEdit.setVisibility(View.GONE);
//
//                }
//
//            }
//            if (context instanceof ScheduledCaptureDeliveryActivity) {
//                String text= ((ScheduledCaptureDeliveryActivity) context).rbNormal.getText().toString();
//                if(text.contains("Order")){
//                    holder.llFoc.setVisibility(View.GONE);
//                    holder.llPriceTag.setVisibility(View.VISIBLE);
//                    holder.llPriceTag.setClickable(false);
//                    holder.llPriceTag.setEnabled(false);
//                    holder.tvEdit.setVisibility(View.GONE);
//                }else {
//                    holder.llFoc.setVisibility(View.VISIBLE);
//                    holder.llPriceTag.setVisibility(View.VISIBLE);
//                    holder.llPriceTag.setClickable(true);
//                    holder.llPriceTag.setEnabled(true);
//                    holder.tvEdit.setVisibility(View.VISIBLE);
//                }
//            }
//        }
//
//        int pos = position + 1;
//        int num = pos * 1000;
//        activeDeliveryDO.line = num;
//
//
//        String shipmentProductsType = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "");
//        activeDeliveryDO.shipmentProductType = shipmentProductsType;
//        if (from.equalsIgnoreCase("AddProducts")) {
//            holder.cbSelected.setVisibility(View.VISIBLE);
//            holder.llPriceTag.setVisibility(View.GONE);
//
//        } else if (from.equalsIgnoreCase("Shipments")) {
//            holder.tvNumberET.setVisibility(View.GONE);
//            holder.ivRemove.setVisibility(View.GONE);
//            holder.ivAdd.setVisibility(View.GONE);
//            holder.tvAvailableQty.setVisibility(View.GONE);
//            holder.tvNumber.setVisibility(View.VISIBLE);
//            holder.cbSelected.setVisibility(View.GONE);
//            holder.tvNumber.setText("Qty : " + activeDeliveryDO.totalQuantity + " " + activeDeliveryDO.unit);
//            holder.llPriceTag.setVisibility(View.GONE);
//            holder.llFoc.setVisibility(View.GONE);
//
//        }
//        if (activeDeliveryDO.shipmentProductType.equalsIgnoreCase(AppConstants.MeterReadingProduct)) {
//            holder.tvInfo.setVisibility(View.VISIBLE);
//            holder.llFoc.setVisibility(View.GONE);
//            holder.tvAvailableQty.setVisibility(View.VISIBLE);
//
//        } else {
//            holder.tvInfo.setVisibility(View.GONE);
//
//        }
//        if (activeDeliveryDO.shipmentProductType.equalsIgnoreCase(AppConstants.FixedQuantityProduct)
//                && !from.equalsIgnoreCase("Shipments")) {
//            holder.tvNumberET.setVisibility(View.VISIBLE);
//            holder.ivRemove.setVisibility(View.VISIBLE);
//            holder.ivAdd.setVisibility(View.VISIBLE);
//            holder.tvNumber.setVisibility(View.GONE);
//            holder.llMeterReadings.setVisibility(View.GONE);
//            holder.llAddRemove.setVisibility(View.VISIBLE);
////            holder.tvAvailableQty.setText("Stock : " + activeDeliveryDO.totalQuantity + " " + activeDeliveryDO.unit);
//            if (from.equalsIgnoreCase("AddProducts")) {
//                holder.tvAvailableQty.setVisibility(View.VISIBLE);
//            } else {
//                holder.tvAvailableQty.setVisibility(View.GONE);
//
//            }
////            activeDeliveryDO.orderedQuantity = activeDeliveryDO.totalQuantity;
////            holder.tvNumberET.setText("" + activeDeliveryDO.totalQuantity);// to fix issue at added products showing full qty instead selected qty in add productlistactivity
//            holder.tvNumberET.setText("" + activeDeliveryDO.orderedQuantity);
//        } else if (activeDeliveryDO.shipmentProductType.equalsIgnoreCase(AppConstants.MeterReadingProduct)
//                && !from.equalsIgnoreCase("Shipments") && !from.equalsIgnoreCase("AddProducts")) {
//            holder.tvNumberET.setVisibility(View.VISIBLE);
//            holder.ivRemove.setVisibility(View.VISIBLE);
//            holder.ivAdd.setVisibility(View.VISIBLE);
//            holder.tvNumber.setVisibility(View.GONE);
//            holder.etTotalMeterQty.setText("" + activeDeliveryDO.orderedQuantity);
////            holder.etKGTotalMeterQty.setText("" + activeDeliveryDO.orderedQuantity);
//            holder.llPriceTag.setVisibility(View.GONE);
//            holder.llFoc.setVisibility(View.GONE);
//            holder.tvInfo.setVisibility(View.VISIBLE);
//
//
//            holder.tvNumberET.setText("");
//            if (activeDeliveryDO.unit.length() > 0) {
//                holder.tvProductUnit.setText("Qty1");
////                holder.tvProductUnit.setText("Qty1 " + "("+(activeDeliveryDO.unit)+")");
//
//                if (activeDeliveryDO.unit.equalsIgnoreCase("KG")) {
////                    holder.llKGMeterReadings.setVisibility(View.VISIBLE);
////                    holder.llMeterReadings.setVisibility(View.GONE);
//                    holder.llMeterReadings.setVisibility(View.VISIBLE);
////                    holder.tvKGProductUnit.setText("Qty1 (" + (activeDeliveryDO.unit) + ")");
////                    holder.tvKGProductUnit.setText("Qty1");
//
//
//                } else {
//                    holder.llMeterReadings.setVisibility(View.VISIBLE);
////                    holder.tvProductUnit.setText("Qty1 (" + (activeDeliveryDO.unit) + ")");
//                    holder.tvProductUnit.setText("Qty1");
//
//                }
//
//            } else {
//                holder.llMeterReadings.setVisibility(View.VISIBLE);
//
//            }
//            holder.llAddRemove.setVisibility(View.GONE);
////            holder.etTotalMeterQty.setText("");
////            holder.etTotalMeterQty.append("" + activeDeliveryDO.totalQuantity);
////            holder.etKGTotalMeterQty.setText("");
////            holder.etKGTotalMeterQty.append("" + activeDeliveryDO.totalQuantity);
//
//            holder.tvAvailableQty.setVisibility(View.GONE);
//        } else if (activeDeliveryDO.productType.equalsIgnoreCase(AppConstants.ShipmentListView)) {
//            holder.tvNumberET.setVisibility(View.GONE);
//            holder.ivRemove.setVisibility(View.GONE);
//            holder.ivAdd.setVisibility(View.GONE);
//            holder.tvNumber.setVisibility(View.VISIBLE);
//            holder.tvAvailableQty.setVisibility(View.GONE);
//            holder.tvNumber.setText("" + activeDeliveryDO.totalQuantity + " " + activeDeliveryDO.unit);
//        }
//
//        holder.cbSelected.setOnCheckedChangeListener(null);
//        holder.cbSelected.setChecked(activeDeliveryDO.isProductAdded);
//        holder.cbSelected.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                activeDeliveryDO.isProductAdded = isChecked;
//                if (isChecked) {
//                    if (activeDeliveryDO.totalQuantity > 0) {
//                        selectedactiveDeliveryDOs.add(activeDeliveryDO);
//
//                    }
//                } else {
//                    selectedactiveDeliveryDOs.remove(activeDeliveryDO);
//                }
//            }
//        });
//        Double maxCount = activeDeliveryDO.totalQuantity;//Integer.parseInt(holder.tvNumberET.getText().toString());
//        final Double[] quantity = {activeDeliveryDO.orderedQuantity};//{Integer.parseInt(holder.tvNumberET.getText().toString())};
//        holder.ivAdd.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (isClickable == true) {
//                    if (quantity[0] < maxCount) {
//                        int count = 0;
//                        AppConstants.productDO.get(position).count = AppConstants.productDO.get(position).count + 1;
//                        quantity[0] = quantity[0] + 1;
//                        holder.tvNumberET.setText("" + quantity[0]++);
//                        activeDeliveryDO.orderedQuantity = quantity[0];
//                        if (context instanceof ScheduledCaptureDeliveryActivity) {
//                            ((ScheduledCaptureDeliveryActivity) context).updateConfirmRequestButtons("", activeDeliveryDOS);
//                            if (!shipmentType.equalsIgnoreCase("Scheduled")) {
//                                Double price = 0.0;
//                                for (ActiveDeliveryDO activeDeliveryDO : AppConstants.productDO) {
//                                    price = price + activeDeliveryDO.price * activeDeliveryDO.count;
//                                    count = count + activeDeliveryDO.count;
//                                }
//                                if (AppConstants.productDO.get(position).count > 1) {
//                                    ((ScheduledCaptureDeliveryActivity) context).tvItemCount.setText(count + " Items");
//                                } else {
//                                    ((ScheduledCaptureDeliveryActivity) context).tvItemCount.setText(count + " Item");
//                                }
//                                ((ScheduledCaptureDeliveryActivity) context).tvItemPrice.setText("AED " + price);
//                                Double vat = price * 5 / 100;
//                                Double total = price + vat;
//                                ((ScheduledCaptureDeliveryActivity) context).tvItemVat.setText("AED " + vat);
//                                ((ScheduledCaptureDeliveryActivity) context).tvItemTotal.setText("AED " + total);
//                                Double amount = ((ScheduledCaptureDeliveryActivity) context).preferenceUtils.getDoubleFromPreference(PreferenceUtils.CREDIT_AMOUNT, 0.0);
//                                if (total > amount) {
//                                    ((ScheduledCaptureDeliveryActivity) context).btnConfirm.setText("Create Order");
//                                    ((ScheduledCaptureDeliveryActivity) context).rbNormal.setText("Normal Order");
//                                    ((ScheduledCaptureDeliveryActivity) context).rbLoan.setText("Loan Order");
//
//                                    ((ScheduledCaptureDeliveryActivity) context).updateUI();
//
//                                } else {
//                                    ((ScheduledCaptureDeliveryActivity) context).btnConfirm.setText("Create Delivery");
//                                    ((ScheduledCaptureDeliveryActivity) context).rbNormal.setText("Normal Delivery");
//                                    ((ScheduledCaptureDeliveryActivity) context).rbLoan.setText("Loan Delivery");
//
//                                    ((ScheduledCaptureDeliveryActivity) context).updateUI();
//
//                                }
//                                holder.tvSelection.setText("" + activeDeliveryDO.price * AppConstants.productDO.get(position).count);
//
//                            }
//
//                        }
//
//                    }
//                } else {
//                    ((BaseActivity) context).showToast("Please process the order..!");
//
//                }
//
//            }
//        });
//        holder.ivRemove.setOnClickListener(view -> {
//            if (isClickable == true) {
//
//                if (quantity[0] > 1) {
//                    int count = 0;
//                    AppConstants.productDO.get(position).count = AppConstants.productDO.get(position).count - 1;
//                    quantity[0] = quantity[0] - 1;
//                    holder.tvNumberET.setText("" + quantity[0]);
//                    activeDeliveryDO.orderedQuantity = quantity[0];
//                    if (context instanceof ScheduledCaptureDeliveryActivity) {
//                        ((ScheduledCaptureDeliveryActivity) context).updateConfirmRequestButtons("", activeDeliveryDOS);
//                        if (!shipmentType.equalsIgnoreCase("Scheduled")) {
//                            Double price = 0.0;
//                            for (ActiveDeliveryDO activeDeliveryDOO : AppConstants.productDO) {
//                                price = price + activeDeliveryDOO.price * activeDeliveryDOO.count;
//                                count = count + activeDeliveryDOO.count;
//                            }
//                            if (AppConstants.productDO.get(position).count > 1) {
//                                ((ScheduledCaptureDeliveryActivity) context).tvItemCount.setText(count + " Items");
//                            } else {
//                                ((ScheduledCaptureDeliveryActivity) context).tvItemCount.setText(count + " Item");
//                            }
//                            ((ScheduledCaptureDeliveryActivity) context).tvItemPrice.setText("AED " + price);
//                            Double vat = price * 5 / 100;
//                            Double total = price + vat;
//                            ((ScheduledCaptureDeliveryActivity) context).tvItemVat.setText("AED " + vat);
//                            ((ScheduledCaptureDeliveryActivity) context).tvItemTotal.setText("AED " + total);
//                            Double amount = ((ScheduledCaptureDeliveryActivity) context).preferenceUtils.getDoubleFromPreference(PreferenceUtils.CREDIT_AMOUNT, 0.0);
//                            if (total > amount) {
//                                ((ScheduledCaptureDeliveryActivity) context).btnConfirm.setText("Create Order");
//                                ((ScheduledCaptureDeliveryActivity) context).rbNormal.setText("Normal Order");
//                                ((ScheduledCaptureDeliveryActivity) context).rbLoan.setText("Loan Order");
//
//                                ((ScheduledCaptureDeliveryActivity) context).updateUI();
//                            } else {
//                                ((ScheduledCaptureDeliveryActivity) context).btnConfirm.setText("Create Delivery");
//                                ((ScheduledCaptureDeliveryActivity) context).rbNormal.setText("Normal Delivery");
//                                ((ScheduledCaptureDeliveryActivity) context).rbLoan.setText("Loan Delivery");
//                                ((ScheduledCaptureDeliveryActivity) context).updateUI();
//
//
//                            }
//                            holder.tvSelection.setText("" + activeDeliveryDO.price * AppConstants.productDO.get(position).count);
//
//                        }
//                    }
//                }
//            } else {
//                ((BaseActivity) context).showToast("Please process the order..!");
//
//            }
//        });
//        holder.tvNumberET.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                try {
//                    if (activeDeliveryDO.totalQuantity > 0) {
//                        if (s.toString().equals(".") && s.toString().equalsIgnoreCase("")) {
//                            holder.tvNumberET.setText("1.0");
//                        }
//
//                        if (!s.toString().equalsIgnoreCase("") || Double.parseDouble(s.toString()) == .0 || !s.toString().equals(".")) {
//                            if (Double.parseDouble(s.toString()) <= maxCount && Double.parseDouble(s.toString()) > 0) {
//                                quantity[0] = Double.parseDouble(s.toString());
//                                activeDeliveryDO.orderedQuantity = quantity[0];
//                                if (context instanceof ScheduledCaptureDeliveryActivity) {
//                                    ((ScheduledCaptureDeliveryActivity) context).updateConfirmRequestButtons("", activeDeliveryDOS);
//                                }
//                            } else {
//                                holder.tvNumberET.setText("1.0");
//                                if (s.length() > 1) {
//                                    final String newText = s.toString().substring(0, s.length() - 1) + "";
//                                    holder.tvNumberET.setText("" + newText);
//                                }
//                            }
//                        } else {
//                            holder.tvNumberET.setText("1.0");
//                        }
//                    } else {
//                        holder.tvNumberET.removeTextChangedListener(this);
//                        holder.tvNumberET.setText("0");
//                        holder.tvNumberET.addTextChangedListener(this);
//
//                    }
//                } catch (NumberFormatException e) {
//                    holder.tvNumberET.setText("1.0");
//                }
//                if (context instanceof ScheduledCaptureDeliveryActivity) {
//                    if (!shipmentType.equalsIgnoreCase("Scheduled")) {
//
//                        Double price = 0.0;
//                        Double count = Double.valueOf((holder.tvNumberET.getText().toString()));
//                        int convertedCount =(int) Math.round(count);
//                        AppConstants.productDO.get(position).count=convertedCount;
//
//                        for (ActiveDeliveryDO activeDeliveryDOO : AppConstants.productDO) {
//                            price = price + activeDeliveryDOO.price * activeDeliveryDOO.count;
//                            count = count + activeDeliveryDOO.count;
//                        }
//
//                        if (AppConstants.productDO.get(position).count > 1) {
//                            ((ScheduledCaptureDeliveryActivity) context).tvItemCount.setText(count + " Items");
//                        } else {
//                            ((ScheduledCaptureDeliveryActivity) context).tvItemCount.setText(count + " Item");
//                        }
//                        ((ScheduledCaptureDeliveryActivity) context).tvItemPrice.setText("AED " + price);
//                        Double vat = price * 5 / 100;
//                        Double total = price + vat;
//                        ((ScheduledCaptureDeliveryActivity) context).tvItemVat.setText("AED " + vat);
//                        ((ScheduledCaptureDeliveryActivity) context).tvItemTotal.setText("AED " + total);
//                        Double amount = ((ScheduledCaptureDeliveryActivity) context).preferenceUtils.getDoubleFromPreference(PreferenceUtils.CREDIT_AMOUNT, 0.0);
//                        if (total > amount) {
//                            ((ScheduledCaptureDeliveryActivity) context).btnConfirm.setText("Create Order");
//                            ((ScheduledCaptureDeliveryActivity) context).rbNormal.setText("Normal Order");
//                            ((ScheduledCaptureDeliveryActivity) context).rbLoan.setText("Loan Order");
//                            ((ScheduledCaptureDeliveryActivity) context).updateUI();
//                        } else {
//                            ((ScheduledCaptureDeliveryActivity) context).btnConfirm.setText("Create Delivery");
//                            ((ScheduledCaptureDeliveryActivity) context).rbNormal.setText("Normal Delivery");
//                            ((ScheduledCaptureDeliveryActivity) context).rbLoan.setText("Loan Delivery");
//                            ((ScheduledCaptureDeliveryActivity) context).updateUI();
//                        }
//                        holder.tvSelection.setText("" + activeDeliveryDO.price * AppConstants.productDO.get(position).count);
//
//                    }
//                }
//
//            }
//        });
//        if (context instanceof ScheduledCaptureDeliveryActivity) {
//            holder.ivDelete.setVisibility(View.GONE);
//        } else {
//            holder.ivDelete.setVisibility(View.GONE);
//        }
//        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (context instanceof ScheduledCaptureDeliveryActivity) {
//                    ((ScheduledCaptureDeliveryActivity) context).deleteShipmentProducts(activeDeliveryDO);
//                }
//            }
//        });
//
//
//        TextWatcher textWatcher1 = new TextWatcher() {// should be less than read 2
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                try {
//                    if (holder.etMeterReading1.getText().toString().trim().equalsIgnoreCase("")) {
//                        holder.tvInfo.setText("Info:  Please enter Read 1 value");
//                        ((ScheduledCaptureDeliveryActivity) context).enableDisableConfirm(false);
//                    } else {
//
//                        if (unit.equalsIgnoreCase("GALLON")) {
//                            holder.tvInfo.setText("");
//
//                            double reading1 = Double.parseDouble(holder.etMeterReading1.getText().toString().trim());
//                            activeDeliveryDO.orderedQuantity = 0.0;
//                            holder.etMeterReading2.setText("");
//                            holder.etMeterReading3.setText("");
//                            activeDeliveryDO.openingQuantity = new BigDecimal(reading1).stripTrailingZeros().toPlainString();
//                            activeDeliveryDO.orderedQuantity = Double.parseDouble(new BigDecimal(maxCount * 4.55).stripTrailingZeros().toPlainString());
//                        } else {
//                            holder.tvInfo.setText("");
//                            double reading1 = Double.parseDouble(holder.etMeterReading1.getText().toString().trim());
//                            activeDeliveryDO.orderedQuantity = 0.0;
//                            holder.etMeterReading2.setText("");
//                            holder.etMeterReading3.setText("");
//                            activeDeliveryDO.openingQuantity = new BigDecimal(reading1).stripTrailingZeros().toPlainString();
//                            activeDeliveryDO.orderedQuantity = Double.parseDouble(new BigDecimal(maxCount).stripTrailingZeros().toPlainString());
//                        }
//
//                    }
//                } catch (NumberFormatException e) {
//                    e.printStackTrace();
//                }
//            }
//        };
//
//        TextWatcher textWatcher2 = new TextWatcher() {//should be less
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {// wherever no need to display toast you can remove
//
//                if (holder.etMeterReading1.getText().toString().trim().equalsIgnoreCase("")) {
//                    holder.tvInfo.setText("Info:  Please enter Read 1 value");
//                    if (context instanceof ScheduledCaptureDeliveryActivity) {
//                        ((ScheduledCaptureDeliveryActivity) context).enableDisableConfirm(false);
//                    }
//                    holder.etMeterReading2.removeTextChangedListener(this);
//                    holder.etMeterReading2.setText("");
//                    holder.etMeterReading2.addTextChangedListener(this);
//                } else {
//                    if (holder.etMeterReading2.getText().toString().trim().equalsIgnoreCase("")) {
////                        if (holder.etMeterReading3.getText().toString().trim().equalsIgnoreCase("")) {
////                        }
//                        holder.tvInfo.setText("Info : Please enter Read 2 value");
//                        if (context instanceof ScheduledCaptureDeliveryActivity) {
//                            ((ScheduledCaptureDeliveryActivity) context).enableDisableConfirm(false);
//                        }
//                    } else {
//                        if (unit.equalsIgnoreCase("GALLON")) {
//                            Double reading1 = 0.0;
//                            Double reading2 = 0.0;
//                            Double reading3 = 0.0;
//
//                            if (holder.etMeterReading1.getText().toString().trim().isEmpty() || holder.etMeterReading1.getText().toString().trim().equalsIgnoreCase(".")) {
//                                reading1 = 0.0;
//                            } else {
//                                reading1 = Double.parseDouble(holder.etMeterReading1.getText().toString().trim());
//
//                            }
//                            if (holder.etMeterReading2.getText().toString().trim().isEmpty() || holder.etMeterReading2.getText().toString().trim().equalsIgnoreCase(".")) {
//                                reading2 = 0.0;
//                            } else {
//                                reading2 = Double.parseDouble(holder.etMeterReading2.getText().toString().trim());
//
//                            }
//                            if (holder.etMeterReading3.getText().toString().trim().isEmpty() || holder.etMeterReading3.getText().toString().trim().equalsIgnoreCase(".")) {
//                                reading3 = 0.0;
//                            } else {
//                                reading3 = Double.parseDouble(holder.etMeterReading3.getText().toString().trim());
//
//                            }
//                            Double qty2 = reading2 - reading1;
//                            if (qty2 > 0) {
//                                holder.etMeterReading3.setText("" + new BigDecimal(qty2 * 4.55).stripTrailingZeros().toPlainString());
//                            } else {
//                                holder.etMeterReading3.setText("");
//                            }
//                            if (reading2 > reading1) {
//                                if (context instanceof ScheduledCaptureDeliveryActivity) {
//                                    ((ScheduledCaptureDeliveryActivity) context).enableDisableConfirm(true);
//                                }
////                            double etReading1 = Double.parseDouble(holder.etMeterReading1.getText().toString().trim());
////                            double etReading2 = Double.parseDouble(holder.etMeterReading2.getText().toString().trim());
////                            double etReading3 = Double.parseDouble(holder.etMeterReading3.getText().toString().trim());
//                                if (holder.etMeterReading3.getText().toString().trim().isEmpty() || holder.etMeterReading3.getText().toString().trim().equalsIgnoreCase(".")) {
//                                    reading3 = 0.0;
//                                } else {
//                                    reading3 = Double.parseDouble(holder.etMeterReading3.getText().toString().trim());
//                                }
//                                activeDeliveryDO.openingQuantity =new BigDecimal(reading1).stripTrailingZeros().toPlainString();
//                                activeDeliveryDO.endingQuantity = new BigDecimal(reading2).stripTrailingZeros().toPlainString();
//                                activeDeliveryDO.orderedQuantity = Double.parseDouble(new BigDecimal(reading3).stripTrailingZeros().toPlainString());
//                            } else {
//                                holder.tvInfo.setText("Info : Please enter higher value than Read 1");
//                                if (context instanceof ScheduledCaptureDeliveryActivity) {
//                                    ((ScheduledCaptureDeliveryActivity) context).enableDisableConfirm(false);
//                                }
//                            }
//                        } else {
//                            Double reading1 = 0.0;
//                            Double reading2 = 0.0;
//                            Double reading3 = 0.0;
//
//                            if (holder.etMeterReading1.getText().toString().trim().isEmpty() || holder.etMeterReading1.getText().toString().trim().equalsIgnoreCase(".")) {
//                                reading1 = 0.0;
//                            } else {
//                                reading1 = Double.parseDouble(holder.etMeterReading1.getText().toString().trim());
//
//                            }
//                            if (holder.etMeterReading2.getText().toString().trim().isEmpty() || holder.etMeterReading2.getText().toString().trim().equalsIgnoreCase(".")) {
//                                reading2 = 0.0;
//                            } else {
//                                reading2 = Double.parseDouble(holder.etMeterReading2.getText().toString().trim());
//
//                            }
//                            if (holder.etMeterReading3.getText().toString().trim().isEmpty() || holder.etMeterReading3.getText().toString().trim().equalsIgnoreCase(".")) {
//                                reading3 = 0.0;
//                            } else {
//
//                                try {
//                                    reading3 = Double.parseDouble(holder.etMeterReading3.getText().toString().trim());
//                                } catch (NumberFormatException e) {
////                                    reading3 = 0.0; // your default value
//                                    holder.etMeterReading3.setText("0.0");
//                                }
//
//                            }
////                        double etReading1 = reading1;
////                        double etReading2 = reading2;
////                        double etReading3 = reading3;
////                        Double reading1 = Double.parseDouble(holder.etMeterReading1.getText().toString().trim());
////                       Double reading2 = Double.parseDouble(holder.etMeterReading2.getText().toString().trim());
//                            Double qty2 = reading2 - reading1;
//                            if (qty2 > 0) {
//                                holder.etMeterReading3.setText("" + new BigDecimal(qty2).stripTrailingZeros().toPlainString());
//                            } else {
//                                holder.etMeterReading3.setText("");
//                            }
//                            if (reading2 > reading1) {
//                                if (context instanceof ScheduledCaptureDeliveryActivity) {
//                                    ((ScheduledCaptureDeliveryActivity) context).enableDisableConfirm(true);
//                                }
//                                if (holder.etMeterReading3.getText().toString().trim().isEmpty() || holder.etMeterReading3.getText().toString().trim().equalsIgnoreCase(".")) {
//                                    reading3 = 0.0;
//                                } else {
//                                    String readng =holder.etMeterReading3.getText().toString().trim();
//                                    if(!readng.isEmpty()){
//                                        try{
//                                            reading3 = Double.parseDouble(readng);
//
//                                        }catch (Exception e){
////                                            reading3 = 0.0;
//                                            holder.etMeterReading3.setText("0.0");
//                                        }
//
//                                    }else {
//                                        reading3 = 0.0;
//
//                                    }
//
//                                }
//                                activeDeliveryDO.openingQuantity = new BigDecimal(reading1).stripTrailingZeros().toPlainString();;
//                                activeDeliveryDO.endingQuantity =  new BigDecimal(reading2).stripTrailingZeros().toPlainString();
//                                activeDeliveryDO.orderedQuantity = Double.parseDouble(new BigDecimal(reading3).stripTrailingZeros().toPlainString());
//                            } else {
//                                holder.tvInfo.setText("Info : Please enter higher value than Read 1");
//                                if (context instanceof ScheduledCaptureDeliveryActivity) {
//                                    ((ScheduledCaptureDeliveryActivity) context).enableDisableConfirm(false);
//                                }
//                            }
//                        }
//
//                    }
//                }
//            }
//        };
//
//        TextWatcher textWatcher3 = new TextWatcher() {//should be less
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {// wherever no need to display toast you can remove
//
//                if (holder.etMeterReading1.getText().toString().trim().equalsIgnoreCase("")) {
//                    holder.tvInfo.setText("Info:  Please enter Read 1 value");
//                    ((ScheduledCaptureDeliveryActivity) context).enableDisableConfirm(false);
//                    holder.etMeterReading3.removeTextChangedListener(this);
//                    holder.etMeterReading3.setText("");
//                    holder.etMeterReading3.addTextChangedListener(this);
//                } else {
//                    if (holder.etMeterReading3.getText().toString().trim().equalsIgnoreCase("")) {
////                        holder.tvInfo.setText("Info:  Please enter quantity2 value");
//                        if (context instanceof ScheduledCaptureDeliveryActivity) {
//                            ((ScheduledCaptureDeliveryActivity) context).enableDisableConfirm(false);
//                        }
//                    } else {
//                        if (unit.equalsIgnoreCase("GALLON")) {
//                            holder.tvInfo.setText("");
//                            if (!holder.etMeterReading2.hasFocus()) {
//
//
//                                Double reading1 = 0.0;
//                                Double reading2 = 0.0;
//                                Double reading3 = 0.0;
//
//                                if (holder.etMeterReading1.getText().toString().trim().isEmpty() || holder.etMeterReading1.getText().toString().trim().equalsIgnoreCase(".")) {
//                                    reading1 = 0.0;
//                                } else {
//                                    reading1 = Double.parseDouble(holder.etMeterReading1.getText().toString().trim());
//
//                                }
//                                if (holder.etMeterReading2.getText().toString().trim().isEmpty() || holder.etMeterReading2.getText().toString().trim().equalsIgnoreCase(".")) {
//                                    reading2 = 0.0;
//                                } else {
//                                    reading2 = Double.parseDouble(holder.etMeterReading2.getText().toString().trim());
//
//                                }
//                                if (holder.etMeterReading3.getText().toString().trim().isEmpty() || holder.etMeterReading3.getText().toString().trim().equalsIgnoreCase(".")) {
//                                    reading3 = 0.0;
//                                } else {
//                                    reading3 = Double.parseDouble(holder.etMeterReading3.getText().toString().trim());
//
//                                }
//                                double etReading1 = reading1;
//                                double etReading2 = reading2;
//                                double etReading3 = reading3;
//                                Double read3 = Double.parseDouble(new BigDecimal(etReading3).stripTrailingZeros().toPlainString());
//                                Double read1 = Double.parseDouble( new BigDecimal(etReading1).stripTrailingZeros().toPlainString());
//                                Double read2 = read1 + read3;
//                                if (read3 > 0) {
//                                    holder.etMeterReading2.removeTextChangedListener(textWatcher2);
//                                    holder.etMeterReading2.setText("" +  new BigDecimal(read2).stripTrailingZeros().toPlainString());
//                                    holder.etMeterReading2.addTextChangedListener(textWatcher2);
//
//                                    activeDeliveryDO.openingQuantity =  new BigDecimal(etReading1).stripTrailingZeros().toPlainString();;
//                                    activeDeliveryDO.endingQuantity =  new BigDecimal(etReading2).stripTrailingZeros().toPlainString();;
//
//                                    activeDeliveryDO.orderedQuantity = Double.parseDouble(new BigDecimal(etReading3 * 4.55).stripTrailingZeros().toPlainString());
//
//                                    if (context instanceof ScheduledCaptureDeliveryActivity) {
//                                        ((ScheduledCaptureDeliveryActivity) context).enableDisableConfirm(true);
//                                    }
//                                } else {
//                                    holder.etMeterReading3.removeTextChangedListener(this);
//                                    holder.etMeterReading3.setText("");
//                                    holder.etMeterReading3.addTextChangedListener(this);
//                                    if (context instanceof ScheduledCaptureDeliveryActivity) {
//                                        ((ScheduledCaptureDeliveryActivity) context).enableDisableConfirm(false);
//                                    }
//                                }
//                            }
//                        } else {
//                            holder.tvInfo.setText("");
//                            if (!holder.etMeterReading2.hasFocus()) {
//
//
//                                Double reading1 = 0.0;
//                                Double reading2 = 0.0;
//                                Double reading3 = 0.0;
//
//                                if (holder.etMeterReading1.getText().toString().trim().isEmpty() || holder.etMeterReading1.getText().toString().trim().equalsIgnoreCase(".")) {
//                                    reading1 = 0.0;
//                                } else {
//                                    reading1 = Double.parseDouble(holder.etMeterReading1.getText().toString().trim());
//
//                                }
//                                if (holder.etMeterReading2.getText().toString().trim().isEmpty() || holder.etMeterReading2.getText().toString().trim().equalsIgnoreCase(".")) {
//                                    reading2 = 0.0;
//                                } else {
//                                    reading2 = Double.parseDouble(holder.etMeterReading2.getText().toString().trim());
//
//                                }
//                                if (holder.etMeterReading3.getText().toString().trim().isEmpty() || holder.etMeterReading3.getText().toString().trim().equalsIgnoreCase(".")) {
//                                    reading3 = 0.0;
//                                } else {
//                                    try {
//                                        reading3 = Double.parseDouble(holder.etMeterReading3.getText().toString().trim());
//
//                                    }catch (Exception e){
//                                        holder.etMeterReading3.setText("0.0");
//
//                                    }
//
//                                }
//                                double etReading1 = reading1;
//                                double etReading2 = reading2;
//                                double etReading3 = reading3;
//
//                                Double read3 = Double.parseDouble(new BigDecimal(etReading3).stripTrailingZeros().toPlainString());
//                                Double read1 = Double.parseDouble(new BigDecimal(etReading1).stripTrailingZeros().toPlainString());
//                                Double read2 = read1 + read3;
//                                if (read3 > 0) {
//                                    holder.etMeterReading2.removeTextChangedListener(textWatcher2);
//                                    holder.etMeterReading2.setText("" + new BigDecimal(read2).stripTrailingZeros().toPlainString());
//                                    holder.etMeterReading2.addTextChangedListener(textWatcher2);
//                                    if (context instanceof ScheduledCaptureDeliveryActivity) {
//                                        ((ScheduledCaptureDeliveryActivity) context).enableDisableConfirm(true);
//                                    }
//                                    activeDeliveryDO.openingQuantity =new BigDecimal(read1).stripTrailingZeros().toPlainString();
//                                    activeDeliveryDO.endingQuantity = new BigDecimal(read2).stripTrailingZeros().toPlainString();
//                                    activeDeliveryDO.orderedQuantity = Double.parseDouble(new BigDecimal(read3).stripTrailingZeros().toPlainString());
//                                } else {
//                                    holder.etMeterReading3.removeTextChangedListener(this);
//                                    holder.etMeterReading3.setText("");
//                                    holder.etMeterReading3.addTextChangedListener(this);
//                                    if (context instanceof ScheduledCaptureDeliveryActivity) {
//                                        ((ScheduledCaptureDeliveryActivity) context).enableDisableConfirm(false);
//                                    }
//                                }
//                            }
//                        }
//
//                    }
//                }
//            }
//        };
//
//        holder.etMeterReading1.addTextChangedListener(textWatcher1);
//
//        holder.etMeterReading2.addTextChangedListener(textWatcher2);
//
//        holder.etMeterReading3.addTextChangedListener(textWatcher3);
//
//
//        holder.llPriceTag.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if (isClickable == true) {
//                    try {
//                        AppCompatDialog dialog = new AppCompatDialog(context, R.style.AppCompatAlertDialogStyle);
//                        dialog.supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
//                        View view = LayoutInflater.from(context).inflate(R.layout.price_custom, null);
//                        EditText etAdd = view.findViewById(R.id.etAdd);
//                        InputFilter filter = new InputFilter() {
//                            final int maxDigitsBeforeDecimalPoint = 5;
//                            final int maxDigitsAfterDecimalPoint = 2;
//
//                            @Override
//                            public CharSequence filter(CharSequence source, int start, int end,
//                                                       Spanned dest, int dstart, int dend) {
//                                StringBuilder builder = new StringBuilder(dest);
//                                builder.replace(dstart, dend, source
//                                        .subSequence(start, end).toString());
//                                if (!builder.toString().matches(
//                                        "(([1-9]{1})([0-9]{0," + (maxDigitsBeforeDecimalPoint - 1) + "})?)?(\\.[0-9]{0," + maxDigitsAfterDecimalPoint + "})?"
//
//                                )) {
//                                    if (source.length() == 0)
//                                        return dest.subSequence(dstart, dend);
//                                    return "";
//                                }
//
//                                return null;
//
//                            }
//                        };
//
//                        etAdd.setFilters(new InputFilter[]{filter});
//                        dialog.setCancelable(true);
//                        dialog.setCanceledOnTouchOutside(true);
//                        Button btnSubmit = view.findViewById(R.id.btnSubmit);
//
//                        btnSubmit.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                String itemName = etAdd.getText().toString().trim();
//
//                                if (itemName.isEmpty()) {
//                                    ((BaseActivity) context).showToast("Please Enter Price");
//                                } else {
//                                    ((BaseActivity) context).preferenceUtils.saveDouble(PreferenceUtils.PRICE_TAG, Double.valueOf(itemName));
//                                    holder.tvSelection.setText("" + itemName);
//                                    activeDeliveryDO.priceTag = Double.valueOf(itemName);
//                                    if (context instanceof ScheduledCaptureDeliveryActivity) {
//                                        ((ScheduledCaptureDeliveryActivity) context).updateConfirmRequestButtons("pc", activeDeliveryDOS);
//                                    }
//                                    holder.llFoc.setVisibility(View.GONE);
////                                    isClickable = false;
//                                    dialog.dismiss();
//                                }
//                            }
//                        });
//
//
//                        dialog.setContentView(view);
//                        if (!dialog.isShowing())
//                            dialog.show();
//                    } catch (Exception e) {
//                    }
//                } else {
//                    ((BaseActivity) context).showToast("Please process the order..!");
//                    holder.llFoc.setVisibility(View.GONE);
//
//                }
//
//            }
//        });
//        holder.swStatus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//
//                if (isClickable == true) {
//
//
//                    if (isChecked) {
//                        Dialog quantityDialog = new Dialog(context, R.style.NewDialog);
//                        quantityDialog.setCancelable(false);
//                        quantityDialog.setCanceledOnTouchOutside(false);
//                        quantityDialog.setContentView(R.layout.simple_list);
//                        Window window = quantityDialog.getWindow();
//                        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
//                        ListView list = (ListView) quantityDialog.findViewById(R.id.lvItems);
//                        ReasonAdapter reasonAdapter = new ReasonAdapter();
//                        reasonMAINDO = ((ScheduledCaptureDeliveryActivity) context).reasonMainDo;
////                        reasonMAINDO = new ReasonMainDO();
//
//                        if (reasonMAINDO != null && reasonMAINDO.reasonDOS.size() > 0) {
//                            quantityDialog.show();
//
//                            list.setAdapter(reasonAdapter);
//                        } else {
//                            ((BaseActivity) context).showToast("No reasons found");
//
//                        }
//                        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                            @Override
//                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//
////                                holder.tvSelection.setText(reasonMainDo.reasonDOS.get(i).reason);
//                                activeDeliveryDO.reasonFOC = reasonMAINDO.reasonDOS.get(i).reason;
//                                if (context instanceof ScheduledCaptureDeliveryActivity) {
//                                    ((ScheduledCaptureDeliveryActivity) context).updateConfirmRequestButtons("FOC", activeDeliveryDOS);
//                                }
//                                quantityDialog.dismiss();
//
//                            }
//                        });
////                       reasonDialog();
//
//                        holder.llPriceTag.setVisibility(View.GONE);
////                        activeDeliveryDO.reasonFOC=((BaseActivity)context).preferenceUtils.getStringFromPreference(PreferenceUtils.FOC,"");
//
////                    if (context instanceof ScheduledCaptureDeliveryActivity) {
////                        ((ScheduledCaptureDeliveryActivity) context).updateConfirmRequestButtons("FOC", activeDeliveryDOS);
////                    }
////                        isClickable = false;
//
//                    } else {
//                        holder.llPriceTag.setVisibility(View.VISIBLE);
//
//                        if (context instanceof ScheduledCaptureDeliveryActivity) {
//                            ((ScheduledCaptureDeliveryActivity) context).updateConfirmRequestButtons("ENABLE", activeDeliveryDOS);
//                        }
//                        isClickable = true;
//
//                    }
//                } else {
////                    if(isChecked){
////                        holder.swStatus.setChecked(true);
////                    }else {
////                        holder.swStatus.setChecked(false);
////
////                    }
////                    holder.swStatus.setChecked(true);
//                    holder.swStatus.setEnabled(false);
//
//                    ((BaseActivity) context).showToast("Please process the order..!");
//
//                }
//            }
//        });
//
//    }
//
//    @Override
//    public int getItemCount() {
//        return activeDeliveryDOS != null ? activeDeliveryDOS.size() : 0;
//    }
//
//
//    public class MyViewHolder extends RecyclerView.ViewHolder {
//        public TextView tvProductName, tvEdit, tvDescription, tvAvailableQty, tvInfo, tvNumber, etTotalMeterQty, tvSelection, tvProductUnit;
//        public RelativeLayout rlRemove, rlAdd;
//        public ImageView ivRemove, ivAdd, ivDelete;
//        public LinearLayout llAddRemove, llMeterReadings, llPriceTag, llFoc;
//        public EditText tvNumberET, etMeterReading1, etMeterReading2, etMeterReading3;
//        public SwitchCompat swStatus;
//
//        private CheckBox cbSelected;
//
//        public MyViewHolder(View view) {
//            super(view);
//            tvProductName = (TextView) view.findViewById(R.id.tvName);
//            tvDescription = (TextView) view.findViewById(R.id.tvDescription);
//            tvNumber = (TextView) view.findViewById(R.id.tvNumber);
//            ivRemove = (ImageView) view.findViewById(R.id.ivRemove);
//            ivAdd = (ImageView) view.findViewById(R.id.ivAdd);
//            tvNumberET = (EditText) view.findViewById(R.id.tvNumberET);
//            swStatus = (SwitchCompat) view.findViewById(R.id.swStatusCustom);
//            tvEdit = (TextView) view.findViewById(R.id.tvEdit);
//
//            tvAvailableQty = (TextView) view.findViewById(R.id.tvAvailableQty);
//            tvInfo = (TextView) view.findViewById(R.id.tvInfo);
//            tvProductUnit = (TextView) view.findViewById(R.id.tvProductUnit);
//            etMeterReading1 = (EditText) view.findViewById(R.id.etMeterReading1);
//            etMeterReading2 = (EditText) view.findViewById(R.id.etMeterReading2);
//            etMeterReading3 = (EditText) view.findViewById(R.id.etMeterReading3);
//            etTotalMeterQty = (TextView) view.findViewById(R.id.etTotalMeterQty);
//
//            llMeterReadings = (LinearLayout) view.findViewById(R.id.llMeterReadings);
//
//            ivDelete = view.findViewById(R.id.ivDelete);
//            cbSelected = view.findViewById(R.id.cbSelected);
//            llAddRemove = (LinearLayout) view.findViewById(R.id.llAddRemove);
//            tvSelection = (TextView) view.findViewById(R.id.tvSelection);
//            llPriceTag = (LinearLayout) view.findViewById(R.id.llPriceTag);
//            llFoc = (LinearLayout) view.findViewById(R.id.llFoc);
//
//        }
//    }
//
//    class ReasonAdapter extends BaseAdapter {
//        private TextView tvName;
//
//        @Override
//        public Object getItem(int i) {
//            return i;
//        }
//
//        @Override
//        public long getItemId(int i) {
//            return i;
//        }
//
//        @Override
//        public View getView(int i, View view, ViewGroup viewGroup) {
//            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            View v = inflater.inflate(R.layout.simple_text, null);
//            tvName = (TextView) v.findViewById(R.id.tvName);
//            tvName.setText(reasonMAINDO.reasonDOS.get(i).reason);
//
//
//            return v;
//        }
//
//        @Override
//        public int getCount() {
//            if (reasonMAINDO.reasonDOS.size() > 0)
//                return reasonMAINDO.reasonDOS.size();
//            else
//                return 0;
//        }
//
//    }
//
////    private Double getCartCount(){
////        Double price =0.0;
////        for(ActiveDeliveryDO activeDeliveryDO : AppConstants.productDO){
////            price = activeDeliveryDO.price*activeDeliveryDO.count;
////        }
////        return price;
////    }
//
//
//}
