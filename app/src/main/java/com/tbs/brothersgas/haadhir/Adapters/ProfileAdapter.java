package com.tbs.brothersgas.haadhir.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.tbs.brothersgas.haadhir.Activitys.CreatePaymentActivity;
import com.tbs.brothersgas.haadhir.Activitys.LoginActivity;
import com.tbs.brothersgas.haadhir.Activitys.OnAccountCreatePaymentActivity;
import com.tbs.brothersgas.haadhir.Model.ProductDO;
import com.tbs.brothersgas.haadhir.R;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;

import java.util.ArrayList;

public class ProfileAdapter extends RecyclerView.Adapter<ProfileAdapter.MyViewHolder> {

    private ArrayList<ProductDO> profileDOS;
    private Context context;

    private int lastCheckedPosition = -1;
    private boolean b = true;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public RadioButton tvName;
        private LinearLayout llDetails;

        public MyViewHolder(View view) {
            super(view);
            tvName = (RadioButton) view.findViewById(R.id.rbProfile);


        }
    }

    public ArrayList<ProductDO> getList(){
        return profileDOS;
    }


    public ProfileAdapter(Context context, ArrayList<ProductDO> profileDoS) {
        this.context = context;
        this.profileDOS = profileDoS;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.profile_data, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ProductDO profileDO = profileDOS.get(position);
        holder.tvName.setText("   " + profileDO.productName);

        holder.tvName.setTextColor(context.getColor(R.color.profile_text_color));
        holder.tvName.setChecked(false);

        if(profileDO.isSelected){
            holder.tvName.setChecked(true);
            holder.tvName.setTextColor(context.getColor(R.color.colorPrimary));
        }

       /* PreferenceUtils preferenceUtils = new PreferenceUtils(context);
        if (b) {
            String name = preferenceUtils.getStringFromPreference(PreferenceUtils.LOGIN_PROFILE_NAME, "");
            if (name.isEmpty()) {
                holder.tvName.setChecked(true);
                holder.tvName.setTextColor(context.getColor(R.color.colorPrimary));

            } else {
                if (name.equalsIgnoreCase(profileDOS.get(position).productName)) {
                    holder.tvName.setChecked(true);
                    holder.tvName.setTextColor(context.getColor(R.color.colorPrimary));

                } else {
                    if (position == lastCheckedPosition) {
                        holder.tvName.setTextColor(context.getColor(R.color.colorPrimary));
                        holder.tvName.setChecked(true);

                    } else {
                        holder.tvName.setTextColor(context.getColor(R.color.profile_text_color));
                        holder.tvName.setChecked(false);

                    }
                }
            }
        } else {

            String name = preferenceUtils.getStringFromPreference(PreferenceUtils.LOGIN_PROFILE_NAME, "");
            if (name.isEmpty()) {

                if (position == lastCheckedPosition) {
                    holder.tvName.setTextColor(context.getColor(R.color.colorPrimary));
                    holder.tvName.setChecked(true);

                } else {
                    holder.tvName.setTextColor(context.getColor(R.color.profile_text_color));
                    holder.tvName.setChecked(false);

                }
            } else {
                if (name.equalsIgnoreCase(profileDOS.get(position).productName)) {

                    if (profileDO.isSelected) {
                        holder.tvName.setChecked(true);
                        holder.tvName.setTextColor(context.getColor(R.color.colorPrimary));

                    } else {
                        holder.tvName.setChecked(false);
                        holder.tvName.setTextColor(context.getColor(R.color.profile_text_color));

                    }
                } else {
                    if (position == lastCheckedPosition) {
                        holder.tvName.setTextColor(context.getColor(R.color.colorPrimary));
                        holder.tvName.setChecked(true);

                    } else {
                        holder.tvName.setTextColor(context.getColor(R.color.profile_text_color));
                        holder.tvName.setChecked(false);

                    }

                }
            }
        }
        if(position==getItemCount()-1){
            b = false;

        }*/

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                String name = preferenceUtils.getStringFromPreference(PreferenceUtils.LOGIN_PROFILE_NAME, "");
//
//                if (name.equalsIgnoreCase(profileDOS.get(position).productName)) {
//
//                }else {
//
//                }
                for (int i = 0; i < profileDOS.size(); i++) {
                    profileDOS.get(i).isSelected =false;
                }
                profileDO.isSelected= true;


                /*lastCheckedPosition = holder.getAdapterPosition();
                holder.tvName.setTextColor(context.getColor(R.color.colorPrimary));
                holder.tvName.setChecked(true);

                preferenceUtils.saveString(PreferenceUtils.PROFILE_ID, String.valueOf(profileDOS.get(position).productId));
                preferenceUtils.saveString(PreferenceUtils.PROFILE_NAME, String.valueOf(profileDOS.get(position).productName));

                if (position == lastCheckedPosition) {
                    holder.tvName.setChecked(true);
                    for (int i = 0; i < profileDOS.size(); i++) {
                        if (i != position) {
                            holder.tvName.setChecked(false);
                        } else {
                            holder.tvName.setChecked(true);

                        }
                    }
                } else {
                    holder.tvName.setChecked(false);
                }*/
                notifyDataSetChanged();

            }
        });
    }

    @Override
    public int getItemCount() {
        return profileDOS.size();
    }

}
