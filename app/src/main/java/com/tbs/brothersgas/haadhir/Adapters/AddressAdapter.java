package com.tbs.brothersgas.haadhir.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tbs.brothersgas.haadhir.Activitys.ActiveDeliveryActivity;
import com.tbs.brothersgas.haadhir.Activitys.AddressListActivity;
import com.tbs.brothersgas.haadhir.Activitys.BaseActivity;
import com.tbs.brothersgas.haadhir.Activitys.CustomerDetailsActivity;
import com.tbs.brothersgas.haadhir.Activitys.VehicleRoutingList;
import com.tbs.brothersgas.haadhir.Model.AddressDO;
import com.tbs.brothersgas.haadhir.Model.CustomerDo;
import com.tbs.brothersgas.haadhir.R;
import com.tbs.brothersgas.haadhir.common.AppConstants;
import com.tbs.brothersgas.haadhir.database.StorageManager;
import com.tbs.brothersgas.haadhir.decanting.DecantingProductDetailsActivity;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;

import java.util.ArrayList;

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.MyViewHolder> {
    int count = 0;
    private ArrayList<AddressDO> addressMainDos;
    private String codE, customeR;
    private Context context;
    private CustomerDo customerDO;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvInvoiceNumber;
        public RelativeLayout rlRemove, rlAdd;
        public ImageView ivRemove, ivAdd;

        public MyViewHolder(View view) {
            super(view);
            tvInvoiceNumber = (TextView) view.findViewById(R.id.tvInvoiceNumber);

        }
    }


    public AddressAdapter(Context context, ArrayList<AddressDO> invoiceDOS, String code, String customer,CustomerDo customerDo) {
        this.context = context;
        this.addressMainDos = invoiceDOS;
        this.codE = code;
        this.customeR = customer;
        this.customerDO = customerDo;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.invoice_data, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final AddressDO addressDO = addressMainDos.get(position);
        if (codE.equals("DECANTING")) {
            holder.tvInvoiceNumber.setText("Address Code : " + addressDO.addressCode + "\n" +
                    "Description      : " + addressDO.addressDescription );
        }else {

            holder.tvInvoiceNumber.setText("Address Code : " + addressDO.addressCode + "\n" +
                    "Description      : " + addressDO.addressDescription + "\n" +
                    "City                   : " + addressDO.city
                    + "\n" + "Emirates          : " + addressDO.emirates);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (codE.equals("MASTER")) {
                    ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.PRODUCT_CODE, addressDO.addressCode);
                    ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.PRODUCT_DESCRIPTION, addressDO.addressDescription);
                    Intent intent = new Intent(context, CustomerDetailsActivity.class);
                    intent.putExtra("CODE", customeR);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(intent);
                }else if (codE.equals("DECANTING")) {
                    ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.PRODUCT_CODE, addressDO.addressCode);
                    ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.PRODUCT_DESCRIPTION, addressDO.addressDescription);
                    Intent intent = new Intent(context, DecantingProductDetailsActivity.class);
                    intent.putExtra("CODE", customeR);
                    intent.putExtra("customerDO", customerDO);
                    intent.putExtra("addressDO", addressDO);

                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    ((BaseActivity) context).startActivityForResult(intent,1);
                } else {
//                    addressDO.lattitude=0.0;
//                    if(addressDO.lattitude==0.0){
//                        Intent intent = new Intent(context, CustomerDetailsActivity.class);
//                        intent.putExtra("CODE", customeR);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                        context.startActivity(intent);
//                    }else {
//                        ((BaseActivity) context).  preferenceUtils.saveDouble(PreferenceUtils.LATTITUDE, addressDO.lattitude);
//                        ((BaseActivity) context).  preferenceUtils.saveDouble(PreferenceUtils.LONGITUDE, addressDO.longitude);
//                        CustomerDo custDo = StorageManager.getInstance(context).getCurrentSpotSalesCustomer(context);
//                        if (custDo != null && !custDo.customerId.equalsIgnoreCase("")) {
//                            if (custDo.customerId.equalsIgnoreCase(customeR)) {
//
//                                Intent intent = new Intent(context, ActiveDeliveryActivity.class);
//                                ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.ProductsType, AppConstants.FixedQuantityProduct);
//                                ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.ShipmentType, context.getResources().getString(R.string.checkin_non_scheduled));
//                                ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.PRODUCT_CODE, addressDO.addressCode);
//                                ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.PRODUCT_DESCRIPTION, addressDO.addressDescription);
//                                ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.PAYMENT_TERM, "" + custDo.paymentTerm);
//                                ((BaseActivity) context).startActivityForResult(intent, 63);
//                            } else {
//                                ((BaseActivity) context).showAppCompatAlert("", "Please process the current customer " + custDo.customerId + "'s non schedule shipment", "OK", "", "", false);
//                            }
//                        } else {
//
//                            StorageManager.getInstance(context).deleteActiveDeliveryMainDo(((BaseActivity) context));
//                            StorageManager.getInstance(context).deleteDepartureData(((BaseActivity) context));
//                            ((BaseActivity) context).preferenceUtils.removeFromPreference(PreferenceUtils.CustomerId);
//                            StorageManager.getInstance(context).deleteCurrentSpotSalesCustomer(((BaseActivity) context));
//                            StorageManager.getInstance(context).deleteCurrentDeliveryItems(((BaseActivity) context));
//                            StorageManager.getInstance(context).deleteReturnCylinders(((BaseActivity) context));
//                            if (StorageManager.getInstance(context).saveCurrentSpotSalesCustomer(((BaseActivity) context), customerDO)) {
//                                Intent intent = new Intent(context, ActiveDeliveryActivity.class);
//                                ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.ProductsType, AppConstants.FixedQuantityProduct);
//                                ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.ShipmentType, context.getResources().getString(R.string.checkin_non_scheduled));
//                                ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.PRODUCT_CODE, addressDO.addressCode);
//                                ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.PRODUCT_DESCRIPTION, addressDO.addressDescription);
//                                ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.PAYMENT_TERM, "" + custDo.paymentTerm);
//                                ((BaseActivity) context).preferenceUtils.saveDouble(PreferenceUtils.CREDIT_AMOUNT, addressDO.creditAmount);
//                                ((BaseActivity) context).startActivityForResult(intent, 63);
//                            }
//                        }
//
//
//                    }

                    ((BaseActivity) context).  preferenceUtils.saveDouble(PreferenceUtils.LATTITUDE, addressDO.lattitude);
                    ((BaseActivity) context).  preferenceUtils.saveDouble(PreferenceUtils.LONGITUDE, addressDO.longitude);
                    CustomerDo custDo = StorageManager.getInstance(context).getCurrentSpotSalesCustomer(context);
                    if (custDo != null && !custDo.customerId.equalsIgnoreCase("")) {
                        if (custDo.customerId.equalsIgnoreCase(customeR)) {

                            Intent intent = new Intent(context, ActiveDeliveryActivity.class);
                            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.ProductsType, AppConstants.FixedQuantityProduct);
                            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.ShipmentType, context.getResources().getString(R.string.checkin_non_scheduled));
                            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.PRODUCT_CODE, addressDO.addressCode);
                            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.PRODUCT_DESCRIPTION, addressDO.addressDescription);
                            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.PAYMENT_TERM, "" + custDo.paymentTerm);
                            ((BaseActivity) context).startActivityForResult(intent, 63);
                        } else {
                            ((BaseActivity) context).showAppCompatAlert("", "Please process the current customer " + custDo.customerId + "'s non schedule shipment", "OK", "", "", false);
                        }
                    } else {

                        StorageManager.getInstance(context).deleteActiveDeliveryMainDo(((BaseActivity) context));
                        StorageManager.getInstance(context).deleteDepartureData(((BaseActivity) context));
                        ((BaseActivity) context).preferenceUtils.removeFromPreference(PreferenceUtils.CustomerId);
                        StorageManager.getInstance(context).deleteCurrentSpotSalesCustomer(((BaseActivity) context));
                        StorageManager.getInstance(context).deleteCurrentDeliveryItems(((BaseActivity) context));
                        StorageManager.getInstance(context).deleteReturnCylinders(((BaseActivity) context));
                        if (StorageManager.getInstance(context).saveCurrentSpotSalesCustomer(((BaseActivity) context), customerDO)) {
                            Intent intent = new Intent(context, ActiveDeliveryActivity.class);
                            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.ProductsType, AppConstants.FixedQuantityProduct);
                            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.ShipmentType, context.getResources().getString(R.string.checkin_non_scheduled));
                            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.PRODUCT_CODE, addressDO.addressCode);
                            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.PRODUCT_DESCRIPTION, addressDO.addressDescription);
                            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.PAYMENT_TERM, "" + custDo.paymentTerm);
                            ((BaseActivity) context).preferenceUtils.saveDouble(PreferenceUtils.CREDIT_AMOUNT, addressDO.creditAmount);
                            ((BaseActivity) context).startActivityForResult(intent, 63);
                        }
                    }

                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return addressMainDos != null ? addressMainDos.size() : 0;
    }

}

