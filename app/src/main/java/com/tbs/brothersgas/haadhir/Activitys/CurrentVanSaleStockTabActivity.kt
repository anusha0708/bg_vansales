package com.tbs.brothersgas.haadhir.Activitys


import com.google.android.material.tabs.TabLayout
import androidx.viewpager.widget.ViewPager
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.brothersgas.haadhir.Adapters.LoadVanSaleStockPagerAdapter
import com.tbs.brothersgas.haadhir.Model.LoadStockDO
import com.tbs.brothersgas.haadhir.Model.LoanReturnDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.*
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.utils.CalendarUtils
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import java.util.*
import kotlin.collections.ArrayList

class CurrentVanSaleStockTabActivity : BaseActivity() {
    private var rlPartyActivity: LinearLayout? = null
    lateinit var adapter: LoadVanSaleStockPagerAdapter
    lateinit var viewPager: ViewPager
    lateinit var btnLoadVehicle: Button
    lateinit var tabLayout: TabLayout
    lateinit var tvCapacity: TextView
    lateinit var tvScheduledStock: TextView
    lateinit var tvNonScheduledStock: TextView
    lateinit var tvBalanceStock: TextView
    lateinit var tvReturnStock: TextView
    lateinit var llReturnStock: LinearLayout

    private var scheduledRootId : String = ""
    private var nonScheduledRootId : String = ""

    //    lateinit var loadStockMainDO : LoadStockMainDO
    var flag:Int = 0
    private var scheduleDos: ArrayList<LoadStockDO> = ArrayList();
    private var nonScheduleDos: ArrayList<LoadStockDO> = ArrayList();

    override fun initialize() {
        rlPartyActivity = layoutInflater.inflate(R.layout.load_vansales_tab, null) as LinearLayout
        llBody.addView(rlPartyActivity, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        disableMenuWithBackButton()
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
        initializeControls()
        val vehicleCheckInDo = StorageManager.getInstance(this@CurrentVanSaleStockTabActivity).getVehicleCheckInData(this@CurrentVanSaleStockTabActivity);
        scheduledRootId = vehicleCheckInDo.scheduledRootId!!
        nonScheduledRootId = vehicleCheckInDo.nonScheduledRootId!!
         nonScheduledRootId = "" + preferenceUtils.getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, "")

        if(intent.hasExtra("ScreenTitle")){
            tvScreenTitle.setText(intent.extras.getString("ScreenTitle"))

        }
        else{
            tvScreenTitle.text = "Load Van Sales Stock";
        }
//        tabLayout.addTab(tabLayout.newTab().setText("Available Stock"))
        tabLayout.addTab(tabLayout.newTab().setText("Scheduled Stock"))
        tabLayout.addTab(tabLayout.newTab().setText("Non-Scheduled Stock"))

        tabLayout.tabGravity = TabLayout.GRAVITY_FILL
        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })
        if (intent.hasExtra("FLAG")) {
            flag = intent.extras.getInt("FLAG")
        }
        if(flag == 1){
            btnLoadVehicle.setVisibility(View.INVISIBLE)
        }
        else  if(flag == 2){
            btnLoadVehicle.setVisibility(View.VISIBLE)
            llReturnStock.setVisibility(View.GONE)
            btnLoadVehicle.setText("UnLoad Vehicle")

        }
//        else  if(flag == 11){
//            btnLoadVehicle.setVisibility(View.VISIBLE)
//            llReturnStock.setVisibility(View.VISIBLE)
//            btnLoadVehicle.setText("Unload Vehicle")
//
//        }
//        else {
//            btnLoadVehicle.setVisibility(View.VISIBLE)
//        }
//        val vehicleCheckInDo = StorageManager.getInstance(this@CurrentVanSaleStockTabActivity).getVehicleCheckInData(this@CurrentVanSaleStockTabActivity);
//        scheduledRootId = vehicleCheckInDo.scheduledRootId!!
//        nonScheduledRootId = vehicleCheckInDo.nonScheduledRootId!!
//
//        if(!scheduledRootId.equals("", true)){
//            scheduleDos = StorageManager.getInstance(this).getVehicleScheduleData();
//        }
//        if(!nonScheduledRootId.equals("", true)){
//            nonScheduleDos = StorageManager.getInstance(this).getVehicleNonScheduleData();
//        }
//        var scheduleTotal:Double = 0.0;
//        var nonScheduleTotal:Double = 0.0;
//        if(scheduleDos != null && scheduleDos!!.size>0){
//            for (j in scheduleDos!!.indices) {
//                scheduleTotal = scheduleTotal+(scheduleDos!!.get(j).quantity * scheduleDos!!.get(j).productWeight)
//            }
//        }
//        if(nonScheduleDos != null && nonScheduleDos!!.size>0){
//            for (j in nonScheduleDos!!.indices) {
//                nonScheduleTotal = nonScheduleTotal+(nonScheduleDos!!.get(j).quantity * nonScheduleDos!!.get(j).productWeight)
//            }
//        }
//        var totalCapacity = preferenceUtils.getStringFromPreference(PreferenceUtils.TOTAL_VEHICLE_CAPACITY, "")
//        var unit = preferenceUtils.getStringFromPreference(PreferenceUtils.UNIT_CAPACITY, "")
//        val loanReturnDOS = StorageManager.getInstance(this).getAllReturnStockValue()
//        var returnStock = getReturnProductsCount(loanReturnDOS)
//
//        tvCapacity.setText(totalCapacity +" "+ unit)
//        tvScheduledStock.setText(""+scheduleTotal +" "+ unit)
//        tvNonScheduledStock.setText(""+nonScheduleTotal+" "+ unit)
//        tvReturnStock.setText(""+returnStock)//need to get it from database of returned stock
//
//        tvBalanceStock.setText(""+(scheduleTotal+nonScheduleTotal+returnStock)+" "+ unit)


            loadVehicleStockData()
            loadNonScheduledVehicleStockData()

        btnLoadVehicle.setOnClickListener {
            showAppCompatAlert("", "Are You Sure You Want to confirm Unload?", "OK", "Cancel", "SUCCESS", true)

        }
    }


    override fun initializeControls() {
        tabLayout               = findViewById<View>(R.id.tab_layout) as TabLayout
        viewPager               = findViewById<View>(R.id.pager) as ViewPager
        btnLoadVehicle          = findViewById<Button>(R.id.btnLoadVehicle) as Button
        tvCapacity              = findViewById<View>(R.id.tvCapacity) as TextView
        tvScheduledStock        = findViewById<View>(R.id.tvScheduledStock) as TextView
        tvNonScheduledStock     = findViewById<View>(R.id.tvNonScheduledStock) as TextView
        tvBalanceStock          = findViewById<View>(R.id.tvBalanceStock) as TextView
        tvReturnStock          = findViewById<View>(R.id.tvReturnStock) as TextView
        llReturnStock          = findViewById<View>(R.id.llReturnedStock) as LinearLayout
        llReturnStock.setVisibility(View.GONE)

    }

    private fun loadVehicleStockData(){
        if(Util.isNetworkAvailable(this)) {
//            if(!scheduledRootId.equals("", true)){
                val loadVanSaleRequest = CurrentScheduledStockRequest(nonScheduledRootId, this@CurrentVanSaleStockTabActivity)
                  showLoader()
                loadVanSaleRequest.setOnResultListener { isError, loadStockMainDo ->
                    hideLoader()
                    if (isError) {
                        showToast("No vehicle data found.")
                    }
                    else {
                        hideLoader()

                        scheduleDos = loadStockMainDo.loadStockDOS
//                        StorageManager.getInstance(this).saveScheduledVehicleStockInLocal(scheduleDos!!)
//                        preferenceUtils.saveString(PreferenceUtils.VEHICLE_CAPACITY,loadStockMainDo.vehicleCapacity+" "+loadStockMainDo.vehicleCapacityUnit)
//                        preferenceUtils.saveString(PreferenceUtils.TOTAL_VEHICLE_CAPACITY, loadStockMainDo.vehicleCapacity)
//                        preferenceUtils.saveString(PreferenceUtils.UNIT_CAPACITY, loadStockMainDo.vehicleCapacityUnit)
//                        preferenceUtils.saveString(PreferenceUtils.TOTAL_SCHEDULED_STOCK, loadStockMainDo.totalScheduledStock)
//                        var unit =preferenceUtils.getStringFromPreference(PreferenceUtils.UNIT_CAPACITY, "")
//
//                        tvCapacity.setText(""+loadStockMainDo.vehicleCapacity+" "+unit)
//                        tvScheduledStock.setText(""+loadStockMainDo.totalScheduledStock+" "+loadStockMainDo.vehicleCapacityUnit)
//                        tvCapacity.setText(""+preferenceUtils.getStringFromPreference(PreferenceUtils.TOTAL_VEHICLE_CAPACITY, ""+" "+unit))
//                        tvScheduledStock.setText(""+loadStockMainDo.totalScheduledStock+" "+loadStockMainDo.vehicleCapacityUnit)
//
//                        // tvNonScheduledStock.setText("Non Scheduled Stock : "+loadStockMainDo.totalNonScheduledStock +" "+loadStockMainDo.vehicleCapacityUnit)
//                        var scheduleTotal = 0;
//                        var nonScheduleTotal = 0;
//                        var scheduleStock =loadStockMainDo.totalScheduledStock
//                        //  var nonScheduleStock =loadStockMainDo.totalNonScheduledStock
//
//                        var total = 0
//
//                        total=loadStockMainDo.vehicleCapacity.toInt()
//                        scheduleTotal=scheduleStock.toInt()
//                        // nonScheduleTotal=nonScheduleStock.toInt()
//                        tvBalanceStock.setText(""+(total-scheduleTotal-nonScheduleTotal)+" "+ loadStockMainDo.vehicleCapacityUnit)

                    }
//                    loadNonScheduledVehicleStockData()
                }
                loadVanSaleRequest.execute()
//            }
//            else{
//                loadNonScheduledVehicleStockData()
//            }
        }
        else {
            showToast("No Internet connection, please try again later")
        }
    }
    private fun loadNonScheduledVehicleStockData(){
        if(Util.isNetworkAvailable(this)){
//            if(!nonScheduledRootId.equals("", true)){
                val loadVanSaleRequest = CurrentNonScheduledStockRequest(nonScheduledRootId, this@CurrentVanSaleStockTabActivity)
//            showLoader()
                loadVanSaleRequest.setOnResultListener { isError, loadStockMainDo ->
                    hideLoader()
                    if (isError) {
                        showToast("No vehicle data found.")
                    }
                    else {
                        hideLoader()

                        nonScheduleDos = loadStockMainDo.loadStockDOS;
//                        StorageManager.getInstance(this).saveNonScheduledVehicleStockInLocal(nonScheduleDos!!)
                        adapter = LoadVanSaleStockPagerAdapter(this, scheduleDos, nonScheduleDos, supportFragmentManager)
                        viewPager.adapter = adapter
//                        var unit =preferenceUtils.getStringFromPreference(PreferenceUtils.UNIT_CAPACITY, "")
//
//                        tvCapacity.setText(""+preferenceUtils.getStringFromPreference(PreferenceUtils.TOTAL_VEHICLE_CAPACITY, ""+" "+unit))
//
//                        //  tvNonScheduledStock.setText("Non Scheduled Stock : "+loadStockMainDo.totalNonScheduledStock +" "+loadStockMainDo.vehicleCapacityUnit)
//                        var scheduleTotal = 0;
//                        var nonScheduleTotal = 0;
//
//                        var scheduleStock =preferenceUtils.getStringFromPreference(PreferenceUtils.TOTAL_SCHEDULED_STOCK, "")
//                        var nonScheduleStock =loadStockMainDo.totalNonScheduledStock
//                        tvScheduledStock.setText(""+scheduleStock+" "+loadStockMainDo.vehicleCapacityUnit)
//                        tvNonScheduledStock.setText(""+nonScheduleStock+" "+loadStockMainDo.vehicleCapacityUnit)
//
//                        var total = 0
//                        var  capacity=preferenceUtils.getStringFromPreference(PreferenceUtils.TOTAL_VEHICLE_CAPACITY,"")
//                        total=capacity.toInt()
//                        scheduleTotal=scheduleStock.toInt()
//
//                        nonScheduleTotal=nonScheduleStock.toInt()
//                        tvBalanceStock.setText(""+(total-scheduleTotal-nonScheduleTotal)+" "+ loadStockMainDo.vehicleCapacityUnit)

                    }
                }
                loadVanSaleRequest.execute()
//            }
//            else{
//                tvNonScheduledStock.setText("0")
//                adapter = LoadVanSaleStockPagerAdapter(this,  scheduleDos, nonScheduleDos, supportFragmentManager)
//                viewPager.adapter = adapter
//            }
        }
        else{
            showToast("No Internet connection, please try again later")
        }
    }
    override fun onButtonNoClick(from: String) {

        if ("SUCCESS".equals(from, ignoreCase = true)) {
//            finish()

        }
    }

    override fun onButtonYesClick(from: String) {

        if ("SUCCESS".equals(from, ignoreCase = true)) {
            var  vehicleCheckInDo = StorageManager.getInstance(this).getVehicleCheckInData(this)
//            var loadStockScheduledDOs = StorageManager.getInstance(this).getVehicleScheduleData()//
//            var loadStockNonScheduledDOs = StorageManager.getInstance(this).getVehicleNonScheduleData()//
            var loadStockScheduledDOs =scheduleDos
            var loadStockNonScheduledDOs = nonScheduleDos//
            var loanReturnDOsMap: LinkedHashMap<String, ArrayList<LoadStockDO>> = LinkedHashMap()

            vehicleCheckInDo.loadStock = resources.getString(R.string.unloaded_stock)
            vehicleCheckInDo.checkOutTimeCaptureStockLoadingTime= CalendarUtils.getTime()
            vehicleCheckInDo.checkOutTimeCaptureStockLoadingDate= CalendarUtils.getDate()
            StorageManager.getInstance(this).insertCheckOutData(this,vehicleCheckInDo)

            if(loadStockNonScheduledDOs.size>0){
                val returnList = ArrayList<LoadStockDO>()
                returnList.addAll(loadStockNonScheduledDOs)
//                for (j in loadStockNonScheduledDOs.indices){
//                }
                loanReturnDOsMap.put("loadStockNonScheduledDOs", returnList);
            }
//            if(loadStockScheduledDOs.size>0){
//                val returnList = ArrayList<LoadStockDO>()
//                returnList.addAll(loadStockScheduledDOs)
////                for (j in loadStockScheduledDOs.indices){
////                }
//                loanReturnDOsMap.put("loadStockScheduledDOs", returnList);
//            }

            // directly you can use arraylist no need of hashmap
            val loadVanSaleRequest = UnLoadStockRequest("","","",loanReturnDOsMap, this@CurrentVanSaleStockTabActivity)
              showLoader();
            loadVanSaleRequest.setOnResultListener { isError, loadStockMainDo ->
                hideLoader()

                if (isError) {
                    Toast.makeText(this@CurrentVanSaleStockTabActivity, "Server Error..please try again later", Toast.LENGTH_SHORT).show()
                } else {
                    if(loadStockMainDo.status==1){
                        refreshMenuAdapter()
                        setResult(4, null)
                        finish()
                        showToast("Unloaded Successfully")
//
                    }else{
                        showToast("Server Error..please try again later")

                    }
                }
            }
            loadVanSaleRequest.execute()


        } else
            if ("FAILURE".equals(from, ignoreCase = true)) {


                //finish()
            }

    }
}