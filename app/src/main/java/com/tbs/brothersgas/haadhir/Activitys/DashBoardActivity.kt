package com.tbs.brothersgas.haadhir.Activitys

import android.annotation.TargetApi
import android.app.IntentService
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Build
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.brothersgas.haadhir.Adapters.*
import com.tbs.brothersgas.haadhir.Model.PickUpDo
import com.tbs.brothersgas.haadhir.Model.UserActivityReportMainDO
import com.tbs.brothersgas.haadhir.Model.VehicleCheckInDo
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.CheckoutTimeCaptureRequest
import com.tbs.brothersgas.haadhir.Requests.CustomerNewRequest
import com.tbs.brothersgas.haadhir.Requests.DisplayPickupListRequest
import com.tbs.brothersgas.haadhir.Requests.UserActivityReportRequest
import com.tbs.brothersgas.haadhir.collector.CollectorCustomerActivity
import com.tbs.brothersgas.haadhir.common.AppConstants
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.history.CustomerActivity
import com.tbs.brothersgas.haadhir.prints.PrinterConnection
import com.tbs.brothersgas.haadhir.prints.PrinterConstants
import com.tbs.brothersgas.haadhir.utils.CalendarUtils
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import com.tbs.brothersgas.pod.Activitys.MasterDataActivity
import com.tbs.brothersgas.pod.Activitys.ScheduledSalesActivity
import com.tbs.brothersgas.pod.Activitys.SpotSalesActivity
import com.tbs.brothersgas.pod.Activitys.StartDayActivity
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class DashBoardActivity : BaseActivity() {
    var REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 200
    lateinit var vehicleCheckInDo: VehicleCheckInDo
    private var pickUpDos: ArrayList<PickUpDo> = ArrayList()
    var isDepartureExisted = false;
    private var userActivityReportMainDO: UserActivityReportMainDO = UserActivityReportMainDO()


    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.vendor_dashboard_layout, null) as ScrollView
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()
        insertDummyContactWrapper()
    }

    @TargetApi(Build.VERSION_CODES.M)
    private fun insertDummyContactWrapper() {
        val permissionsNeeded = ArrayList<String>()
        val permissionsList = ArrayList<String>()
        if (!addPermission(permissionsList, android.Manifest.permission.CAMERA))
            permissionsNeeded.add("Camera")
        if (!addPermission(permissionsList, android.Manifest.permission.ACCESS_FINE_LOCATION))
            permissionsNeeded.add("Location")
        if (!addPermission(permissionsList, android.Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Storage")
        if (permissionsList.size > 0) {
            requestPermissions(permissionsList.toArray(arrayOfNulls<String>(permissionsList.size)),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS)
            return
        }
    }

    private fun addPermission(permissionsList: MutableList<String>, permission: String): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission)
                // Check for Rationale Option
                if (!shouldShowRequestPermissionRationale(permission))
                    return false
            }
        }
        return true
    }

    override fun initializeControls() {
        //ivBack.visibility = View.GONE
        ivMenu.visibility = View.VISIBLE
        ivPic!!.visibility = View.VISIBLE
        preferenceUtils = PreferenceUtils(this)

        tvScreenTitle.setText("" + preferenceUtils.getStringFromPreference(PreferenceUtils.LOGIN_DRIVER_NAME, ""))
        vehicleCheckInDo = StorageManager.getInstance(this).getVehicleCheckOutData(this)
        //  disableMenuWithBackButton()
        if (vehicleCheckInDo != null) {

        } else {
            vehicleCheckInDo = VehicleCheckInDo()

        }

        var llStartDay = findViewById<View>(R.id.llStartDay) as LinearLayout
        var llScheduledSales = findViewById<View>(R.id.llScheduledSales) as LinearLayout
        var llSpotSales = findViewById<View>(R.id.llSpotSales) as LinearLayout
        var llCheckOut = findViewById<View>(R.id.llCheckOut) as LinearLayout
//        var llPaymentReciept = findViewById<View>(R.id.llPaymentReciept) as LinearLayout
        var llDocReciept = findViewById<View>(R.id.llDocReciept) as LinearLayout

        var llMasterData = findViewById<View>(R.id.llMasterData) as LinearLayout

        llStartDay.setOnClickListener {
            Log.d("Site_ids", preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID, "") + "---" + preferenceUtils.getStringFromPreference(PreferenceUtils.MAIN_SIIDE_ID, ""));
            preferenceUtils.saveString(PreferenceUtils.B_SITE_ID, preferenceUtils.getStringFromPreference(PreferenceUtils.MAIN_SIIDE_ID, ""))
            val intent = Intent(this@DashBoardActivity, StartDayActivity::class.java)
            startActivity(intent)
        }

        llScheduledSales.setOnClickListener {
            Log.d("Site_ids", preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID, "") + "---" + preferenceUtils.getStringFromPreference(PreferenceUtils.MAIN_SIIDE_ID, ""));

            preferenceUtils.saveString(PreferenceUtils.B_SITE_ID, preferenceUtils.getStringFromPreference(PreferenceUtils.MAIN_SIIDE_ID, ""))
            val intent = Intent(this@DashBoardActivity, ScheduledSalesActivity::class.java)
            startActivity(intent)
        }
        llSpotSales.setOnClickListener {
            Log.d("Site_ids", preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID, "") + "---" + preferenceUtils.getStringFromPreference(PreferenceUtils.MAIN_SIIDE_ID, ""));

            preferenceUtils.saveString(PreferenceUtils.B_SITE_ID, preferenceUtils.getStringFromPreference(PreferenceUtils.MAIN_SIIDE_ID, ""))
            val intent = Intent(this@DashBoardActivity, SpotSalesActivity::class.java)
            startActivity(intent)
        }
        llMasterData.setOnClickListener {
            val intent = Intent(this@DashBoardActivity, MasterDataActivity::class.java)
            startActivity(intent)
        }
//        llPaymentReciept.setOnClickListener {
//            if (isExpired()) {
//                showAppCompatAlert("", "You are not authorized to use this function", "Ok", "", "Expired", false);
//            } else {
//                val intent = Intent(this@DashBoardActivity, CollectorCustomerActivity::class.java)
//                startActivity(intent)
//            }
//
////            val intent = Intent(this@DashBoardActivity, CollectorCustomerActivity::class.java)
////            startActivity(intent)
//        }

        llDocReciept.setOnClickListener {
            var flag = preferenceUtils.getIntFromPreference(PreferenceUtils.USER_AUTHORIZED, 0)
            if (flag == 20) {
                val intent = Intent(this@DashBoardActivity, CustomerActivity::class.java)
                startActivity(intent)
            } else {
                showAppCompatAlert("Info!", "You are not authorized to use this function", "OK", "", "FAILURE", false)

            }

        }

        llCheckOut.setOnClickListener {

            //            showAppCompatAlert("", "Are You Sure You Want to \n do Checkout?", "OK", "Cancel", "CHECKOUT", true)
            val vehicleCheckInDo = StorageManager.getInstance(this@DashBoardActivity).getVehicleCheckInData(this@DashBoardActivity);
            if (!vehicleCheckInDo.checkInStatus!!.isEmpty()) {
                var shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
                if (shipmentId == null || shipmentId.equals("")) {
                    val customerDo = StorageManager.getInstance(this@DashBoardActivity).getCurrentSpotSalesCustomer(this@DashBoardActivity)
                    if (customerDo.customerId.equals("")) {
                        loadData()
//                        val intent = Intent(this@DashBoardActivity, CheckOutScreen::class.java)
//                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//                        startActivity(intent)
                    } else {
                        showAppCompatAlert("", "Please process the current customer " + customerDo.customerId + "'s non schedule shipment", "OK", "", "FAILURE", false)
                    }

                } else {
                    showToast("Please process the current schedule " + shipmentId + " shipment")
                }


            } else {
                showAppCompatAlert("Alert!", "Please Finish Check-in Process", "OK", "", "FAILURE", false)
            }
        }


    }

    private fun isExpired(): Boolean {
        try {
            val dateFormat = "dd/MM/yyyy"
            val expireDate = "01/08/2019"
            val sdf = SimpleDateFormat(dateFormat, Locale.getDefault()) // 28-01-2019
            val today = getToday(dateFormat)
            val eDate = sdf.parse(expireDate)
            val curDate = sdf.parse(today)
            if (eDate.compareTo(curDate) >= 0) {
                return true;
            }

//            val sdf = SimpleDateFormat("dd/MM/yyyy")
//            val sdf2 = SimpleDateFormat("yyyyMMdd")
//
//            var date =CalendarUtils.getDate()
//            var currentDate = sdf.parse(date)
//            val strDate = sdf.parse(expireDate)
//            if (currentDate.time< strDate.time) {
//                return true
//            } else {
//                return false
//            }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e("DashBoardActivity", "ExpireDate() : " + e.message);
        }
        return false
    }

    private fun getToday(format: String): String {
        val date = Date()
        return SimpleDateFormat(format).format(date)
    }

    override fun onResume() {
        pairedDevices()

        super.onResume()
    }

    override fun onButtonYesClick(from: String) {
        if (from.equals("CheckInSuccess")) {
            vehicleCheckInDo.checkOutTimeCapturecheckOutTime = CalendarUtils.getTime()
            vehicleCheckInDo.checkOutTimeCapturcheckOutDate = CalendarUtils.getDate()
            checkOutTimeCapture()
        } else {
            hideLoader()
        }

        super.onButtonYesClick(from)
    }

    private fun clearDataLocalData() {
        preferenceUtils.removeFromPreference(PreferenceUtils.CHECKIN_DATE)
        preferenceUtils.removeFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.Non_Scheduled_Route_Id)
        preferenceUtils.removeFromPreference(PreferenceUtils.CUSTOMER)
        preferenceUtils.removeFromPreference(PreferenceUtils.LATTITUDE)
        preferenceUtils.removeFromPreference(PreferenceUtils.LONGITUDE)
        preferenceUtils.removeFromPreference(PreferenceUtils.VEHICLE_CHECKED_ROUTE_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.CARRIER_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.NOTES)
        preferenceUtils.removeFromPreference(PreferenceUtils.PREVIEW_INVOICE_ID);
        preferenceUtils.removeFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER);
        preferenceUtils.removeFromPreference(PreferenceUtils.PAYMENT_ID);
        preferenceUtils.removeFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.INVOICE_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.SHIPMENT)
        preferenceUtils.removeFromPreference(PreferenceUtils.SITE_NAME)
        preferenceUtils.removeFromPreference(PreferenceUtils.VOLUME)
        preferenceUtils.removeFromPreference(PreferenceUtils.WEIGHT)
//        preferenceUtils.removeFromPreference(PreferenceUtils.CV_PLATE)
        preferenceUtils.removeFromPreference(PreferenceUtils.CA_DATE)
        preferenceUtils.removeFromPreference(PreferenceUtils.CA_TIME)
        preferenceUtils.removeFromPreference(PreferenceUtils.CD_DATE)
        preferenceUtils.removeFromPreference(PreferenceUtils.CVEHICLE_CODE)
        preferenceUtils.removeFromPreference(PreferenceUtils.CD_TIME)
        preferenceUtils.removeFromPreference(PreferenceUtils.CN_SHIPMENTS)
        preferenceUtils.removeFromPreference(PreferenceUtils.CHECK_IN_STATUS)
        preferenceUtils.removeFromPreference(PreferenceUtils.TOTAL_LOAD)
        preferenceUtils.removeFromPreference(PreferenceUtils.VEHICLE_CAPACITY)
        preferenceUtils.removeFromPreference(PreferenceUtils.SELECTION)
//        preferenceUtils.removeFromPreference(PreferenceUtils.Non_Scheduled_Route_Id)
        preferenceUtils.removeFromPreference(PreferenceUtils.TOTAL_NON_SCHEDULED_STOCK)
        preferenceUtils.removeFromPreference(PreferenceUtils.DAMAGE_REASON)
        preferenceUtils.removeFromPreference(PreferenceUtils.DAMAGE_REASON_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.EMIRATES_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.COMMENT_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.PRICE_TAG)


//          preferenceUtils.removeFromPreference(PreferenceUtils.CONFIG_SUCCESS                )
        preferenceUtils.removeFromPreference(PreferenceUtils.CAPTURE)
        preferenceUtils.removeFromPreference(PreferenceUtils.DAMAGE)
        preferenceUtils.removeFromPreference(PreferenceUtils.STOCK_COUNT)
        preferenceUtils.removeFromPreference(PreferenceUtils.BANK_REASON)
        preferenceUtils.removeFromPreference(PreferenceUtils.WEIGHT)
        preferenceUtils.removeFromPreference(PreferenceUtils.VOLUME)
        preferenceUtils.removeFromPreference(PreferenceUtils.VEHICLE_CAPACITY)
        preferenceUtils.removeFromPreference(PreferenceUtils.TOTAL_LOAD)
        preferenceUtils.removeFromPreference(PreferenceUtils.CUSTOMER)
        preferenceUtils.removeFromPreference(PreferenceUtils.CUSTOMER_NAME)
//          preferenceUtils.removeFromPreference(PreferenceUtils.SITE_NAME)
        preferenceUtils.removeFromPreference(PreferenceUtils.TOTAL_NON_SCHEDULED_STOCK)
        preferenceUtils.removeFromPreference(PreferenceUtils.CREDIT_INVOICE_AMOUNT)
        preferenceUtils.removeFromPreference(PreferenceUtils.SELECTION_INVOICE_ID)
//          preferenceUtils.removeFromPreference(PreferenceUtils.SHIPMENT_ID)
//          preferenceUtils.removeFromPreference(PreferenceUtils.USER_NAME)
//          preferenceUtils.removeFromPreference(PreferenceUtils.PASSWORD )
//          preferenceUtils.removeFromPreference(PreferenceUtils.A_USER_NAME                   )
//          preferenceUtils.removeFromPreference(PreferenceUtils.A_PASSWORD                    )
//          preferenceUtils.removeFromPreference(PreferenceUtils.IP_ADDRESS                    )
//        preferenceUtils.removeFromPreference(PreferenceUtils.PORT                          )
//        preferenceUtils.removeFromPreference(PreferenceUtils.ALIAS                         )
        preferenceUtils.removeFromPreference(PreferenceUtils.MESSAGE_SPOT)
        preferenceUtils.removeFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID)
//          preferenceUtils.removeFromPreference(PreferenceUtils.PROFILE_PICTURE)
        preferenceUtils.removeFromPreference(PreferenceUtils.VEHICLE_CHECKED_ROUTE_ID)
//          preferenceUtils.removeFromPreference(PreferenceUtils.SITE_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.SHIPMENT)
        preferenceUtils.removeFromPreference(PreferenceUtils.ShipmentType)
        preferenceUtils.removeFromPreference(PreferenceUtils.ProductsType)
        preferenceUtils.removeFromPreference(PreferenceUtils.CustomerId)
        preferenceUtils.removeFromPreference(PreferenceUtils.ShipmentProductsType)
        preferenceUtils.removeFromPreference(PreferenceUtils.PRODUCT_APPROVAL)
        preferenceUtils.removeFromPreference(PreferenceUtils.RESCHEDULE_APPROVAL)
        preferenceUtils.removeFromPreference(PreferenceUtils.DELIVERY_NOTE)
        preferenceUtils.removeFromPreference(PreferenceUtils.DELIVERY_NOTE_RESHEDULE)
        preferenceUtils.removeFromPreference(PreferenceUtils.PRODUCT_CODE)
        preferenceUtils.removeFromPreference(PreferenceUtils.PRODUCT_DESCRIPTION)
        preferenceUtils.removeFromPreference(PreferenceUtils.TOTAL_AMOUNT)
        preferenceUtils.removeFromPreference(PreferenceUtils.PAYMENT_TYPE)
        preferenceUtils.removeFromPreference(PreferenceUtils.C_LAT)
        preferenceUtils.removeFromPreference(PreferenceUtils.C_LONG)
//          preferenceUtils.removeFromPreference(PreferenceUtils.B_SITE_NAME)
//          preferenceUtils.removeFromPreference(PreferenceUtils.COMPANY)
//          preferenceUtils.removeFromPreference(PreferenceUtils.COMPANY_DES)
//          preferenceUtils.removeFromPreference(PreferenceUtils.B_SITE_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.DATE_FORMAT)
        preferenceUtils.removeFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER)
        preferenceUtils.removeFromPreference(PreferenceUtils.NONBG)
//          preferenceUtils.removeFromPreference(PreferenceUtils.USER_ROLE)
//          preferenceUtils.removeFromPreference(PreferenceUtils.LOGIN_EMAIL)
//          preferenceUtils.removeFromPreference(PreferenceUtils.USER_AUTHORIZED)
        preferenceUtils.removeFromPreference(PreferenceUtils.Reason_CODE)
        preferenceUtils.removeFromPreference(PreferenceUtils.CARRIER_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.REASON)
        preferenceUtils.removeFromPreference(PreferenceUtils.Reason_Payment)
        preferenceUtils.removeFromPreference(PreferenceUtils.SELECTION)
//          preferenceUtils.removeFromPreference(PreferenceUtils.V_PLATE)
        preferenceUtils.removeFromPreference(PreferenceUtils.D_TIME)
        preferenceUtils.removeFromPreference(PreferenceUtils.A_TIME)
//          preferenceUtils.removeFromPreference(PreferenceUtils.N_SHIPMENTS)
        preferenceUtils.removeFromPreference(PreferenceUtils.D_DATE)
        preferenceUtils.removeFromPreference(PreferenceUtils.A_DATE)
        preferenceUtils.removeFromPreference(PreferenceUtils.VEHICLE_CODE)
        preferenceUtils.removeFromPreference(PreferenceUtils.LOAN_RETURN_NUMBER)
        preferenceUtils.removeFromPreference(PreferenceUtils.DOC_NUMBER)
//          preferenceUtils.removeFromPreference(PreferenceUtils.NON_VEHICLE_CODE)
        preferenceUtils.removeFromPreference(PreferenceUtils.LOCATION)
        preferenceUtils.removeFromPreference(PreferenceUtils.CD_TIME)
        preferenceUtils.removeFromPreference(PreferenceUtils.CA_TIME)
        preferenceUtils.removeFromPreference(PreferenceUtils.CN_SHIPMENTS)
        preferenceUtils.removeFromPreference(PreferenceUtils.CD_DATE)
        preferenceUtils.removeFromPreference(PreferenceUtils.CA_DATE)
//          preferenceUtils.removeFromPreference(PreferenceUtils.CVEHICLE_CODE)
        preferenceUtils.removeFromPreference(PreferenceUtils.STATUS)
        preferenceUtils.removeFromPreference(PreferenceUtils.SHIPMENT_DT)
        preferenceUtils.removeFromPreference(PreferenceUtils.SHIPMENT_AT)
        preferenceUtils.removeFromPreference(PreferenceUtils.NOTES)
//          preferenceUtils.removeFromPreference(PreferenceUtils.DRIVER_NAME )
//          preferenceUtils.removeFromPreference(PreferenceUtils.LOGIN_DRIVER_NAME             )
        preferenceUtils.removeFromPreference(PreferenceUtils.DISTANCE)
        preferenceUtils.removeFromPreference(PreferenceUtils.EMAIL)
        preferenceUtils.removeFromPreference(PreferenceUtils.PHONE)
        preferenceUtils.removeFromPreference(PreferenceUtils.LONGITUDE2)
        preferenceUtils.removeFromPreference(PreferenceUtils.LATTITUDE2)
        preferenceUtils.removeFromPreference(PreferenceUtils.LONGITUDE)
        preferenceUtils.removeFromPreference(PreferenceUtils.LATTITUDE)
        preferenceUtils.removeFromPreference(PreferenceUtils.CHECKIN_DATE)
//          preferenceUtils.removeFromPreference(PreferenceUtils.DRIVER_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.VR_ID)
//          preferenceUtils.removeFromPreference(PreferenceUtils.CHEQUE_DATE                   )
        preferenceUtils.removeFromPreference(PreferenceUtils.INVOICE_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.PAYMENT_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.INVOICE_AMOUNT)
//          preferenceUtils.removeFromPreference(PreferenceUtils.CURRENCY)
        preferenceUtils.removeFromPreference(PreferenceUtils.INVOICE_SHIPMENT_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.PREVIEW_INVOICE_ID)
//          preferenceUtils.removeFromPreference(PreferenceUtils.LOGIN_SUCCESS)
        preferenceUtils.removeFromPreference(PreferenceUtils.PAYMENT_INVOICE_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.SALES_ORDER_NUMBER);
        preferenceUtils.removeFromPreference(PreferenceUtils.FINANTIAL_SITE_ID);
        preferenceUtils.removeFromPreference(PreferenceUtils.CREQUESTED_NUMBER);
        preferenceUtils.removeFromPreference(PreferenceUtils.REQUESTED_NUMBER);
        preferenceUtils.removeFromPreference(PreferenceUtils.CREDIT_FLAG);





        StorageManager.getInstance(this).deleteCheckInData(this)
        StorageManager.getInstance(this).deleteCompletedShipments(this)
        StorageManager.getInstance(this).deleteSkipShipmentList(this)
        StorageManager.getInstance(this).deleteVanScheduleProducts(this);
        StorageManager.getInstance(this).deleteSpotSalesCustomerList(this);
        StorageManager.getInstance(this).deleteVehicleInspectionList(this);
        StorageManager.getInstance(this).deleteGateInspectionList(this);
        StorageManager.getInstance(this).deleteSiteListData(this);
        StorageManager.getInstance(this).deleteShipmentListData(this)
        StorageManager.getInstance(this).deleteCheckOutData(this)
        StorageManager.getInstance(this).deleteScheduledNonScheduledReturnData();// clearing all tables
        StorageManager.getInstance(this).deleteDamageDeliveryItems(this)
        StorageManager.getInstance(this).deleteActiveDeliveryMainDo(this)
        StorageManager.getInstance(this).deleteDepartureData(this)
        StorageManager.getInstance(this).deleteVanScheduleProducts(this);
        StorageManager.getInstance(this).deleteVanNonScheduleProducts(this);


        selectActivityList()
    }

    private fun checkOutTimeCapture() {
        var driverId = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "")
        var date = CalendarUtils.getDate()
        var time = CalendarUtils.getTime()

        var vehicleCheckOutDo = StorageManager.getInstance(this).getVehicleCheckOutData(this);
        var arrivaldate = vehicleCheckOutDo.checkOutTimeCaptureArrivalTime
        var arrivalTime = vehicleCheckOutDo.checkOutTimeCaptureArrivalDate
        var startTime = vehicleCheckOutDo.checkOutTimeCaptureStartLoadingTime
        var startdate = vehicleCheckOutDo.checkOutTimeCaptureStartLoadingDate
        var stockDate = vehicleCheckOutDo.checkOutTimeCaptureStockLoadingTime
        var stockTime = vehicleCheckOutDo.checkOutTimeCaptureStockLoadingDate
        var endloadingtime = vehicleCheckOutDo.checkOutTimeCaptureEndLoadingTime
        var endloadingdate = vehicleCheckOutDo.checkOutTimeCaptureEndLoadingDate
        var vehicleinspectiontime = vehicleCheckOutDo.checkOutTimeCaptureVehicleInspectionTime
        var vehicleinspectiondate = vehicleCheckOutDo.checkOutTimeCapturVehicleInspectionDate
        var gateinspectiontime = vehicleCheckOutDo.checkOutTimeCaptureGateInspectionTime
        var gateinspectiondate = vehicleCheckOutDo.checkOutTimeCapturGateInspectionDate
        var CheckOutdate = vehicleCheckOutDo.checkOutTimeCapturcheckOutDate
        var CheckOuttime = vehicleCheckOutDo.checkOutTimeCapturecheckOutTime

        val siteListRequest = CheckoutTimeCaptureRequest(driverId, date, arrivaldate, arrivalTime, startTime, startdate, stockDate, stockTime, endloadingtime, endloadingdate,
                vehicleinspectiontime, vehicleinspectiondate, gateinspectiontime, gateinspectiondate, CheckOuttime, CheckOutdate, this)
        showLoader()
        siteListRequest.setOnResultListener { isError, loginDo ->


            if (isError) {
                hideLoader()
                showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.server_error), "" + resources.getString(R.string.ok), "", "" + resources.getString(R.string.from_failure), false)

//                    Toast.makeText(this@CheckOutScreen, "Failed", Toast.LENGTH_SHORT).show()
            } else {
                hideLoader()
                if (loginDo.flag == 1) {
//                        showToast("Success")
                    clearDataLocalData()
                } else {
                    hideLoader()
                    showToast("Unable to do CheckOut..please try again")
                }

            }

            hideLoader()

        }

        siteListRequest.execute()
    }

    private fun loadData() {
        showLoader()
        val vehicleRoutId = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "")

        if (vehicleRoutId.length > 0) {
            val driverListRequest = DisplayPickupListRequest(this, vehicleRoutId)
            driverListRequest.setOnResultListener { isError, pods ->
                hideLoader()
                if (isError) {
                } else {
                    pickUpDos = pods
                    if (pickUpDos.size > 0) {
                        for (k in pickUpDos.indices) {
                            if (pickUpDos.get(k).departuredFlag != 2) {
                                isDepartureExisted = true
                                break
                            } else {
                                isDepartureExisted = false
                            }
                        }
                        if (isDepartureExisted) {
                            showAppCompatAlert("", "Kindly process all scheduled deliveries from the list to Check Out", "OK", "", "FAILURE", false)
                        } else {
                            var stringBuilder = StringBuilder()
                            stringBuilder.append("CheckOut At ")
                            stringBuilder.append(CalendarUtils.getCurrentDate())
                            stringBuilder.append(" ?")
                            showAppCompatAlert("Alert!", "" + stringBuilder.toString(), "OK", "Cancel", "CheckInSuccess", false)


                        }

                    } else {
                        var stringBuilder = StringBuilder()
                        stringBuilder.append("CheckOut At ")
                        stringBuilder.append(CalendarUtils.getCurrentDate())
                        stringBuilder.append(" ?")
                        showAppCompatAlert("Alert!", "" + stringBuilder.toString(), "OK", "Cancel", "CheckInSuccess", false)
                    }
                    hideLoader()

                }
            }
            driverListRequest.execute()
        } else {
            hideLoader()
            var stringBuilder = StringBuilder()
            stringBuilder.append("CheckOut At ")
            stringBuilder.append(CalendarUtils.getCurrentDate())
            stringBuilder.append(" ?")
            showAppCompatAlert("Alert!", "" + stringBuilder.toString(), "OK", "Cancel", "CheckInSuccess", false)

        }

    }


    private fun printDocument(obj: Any, from: Int) {
        val printConnection = PrinterConnection.getInstance(this@DashBoardActivity)
        if (printConnection.isBluetoothEnabled()) {
            printConnection.connectToPrinter(obj, from)
            preferenceUtils.removeFromPreference(PreferenceUtils.Non_Scheduled_Route_Id)
            preferenceUtils.removeFromPreference(PreferenceUtils.CV_PLATE)


        } else {
//            PrinterConstants.PRINTER_MAC_ADDRESS = ""
            showToast("Please enable your mobile Bluetooth.")
//            showAppCompatAlert("", "Please enable your mobile Bluetooth.", "Enable", "Cancel", "EnableBluetooth", false)
        }
    }

    private fun pairedDevices() {
        val pairedDevices = BluetoothAdapter.getDefaultAdapter().getBondedDevices()
        if (pairedDevices.size > 0) {
            for (device in pairedDevices) {
                val deviceName = device.getName()
                val mac = device.getAddress() // MAC address
//                if (StorageManager.getInstance(this).getPrinterMac(this).equals("")) {
                StorageManager.getInstance(this).savePrinterMac(this, mac);
//                }
                Log.e("Bluetooth", "Name : " + deviceName + " Mac : " + mac)
                break
            }
        }
    }


    override fun onStart() {
        super.onStart()
        val filter = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        registerReceiver(mReceiver, filter)
    }

    override fun onDestroy() {
        unregisterReceiver(mReceiver)
//        PrinterConstants.PRINTER_MAC_ADDRESS = ""
        super.onDestroy()
    }

    private val mReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            if (BluetoothDevice.ACTION_FOUND == action) {
                val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE);
                PrinterConstants.bluetoothDevices.add(device)
                Log.e("Bluetooth", "Discovered => name : " + device!!.name + ", Mac : " + device.address)
            } else if (action.equals(BluetoothDevice.ACTION_ACL_CONNECTED)) {
                Log.e("Bluetooth", "status : ACTION_ACL_CONNECTED")
                pairedDevices()
            } else if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                val state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);

                if (state == BluetoothAdapter.STATE_OFF) {
                    Log.e("Bluetooth", "status : STATE_OFF")
                } else if (state == BluetoothAdapter.STATE_TURNING_OFF) {
                    Log.e("Bluetooth", "status : STATE_TURNING_OFF")
                } else if (state == BluetoothAdapter.STATE_ON) {
                    Log.e("Bluetooth", "status : STATE_ON")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_TURNING_ON) {
                    Log.e("Bluetooth", "status : STATE_TURNING_ON")
                } else if (state == BluetoothAdapter.STATE_CONNECTING) {
                    Log.e("Bluetooth", "status : STATE_CONNECTING")
                } else if (state == BluetoothAdapter.STATE_CONNECTED) {
                    Log.e("Bluetooth", "status : STATE_CONNECTED")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_DISCONNECTED) {
                    Log.e("Bluetooth", "status : STATE_DISCONNECTED")
                }
            }
        }
    }

    private fun selectActivityList() {
        hideLoader()


        if (Util.isNetworkAvailable(this)) {
            val site = preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID, "")
            var userActivityReportActivity = UserActivityReportRequest(CalendarUtils.getDate(), this@DashBoardActivity, site)

            userActivityReportActivity.setOnResultListener { isError, userActivityMainDO ->
                hideLoader()

                if (isError) {

                    Toast.makeText(this@DashBoardActivity, resources.getString(R.string.server_error), Toast.LENGTH_SHORT).show()
                } else {
                    userActivityReportMainDO = userActivityMainDO
                    if (userActivityMainDO != null) {
                        if (userActivityReportMainDO.spotDeliveryDOS.size > 0 || userActivityReportMainDO.salesInvoiceDOS.size > 0 ||
                                userActivityReportMainDO.cashDOS.size > 0 || userActivityReportMainDO.chequeDOS.size > 0 ||
                                userActivityReportMainDO.productDOS.size > 0 || userActivityReportMainDO.stockDOS.size > 0) {
                            AppConstants.Trans_Date = CalendarUtils.getDate()
                            printDocument(userActivityReportMainDO, PrinterConstants.PrintUserActivityReport)
                            showToast("Updated Successfully")

                        } else {
                            Toast.makeText(this@DashBoardActivity, resources.getString(R.string.no_data), Toast.LENGTH_SHORT).show()

                        }

                    }


                }

                hideLoader()

            }

            userActivityReportActivity.execute()
        } else {
//            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "",false)
            showToast("" + resources.getString(R.string.internet_connection))
        }

//


    }

}