//package com.tbs.brothersgas.haadhir.Requests;
//
///**
// * Created by Vijay on 19-05-2016.
// */
//
//import android.content.Context;
//import android.os.AsyncTask;
//
//import com.tbs.brothersgas.haadhir.Activitys.BaseActivity;
//import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryDO;
//import com.tbs.brothersgas.haadhir.Model.ImageDO;
//import com.tbs.brothersgas.haadhir.Model.LoadStockDO;
//import com.tbs.brothersgas.haadhir.Model.SuccessDO;
//import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;
//
//import org.json.JSONArray;
//import org.json.JSONObject;
//import org.ksoap2.HeaderProperty;
//import org.ksoap2.SoapEnvelope;
//import org.ksoap2.serialization.SoapObject;
//import org.ksoap2.serialization.SoapSerializationEnvelope;
//import org.ksoap2.transport.HttpTransportSE;
//import org.xmlpull.v1.XmlPullParser;
//import org.xmlpull.v1.XmlPullParserFactory;
//
//import java.io.StringReader;
//import java.net.SocketTimeoutException;
//import java.util.ArrayList;
//import java.util.List;
//
//public class UpdateRunQtyRequest extends AsyncTask<String, Void, Boolean> {
//
//    private SuccessDO successDO;
//    private Context mContext;
//    private String shipmentID, date, reason, comment;
//    String username, password, ip, pool, port;
//    PreferenceUtils preferenceUtils;
//    private ArrayList<ActiveDeliveryDO> activeDeliveryDOS;
//    private int flaG;
//
//    public UpdateRunQtyRequest(String shipment, ArrayList<ActiveDeliveryDO> activeDeliveryDoS, Context mContext) {
//
//        this.mContext = mContext;
//
//        this.activeDeliveryDOS = activeDeliveryDoS;
//        this.shipmentID = shipment;
//
//
//    }
//
//
//    public void setOnResultListener(OnResultListener onResultListener) {
//        this.onResultListener = onResultListener;
//    }
//
//    OnResultListener onResultListener;
//
//    public interface OnResultListener {
//        public void onCompleted(boolean isError, SuccessDO invoiceHistoryMainDO);
//
//    }
//
//    public boolean runRequest() {
//        // System.out.println("CUSTOMER ID " + customerId);
//        String NAMESPACE = "http://www.adonix.com/WSS";
//        String METHOD_NAME = "run";
//        String SOAP_ACTION = "CAdxWebServiceXmlCC";
////        String URL = "http://183.82.9.23:8124/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC";
//        preferenceUtils = new PreferenceUtils(mContext);
//        username = preferenceUtils.getStringFromPreference(PreferenceUtils.A_USER_NAME, "");
//        password = preferenceUtils.getStringFromPreference(PreferenceUtils.A_PASSWORD, "");
//        ip = preferenceUtils.getStringFromPreference(PreferenceUtils.IP_ADDRESS, "");
//        port = preferenceUtils.getStringFromPreference(PreferenceUtils.PORT, "");
//        pool = preferenceUtils.getStringFromPreference(PreferenceUtils.ALIAS, "");
//
//        String URL = "http://" + ip + ":" + port + "/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC";
//        String site  = preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID, "");
//
//        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
//        // Set all input params
//        request.addProperty("publicName", "XDELIVMETR");
//
//        JSONObject jsonObject = new JSONObject();
//
//        try {
//            jsonObject.put("I_XSDHNUM", shipmentID);
//            JSONArray jsonArray =new JSONArray();
//
//
//                if(activeDeliveryDOS!=null && activeDeliveryDOS.size()>0) {
//                    for (int i = 0; i < activeDeliveryDOS.size(); i++) {
//                        JSONObject jsonObject1 = new JSONObject();
//
//                        jsonObject1.put("I_XCMTSR", activeDeliveryDOS.get(i).openingQuantity);
//                        jsonObject1.put("I_XOMTSR", activeDeliveryDOS.get(i).endingQuantity);
//                        jsonObject1.put("I_XORGNLMTR", activeDeliveryDOS.get(i).totalQuantity);
//                        jsonObject1.put("I_XSDDLIN", activeDeliveryDOS.get(i).linenumber);
//
//                 /*   jsonArray.put("I_YQTY",createDeliveryDoS.get(i).totalQuantity);
//                    jsonArray.put("I_YITMREF",createDeliveryDoS.get(i).item);*/
//                        jsonArray.put(i, jsonObject1);
//                    }
//                    jsonObject.put("GRP2", jsonArray);
//                }
//
//
//
//
//        } catch (Exception e) {
//            System.out.println("Exception " + e);
//        }
//        request.addProperty("inputXml", jsonObject.toString());
//
//        SoapObject callcontext = new SoapObject("", "callContext");
//        // Set all input params
//        callcontext.addProperty("codeLang", "ENG");
//        callcontext.addProperty("poolAlias", pool);
//        callcontext.addProperty("poolId", "");
//        callcontext.addProperty("codeUser", username);
//        callcontext.addProperty("password", password);
//        callcontext.addProperty("requestConfig", "adxwss.trace.on=off");
//
//        request.addSoapObject(callcontext);
//
//
//        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
//
//        envelope.setOutputSoapObject(request);
//
//        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
//        androidHttpTransport.debug = true;
//
//        try {
//            List<HeaderProperty> headerList = new ArrayList<HeaderProperty>();
//            headerList.add(new HeaderProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode((username + ":" + password).getBytes())));
//
//
//            androidHttpTransport.call(SOAP_ACTION, envelope, headerList);
//
//
//            SoapObject response = (SoapObject) envelope.getResponse();
//            String resultXML = (String) response.getProperty("resultXml");
//            if (resultXML != null && resultXML.length() > 0) {
//                return parseXML(resultXML);
//            } else {
//                return false;
//            }
//        } catch (SocketTimeoutException e) {
//            e.printStackTrace();
//            return false;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return false;
//        }
//
//    }
//
//    public boolean parseXML(String xmlString) {
//        System.out.println("xmlString " + xmlString);
//        try {
//            String text = "", attribute = "", startTag = "", endTag = "";
//            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
//            factory.setNamespaceAware(true);
//            XmlPullParser xpp = factory.newPullParser();
//
//            xpp.setInput(new StringReader(xmlString));
//            int eventType = xpp.getEventType();
//
//
//            successDO = new SuccessDO();
//
//            while (eventType != XmlPullParser.END_DOCUMENT) {
//                if (eventType == XmlPullParser.START_TAG) {
//
//                    startTag = xpp.getName();
//                    if (startTag.equalsIgnoreCase("FLD")) {
//                        attribute = xpp.getAttributeValue(null, "NAME");
//                    } else if (startTag.equalsIgnoreCase("GRP")) {
//
//                    } else if (startTag.equalsIgnoreCase("TAB")) {
//
//                    } else if (startTag.equalsIgnoreCase("LIN")) {
//
//                    }
//                } else if (eventType == XmlPullParser.END_TAG) {
//                    endTag = xpp.getName();
//
//                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {
//                        if (attribute.equalsIgnoreCase("O_XFLG")) {
//                            if (text.length() > 0) {
//
//                                successDO.flag = Integer.parseInt(text);
//                            }
//
//
//                        }
//                        text="";
//
//                    }
//
//                    if (endTag.equalsIgnoreCase("GRP")) {
//                    }
//
//                    if (endTag.equalsIgnoreCase("LIN")) {
//                    }
//
//                } else if (eventType == XmlPullParser.TEXT) {
//                    text = xpp.getText();
//                }
//
//                eventType = xpp.next();
//            }
//            return true;
//        } catch (Exception e) {
//            System.out.println("Exception Parser" + e);
//
//            return false;
//        }
//    }
//
//
//    @Override
//    protected void onPreExecute() {
//        super.onPreExecute();
//
//        ((BaseActivity)mContext).showLoader();    }
//
//    @Override
//    protected Boolean doInBackground(String... param) {
//        return runRequest();
//    }
//
//    @Override
//    protected void onPostExecute(Boolean result) {
//        super.onPostExecute(result);
//
//        ((BaseActivity)mContext).hideLoader();
//        if (onResultListener != null) {
//            onResultListener.onCompleted(!result, successDO);
//        }
//    }
//}