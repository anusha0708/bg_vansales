package com.tbs.brothersgas.haadhir.common;

import android.content.Context;
import android.util.Log;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.tbs.brothersgas.haadhir.Model.UserCurrentTime;
import com.tbs.brothersgas.haadhir.listeners.ResultListner;
import com.tbs.brothersgas.haadhir.utils.AppPrefs;
import com.tbs.brothersgas.haadhir.utils.Constants;
import com.tbs.brothersgas.haadhir.utils.Util;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/*
 * Created by developer on 22/2/19.
 */
public class FireBaseOperations {


    public static void setCurrentUserTime() {

        FirebaseFirestore.getInstance()
                .collection(Constants.FB_TABLE_USERS)
                .document(AppPrefs.getString(AppPrefs.DRIVER_ID, "QUA02")).
                collection(Constants.USERTIMINGS).
                document(Constants.CURRENTTIME).set(new UserCurrentTime());
    }


    public static void getUserTime(ResultListner resultListner) {
        final CollectionReference cr = FirebaseFirestore
                .getInstance()
                .collection(Constants.FB_TABLE_USERS)
                .document(AppPrefs.getString(AppPrefs.DRIVER_ID, "QUA02"))
                .collection(Constants.USERTIMINGS);

        cr.document(Constants.CURRENTTIME).get()
                .addOnSuccessListener(documentSnapshot -> {
                    if (documentSnapshot != null && documentSnapshot.exists()) {
                        UserCurrentTime userCurrentTime = documentSnapshot.toObject(UserCurrentTime.class);
                        if ((userCurrentTime != null ? userCurrentTime.getCurrentTime() : null) != null) {
                            Log.e("Differnt", userCurrentTime.getCurrentTime() + "");
                            resultListner.onResultListner(userCurrentTime.getCurrentTime(), true);
                        }
                    }
                }).addOnFailureListener(e -> {
            resultListner.onResultListner(null, true);
        });
    }


    public static void checkingDate(Context context) {
        new Thread(() -> {
            FireBaseOperations.setCurrentUserTime();
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //Toast.makeText(context, "DateChanged", Toast.LENGTH_SHORT).show();
            if (Util.isNetworkAvailable(context)) {
                long timeFromLocal = Calendar.getInstance().getTimeInMillis();
                FireBaseOperations.getUserTime((object, isSuccess) -> {
                    if (isSuccess) {
                        Log.d("userTimings0--->", object + "");
                        Date date = (Date) object;

                        if(date!=null){
                            long diffInMillisec = date.getTime() - timeFromLocal;
                            Log.d("Diff--->", diffInMillisec + "");


                            showDialog(diffInMillisec);

                        }

                    } else {
                        AppPrefs.putBoolean(AppPrefs.IS_TIME_CHANGED, false);
                    }
                });
            }
        }).start();

    }

    private static void showDialog(long diffInMillisec) {
        long diffInDays = TimeUnit.MILLISECONDS.toDays(diffInMillisec);
        long diffInHours = TimeUnit.MILLISECONDS.toHours(diffInMillisec);
        long diffInMin = TimeUnit.MILLISECONDS.toMinutes(diffInMillisec);
        long diffInSec = TimeUnit.MILLISECONDS.toSeconds(diffInMillisec);

        Log.d("diffInDays->", diffInDays + "---" + diffInHours + "---" + diffInMin + "--" + diffInSec);

        if (diffInDays != 0 || diffInHours != 0 || diffInMin != 0) {
            AppPrefs.putBoolean(AppPrefs.IS_TIME_CHANGED, true);
        } else {
            AppPrefs.putBoolean(AppPrefs.IS_TIME_CHANGED, false);
        }
    }
}
