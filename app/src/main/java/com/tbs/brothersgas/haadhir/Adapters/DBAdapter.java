package com.tbs.brothersgas.haadhir.Adapters;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBAdapter {
    public static final String DATABASE_NAME = "ramsdb";
    public static final String TABLE_NAME = "sampletable";
    public static final int DATABASE_VERSION = 1;

    public static final String COL_ROWID = "rowid";
    public static final String COL_FIRSTNAME = "firstname";
    public static final String COL_LASTNAME = "lastname";

    String CREATE_TABLE = "create table sampletable (rowid integer primary key autoincrement,firstname text not null,lastname text not null)";

    SQLiteDatabase db;

    DBHelper dbHelper;

    public DBAdapter(Context context) {
        // TODO Auto-generated constructor stub
        dbHelper = new DBHelper(context);
    }

   public DBAdapter openDatabase(){
        db = dbHelper.getWritableDatabase();
        return this;
    }


    void closeDatabase(){
        dbHelper.close();
    }

    public void insertValues(String fname,String lname){
        ContentValues con = new ContentValues();
        con.put(COL_FIRSTNAME, fname);
        con.put(COL_LASTNAME, lname);

        db.insert(TABLE_NAME, null, con);
    }

 public    Cursor getAllValues(){

        String[] columns = {COL_ROWID,COL_FIRSTNAME,COL_LASTNAME};

        return db.query(TABLE_NAME, columns, null, null, null, null, null);
    }

    void deleteAllRecords(){
        db.delete(TABLE_NAME, null, null);
    }

    void deleteOneRecord(String rowid){
        db.delete(TABLE_NAME, rowid +"="+COL_ROWID, null);
    }

    void updateOneRecord(String rowid,String fname,String lname){

        ContentValues con = new ContentValues();
        con.put(COL_FIRSTNAME, fname);
        con.put(COL_LASTNAME, lname);

        db.update(TABLE_NAME, con, rowid +"="+COL_ROWID, null);
    }



    class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
            // TODO Auto-generated constructor stub
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            // TODO Auto-generated method stub
            db.execSQL(CREATE_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // TODO Auto-generated method stub

        }

    }
}