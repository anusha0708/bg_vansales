package com.tbs.brothersgas.haadhir.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tbs.brothersgas.haadhir.Activitys.BaseActivity;
import com.tbs.brothersgas.haadhir.Model.LoadStockDO;
import com.tbs.brothersgas.haadhir.R;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;

import java.util.ArrayList;

public class UserActivityProductAdapter extends RecyclerView.Adapter<UserActivityProductAdapter.MyViewHolder> {

    private ArrayList<LoadStockDO> loadStockDOS;
    private Context context;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvQuantity, tvProductName;

        public MyViewHolder(View view) {
            super(view);
            tvQuantity = (TextView) view.findViewById(R.id.tvQuantity);
            tvProductName = (TextView) view.findViewById(R.id.tvProductName);


        }
    }


    public UserActivityProductAdapter(Context context, ArrayList<LoadStockDO> siteDOS) {
        this.context = context;
        this.loadStockDOS = siteDOS;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pro_data_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final LoadStockDO loadStockDO = loadStockDOS.get(position);

        String amount = String.valueOf(loadStockDO.quantity);
        if(((BaseActivity)context).preferenceUtils.getbooleanFromPreference(PreferenceUtils.IS_MULTI_SITE,false)){
            holder.tvProductName.setText(loadStockDO.productDescription+"\n"+loadStockDO.site+" - "+loadStockDO.siteDescription);

        }else{
            holder.tvProductName.setText(loadStockDO.productDescription);
        }
        holder.tvQuantity.setText("" + amount);


    }

    @Override
    public int getItemCount() {
        return loadStockDOS.size();
    }

}
