package com.tbs.brothersgas.haadhir.collector

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.app.Dialog
import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.os.Looper
import android.provider.MediaStore
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Base64
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import android.webkit.PermissionRequest
import android.widget.*
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.single.PermissionListener
import com.tbs.brothersgas.haadhir.Activitys.BaseActivity
import com.tbs.brothersgas.haadhir.Adapters.BankListAdapter
import com.tbs.brothersgas.haadhir.Adapters.CardAdapter
import com.tbs.brothersgas.haadhir.Adapters.FinantialSiteListAdapter
import com.tbs.brothersgas.haadhir.Model.*
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.*
import com.tbs.brothersgas.haadhir.common.AppConstants
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.pdfs.PaymentReceiptPDF
import com.tbs.brothersgas.haadhir.utils.CalendarUtils
import com.tbs.brothersgas.haadhir.utils.LogUtils
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import kotlinx.android.synthetic.main.collector_create_payments_screen.*
import kotlinx.android.synthetic.main.include_credit_card.*
import java.io.ByteArrayOutputStream
import java.io.FileInputStream
import java.text.DateFormat
import java.util.*


class CreatePaymentActivity : BaseActivity() {

    lateinit var etChequeNo: EditText
    lateinit var tvBankSelection: TextView
    lateinit var dialog: Dialog
    lateinit var bankDialog: Dialog
    lateinit var etAmount: EditText
    private var calender: Calendar = Calendar.getInstance()
    lateinit var podDo: PodDo
    var encImage = ""
    lateinit var createPDFpaymentDO: PaymentPdfDO
    private var selectedInvoiceDOs = ArrayList<UnPaidInvoiceDO>()
    private var selectedCustomerDOs = ArrayList<CustomerDo>()
    private lateinit var ivCapturedPic: ImageView
    private lateinit var ivCamera: ImageView
    private lateinit var llChequeDate: LinearLayout
    private lateinit var llChequeNo: LinearLayout
    private lateinit var llView: LinearLayout
    private lateinit var llBankList: LinearLayout
    lateinit var cardDialog: Dialog
    private var chargeType: Int = 3

    lateinit var btnPayments: Button
    public lateinit var tvSelection: TextView
    private lateinit var cheque: String
    private lateinit var bank: String
    private lateinit var amount: String
    private lateinit var tvCurrency: TextView
    private lateinit var tvSelectDate: TextView
    private lateinit var createPaymentDo: CreatePaymentDO
    private lateinit var unpaidMainDo: UnPaidInvoiceMainDO
    private lateinit var llCapture: LinearLayout
    private lateinit var siteListRequest: UnPaidInvoicesListRequest
    private lateinit var siteListRequest2: CreateMiscelleneousPaymentRequest
    var picUri: Uri? = null
    internal var isImageCaptured = false
    internal var imagePath = ""
    private var cday: Int = 0
    private var cmonth: Int = 0
    private var cyear: Int = 0
    private var type: Int = 10
    private var paymentType: Int = 0

    private var fromId = 0
    private var totalAmount = ""

    private var bitmap: Bitmap? = null
    private  var chequeDate: String=""
    private lateinit var cid: String
    var REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 200
    private var mLastUpdateTime: String? = null
    private val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 10000
    private val FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS: Long = 5000
    private val REQUEST_CHECK_SETTINGS = 100
    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private var mSettingsClient: SettingsClient? = null
    private var mLocationRequest: LocationRequest? = null
    private var mLocationSettingsRequest: LocationSettingsRequest? = null
    private var mLocationCallback: LocationCallback? = null
    private var mCurrentLocation: Location? = null
    private var mRequestingLocationUpdates: Boolean? = null
    private var payId = ""
    private lateinit var llSiteList: LinearLayout
    lateinit var tvSiteSelection: TextView
    lateinit var siteDialog: Dialog
    private lateinit var site: String
    lateinit var tvCardSelection: TextView
    lateinit var tvCharge: TextView
    private lateinit var cardtype: String

    override fun initialize() {
        val llCategories = getLayoutInflater().inflate(R.layout.collector_create_payments_screen, null) as ScrollView
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            val intent = Intent()
            setResult(1, intent)
            finish()
        }
        disableMenuWithBackButton()
        initializeControls()
        preferenceUtils.removeFromPreference(PreferenceUtils.CARD_TYPE)
        preferenceUtils.removeFromPreference(PreferenceUtils.SERVICE_CHARGES)
        if (intent.hasExtra("SINGLE")) {
            payId = intent!!.extras!!.getString("SINGLE")!!
        }
        if (intent.hasExtra("selectedInvoiceDOs")) {
            selectedInvoiceDOs = intent.getSerializableExtra("selectedInvoiceDOs") as ArrayList<UnPaidInvoiceDO>

        }
        if (intent.hasExtra("selectedCustomerDOs")) {
            selectedCustomerDOs = intent.getSerializableExtra("selectedCustomerDOs") as ArrayList<CustomerDo>
        }
        if(selectedCustomerDOs.size>1){
            rbconlinePayments.visibility=View.GONE
        }else{
            rbconlinePayments.visibility=View.VISIBLE

        }
        if (selectedInvoiceDOs != null && selectedInvoiceDOs.size > 0) {
            llSiteList.visibility = View.GONE
        } else {
            llSiteList.visibility = View.VISIBLE

        }
        totalAmount = preferenceUtils.getStringFromPreference(PreferenceUtils.CREDIT_INVOICE_AMOUNT, "")

        if (intent.hasExtra("Sales")) {
            fromId = intent!!.extras!!.getInt("Sales")
        }
        if (fromId == 1) {
            llView.setVisibility(View.GONE)
            if (!totalAmount.equals("0.00")) {
                etAmount.setText(totalAmount)
            }

        } else {
            llView.setVisibility(View.VISIBLE)
            val data = preferenceUtils.getStringFromPreference(PreferenceUtils.INVOICE_ID, "")
            tvSelection.setText("" + data)
            var invoiceAmount = preferenceUtils.getStringFromPreference(PreferenceUtils.INVOICE_AMOUNT, "")
            if (!invoiceAmount.equals("0.00")) {
                etAmount.setText(invoiceAmount)
            }

        }
        podDo = StorageManager.getInstance(this).getDepartureData(this);
        if (!podDo.payment.equals("")) {

//            btnPayments.setBackgroundColor(resources.getColor(R.color.md_gray_light))
//            btnPayments.setClickable(false)
//            btnPayments.setEnabled(false)

            var selectedId = tvSelection.text.toString().trim()


            var invoiceId = preferenceUtils.getStringFromPreference(PreferenceUtils.PAYMENT_INVOICE_ID, "")
            if (invoiceId.length > 0 && selectedId.equals(invoiceId)) {
//                btnPayments.setBackgroundColor(resources.getColor(R.color.md_gray_light))
//                btnPayments.setClickable(false)
//                btnPayments.setEnabled(false)

            } else {
                btnPayments.setBackgroundColor(resources.getColor(R.color.md_green))
                btnPayments.setClickable(true)
                btnPayments.setEnabled(true)
            }
        } else {
            btnPayments.setBackgroundColor(resources.getColor(R.color.md_green))
            btnPayments.setClickable(true)
            btnPayments.setEnabled(true)
        }



        cmonth = calender.get(Calendar.MONTH)
        cday = calender.get(Calendar.YEAR)
        cday = calender.get(Calendar.DAY_OF_MONTH)


        ivCamera.setOnClickListener {
            Util.makeFolder(Environment.getExternalStorageDirectory().toString(), "/BrothersGas")
            AppConstants.sdCardPath = Environment.getExternalStorageDirectory().toString() + "/BrothersGas"
            Util.selectImageDialog(this@CreatePaymentActivity, "Cheque Image")

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AppConstants.CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {

            if (resultCode == Activity.RESULT_OK) {
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                    try {
                        val fis = FileInputStream(AppConstants.file1)
                        bitmap = BitmapFactory.decodeStream(fis)
                        val baos = ByteArrayOutputStream()
                        bitmap!!.compress(Bitmap.CompressFormat.JPEG, 75, baos)
                        val bytes = baos.toByteArray()
                        encImage = Base64.encodeToString(bytes, Base64.DEFAULT)
                        ivCapturedPic.setImageBitmap(bitmap)
                        preferenceUtils.saveString(PreferenceUtils.CAPTURE, encImage)
                        LogUtils.debug("PIC", encImage)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    // now you can upload your image file
                } else {
                    imagePath = AppConstants.imageUri.getPath()!!
                    AppConstants.tempPath = AppConstants.imageUri.getPath()
                    isImageCaptured = true
                    val options = BitmapFactory.Options()
                    options.inSampleSize = 8
                    bitmap = BitmapFactory.decodeFile(AppConstants.tempPath, options)
                    val bdrawable = BitmapDrawable(resources, bitmap)

                    ivCapturedPic.setImageBitmap(bitmap)
                    val baos = ByteArrayOutputStream()
                    bitmap!!.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                    val bytes = baos.toByteArray()
                    encImage = Base64.encodeToString(bytes, Base64.DEFAULT)
                }


//                cheaqueImage.setText("")
            } else if (resultCode == Activity.RESULT_CANCELED) {

                Toast.makeText(this, " Picture was not taken ", Toast.LENGTH_SHORT).show()
            } else {

                Toast.makeText(this, " Picture was not taken ", Toast.LENGTH_SHORT).show()
            }
        }
        else if (requestCode == AppConstants.SELECT_FILE) {
            if (resultCode == Activity.RESULT_OK) {
                val selectedImage = data!!.data
                val filePath = arrayOf(MediaStore.Images.Media.DATA)
                val c = contentResolver.query(selectedImage!!, filePath, null, null, null)
                c!!.moveToFirst()
                val columnIndex = c.getColumnIndex(filePath[0])
                AppConstants.tempPath = c.getString(columnIndex)
                imagePath = c.getString(columnIndex)
                c.close()
                isImageCaptured = true
                val options = BitmapFactory.Options()
                options.inSampleSize = 8
                bitmap = BitmapFactory.decodeFile(AppConstants.tempPath, options)
                val bdrawable = BitmapDrawable(resources, bitmap)
                ivCapturedPic.setImageBitmap(bitmap)
                val baos = ByteArrayOutputStream()
                bitmap!!.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                val bytes = baos.toByteArray()
                encImage = Base64.encodeToString(bytes, Base64.DEFAULT)
            } else {
                Toast.makeText(this, " This Image cannot be stored .please try with some other Image. ", Toast.LENGTH_SHORT).show()
            }

        }
        if (requestCode == 1 && resultCode == 1) {
            val intent = Intent()
            setResult(1, intent)
            finish()
        }
    }


    @SuppressLint("MissingPermission")
    override fun initializeControls() {
        tvScreenTitle.setText(R.string.payment)
        tvSelection = findViewById(R.id.tvSelection) as TextView
        llView = findViewById(R.id.llView) as LinearLayout
        tvBankSelection = findViewById(R.id.tvBankSelection) as TextView
        llBankList = findViewById(R.id.llBankList) as LinearLayout
        etChequeNo = findViewById(R.id.etChequeNo) as EditText
        etAmount = findViewById(R.id.etAmount) as EditText
        tvCurrency = findViewById(R.id.tvCurrency) as TextView
        tvSelectDate = findViewById(R.id.tvSelectDate) as TextView
        btnPayments = findViewById(R.id.btnPayments) as Button
        ivCapturedPic = findViewById(R.id.ivCapturedPic) as ImageView
        ivCamera = findViewById(R.id.ivCamera) as ImageView
        llChequeDate = findViewById(R.id.llChequeDate) as LinearLayout
        llChequeNo = findViewById(R.id.llChequeNo) as LinearLayout
        llCapture = findViewById(R.id.llCapture) as LinearLayout
        var yourRadioGroup = findViewById(R.id.radioGroup) as RadioGroup
        var rbCash = findViewById(R.id.rbCash) as RadioButton
        var rbCheque = findViewById(R.id.rbCheque) as RadioButton
        llSiteList = findViewById(R.id.llSiteList) as LinearLayout
        tvSiteSelection = findViewById(R.id.tvSiteSelection) as TextView
        tvCardSelection = findViewById(R.id.tvCardSelection) as TextView
        tvCharge = findViewById(R.id.tvCreditCharge) as TextView

        rbCash.isChecked = false

        clickOnCheque(rbCheque)

        ivCapturedPic.setOnClickListener {
            if (bitmap != null) {
                showChequePreview(bitmap!!)
            }
        }

        location()
        paymentType = preferenceUtils.getIntFromPreference(PreferenceUtils.PAYMENT_TYPE, 0)
        var currency = preferenceUtils.getStringFromPreference(PreferenceUtils.CURRENCY, "")
        if(currency.isNotEmpty()){
            tvCurrency.setText("" + currency)
        }else{
            tvCurrency.setText("AED")

        }


        if (paymentType == 10) {
            rbCash.setChecked(true)
            rbCheque.isClickable = false
            rbCheque.setEnabled(false)
            clickOnCash(rbCash)

        } else if (paymentType == 20) {
            rbCheque.setChecked(true)
            rbCash.isClickable = false
            rbCash.setEnabled(false)
            clickOnCheque(rbCheque)

        } else if (paymentType == 30) {
            rbCheque.setChecked(true)
            rbCheque.isClickable = true
            rbCheque.setEnabled(true)
            rbCash.isClickable = true
            rbCash.setEnabled(true)
        } else {
            rbCash.setChecked(true)
            rbCash.isClickable = true
            rbCash.setEnabled(true)
            rbCash.isClickable = true
            rbCash.setEnabled(true)
        }
        yourRadioGroup.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {
                when (checkedId) {
                    R.id.rbCash -> {
                        clickOnCash(rbCash)
                    }
                    R.id.rbCheque -> {
                        clickOnCheque(rbCheque)
                    }
                    R.id.rbconlinePayments -> {
                        clickOnOnline(rbconlinePayments)
                    }
                    R.id.rbCreditcard -> {
                        clickOnCard(rbCreditcard)
                    }
                }
            }
        })


        var chequeValue = preferenceUtils.getIntFromPreference(PreferenceUtils.CHEQUE_DATE, 0)
        tvSelectDate.setOnClickListener {
            var calender: Calendar = Calendar.getInstance()
            val datePicker = DatePickerDialog(this@CreatePaymentActivity, datePickerListener, calender.get(Calendar.YEAR), calender.get(Calendar.MONTH), calender.get(Calendar.DAY_OF_MONTH))

            if (!datePicker.isShowing) {
                calender.add(Calendar.DAY_OF_MONTH, -chequeValue)
                var result = calender.getTime()
                datePicker.getDatePicker().setMinDate(result.time);
                datePicker.show()
            }

        }
        cheque = etChequeNo.text.toString().trim()
        bank = tvBankSelection.text.toString().trim()
        amount = etAmount.text.toString().trim()
        rgCredit.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {
                when (checkedId) {
                    R.id.rbCustomer -> {
                        chargeType=3
                        cardChecks()
                    }
                    R.id.rbBG -> {
                        chargeType=2

                        cardChecks()
                    }



                }
            }
        })

        btnPayments.setOnClickListener {
            Util.preventTwoClick(it)
            amount = etAmount.text.toString().trim()
            cheque = etChequeNo.text.toString().trim()
            bank = tvBankSelection.text.toString().trim()
            cardtype = tvCardSelection.text.toString().trim()

            site = tvSiteSelection.text.toString().trim()
            if (selectedInvoiceDOs != null && selectedInvoiceDOs.size > 0) {
                site = "Site"
            } else {

            }
            if (type == 10) {
                when {
                    site.isEmpty() -> showToast("Please select site")
                    amount.isEmpty() -> showToast("Please enter amount")
                    else -> customerAuthorization(0)
                }

            }
            else if (type == 50) {

                when {
                    site.isEmpty() -> showToast("Please select site")
                    amount.isEmpty() -> showToast("please enter amount")
                    cardtype.isEmpty() -> showToast("please select cardtype")

                    else -> customerAuthorization(0)
                }

            }else if(type==40){
                when {
                    amount.isEmpty() -> showToast("Please enter amount")
                    else -> paymentCreation()
                }
            } else{

                when {
                    site.isEmpty() -> showToast("Please select site")
                    cheque.isEmpty() -> showToast("Please enter cheque number")
                    chequeDate.isEmpty() -> showToast("Please select date")
                    amount.isEmpty() -> showToast("Please enter amount")
                    bank.isEmpty() -> showToast("Please select bank")
                    bitmap == null -> showToast("Please capture image")
                    else -> customerAuthorization(1)
                }

            }
        }

//        tvSelection.setOnClickListener {
//            selectInvoiceList()
//        }
        tvBankSelection.setOnClickListener {
            selectBankList()
        }
        tvSiteSelection.setOnClickListener {
            selectSiteList()
        }
        tvCardSelection.setOnClickListener {
            selectCardList()
        }
        etAmount.addTextChangedListener(textWatcher)


    }
    fun cardChecks() {
        if (type == 50 && !etAmount.text.toString().isNullOrEmpty()) {
            if (rbCustomer.isChecked) {
                var amount = java.lang.Double.parseDouble(etAmount.text.toString().toString())
                var service = preferenceUtils.getDoubleFromPreference(PreferenceUtils.SERVICE_CHARGES, 0.0)
                var total = amount /(100- service-service*5/100)*100
                var serviceCharge = (total *service/ 100)
                var vat = serviceCharge* 5 / 100

                tvInvoiceAmount.setText("" + etAmount.text.toString())
                tvServiceCharge.setText(String.format("%.2f", serviceCharge))
                tvItemVat.setText(String.format("%.2f", vat))
                tvItemTotal.setText(String.format("%.2f", total))
            } else {
                tvInvoiceAmount.setText("" + etAmount.text.toString())
                tvServiceCharge.setText("0.00 ")
                tvItemVat.setText("0.00 ")
                tvItemTotal.setText(etAmount.text.toString())
            }

        } else {

        }
    }
    fun selectCardList() {
        cardDialog = Dialog(this, R.style.NewDialog)
        cardDialog.setCancelable(true)
        cardDialog.setCanceledOnTouchOutside(true)
        cardDialog.setContentView(R.layout.simple_list_dialog)
        val window = cardDialog.getWindow()
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)

        var recyclerView = cardDialog.findViewById(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        recyclerView.setLayoutManager(androidx.recyclerview.widget.LinearLayoutManager(this));

        val siteListRequest = CardListRequest(this@CreatePaymentActivity)
        siteListRequest.setOnResultListener { isError, unPaidInvoiceMainDO ->
            hideLoader()
            if (isError) {
                Toast.makeText(this@CreatePaymentActivity, resources.getString(R.string.server_error), Toast.LENGTH_SHORT).show()
            } else {
                if (unPaidInvoiceMainDO != null) {


                    var siteAdapter = CardAdapter("COLLECTOR", this@CreatePaymentActivity, unPaidInvoiceMainDO.cardDOS, 3)
                    recyclerView.setAdapter(siteAdapter)
//                    var siteAdapter = CreatePaymentAdapter(this@CreatePaymentActivity, createPaymentDo)
//                    recyclerView.setAdapter(siteAdapter)
                } else {
                    hideLoader()
                    showAlert("" + resources.getString(R.string.server_error))
                }
            }


        }

        siteListRequest.execute()


        cardDialog.show()

    }

    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            var cardtype = tvCardSelection.text.toString()
            if (type == 50 && !s.isNullOrEmpty() && !cardtype.isNullOrEmpty()) {
                if (rbCustomer.isChecked) {
                    var amount = java.lang.Double.parseDouble(s.toString())
                    var service = preferenceUtils.getDoubleFromPreference(PreferenceUtils.SERVICE_CHARGES, 0.0)
                    var total = amount /(100- service-service*5/100)*100
                    var serviceCharge = (total *service/ 100)
                    var vat = serviceCharge* 5 / 100

                    tvInvoiceAmount.setText("" + s)
                    tvServiceCharge.setText(String.format("%.2f", serviceCharge))
                    tvItemVat.setText(String.format("%.2f", vat))
                    tvItemTotal.setText(String.format("%.2f", total))
                } else {
                    tvInvoiceAmount.setText("" + s+" ")
                    tvServiceCharge.setText("0.00 ")
                    tvItemVat.setText("0.00 ")
                    tvItemTotal.setText("" +s + " ")
                }

            } else if (type == 50 && !s.isNullOrEmpty() && cardtype.isNullOrEmpty()) {
                showToast("Please select Card type")
            } else if (type == 50 && s.isNullOrEmpty()) {
                tvInvoiceAmount.setText("" + 0)
                tvServiceCharge.setText("0.00 ")
                tvItemVat.setText("0.00 ")
                tvItemTotal.setText("" + 0)
            }
        }
    }
    private fun clickOnCard(rbCreditcard: RadioButton) {
        type = 50
        rbCreditcard.isChecked = true
        llCRCardUI.visibility = View.VISIBLE
        cardChecks()
        llAmount.setVisibility(View.VISIBLE)
        llChequeDate.setVisibility(View.GONE)
        llChequeNo.setVisibility(View.GONE)
        llCapture.setVisibility(View.GONE)
        llBankList.setVisibility(View.GONE)

        if (selectedInvoiceDOs != null && selectedInvoiceDOs.size > 0) {
            llSiteList.visibility = View.GONE
        } else {
            llSiteList.visibility = View.VISIBLE

        }

    }
    private fun clickOnOnline(rbOnline: RadioButton) {
        type = 40
        rbconlinePayments.isChecked = true
        llCRCardUI.visibility = View.GONE
        llSiteList.visibility = View.GONE
        llAmount.setVisibility(View.VISIBLE)
        llChequeDate.setVisibility(View.GONE)
        llChequeNo.setVisibility(View.GONE)
        llCapture.setVisibility(View.GONE)
        llBankList.setVisibility(View.GONE)


    }

    private fun clickOnCash(rbCash: RadioButton) {
        type = 10
        rbCash.isChecked = true
        llCRCardUI.visibility = View.GONE
//        llSiteList.visibility = View.VISIBLE

        etChequeNo.isEnabled = false
        etChequeNo.isClickable = false
        llChequeDate.setVisibility(View.GONE)
        llChequeNo.setVisibility(View.GONE)
        llCapture.setVisibility(View.GONE)
        llBankList.setVisibility(View.GONE)
        if (selectedInvoiceDOs != null && selectedInvoiceDOs.size > 0) {
            llSiteList.visibility = View.GONE
        } else {
            llSiteList.visibility = View.VISIBLE

        }
        etChequeNo.setText("")
        etChequeNo.setHint("Enter cheque number")
        tvSelectDate.isEnabled = false
        tvSelectDate.isClickable = false
        tvSelectDate.setText("")
        chequeDate = ""
        tvSelectDate.setHint("Select cheque date")
        tvBankSelection.isEnabled = true
        tvBankSelection.isClickable = true
        tvBankSelection.setText("")
        tvBankSelection.setHint("Select bank")
        ivCapturedPic.setBackgroundColor(resources.getColor(android.R.color.transparent))
        ivCapturedPic.setImageResource(R.drawable.gallary)
        ivCamera.isClickable = false
        ivCamera.isEnabled = false
        ivCapturedPic.isClickable = false
        ivCapturedPic.isEnabled = false
    }

    private fun customerAuthorization(type: Int) {
        if (intent.hasExtra("CODE")) {
            cid = intent.extras.getString("CODE")
        }

        if (type == 0) {
            if (cid != null && cid.length > 0) {
                customerManagement(cid, 13)

            } else {
//                customerManagement("", 13)
                paymentCreation()

            }
        } else {
            if (cid != null && cid.length > 0) {
                customerManagement(cid, 14)

            } else {
//                customerManagement("", 14)
                paymentCreation()

            }
        }
    }

    private fun clickOnCheque(rbCheque: RadioButton) {
        rbCheque.isChecked = true
        llCRCardUI.visibility = View.GONE

//        llSiteList.visibility = View.VISIBLE
        if (selectedInvoiceDOs != null && selectedInvoiceDOs.size > 0) {
            llSiteList.visibility = View.GONE
        } else {
            llSiteList.visibility = View.VISIBLE

        }
        type = 20
        tvSelectDate.isEnabled = true
        tvSelectDate.isClickable = true
        tvSelectDate.setText("")
        chequeDate = ""
        tvSelectDate.setHint("Select cheque date")
        llChequeDate.setVisibility(View.VISIBLE)
        llCapture.setVisibility(View.VISIBLE)
        llBankList.setVisibility(View.VISIBLE)

        llChequeNo.setVisibility(View.VISIBLE)
        etChequeNo.isEnabled = true
        etChequeNo.isClickable = true
        etChequeNo.setText("")
        etChequeNo.setHint("Enter cheque number")
        tvBankSelection.isEnabled = true
        tvBankSelection.isClickable = true
        tvBankSelection.setText("")
        tvBankSelection.setHint("Select bank")
        ivCamera.isClickable = true
        ivCamera.isEnabled = true
        ivCapturedPic.isClickable = true
        ivCapturedPic.isEnabled = true
    }

    private fun showChequePreview(bitmap: Bitmap) {
        if (bitmap != null) {
            val dialog = Dialog(this, R.style.NewDialog)
            dialog.setContentView(R.layout.cheque_img_preview_dialog)
            dialog.setCancelable(true)
            dialog.setCanceledOnTouchOutside(true)
            dialog.window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)
            val tvCloseable = dialog.findViewById(R.id.tvClose) as TextView
            val ivChequePreview = dialog.findViewById(R.id.ivChequePreview) as ImageView

            ivChequePreview.setImageBitmap(bitmap)
            tvCloseable.setOnClickListener {
                dialog.dismiss()
            }

            dialog.show()
        } else {
            showToast("Please capture pic for cheque")
        }
    }

    fun selectSiteList() {
        siteDialog = Dialog(this, R.style.NewDialog)
        siteDialog.setCancelable(true)
        siteDialog.setCanceledOnTouchOutside(true)
        siteDialog.setContentView(R.layout.simple_list_dialog)
        val window = siteDialog.getWindow()
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)

        var recyclerView = siteDialog.findViewById(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        recyclerView.setLayoutManager(androidx.recyclerview.widget.LinearLayoutManager(this));

        val siteListRequest = FinantialSiteListRequest(this@CreatePaymentActivity)
        siteListRequest.setOnResultListener { isError, unPaidInvoiceMainDO ->

            if (isError) {
                hideLoader()

                Toast.makeText(this@CreatePaymentActivity, resources.getString(R.string.server_error), Toast.LENGTH_SHORT).show()
            } else {
                if (unPaidInvoiceMainDO != null) {

                    var siteAdapter = FinantialSiteListAdapter("Collector", this@CreatePaymentActivity, unPaidInvoiceMainDO.finantialSiteDOS, 3)
                    recyclerView.setAdapter(siteAdapter)
//                    var siteAdapter = CreatePaymentAdapter(this@CreatePaymentActivity, createPaymentDo)
//                    recyclerView.setAdapter(siteAdapter)
                    hideLoader()

                } else {
                    hideLoader()
                    showAlert("" + resources.getString(R.string.server_error))
                }
            }

        }

        siteListRequest.execute()


        siteDialog.show()

    }

    private val datePickerListener: DatePickerDialog.OnDateSetListener = OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        cday = dayOfMonth
        cmonth = monthOfYear + 1
        cyear = year
        var monthString = cmonth.toString()
        if (monthString.length == 1) {
            monthString = "0$monthString"
        } else {
            monthString = cmonth.toString()
        }

        var dayString = cday.toString()
        if (dayString.length == 1) {
            dayString = "0$dayString"
        } else {
            dayString = cday.toString()
        }
        chequeDate = "" + cyear + monthString + dayString;
        tvSelectDate.setText("" + dayString + " - " + monthString + " - " + cyear)
    }

    private fun paymentCreation() {
        if(type==40){
           if( selectedCustomerDOs.get(0).email.isNotEmpty()){
               if (Util.isNetworkAvailable(this@CreatePaymentActivity)) {
                   showLoader()

                   val onlinePayment = OnlinePayments(this, "",selectedInvoiceDOs, selectedCustomerDOs.get(0).customerId,
                           selectedCustomerDOs.get(0).customerName, amount.toDouble(), selectedCustomerDOs.get(0).email)
                   onlinePayment.setOnResultListener(OnlinePayments.OnResultListener() { isError, successDO ->
                       if (isError) {
                           hideLoader()
                           showAppCompatAlert("Info!", "Error in Service Response, Please try again...", "Ok", "", "", false)

                       } else {
                           hideLoader()

                           if (successDO.flag === 1) {
                               showAppCompatAlert("Info!", "Payment Created Successfully...", "Ok", "", "ONLINE", false)
                           }else{
                               showAppCompatAlert("Info!", "Error in Service Response, Please try again...", "Ok", "", "", false)

                           }
                       }
                   })
                   onlinePayment.execute()
               } else {
                   showAlert("Internet Unavailable ! Please connect to Internet.")

               }
           }else{
               hideLoader()
               showAlertEmail()
           }

        }
        else{
            if (!lattitudeFused.isNullOrEmpty()) {
                stopLocationUpdates()
                if(type==50){
                    amount = tvItemTotal.text.toString().trim()

                }else{
                    amount = etAmount.text.toString().trim()

                }

                if (selectedInvoiceDOs != null && selectedInvoiceDOs.size > 0) {
                    val siteMultipleListRequest = CreateMultiplePaymentRequest(chargeType,cardtype,
                            tvServiceCharge.text.toString().replace("AED","").trim().toDouble(),tvItemVat.text.toString().replace("AED","").trim().toDouble()
                            ,lattitudeFused, longitudeFused, addressFused, selectedInvoiceDOs, cheque, chequeDate, bank, amount.toDouble(), type, encImage, this@CreatePaymentActivity)
                    siteMultipleListRequest.setOnResultListener { isError, createPaymentDO ->
                        hideLoader()

                        if (isError) {
                            showAppCompatAlert("Info!", "Error in Service Response, Please try again...", "Ok", "", "", false)
                        } else {
                            if (createPaymentDO != null) {
                                createPaymentDo = createPaymentDO
                                if (createPaymentDO.paymentNumber.length > 0) {
                                    stopLocUpdates()

                                    preferenceUtils.saveString(PreferenceUtils.PAYMENT_INVOICE_ID, createPaymentDO.invoiceNumber)
                                    preferenceUtils.saveString(PreferenceUtils.PAYMENT_ID, createPaymentDO.paymentNumber)
//                            showAppCompatAlert("Success", "Payment Created - " + createPaymentDO.paymentNumber, "Ok", "", "SUCCESS", false)
                                    showToast("Payment Created - " + createPaymentDO.paymentNumber)

//                            preparePaymentCreation();
                                    preParePaymentCreation()
                                    podDo.payment = CalendarUtils.getCurrentDate();
                                    StorageManager.getInstance(this).saveDepartureData(this, podDo)
                                    btnPayments.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                                    btnPayments.setClickable(false)
                                    btnPayments.setEnabled(false)

                                } else if (createPaymentDO.message.length > 0) {
                                    showAppCompatAlert("Error!", " " + createPaymentDO.message, "Ok", "", "", false)

                                } else {
                                    showAppCompatAlert("Error", "Payment Not Created, please try again.", "Ok", "", "failed", false)

                                }
                            } else {
                                showAppCompatAlert("Error", "Payment Not Created!", "Ok", "", "fail", false)
                            }
                        }


                    }

                    siteMultipleListRequest.execute()

                } else {
                    if (payId.length > 0 && selectedCustomerDOs.size < 2) {
                        val siteListRequest = CreateMiscelleneousPaymentRequest(chargeType,cardtype,
                                tvServiceCharge.text.toString().replace("AED","").trim().toDouble(),tvItemVat.text.toString().replace("AED","").trim().toDouble()
                                ,lattitudeFused, longitudeFused, addressFused, selectedCustomerDOs.get(0).customerId, cheque, chequeDate, bank, java.lang.Double.parseDouble(amount), type, encImage, this@CreatePaymentActivity)
                        siteListRequest.setOnResultListener { isError, createPaymentDO ->
                            hideLoader()

                            if (isError) {
                                showAppCompatAlert("Info!", "Error in Service Response, Please try again...", "Ok", "", "", false)
                            } else {
                                if (createPaymentDO != null) {
                                    createPaymentDo = createPaymentDO
                                    if (createPaymentDO.paymentNumber.length > 0) {
                                        stopLocUpdates()

                                        preferenceUtils.saveString(PreferenceUtils.PAYMENT_INVOICE_ID, createPaymentDO.invoiceNumber)
                                        preferenceUtils.saveString(PreferenceUtils.PAYMENT_ID, createPaymentDO.paymentNumber)
//                            showAppCompatAlert("Success", "Payment Created - " + createPaymentDO.paymentNumber, "Ok", "", "SUCCESS", false)
                                        showToast("Payment Created - " + createPaymentDO.paymentNumber)

//                            preparePaymentCreation();
                                        preParePaymentCreation()
                                        podDo.payment = CalendarUtils.getCurrentDate();
                                        StorageManager.getInstance(this).saveDepartureData(this, podDo)
                                        btnPayments.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                                        btnPayments.setClickable(false)
                                        btnPayments.setEnabled(false)

                                    } else if (createPaymentDO.message.length > 0) {
                                        showAppCompatAlert("Error", " " + createPaymentDO.message, "Ok", "", "", false)

                                    } else {
                                        showAppCompatAlert("Error", "Payment Not Created, please try again.", "Ok", "", "failed", false)

                                    }
                                } else {
                                    showAppCompatAlert("Error", "Payment Not Created!", "Ok", "", "fail", false)
                                }
                                // Toast.makeText(this@CreatePaymentActivity, "Payment Created", Toast.LENGTH_SHORT).show()

//                    var siteAdapter = CreatePaymentAdapter(this@CreatePaymentActivity, createPaymentDo)
//                    recyclerView.setAdapter(siteAdapter)

                            }


                        }

                        siteListRequest.execute()
                    } else {

                        siteListRequest2 = CreateMiscelleneousPaymentRequest(chargeType,cardtype,
                                tvServiceCharge.text.toString().replace("AED","").trim().toDouble(),tvItemVat.text.toString().replace("AED","").trim().toDouble()
                                ,lattitudeFused, longitudeFused, addressFused, selectedCustomerDOs.get(0).customerId, cheque, chequeDate, bank, java.lang.Double.parseDouble(amount), type, encImage, this@CreatePaymentActivity)

                        siteListRequest2.setOnResultListener { isError, createPaymentDO ->
                            hideLoader()

                            if (isError) {
                                showAppCompatAlert("Info!", "Error in Service Response, Please try again...", "Ok", "", "", false)
                            } else {
                                if (createPaymentDO != null) {
                                    createPaymentDo = createPaymentDO
                                    if (createPaymentDO.paymentNumber.length > 0) {
                                        stopLocUpdates()
                                        preferenceUtils.saveString(PreferenceUtils.PAYMENT_INVOICE_ID, createPaymentDO.invoiceNumber)
                                        preferenceUtils.saveString(PreferenceUtils.PAYMENT_ID, createPaymentDO.paymentNumber)
//                            showAppCompatAlert("Success", "Payment Created - " + createPaymentDO.paymentNumber, "Ok", "", "SUCCESS", false)
                                        showToast("Payment Created - " + createPaymentDO.paymentNumber)

//                            preparePaymentCreation();
                                        preParePaymentCreation()
                                        podDo.payment = CalendarUtils.getCurrentDate();
                                        StorageManager.getInstance(this).saveDepartureData(this, podDo)
                                        btnPayments.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                                        btnPayments.setClickable(false)
                                        btnPayments.setEnabled(false)

                                    } else if (createPaymentDO.message.length > 0) {
                                        showAppCompatAlert("Error", " " + createPaymentDO.message, "Ok", "", "", false)

                                    } else {
                                        showAppCompatAlert("Error", "Payment Not Created, please try again.", "Ok", "", "failed", false)

                                    }
                                } else {
                                    showAppCompatAlert("Error", "Payment Not Created!", "Ok", "", "fail", false)
                                }

                                // Toast.makeText(this@CreatePaymentActivity, "Payment Created", Toast.LENGTH_SHORT).show()

//                    var siteAdapter = CreatePaymentAdapter(this@CreatePaymentActivity, createPaymentDo)
//                    recyclerView.setAdapter(siteAdapter)

                            }

                        }

                        siteListRequest2.execute()
                    }
                }
            } else {
                showLoader()
                locationFetch()
//            showAppCompatAlert("", resources.getString(R.string.location_capture), "OK", "", "", false)
            }
        }



    }

    private fun customerManagement(id: String, type: Int) {
        if (Util.isNetworkAvailable(this)) {
            val request = CustomerManagementRequest(id, type, this)
            request.setOnResultListener { isError, customerDo ->
                hideLoader()
                if (customerDo != null) {
                    if (isError) {
                        showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
                    } else {
                        if (customerDo.flag.equals("Customer authorized for this transaction", true)) {
                            paymentCreation()
                        } else {
                            showAppCompatAlert("Alert!", "Customer not authorized for this transaction", "Ok", "", "", false)

                        }
                    }
                } else {
                    showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
                }
            }
            request.execute()
        } else {
            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "", false)

        }


    }


    fun selectBankList() {
        bankDialog = Dialog(this, R.style.NewDialog)
        bankDialog.setCancelable(true)
        bankDialog.setCanceledOnTouchOutside(true)
        bankDialog.setContentView(R.layout.simple_list_dialog)
        val window = bankDialog.getWindow()
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)
        var data = preferenceUtils.getStringFromPreference(PreferenceUtils.Reason_Payment, "")
//            tvInvoiceId.setText("" + data)
//            tvInvoiceId.setOnClickListener({
//                preferenceUtils.saveString(PreferenceUtils.SITE_NAME, tvInvoiceId.text.toString())
//                tvSelection.text = "" + preferenceUtils.getStringFromPreference(PreferenceUtils.SITE_NAME, "")
//                dialog.dismiss()
//            })
        var site = preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID, "")

        var recyclerView = bankDialog.findViewById(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        recyclerView.setLayoutManager(androidx.recyclerview.widget.LinearLayoutManager(this));

        val siteListRequest = BankListRequest(site, this@CreatePaymentActivity)
        siteListRequest.setOnResultListener { isError, unPaidInvoiceMainDO ->
            hideLoader()

            if (isError) {
                Toast.makeText(this@CreatePaymentActivity, resources.getString(R.string.server_error), Toast.LENGTH_SHORT).show()
            } else {
                if (unPaidInvoiceMainDO != null) {

                    var siteAdapter = BankListAdapter(this@CreatePaymentActivity, unPaidInvoiceMainDO.bankDOS, 2)
                    recyclerView.setAdapter(siteAdapter)
//                    var siteAdapter = CreatePaymentAdapter(this@CreatePaymentActivity, createPaymentDo)
//                    recyclerView.setAdapter(siteAdapter)

                } else {
                    hideLoader()
                    showAlert("" + resources.getString(R.string.server_error))
                }
            }

        }

        siteListRequest.execute()


        bankDialog.show()

    }

    override fun onButtonYesClick(from: String) {

        if ("SUCCESS".equals(from, ignoreCase = true)) {
            finish()
        }
        if ("ONLINE".equals(from, ignoreCase = true)) {
            val intent = Intent()
            setResult(1, intent)
            finish()
        }
    }

    override fun onButtonNoClick(from: String) {

        if ("SUCCESS".equals(from, ignoreCase = true)) {
            finish()
        }
    }


    private fun preParePaymentCreation() {
        var id = preferenceUtils.getStringFromPreference(PreferenceUtils.PAYMENT_ID, "")
        if (id.length > 0) {
            if (Util.isNetworkAvailable(this)) {
                val siteListRequest = PDFPaymentDetailsRequest(id, this)
                siteListRequest.setOnResultListener { isError, createPDFInvoiceDO ->
                    hideLoader()
                    if (createPDFInvoiceDO != null) {
                        if (isError) {
                            showAppCompatAlert("Error", "Payment pdf api error", "Ok", "", "", false)
                        } else {
                            createPDFpaymentDO = createPDFInvoiceDO
                            if (createPDFInvoiceDO != null) {
                                PaymentReceiptPDF(this).prepareCollectorPDF(createPDFInvoiceDO, "Collector")
                            } else {
                                //Display error message
                            }
                        }
                    } else {
                        showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
                    }
                }
                siteListRequest.execute()
            } else {
                showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "", false)
            }


        } else {
            showAppCompatAlert("Error", "No Payment Id found", "Ok", "", "", false)

        }

    }


    private fun pairedDevices() {
        val pairedDevices = BluetoothAdapter.getDefaultAdapter().getBondedDevices()
        if (pairedDevices.size > 0) {
            for (device in pairedDevices) {
                val deviceName = device.getName()
                val mac = device.getAddress() // MAC address
//                if (StorageManager.getInstance(this).getPrinterMac(this).equals("")) {
                StorageManager.getInstance(this).savePrinterMac(this, mac);
//                }
                Log.e("Bluetooth", "Name : " + deviceName + " Mac : " + mac)
                break
            }
        }
    }

    override fun onBackPressed() {
        hideLoader()
        val intent = Intent()
        setResult(1, intent)
        finish()
        super.onBackPressed()

    }

    fun locationFetch() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mSettingsClient = LocationServices.getSettingsClient(this)

        mLocationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                super.onLocationResult(locationResult)
                // location is received
                mCurrentLocation = locationResult!!.lastLocation
                mLastUpdateTime = DateFormat.getTimeInstance().format(Date())

                updateLocUI()
            }
        }

        mRequestingLocationUpdates = false

        mLocationRequest = LocationRequest()
        mLocationRequest!!.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS)
        mLocationRequest!!.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS)
        mLocationRequest!!.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)

        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(mLocationRequest!!)
        mLocationSettingsRequest = builder.build()

        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(object : PermissionListener {
                    override fun onPermissionRationaleShouldBeShown(permission: com.karumi.dexter.listener.PermissionRequest?, token: PermissionToken?) {
                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }

                    override fun onPermissionGranted(response: PermissionGrantedResponse) {
                        mRequestingLocationUpdates = true
                        startLocUpdates()
                    }

                    override fun onPermissionDenied(response: PermissionDeniedResponse) {
                        if (response.isPermanentlyDenied()) {

                            showToast("Location Permission denied")
                            // open device settings when the permission is
                            // denied permanently
//                            openSettings()
                        }
                    }

                    fun onPermissionRationaleShouldBeShown(permission: PermissionRequest, token: PermissionToken) {
                        token.continuePermissionRequest()
                    }
                }).check()
    }

    fun updateLocUI() {
        if (mCurrentLocation != null) {
            stopLocUpdates()
            hideLoader()
            paymentCreation()

            lattitudeFused = mCurrentLocation!!.getLatitude().toString()
            longitudeFused = mCurrentLocation!!.getLongitude().toString()
            var gcd = Geocoder(getBaseContext(), Locale.getDefault());

            try {
                var addresses = gcd.getFromLocation(mCurrentLocation!!.getLatitude(),
                        mCurrentLocation!!.getLongitude(), 1);
                if (addresses.size > 0) {
                    System.out.println(addresses.get(0).getLocality());
                    var cityName = addresses.get(0).getLocality();
                    addressFused = cityName
                }
            } catch (e: java.lang.Exception) {
                e.printStackTrace();
            }


        }

    }

    @SuppressLint("MissingPermission")
    private fun startLocUpdates() {
        mSettingsClient!!
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this) {
                    //                    Log.i(TAG, "All location settings are satisfied.")

//                    Toast.makeText(applicationContext, "Started location updates!", Toast.LENGTH_SHORT).show()


                    mFusedLocationClient!!.requestLocationUpdates(mLocationRequest,
                            mLocationCallback, Looper.myLooper())

                    updateLocUI()
                }
                .addOnFailureListener(this) { e ->
                    val statusCode = (e as ApiException).statusCode
                    when (statusCode) {
                        LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
//                            Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " + "location settings ")
                            try {
                                // Show the dialog by calling startResolutionForResult(), and check the
                                // result in onActivityResult().
                                val rae = e as ResolvableApiException
                                rae.startResolutionForResult(this@CreatePaymentActivity, REQUEST_CHECK_SETTINGS)
                            } catch (sie: IntentSender.SendIntentException) {
//                                Log.i(TAG, "PendingIntent unable to execute request.")
                            }

                        }
                        LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                            val errorMessage = "Location settings are inadequate, and cannot be " + "fixed here. Fix in Settings."
//                            Log.e(TAG, errorMessage)

                            Toast.makeText(this@CreatePaymentActivity, errorMessage, Toast.LENGTH_LONG).show()
                        }
                    }

                    updateLocUI()
                }
    }

    fun stopLocUpdates() {
        // Removing location updates
        if (mFusedLocationClient != null && mLocationCallback != null) {
            mFusedLocationClient!!
                    .removeLocationUpdates(mLocationCallback)
                    .addOnCompleteListener(this) {

                    }
        }

    }
    fun showAlertEmail() {
        val dialogg = Dialog(this@CreatePaymentActivity)
        dialogg.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogg.setCancelable(false)
        dialogg.setContentView(R.layout.email_dialog)
        val text = dialogg.findViewById<View>(R.id.macInput) as EditText
        val cancel = dialogg.findViewById<View>(R.id.cancel) as Button
        cancel.setOnClickListener { dialogg.dismiss() }
        val dialogButton = dialogg.findViewById<View>(R.id.print) as Button
        dialogButton.setOnClickListener {
            val emal = text.text.toString()
            if (!TextUtils.isEmpty(emal)) {
                if (Util.isValidEmailId(emal)) {
                    dialogg.dismiss()

                    if (Util.isNetworkAvailable(this@CreatePaymentActivity)) {
                        showLoader()

                        val onlinePayment = OnlinePayments(this, "",selectedInvoiceDOs, selectedCustomerDOs.get(0).customerId,
                                selectedCustomerDOs.get(0).customerName, amount.toDouble(), emal)
                        onlinePayment.setOnResultListener(OnlinePayments.OnResultListener() { isError, successDO ->
                            if (isError) {
                                hideLoader()
                                showAppCompatAlert("Info!", "Error in Service Response, Please try again...", "Ok", "", "", false)

                            } else {
                                hideLoader()

                                if (successDO.flag === 1) {
                                    showAppCompatAlert("Info!", "Payment Created Successfully...", "Ok", "", "ONLINE", false)
                                }else{
                                    showAppCompatAlert("Info!", "Error in Service Response, Please try again...", "Ok", "", "", false)

                                }
                            }
                        })
                        onlinePayment.execute()
                    } else {
                        showAlert("Internet Unavailable ! Please connect to Internet.")

                    }
                } else {
                    Toast.makeText(this@CreatePaymentActivity, "Please provide valid email id", Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(this@CreatePaymentActivity, "Please provide email id", Toast.LENGTH_SHORT).show()
            }
        }
        dialogg.show()
    }

}