package com.tbs.brothersgas.haadhir.collector

import android.app.DatePickerDialog
import android.app.Notification
import android.content.Intent
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.brothersgas.haadhir.Activitys.BaseActivity
import com.tbs.brothersgas.haadhir.Adapters.CreatePaymentAdapter
import com.tbs.brothersgas.haadhir.Model.CustomerDo
import com.tbs.brothersgas.haadhir.Model.LoadStockDO
import com.tbs.brothersgas.haadhir.Model.UnPaidInvoiceDO
import com.tbs.brothersgas.haadhir.Model.UnPaidInvoiceMainDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.AllUnPaidInvoicesListRequest
import com.tbs.brothersgas.haadhir.Requests.UnPaidInvoicesListRequest
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.utils.CalendarUtils
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.Objects.compare


class InvoiceListActivity : BaseActivity() {
    private var invoiceAdapter: com.tbs.brothersgas.haadhir.collector.CreatePaymentAdapter = com.tbs.brothersgas.haadhir.collector.CreatePaymentAdapter(this, ArrayList(), "", "")
    lateinit var loadStockDOs: ArrayList<LoadStockDO>
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var linearLayoutManager: androidx.recyclerview.widget.LinearLayoutManager
    private var cday: Int = 0
    private var cmonth: Int = 0
    private var cyear: Int = 0
    private lateinit var siteListRequest2: CollectorAllUnPaidInvoicesListRequest
    lateinit var tvAmount: TextView
    lateinit var tvNoData: TextView
    lateinit var tvFrom: TextView
    lateinit var btnSearch: Button
    lateinit var ivGoBack: ImageView
    lateinit var ivShowAll: Button
    lateinit var ivSelAll: ImageView

    lateinit var tvTO: TextView
    lateinit var tvNoDataFound: TextView
    lateinit var tvPaidAmount: TextView
    private var unPaidInvoiceMainDO: UnPaidInvoiceMainDO = UnPaidInvoiceMainDO()
    lateinit var view: LinearLayout
    private var fromId = 0
    private lateinit var siteListRequest: UnPaidInvoicesListRequest
    private var from: String = ""
    private var customerid = ""
    private lateinit var btnAddMore: Button
    private var selectedCustomerDOs = ArrayList<CustomerDo>()
    private var selectedDateDOs = ArrayList<UnPaidInvoiceDO>()

    var startDate = ""
    var endDate = ""
    var to = ""
    override protected fun onResume() {
        super.onResume()
//        selectInvoiceList()
    }

    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.credit_invoice_list, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
//        ivSelAll.setVisibility(View.VISIBLE)
        flToolbar.visibility = View.GONE

        initializeControls()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            val intent = Intent()

            setResult(1, intent)
            finish()
        }


        btnSearch.setOnClickListener {
            var title = btnSearch.text.toString()
            if (title.contentEquals("Search")) {
                var totalAmount = 0.0
                var frm = tvFrom.text.toString().trim();
                var to = tvTO.text.toString().trim()
                if (frm.isNotEmpty() && to.isNotEmpty()) {
//                selectedDateDOs.removeAll(ArrayList<UnPaidInvoiceDO>())
                    selectedDateDOs.clear()
                    val dates = ArrayList<Date>()

                    val formatter: DateFormat
                    formatter = SimpleDateFormat("yyyyMMdd")

                    val startDate = formatter.parse(startDate) as Date
                    val endDate = formatter.parse(endDate) as Date
                    val interval = (24 * 1000 * 60 * 60).toLong() // 1 hour in millis
                    val endTime = endDate.time // create your endtime here, possibly using Calendar or Date
                    var curTime = startDate.time
                    while (curTime <= endTime) {
                        dates.add(Date(curTime))
                        curTime += interval
                    }
                    for (i in dates.indices) {
                        val ds = formatter.format(dates[i])
                        println(" Date is ...$ds")
                    }
                    var isProductExisted = false;

                    for (i in unPaidInvoiceMainDO.unPaidInvoiceDOS.indices) {
                        for (k in dates.indices) {
                            val ds = formatter.format(dates[k])

                            if (unPaidInvoiceMainDO.unPaidInvoiceDOS.get(i).accountingDate.contains("$ds", true)) {
                                isProductExisted = true
                                totalAmount = totalAmount + unPaidInvoiceMainDO.unPaidInvoiceDOS.get(i).outstandingAmount

                                break
                            }
                        }
                        if (isProductExisted) {
                            isProductExisted = false
                            selectedDateDOs!!.add(unPaidInvoiceMainDO.unPaidInvoiceDOS.get(i))
                            println(selectedDateDOs)

                            continue
                        } else {

                            println(" Date is ...$selectedDateDOs")

                        }
                    }
                    Collections.sort(selectedDateDOs, object:Comparator<UnPaidInvoiceDO> {
                        override fun compare(o1:UnPaidInvoiceDO, o2:UnPaidInvoiceDO):Int {
                            return o1.accountingDate.compareTo(o2.accountingDate)
                        }
                    })
                    Collections.reverse(selectedDateDOs)
//                    Collections.sort(selectedDateDOs);
                    invoiceAdapter = com.tbs.brothersgas.haadhir.collector.CreatePaymentAdapter(this@InvoiceListActivity, selectedDateDOs, customerid, from)
                    recycleview.setAdapter(invoiceAdapter)

                    if (unPaidInvoiceMainDO.unPaidInvoiceDOS.size > 0) {
                        tvAmount.setText("Total Amount : " + String.format("%.2f", totalAmount) + " " + unPaidInvoiceMainDO.unPaidInvoiceDOS.get(0).currency)
                        tvAmount.setVisibility(View.VISIBLE)
                        tvPaidAmount.setVisibility(View.VISIBLE)
                        preferenceUtils.saveString(PreferenceUtils.TOTAL_AMOUNT, String.format("%.2f", totalAmount))

                    }

                    preferenceUtils.saveInt(PreferenceUtils.PAYMENT_TYPE, unPaidInvoiceMainDO.type)
                    btnSearch.setText("Clear")

                } else {
                    showToast("Please select From and To dates")
//                    btnSearch.setText("Search")

                }
            } else {
                tvFrom.setText("")
                tvTO.setText("")

                tvFrom.setHint("From :")
                tvTO.setHint("To : ")
                btnSearch.setText("Search")
                invoiceAdapter = com.tbs.brothersgas.haadhir.collector.CreatePaymentAdapter(this@InvoiceListActivity, unPaidInvoiceMainDO.unPaidInvoiceDOS, customerid, from)
                recycleview.setAdapter(invoiceAdapter)
                if (unPaidInvoiceMainDO.unPaidInvoiceDOS.size > 0) {
                    tvAmount.setText("Total Amount : " + String.format("%.2f", unPaidInvoiceMainDO.totalAmount) + " " + unPaidInvoiceMainDO.unPaidInvoiceDOS.get(0).currency)

                }


            }

        }
        val c = Calendar.getInstance()
        val cyear = c.get(Calendar.YEAR)
        val cmonth = c.get(Calendar.MONTH)
        val cday = c.get(Calendar.DAY_OF_MONTH)
        tvFrom.setOnClickListener {
            val datePicker = DatePickerDialog(this@InvoiceListActivity, datePickerListener, cyear, cmonth, cday)

            datePicker.show()
        }

        tvTO.setOnClickListener {
            val datePicker = DatePickerDialog(this@InvoiceListActivity, endDatePickerListener, cyear, cmonth, cday)

            datePicker.show()
        }

        ivSelAll.setTag(false)
        ivSelAll.setOnClickListener {
            if (selectedDateDOs.isEmpty()) {
                if (unPaidInvoiceMainDO != null && unPaidInvoiceMainDO.unPaidInvoiceDOS != null && unPaidInvoiceMainDO.unPaidInvoiceDOS.size > 0) {
                    if (ivSelAll.getTag() as Boolean) {
                        ivSelAll.setTag(false)
                        ivSelAll.setImageResource(R.drawable.select_all)// change image as checked
                        if (unPaidInvoiceMainDO != null && unPaidInvoiceMainDO.unPaidInvoiceDOS != null && unPaidInvoiceMainDO.unPaidInvoiceDOS.size > 0) {
                            for (i in unPaidInvoiceMainDO.unPaidInvoiceDOS.indices) {
                                unPaidInvoiceMainDO.unPaidInvoiceDOS.get(i).isSelected = false
                            }
                            invoiceAdapter = com.tbs.brothersgas.haadhir.collector.CreatePaymentAdapter(this@InvoiceListActivity, unPaidInvoiceMainDO.unPaidInvoiceDOS, customerid, from)
                            recycleview.setAdapter(invoiceAdapter)
//                    recycleview.smoothScrollToPosition(invoiceAdapter.getItemCount() - 1);
                        }
                    } else {
                        ivSelAll.setTag(true)
                        ivSelAll.setImageResource(R.drawable.selected)
//                change image like unchecked
                        if (unPaidInvoiceMainDO != null && unPaidInvoiceMainDO.unPaidInvoiceDOS != null && unPaidInvoiceMainDO.unPaidInvoiceDOS.size > 0) {
                            for (i in unPaidInvoiceMainDO.unPaidInvoiceDOS.indices) {
                                unPaidInvoiceMainDO.unPaidInvoiceDOS.get(i).isSelected = true
                            }
                            invoiceAdapter = com.tbs.brothersgas.haadhir.collector.CreatePaymentAdapter(this@InvoiceListActivity, unPaidInvoiceMainDO.unPaidInvoiceDOS, customerid, from)
                            recycleview.setAdapter(invoiceAdapter)
//                    recycleview.smoothScrollToPosition(invoiceAdapter.getItemCount() - 1);

                        }
                    }
                }

            } else {
                if (unPaidInvoiceMainDO != null && selectedDateDOs != null && selectedDateDOs.size > 0) {
                    var totalAmount = 0.0
                    if (ivSelAll.getTag() as Boolean) {
                        ivSelAll.setTag(false)
                        ivSelAll.setImageResource(R.drawable.select_all)// change image as checked
                        if (unPaidInvoiceMainDO != null && selectedDateDOs != null && selectedDateDOs.size > 0) {
                            for (i in selectedDateDOs.indices) {
                                selectedDateDOs.get(i).isSelected = false
                            }
                            invoiceAdapter = com.tbs.brothersgas.haadhir.collector.CreatePaymentAdapter(this@InvoiceListActivity, selectedDateDOs, customerid, from)
                            recycleview.setAdapter(invoiceAdapter)
//                    recycleview.smoothScrollToPosition(invoiceAdapter.getItemCount() - 1);
                        }
                    } else {
                        ivSelAll.setTag(true)
                        ivSelAll.setImageResource(R.drawable.selected)
//                change image like unchecked
                        if (unPaidInvoiceMainDO != null && selectedDateDOs != null && selectedDateDOs.size > 0) {
                            for (i in selectedDateDOs.indices) {
                                selectedDateDOs.get(i).isSelected = true
                                totalAmount = totalAmount + selectedDateDOs.get(i).outstandingAmount

                            }
                            invoiceAdapter = com.tbs.brothersgas.haadhir.collector.CreatePaymentAdapter(this@InvoiceListActivity, selectedDateDOs, customerid, from)
                            recycleview.setAdapter(invoiceAdapter)
//                    recycleview.smoothScrollToPosition(invoiceAdapter.getItemCount() - 1);

                        }
                    }
                    tvAmount.setText("Total Amount : " + String.format("%.2f", totalAmount) + " " + unPaidInvoiceMainDO.unPaidInvoiceDOS.get(0).currency)
                }

            }

        }

    }

    override fun initializeControls() {
        tvScreenTitle.setText("")
        ivSelAll = findViewById(R.id.ivSelAll) as ImageView
        ivGoBack = findViewById(R.id.ivGoBack) as ImageView
        ivGoBack.setOnClickListener {
            val intent = Intent()

            setResult(1, intent)
            finish()
        }
        ivShowAll = findViewById(R.id.ivShowAll) as Button

        recycleview = findViewById(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        tvNoDataFound = findViewById(R.id.tvNoDataFound) as TextView
        view = findViewById(R.id.view) as LinearLayout
        btnAddMore = findViewById(R.id.btnAddMore) as Button
        btnSearch = findViewById(R.id.btnSearch) as Button
        tvFrom = findViewById(R.id.tvFrom) as TextView
        tvTO = findViewById(R.id.tvTO) as TextView
        tvNoData = findViewById(R.id.tvNoDataFound) as TextView
        tvAmount = findViewById(R.id.tvAmount) as TextView
        tvPaidAmount = findViewById(R.id.tvPaidAmount) as TextView
        if (intent.hasExtra("selectedCustomerDOs")) {
            selectedCustomerDOs = intent.getSerializableExtra("selectedCustomerDOs") as ArrayList<CustomerDo>
        }
        btnSearch.setText("Search")
        recycleview.setLayoutManager(linearLayoutManager)
        if (selectedCustomerDOs.size == 1) {
            ivShowAll.visibility = View.VISIBLE

        }

        ivShowAll.setOnClickListener {
            selectALLInvoiceList()
        }
        selectInvoiceList()
        if (intent.hasExtra("CODE")) {
            customerid = intent.extras.getString("CODE")
        }
        val btnCreate = findViewById(R.id.btnCreate) as Button

        btnCreate.setOnClickListener {
            insertContactWrapper()
            if (invoiceAdapter != null) {
                val selectedUnPaidInvoiceDOS = invoiceAdapter.getselectedUnPaidInvoiceDOS();
                if (selectedUnPaidInvoiceDOS != null && !selectedUnPaidInvoiceDOS.isEmpty()) {

                    val intent = Intent(this@InvoiceListActivity, com.tbs.brothersgas.haadhir.collector.CreatePaymentActivity::class.java);
                    intent.putExtra("selectedInvoiceDOs", selectedUnPaidInvoiceDOS);
                    intent.putExtra("Sales", 1)
                    intent.putExtra("CODE", "")
                    intent.putExtra("selectedCustomerDOs", selectedCustomerDOs);

                    startActivityForResult(intent, 11);
                } else {
                    if (selectedCustomerDOs.size > 1) {
                        showToast("Please select Invoices")

                    } else {
                        val intent = Intent(this@InvoiceListActivity, com.tbs.brothersgas.haadhir.collector.CreatePaymentActivity::class.java);
                        intent.putExtra("Sales", 1)
                        intent.putExtra("CODE", "")
                        intent.putExtra("selectedCustomerDOs", selectedCustomerDOs);

                        startActivityForResult(intent, 11);
                    }
                }
            } else {
                val intent = Intent(this@InvoiceListActivity, com.tbs.brothersgas.haadhir.collector.CreatePaymentActivity::class.java);
                intent.putExtra("Sales", 1)
                intent.putExtra("CODE", "")
                intent.putExtra("selectedCustomerDOs", selectedCustomerDOs);

                startActivityForResult(intent, 11); }
        }
        btnAddMore.setOnClickListener {

            if (invoiceAdapter != null) {
                val selectedUnPaidInvoiceDOS = invoiceAdapter.getselectedUnPaidInvoiceDOS();
                if (selectedUnPaidInvoiceDOS != null && !selectedUnPaidInvoiceDOS.isEmpty()) {
                    val intent = Intent(this@InvoiceListActivity, com.tbs.brothersgas.haadhir.collector.CreatePaymentActivity::class.java);
                    intent.putExtra("selectedInvoiceDOs", selectedUnPaidInvoiceDOS);
                    intent.putExtra("Sales", 1)
                    intent.putExtra("CODE", "")
                    intent.putExtra("selectedCustomerDOs", selectedCustomerDOs);

                    startActivityForResult(intent, 11);
                } else {
                    showToast("Please select Invoices")
                }
            } else {
                showToast("No Invoices found")
            }

        }
    }

    private fun selectALLInvoiceList() {
//
//        val activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)
//        val customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)

        if (Util.isNetworkAvailable(this)) {

            showLoader()
            siteListRequest2 = CollectorAllUnPaidInvoicesListRequest(selectedCustomerDOs, this@InvoiceListActivity)

            siteListRequest2.setOnResultListener { isError, unPaidInvoiceMainDO ->
                if (isError) {
                    hideLoader()

                    Toast.makeText(this@InvoiceListActivity, resources.getString(R.string.error_NoData), Toast.LENGTH_SHORT).show()
                } else {
                    if (unPaidInvoiceMainDO != null && unPaidInvoiceMainDO.unPaidInvoiceDOS.size > 0) {
                        tvAmount.setVisibility(View.VISIBLE)
                        tvAmount.setText("" + String.format("%.2f", unPaidInvoiceMainDO.totalAmount) + " " + unPaidInvoiceMainDO.unPaidInvoiceDOS.get(0).currency)
                        preferenceUtils.saveString(PreferenceUtils.TOTAL_AMOUNT, String.format("%.2f", unPaidInvoiceMainDO.totalAmount))
                        preferenceUtils.saveInt(PreferenceUtils.PAYMENT_TYPE, unPaidInvoiceMainDO.type)

                        tvNoData.setVisibility(View.GONE)
                        hideLoader()

                    } else {
                        tvAmount.setVisibility(View.GONE)

                        tvNoData.setVisibility(View.VISIBLE)
                        recycleview.setVisibility(View.GONE)
                        view.setVisibility(View.VISIBLE)

                        hideLoader()

                    }
//                    tvAmount.setVisibility(View.VISIBLE)
//                    view.setVisibility(View.VISIBLE)
//                    recycleview.setVisibility(View.VISIBLE)
//
////                        var siteAdapter = CreatePaymentAdapter(this@InvoiceListActivity, unPaidInvoiceMainDO.unPaidInvoiceDOS)
////                        recycleview.setAdapter(siteAdapter)
//                    var invoiceAdapter = com.tbs.brothersgas.haadhir.collector.CreatePaymentAdapter(this@InvoiceListActivity, unPaidInvoiceMainDO.unPaidInvoiceDOS, customerid, "CREDIT")
//                    recycleview.setAdapter(invoiceAdapter)
//                    tvAmount.setText("Total Amount : " + String.format("%.2f", unPaidInvoiceMainDO.totalAmount) + " " + unPaidInvoiceMainDO.unPaidInvoiceDOS.get(0).currency)
//                    preferenceUtils.saveString(PreferenceUtils.TOTAL_AMOUNT, "" + String.format("%.2f", unPaidInvoiceMainDO.totalAmount))
//
////                    var siteAdapter = CreatePaymentAdapter(this@CreatePaymentActivity, createPaymentDo)
////                    recyclerView.setAdapter(siteAdapter)f

                }


            }

            siteListRequest2.execute()
        } else {
            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "", false)

        }


    }


    private fun selectInvoiceList() {


//        var tvInvoiceId = findViewById(R.id.tvInvoiceId) as TextView


        recycleview.setLayoutManager(androidx.recyclerview.widget.LinearLayoutManager(this));
        val activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)
        val customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)

        if (intent.hasExtra("CODE")) {
            customerid = intent.extras.getString("CODE")
        }

        val ShipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, getResources().getString(R.string.checkin_non_scheduled))



        if (selectedCustomerDOs != null && selectedCustomerDOs.size > 0) {
            var multisiteListRequest = CrediUnPaidInvoicesListRequest(selectedCustomerDOs, this@InvoiceListActivity)

            multisiteListRequest.setOnResultListener { isError, unPaidInvoiceMainDo ->
                hideLoader()
                if (unPaidInvoiceMainDo != null && unPaidInvoiceMainDo.unPaidInvoiceDOS.size > 0) {
                    unPaidInvoiceMainDO = unPaidInvoiceMainDo;
//                    tvInvoiceId.setVisibility(View.VISIBLE)
                    tvAmount.setVisibility(View.VISIBLE)
                    tvAmount.setText("Total Amount : " + String.format("%.2f", unPaidInvoiceMainDo.totalAmount) + " " + unPaidInvoiceMainDo.unPaidInvoiceDOS.get(0).currency)
                    tvPaidAmount.setVisibility(View.VISIBLE)

                    preferenceUtils.saveString(PreferenceUtils.TOTAL_AMOUNT, String.format("%.2f", unPaidInvoiceMainDo.totalAmount))
                    preferenceUtils.saveInt(PreferenceUtils.PAYMENT_TYPE, unPaidInvoiceMainDo.type)


                    tvNoData.setVisibility(View.GONE)
                    if (isError) {
                        hideLoader()

                        Toast.makeText(this@InvoiceListActivity, resources.getString(R.string.error_NoData), Toast.LENGTH_SHORT).show()
                    } else {
                        hideLoader()
                        tvPaidAmount.setVisibility(View.VISIBLE)

                        tvAmount.setVisibility(View.VISIBLE)
                        view.setVisibility(View.VISIBLE)
                        this.from = "CREDIT";
                        invoiceAdapter = com.tbs.brothersgas.haadhir.collector.CreatePaymentAdapter(this@InvoiceListActivity, unPaidInvoiceMainDo.unPaidInvoiceDOS, customerid, "CREDIT")
                        recycleview.setAdapter(invoiceAdapter)
                        tvAmount.setText("Total Amount : " + String.format("%.2f", unPaidInvoiceMainDo.totalAmount) + " " + unPaidInvoiceMainDo.unPaidInvoiceDOS.get(0).currency)
                        preferenceUtils.saveString(PreferenceUtils.TOTAL_AMOUNT, "" + String.format("%.2f", unPaidInvoiceMainDo.totalAmount))

//                    var siteAdapter = CreatePaymentAdapter(this@CreatePaymentActivity, createPaymentDo)
//                    recyclerView.setAdapter(siteAdapter)f

                    }
                } else {
//                tvInvoiceId.setVisibility(View.VISIBLE)
                    tvAmount.setVisibility(View.GONE)
                    tvPaidAmount.setVisibility(View.GONE)

                    tvNoData.setVisibility(View.VISIBLE)
                    recycleview.setVisibility(View.GONE)
                    view.setVisibility(View.VISIBLE)

                    hideLoader()

                }

            }

            multisiteListRequest.execute()
        } else {
            var siteListRequest = UnPaidInvoicesListRequest(customerid, this@InvoiceListActivity)

            siteListRequest.setOnResultListener { isError, unPaidInvoiceMainDo ->
                hideLoader()
                this.unPaidInvoiceMainDO = unPaidInvoiceMainDo;

                if (unPaidInvoiceMainDO != null && unPaidInvoiceMainDo.unPaidInvoiceDOS.size > 0) {
                    this.unPaidInvoiceMainDO = unPaidInvoiceMainDo;
//                    tvInvoiceId.setVisibility(View.VISIBLE)
                    tvAmount.setVisibility(View.VISIBLE)
                    tvAmount.setText("Total Amount : " + String.format("%.2f", unPaidInvoiceMainDo.totalAmount) + " " + unPaidInvoiceMainDo.unPaidInvoiceDOS.get(0).currency)

                    preferenceUtils.saveString(PreferenceUtils.TOTAL_AMOUNT, String.format("%.2f", unPaidInvoiceMainDo.totalAmount))
                    preferenceUtils.saveInt(PreferenceUtils.PAYMENT_TYPE, unPaidInvoiceMainDo.type)


                    tvNoData.setVisibility(View.GONE)
                    if (isError) {
                        hideLoader()

                        Toast.makeText(this@InvoiceListActivity, resources.getString(R.string.error_NoData), Toast.LENGTH_SHORT).show()
                    } else {
                        hideLoader()

                        tvAmount.setVisibility(View.VISIBLE)
                        view.setVisibility(View.VISIBLE)
                        this.from = "";
                        invoiceAdapter = com.tbs.brothersgas.haadhir.collector.CreatePaymentAdapter(this@InvoiceListActivity, unPaidInvoiceMainDo.unPaidInvoiceDOS, customerid, "")
                        recycleview.setAdapter(invoiceAdapter)
                        tvAmount.setText("Total Amount : " + String.format("%.2f", unPaidInvoiceMainDo.totalAmount) + " " + unPaidInvoiceMainDo.unPaidInvoiceDOS.get(0).currency)
                        preferenceUtils.saveString(PreferenceUtils.TOTAL_AMOUNT, "" + String.format("%.2f", unPaidInvoiceMainDo.totalAmount))

//                    var siteAdapter = CreatePaymentAdapter(this@CreatePaymentActivity, createPaymentDo)
//                    recyclerView.setAdapter(siteAdapter)f

                    }
                } else {
//                tvInvoiceId.setVisibility(View.VISIBLE)
                    tvAmount.setVisibility(View.GONE)
                    tvPaidAmount.setVisibility(View.GONE)

                    tvNoData.setVisibility(View.VISIBLE)
                    recycleview.setVisibility(View.GONE)
                    view.setVisibility(View.VISIBLE)

                    hideLoader()

                }

            }

            siteListRequest.execute()
        }


    }

    private val datePickerListener: DatePickerDialog.OnDateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        cday = dayOfMonth
        cmonth = monthOfYear + 1
        cyear = year
        var monthString = cmonth.toString()
        if (monthString.length == 1) {
            monthString = "0$monthString"
        } else {
            monthString = cmonth.toString()
        }

        var dayString = cday.toString()
        if (dayString.length == 1) {
            dayString = "0$dayString"
        } else {
            dayString = cday.toString()
        }
        startDate = "" + cyear + monthString + dayString;
        tvFrom.setText("" + dayString + "-" + monthString + "-" + cyear)
        btnSearch.setText("Search")
//        from = tvFrom.text.toString()

    }
    private val endDatePickerListener: DatePickerDialog.OnDateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        cday = dayOfMonth
        cmonth = monthOfYear + 1
        cyear = year
        var monthString = cmonth.toString()
        if (monthString.length == 1) {
            monthString = "0$monthString"
        } else {
            monthString = cmonth.toString()
        }

        var dayString = cday.toString()
        if (dayString.length == 1) {
            dayString = "0$dayString"
        } else {
            dayString = cday.toString()
        }
        endDate = "" + cyear + monthString + dayString;
        tvTO.setText("" + dayString + "-" + monthString + "-" + cyear)
        to = tvTO.text.toString()
        btnSearch.setText("Search")

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 11 && resultCode == 1) {
            selectInvoiceList()
        }
    }

    override fun onBackPressed() {
        val intent = Intent()

        setResult(1, intent)
        finish()
        super.onBackPressed()
    }
}