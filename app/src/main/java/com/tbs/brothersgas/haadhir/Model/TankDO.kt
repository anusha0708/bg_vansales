package com.tbs.brothersgas.haadhir.Model

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.ArrayList

/**
 *Created by kishoreganji on 21-12-2018.
 *Copyright (C) 2018 TBS - All Rights Reserved
 */
data class TankDO(

        @SerializedName("tank1") var tank1: String?                                               = "",
        @SerializedName("tank2") var tank2: String?                                               = "",
        @SerializedName("tank3") var tank3: String?                                               = "",
        @SerializedName("tank4") var tank4: String?                                               = "",
        @SerializedName("tank5") var tank5: String?                                               = "",
        @SerializedName("tank6") var tank6: String?                                               = "",
        @SerializedName("tank7") var tank7: String?                                               = "",
        @SerializedName("tank8") var tank8: String?                                               = "",
        @SerializedName("tank9") var tank9: String?                                               = "",
        @SerializedName("tank10") var tank10: String?                                               = "",
        @SerializedName("tank11") var tank11: String?                                               = "",
        @SerializedName("tank12") var tank12: String?                                               = "",
        @SerializedName("tank13") var tank13: String?                                               = "",
        @SerializedName("tank14") var tank14: String?                                               = "",
        @SerializedName("tank15") var tank15: String?                                               = "",
        @SerializedName("tank16") var tank16: String?                                               = "",
        @SerializedName("tank17") var tank17: String?                                               = "",
        @SerializedName("tank18") var tank18: String?                                               = "",
        @SerializedName("tank19") var tank19: String?                                               = "",
        @SerializedName("tank20") var tank20: String?                                               = ""

        ) : Serializable