package com.tbs.brothersgas.haadhir.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;


import com.tbs.brothersgas.haadhir.Activitys.BaseActivity;
import com.tbs.brothersgas.haadhir.Model.CustomerDetailsDo;
import com.tbs.brothersgas.haadhir.Model.CustomerDetailsMainDo;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;
import com.tbs.brothersgas.haadhir.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class CustomerDetailsRequest extends AsyncTask<String, Void, Boolean> {

    private CustomerDetailsMainDo customerDetailsMainDo;
    private CustomerDetailsDo customerDetailsDo;
    private Context mContext;
    private String id;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;

    public CustomerDetailsRequest(String id, Context mContext) {

        this.mContext = mContext;
        this.id = id;
    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        public void onCompleted(boolean isError, CustomerDetailsMainDo customerDetailsMainDo);

    }

    public boolean runRequest() {
        // System.out.println("CUSTOMER ID " + customerId);
        String NAMESPACE = "http://www.adonix.com/WSS";
        String METHOD_NAME = "run";
        String SOAP_ACTION = "CAdxWebServiceXmlCC";
        preferenceUtils = new PreferenceUtils(mContext);
        username = preferenceUtils.getStringFromPreference(PreferenceUtils.A_USER_NAME, "");
        password = preferenceUtils.getStringFromPreference(PreferenceUtils.A_PASSWORD, "");
        ip = preferenceUtils.getStringFromPreference(PreferenceUtils.IP_ADDRESS, "");
        port = preferenceUtils.getStringFromPreference(PreferenceUtils.PORT, "");
        pool = preferenceUtils.getStringFromPreference(PreferenceUtils.ALIAS, "");
        String URL = "http://" + ip + ":" + port + "/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC";

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        // Set all input params
        request.addProperty("publicName", WebServiceConstants.SPOT_CUSTOMER_DETAILS);
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("I_YBPCNUM", id);
            jsonObject.put("I_YBPAADD", preferenceUtils.getStringFromPreference(PreferenceUtils.PRODUCT_CODE, ""));

        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        request.addProperty("inputXml", jsonObject.toString());

        SoapObject callcontext = new SoapObject("", "callContext");
        // Set all input params
        callcontext.addProperty("codeLang", "ENG");
        callcontext.addProperty("poolAlias", pool);
        callcontext.addProperty("poolId", "");
        callcontext.addProperty("codeUser", username);
        callcontext.addProperty("password", password);
        callcontext.addProperty("requestConfig", "adxwss.trace.on=off");

        request.addSoapObject(callcontext);


        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.setOutputSoapObject(request);

        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
        androidHttpTransport.debug = true;

        try {
            List<HeaderProperty> headerList = new ArrayList<HeaderProperty>();
            headerList.add(new HeaderProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode((username + ":" + password).getBytes())));


            androidHttpTransport.call(SOAP_ACTION, envelope, headerList);


            SoapObject response = (SoapObject) envelope.getResponse();
            String resultXML = (String) response.getProperty("resultXml");
            if (resultXML != null && resultXML.length() > 0) {
                return parseXML(resultXML);
            } else {
                return false;
            }
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            customerDetailsMainDo = new CustomerDetailsMainDo();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        customerDetailsMainDo.customerDetailsDos = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        customerDetailsDo = new CustomerDetailsDo();

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {
//                        if (attribute.equalsIgnoreCase("I_YBPCNUM")) {
//                            customerDetailsDo.number(text);
//                            customerDetailsDos.add(customerDetailsDo);
//
//                        }else

                        if (attribute.equalsIgnoreCase("O_YBCGCOD")) {
                            if (text.length() > 0) {

                                customerDetailsMainDo.name = text;
                            }


                        } else if (attribute.equalsIgnoreCase("O_YBPCSHO")) {
                            if (text.length() > 0) {

                                customerDetailsMainDo.descrption = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YBUS")) {
                            if (text.length() > 0) {

                                customerDetailsMainDo.buisinessLine=text;
                            }


                        }
                        else if (attribute.equalsIgnoreCase("O_YSAT")) {
                            if (text.length() > 0) {

                                customerDetailsDo.emirates=text;
                            }


                        }
                        else if (attribute.equalsIgnoreCase("O_YCUR")) {
                            if (text.length() > 0) {

                                customerDetailsMainDo.currency(text);
                            }


                        } else if (attribute.equalsIgnoreCase("O_YBPDADD")) {
                            if (text.length() > 0) {

                                customerDetailsMainDo.address(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YBPCINV")) {
                            if (text.length() > 0) {

                                customerDetailsMainDo.bill(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YBPAINV")) {
                            if (text.length() > 0) {

                                customerDetailsMainDo.billAddress = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YBPCPYR")) {
                            if (text.length() > 0) {

                                customerDetailsMainDo.pay = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YBPAPYR")) {
                            if (text.length() > 0) {

                                customerDetailsMainDo.payByAddress(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YACCCOD")) {
                            if (text.length() > 0) {

                                customerDetailsMainDo.code(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YVACBPR")) {
                            if (text.length() > 0) {

                                customerDetailsMainDo.rule(text);
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_XGEOX")) {
                            if (text.length() > 0) {

                                customerDetailsMainDo.lattitude=text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_XGEOY")) {
                            if (text.length() > 0) {

                                customerDetailsMainDo.longitude=text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YPTE")) {
                            if (text.length() > 0) {

                                customerDetailsMainDo.term(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YBPADDLIG")) {
                            if (text.length() > 0) {

                                customerDetailsDo.addressLine(text);
                            }


                        }
                        else if (attribute.equalsIgnoreCase("O_YBPADES")) {
                            if (text.length() > 0) {

                                customerDetailsDo.addressDescription(text);
                            }


                        } else if (attribute.equalsIgnoreCase("O_YPOSCOD")) {
                            if (text.length() > 0) {

                                customerDetailsDo.postalCode(text);
                            }



                        } else if (attribute.equalsIgnoreCase("O_YCTY")) {
                            if (text.length() > 0) {

                                customerDetailsDo.city(text);
                            }



                        } else if (attribute.equalsIgnoreCase("O_YTEL")) {
                            if (text.length() > 0) {

                                customerDetailsDo.telephone(text);
                            }



                        } else if (attribute.equalsIgnoreCase("O_YMOB")) {
                            if (text.length() > 0) {

                                customerDetailsDo.mobile(text);
                            }



                        }

                        text = "";
                    }


                    if (endTag.equalsIgnoreCase("GRP")) {
                        // customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        ((BaseActivity) mContext).showLoader();

        // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        ((BaseActivity) mContext).hideLoader();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, customerDetailsMainDo);
        }
    }
}