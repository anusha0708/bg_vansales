package com.tbs.brothersgas.haadhir.utils;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/*
 * Created by developer on 22/2/19.
 */
public interface Constants {

    String FB_TABLE_USERS = "table_users";
    String USERTIMINGS = "user_timings";
    String CURRENTTIME="current_time";
    String SCREEN_TYPE = "screen_type";
    String SHIPMENT_ID = "shipment_id";
    String CUSTOMER_CODE = "customer_code";
    String IS_FROM_DELIVERY = "is_from_delivery";
    String IS_FROM_POD = "IS_FROM_POD";

}
