package com.tbs.brothersgas.haadhir.Activitys

import android.graphics.BitmapFactory
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.bumptech.glide.Glide


//
class ViewScannedImageActivity : BaseActivity() {
     var ivSign: ImageView? = null

    override  protected fun onResume() {
        super.onResume()
    }
    override fun initialize() {
      var  llCategories = getLayoutInflater().inflate(R.layout.alerts_screen, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
    }
    override fun initializeControls() {
        tvScreenTitle.setText("View Image")
        ivSign = findViewById<View>(R.id.ivSign) as ImageView
        var podDo = StorageManager.getInstance(this).getDepartureData(this);
        val path =  podDo.signature
        val bmp = BitmapFactory.decodeFile(path)

        Glide.with(this)
                .load<Any>(path)
                .into(ivSign)
    }
}