package com.tbs.brothersgas.haadhir.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;


import com.google.firebase.iid.FirebaseInstanceId;
import com.tbs.brothersgas.haadhir.Activitys.BaseActivity;
import com.tbs.brothersgas.haadhir.Model.LoginDO;
import com.tbs.brothersgas.haadhir.utils.LogUtils;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;
import com.tbs.brothersgas.haadhir.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;


public class AdminLoginRequest extends AsyncTask<String, Void, Boolean> {
    boolean isValid = false;
    private Context mContext;
    String username, passWord, ip, pool, port, pwd;
    PreferenceUtils preferenceUtils;
    LoginDO loginDO;
    String rol;

    public AdminLoginRequest(String role,Context mContext) {
        this.rol = role;

        this.mContext = mContext;
    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        public void onCompleted(boolean isError, boolean isValid, LoginDO loginDO);

    }

    public boolean runRequest(String userID, String pwd) {

        preferenceUtils = new PreferenceUtils(mContext);
        username = preferenceUtils.getStringFromPreference(PreferenceUtils.A_USER_NAME, "");
        passWord = preferenceUtils.getStringFromPreference(PreferenceUtils.A_PASSWORD, "");
        ip = preferenceUtils.getStringFromPreference(PreferenceUtils.IP_ADDRESS, "");
        port = preferenceUtils.getStringFromPreference(PreferenceUtils.PORT, "");
        pool = preferenceUtils.getStringFromPreference(PreferenceUtils.ALIAS, "");


        String NAMESPACE = "http://www.adonix.com/WSS";
        String METHOD_NAME = "run";
        String SOAP_ACTION = "CAdxWebServiceXmlCC";
//        String URL = "http://183.82.9.23:8124/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC";
        String URL = "http://" + ip + ":" + port + "/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC";

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        // Set all input params
        request.addProperty("publicName", WebServiceConstants.LOGIN);

        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("XUNAME", userID);
            jsonObject.put("XPWD", pwd);
            if(rol.isEmpty()){
                jsonObject.put("I_XPROFILE", 10);

            }else {
                jsonObject.put("I_XPROFILE", rol);

            }

            Log.e("FirebaseInstanceId",FirebaseInstanceId.getInstance().getToken());
            jsonObject.put("XFCMKEY", FirebaseInstanceId.getInstance().getToken());

        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        request.addProperty("inputXml", jsonObject.toString());

        SoapObject callcontext = new SoapObject("", "callContext");
        // Set all input params
        callcontext.addProperty("codeLang", "ENG");
        callcontext.addProperty("poolAlias", pool);
        callcontext.addProperty("poolId", "");
        callcontext.addProperty("codeUser", username);
        callcontext.addProperty("password", passWord);
        callcontext.addProperty("requestConfig", "adxwss.trace.on=off");

        request.addSoapObject(callcontext);


        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.setOutputSoapObject(request);

        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
        androidHttpTransport.debug = true;

        try {
            List<HeaderProperty> headerList = new ArrayList<HeaderProperty>();
            headerList.add(new HeaderProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode((username + ":" + passWord).getBytes())));


            androidHttpTransport.call(SOAP_ACTION, envelope, headerList);


            SoapObject response = (SoapObject) envelope.getResponse();
            String resultXML = (String) response.getProperty("resultXml");
            if (resultXML != null && resultXML.length() > 0) {
                return parseXML(resultXML);
            } else {
                return false;
            }
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        Log.d("XML_STRING_Login-->", xmlString);
        try {
            String text = "", attribute = "", startTag = "", attribute2 = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();
            loginDO = new LoginDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    if (startTag != null && startTag.equalsIgnoreCase("FLD")) {
                        if (attribute.equalsIgnoreCase("XFLG")) {
                            if (text.equalsIgnoreCase("1")) {
                                isValid = true;
                            }
                            loginDO.flag = Integer.parseInt(text);
                        } else if (attribute.equalsIgnoreCase("O_YDIMG")) {
                            if (loginDO.Image.equalsIgnoreCase(""))
                                loginDO.Image = text;

                        } else if (attribute.equalsIgnoreCase("XUNAME")) {

                            loginDO.driverCode = text;

                        } else if (attribute.equalsIgnoreCase("XDNAME")) {

                            loginDO.driverName = text;

                        } else if (attribute.equalsIgnoreCase("XFCY")) {

                            loginDO.site = text;

                        } else if (attribute.equalsIgnoreCase("XFCYDES")) {

                            loginDO.siteDescription = text;

                        } else if (attribute.equalsIgnoreCase("XROLE")) {
                            if (text.length() > 0) {
                                loginDO.role = Integer.parseInt(text);
                            }

                        } else if (attribute.equalsIgnoreCase("XCPY")) {

                            loginDO.company = text;

                        } else if (attribute.equalsIgnoreCase("XCPYDES")) {

                            loginDO.companyDescription = text;

                        } else if (attribute.equalsIgnoreCase("O_XCHQDAT")) {

                            loginDO.chequeDays = Integer.parseInt(text);

                        } else if (attribute.equalsIgnoreCase("XEMAIL")) {
                            if (text.length() > 0) {
                                loginDO.email = text;

                            }

                        } else if (attribute.equalsIgnoreCase("O_XAPPVERSION")) {
                            if (text.length() > 0) {
                                loginDO.version = text;

                            }

                        } else if (attribute.equalsIgnoreCase("O_XPCFLG")) {
                            if (text.length() > 0) {
                                loginDO.authorized = Integer.parseInt(text);

                            }

                        }
                        text = "";
                    }
                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }
                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception " + e);

            return false;
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        ((BaseActivity) mContext).showLoader();
        //  ProgressTask.getInstance().showProgress(mContext, false, "Checking Credentials...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest(param[0], param[1]);
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        ((BaseActivity) mContext).hideLoader();
        if (onResultListener != null) {
            if (loginDO != null) {

                onResultListener.onCompleted(!result, isValid, loginDO);
            } else {
                if (preferenceUtils.getStringFromPreference(PreferenceUtils.IP_ADDRESS, "").isEmpty()) {
                    ((BaseActivity) mContext).showAppCompatAlert("Alert!", "Please Finish Configuration First", "OK", "", "FAILURE", false);

                } else {

                    ((BaseActivity) mContext).showAppCompatAlert("Alert!", "Server Error..please try again", "OK", "", "FAILURE", false);
                }
            }
        }
    }
}