package com.tbs.brothersgas.haadhir.Activitys

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.app.Dialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.*
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.os.Looper
import android.provider.MediaStore
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Base64
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import android.webkit.PermissionRequest
import android.widget.*
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.gson.Gson
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.single.PermissionListener
import com.tbs.brothersgas.haadhir.Adapters.BankListAdapter
import com.tbs.brothersgas.haadhir.Adapters.CardAdapter
import com.tbs.brothersgas.haadhir.Model.*
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.*
import com.tbs.brothersgas.haadhir.background.InvoiceEmailService
import com.tbs.brothersgas.haadhir.background.PaymentEmailService
import com.tbs.brothersgas.haadhir.common.AppConstants
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.listeners.ResultListner
import com.tbs.brothersgas.haadhir.pdfs.PaymentReceiptPDF
import com.tbs.brothersgas.haadhir.prints.PrinterConnection
import com.tbs.brothersgas.haadhir.prints.PrinterConstants
import com.tbs.brothersgas.haadhir.utils.CalendarUtils
import com.tbs.brothersgas.haadhir.utils.LogUtils
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import kotlinx.android.synthetic.main.create_payments_screen.*
import kotlinx.android.synthetic.main.include_cheque.*
import kotlinx.android.synthetic.main.include_credit_card.*
import java.io.ByteArrayOutputStream
import java.io.FileInputStream
import java.text.DateFormat
import java.util.*
import kotlin.collections.ArrayList


class CreatePaymentActivity : BaseActivity() {
    private var mLastClickTime: Long = 0

    var picUri: Uri? = null

    lateinit var etChequeNo: EditText
    lateinit var tvBankSelection: TextView
    lateinit var dialog: Dialog
    lateinit var bankDialog: Dialog
    lateinit var etAmount: EditText
    private var calender: Calendar = Calendar.getInstance()
    lateinit var podDo: PodDo
    var encImage = ""
    lateinit var createPDFpaymentDO: PaymentPdfDO
    private lateinit var llBankList: LinearLayout
    lateinit var tvCardSelection: TextView
    lateinit var tvCharge: TextView
    private lateinit var ivCapturedPic: ImageView
    private lateinit var ivCamera: ImageView
    private lateinit var llChequeDate: LinearLayout
    private lateinit var llChequeNo: LinearLayout
    private lateinit var llView: LinearLayout
    private lateinit var cardtype: String

    lateinit var btnPayments: Button
    public lateinit var tvSelection: TextView
    private lateinit var cheque: String
    private lateinit var bank: String
    private lateinit var amount: String
    private lateinit var tvCurrency: TextView
    private lateinit var tvSelectDate: TextView
    private lateinit var createPaymentDo: CreatePaymentDO
    private lateinit var unpaidMainDo: UnPaidInvoiceMainDO
    private lateinit var llCapture: LinearLayout
    private lateinit var siteListRequest: UnPaidInvoicesListRequest
    private lateinit var llAmount: LinearLayout
    private var price: kotlin.Double = 0.0
    lateinit var cardDialog: Dialog

    private var cday: Int = 0
    private var cmonth: Int = 0
    private var cyear: Int = 0
    private var type: Int = 10
    private var chargeType: Int = 3

    private var paymentType: Int = 0
    internal var isImageCaptured = false
    internal var imagePath = ""
    private var fromId = 0
    private var totalAmount = ""

    private var bitmap: Bitmap? = null
    private var chequeDate: String = ""

    private var mLastUpdateTime: String? = null
    private val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 10000
    private val FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS: Long = 5000
    private val REQUEST_CHECK_SETTINGS = 100
    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private var mSettingsClient: SettingsClient? = null
    private var mLocationRequest: LocationRequest? = null
    private var mLocationSettingsRequest: LocationSettingsRequest? = null
    private var mLocationCallback: LocationCallback? = null
    private var mCurrentLocation: Location? = null
    private var mRequestingLocationUpdates: Boolean? = null


    override fun initialize() {
        val llCategories = getLayoutInflater().inflate(R.layout.create_payments_screen, null) as ScrollView
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            val intent = Intent()
            setResult(1, intent)
            finish()
        }
        disableMenuWithBackButton()
        initializeControls()
        totalAmount = preferenceUtils.getStringFromPreference(PreferenceUtils.TOTAL_AMOUNT, "")

        if (intent.hasExtra("Sales")) {
            fromId = intent!!.extras!!.getInt("Sales")
        }
        if (fromId == 1) {
            llView.setVisibility(View.GONE)
            etAmount.setText(totalAmount)

        } else {
            llView.setVisibility(View.VISIBLE)
            val data = preferenceUtils.getStringFromPreference(PreferenceUtils.INVOICE_ID, "")
            tvSelection.setText("" + data)
            etAmount.setText(preferenceUtils.getStringFromPreference(PreferenceUtils.INVOICE_AMOUNT, ""))

        }
        preferenceUtils.removeFromPreference(PreferenceUtils.CARD_TYPE)
        preferenceUtils.removeFromPreference(PreferenceUtils.SERVICE_CHARGES)
        podDo = StorageManager.getInstance(this).getDepartureData(this);
        if (!podDo.payment.equals("")) {

//            btnPayments.setBackgroundColor(resources.getColor(R.color.md_gray_light))
//            btnPayments.setClickable(false)
//            btnPayments.setEnabled(false)

            var selectedId = tvSelection.text.toString().trim()


            var invoiceId = preferenceUtils.getStringFromPreference(PreferenceUtils.PAYMENT_INVOICE_ID, "")
            if (invoiceId.length > 0 && selectedId.equals(invoiceId)) {
//                btnPayments.setBackgroundColor(resources.getColor(R.color.md_gray_light))
//                btnPayments.setClickable(false)
//                btnPayments.setEnabled(false)

            } else {
                btnPayments.setBackgroundColor(resources.getColor(R.color.md_green))
                btnPayments.setClickable(true)
                btnPayments.setEnabled(true)
            }
        } else {
            btnPayments.setBackgroundColor(resources.getColor(R.color.md_green))
            btnPayments.setClickable(true)
            btnPayments.setEnabled(true)
        }
//
//        if (!podDo.payment.equals("")) {
//
//            btnPayments.setBackgroundColor(resources.getColor(R.color.md_gray_light))
//            btnPayments.setClickable(false)
//            btnPayments.setEnabled(false)
//            var selectedId= tvSelection.text.toString().trim()
//
//            var deliveryId = preferenceUtils.getStringFromPreference(PreferenceUtils.PAYMENT_INVOICE_ID, "")
//            if(deliveryId.length>0&&selectedId.equals(deliveryId)){
//                btnPayments.setBackgroundColor(resources.getColor(R.color.md_gray_light))
//                btnPayments.setClickable(false)
//                btnPayments.setEnabled(false)
//
//            }else{
//                btnPayments.setBackgroundColor(resources.getColor(R.color.md_green))
//                btnPayments.setClickable(true)
//                btnPayments.setEnabled(true)
//            }
//        }else{
//            btnPayments.setBackgroundColor(resources.getColor(R.color.md_green))
//            btnPayments.setClickable(true)
//            btnPayments.setEnabled(true)
//        }


//        bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.gallary);
        cmonth = calender.get(Calendar.MONTH)
        cday = calender.get(Calendar.YEAR)
        cday = calender.get(Calendar.DAY_OF_MONTH)


        ivCamera.setOnClickListener {
            try {
                Util.makeFolder(Environment.getExternalStorageDirectory().toString(), "/BrothersGas")
                AppConstants.sdCardPath = Environment.getExternalStorageDirectory().toString() + "/BrothersGas"
                Util.selectImageDialog(this@CreatePaymentActivity, "Cheque Image")
            } catch (e: Exception) {
                e.printStackTrace()
            }


        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AppConstants.CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {

            if (resultCode == Activity.RESULT_OK) {
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                    try {
                        val fis = FileInputStream(AppConstants.file1)
                        bitmap = BitmapFactory.decodeStream(fis)
                        val baos = ByteArrayOutputStream()
                        bitmap!!.compress(Bitmap.CompressFormat.JPEG, 75, baos)
                        val bytes = baos.toByteArray()
                        encImage = Base64.encodeToString(bytes, Base64.DEFAULT)
                        ivCapturedPic.setImageBitmap(bitmap)
                        preferenceUtils.saveString(PreferenceUtils.CAPTURE, encImage)

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    // now you can upload your image file
                } else {
                    imagePath = AppConstants.imageUri.getPath()!!
                    AppConstants.tempPath = AppConstants.imageUri.getPath()
                    isImageCaptured = true
                    val options = BitmapFactory.Options()
                    options.inSampleSize = 8
                    bitmap = BitmapFactory.decodeFile(AppConstants.tempPath, options)
                    val bdrawable = BitmapDrawable(resources, bitmap)

                    ivCapturedPic.setImageBitmap(bitmap)
                    val baos = ByteArrayOutputStream()
                    bitmap!!.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                    val bytes = baos.toByteArray()
                    encImage = Base64.encodeToString(bytes, Base64.DEFAULT)
                }


//                cheaqueImage.setText("")
            } else if (resultCode == Activity.RESULT_CANCELED) {

                Toast.makeText(this, " Picture was not taken ", Toast.LENGTH_SHORT).show()
            } else {

                Toast.makeText(this, " Picture was not taken ", Toast.LENGTH_SHORT).show()
            }
        } else if (requestCode == AppConstants.SELECT_FILE) {
            if (resultCode == Activity.RESULT_OK) {
                val selectedImage = data!!.data
                val filePath = arrayOf(MediaStore.Images.Media.DATA)
                val c = contentResolver.query(selectedImage!!, filePath, null, null, null)
                c!!.moveToFirst()
                val columnIndex = c.getColumnIndex(filePath[0])
                AppConstants.tempPath = c.getString(columnIndex)
                imagePath = c.getString(columnIndex)
                c.close()
                isImageCaptured = true
                val options = BitmapFactory.Options()
                options.inSampleSize = 8
                bitmap = BitmapFactory.decodeFile(AppConstants.tempPath, options)
                val bdrawable = BitmapDrawable(resources, bitmap)
                ivCapturedPic.setImageBitmap(bitmap)
                val baos = ByteArrayOutputStream()
                bitmap!!.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                val bytes = baos.toByteArray()
                encImage = Base64.encodeToString(bytes, Base64.DEFAULT)
            } else {
                Toast.makeText(this, " This Image cannot be stored .please try with some other Image. ", Toast.LENGTH_SHORT).show()
            }

        }
        if (requestCode == 1 && resultCode == 1) {
            val intent = Intent()
            setResult(1, intent)
            finish()
        }
    }

    @SuppressLint("MissingPermission")
    override fun initializeControls() {
        tvScreenTitle.setText(R.string.payment)
        location()
        tvSelection = findViewById(R.id.tvSelection) as TextView
        llView = findViewById(R.id.llView) as LinearLayout
        tvBankSelection = findViewById(R.id.tvBankSelection) as TextView
        etChequeNo = findViewById(R.id.etChequeNo) as EditText
        etAmount = findViewById(R.id.etAmount) as EditText
        tvCurrency = findViewById(R.id.tvCurrency) as TextView
        tvSelectDate = findViewById(R.id.tvSelectDate) as TextView
        btnPayments = findViewById(R.id.btnPayments) as Button
        ivCapturedPic = findViewById(R.id.ivCapturedPic) as ImageView
        llBankList = findViewById(R.id.llBankList) as LinearLayout
        tvCardSelection = findViewById(R.id.tvCardSelection) as TextView
        tvCharge = findViewById(R.id.tvCreditCharge) as TextView

        ivCamera = findViewById(R.id.ivCamera) as ImageView
        llChequeDate = findViewById(R.id.llChequeDate) as LinearLayout
        llChequeNo = findViewById(R.id.llChequeNo) as LinearLayout
        llCapture = findViewById(R.id.llCapture) as LinearLayout
        llAmount = findViewById(R.id.llAmount) as LinearLayout

        val yourRadioGroup = findViewById(R.id.radioGroup) as RadioGroup
        val rbCash = findViewById(R.id.rbCash) as RadioButton
        val rbCheque = findViewById(R.id.rbCheque) as RadioButton


        rbCash.isChecked = true
        clickOnCash(rbCash)
        ivCapturedPic.setOnClickListener {
            if (bitmap != null) {
                showChequePreview(bitmap!!)
            }
        }
        rgCredit.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {
                when (checkedId) {
                    R.id.rbCustomer -> {
                        chargeType = 3
                        cardChecks()
                    }
                    R.id.rbBG -> {
                        chargeType = 2

                        cardChecks()
                    }


                }
            }
        })

        paymentType = preferenceUtils.getIntFromPreference(PreferenceUtils.PAYMENT_TYPE, 0)
        var currency = preferenceUtils.getStringFromPreference(PreferenceUtils.CURRENCY, "")
        tvCurrency.setText("" + currency)
        if (paymentType == 1) {
            rbCash.setChecked(true)
            rbCheque.isClickable = false
            rbCheque.setEnabled(false)
            clickOnCash(rbCash)

        } else if (paymentType == 2) {
            rbCheque.setChecked(true)
            rbCash.isClickable = false
            rbCash.setEnabled(false)
            clickOnCheque(rbCheque)

        } else {
            rbCash.setChecked(true)
            rbCash.isClickable = true
            rbCash.setEnabled(true)
            rbCheque.isClickable = true
            rbCheque.setEnabled(true)
        }

        yourRadioGroup.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {
                when (checkedId) {
                    R.id.rbCash -> {
                        clickOnCash(rbCash)
                    }
                    R.id.rbCheque -> {
                        clickOnCheque(rbCheque)
                    }
                    R.id.rbCreditcard -> {
                        clickOnCard(rbCreditcard)
                    }
                    R.id.onlinePayments -> {
                        clickOnOnline(onlinePayments)
                    }

                }
            }
        })


        var chequeValue = preferenceUtils.getIntFromPreference(PreferenceUtils.CHEQUE_DATE, 0)
        tvSelectDate.setOnClickListener {
            var calender: Calendar = Calendar.getInstance()
            val datePicker = DatePickerDialog(this@CreatePaymentActivity, datePickerListener, calender.get(Calendar.YEAR), calender.get(Calendar.MONTH), calender.get(Calendar.DAY_OF_MONTH))

            if (!datePicker.isShowing) {
                calender.add(Calendar.DAY_OF_MONTH, -chequeValue)
                var result = calender.getTime()
                datePicker.getDatePicker().setMinDate(result.time);
                datePicker.show()
            }

        }
        cheque = etChequeNo.text.toString().trim()
        bank = tvBankSelection.text.toString().trim()
        amount = etAmount.text.toString().trim()

        btnPayments.setOnClickListener {
            Util.preventTwoClick(it)
            cardtype = tvCardSelection.text.toString().trim()

            amount = etAmount.text.toString().trim()
            cheque = etChequeNo.text.toString().trim()
            bank = tvBankSelection.text.toString().trim()
            val data = tvSelection.text.toString()
            if (type == 10) {
                when {
//                    data.isEmpty() -> showToast("please select invoice")
                    amount.isEmpty() -> showToast("please enter amount")
                    else -> customerAuthorization(0)
                }

            } else if (type == 40) {
                when {
//                    data.isEmpty() -> showToast("please select invoice")
                    amount.isEmpty() -> showToast("please enter amount")
                    else -> paymentCreation()
                }

            } else if (type == 50) {
                when {
                    amount.isEmpty() -> showToast("please enter amount")
                    cardtype.isEmpty() -> showToast("please select cardtype")

                    else -> customerAuthorization(0)
                }

            } else {

//                val value = preferenceUtils.getStringFromPreference(PreferenceUtils.CAPTURE, "")

                when {

//                    data.isEmpty() -> showToast("please select invoice")
                    cheque.isEmpty() -> showToast("please enter cheque number")
                    chequeDate.isEmpty() -> showToast("please select date")
                    amount.isEmpty() -> showToast("please enter amount")
                    bank.isEmpty() -> showToast("please select bank")
                    bitmap == null -> showToast("please capture image")


                    else -> customerAuthorization(1)
                }

            }
        }

//        tvSelection.setOnClickListener {
//            selectInvoiceList()
//        }
        tvBankSelection.setOnClickListener {
            selectBankList()
        }
        tvCardSelection.setOnClickListener {
            selectCardList()
        }
        etAmount.addTextChangedListener(textWatcher)


    }

    fun cardChecks() {
        if (type == 50 && !etAmount.text.toString().isNullOrEmpty()) {
            if (rbCustomer.isChecked) {
                var amount = java.lang.Double.parseDouble(etAmount.text.toString().toString())
                var service = preferenceUtils.getDoubleFromPreference(PreferenceUtils.SERVICE_CHARGES, 0.0)
                var total = amount / (100 - service - service * 5 / 100) * 100
                var serviceCharge = (total * service / 100)
                var vat = serviceCharge * 5 / 100

                tvInvoiceAmount.setText("" + etAmount.text.toString())
                tvServiceCharge.setText(String.format("%.2f", serviceCharge))
                tvItemVat.setText(String.format("%.2f", vat))
                tvItemTotal.setText(String.format("%.2f", total))
            } else {
                tvInvoiceAmount.setText("" + etAmount.text.toString())
                tvServiceCharge.setText("0.00")
                tvItemVat.setText("0.00")
                tvItemTotal.setText(etAmount.text.toString())
            }

        } else {

        }
    }

    fun selectCardList() {
        cardDialog = Dialog(this, R.style.NewDialog)
        cardDialog.setCancelable(true)
        cardDialog.setCanceledOnTouchOutside(true)
        cardDialog.setContentView(R.layout.simple_list_dialog)
        val window = cardDialog.getWindow()
        window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)

        var recyclerView = cardDialog.findViewById(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        recyclerView.setLayoutManager(androidx.recyclerview.widget.LinearLayoutManager(this));

        val siteListRequest = CardListRequest(this@CreatePaymentActivity)
        siteListRequest.setOnResultListener { isError, unPaidInvoiceMainDO ->
            hideLoader()
            if (isError) {
                Toast.makeText(this@CreatePaymentActivity, resources.getString(R.string.server_error), Toast.LENGTH_SHORT).show()
            } else {
                if (unPaidInvoiceMainDO != null) {


                    var siteAdapter = CardAdapter("SINGLE", this@CreatePaymentActivity, unPaidInvoiceMainDO.cardDOS, 3)
                    recyclerView.setAdapter(siteAdapter)
//                    var siteAdapter = CreatePaymentAdapter(this@CreatePaymentActivity, createPaymentDo)
//                    recyclerView.setAdapter(siteAdapter)
                } else {
                    hideLoader()
                    showAlert("" + resources.getString(R.string.server_error))
                }
            }


        }

        siteListRequest.execute()


        cardDialog.show()

    }

    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            var cardtype = tvCardSelection.text.toString()
            if (type == 50 && !s.isNullOrEmpty() && !cardtype.isNullOrEmpty()) {
                if (rbCustomer.isChecked) {
                    var amount = java.lang.Double.parseDouble(s.toString())
                    var service = preferenceUtils.getDoubleFromPreference(PreferenceUtils.SERVICE_CHARGES, 0.0)
                    var total = amount / (100 - service - service * 5 / 100) * 100
                    var serviceCharge = (total * service / 100)
                    var vat = serviceCharge * 5 / 100

                    tvInvoiceAmount.setText("" + s)
                    tvServiceCharge.setText(String.format("%.2f", serviceCharge))
                    tvItemVat.setText(String.format("%.2f", vat))
                    tvItemTotal.setText(String.format("%.2f", total))
                } else {
                    tvInvoiceAmount.setText("" + s + " ")
                    tvServiceCharge.setText("0.00 ")
                    tvItemVat.setText("0.00 ")
                    tvItemTotal.setText("" + s + " ")
                }

            } else if (type == 50 && !s.isNullOrEmpty() && cardtype.isNullOrEmpty()) {
                showToast("Please select Card type")
            } else if (type == 50 && s.isNullOrEmpty()) {
                tvInvoiceAmount.setText("" + 0)
                tvServiceCharge.setText("0.00 ")
                tvItemVat.setText("0.00 ")
                tvItemTotal.setText("" + 0)
            }
        }
    }

    private fun clickOnCard(rbCreditcard: RadioButton) {
        type = 50
        rbCreditcard.isChecked = true
        cardChecks()
        llAmount.setVisibility(View.VISIBLE)
        llChequeDate.setVisibility(View.GONE)
        llCRCardUI.setVisibility(View.VISIBLE)
        llChequeNo.setVisibility(View.GONE)
        llCapture.setVisibility(View.GONE)
        llBankList.setVisibility(View.GONE)
//        llSiteList.visibility = View.VISIBLE


    }

    private fun clickOnCash(rbCash: RadioButton) {
        type = 10
        llCRCardUI.setVisibility(View.GONE)

        rbCash.isChecked = true
        etChequeNo.isEnabled = false
        etChequeNo.isClickable = false
        llChequeDate.setVisibility(View.GONE)
        llChequeNo.setVisibility(View.GONE)
        llCapture.setVisibility(View.GONE)
        llBankList.setVisibility(View.GONE)
        etChequeNo.setText("")
        etChequeNo.setHint("Cheque number")
        tvSelectDate.isEnabled = false
        tvSelectDate.isClickable = false
        tvSelectDate.setText("")
        chequeDate = ""
        tvSelectDate.setHint("Cheque date")
        tvBankSelection.isEnabled = true
        tvBankSelection.isClickable = true
        tvBankSelection.setText("")
        tvBankSelection.setHint("Select bank")
        ivCapturedPic.setBackgroundColor(resources.getColor(android.R.color.transparent))
        ivCapturedPic.setImageResource(R.drawable.gallary)
        ivCamera.isClickable = false
        ivCamera.isEnabled = false
        ivCapturedPic.isClickable = false
        ivCapturedPic.isEnabled = false
    }


    private fun clickOnOnline(rbOnline: RadioButton) {
        type = 40
        onlinePayments.isChecked = true
        llCRCardUI.setVisibility(View.GONE)
        llSiteList.visibility = View.GONE

        llAmount.setVisibility(View.VISIBLE)
        llChequeDate.setVisibility(View.GONE)
        llChequeNo.setVisibility(View.GONE)
        llCapture.setVisibility(View.GONE)
        llBankList.setVisibility(View.GONE)


    }

    private fun customerAuthorization(type: Int) {
        val customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)
        var activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)


        if (type == 0) {
            val ShipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, getResources().getString(R.string.checkin_non_scheduled))
            if (ShipmentType.equals(getResources().getString(R.string.checkin_non_scheduled), true)) {

                customerManagement(customerDo.customerId, 13)
            } else {
                customerManagement(activeDeliverySavedDo.customer, 13)

            }
        } else if (type == 40) {
            val ShipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, getResources().getString(R.string.checkin_non_scheduled))
            if (ShipmentType.equals(getResources().getString(R.string.checkin_non_scheduled), true)) {

                customerManagement(customerDo.customerId, 13)
            } else {
                customerManagement(activeDeliverySavedDo.customer, 13)

            }
        } else {
            val ShipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, getResources().getString(R.string.checkin_non_scheduled))
            if (ShipmentType.equals(getResources().getString(R.string.checkin_non_scheduled), true)) {

                customerManagement(customerDo.customerId, 14)
            } else {
                customerManagement(activeDeliverySavedDo.customer, 14)

            }
        }
    }

    private fun clickOnCheque(rbCheque: RadioButton) {
        rbCheque.isChecked = true
        type = 20
        tvSelectDate.isEnabled = true
        tvSelectDate.isClickable = true
        tvSelectDate.setText("")
        chequeDate = ""
        tvSelectDate.setHint("Cheque date")
        llChequeDate.setVisibility(View.VISIBLE)
        llCapture.setVisibility(View.VISIBLE)
        llBankList.setVisibility(View.VISIBLE)
        llCRCardUI.setVisibility(View.GONE)

        llChequeNo.setVisibility(View.VISIBLE)
        etChequeNo.isEnabled = true
        etChequeNo.isClickable = true
        etChequeNo.setText("")
        etChequeNo.setHint("Cheque number")
        tvBankSelection.isEnabled = true
        tvBankSelection.isClickable = true
        tvBankSelection.setText("")
        tvBankSelection.setHint("Select bank")
        ivCamera.isClickable = true
        ivCamera.isEnabled = true
        ivCapturedPic.isClickable = true
        ivCapturedPic.isEnabled = true
    }

    private fun showChequePreview(bitmap: Bitmap) {
        if (bitmap != null) {
            val dialog = Dialog(this, R.style.NewDialog)
            dialog.setContentView(R.layout.cheque_img_preview_dialog)
            dialog.setCancelable(true)
            dialog.setCanceledOnTouchOutside(true)
            dialog.window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)
            val tvCloseable = dialog.findViewById(R.id.tvClose) as TextView
            val ivChequePreview = dialog.findViewById(R.id.ivChequePreview) as ImageView

            ivChequePreview.setImageBitmap(bitmap)
            tvCloseable.setOnClickListener {
                dialog.dismiss()
            }

            dialog.show()
        } else {
            showToast("Please capture pic for cheque")
        }
    }

    private val datePickerListener: DatePickerDialog.OnDateSetListener = OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        cday = dayOfMonth
        cmonth = monthOfYear + 1
        cyear = year
        var monthString = cmonth.toString()
        if (monthString.length == 1) {
            monthString = "0$monthString"
        } else {
            monthString = cmonth.toString()
        }

        var dayString = cday.toString()
        if (dayString.length == 1) {
            dayString = "0$dayString"
        } else {
            dayString = cday.toString()
        }
        chequeDate = "" + cyear + monthString + dayString;
        tvSelectDate.setText("" + dayString + " - " + monthString + " - " + cyear)

    }

    private fun paymentCreation() {

        var payId = "";
        var id = tvSelection.text.toString()
        amount = etAmount.text.toString().trim()
        price = amount.toDouble()
        if (intent.hasExtra("SINGLE")) {
            payId = intent!!.extras!!.getString("SINGLE")!!
        }
        if (type == 40) {
            val customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)
            var activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)
            val ShipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, getResources().getString(R.string.checkin_non_scheduled))
            if (ShipmentType.equals(getResources().getString(R.string.checkin_non_scheduled), true)) {
                showLoader()
                if (Util.isNetworkAvailable(this@CreatePaymentActivity)) {
                    if (customerDo.email.isNotEmpty()) {
                        val onlinePayment = OnlinePayments(this, id, ArrayList<UnPaidInvoiceDO>(), customerDo.customerId,
                                customerDo.customerName, amount.toDouble(), customerDo.email)
                        onlinePayment.setOnResultListener(OnlinePayments.OnResultListener() { isError, successDO ->
                            if (isError) {
                                hideLoader()
                                showAppCompatAlert("Info!", "Error in Service Response, Please try again...", "Ok", "", "", false)

                            } else {
                                hideLoader()

                                if (successDO.flag === 1) {
                                    showAppCompatAlert("Info!", "Payment Created Successfully...", "Ok", "", "ONLINE", false)
                                } else {
                                    showAppCompatAlert("Info!", "Error in Service Response, Please try again...", "Ok", "", "", false)

                                }
                            }
                        })
                        onlinePayment.execute()
                    } else {
                        hideLoader()
                        showAlertEmail(id, ArrayList<UnPaidInvoiceDO>(), customerDo.customerId, customerDo.customerName, amount.toDouble(), customerDo.email)

                    }

                } else {
                    showAlert("Internet Unavailable ! Please connect to Internet.")

                }
            } else {
                if (Util.isNetworkAvailable(this@CreatePaymentActivity)) {
                    showLoader()
                    if (activeDeliverySavedDo.email.isNotEmpty()) {
                        val onlinePayment = OnlinePayments(this, id, ArrayList<UnPaidInvoiceDO>(), activeDeliverySavedDo.customer,
                                activeDeliverySavedDo.customerDescription, amount.toDouble(), activeDeliverySavedDo.email)
                        onlinePayment.setOnResultListener(OnlinePayments.OnResultListener() { isError, successDO ->
                            if (isError) {
                                hideLoader()
                                showAppCompatAlert("Info!", "Error in Service Response, Please try again...", "Ok", "", "", false)
                            } else {
                                hideLoader()
                                if (successDO.flag === 1) {
                                    showAppCompatAlert("Info!", "Payment Created Successfully...", "Ok", "", "ONLINE", false)
                                } else {
                                    showAppCompatAlert("Info!", "Error in Service Response, Please try again...", "Ok", "", "", false)
                                }
                            }
                        })
                        onlinePayment.execute()
                    } else {
                        hideLoader()
                        showAlertEmail(id, ArrayList<UnPaidInvoiceDO>(), activeDeliverySavedDo.customer,
                                activeDeliverySavedDo.customerDescription, amount.toDouble(), activeDeliverySavedDo.email)
                    }

                } else {
                    showAlert("Internet Unavailable ! Please connect to Internet.")
                    hideLoader()
                }
            }
        } else {
            if (type == 50) {
                amount = tvItemTotal.text.toString().trim()

            } else {
                amount = etAmount.text.toString().trim()

            }

            if (payId.length > 0) {
                val siteListRequest = CreateSinglePaymentRequest(chargeType, cardtype,
                        tvServiceCharge.text.toString().replace("AED", "").trim().toDouble(),
                        tvItemVat.text.toString().replace("AED", "").trim().toDouble()
                        , lattitudeFused, longitudeFused, addressFused, id, cheque, chequeDate, bank, amount.toDouble(), type, encImage, this@CreatePaymentActivity)

                siteListRequest.setOnResultListener { isError, createPaymentDO ->
                    hideLoader()

                    if (isError) {
                        showAppCompatAlert("Info!", "Error in Service Response, Please try again...", "Ok", "", "", false)
                    } else {
                        if (createPaymentDO != null) {
                            createPaymentDo = createPaymentDO
                            if (createPaymentDO.paymentNumber.length > 0) {
                                preferenceUtils.saveString(PreferenceUtils.PAYMENT_INVOICE_ID, createPaymentDO.invoiceNumber)
                                preferenceUtils.saveString(PreferenceUtils.PAYMENT_ID, createPaymentDO.paymentNumber)
//                            showAppCompatAlert("Success", "Payment Created - " + createPaymentDO.paymentNumber, "Ok", "", "SUCCESS", false)
                                showToast("Payment Created - " + createPaymentDO.paymentNumber)

                                preparePaymentCreation();
                                stopLocUpdates()

                                podDo.payment = CalendarUtils.getCurrentDate();
                                StorageManager.getInstance(this).saveDepartureData(this, podDo)
                                btnPayments.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                                btnPayments.setClickable(false)
                                btnPayments.setEnabled(false)

                            } else if (createPaymentDO.message.length > 0) {
                                showAppCompatAlert("Error!", " " + createPaymentDO.message, "Ok", "", "", false)

                            } else {
                                showAppCompatAlert("Error", "Payment Not Created, please try again..", "Ok", "", "failed", false)

                            }
                        } else {
                            showAppCompatAlert("Error", "Payment Not Created!", "Ok", "", "fail", false)
                        }
                        // Toast.makeText(this@CreatePaymentActivity, "Payment Created", Toast.LENGTH_SHORT).show()

//                    var siteAdapter = CreatePaymentAdapter(this@CreatePaymentActivity, createPaymentDo)
//                    recyclerView.setAdapter(siteAdapter)

                    }


                }

                siteListRequest.execute()
            } else {
                Toast.makeText(this@CreatePaymentActivity, "Cannot Create Payment", Toast.LENGTH_SHORT).show()

            }
//            else {
//                val siteListRequest = CreatePaymentRequest(lattitudeFused, longitudeFused, addressFused, id, cheque, chequeDate, bank, price, type, encImage, this@CreatePaymentActivity)
//                siteListRequest.setOnResultListener { isError, createPaymentDO ->
//                    hideLoader()
//
//                    if (isError) {
//                        showAppCompatAlert("Info!", "Error in Service Response, Please try again...", "Ok", "", "", false)
//                    } else {
//                        if (createPaymentDO != null) {
//                            createPaymentDo = createPaymentDO
//                            if (createPaymentDO.paymentNumber.length > 0) {
//                                preferenceUtils.saveString(PreferenceUtils.PAYMENT_INVOICE_ID, createPaymentDO.invoiceNumber)
//                                preferenceUtils.saveString(PreferenceUtils.PAYMENT_ID, createPaymentDO.paymentNumber)
////                            showAppCompatAlert("Success", "Payment Created - " + createPaymentDO.paymentNumber, "Ok", "", "SUCCESS", false)
//                                showToast("Payment Created - " + createPaymentDO.paymentNumber)
//                                preparePaymentCreation();
//                                stopLocUpdates()
//
//                                podDo.payment = CalendarUtils.getCurrentDate();
//                                StorageManager.getInstance(this).saveDepartureData(this, podDo)
//                                btnPayments.setBackgroundColor(resources.getColor(R.color.md_gray_light))
//                                btnPayments.setClickable(false)
//                                btnPayments.setEnabled(false)
//
//                            } else if (createPaymentDO.message.length > 0) {
//                                showAppCompatAlert("Error!", " " + createPaymentDO.message, "Ok", "", "", false)
//
//                            } else {
//                                showAppCompatAlert("Error", "Payment Not Created, please try again.", "Ok", "", "failed", false)
//
//                            }
//                        } else {
//                            showAppCompatAlert("Error", "Payment Not Created", "Ok", "", "fail", false)
//                        }
//                        // Toast.makeText(this@CreatePaymentActivity, "Payment Created", Toast.LENGTH_SHORT).show()
//
////                    var siteAdapter = CreatePaymentAdapter(this@CreatePaymentActivity, createPaymentDo)
////                    recyclerView.setAdapter(siteAdapter)
//
//                    }
//
//
//                }
//
//                siteListRequest.execute()
//            }
        }


    }

    private fun customerManagement(id: String, type: Int) {
        if (Util.isNetworkAvailable(this)) {
            val request = CustomerManagementRequest(id, type, this)
            request.setOnResultListener { isError, customerDo ->
                hideLoader()
                if (customerDo != null) {
                    if (isError) {
                        showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
                    } else {
                        if (customerDo.flag.equals("Customer authorized for this transaction", true)) {
                            if (Util.isNetworkAvailable(this)) {
                                if (Util.isNetworkAvailable(this)) {
                                    if (!lattitudeFused.isNullOrEmpty()) {
                                        stopLocationUpdates()
                                        paymentCreation()
                                    } else {
                                        showLoader()
                                        locationFetch()
//                                        showAppCompatAlert("", resources.getString(R.string.location_capture), "OK", "", "", false)
                                    }


                                } else {
                                    showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

                                }


                            } else {
                                showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

                            }

                        } else {
                            showAppCompatAlert("Alert!", "Customer not authorized for this transaction", "Ok", "", "", false)

                        }
                    }
                } else {
                    showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
                }
            }
            request.execute()
        } else {
            showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

        }


    }


    fun selectBankList() {
        bankDialog = Dialog(this, R.style.NewDialog)
        bankDialog.setCancelable(true)
        bankDialog.setCanceledOnTouchOutside(true)
        bankDialog.setContentView(R.layout.simple_list_dialog)
        val window = bankDialog.getWindow()
        window?.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)
        var data = preferenceUtils.getStringFromPreference(PreferenceUtils.Reason_Payment, "")
//            tvInvoiceId.setText("" + data)
//            tvInvoiceId.setOnClickListener({
//                preferenceUtils.saveString(PreferenceUtils.SITE_NAME, tvInvoiceId.text.toString())
//                tvSelection.text = "" + preferenceUtils.getStringFromPreference(PreferenceUtils.SITE_NAME, "")
//                dialog.dismiss()
//            })
        var site = preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID, "")

        var recyclerView = bankDialog.findViewById(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        recyclerView.setLayoutManager(androidx.recyclerview.widget.LinearLayoutManager(this));
        if (Util.isNetworkAvailable(this)) {
            val siteListRequest = BankListRequest(site, this@CreatePaymentActivity)
            siteListRequest.setOnResultListener { isError, unPaidInvoiceMainDO ->
                hideLoader()

                if (isError) {
                    Toast.makeText(this@CreatePaymentActivity, resources.getString(R.string.server_error), Toast.LENGTH_SHORT).show()
                } else {
                    if (unPaidInvoiceMainDO != null) {
                        var siteAdapter = BankListAdapter(this@CreatePaymentActivity, unPaidInvoiceMainDO.bankDOS, 1)
                        recyclerView.setAdapter(siteAdapter)
//                    var siteAdapter = CreatePaymentAdapter(this@CreatePaymentActivity, createPaymentDo)
//                    recyclerView.setAdapter(siteAdapter)

                    } else {
                        hideLoader()
                        showAlert("" + resources.getString(R.string.server_error))
                    }
                }

            }

            siteListRequest.execute()
        } else {
            showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

        }




        bankDialog.show()

    }

    override fun onButtonYesClick(from: String) {

        if ("SUCCESS".equals(from, ignoreCase = true)) {
            finish()
        }
        if ("ONLINE".equals(from, ignoreCase = true)) {
            finish()
        }
    }

    override fun onButtonNoClick(from: String) {

        if ("SUCCESS".equals(from, ignoreCase = true)) {
            finish()
        }
    }

    private fun preparePaymentCreation() {

        val id = preferenceUtils.getStringFromPreference(PreferenceUtils.PAYMENT_ID, "")
//        id = "SD-U101-19000886";
        if (id.length > 0) {
            if (Util.isNetworkAvailable(this)) {
                val siteListRequest = PDFPaymentDetailsRequest(id, this);
                val listener = PDFPaymentDetailsRequest.OnResultListener { isError, createPDFInvoiceDO ->
                    hideLoader()
                    if (createPDFInvoiceDO != null) {

                        if (isError) {
                            finish()
                            showToast("Unable to send Email")
                        } else {
                            createPDFpaymentDO = createPDFInvoiceDO
                            if (createPaymentDo.email == 10) {
                                createPaymentPDF(createPDFInvoiceDO)
                            } else {
                                if (createPaymentDo.print == 10) {
                                    printDocument(createPDFpaymentDO, PrinterConstants.PrintPaymentReport)
                                }
                                val intent = Intent()
                                setResult(1, intent)
                                finish()
                            }

                        }
                    } else {
                        showToast("Unable to send Email")
                    }
                }
                siteListRequest.setOnResultListener(listener)
                siteListRequest.execute()
            } else {
                showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "", false)

            }


        } else {
            showToast("Unable to send Email")

        }

    }

    private fun printDocument(obj: Any, from: Int) {
        val printConnection = PrinterConnection.getInstance(this@CreatePaymentActivity)
        if (printConnection.isBluetoothEnabled()) {
            printConnection.connectToPrinter(obj, from)
        } else {
//            PrinterConstants.PRINTER_MAC_ADDRESS = ""
            showToast("Please enable your mobile Bluetooth.")
//            showAppCompatAlert("", "Please enable your mobile Bluetooth.", "Enable", "Cancel", "EnableBluetooth", false)
        }
    }

    private fun pairedDevices() {
        val pairedDevices = BluetoothAdapter.getDefaultAdapter().getBondedDevices()
        if (pairedDevices.size > 0) {
            for (device in pairedDevices) {
                val deviceName = device.getName()
                val mac = device.getAddress() // MAC address
//                if (StorageManager.getInstance(this).getPrinterMac(this).equals("")) {
                StorageManager.getInstance(this).savePrinterMac(this, mac);
//                }
                Log.e("Bluetooth", "Name : " + deviceName + " Mac : " + mac)
                break
            }
        }
    }

    override fun onResume() {
        super.onResume()
        pairedDevices()
    }

    override fun onStart() {
        super.onStart()
        val filter = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        registerReceiver(mReceiver, filter)
//        val filter = IntentFilter(Intent.PAIRIN.ACTION_FOUND Intent. "android.bluetooth.device.action.PAIRING_REQUEST");
//        registerReceiver(mReceiver, filter)
//        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
//        mBluetoothAdapter.startDiscovery()
//        val filter2 = IntentFilter( "android.bluetooth.device.action.PAIRING_REQUEST")
//        registerReceiver(mReceiver, filter2)

    }

    override fun onDestroy() {
        unregisterReceiver(mReceiver)
//        PrinterConstants.PRINTER_MAC_ADDRESS = ""
        AppConstants.file2 = null

        super.onDestroy()

    }

    private val mReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            if (BluetoothDevice.ACTION_FOUND == action) {
                val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE);
                PrinterConstants.bluetoothDevices.add(device)
                Log.e("Bluetooth", "Discovered => name : " + device.name + ", Mac : " + device.address)
            } else if (action.equals(BluetoothDevice.ACTION_ACL_CONNECTED)) {
                Log.e("Bluetooth", "status : ACTION_ACL_CONNECTED")
                pairedDevices()
            } else if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                val state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);

                if (state == BluetoothAdapter.STATE_OFF) {
                    Log.e("Bluetooth", "status : STATE_OFF")
                } else if (state == BluetoothAdapter.STATE_TURNING_OFF) {
                    Log.e("Bluetooth", "status : STATE_TURNING_OFF")
                } else if (state == BluetoothAdapter.STATE_ON) {
                    Log.e("Bluetooth", "status : STATE_ON")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_TURNING_ON) {
                    Log.e("Bluetooth", "status : STATE_TURNING_ON")
                } else if (state == BluetoothAdapter.STATE_CONNECTING) {
                    Log.e("Bluetooth", "status : STATE_CONNECTING")
                } else if (state == BluetoothAdapter.STATE_CONNECTED) {
                    Log.e("Bluetooth", "status : STATE_CONNECTED")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_DISCONNECTED) {
                    Log.e("Bluetooth", "status : STATE_DISCONNECTED")
                }
            }
        }
    }

    private fun createPaymentPDF(createPDFInvoiceDO: PaymentPdfDO) {
        captureInfo(createPDFInvoiceDO.mail, ResultListner { `object`, isSuccess ->
            if (isSuccess) {
                PaymentReceiptPDF(this).createReceiptPDF(createPDFInvoiceDO, "Email")


                if (createPaymentDo.print == 10) {
                    printDocument(createPDFpaymentDO, PrinterConstants.PrintPaymentReport)
                }
                val intent1 = Intent()
                setResult(1, intent1)
                finish()

            }
        });
//        if (createPDFInvoiceDO != null && createPDFInvoiceDO.mail.length > 0) {
//            //            val intent = Intent(this@CreatePaymentActivity, PaymentEmailService::class.java)
////            var gson = Gson()
////            var invDO = gson.toJson(createPDFInvoiceDO)
////            intent.putExtra("object", invDO);
////            startService(intent);
//
//
//        } else {
//            showToast("Email address not found")
//            //Display error message
//        }
    }

    override fun onBackPressed() {

        val intent = Intent()
        setResult(1, intent)
        finish()
        super.onBackPressed()

    }

    fun locationFetch() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mSettingsClient = LocationServices.getSettingsClient(this)

        mLocationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                super.onLocationResult(locationResult)
                // location is received
                mCurrentLocation = locationResult!!.lastLocation
                mLastUpdateTime = DateFormat.getTimeInstance().format(Date())

                updateLocUI()
            }
        }

        mRequestingLocationUpdates = false

        mLocationRequest = LocationRequest()
        mLocationRequest!!.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS)
        mLocationRequest!!.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS)
        mLocationRequest!!.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)

        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(mLocationRequest!!)
        mLocationSettingsRequest = builder.build()

        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(object : PermissionListener {
                    override fun onPermissionRationaleShouldBeShown(permission: com.karumi.dexter.listener.PermissionRequest?, token: PermissionToken?) {
                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }

                    override fun onPermissionGranted(response: PermissionGrantedResponse) {
                        mRequestingLocationUpdates = true
                        startLocUpdates()
                    }

                    override fun onPermissionDenied(response: PermissionDeniedResponse) {
                        if (response.isPermanentlyDenied()) {

                            showToast("Location Permission denied")
                            // open device settings when the permission is
                            // denied permanently
//                            openSettings()
                        }
                    }

                    fun onPermissionRationaleShouldBeShown(permission: PermissionRequest, token: PermissionToken) {
                        token.continuePermissionRequest()
                    }
                }).check()
    }

    fun updateLocUI() {
        if (mCurrentLocation != null) {
            stopLocUpdates()
            hideLoader()
            paymentCreation()

            lattitudeFused = mCurrentLocation!!.getLatitude().toString()
            longitudeFused = mCurrentLocation!!.getLongitude().toString()
            var gcd = Geocoder(getBaseContext(), Locale.getDefault());

            try {
                var addresses = gcd.getFromLocation(mCurrentLocation!!.getLatitude(),
                        mCurrentLocation!!.getLongitude(), 1);
                if (addresses.size > 0) {
                    System.out.println(addresses.get(0).getLocality());
                    var cityName = addresses.get(0).getLocality();
                    addressFused = cityName
                }
            } catch (e: java.lang.Exception) {
                e.printStackTrace();
            }


        }

    }

    @SuppressLint("MissingPermission")
    private fun startLocUpdates() {
        mSettingsClient!!
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this) {
                    //                    Log.i(TAG, "All location settings are satisfied.")

//                    Toast.makeText(applicationContext, "Started location updates!", Toast.LENGTH_SHORT).show()


                    mFusedLocationClient!!.requestLocationUpdates(mLocationRequest,
                            mLocationCallback, Looper.myLooper())

                    updateLocUI()
                }
                .addOnFailureListener(this) { e ->
                    val statusCode = (e as ApiException).statusCode
                    when (statusCode) {
                        LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
//                            Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " + "location settings ")
                            try {
                                // Show the dialog by calling startResolutionForResult(), and check the
                                // result in onActivityResult().
                                val rae = e as ResolvableApiException
                                rae.startResolutionForResult(this@CreatePaymentActivity, REQUEST_CHECK_SETTINGS)
                            } catch (sie: IntentSender.SendIntentException) {
//                                Log.i(TAG, "PendingIntent unable to execute request.")
                            }

                        }
                        LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                            val errorMessage = "Location settings are inadequate, and cannot be " + "fixed here. Fix in Settings."
//                            Log.e(TAG, errorMessage)

                            Toast.makeText(this@CreatePaymentActivity, errorMessage, Toast.LENGTH_LONG).show()
                        }
                    }

                    updateLocUI()
                }
    }

    fun stopLocUpdates() {
        // Removing location updates
        if (mFusedLocationClient != null && mLocationCallback != null) {
            mFusedLocationClient!!
                    .removeLocationUpdates(mLocationCallback)
                    .addOnCompleteListener(this) {

                    }
        }

    }

    fun showAlertEmail(id: String, array: ArrayList<UnPaidInvoiceDO>, cusID: String, cusName: String, anount: Double, email: String) {
        val dialogg = Dialog(this@CreatePaymentActivity)
        dialogg.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogg.setCancelable(false)
        dialogg.setContentView(R.layout.email_dialog)
        val text = dialogg.findViewById<View>(R.id.macInput) as EditText
        val cancel = dialogg.findViewById<View>(R.id.cancel) as Button
        cancel.setOnClickListener { dialogg.dismiss() }
        val dialogButton = dialogg.findViewById<View>(R.id.print) as Button
        dialogButton.setOnClickListener {
            val emal = text.text.toString()
            if (!TextUtils.isEmpty(emal)) {
                if (Util.isValidEmailId(emal)) {
                    dialogg.dismiss()
                    showLoader()
                    val onlinePayment = OnlinePayments(this, id, ArrayList<UnPaidInvoiceDO>(), cusID,
                            cusName, anount, emal)
                    onlinePayment.setOnResultListener(OnlinePayments.OnResultListener() { isError, successDO ->
                        if (isError) {
                            hideLoader()
                            showAppCompatAlert("Info!", "Error in Service Response, Please try again...", "Ok", "", "", false)

                        } else {
                            hideLoader()

                            if (successDO.flag === 1) {
                                showAppCompatAlert("Info!", "Payment Created Successfully...", "Ok", "", "ONLINE", false)
                            } else {
                                showAppCompatAlert("Info!", "Error in Service Response, Please try again...", "Ok", "", "", false)

                            }
                        }
                    })
                    onlinePayment.execute()
                } else {
                    Toast.makeText(this@CreatePaymentActivity, "Please provide valid email id", Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(this@CreatePaymentActivity, "Please provide email id", Toast.LENGTH_SHORT).show()
            }
        }
        dialogg.show()
    }
}