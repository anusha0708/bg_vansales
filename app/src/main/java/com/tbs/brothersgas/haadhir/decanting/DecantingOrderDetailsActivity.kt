package com.tbs.brothersgas.haadhir.decanting

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.appcompat.app.AppCompatDialog
import com.tbs.brothersgas.haadhir.Activitys.BaseActivity
import com.tbs.brothersgas.haadhir.Activitys.NonBGActivity
import com.tbs.brothersgas.haadhir.Activitys.SignatureActivity
import com.tbs.brothersgas.haadhir.Adapters.ScheduledProductsAdapter
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryDO
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryMainDO
import com.tbs.brothersgas.haadhir.Model.CustomerDo
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.ActiveDeliveryRequest
import com.tbs.brothersgas.haadhir.Requests.CreateDeliveryRequest
import com.tbs.brothersgas.haadhir.Requests.ExecutiveActiveDeliveryRequest
import com.tbs.brothersgas.haadhir.common.AppConstants
import com.tbs.brothersgas.haadhir.pdfs.CYLDeliveryNotePDF
import com.tbs.brothersgas.haadhir.utils.Constants
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import kotlinx.android.synthetic.main.decanting_order_details.*
import java.util.*

class DecantingOrderDetailsActivity : BaseActivity() {
    var shipmentId: String = ""
    var customerCode: String = ""
    lateinit var userId: String
    lateinit var orderCode: String
    lateinit var llOrderHistory: View
    lateinit var customerDos: ArrayList<CustomerDo>

    lateinit var llSearch: LinearLayout
    lateinit var etSearch: EditText
    var startDate = ""
    var endDate = ""
    var to = ""
    var type = 0
    lateinit var activeDeliveryDO: ActiveDeliveryMainDO

    override fun initialize() {
        llOrderHistory = layoutInflater.inflate(R.layout.decanting_order_details, null)
        llBody.addView(llOrderHistory, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            val intent = Intent()
            setResult(1, intent)
            finish()
        }
        var screenType = intent.getIntExtra(Constants.SCREEN_TYPE, 0)
        if (screenType == 1) {
            type = 2
        } else {
            type = 1
        }
        disableMenuWithBackButton()
        initializeControls()

        shipmentId = intent.getStringExtra(Constants.SHIPMENT_ID)!!
        tvScreenTitle.setText(shipmentId)
        customerCode = intent.getStringExtra(Constants.CUSTOMER_CODE)!!


        getList();

        btn_proceed_sign.setOnClickListener {
            if(activeDeliveryDO!=null){

                createDelivery(shipmentId)
            }
        }

    }

    private fun createDelivery(id: String) {


        if (Util.isNetworkAvailable(this)) {

            var createDeliveryRequest = DecantingCreationRequest(id, activeDeliveryDO.stockType, type, activeDeliveryDO.activeDeliveryDOS, this)

            createDeliveryRequest.setOnResultListener { isError, deliveryMainDo, message ->
                hideLoader()
                var createDeliveryMAinDo = deliveryMainDo
                if (isError) {

                    hideLoader()
                    showAlert(resources.getString(R.string.server_error))
                } else {
                    if (createDeliveryMAinDo.status == 2) {
                        if (createDeliveryMAinDo.deliveryNumber.length > 0) {
//                            preferenceUtils.saveString(PreferenceUtils.SPOT_DELIVERY_NUMBER, createDeliveryMAinDo.deliveryNumber)
                            if (createDeliveryMAinDo.message.length > 0) {
                                showAppCompatAlert("Info!", "" + createDeliveryMAinDo.message + "-" + createDeliveryMAinDo.deliveryNumber,
                                        "Ok", "", "SUCCESS", false)

//                                showToast("" + createDeliveryMAinDo.message)


                            }

                        } else {
                            if (createDeliveryMAinDo.message.length > 0) {
                                showAppCompatAlert("Error", "" + createDeliveryMAinDo.message, "Ok", "", "FAILURE", false)

                            } else {
                                showAppCompatAlert("Error", "Document Not Created ", "Ok", "", "FAILURE", false)

                            }


                        }

//
//                          val driverAdapter = CreateDeliveryProductAdapter(this, deliveryMainDo.nonSheduledProductDOS)
//                          recycleview.setAdapter(driverAdapter)
                    } else {
                        hideLoader()
                        if (createDeliveryMAinDo.message.length > 0) {
                            showAppCompatAlert("Error", "" + createDeliveryMAinDo.message, "Ok", "", "FAILURE", false)

                        } else {
                            showAppCompatAlert("Error", "Document Not Created", "Ok", "", "FAILURE", false)

                        }

                    }
                }
            }
            createDeliveryRequest.execute()
        } else {
            showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

        }

    }

    private fun getList() {

        if (Util.isNetworkAvailable(this)) {
            val driverListRequest = DecantingDetailsRequest(this@DecantingOrderDetailsActivity, shipmentId)
            driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                hideLoader()
                if (isError) {
                    llDetails.visibility = View.GONE
                    btn_proceed_sign.visibility = View.GONE
                    showAlert(resources.getString(R.string.server_error))
                    tvNoOrders.visibility=View.VISIBLE
                } else {
                    activeDeliveryDO = activeDeliveryDo
                    tvNoOrders.visibility=View.GONE

                    llDetails.visibility = View.VISIBLE

                    if (activeDeliveryDo.createdDate.length > 0) {
                        val aMonth = activeDeliveryDo.createdDate.substring(4, 6)
                        val ayear = activeDeliveryDo.createdDate.substring(0, 4)
                        val aDate = activeDeliveryDo.createdDate.substring(Math.max(activeDeliveryDo.createdDate.length - 2, 0))
                        tvDeliveryDate.text = "" + aDate + "-" + aMonth + "-" + ayear


                    }
                    if (activeDeliveryDo.accountedDate.length > 0) {
                        val aMonth = activeDeliveryDo.accountedDate.substring(4, 6)
                        val ayear = activeDeliveryDo.accountedDate.substring(0, 4)
                        val aDate = activeDeliveryDo.accountedDate.substring(Math.max(activeDeliveryDo.accountedDate.length - 2, 0))
                        tvCustomer.text = "" + aDate + "-" + aMonth + "-" + ayear


                    }
                    tvShipmentId.text = "" + activeDeliveryDo.shipmentNumber
                    tvSite.text = "" + activeDeliveryDo.site
                    tvAddress.text = "" + activeDeliveryDo.siteAddress1


                    var loadStockAdapter = ScheduledProductsAdapter(this@DecantingOrderDetailsActivity, activeDeliveryDo.activeDeliveryDOS, "DECANTING", "", 0)
                    recycleview.setAdapter(loadStockAdapter)
                }
            }
            driverListRequest.execute()

        } else {
            showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

        }
    }


    override fun initializeControls() {
    }
/*
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 11 && resultCode == 11) {
            val intent = Intent();
            prepareDeliveryNoteCreation(intent.getStringExtra(Constants.SHIPMENT_ID))
        }
    }

   */

    override fun onButtonYesClick(from: String) {
        super.onButtonYesClick(from)
        if ("SUCCESS".equals(from, ignoreCase = true)) {
            val intent = Intent()
            setResult(1, intent)
            finish()
        }

    }


}
