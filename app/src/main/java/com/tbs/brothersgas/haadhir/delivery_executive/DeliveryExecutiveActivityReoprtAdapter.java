package com.tbs.brothersgas.haadhir.delivery_executive;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryDO;
import com.tbs.brothersgas.haadhir.Model.TransactionCashDO;
import com.tbs.brothersgas.haadhir.R;

import java.util.ArrayList;

public class DeliveryExecutiveActivityReoprtAdapter extends RecyclerView.Adapter<DeliveryExecutiveActivityReoprtAdapter.MyViewHolder> {

    private ArrayList<ActiveDeliveryDO> siteDOS;
    private Context context;
    private String customerId;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvRecieptNumber, tvCustomerName;
        private TextView tvAmount;

        public MyViewHolder(View view) {
            super(view);
            tvAmount = (TextView) view.findViewById(R.id.tvAmount);
            tvRecieptNumber = (TextView) view.findViewById(R.id.tvRecieptNumber);
            tvCustomerName = (TextView) view.findViewById(R.id.tvCustomerName);


        }
    }


    public DeliveryExecutiveActivityReoprtAdapter(Context context, ArrayList<ActiveDeliveryDO> siteDOS, String customerID) {
        this.context = context;
        this.siteDOS = siteDOS;
        this.customerId = customerID;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.transaction_data_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final ActiveDeliveryDO data = siteDOS.get(position);

        holder.tvRecieptNumber.setText(data.shipmentId);
        holder.tvCustomerName.setText(data.customer + " " + data.customerDes);

        String aMonth = data.date.substring(4, 6);
        String ayear = data.date.substring(0, 4);
        String aDate = data.date.substring(Math.max(data.date.length() - 2, 0));
        holder.tvAmount.setText("" + "" + aDate + "-" + aMonth + "-" + ayear);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });


    }

    @Override
    public int getItemCount() {
        return siteDOS.size();
    }

}
