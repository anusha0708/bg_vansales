package com.tbs.brothersgas.haadhir.Activitys

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.brothersgas.haadhir.Adapters.LoanReturnAdapter
import com.tbs.brothersgas.haadhir.Model.LoadStockDO
import com.tbs.brothersgas.haadhir.Model.LoanReturnDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.*
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.pdfs.CylinderIssRecPdfBuilder
import com.tbs.brothersgas.haadhir.prints.PrinterConnection
import com.tbs.brothersgas.haadhir.prints.PrinterConstants
import com.tbs.brothersgas.haadhir.utils.CalendarUtils
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import kotlin.collections.ArrayList
import kotlin.collections.LinkedHashMap

//
class SelectedReturnDetailsActivity : BaseActivity() {
    lateinit var invoiceAdapter: LoanReturnAdapter
    lateinit var loadStockDOs: ArrayList<LoadStockDO>
    lateinit var recycleview : androidx.recyclerview.widget.RecyclerView
    lateinit var tvNoDataFound:TextView
    lateinit var tvBalanceQty:TextView
    lateinit var tvReturnedQty:TextView
    lateinit var tvIssuedQty:TextView
    lateinit var tvOpeningQty:TextView
    lateinit var openingQty:String
    private var issuedQty : Int = 0
    private var type : Int = 1

    private var   returnQty :Double=0.0
    private var balanceQty:Double=0.0
    private lateinit var btnConfirm: Button
    private lateinit var rbEmptyCylinder: RadioButton
    private lateinit var rbSalesReturn: RadioButton
    private var loanReturnDOsMap: LinkedHashMap<String, ArrayList<LoanReturnDO>> = LinkedHashMap()
    private var loanReturnDos: ArrayList<LoanReturnDO> = ArrayList()

    override fun initialize() {
      val  llCategories = getLayoutInflater().inflate(R.layout.selected_return_details, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
//        ivAdd.setVisibility(View.VISIBLE)
        if(intent.hasExtra("IssuedQty"))
            issuedQty = intent.extras.getInt("IssuedQty", 0)

        if (intent.hasExtra("Products")) {
            loanReturnDos = intent.getSerializableExtra("Products") as ArrayList<LoanReturnDO>
        }
        if (intent.hasExtra("openingQty")) {
            openingQty = intent.extras.getString("openingQty", "")

        }

        initializeControls()
        val rgType= findViewById(R.id.rgType) as RadioGroup

        rgType.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {
                when (checkedId) {
                    R.id.rbEmptyCylinder -> {
                        type=1
                        refreshAdapter(type)
                    }
                    R.id.rbSalesReturn -> {
                        type=2
                        refreshAdapter(type)
                    }

                }
            }
        })
        tvOpeningQty.setText(""+openingQty)
        tvIssuedQty.setText(""+issuedQty)

        loanReturnDOsMap = LinkedHashMap<String, ArrayList<LoanReturnDO>>();

        bindData(loanReturnDos);

        ivAdd.setOnClickListener{
            val intent = Intent(this@SelectedReturnDetailsActivity, AddMoreReturnProductsActivity::class.java)
            intent.putExtra("selectedLoanReturnDOs", loanReturnDos);
            startActivityForResult(intent, 177);
        }

        btnConfirm.setOnClickListener {
            if(loanReturnDOsMap!=null && loanReturnDOsMap.size>0){
                StorageManager.getInstance(this).saveReturnCylinders(this, loanReturnDos)
                val returnCount = StorageManager.getInstance(this).saveAllReturnStockValue(loanReturnDos);
                if(returnCount>0){
                    var recievedSite = preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID, "")
                    var vehicleCode = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_CODE, "")

                    val customer = preferenceUtils.getStringFromPreference(PreferenceUtils.CUSTOMER, "")
                    val returnDate = CalendarUtils.getDate()
                    if (Util.isNetworkAvailable(this)) {

                        val siteListRequest = CreateLoanReturnRequest( recievedSite,customer,vehicleCode,type,loanReturnDOsMap,this@SelectedReturnDetailsActivity)
                        showLoader()
                        siteListRequest.setOnResultListener { isError, loanReturnMainDo ->
                            hideLoader()
                            if (loanReturnMainDo != null) {
                                if (isError) {
                                    showToast("Unable to Create Loan")
                                } else {
                                    if(loanReturnMainDo.flag==2){
                                        preferenceUtils.saveString(PreferenceUtils.LOAN_RETURN_NUMBER,loanReturnMainDo.loanReturnNumber)
                                        prepareCylinderIssueNoteCreation("Print")

                                        val intent = Intent()
                                        intent.putExtra("CapturedReturns", true)
                                        setResult(11, intent)
                                        showToast("Return Created")

                                        finish()
                                    }else{
                                        showToast("Unable to Create Loan")
                                    }

                                }
                            } else {
                                showToast("Unable to Create Loan")
                            }

                        }
                        siteListRequest.execute()


                    } else {
                        showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

                    }

                }
                else{
                    showToast("Unable to Create Loan")
                }
            }
        }


        setReturnQty(loanReturnDos)
    }

    private fun refreshAdapter(type : Int){
        if(type == 2){
            if(invoiceAdapter == null){
                invoiceAdapter = LoanReturnAdapter(this@SelectedReturnDetailsActivity, loanReturnDOsMap,"")
                recycleview.adapter = invoiceAdapter
            }
            else{
                invoiceAdapter.refreshAdapter(loanReturnDOsMap, "");
            }
        }
        else {
            if(invoiceAdapter == null){
                invoiceAdapter = LoanReturnAdapter(this@SelectedReturnDetailsActivity, loanReturnDOsMap,"salesreturn")
                recycleview.adapter = invoiceAdapter
            }
            else{
                invoiceAdapter.refreshAdapter(loanReturnDOsMap, "salesreturn");
            }
        }
    }

    private fun setReturnQty(loanReturnDos : ArrayList<LoanReturnDO>){
        if(loanReturnDos!=null && loanReturnDos.size>0){
            for (i in loanReturnDos.indices){
                returnQty = returnQty + loanReturnDos.get(i).qty
            }
        }
        tvReturnedQty.setText(""+returnQty)
        openingQty.toDouble()
        tvBalanceQty.setText(""+(openingQty.toDouble()+issuedQty-returnQty))
        balanceQty = tvBalanceQty.text.toString().toDouble()
    }

    override fun initializeControls() {
        tvScreenTitle.setText(R.string.inventory_count)
        recycleview    = findViewById(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
        tvNoDataFound  = findViewById(R.id.tvNoDataFound) as TextView
        tvOpeningQty   = findViewById(R.id.tvOpeningQty) as TextView
        tvIssuedQty    = findViewById(R.id.tvIssuedQty) as TextView
        tvReturnedQty  = findViewById(R.id.tvReturnedQty) as TextView
        tvBalanceQty   = findViewById(R.id.tvBalanceQty) as TextView
        recycleview.setLayoutManager(linearLayoutManager)
        btnConfirm     = findViewById(R.id.btnConfirm) as Button
        rbEmptyCylinder  = findViewById(R.id.rbEmptyCylinder) as RadioButton
        rbSalesReturn    = findViewById(R.id.rbSalesReturn) as RadioButton



//        showLoader()
//        val driverListRequest = LoanReturnRequest(this@SelectedReturnDetailsActivity)
//        driverListRequest.setOnResultListener { isError, invoiceHistoryMainDOs ->
//            hideLoader()
//            if (isError) {
//                tvNoDataFound.setVisibility(View.VISIBLE)
//                recycleview.setVisibility(View.GONE)
//                Toast.makeText(this@SelectedReturnDetailsActivity, R.string.error_customer_list, Toast.LENGTH_SHORT).show()
//            }
//            else {
//
//
//            }
//        }
//        driverListRequest.execute()

    }

    private fun bindData(loanReturnDos: ArrayList<LoanReturnDO>){

        val shipmentIds = ArrayList<String>()
        if(loanReturnDos!=null && loanReturnDos.size>0){
            for (i in loanReturnDos.indices){
                if(!shipmentIds.contains(loanReturnDos.get(i).shipmentNumber)){
                    shipmentIds.add(loanReturnDos.get(i).shipmentNumber)
                }
            }
            setReturnQty(loanReturnDos)
        }
        if(shipmentIds.size>0){
            for (j in shipmentIds.indices){
                val returnList = ArrayList<LoanReturnDO>()
                for (i in loanReturnDos.indices) {
                    if (loanReturnDos.get(i).shipmentNumber.equals(shipmentIds.get(j))) {
                        returnList.add(loanReturnDos.get(i))
                    }
                }
                loanReturnDOsMap.put(shipmentIds.get(j), returnList);
            }
        }
        if(loanReturnDOsMap.size>0){
            tvNoDataFound.setVisibility(View.GONE)
            recycleview.setVisibility(View.VISIBLE)
            invoiceAdapter = LoanReturnAdapter(this@SelectedReturnDetailsActivity, loanReturnDOsMap,"salesreturn")
            recycleview.adapter = invoiceAdapter
        }else{
            tvNoDataFound.setVisibility(View.VISIBLE)
            recycleview.setVisibility(View.GONE)
            btnConfirm.setVisibility(View.GONE)
        }

    }


    private var selectedLoanReturnDOs: ArrayList<LoanReturnDO> = ArrayList()

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 177 && resultCode == 177){
            if(data!!.hasExtra("selectedLoanReturnDOs")){
                if(selectedLoanReturnDOs!=null && selectedLoanReturnDOs.size>0) {
                    loanReturnDos.removeAll(selectedLoanReturnDOs)
                }
                selectedLoanReturnDOs = data.extras.getSerializable("selectedLoanReturnDOs") as ArrayList<LoanReturnDO>
                if(selectedLoanReturnDOs!=null && selectedLoanReturnDOs.size>0){
                    var isProductExisted = false;
                    for (i in selectedLoanReturnDOs.indices) {
                        for (k in loanReturnDos!!.indices) {
                            if (selectedLoanReturnDOs.get(i).productName.equals(loanReturnDos!!.get(k).productName, true)) {// && selectedLoanReturnDOs.get(i).shipmentId.equals(loanReturnDos!!.get(k).shipmentId, true)
                                loanReturnDos!!.get(k).qty = selectedLoanReturnDOs.get(i).qty
                                isProductExisted = true
                                break
                            }
                        }
                        if (!isProductExisted) {
                            loanReturnDos!!.add(selectedLoanReturnDOs.get(i))
                        }
                    }
                    bindData(loanReturnDos)
                }


            }
        }
    }


    private fun prepareCylinderIssueNoteCreation(from: String) {
        val id = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
        val loanReturnNumber = preferenceUtils.getStringFromPreference(PreferenceUtils.LOAN_RETURN_NUMBER, "")

//        val id="SD-U101-19000847"
        if (loanReturnNumber.length > 0) {
            val driverListRequest = CylinderIssueRequest(loanReturnNumber, this@SelectedReturnDetailsActivity)
            val onResultListener = OnResultListener(from)
            driverListRequest.setOnResultListener(onResultListener)
            driverListRequest.execute()
        } else {
            showAppCompatAlert("Error", "Please create Loan first!!", "Ok", "", "", false)



        }
    }

    private fun OnResultListener(from: String): OnResultListener {
        return OnResultListener { isError, cylinderIssueMainDO ->
            if (cylinderIssueMainDO != null) {
                if (isError) {
                    showAppCompatAlert("Error", "Loan pdf data error", "Ok", "", "", false)
                } else {
                    if (from.equals("Print", true)) {
                        printDocument(cylinderIssueMainDO, PrinterConstants.PrintLoanReturn)
                    } else {
                        CylinderIssRecPdfBuilder.getBuilder(this@SelectedReturnDetailsActivity).build(cylinderIssueMainDO, "Email")
                    }
                }
            } else {
                showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
            }
        }

    }
    private fun printDocument(obj: Any, from: Int) {
        val printConnection = PrinterConnection.getInstance(this@SelectedReturnDetailsActivity)
        if (printConnection.isBluetoothEnabled()) {
            printConnection.connectToPrinter(obj, from)
        } else {
//            PrinterConstants.PRINTER_MAC_ADDRESS = ""
            showAppCompatAlert("", "Please enable your mobile Bluetooth.", "Enable", "Cancel", "EnableBluetooth", false)
        }
    }
    override fun onResume() {
        super.onResume()
        pairedDevices()
    }

    override fun onStart() {
        super.onStart()
        val filter = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        registerReceiver(mReceiver, filter)
//        val filter = IntentFilter(Intent.PAIRIN.ACTION_FOUND Intent. "android.bluetooth.device.action.PAIRING_REQUEST");
//        registerReceiver(mReceiver, filter)
//        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
//        mBluetoothAdapter.startDiscovery()
//        val filter2 = IntentFilter( "android.bluetooth.device.action.PAIRING_REQUEST")
//        registerReceiver(mReceiver, filter2)

    }

    override fun onDestroy() {
        hideLoader()
        unregisterReceiver(mReceiver)
//        PrinterConstants.PRINTER_MAC_ADDRESS = ""
        super.onDestroy()
    }

    private val mReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            if (BluetoothDevice.ACTION_FOUND == action) {
                val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE);
                PrinterConstants.bluetoothDevices.add(device)
                Log.e("Bluetooth", "Discovered => name : " + device.name + ", Mac : " + device.address)
            } else if (action.equals(BluetoothDevice.ACTION_ACL_CONNECTED)) {
                Log.e("Bluetooth", "status : ACTION_ACL_CONNECTED")
                pairedDevices()
            } else if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                val state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);

                if (state == BluetoothAdapter.STATE_OFF) {
                    Log.e("Bluetooth", "status : STATE_OFF")
                } else if (state == BluetoothAdapter.STATE_TURNING_OFF) {
                    Log.e("Bluetooth", "status : STATE_TURNING_OFF")
                } else if (state == BluetoothAdapter.STATE_ON) {
                    Log.e("Bluetooth", "status : STATE_ON")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_TURNING_ON) {
                    Log.e("Bluetooth", "status : STATE_TURNING_ON")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_CONNECTING) {
                    Log.e("Bluetooth", "status : STATE_CONNECTING")
                } else if (state == BluetoothAdapter.STATE_CONNECTED) {
                    Log.e("Bluetooth", "status : STATE_CONNECTED")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_DISCONNECTED) {
                    Log.e("Bluetooth", "status : STATE_DISCONNECTED")
                }
            }
        }
    }

    private fun pairedDevices() {
        val pairedDevices = BluetoothAdapter.getDefaultAdapter().getBondedDevices()
        if (pairedDevices.size > 0) {
            for (device in pairedDevices) {
                val deviceName = device.getName()
                val mac = device.getAddress() // MAC address
//                if (StorageManager.getInstance(this).getPrinterMac(this).equals("")) {
                StorageManager.getInstance(this).savePrinterMac(this, mac);
//                }
                Log.e("Bluetooth", "Name : " + deviceName + " Mac : " + mac)
                break
            }
        }
    }


}