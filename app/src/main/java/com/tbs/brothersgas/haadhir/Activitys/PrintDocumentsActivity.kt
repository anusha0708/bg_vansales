package com.tbs.brothersgas.haadhir.Activitys

import android.Manifest
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Environment
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.appcompat.app.AppCompatDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.tbs.brothersgas.haadhir.Model.*
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.*
import com.tbs.brothersgas.haadhir.common.AppConstants
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.listeners.ResultListner
import com.tbs.brothersgas.haadhir.pdfs.*
import com.tbs.brothersgas.haadhir.pdfs.utils.PDFConstants
import com.tbs.brothersgas.haadhir.print.FileHelper
import com.tbs.brothersgas.haadhir.print.PrintActivity
import com.tbs.brothersgas.haadhir.prints.PrinterConnection
import com.tbs.brothersgas.haadhir.prints.PrinterConstants
import com.tbs.brothersgas.haadhir.utils.Constants
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import java.io.File
import java.io.InputStream
import java.io.OutputStream
import java.io.UnsupportedEncodingException


//
class PrintDocumentsActivity : BaseActivity() {

    lateinit var btnCompleted: Button
    lateinit var btnShow: Button
    lateinit var `in`: InputStream
    lateinit var out: OutputStream
    val fileName = "data.txt"
    var company = ""
    var companyDescription = ""
    val path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/instinctcoder/readwrite/"
    val TAG = FileHelper::class.java.name
    private val permissionRequestCode = 34
    lateinit var cbDeliveryEmail: ImageView
    lateinit var cbDeliveryPrint: ImageView
    lateinit var cbInvoiceEmail: ImageView
    lateinit var cbInvoicePrint: ImageView
    lateinit var cbPaymentEmail: ImageView
    lateinit var cbPaymentPrint: ImageView
    lateinit var cbCylinderEmail: ImageView
    lateinit var cbCylinderPrint: ImageView

    lateinit var cbDeliveryPreview: ImageView
    lateinit var cbInvoicePreview: ImageView
    lateinit var cbPaymentPreview: ImageView
    lateinit var cbCylinderPreview: ImageView

    lateinit var img_qr_code_image: ImageView

    private var shipmentType = ""
    lateinit var createPDFInvoiceDo: CreateInvoicePaymentDO
    lateinit var createPDFpaymentDO: PaymentPdfDO
    lateinit var customerDO: CustomerDo
    lateinit var activedeliveDO: ActiveDeliveryMainDO

    override fun initialize() {
        val llCategories = getLayoutInflater().inflate(R.layout.print_documents, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        tvScreenTitle.setText(R.string.print_documents)
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
        shipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, "")
        initializeControls()
        company = preferenceUtils.getStringFromPreference(PreferenceUtils.COMPANY, "")
        companyDescription = preferenceUtils.getStringFromPreference(PreferenceUtils.COMPANY_DES, "")

        btnCompleted.setOnClickListener {
            //            val value = preferenceUtils.getStringFromPreference(PreferenceUtils.DELIVERY_NOTE, "")
//
//             if (value.equals("", true) && !value.equals("SUCCESS", true)) {
//                showToast("Please complete the delivery note")
//            }
//            else{
//                showAppCompatAlert("", "Are You Sure You Want to Exit?", "OK", "Cancel", "EXIT", true)
//            }
            val value = preferenceUtils.getStringFromPreference(PreferenceUtils.DELIVERY_NOTE, "")
            val value2 = preferenceUtils.getStringFromPreference(PreferenceUtils.DELIVERY_NOTE_RESHEDULE, "")

            if (value.equals("", true) && !value.equals("SUCCESS", true)) {
                if (!value2.equals("", true)) {
                    showAppCompatAlert("", "Are You Sure You Want to Exit?", "OK", "Cancel", "EXIT", true)
                } else {

                    showAppCompatAlert("", "Please Complete Delivery Note ", "Ok", "Cancel", "", false)
                }
            } else {
                if (!value2.equals("", true)) {
                    showAppCompatAlert("", "Are You Sure You Want to Exit?", "OK", "Cancel", "EXIT", true)
                } else if (value.equals("", true) && !value.equals("SUCCESS", true)) {
//
                    showAppCompatAlert("", "Please Complete Delivery Note ", "Ok", "Cancel", "", false)
                } else {
                    showAppCompatAlert("", "Are You Sure You Want to Exit?", "OK", "Cancel", "EXIT", true)
                }
            }
        }
    }

    override fun initializeControls() {
        btnCompleted = findViewById<View>(R.id.btnCompleted) as Button
        cbDeliveryEmail = findViewById<View>(R.id.cbDeliveryEmail) as ImageView
        cbDeliveryPrint = findViewById<View>(R.id.cbDeliveryPrint) as ImageView
        cbInvoiceEmail = findViewById<View>(R.id.cbInvoiceEmail) as ImageView
        cbInvoicePrint = findViewById<View>(R.id.cbInvoicePrint) as ImageView
        cbPaymentEmail = findViewById<View>(R.id.cbPaymentEmail) as ImageView
        cbPaymentPrint = findViewById<View>(R.id.cbPaymentPrint) as ImageView
        cbCylinderEmail = findViewById<View>(R.id.cbCylinderEmail) as ImageView
        cbCylinderPrint = findViewById<View>(R.id.cbCylinderPrint) as ImageView

        cbDeliveryPreview = findViewById<View>(R.id.cbDeliveryPreview) as ImageView
        cbInvoicePreview = findViewById<View>(R.id.cbInvoicePreview) as ImageView
        cbPaymentPreview = findViewById<View>(R.id.cbPaymentPreview) as ImageView
        cbCylinderPreview = findViewById<View>(R.id.cbCylinderPreview) as ImageView
        img_qr_code_image = findViewById<View>(R.id.img_qr_code_image) as ImageView

        btnShow = findViewById<View>(R.id.btnShow) as Button


        customerDO = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)
        activedeliveDO = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)

        btnShow.setOnClickListener {
            val intent = Intent(this@PrintDocumentsActivity, MainActivity::class.java)
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
        }
//        try {
//          var  qrCode = Util.encodeAsBitmap(this, "RCHQ-U1L1900001")
//            img_qr_code_image.setImageBitmap(qrCode)
//        } catch (e: WriterException) {
//            e.printStackTrace()
//        }


//        cbInvoicePreview?.setOnCheckedChangeListener { buttonView, isChecked ->
//
//            if (isChecked) {
//                prePrepareInvoiceCreation()
//            } else {
//
//            }
//
//        }

        cbDeliveryPreview.setOnClickListener {
            if (Util.isNetworkAvailable(this)) {
                prePrepareDeliveryNoteCreation()

            } else {
                showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

            }

        }

        cbInvoicePreview.setOnClickListener {
            if (Util.isNetworkAvailable(this)) {

                prePrepareInvoiceCreation()
            } else {
                showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

            }

        }
        cbPaymentPreview.setOnClickListener {
            if (Util.isNetworkAvailable(this)) {

                preParePaymentCreation()
            } else {
                showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

            }

        }
        cbCylinderPreview.setOnClickListener {
            if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.FixedQuantityProduct)) {
                if (Util.isNetworkAvailable(this)) {
                    prePareCylinderIssueNoteCreation()

                } else {
                    showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

                }

            }
        }
        cbDeliveryPrint.setOnClickListener {
            val customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)
            val activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)
            val ShipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, getResources().getString(R.string.checkin_non_scheduled))
            if (ShipmentType.equals(getResources().getString(R.string.checkin_non_scheduled), true)) {
                customerAuthorizationManagement(customerDo.customerId, 5)
            } else {
                customerAuthorizationManagement(activeDeliverySavedDo.customer, 5)
            }
        }
        cbDeliveryEmail.setOnClickListener {
            if (shipmentType.equals(getResources().getString(R.string.checkin_non_scheduled), true)) {
                captureInfo(customerDO.deliveryEmail,ResultListner { `object`, isSuccess ->
                    if (isSuccess) {
                        customerAuthorizationManagement(customerDO.customerId, 9)

                    }
                })
            } else {
                captureInfo(activedeliveDO.deliveryEmail,ResultListner { `object`, isSuccess ->
                    if (isSuccess) {
                        customerAuthorizationManagement(activedeliveDO.customer, 9)
                    }
                })

            }

        }

        cbInvoiceEmail.setOnClickListener {
            if (shipmentType.equals(getResources().getString(R.string.checkin_non_scheduled), true)) {
                captureInfo(customerDO.invoiceEmail,ResultListner { `object`, isSuccess ->
                    if (isSuccess) {
                        customerAuthorizationManagement(customerDO.customerId, 10)
                    }
                })
            } else {
                captureInfo(activedeliveDO.invoiceEmail,ResultListner { `object`, isSuccess ->
                    if (isSuccess) {
                        customerAuthorizationManagement(activedeliveDO.customer, 10)
                    }
                })

            }
        }

        cbCylinderEmail.setOnClickListener {
            if (shipmentType.equals(getResources().getString(R.string.checkin_non_scheduled), true)) {
                captureInfo(customerDO.cylinderIssueEmail,ResultListner { `object`, isSuccess ->
                    if (isSuccess) {
                        customerAuthorizationManagement(customerDO.customerId, 12)
                    }
                })
            } else {
                captureInfo(activedeliveDO.cylinderIssueEmail,ResultListner { `object`, isSuccess ->
                    if (isSuccess) {
                        customerAuthorizationManagement(activedeliveDO.customer, 12)
                    }
                })

            }
        }
        cbPaymentEmail.setOnClickListener {
            if (shipmentType.equals(getResources().getString(R.string.checkin_non_scheduled), true)) {
                captureInfo(customerDO.paymentEmail,ResultListner { `object`, isSuccess ->
                    if (isSuccess) {
                        customerAuthorizationManagement(customerDO.customerId, 11)
                    }
                })
            } else {
                captureInfo(activedeliveDO.paymentEmail,ResultListner { `object`, isSuccess ->
                    if (isSuccess) {
                        customerAuthorizationManagement(activedeliveDO.customer, 11)
                    }
                })

            }
        }
        cbInvoicePrint.setOnClickListener {
            val customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)
            val activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)
            val ShipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, getResources().getString(R.string.checkin_non_scheduled))
            if (ShipmentType.equals(getResources().getString(R.string.checkin_non_scheduled), true)) {
                customerAuthorizationManagement(customerDo.customerId, 6)
            } else {
                customerAuthorizationManagement(activeDeliverySavedDo.customer, 6)
            }
        }
        cbPaymentPrint.setOnClickListener {


            val customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)
            val activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)
            val ShipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, getResources().getString(R.string.checkin_non_scheduled))
            if (ShipmentType.equals(getResources().getString(R.string.checkin_non_scheduled), true)) {
                customerAuthorizationManagement(customerDo.customerId, 7)
            } else {
                customerAuthorizationManagement(activeDeliverySavedDo.customer, 7)
            }
        }

        cbCylinderPrint.setOnClickListener {
            val customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)
            val activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)
            val ShipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, getResources().getString(R.string.checkin_non_scheduled))
            if (ShipmentType.equals(getResources().getString(R.string.checkin_non_scheduled), true)) {
                customerAuthorizationManagement(customerDo.customerId, 8)
            } else {
                customerAuthorizationManagement(activeDeliverySavedDo.customer, 8)
            }
        }

    }

    private fun printDocument(obj: Any, from: Int) {
        val printConnection = PrinterConnection.getInstance(this@PrintDocumentsActivity)
        if (printConnection.isBluetoothEnabled()) {
            printConnection.connectToPrinter(obj, from)
        } else {
//            PrinterConstants.PRINTER_MAC_ADDRESS = ""
            showAppCompatAlert("", "Please enable your mobile Bluetooth.", "Enable", "Cancel", "EnableBluetooth", false)
        }
    }

    fun copyPdfFile(inputStream: InputStream, out: OutputStream?) {
        val buffer = ByteArray(1024)
//            var read:Int
        val read = inputStream.read(buffer)
        while ((read) != -1) {
            out!!.write(buffer, 0, read)
        }
    }

    //        fun copyPdfFile(`in`:InputStream, out:OutputStream) {
//
//        }
    fun CopyReadPDFFromAssets() {
        val assetManager = getAssets()
        val dir = File(android.os.Environment.getExternalStorageDirectory().toString()
                + File.separator
                + "PDF"
                + File.separator)
        if (!dir.exists()) {
            dir.mkdir()
        }

        val file = File(dir.getPath() + File.separator + PDFConstants.CYL_DELIVERY_NOTE_PDF_NAME)
        try {
            `in` = assetManager.open(dir.getPath() + File.separator + PDFConstants.CYL_DELIVERY_NOTE_PDF_NAME)
            out = openFileOutput(file.getName(), Context.MODE_WORLD_READABLE)
            copyPdfFile(`in`, out)
            `in`.close()
//                `in` = null
            out.flush()
            out.close()
//                out = null
        } catch (e: Exception) {
            Log.e("exception", e.message)
        }
        val intent = Intent(Intent.ACTION_VIEW)
        intent.setDataAndType(
                Uri.parse("file://" + getFilesDir() + "/" + PDFConstants.CYL_DELIVERY_NOTE_PDF_NAME),
                "application/pdf")
        startActivity(intent)
    }


    private fun checkFilePermission(): Boolean {
        if (ContextCompat.checkSelfPermission(this@PrintDocumentsActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this@PrintDocumentsActivity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), permissionRequestCode)
        } else {
            return true
        }
        return false
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == permissionRequestCode) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                createInvoicePDF(createPDFInvoiceDo)
            } else {
                Log.e(TAG, "WRITE_PERMISSION_DENIED")
            }
        }

    }

    private fun printDocuments(view: View) {
        startActivity(Intent(this@PrintDocumentsActivity, PrintActivity::class.java))
    }

    private fun createInvoicePDF(createPDFInvoiceDO: CreateInvoicePaymentDO) {
        BGInvoicePdf.getBuilder(this).build(createPDFInvoiceDO, "Email");
    }

    private fun prepareDeliveryNoteCreation(from: String) {
        val id = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")

        val spotSaleDeliveryId = preferenceUtils.getStringFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER, "")
        if (id.length > 0) {
            if (Util.isNetworkAvailable(this)) {

                val driverListRequest = ActiveDeliveryRequest(id, this@PrintDocumentsActivity)
                driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                    hideLoader()
                    if (activeDeliveryDo != null) {
                        if (isError) {
                            showAppCompatAlert("Error", "Delivery Note pdf data error", "Ok", "", "", false)
                        } else {
                            if (from.equals("Print", true)) {
                                if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
                                    printDocument(activeDeliveryDo, PrinterConstants.PrintBulkDeliveryNotes);
                                } else {
                                    printDocument(activeDeliveryDo, PrinterConstants.PrintCylinderDeliveryNotes);
                                }
                            } else {
                                if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
                                    BulkDeliveryNotePDF(this).createDeliveryPDF(activeDeliveryDo, "Email").CreateDeliveryPDF().execute()
                                } else {
                                    CYLDeliveryNotePDF(this).createDeliveryPDF(activeDeliveryDo, "Email").CreateDeliveryPDF().execute()
                                }
                            }
                        }
                    } else {
                        showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
                    }
                }
                driverListRequest.execute()

            } else {
                showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

            }

        } else {
            if (spotSaleDeliveryId.length > 0) {
                if (Util.isNetworkAvailable(this)) {

                    val driverListRequest = ActiveDeliveryRequest(spotSaleDeliveryId, this@PrintDocumentsActivity)
                    driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                        hideLoader()
                        if (activeDeliveryDo != null) {
                            if (isError) {
                                showAppCompatAlert("Error", "Delivery Note pdf data error", "Ok", "", "", false)
                            } else {
                                if (from.equals("Print", true)) {
                                    if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
                                        printDocument(activeDeliveryDo, PrinterConstants.PrintBulkDeliveryNotes);
                                    } else {
                                        printDocument(activeDeliveryDo, PrinterConstants.PrintCylinderDeliveryNotes);
                                    }
                                } else {
                                    if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
                                        BulkDeliveryNotePDF(this).createDeliveryPDF(activeDeliveryDo, "Email").CreateDeliveryPDF().execute()
                                    } else {
                                        CYLDeliveryNotePDF(this).createDeliveryPDF(activeDeliveryDo, "Email").CreateDeliveryPDF().execute()
                                    }
                                }
                            }
                        } else {
                            showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
                        }
                    }
                    driverListRequest.execute()
                } else {
                    showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

                }

            } else {
                showAppCompatAlert("Error", "Please create delivery first!!", "Ok", "", "", false)

            }

        }
    }

    private fun prepareInvoiceCreation(from: String) {
//        var id="CNV-U102-20000084"

        var id = preferenceUtils.getStringFromPreference(PreferenceUtils.PREVIEW_INVOICE_ID, "")
        var activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)
         if(activeDeliverySavedDo!=null&&activeDeliverySavedDo.invoiceNumber.isNotEmpty()){
             id =activeDeliverySavedDo.invoiceNumber
         }
//        var id="CDC-U101-19000091"//
        if (id.isNotEmpty()) {
            if (Util.isNetworkAvailable(this)) {
                val siteListRequest = InvoiceDetailsRequest(id, this)
                siteListRequest.setOnResultListener { isError, createPDFInvoiceDO ->
                    hideLoader()
                    if (createPDFInvoiceDO != null) {
                        if (isError) {
                            showAppCompatAlert("Error", "Invoice pdf api error", "Ok", "", "", false)
                        } else {
                            createPDFInvoiceDo = createPDFInvoiceDO
                            if (from.equals("Print", true)) {

                                printDocument(createPDFInvoiceDO, PrinterConstants.PrintInvoiceReport);
                            } else {
                                if (checkFilePermission()) {
                                    createInvoicePDF(createPDFInvoiceDO)
                                }
                            }
                        }
                    } else {
                        showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
                    }
                }
                siteListRequest.execute()
            } else {
                showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "",false)

            }


        } else {

            showAppCompatAlert("Error", "No Invoice Id Found", "Ok", "", "Failure", false)
        }
    }

    private fun preparePaymentCreation(from: String) {
        val id = preferenceUtils.getStringFromPreference(PreferenceUtils.PAYMENT_ID, "")
//       var id = "RCSH-U1L1911162";
        if (id.length > 0) {
            val siteListRequest = PDFPaymentDetailsRequest(id, this);
            val listener = PDFPaymentDetailsRequest.OnResultListener { isError, createPDFInvoiceDO ->
                hideLoader()
                if (createPDFInvoiceDO != null) {
                    if (isError) {
                        showAppCompatAlert("Error", "Payment pdf api error", "Ok", "", "", false)
                    } else {
                        createPDFpaymentDO = createPDFInvoiceDO
                        if (from.equals("Print", true)) {
                            printDocument(createPDFpaymentDO, PrinterConstants.PrintPaymentReport)
                        } else {
                            createPaymentPDF(createPDFInvoiceDO)
                        }
                    }
                } else {
                    showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
                }
            }
            siteListRequest.setOnResultListener(listener)
            siteListRequest.execute()
        } else {
            showAppCompatAlert("Error", "No Payment Id found", "Ok", "", "", false)

        }

    }

    private fun prepareCylinderIssueNoteCreation(from: String) {
        val id = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
        val spotSaleDeliveryId = preferenceUtils.getStringFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER, "")

//        val id="SD-U101-19000847"
        if (id.length > 0) {
            val driverListRequest = CylinderIssueRequest(id, this@PrintDocumentsActivity)
            val onResultListener = OnResultListener(from)
            driverListRequest.setOnResultListener(onResultListener)
            driverListRequest.execute()
        } else {
            if (spotSaleDeliveryId.length > 0) {
                val driverListRequest = CylinderIssueRequest(spotSaleDeliveryId, this@PrintDocumentsActivity)
                val onResultListener = OnResultListener(from)
                driverListRequest.setOnResultListener(onResultListener)
                driverListRequest.execute()
            } else {
                showAppCompatAlert("Error", "Please create delivery first!!", "Ok", "", "", false)

            }

        }
    }

    private fun OnResultListener(from: String): OnResultListener {
        return OnResultListener { isError, cylinderIssueMainDO ->
            if (cylinderIssueMainDO != null) {
                if (isError) {
                    showAppCompatAlert("Error", "Delivery Note pdf data error", "Ok", "", "", false)
                } else {
                    if (from.equals("Print", true)) {
                        printDocument(cylinderIssueMainDO, PrinterConstants.PrintCylinderIssue)
                    } else {
                        CylinderIssRecPdfBuilder.getBuilder(this@PrintDocumentsActivity).build(cylinderIssueMainDO, "Email")
                    }
                }
            } else {
                showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
            }
        }

    }

    private fun createPaymentPDF(createPDFInvoiceDO: PaymentPdfDO) {
        if (createPDFInvoiceDO != null) {
            PaymentReceiptPDF(this).createReceiptPDF(createPDFInvoiceDO, "Email")
            return

        } else {
            //Display error message
        }
    }

    private fun createCylinderIssuePDF(createPDFInvoiceDO: ActiveDeliveryMainDO) {
        val loanReturnDOS = StorageManager.getInstance(this).getReturnCylinders(this@PrintDocumentsActivity);
        val deliveryDos = StorageManager.getInstance(this).getCurrentDeliveryItems(this@PrintDocumentsActivity);

        if (deliveryDos != null && deliveryDos.size > 0) {
            createPDFInvoiceDO.activeDeliveryDOS.addAll(deliveryDos)
        }
        if (loanReturnDOS != null && loanReturnDOS.size > 0) {
            for (i in loanReturnDOS.indices) {
                val activeDeliveryD0 = ActiveDeliveryDO()
                activeDeliveryD0.productDescription = loanReturnDOS.get(i).productDescription;
                activeDeliveryD0.totalQuantity = loanReturnDOS.get(i).qty as Double;
                activeDeliveryD0.receivedQuantity = loanReturnDOS.get(i).qty as Int;

                createPDFInvoiceDO.activeDeliveryDOS.add(activeDeliveryD0)
            }
        }

        //val builder = CylinderIssRecPdfBuilder.getBuilder(this@PrintDocumentsActivity).build(createPDFInvoiceDO)
    }

    override fun onButtonNoClick(from: String) {

    }

    override fun onButtonYesClick(from: String) {
        if ("EXIT".equals(from, ignoreCase = true)) {
            setResult(3, null)
            finish()
        } else if ("FAILURE".equals(from, ignoreCase = true)) {

        } else if (from.equals("EnableBluetooth", true)) {
//            val printConnection = PrinterConnection.getInstance(this@PrintDocumentsActivity);
//            printConnection.enableBluetooth()
//            cbDeliveryPrint.isChecked = false;
            startActivity(Intent(Settings.ACTION_BLUETOOTH_SETTINGS));
        }
    }

//    private fun customerManagement(id: String, type: Int) {
//        val request = CustomerManagementRequest(id, type, this)
//        request.setOnResultListener { isError, customerDo ->
//            hideLoader()
//            if (customerDo != null) {
//                if (isError) {
//                    showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
//                } else {
//                    if (type == 1) {
//                        if (customerDo.flag.equals("Customer authorized for this transaction", true)) {
//                            if (!cbDeliveryEmail.isChecked && !cbDeliveryPrint.isChecked) {
//                                showToast("Please Check Email or Print")
//                            } else if (cbDeliveryEmail.isChecked || cbDeliveryPrint.isChecked) {
//
//                                val value = preferenceUtils.getStringFromPreference(PreferenceUtils.DELIVERY_NOTE_RESHEDULE, "")
//
//                                if (!value.equals("", true)) {
//                                    showAppCompatAlert("", "You Cannot Print Delivery Note", "Ok", "Cancel", "", false)
//                                } else {
//
//                                    prepareDeliveryNoteCreation()
//                                }
//                            }
//                        } else {
//                            showAlert("Customer Not Authorized")
//                        }
//                    } else if (type == 2) {
//                        if (customerDo.flag.equals("Customer authorized for this transaction")) {
//                            if (!cbInvoiceEmail.isChecked && !cbInvoicePrint.isChecked) {
//                                showToast("Please Check Email or Print")
//                            } else if (cbInvoicePrint.isChecked || cbInvoiceEmail.isChecked) {
//                                prepareInvoiceCreation()
//                            }
//                        } else {
//                            showAlert("Customer Not Authorized")
//                        }
//                    } else if (type == 3) {
//                        if (!cbPaymentEmail.isChecked && !cbPaymentPrint.isChecked) {
//                            showToast("Please Check Email or Print")
//                        } else if (cbPaymentEmail.isChecked || cbPaymentPrint.isChecked) {
//                            preparePaymentCreation()
//                        }
//                    } else if (type == 4) {
//                        if (!cbCylinderEmail.isChecked && !cbCylinderPrint.isChecked) {
//                            showToast("Please Check Email or Print")
//                        } else if (cbCylinderEmail.isChecked || cbCylinderPrint.isChecked) {
//                            prepareCylinderIssueNoteCreation()
//                        }
//                    }
//                }
//            } else {
//                showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
//            }
//        }
//        request.execute()
//    }

    private fun customerAuthorizationManagement(id: String, type: Int) {
        if (Util.isNetworkAvailable(this)) {
            val request = CustomerManagementRequest(id, type, this)
            request.setOnResultListener { isError, customerDo ->
                hideLoader()
                if (customerDo != null) {
                    if (isError) {
                        showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
                    } else {
                        if (type == 9) {
                            if (customerDo.flag.equals("Customer authorized for this transaction", true)) {
                                val value = preferenceUtils.getStringFromPreference(PreferenceUtils.DELIVERY_NOTE_RESHEDULE, "")
                                if (!value.equals("", true)) {
                                    showAppCompatAlert("", "You Cannot Print Delivery Note", "Ok", "Cancel", "", false)
                                } else {
                                    val podDo = StorageManager.getInstance(this).getDepartureData(this);
//                                if (podDo.podTimeCaptureValidateDeliveryTime.equals("")) {
//                                    showAppCompatAlert("", "Please Validate the delivery first", "Ok", "Cancel", "", false)
//                                }
//                                else {
//                                }
                                    prepareDeliveryNoteCreation("Email")
                                }
                            } else {
                                preferenceUtils.saveString(PreferenceUtils.DELIVERY_NOTE, "SUCCESS")
                                showAlert("Customer Not Authorized for this transaction")
                            }
                        } else if (type == 5) {
                            if (customerDo.flag.equals("Customer authorized for this transaction", true)) {
                                prepareDeliveryNoteCreation("Print")
                            } else {
                                preferenceUtils.saveString(PreferenceUtils.DELIVERY_NOTE, "SUCCESS")
                                showAlert("Customer Not Authorized for this transaction")
                            }
                        } else if (type == 10) {
                            if (customerDo.flag.equals("Customer authorized for this transaction")) {
                                prepareInvoiceCreation("Email")
                            } else {
                                showAlert("Customer Not Authorized for this transaction")
                            }
                        } else if (type == 6) {
                            if (customerDo.flag.equals("Customer authorized for this transaction")) {
                                prepareInvoiceCreation("Print")
                            } else {
                                showAlert("Customer Not Authorized for this transaction")
                            }
                        } else if (type == 11) {
                            if (customerDo.flag.equals("Customer authorized for this transaction")) {
                                preparePaymentCreation("Email")
                            } else {
                                showAlert("Customer Not Authorized for this transaction")
                            }
                        } else if (type == 7) {
                            if (customerDo.flag.equals("Customer authorized for this transaction")) {
                                preparePaymentCreation("Print")
                            } else {
                                showAlert("Customer Not Authorized for this transaction")
                            }
                        } else if (type == 12) {
                            if (customerDo.flag.equals("Customer authorized for this transaction")) {
                                if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.FixedQuantityProduct)) {
                                    prepareCylinderIssueNoteCreation("Email")
                                }
                            } else {
                                showAlert("Customer Not Authorized for this transaction")
                            }
                        } else if (type == 8) {
                            if (customerDo.flag.equals("Customer authorized for this transaction")) {
                                if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.FixedQuantityProduct)) {
                                    prepareCylinderIssueNoteCreation("Print")
                                }
                            } else {
                                showAlert("Customer Not Authorized for this transaction")
                            }
                        }

                    }
                } else {
                    showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
                }
            }
            request.execute()
        } else {
            showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

        }


    }

    private fun prePrepareDeliveryNoteCreation() {
        val id = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
        val spotSaleDeliveryId = preferenceUtils.getStringFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER, "")

        if (id.length > 0) {
            val driverListRequest = ActiveDeliveryRequest(id, this@PrintDocumentsActivity)
            driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                hideLoader()
                if (activeDeliveryDo != null) {
                    if (isError) {
                        showAppCompatAlert("Error", "Delivery Note pdf data error", "Ok", "", "", false)
                    } else {
                        if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
                            BulkDeliveryNotePDF(this).createDeliveryPDF(activeDeliveryDo, "Preview").CreatePDF().execute()
                        } else {
                            CYLDeliveryNotePDF(this).createDeliveryPDF(activeDeliveryDo, "Preview").CreatePDF().execute()
                        }
                    }
                } else {
                    showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
                }
            }
            driverListRequest.execute()
        } else {
            val driverListRequest = ActiveDeliveryRequest(spotSaleDeliveryId, this@PrintDocumentsActivity)
            driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                hideLoader()
                if (activeDeliveryDo != null) {
                    if (isError) {
                        showAppCompatAlert("Error", "Delivery Note pdf data error", "Ok", "", "", false)
                    } else {
                        if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
                            BulkDeliveryNotePDF(this).createDeliveryPDF(activeDeliveryDo, "Preview").CreatePDF().execute()
                        } else {
                            CYLDeliveryNotePDF(this).createDeliveryPDF(activeDeliveryDo, "Preview").CreatePDF().execute()
                        }
                    }
                } else {
                    showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
                }
            }
            driverListRequest.execute()
        }
    }

    private fun prePrepareInvoiceCreation() {
        var id = preferenceUtils.getStringFromPreference(PreferenceUtils.PREVIEW_INVOICE_ID, "")
        var activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)
        if(activeDeliverySavedDo!=null&&activeDeliverySavedDo.invoiceNumber.isNotEmpty()){
            id =activeDeliverySavedDo.invoiceNumber
        }
        if (id.length > 0) {
            if (Util.isNetworkAvailable(this)) {

                val siteListRequest = InvoiceDetailsRequest(id, this)
                siteListRequest.setOnResultListener { isError, createPDFInvoiceDO ->
                    hideLoader()
                    if (createPDFInvoiceDO != null) {
                        if (isError) {
                            showAppCompatAlert("Error", "Invoice pdf api error", "Ok", "", "", false)
                        } else {
                            createPDFInvoiceDo = createPDFInvoiceDO

                            if (checkFilePermission()) {
                                BGInvoicePdf.getBuilder(this).createPDF(createPDFInvoiceDO, "Preview");
                            }

                        }
                    } else {
                        showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
                    }
                }
                siteListRequest.execute()
            } else {
                showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "",false)

            }

        } else {
            showAppCompatAlert("Error", "No Invoice Id Found", "Ok", "", "Failure", false)

        }

    }

    private fun preParePaymentCreation() {
        var id = preferenceUtils.getStringFromPreference(PreferenceUtils.PAYMENT_ID, "")
//        var id ="RCCV-U1L2000031"
        if (id.length > 0) {
            val siteListRequest = PDFPaymentDetailsRequest(id, this)
            siteListRequest.setOnResultListener { isError, createPDFInvoiceDO ->
                hideLoader()
                if (createPDFInvoiceDO != null) {
                    if (isError) {
                        showAppCompatAlert("Error", "Payment pdf api error", "Ok", "", "", false)
                    } else {
                        createPDFpaymentDO = createPDFInvoiceDO
                        if (createPDFInvoiceDO != null) {
                            PaymentReceiptPDF(this).preparePDF(createPDFInvoiceDO, "Preview")
                        } else {
                            //Display error message
                        }
                    }
                } else {
                    showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
                }
            }
            siteListRequest.execute()
        } else {
            showAppCompatAlert("Error", "No Payment Id found", "Ok", "", "", false)

        }

    }


    private fun prePareCylinderIssueNoteCreation() {
        val id = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
        val spotSaleDeliveryId = preferenceUtils.getStringFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER, "")

//        var id="SD-U101-19000847"
        if (id.length > 0) {
            val driverListRequest = CylinderIssueRequest(id, this@PrintDocumentsActivity)
            driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                hideLoader()
                if (activeDeliveryDo != null) {
                    if (isError) {
                        showAppCompatAlert("Error", "Delivery Note pdf data error", "Ok", "", "", false)
                    } else {
                        if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.FixedQuantityProduct)) {
                            CylinderIssRecPdfBuilder.getBuilder(this@PrintDocumentsActivity).createPDF(activeDeliveryDo, "Preview")
                        }

                    }
                } else {
                    showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
                }
            }
            driverListRequest.execute()

        } else {
            if (spotSaleDeliveryId.length > 0) {
                val driverListRequest = CylinderIssueRequest(spotSaleDeliveryId, this@PrintDocumentsActivity)
                driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                    hideLoader()
                    if (activeDeliveryDo != null) {
                        if (isError) {
                            showAppCompatAlert("Error", "Delivery Note pdf data error", "Ok", "", "", false)
                        } else {

                            CylinderIssRecPdfBuilder.getBuilder(this@PrintDocumentsActivity).createPDF(activeDeliveryDo, "Preview")

                        }
                    } else {
                        showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
                    }
                }
                driverListRequest.execute()

            }
        }
    }

    override fun onResume() {
        super.onResume()
        pairedDevices()
    }

    override fun onStart() {
        super.onStart()
        val filter = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        registerReceiver(mReceiver, filter)
//        val filter = IntentFilter(Intent.PAIRIN.ACTION_FOUND Intent. "android.bluetooth.device.action.PAIRING_REQUEST");
//        registerReceiver(mReceiver, filter)
//        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
//        mBluetoothAdapter.startDiscovery()
//        val filter2 = IntentFilter( "android.bluetooth.device.action.PAIRING_REQUEST")
//        registerReceiver(mReceiver, filter2)

    }

    override fun onDestroy() {
        unregisterReceiver(mReceiver)
//        PrinterConstants.PRINTER_MAC_ADDRESS = ""
        super.onDestroy()
    }

    private val mReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            if (BluetoothDevice.ACTION_FOUND == action) {
                val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE);
                PrinterConstants.bluetoothDevices.add(device)
                Log.e("Bluetooth", "Discovered => name : " + device.name + ", Mac : " + device.address)
            } else if (action.equals(BluetoothDevice.ACTION_ACL_CONNECTED)) {
                Log.e("Bluetooth", "status : ACTION_ACL_CONNECTED")
                pairedDevices()
            } else if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                val state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);

                if (state == BluetoothAdapter.STATE_OFF) {
                    Log.e("Bluetooth", "status : STATE_OFF")
                } else if (state == BluetoothAdapter.STATE_TURNING_OFF) {
                    Log.e("Bluetooth", "status : STATE_TURNING_OFF")
                } else if (state == BluetoothAdapter.STATE_ON) {
                    Log.e("Bluetooth", "status : STATE_ON")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_TURNING_ON) {
                    Log.e("Bluetooth", "status : STATE_TURNING_ON")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_CONNECTING) {
                    Log.e("Bluetooth", "status : STATE_CONNECTING")
                } else if (state == BluetoothAdapter.STATE_CONNECTED) {
                    Log.e("Bluetooth", "status : STATE_CONNECTED")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_DISCONNECTED) {
                    Log.e("Bluetooth", "status : STATE_DISCONNECTED")
                }
            }
        }
    }

    private fun pairedDevices() {
        val pairedDevices = BluetoothAdapter.getDefaultAdapter().getBondedDevices()
        if (pairedDevices.size > 0) {
            for (device in pairedDevices) {
                val deviceName = device.getName()
                val mac = device.getAddress() // MAC address
//                if (StorageManager.getInstance(this).getPrinterMac(this).equals("")) {
                StorageManager.getInstance(this).savePrinterMac(this, mac);
//                }
                Log.e("Bluetooth", "Name : " + deviceName + " Mac : " + mac)
                break
            }
        }
    }



}