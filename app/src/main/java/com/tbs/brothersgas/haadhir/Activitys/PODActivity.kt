@file:Suppress("NAME_SHADOWING")

package com.tbs.brothersgas.haadhir.Activitys

import android.annotation.SuppressLint
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.location.Geocoder
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import androidx.core.content.FileProvider
import androidx.appcompat.app.AppCompatDialog
import android.util.Base64
import android.util.Log
import android.view.*
import android.widget.*
import com.tbs.brothersgas.haadhir.Model.*
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.*
import com.tbs.brothersgas.haadhir.common.AppConstants
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.pdfs.BulkDeliveryNotePDF
import com.tbs.brothersgas.haadhir.pdfs.CYLDeliveryNotePDF
import com.tbs.brothersgas.haadhir.pdfs.CylinderIssRecPdfBuilder
import com.tbs.brothersgas.haadhir.prints.PrinterConnection
import com.tbs.brothersgas.haadhir.prints.PrinterConstants
import com.tbs.brothersgas.haadhir.utils.CalendarUtils
import com.tbs.brothersgas.haadhir.utils.LogUtils
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.util.*


//
class PODActivity : BaseActivity() {

    lateinit var podDo: PodDo
    lateinit var btnConfirmDeparture: LinearLayout
    lateinit var btnConfirmArrival: LinearLayout
    lateinit var btnDeliveryDetails: LinearLayout
    lateinit var btnGenerate: LinearLayout
    var distance: Double = 0.0
    private var nonScheduleDos: ArrayList<LoadStockDO> = ArrayList()
    lateinit var btnInvoice: LinearLayout
    lateinit var btnPayments: LinearLayout
    lateinit var btnValidateDelivery: Button
    lateinit var customerManagementDO: CustomerManagementDO
    lateinit var activeDeliverySavedDo: ActiveDeliveryMainDO
    lateinit var tvATime: TextView
    lateinit var tvCaptureListTime: TextView
    lateinit var tvDepartureTime: TextView

    private var count: Int = 0
    var lat: Double? = null
    var lng: Double? = null

    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.pod_screen, null) as ScrollView
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()

        disableMenuWithBackButton()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }

        var shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
        var customerName = preferenceUtils.getStringFromPreference(PreferenceUtils.CUSTOMER_NAME, "")

        tvScreenTitleTop.visibility = View.GONE
        tvScreenTitleBottom.visibility = View.GONE
        tvScreenTitle.visibility = View.VISIBLE
        tvScreenTitle.maxLines = 2
        tvScreenTitle.setSingleLine(false)
        activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)

        tvScreenTitle.setText("" + activeDeliverySavedDo.shipmentNumber + "\n" + customerName)
//
//        //Deleting signature after submit
//        val files = File(Environment.getExternalStorageDirectory().getPath() + "/UserSignature/signature.png");
//        if (files.exists()) {
//            files.delete();
//        }
        if (intent.hasExtra("DISTANCE")) {
            distance = intent.getExtras().getDouble("DISTANCE")
        }

        var shipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, "")
        podDo = StorageManager.getInstance(this).getDepartureData(this);

        if (distance.equals(0.0)) {
            if (shipmentType.equals(getResources().getString(R.string.checkin_scheduled), true) && shipmentId.length > 0) {
                podDo.arrivalTime = CalendarUtils.getCurrentDate();
                tvATime.setText("Arrival Time : " + podDo.arrivalTime)
                podDo.podTimeCaptureArrivalTime = CalendarUtils.getTime()
                podDo.podTimeCaptureArrivalDate = CalendarUtils.getDate()
                btnConfirmArrival.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnConfirmArrival.setClickable(false)
                btnConfirmArrival.setEnabled(false)
                saveDepartureData()
                if (activeDeliverySavedDo.validateFlag == 2) {

                    btnConfirmArrival.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                    btnConfirmArrival.setClickable(false)
                    btnConfirmArrival.setEnabled(false)
                    btnDeliveryDetails.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                    btnDeliveryDetails.setClickable(false)
                    btnDeliveryDetails.setEnabled(false)

                }
                if (activeDeliverySavedDo.invoiceFlag == 2) {

                    btnInvoice.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                    btnInvoice.setClickable(false)
                    btnInvoice.setEnabled(false)
                    btnDeliveryDetails.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                    btnDeliveryDetails.setClickable(false)
                    btnDeliveryDetails.setEnabled(false)

                }
            }
        } else {
            btnConfirmArrival.setBackgroundColor(resources.getColor(R.color.white))
            btnConfirmArrival.setClickable(true)
            btnConfirmArrival.setEnabled(true)
        }

        if (podDo != null) {
            if (!podDo.arrivalTime.equals("")) {
                tvATime.setText("Arrival Time : " + podDo.arrivalTime)
                btnConfirmArrival.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnConfirmArrival.setClickable(false)
                btnConfirmArrival.setEnabled(false)
            }

            if (!podDo.capturedProductListTime.equals("")) {
                tvCaptureListTime.setText("Delivered Time : " + podDo.capturedProductListTime)
                btnDeliveryDetails.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnDeliveryDetails.setClickable(false)
                btnDeliveryDetails.setEnabled(false)
                podDo.cancelRescheduleTime = CalendarUtils.getCurrentDate();

                saveDepartureData()
            }


            if (!podDo.paymentsTime.equals("")) {
                btnGenerate.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnGenerate.setClickable(false)
                btnGenerate.setEnabled(false)
            }

            if (!podDo.podTimeCaptureValidateDeliveryDate.equals("")) {
                btnValidateDelivery.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnValidateDelivery.setClickable(false)
                btnValidateDelivery.setEnabled(false)

            }

            if (!podDo.departureTime.equals("")) {
                tvDepartureTime.setText("Departured Time : " + podDo.departureTime)
                btnConfirmDeparture.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnConfirmDeparture.setClickable(false)
                btnConfirmDeparture.setEnabled(false)

            }
            if (!podDo.invoice.equals("")) {
                btnInvoice.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnInvoice.setClickable(false)
                btnInvoice.setEnabled(false)
            }

            if (!podDo.capturedReturns.equals("")) {

            }

        } else {
            podDo = PodDo();
            if (activeDeliverySavedDo.validateFlag == 2) {

                btnConfirmArrival.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnConfirmArrival.setClickable(false)
                btnConfirmArrival.setEnabled(false)
                btnDeliveryDetails.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnDeliveryDetails.setClickable(false)
                btnDeliveryDetails.setEnabled(false)

            }
            if (activeDeliverySavedDo.invoiceFlag == 2) {

                btnInvoice.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnInvoice.setClickable(false)
                btnInvoice.setEnabled(false)
                btnDeliveryDetails.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnDeliveryDetails.setClickable(false)
                btnDeliveryDetails.setEnabled(false)

            }
        }

        if (shipmentType.equals(getResources().getString(R.string.checkin_non_scheduled), true)) {
            podDo.arrivalTime = CalendarUtils.getCurrentDate();
            tvATime.setText("Arrival Time : " + podDo.arrivalTime)
            podDo.podTimeCaptureArrivalTime = CalendarUtils.getTime()
            podDo.podTimeCaptureArrivalDate = CalendarUtils.getDate()
            btnConfirmArrival.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnConfirmArrival.setClickable(false)
            btnConfirmArrival.setEnabled(false)
            btnConfirmArrival.setVisibility(View.GONE)
            saveDepartureData()
            val customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)
            tvScreenTitle.setText("" + customerDo.customerName)
//            val nonScheduleDOs = StorageManager.getInstance(this@PODActivity).getVehicleNonScheduleData()
            loadNonVehicleStockData()


        }
        btnConfirmDeparture.setOnClickListener {
            val ShipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, getResources().getString(R.string.checkin_non_scheduled))
            if (ShipmentType.equals(getResources().getString(R.string.checkin_non_scheduled), true)) {
                showAppCompatAlert("", "Are you Leaving " + customerName + " on " + CalendarUtils.getCurrentDate() + " ?", "Confirm", "Cancel", "Departure", false)

            } else {


                if (podDo.arrivalTime.equals("")) {
                    showToast("Please complete arrival confirmation.")
                } else if (podDo.cancelRescheduleTime.equals("", true) || podDo.capturedProductListTime.equals("", true)) {
                    if (activeDeliverySavedDo.validateFlag == 2) {
                        showAppCompatAlert("", "Are you Leaving " + customerName + " on " + CalendarUtils.getCurrentDate() + " ?", "Confirm", "Cancel", "Departure", false)

                    } else {

                        showAppCompatAlert("", "Please Process the Shipment", "Confirm", "Cancel", "", false)
                    }
                } else {
                    showAppCompatAlert("", "Are you Leaving " + customerName + " on " + CalendarUtils.getCurrentDate() + " ?", "Confirm", "Cancel", "Departure", false)

                }
            }
        }


        btnConfirmArrival.setOnClickListener {
            clickOnConfirmArrival("Have you reached " + customerName + " on " + CalendarUtils.getCurrentDate() + " ?")
        }
        btnDeliveryDetails.setOnClickListener {


            if (podDo.arrivalTime.equals("")) {
                showToast("Please complete arrival confirmation.")
            } else {
                var intent = Intent(this@PODActivity, ScheduledCaptureDeliveryActivity::class.java);
                startActivityForResult(intent, 1);

            }

        }
        btnValidateDelivery.setOnClickListener {
            if (podDo.arrivalTime.equals("")) {
                showToast("Please complete arrival confirmation.")
            } else if (podDo.capturedProductListTime.equals("")) {
                showToast("Please complete Capture Delivery.")
            } else {

                val ShipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, getResources().getString(R.string.checkin_non_scheduled))
                if (ShipmentType.equals(getResources().getString(R.string.checkin_non_scheduled), true)) {
                    val shipmentNumber = preferenceUtils.getStringFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER, "")
                    if (Util.isNetworkAvailable(this)) {
                        val driverListRequest = ValidateDeliveryRequest(0.0, "", podDo.deliveryNotes, shipmentNumber, lattitudeFused, longitudeFused, addressFused, this@PODActivity)
                        driverListRequest.setOnResultListener { isError, validateDo ->
                            hideLoader()
                            if (isError) {
                                hideLoader()

                                Toast.makeText(this@PODActivity, "Validate Failure", Toast.LENGTH_SHORT).show()
                            } else {
                                if (validateDo.flag == 1 && validateDo.flag2 == 2) {
                                    hideLoader()
                                    podDo.podTimeCaptureValidateDeliveryTime = CalendarUtils.getTime()
                                    podDo.podTimeCaptureValidateDeliveryDate = CalendarUtils.getDate()
                                    btnValidateDelivery.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                                    btnValidateDelivery.setClickable(false)
                                    btnValidateDelivery.setEnabled(false)

                                    StorageManager.getInstance(this).saveDepartureData(this, podDo)
                                    Toast.makeText(this@PODActivity, "Validated  and Misc.Issue created", Toast.LENGTH_SHORT).show()

                                } else if (validateDo.flag == 1 && validateDo.flag2 == 1) {
                                    hideLoader()
                                    podDo.podTimeCaptureValidateDeliveryTime = CalendarUtils.getTime()
                                    podDo.podTimeCaptureValidateDeliveryDate = CalendarUtils.getDate()
                                    btnValidateDelivery.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                                    btnValidateDelivery.setClickable(false)
                                    btnValidateDelivery.setEnabled(false)

                                    StorageManager.getInstance(this).saveDepartureData(this, podDo)
                                    Toast.makeText(this@PODActivity, " Delivery Validated but Misc.Issue not created", Toast.LENGTH_SHORT).show()

                                } else if (validateDo.flag == 2 && validateDo.flag2 == 2) {
                                    btnValidateDelivery.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                                    btnValidateDelivery.setClickable(false)
                                    podDo.podTimeCaptureValidateDeliveryTime = CalendarUtils.getTime()
                                    podDo.podTimeCaptureValidateDeliveryDate = CalendarUtils.getDate()
                                    StorageManager.getInstance(this).saveDepartureData(this, podDo)
                                    Toast.makeText(this@PODActivity, "Already Validated", Toast.LENGTH_SHORT).show()

                                } else {
                                    Toast.makeText(this@PODActivity, "Validate Failure", Toast.LENGTH_SHORT).show()

                                }

                            }
                        }
                        driverListRequest.execute()
                    } else {
                        showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "", false)

                    }


                } else {
                    if (Util.isNetworkAvailable(this)) {
                        val driverListRequest = ValidateDeliveryRequest(0.0, "", podDo.deliveryNotes, activeDeliverySavedDo.shipmentNumber, lattitudeFused, longitudeFused, addressFused, this@PODActivity)
                        driverListRequest.setOnResultListener { isError, validateDo ->
                            hideLoader()
                            if (isError) {
                                hideLoader()
                                Toast.makeText(this@PODActivity, "Validate Failure", Toast.LENGTH_SHORT).show()
                            } else {
                                if (validateDo.flag == 1 && validateDo.flag2 == 2) {
                                    hideLoader()
                                    podDo.podTimeCaptureValidateDeliveryTime = CalendarUtils.getTime()
                                    podDo.podTimeCaptureValidateDeliveryDate = CalendarUtils.getDate()
                                    btnValidateDelivery.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                                    btnValidateDelivery.setClickable(false)
                                    btnValidateDelivery.setEnabled(false)
                                    StorageManager.getInstance(this).saveDepartureData(this, podDo)
                                    Toast.makeText(this@PODActivity, "Validated  and Misc.Issue created", Toast.LENGTH_SHORT).show()

                                } else if (validateDo.flag == 1 && validateDo.flag2 == 1) {
                                    hideLoader()
                                    podDo.podTimeCaptureValidateDeliveryTime = CalendarUtils.getTime()
                                    podDo.podTimeCaptureValidateDeliveryDate = CalendarUtils.getDate()
                                    btnValidateDelivery.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                                    btnValidateDelivery.setClickable(false)
                                    btnValidateDelivery.setEnabled(false)
                                    StorageManager.getInstance(this).saveDepartureData(this, podDo)
                                    Toast.makeText(this@PODActivity, "Delivery Validated but Misc.Issue not created", Toast.LENGTH_SHORT).show()

                                } else if (validateDo.flag == 2 && validateDo.flag2 == 2) {
                                    btnValidateDelivery.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                                    btnValidateDelivery.setClickable(false)
                                    podDo.podTimeCaptureValidateDeliveryTime = CalendarUtils.getTime()
                                    podDo.podTimeCaptureValidateDeliveryDate = CalendarUtils.getDate()
                                    StorageManager.getInstance(this).saveDepartureData(this, podDo)
                                    Toast.makeText(this@PODActivity, "Already Validated", Toast.LENGTH_SHORT).show()

                                } else {
                                    Toast.makeText(this@PODActivity, "Validate Failure", Toast.LENGTH_SHORT).show()

                                }

                            }
                        }
                        driverListRequest.execute()
                    } else {
                        showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "", false)

                    }


                }

            }

        }
        btnInvoice.setOnClickListener {
            var intent = Intent(this@PODActivity, CreateInvoiceActivity::class.java)
            startActivityForResult(intent, 1010)
        }
        btnPayments.setOnClickListener {
            var intent = Intent(this@PODActivity, InvoiceListActivity::class.java)
            startActivity(intent)
        }

    }

    override fun initializeControls() {
        tvScreenTitleTop.setVisibility(View.VISIBLE)
        tvScreenTitleBottom.setVisibility(View.VISIBLE)

        btnConfirmDeparture = findViewById(R.id.btnConfirmDeparture) as LinearLayout
        tvATime = findViewById(R.id.tvATime) as TextView
        tvCaptureListTime = findViewById(R.id.tvCaptureListTime) as TextView
        tvDepartureTime = findViewById(R.id.tvDepartureTime) as TextView

        btnConfirmArrival = findViewById(R.id.btnConfirmArrival) as LinearLayout
        btnDeliveryDetails = findViewById(R.id.btnDeliveryDetails) as LinearLayout
        btnValidateDelivery = findViewById(R.id.btnValidateDelivery) as Button
        btnGenerate = findViewById(R.id.btnGenerate) as LinearLayout
        btnInvoice = findViewById(R.id.btnInvoice) as LinearLayout
        btnPayments = findViewById(R.id.btnPayments) as LinearLayout
        val customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)

        ivCustomerLocation.setVisibility(View.VISIBLE)
        ivCustomerLocation.setOnClickListener {
            if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
                mDrawerLayout.closeDrawer(Gravity.LEFT)
            } else {
                var shipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, "")
                val popup = PopupMenu(this@PODActivity, ivCustomerLocation)
                if (shipmentType.equals(resources.getString(R.string.checkin_scheduled), true)) {
                    popup.getMenuInflater().inflate(R.menu.pod_scheduled_menu, popup.getMenu())
                } else {
                    if (customerDo.customerId.equals(resources.getString(R.string.u109))) {
                        popup.getMenuInflater().inflate(R.menu.non_scheduled_u109, popup.getMenu())

                    } else {
                        popup.getMenuInflater().inflate(R.menu.non_schedule_damage_menu, popup.getMenu())

                    }
                }
                popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                    override fun onMenuItemClick(item: MenuItem): Boolean {
                        if (item.title == "Customer Location") {
                            //   captureCustomerLocation();
                            if (shipmentType.equals(resources.getString(R.string.checkin_scheduled), true)) {
                                val activeDeliverySavedDo = StorageManager.getInstance(this@PODActivity).getActiveDeliveryMainDo(this@PODActivity)
                                val intent = Intent(this@PODActivity, CustomerLocActivty::class.java)
                                intent.putExtra("CODE", activeDeliverySavedDo.customer)
                                startActivity(intent)
                            } else {
                                val customerDo = StorageManager.getInstance(this@PODActivity).getCurrentSpotSalesCustomer(this@PODActivity)
                                val intent = Intent(this@PODActivity, CustomerLocActivty::class.java)
                                intent.putExtra("CODE", customerDo.customerId)
                                startActivity(intent)
                            }

                        } else if (item.title.equals("Force Skip")) {
                            if (shipmentType.equals(resources.getString(R.string.checkin_scheduled), true)) {
                                skipShipment(1)
                            } else {
                                if (!preferenceUtils.getStringFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER, "").isEmpty()) {
                                    if (!podDo.capturedProductListTime.isNullOrEmpty()) {
                                        skipShipment(2)
                                        //online 1 offline 2
                                    } else {
                                        skipShipment(1)
                                    }
                                } else {
                                    skipShipment(2)
                                }
                            }

                        } else if (item.title.equals("Available Stock")) {
                            val intent = Intent(this@PODActivity, AvailableStockActivity::class.java)
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            startActivity(intent)
                        } else if (item.title.equals("Change MR Location")) {
                            val intent = Intent(this@PODActivity, PODMRLocationsActivity::class.java)
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            startActivity(intent)
                        } else if (item.title.equals("Sales Return")) {
                            if (podDo.arrivalTime.equals("")) {
                                showToast("Please complete arrival confirmation.")
                            } else {
                                val intent = Intent(this@PODActivity, CaptureSalesReturnDetailsActivity::class.java);
                                startActivityForResult(intent, 11); }
                        } else if (item.title.equals("Emirates ID")) {
                            if (podDo.arrivalTime.equals("")) {
                                showToast("Please complete arrival confirmation.")
                            } else {
                                showAddItemDialog()
                            }
                        } else if (item.title.equals("Delivery Remarks")) {
                            if (podDo.arrivalTime.equals("")) {
                                showToast("Please complete arrival confirmation.")
                            } else {
                                val intent = Intent(this@PODActivity, DeliveryNotesActivity::class.java);
                                startActivityForResult(intent, 120)
                            }
                        } else if (item.title.equals("Invoice Remarks")) {
                            if (podDo.arrivalTime.equals("")) {
                                showToast("Please complete arrival confirmation.")
                            } else {
                                val intent = Intent(this@PODActivity, InvoiceNotesActivity::class.java);
                                startActivityForResult(intent, 130)
                            }
                        } else if (item.title.equals("Damaged Cylinders")) {


                            if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
                                showToast("You cannot use this function ")

                            } else {
                                if (podDo.arrivalTime.equals("")) {
                                    showToast("Please Complete Arrival First")
                                } else if (!podDo.capturedProductListTime.equals("")) {
                                    showToast("You cannot use this function ")
                                } else {
                                    var damage = preferenceUtils.getStringFromPreference(PreferenceUtils.DAMAGE, "")
                                    if (!damage.isEmpty()) {
                                        showToast("You already used this function ")
                                    } else {
                                        val intent = Intent(this@PODActivity, DamageCylindersActivity::class.java)
                                        intent.putExtra("DAMAGE", 1)
                                        startActivityForResult(intent, 98)
                                    }


                                }
                            }


                        } else if (item.title.equals("Non BG Cylinder Return")) {

                            if (shipmentType.equals(resources.getString(R.string.checkin_scheduled), true)) {
                                var nonBG = preferenceUtils.getStringFromPreference(PreferenceUtils.NONBG, "")
                                if (nonBG.isEmpty()) {
                                    if (podDo.arrivalTime.equals("")) {
                                        showToast("Please Complete Arrival First")
                                    }
//                                    else if (!podDo.capturedProductListTime.equals("")) {
//                                        showToast("You cannot use this function now")
//                                    }
                                    else {
                                        val intent = Intent(this@PODActivity, NonBGActivity::class.java)
                                        startActivityForResult(intent, 11);
                                    }
                                } else {
                                    showToast("You already used this function ")

                                }

                            } else {
                                var nonBG = preferenceUtils.getStringFromPreference(PreferenceUtils.NONBG, "")
                                var deliveryID = preferenceUtils.getStringFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER, "")

                                if (nonBG.isEmpty()) {
                                    if (deliveryID.isEmpty()) {
                                        showToast("Please Create Delivery First")
                                    } else {
                                        val intent = Intent(this@PODActivity, NonBGActivity::class.java)
                                        startActivityForResult(intent, 11);
                                    }
                                } else {
                                    showToast("You already used this function ")

                                }

                            }


                        } else if (item.title.equals("Cancel/Reschedule")) {

                            if (podDo.arrivalTime.equals("")) {
                                showToast("Please Complete Arrival First")
                            } else if (!podDo.capturedProductListTime.equals("")) {
                                showToast("You cannot use this function now")
                            } else {
                                if (activeDeliverySavedDo.validateFlag != 2) {
                                    val intent = Intent(this@PODActivity, Cancel_RescheduleActivity::class.java);
                                    startActivityForResult(intent, 511);

                                } else {
                                    showToast("You cannot use this function now")

                                }

                            }

                        } else if (item.title.equals("Loan Return Details")) {

                            if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
                                showToast("You cannot use this function ")

                            } else {
                                if (podDo.arrivalTime.equals("")) {
                                    showToast("Please complete arrival confirmation.")
                                } else {
                                    val intent = Intent(this@PODActivity, CaptureReturnDetailsActivity::class.java);
                                    intent.putExtra("IssuedQty", issuedQty)
                                    startActivityForResult(intent, 11);
                                }
                            }

                        } else if (item.title.equals("Print/Email Documents")) {
                            if (podDo.arrivalTime.equals("")) {
                                showToast("Please complete arrival confirmation.")
                            } else {
                                val intent = Intent(this@PODActivity, PrintDocumentsActivity::class.java);
                                startActivityForResult(intent, 3);
                            }
                        } else if (item.title.equals("Re-Validate Delivery")) {
                            if (podDo.arrivalTime.equals("")) {
                                showToast("Please complete arrival confirmation.")
                            }
                            if (podDo.capturedProductListTime.equals("")) {
                                showToast("Please create delivery")
                            } else {
                                if (!lattitudeFused.isNullOrEmpty()) {
                                    stopLocationUpdates()

                                    validatedelivery()
                                } else {
                                    location()

                                    showAppCompatAlert("", resources.getString(R.string.location_capture), "OK", "", "", false)

                                }

                            }
//                            else   if (!podDo.podTimeCaptureValidateDeliveryTime.equals("")) {
//                                showToast("Delivery Already Validated")
//                            }
//                            else {
//                                 if (!podDo.capturedProductListTime.equals("")) {
//                                     validatedelivery()
//                                }
//                            }

                        } else {
                            if (podDo.arrivalTime.equals("")) {
                                showToast("Please complete arrival confirmation.")
                            } else if (item.title.equals("Print Delivery Document")) {

                                var shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")

                                var shipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, "")

                                if (shipmentType.equals(resources.getString(R.string.checkin_scheduled), true)) {
                                    val activeDeliverySavedDo = StorageManager.getInstance(this@PODActivity).getActiveDeliveryMainDo(this@PODActivity)
                                    customerAuthorizationManagement(activeDeliverySavedDo.customer, shipmentId)

                                } else {
                                    val customerDo = StorageManager.getInstance(this@PODActivity).getCurrentSpotSalesCustomer(this@PODActivity)
                                    customerAuthorizationManagement(customerDo.customerId, shipmentId)

                                }
//                prepareDeliveryNoteCreation()
                            }
                        }


                        return true
                    }
                })
                popup.show()
            }
        }

    }

    fun customerManagement(id: String, cam: String, sig: String, from: String) {
        if (Util.isNetworkAvailable(this)) {
            val request = CustomerSignatureManagementRequest(id, cam, sig, this)
            request.setOnResultListener { isError, customerDo ->
                hideLoader()
                if (customerDo != null) {
                    if (isError) {
                        showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
                    } else {
                        if (!from.equals("IMAGE")) {
                            if (customerDo.sig == 2) {
//add
                                val intent = Intent(this@PODActivity, PODActivity::class.java);
                                startActivityForResult(intent, 13)

                            } else {
                                showAlert("Driver Not Authorized")
                            }
                        } else {
                            if (customerDo.cam == 2) {
//add
                                count = count + 1;
                                selectPhotoFromCamera(111)

                            } else {
                                showAlert("Driver Not Authorized")
                            }
                        }


                    }
                } else {
                    showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
                }
            }
            request.execute()
        } else {
            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "", false)

        }


    }


    private var file: File? = null

    private fun selectPhotoFromCamera(requestCode: Int) {
        try {
            val camIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            file = File(Environment.getExternalStorageDirectory(), "vansales_" + System.currentTimeMillis().toString() + ".png")
            val uri = Uri.fromFile(file)
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                camIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
            } else {
                val file = File(uri.getPath())
                val photoUri = FileProvider.getUriForFile(applicationContext, applicationContext.packageName + ".fileprovider", file)
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
            }
//            camIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
            camIntent.putExtra("return-data", true)
            startActivityForResult(camIntent, requestCode)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    @SuppressLint("MissingPermission")


    private fun customerAuthorizationManagement(id: String, type: String) {
        val request = CustomerDeliveryAuthorizationsRequest(id, type, this)
        request.setOnResultListener { isError, customerDo ->
            hideLoader()

            if (customerDo != null) {
                customerManagementDO = customerDo
                if (isError) {
                    showToast("Server Error. Please Try again!!")
//                    showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
                } else {

                    val value = preferenceUtils.getStringFromPreference(PreferenceUtils.DELIVERY_NOTE_RESHEDULE, "")
                    if (!value.equals("", true)) {

                        showAppCompatAlert("", "You Cannot Print Delivery Note", "Ok", "Cancel", "", false)
                    } else {
                        val podDo = StorageManager.getInstance(this).getDepartureData(this);

                        prepareDeliveryNoteCreation()
                    }


                }
            } else {
                showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
            }
        }
        request.execute()
    }


    private var issuedQty: Double = 0.0
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (preferenceUtils.getStringFromPreference(PreferenceUtils.INVOICE_ID, "").isNotEmpty()) {
            btnInvoice.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnInvoice.setClickable(false)
            btnInvoice.setEnabled(false)
        } else {
            btnInvoice.setBackgroundColor(resources.getColor(R.color.white))
            btnInvoice.setClickable(true)
            btnInvoice.setEnabled(true)
        }
        if (requestCode == 1 && resultCode == 1) {
            if (preferenceUtils.getStringFromPreference(PreferenceUtils.SKIP_ERROR, "").equals("SUCCESS")){
                preferenceUtils.removeFromPreference(PreferenceUtils.SKIP_ERROR)
//                finish()
            }else
                if (preferenceUtils.getStringFromPreference(PreferenceUtils.FORCE_SKIP, "").equals("SUCCESS")) {
                    showToast("Delivery has skipped")
                    StorageManager.getInstance(this).deleteActiveDeliveryMainDo(this)
                    StorageManager.getInstance(this).deleteDepartureData(this)
                    StorageManager.getInstance(this).deleteCurrentSpotSalesCustomer(this)
                    preferenceUtils.removeFromPreference(PreferenceUtils.FORCE_SKIP);
                    StorageManager.getInstance(this).deleteCurrentDeliveryItems(this)
                    StorageManager.getInstance(this).deleteReturnCylinders(this)
                    preferenceUtils.removeFromPreference(PreferenceUtils.SHIPMENT)
                    preferenceUtils.removeFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER)
                    preferenceUtils.removeFromPreference(PreferenceUtils.DELIVERY_NOTE)
                    preferenceUtils.removeFromPreference(PreferenceUtils.DELIVERY_NOTE_RESHEDULE)
                    preferenceUtils.removeFromPreference(PreferenceUtils.PRODUCT_APPROVAL);
                    preferenceUtils.removeFromPreference(PreferenceUtils.RESCHEDULE_APPROVAL);
                    preferenceUtils.removeFromPreference(PreferenceUtils.INVOICE_ID);
                    preferenceUtils.removeFromPreference(PreferenceUtils.PAYMENT_ID);
                    val intent = Intent();
                    intent.putExtra("SkipShipment", "Skip")
                    setResult(52, intent)
                    finish()
                } else {
                    if (data!!.hasExtra("DeliveredProducts")) {//delivered product list  ArrayList<ActiveDeliveryDO>
                        val deliveredProductsList = data.getSerializableExtra("DeliveredProducts") as ArrayList<ActiveDeliveryDO>
                        if (deliveredProductsList != null) {
                            for (i in deliveredProductsList.indices) {
                                issuedQty = issuedQty + deliveredProductsList.get(i).orderedQuantity
                            }
                        }
                    }
                    podDo.capturedProductListTime = CalendarUtils.getCurrentDate();
                    podDo.podTimeCaptureCaptureDeliveryTime = CalendarUtils.getTime()
                    podDo.podTimeCaptureCaptureDeliveryDate = CalendarUtils.getDate()
                    tvCaptureListTime.setText("Delivered Time : " + podDo.capturedProductListTime)
                    btnDeliveryDetails.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                    btnDeliveryDetails.setClickable(false)
                    btnDeliveryDetails.setClickable(false)
                    podDo.cancelRescheduleTime = CalendarUtils.getCurrentDate();
                    podDo.signatureEncode = StorageManager.getInstance(this).getDepartureData(this).signatureEncode
                    StorageManager.getInstance(this).saveDepartureData(this, podDo)

                }


//            StorageManager.getInstance(this).saveDepartureData(this, podDo)
//            validatedelivery()

        } else if (requestCode == 98 && resultCode == 98) {

            StorageManager.getInstance(this).saveDepartureData(this, podDo)

        } else if (requestCode == 2 && resultCode == 2) {
            podDo.paymentsTime = CalendarUtils.getCurrentDate();
            btnGenerate.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnGenerate.setClickable(false)
            btnGenerate.setClickable(false)
            StorageManager.getInstance(this).saveDepartureData(this, podDo)
        } else if (requestCode == 3 && resultCode == 3) {
            if (preferenceUtils.getStringFromPreference(PreferenceUtils.DELIVERY_NOTE, "").equals("SUCCESS", true)) {
                podDo.printsTime = CalendarUtils.getCurrentDate();

                StorageManager.getInstance(this).saveDepartureData(this, podDo)
            } else {
                showToast("You have not completed the delivery note process")
            }
        } else if (requestCode == 11 && resultCode == 11) {
            podDo.capturedReturns = CalendarUtils.getCurrentDate();
//            btnReturnDetails.setBackgroundColor(resources.getColor(R.color.md_gray_light))
//            btnReturnDetails.setClickable(false)
//            btnReturnDetails.setClickable(false)

            StorageManager.getInstance(this).saveDepartureData(this, podDo)
        } else if (requestCode == 1010 && resultCode == 10) {
//            podDo.capturedReturns = CalendarUtils.getCurrentDate();
            btnInvoice.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnInvoice.setClickable(false)
            btnInvoice.setClickable(false)

//            StorageManager.getInstance(this).saveDepartureData(this, podDo)
        } else if (requestCode == 120 && resultCode == Activity.RESULT_OK) {
            // Get String data from Intent
            val returnString = data!!.getStringExtra("DELIVERY_REMARKS")
            LogUtils.debug("DELIVERY_REMARKS", returnString)
            podDo.deliveryNotes = returnString;
            StorageManager.getInstance(this).saveDepartureData(this, podDo)
        } else if (requestCode == 130 && resultCode == Activity.RESULT_OK) {
            // Get String data from Intent
            val returnString = data!!.getStringExtra("INVOICE_REMARKS")
            LogUtils.debug("INVOICE_REMARKS", returnString)
            podDo.invoiceNotes = returnString;
            StorageManager.getInstance(this).saveDepartureData(this, podDo)
        } else if (requestCode == 13 && resultCode == Activity.RESULT_OK) {
            // Get String data from Intent
            val returnString = data!!.getStringExtra("SIGNATURE")
            val signatureEncode = data!!.getStringExtra("SignatureEncode")
            LogUtils.debug("SIGNATURE Path : ", returnString)
            LogUtils.debug("SignatureEncode : ", signatureEncode)
            podDo.signature = returnString;
            podDo.signatureEncode = signatureEncode;
            StorageManager.getInstance(this).saveDepartureData(this, podDo)
//            validatedelivery()
        } else if (requestCode == 1 && resultCode == 56) {
            // Get String data from Intent
            val returnString = data!!.getStringExtra("Status")
            //  var list = data!!.getSerializableExtra("List") as ArrayList<ActiveDeliveryDO>

            LogUtils.debug("TESTING", returnString)
            // podDo.shipmentList=list
            StorageManager.getInstance(this).saveDepartureData(this, podDo)

        } else if (requestCode == 511 && resultCode == 123) {

            val data = data!!.getStringExtra("data")
            val array = data.split(",")

            podDo.rescheduleStatus = array

            StorageManager.getInstance(this).saveDepartureData(this, podDo)
        } else if (requestCode == 511 && resultCode == 511) {
            podDo.cancelRescheduleTime = CalendarUtils.getCurrentDate();

            podDo.podTimeCaptureValidateDeliveryDate = CalendarUtils.getCurrentDate();
            podDo.capturedProductListTime = CalendarUtils.getCurrentDate();
//            podDo.unLoadingTime = CalendarUtils.getCurrentDate();
            podDo.capturedReturns = CalendarUtils.getCurrentDate();
            podDo.podTimeCaptureEndLoadingTime = CalendarUtils.getTime()
            podDo.podTimeCaptureEndLoadingDate = CalendarUtils.getDate()
            btnDeliveryDetails.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnDeliveryDetails.setClickable(false)
            btnDeliveryDetails.setClickable(false)


            btnValidateDelivery.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnValidateDelivery.setClickable(false)
            btnValidateDelivery.setClickable(false)
            StorageManager.getInstance(this).saveDepartureData(this, podDo)
            podDo.departureTime = CalendarUtils.getCurrentDate();
            tvDepartureTime.setText("Departured Time" + podDo.departureTime)
            podDo.podTimeCapturepodDepartureTime = CalendarUtils.getTime()
            podDo.podTimeCapturpodDepartureDate = CalendarUtils.getDate()

            btnConfirmDeparture.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnConfirmDeparture.setClickable(false)
            btnConfirmDeparture.setEnabled(false)
//            val ShipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, "")

//            images()
//            signatureImages()
            var shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
            podTimeCapture(shipmentId)
            val shipmentListData = StorageManager.getInstance(this).getShipmentListData(this)
            val completedShipmentsList = StorageManager.getInstance(this).getCompletedShipments(this)
            for (i in shipmentListData!!.indices) {
                for (j in completedShipmentsList.indices) {
                    if (completedShipmentsList.get(j).shipmentNumber.equals(shipmentListData.get(i).shipmentNumber, true)) {
                        shipmentListData.get(i).status = "Completed";
                        completedShipmentsList.add(shipmentListData.get(i))
                    }
                }
                if (shipmentId.equals(shipmentListData.get(i).shipmentNumber, true)) {
                    shipmentListData.get(i).status = "Completed";
                    completedShipmentsList.add(shipmentListData.get(i))
                }
            }
            //  podTimeCapture(shipmentId)
            showToast("Updated Successfuly")
            StorageManager.getInstance(this).saveDepartureData(this, podDo);
            StorageManager.getInstance(this).deleteActiveDeliveryMainDo(this)
            StorageManager.getInstance(this).deleteDepartureData(this)
            StorageManager.getInstance(this).saveCompletedShipments(this, completedShipmentsList)
            StorageManager.getInstance(this).saveShipmentListData(this, shipmentListData)
            StorageManager.getInstance(this).deleteCurrentDeliveryItems(this)
            StorageManager.getInstance(this).deleteReturnCylinders(this)
            preferenceUtils.removeFromPreference(PreferenceUtils.SHIPMENT)
            preferenceUtils.removeFromPreference(PreferenceUtils.PRICE_TAG)

            preferenceUtils.removeFromPreference(PreferenceUtils.DELIVERY_NOTE)
            preferenceUtils.removeFromPreference(PreferenceUtils.DELIVERY_NOTE_RESHEDULE)
            preferenceUtils.removeFromPreference(PreferenceUtils.INVOICE_SHIPMENT_ID);
            preferenceUtils.removeFromPreference(PreferenceUtils.PAYMENT_INVOICE_ID);
            preferenceUtils.removeFromPreference(PreferenceUtils.SELECTION_INVOICE_ID);
            preferenceUtils.removeFromPreference(PreferenceUtils.INVOICE_AMOUNT);
            preferenceUtils.removeFromPreference(PreferenceUtils.PREVIEW_INVOICE_ID);

            preferenceUtils.removeFromPreference(PreferenceUtils.PRODUCT_APPROVAL);
            preferenceUtils.removeFromPreference(PreferenceUtils.RESCHEDULE_APPROVAL);
            preferenceUtils.removeFromPreference(PreferenceUtils.INVOICE_ID);
            preferenceUtils.removeFromPreference(PreferenceUtils.NONBG);
            preferenceUtils.removeFromPreference(PreferenceUtils.EMIRATES_ID)
            preferenceUtils.removeFromPreference(PreferenceUtils.COMMENT_ID)
            preferenceUtils.removeFromPreference(PreferenceUtils.DAMAGE);

            preferenceUtils.removeFromPreference(PreferenceUtils.PAYMENT_ID);
//            preferenceUtils.removeFromPreference(PreferenceUtils.SHIPMENT_ID);

            moveBack()
            val intent = Intent();
            intent.putExtra("SkipShipment", "Cancel")
            setResult(19, intent)
            finish()


        } else if (requestCode == 111 && resultCode == Activity.RESULT_OK) {

            try {
//                val ivCamera = findViewById<ImageView>(R.id.ivCamera)
                val fis = FileInputStream(file)
                val bitmap = BitmapFactory.decodeStream(fis)
                val baos = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                val bytes = baos.toByteArray()
                val encImage = Base64.encodeToString(bytes, Base64.DEFAULT)
                podDo.capturedImagesList = encImage;
//                ivCamera.setImageBitmap(bm)// in xml file remove tint if you want to display the image
                StorageManager.getInstance(this).saveDepartureData(this, podDo)
                Log.d("PIC", encImage)
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            }
        }
    }

    private fun clickOnConfirmArrival(message: String) {
        if (!podDo.arrivalTime.equals("")) {
            showToast("Already completed")
        } else {
            showAppCompatAlert("Alert!", message, "Confirm", "Cancel", "ConfirmArrival", false);
        }
    }

    private fun clickOnStartUnloading(message: String) {
        if (!podDo.unLoadingTime.equals("")) {
            showToast("Already completed")
        } else if (podDo.arrivalTime.equals("")) {
            showToast("Please complete arrival confirmation.")
        } else {
            showAppCompatAlert("Alert!", message, "Confirm", "Cancel", "StartUnloading", false);
        }
    }

    private fun skipShipment(type: Int) {
        if (type == 1) {
            if (Util.isNetworkAvailable(this)) {
                val siteListRequest = LeaveShipmentRequest(this@PODActivity)
                siteListRequest.setOnResultListener { isError, loginDo ->
                    hideLoader()
                    if (loginDo != null) {
                        if (isError) {
                            hideLoader()
                            showAppCompatAlert("Alert!", resources.getString(R.string.server_error), "OK", "", "", false)
                        } else {
                            hideLoader()
                            if (loginDo.flag == 20) {
                                showToast("Delivery has skipped")
                                StorageManager.getInstance(this).deleteActiveDeliveryMainDo(this)
                                StorageManager.getInstance(this).deleteDepartureData(this)
                                StorageManager.getInstance(this).deleteCurrentSpotSalesCustomer(this)

                                StorageManager.getInstance(this).deleteCurrentDeliveryItems(this)
                                StorageManager.getInstance(this).deleteReturnCylinders(this)
                                preferenceUtils.removeFromPreference(PreferenceUtils.SHIPMENT)
                                preferenceUtils.removeFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER)
                                preferenceUtils.removeFromPreference(PreferenceUtils.DELIVERY_NOTE)
                                preferenceUtils.removeFromPreference(PreferenceUtils.DELIVERY_NOTE_RESHEDULE)
                                preferenceUtils.removeFromPreference(PreferenceUtils.PRODUCT_APPROVAL);
                                preferenceUtils.removeFromPreference(PreferenceUtils.RESCHEDULE_APPROVAL);
                                preferenceUtils.removeFromPreference(PreferenceUtils.INVOICE_ID);
                                preferenceUtils.removeFromPreference(PreferenceUtils.PAYMENT_ID);
                                val intent = Intent();
                                intent.putExtra("SkipShipment", "Skip")
                                setResult(52, intent)
                                finish()

                            } else {
                                showToast("Delivery not skipped")
                            }

                        }
                    } else {
                        hideLoader()
                    }

                }

                siteListRequest.execute()
            } else {
                showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "", false)

            }
        } else {
            showToast("Delivery has skipped")
            StorageManager.getInstance(this).deleteActiveDeliveryMainDo(this)
            StorageManager.getInstance(this).deleteDepartureData(this)
            StorageManager.getInstance(this).deleteCurrentSpotSalesCustomer(this)

            StorageManager.getInstance(this).deleteCurrentDeliveryItems(this)
            StorageManager.getInstance(this).deleteReturnCylinders(this)
            preferenceUtils.removeFromPreference(PreferenceUtils.SHIPMENT)
            preferenceUtils.removeFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER)
            preferenceUtils.removeFromPreference(PreferenceUtils.DELIVERY_NOTE)
            preferenceUtils.removeFromPreference(PreferenceUtils.DELIVERY_NOTE_RESHEDULE)
            preferenceUtils.removeFromPreference(PreferenceUtils.PRODUCT_APPROVAL);
            preferenceUtils.removeFromPreference(PreferenceUtils.RESCHEDULE_APPROVAL);
            preferenceUtils.removeFromPreference(PreferenceUtils.INVOICE_ID);
            preferenceUtils.removeFromPreference(PreferenceUtils.PAYMENT_ID);
            val intent = Intent();
            intent.putExtra("SkipShipment", "Skip")
            setResult(52, intent)
            finish()

        }


    }


    private fun validatedelivery() {
//        var  podDo = StorageManager.getInstance(this).getDepartureData(this);
        podDo = StorageManager.getInstance(this).getDepartureData(this);

        val ShipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, getResources().getString(R.string.checkin_non_scheduled))
        if (ShipmentType.equals(getResources().getString(R.string.checkin_non_scheduled), true)) {
            val shipmentNumber = preferenceUtils.getStringFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER, "")
            if (shipmentNumber.length > 0) {
                val driverListRequest = ValidateDeliveryRequest(0.0, "", podDo.deliveryNotes, shipmentNumber, lattitudeFused, longitudeFused, addressFused, this@PODActivity)
                driverListRequest.setOnResultListener { isError, validateDo ->
                    hideLoader()
                    if (isError) {
                        hideLoader()

                        showToast("Shipment not validated")
                    } else {
                        if (validateDo.flag == 1 && validateDo.flag2 == 2) {
                            hideLoader()
                            podDo.podTimeCaptureValidateDeliveryTime = CalendarUtils.getTime()
                            podDo.podTimeCaptureValidateDeliveryDate = CalendarUtils.getDate()
                            if (validateDo.message.length > 0) {
                                showToast("" + validateDo.message)

                            } else {

                                Toast.makeText(this@PODActivity, "Shipment Validated", Toast.LENGTH_SHORT).show()
                            }


                            StorageManager.getInstance(this).saveDepartureData(this, podDo)


                            var shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")

                            var shipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, "")

                            if (shipmentType.equals(resources.getString(R.string.checkin_scheduled), true)) {
                                val activeDeliverySavedDo = StorageManager.getInstance(this@PODActivity).getActiveDeliveryMainDo(this@PODActivity)
                                customerAuthorizationManagement(activeDeliverySavedDo.customer, shipmentId)

                            } else {
                                val customerDo = StorageManager.getInstance(this@PODActivity).getCurrentSpotSalesCustomer(this@PODActivity)
                                customerAuthorizationManagement(customerDo.customerId, shipmentId)

                            }

//                        val intent = Intent()
//                        intent.putExtra("SIGNATURE", StoredPath)
//                        intent.putExtra("SignatureEncode", encodedImage)
//
//                        setResult(Activity.RESULT_OK, intent)
//                        finish()
                        } else if (validateDo.flag == 1 && validateDo.flag2 == 1) {
                            hideLoader()
                            podDo.podTimeCaptureValidateDeliveryTime = CalendarUtils.getTime()
                            podDo.podTimeCaptureValidateDeliveryDate = CalendarUtils.getDate()

                            StorageManager.getInstance(this).saveDepartureData(this, podDo)
                            if (validateDo.message.length > 0) {
                                showToast("" + validateDo.message)

                            } else {

                                Toast.makeText(this@PODActivity, "Shipment Validated", Toast.LENGTH_SHORT).show()
                            }

                            StorageManager.getInstance(this).saveDepartureData(this, podDo)


                            var shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")

                            var shipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, "")

                            if (shipmentType.equals(resources.getString(R.string.checkin_scheduled), true)) {
                                val activeDeliverySavedDo = StorageManager.getInstance(this@PODActivity).getActiveDeliveryMainDo(this@PODActivity)
                                customerAuthorizationManagement(activeDeliverySavedDo.customer, shipmentId)

                            } else {
                                val customerDo = StorageManager.getInstance(this@PODActivity).getCurrentSpotSalesCustomer(this@PODActivity)
                                customerAuthorizationManagement(customerDo.customerId, shipmentId)

                            }
//
//                        val intent = Intent()
//                        intent.putExtra("SIGNATURE", StoredPath)
//                        intent.putExtra("SignatureEncode", encodedImage)
//
//                        setResult(Activity.RESULT_OK, intent)
//                        finish()

                        } else if (validateDo.flag == 2 && validateDo.flag2 == 2) {

                            podDo.podTimeCaptureValidateDeliveryTime = CalendarUtils.getTime()
                            podDo.podTimeCaptureValidateDeliveryDate = CalendarUtils.getDate()
                            StorageManager.getInstance(this).saveDepartureData(this, podDo)
                            if (validateDo.message.length > 0) {
                                showToast("" + validateDo.message)

                            } else {

                                Toast.makeText(this@PODActivity, "Shipment already validated", Toast.LENGTH_SHORT).show()
                            }

                            StorageManager.getInstance(this).saveDepartureData(this, podDo)


                            var shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")

                            var shipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, "")

                            if (shipmentType.equals(resources.getString(R.string.checkin_scheduled), true)) {
                                val activeDeliverySavedDo = StorageManager.getInstance(this@PODActivity).getActiveDeliveryMainDo(this@PODActivity)
                                customerAuthorizationManagement(activeDeliverySavedDo.customer, shipmentId)

                            } else {
                                val customerDo = StorageManager.getInstance(this@PODActivity).getCurrentSpotSalesCustomer(this@PODActivity)
                                customerAuthorizationManagement(customerDo.customerId, shipmentId)

                            }

//                        val intent = Intent()
//                        intent.putExtra("SIGNATURE", StoredPath)
//                        intent.putExtra("SignatureEncode", encodedImage)
//
//                        setResult(Activity.RESULT_OK, intent)
//                        finish()
                        } else {
                            if (validateDo.message.length > 0) {
                                showToast("" + validateDo.message)
                            } else {
                                showToast("Shipment not validated")
                            }

                        }

                    }
                }
                driverListRequest.execute()
            } else {
                Toast.makeText(this@PODActivity, "Delivery Id not found", Toast.LENGTH_SHORT).show()

            }

        } else {
            var activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)

            val driverListRequest = ValidateDeliveryRequest(0.0, "", podDo.deliveryNotes, activeDeliverySavedDo.shipmentNumber, lattitudeFused, longitudeFused, addressFused, this@PODActivity)
            driverListRequest.setOnResultListener { isError, validateDo ->
                hideLoader()
                if (isError) {
                    hideLoader()
                    Toast.makeText(this@PODActivity, "Shipment not validated", Toast.LENGTH_SHORT).show()
                } else {
                    if (validateDo.flag == 1 && validateDo.flag2 == 2) {
                        hideLoader()
                        podDo.podTimeCaptureValidateDeliveryTime = CalendarUtils.getTime()
                        podDo.podTimeCaptureValidateDeliveryDate = CalendarUtils.getDate()

                        StorageManager.getInstance(this).saveDepartureData(this, podDo)
                        if (validateDo.message.length > 0) {
                            showToast("" + validateDo.message)

                        } else {

                            Toast.makeText(this@PODActivity, "Shipment  validated", Toast.LENGTH_SHORT).show()
                        }

                        StorageManager.getInstance(this).saveDepartureData(this, podDo)


                        var shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")

                        var shipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, "")

                        if (shipmentType.equals(resources.getString(R.string.checkin_scheduled), true)) {
                            val activeDeliverySavedDo = StorageManager.getInstance(this@PODActivity).getActiveDeliveryMainDo(this@PODActivity)
                            customerAuthorizationManagement(activeDeliverySavedDo.customer, shipmentId)

                        } else {
                            val customerDo = StorageManager.getInstance(this@PODActivity).getCurrentSpotSalesCustomer(this@PODActivity)
                            customerAuthorizationManagement(customerDo.customerId, shipmentId)

                        }

//                        val intent = Intent()
//                        intent.putExtra("SIGNATURE", StoredPath)
//                        intent.putExtra("SignatureEncode", encodedImage)
//
//                        setResult(Activity.RESULT_OK, intent)
//                        finish()

                    } else if (validateDo.flag == 1 && validateDo.flag2 == 1) {
                        hideLoader()
                        podDo.podTimeCaptureValidateDeliveryTime = CalendarUtils.getTime()
                        podDo.podTimeCaptureValidateDeliveryDate = CalendarUtils.getDate()

                        StorageManager.getInstance(this).saveDepartureData(this, podDo)
                        if (validateDo.message.length > 0) {
                            showToast("" + validateDo.message)

                        } else {

                            Toast.makeText(this@PODActivity, "Shipment  validated", Toast.LENGTH_SHORT).show()
                        }

                        StorageManager.getInstance(this).saveDepartureData(this, podDo)


                        var shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")

                        var shipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, "")

                        if (shipmentType.equals(resources.getString(R.string.checkin_scheduled), true)) {
                            val activeDeliverySavedDo = StorageManager.getInstance(this@PODActivity).getActiveDeliveryMainDo(this@PODActivity)
                            customerAuthorizationManagement(activeDeliverySavedDo.customer, shipmentId)

                        } else {
                            val customerDo = StorageManager.getInstance(this@PODActivity).getCurrentSpotSalesCustomer(this@PODActivity)
                            customerAuthorizationManagement(customerDo.customerId, shipmentId)

                        }

//                        val intent = Intent()
//                        intent.putExtra("SIGNATURE", StoredPath)
//                        intent.putExtra("SignatureEncode", encodedImage)
//
//                        setResult(Activity.RESULT_OK, intent)
//                        finish()
                    } else if (validateDo.flag == 2 && validateDo.flag2 == 2) {

                        podDo.podTimeCaptureValidateDeliveryTime = CalendarUtils.getTime()
                        podDo.podTimeCaptureValidateDeliveryDate = CalendarUtils.getDate()
                        StorageManager.getInstance(this).saveDepartureData(this, podDo)
                        if (validateDo.message.length > 0) {
                            showToast("" + validateDo.message)

                        } else {

                            Toast.makeText(this@PODActivity, "Shipment Already validated", Toast.LENGTH_SHORT).show()
                        }

                        StorageManager.getInstance(this).saveDepartureData(this, podDo)


//                        val intent = Intent()
//                        intent.putExtra("SIGNATURE", StoredPath)
//                        intent.putExtra("SignatureEncode", encodedImage)
//
//                        setResult(Activity.RESULT_OK, intent)
//                        finish()

                        var shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")

                        var shipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, "")

                        if (shipmentType.equals(resources.getString(R.string.checkin_scheduled), true)) {
                            val activeDeliverySavedDo = StorageManager.getInstance(this@PODActivity).getActiveDeliveryMainDo(this@PODActivity)
                            customerAuthorizationManagement(activeDeliverySavedDo.customer, shipmentId)

                        } else {
                            val customerDo = StorageManager.getInstance(this@PODActivity).getCurrentSpotSalesCustomer(this@PODActivity)
                            customerAuthorizationManagement(customerDo.customerId, shipmentId)

                        }
                    } else {
                        if (validateDo.message.length > 0) {
                            showToast("" + validateDo.message)

                        } else {

                            Toast.makeText(this@PODActivity, "Shipment not validated", Toast.LENGTH_SHORT).show()
                        }


                    }

                }
            }
            driverListRequest.execute()
        }

    }


    override fun onButtonYesClick(from: String) {
        super.onButtonYesClick(from)
        var shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
        var shipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, "")

        if (from.equals("ConfirmArrival")) {
            if (shipmentType.equals(getResources().getString(R.string.checkin_scheduled), true) && shipmentId.length > 0) {
                podDo.arrivalTime = CalendarUtils.getCurrentDate();
                tvATime.setText("Arrival Time : " + podDo.arrivalTime)
                podDo.podTimeCaptureArrivalTime = CalendarUtils.getTime()
                podDo.podTimeCaptureArrivalDate = CalendarUtils.getDate()
                btnConfirmArrival.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnConfirmArrival.setClickable(false)
                btnConfirmArrival.setEnabled(false)
                saveDepartureData()
            } else {
                val customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)
                podDo.arrivalTime = CalendarUtils.getCurrentDate();
                tvATime.setText("Arrival Time : " + podDo.arrivalTime)
                podDo.podTimeCaptureArrivalTime = CalendarUtils.getTime()
                podDo.podTimeCaptureArrivalDate = CalendarUtils.getDate()
                btnConfirmArrival.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnConfirmArrival.setClickable(false)
                btnConfirmArrival.setEnabled(false)
                saveDepartureData()

            }
        } else if (from.equals("Departure")) {

            val ShipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, "")
            if (ShipmentType.equals("Non Scheduled", true)) {
                podTimeCapture("")

            } else {

                podTimeCapture(shipmentId)

            }
        }
    }

    private fun departureClear() {
        podDo.departureTime = CalendarUtils.getCurrentDate();
        tvDepartureTime.setText("Departured Time" + podDo.departureTime)
        podDo.podTimeCapturepodDepartureTime = CalendarUtils.getTime()
        podDo.podTimeCapturpodDepartureDate = CalendarUtils.getDate()
        StorageManager.getInstance(this).saveDepartureData(this, podDo)
        StorageManager.getInstance(this).deleteCurrentSpotSalesCustomer(this)
        StorageManager.getInstance(this).deleteActiveDeliveryMainDo(this)
        StorageManager.getInstance(this).deleteDepartureData(this)
        StorageManager.getInstance(this).deleteCurrentDeliveryItems(this)
        StorageManager.getInstance(this).deleteReturnCylinders(this)
        StorageManager.getInstance(this).deleteCurrentSpotSalesCustomer(this)
        preferenceUtils.removeFromPreference(PreferenceUtils.SHIPMENT)
        preferenceUtils.removeFromPreference(PreferenceUtils.DELIVERY_NOTE)
        preferenceUtils.removeFromPreference(PreferenceUtils.DELIVERY_NOTE_RESHEDULE)
        preferenceUtils.removeFromPreference(PreferenceUtils.INVOICE_SHIPMENT_ID);
        preferenceUtils.removeFromPreference(PreferenceUtils.PAYMENT_INVOICE_ID);
        preferenceUtils.removeFromPreference(PreferenceUtils.SELECTION_INVOICE_ID);
        preferenceUtils.removeFromPreference(PreferenceUtils.INVOICE_AMOUNT);
        preferenceUtils.removeFromPreference(PreferenceUtils.NONBG);
        preferenceUtils.removeFromPreference(PreferenceUtils.PREVIEW_INVOICE_ID);
        preferenceUtils.removeFromPreference(PreferenceUtils.EMIRATES_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.COMMENT_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.DAMAGE);
        preferenceUtils.removeFromPreference(PreferenceUtils.PRODUCT_APPROVAL);
        preferenceUtils.removeFromPreference(PreferenceUtils.RESCHEDULE_APPROVAL);
        preferenceUtils.removeFromPreference(PreferenceUtils.INVOICE_ID);
        preferenceUtils.removeFromPreference(PreferenceUtils.PAYMENT_ID);
        preferenceUtils.removeFromPreference(PreferenceUtils.PRICE_TAG)
        preferenceUtils.removeFromPreference(PreferenceUtils.CustomerId);
        preferenceUtils.removeFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER);
        preferenceUtils.removeFromPreference(PreferenceUtils.DAMAGE);
        preferenceUtils.removeFromPreference(PreferenceUtils.SALES_ORDER_NUMBER);
        preferenceUtils.removeFromPreference(PreferenceUtils.FINANTIAL_SITE_ID);
        preferenceUtils.removeFromPreference(PreferenceUtils.CREQUESTED_NUMBER);
        preferenceUtils.removeFromPreference(PreferenceUtils.REQUESTED_NUMBER);
        preferenceUtils.removeFromPreference(PreferenceUtils.CREDIT_FLAG);
        preferenceUtils.removeFromPreference(PreferenceUtils.CREDIT_AMOUNT);
        preferenceUtils.removeFromPreference(PreferenceUtils.POD_NAME);
        preferenceUtils.removeFromPreference(PreferenceUtils.POD_NUMBER);
        preferenceUtils.removeFromPreference(PreferenceUtils.METER_1);
        preferenceUtils.removeFromPreference(PreferenceUtils.METER_2);
        preferenceUtils.removeFromPreference(PreferenceUtils.MR_CODE);
        btnConfirmDeparture.setBackgroundColor(resources.getColor(R.color.md_gray_light))
        btnConfirmDeparture.setClickable(false)
        btnConfirmDeparture.setEnabled(false)
        hideLoader()
        moveBack()
    }

    private fun prepareDeliveryNoteCreation() {
        val id = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
        val spotSaleDeliveryId = preferenceUtils.getStringFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER, "")
        if (id.length > 0) {
            if (Util.isNetworkAvailable(this)) {

                val driverListRequest = ActiveDeliveryRequest(id, this@PODActivity)
                driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                    hideLoader()
                    if (activeDeliveryDo != null) {
                        if (isError) {
                            showAppCompatAlert("Error", "Delivery Note pdf data error", "Ok", "", "", false)
                        } else {
                            if (customerManagementDO.deliveryEmail == 2) {
                                if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
                                    BulkDeliveryNotePDF(this).createDeliveryPDF(activeDeliveryDo, "Email").CreateDeliveryPDF().execute()
                                } else {
                                    CYLDeliveryNotePDF(this).createDeliveryPDF(activeDeliveryDo, "Email").CreateDeliveryPDF().execute()
                                }
                            }
                            if (customerManagementDO.cylinderEmail == 2) {
                                if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.FixedQuantityProduct)) {
                                    prepareCylinderIssueNoteCreation()
                                }

                            }

                            if (customerManagementDO.deliveryPrint == 2) {
                                if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
                                    printDocument(activeDeliveryDo, PrinterConstants.PrintBulkDeliveryNotes);
                                } else {
                                    printDocument(activeDeliveryDo, PrinterConstants.PrintCylinderDeliveryNotes);
                                }
                            }


                        }
                    } else {
                        showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
                    }
                }
                driverListRequest.execute()
            } else {
                showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

            }


        } else {
            if (spotSaleDeliveryId.length > 0) {
                if (Util.isNetworkAvailable(this)) {

                    val driverListRequest = ActiveDeliveryRequest(spotSaleDeliveryId, this@PODActivity)
                    driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                        hideLoader()
                        if (activeDeliveryDo != null) {
                            if (isError) {
                                showAppCompatAlert("Error", "Delivery Note pdf data error", "Ok", "", "", false)
                            } else {
                                if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
                                    printDocument(activeDeliveryDo, PrinterConstants.PrintBulkDeliveryNotes);
                                } else {
                                    printDocument(activeDeliveryDo, PrinterConstants.PrintCylinderDeliveryNotes);
                                }

                                if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
                                    BulkDeliveryNotePDF(this).createDeliveryPDF(activeDeliveryDo, "Email").CreateDeliveryPDF().execute()
                                } else {
                                    CYLDeliveryNotePDF(this).createDeliveryPDF(activeDeliveryDo, "Email").CreateDeliveryPDF().execute()
                                }

                            }
                        } else {
                            showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
                        }
                    }
                    driverListRequest.execute()
                } else {
                    showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

                }

            } else {
                showAppCompatAlert("Info!", "Please Create delivery first!!", "Ok", "", "", false)

            }

        }
    }

    private fun printDocument(obj: Any, from: Int) {
        val printConnection = PrinterConnection.getInstance(this@PODActivity)
        if (printConnection.isBluetoothEnabled()) {
            printConnection.connectToPrinter(obj, from)
        } else {
//            PrinterConstants.PRINTER_MAC_ADDRESS = ""
            showToast("Please enable your mobile Bluetooth.")
//            showAppCompatAlert("", "Please enable your mobile Bluetooth.", "Enable", "Cancel", "EnableBluetooth", false)
        }
    }

    private fun pairedDevices() {
        val pairedDevices = BluetoothAdapter.getDefaultAdapter().getBondedDevices()
        if (pairedDevices.size > 0) {
            for (device in pairedDevices) {
                val deviceName = device.getName()
                val mac = device.getAddress() // MAC address
//                if (StorageManager.getInstance(this).getPrinterMac(this).equals("")) {
                StorageManager.getInstance(this).savePrinterMac(this, mac);
//                }
                Log.e("Bluetooth", "Name : " + deviceName + " Mac : " + mac)
                break
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (preferenceUtils.getStringFromPreference(PreferenceUtils.INVOICE_ID, "").isNotEmpty()) {
            btnInvoice.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnInvoice.setClickable(false)
            btnInvoice.setEnabled(false)
        } else {
            btnInvoice.setBackgroundColor(resources.getColor(R.color.white))
            btnInvoice.setClickable(true)
            btnInvoice.setEnabled(true)
        }
        pairedDevices()
    }

    override fun onStart() {
        super.onStart()
        val filter = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        registerReceiver(mReceiver, filter)
//        val filter = IntentFilter(Intent.PAIRIN.ACTION_FOUND Intent. "android.bluetooth.device.action.PAIRING_REQUEST");
//        registerReceiver(mReceiver, filter)
//        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
//        mBluetoothAdapter.startDiscovery()
//        val filter2 = IntentFilter( "android.bluetooth.device.action.PAIRING_REQUEST")
//        registerReceiver(mReceiver, filter2)

    }

    override fun onDestroy() {
        unregisterReceiver(mReceiver)
//        PrinterConstants.PRINTER_MAC_ADDRESS = ""
        hideLoader()
        super.onDestroy()
    }

    private val mReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            if (BluetoothDevice.ACTION_FOUND == action) {
                val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE);
                PrinterConstants.bluetoothDevices.add(device)
                Log.e("Bluetooth", "Discovered => name : " + device.name + ", Mac : " + device.address)
            } else if (action.equals(BluetoothDevice.ACTION_ACL_CONNECTED)) {
                Log.e("Bluetooth", "status : ACTION_ACL_CONNECTED")
                pairedDevices()
            } else if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                val state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);

                if (state == BluetoothAdapter.STATE_OFF) {
                    Log.e("Bluetooth", "status : STATE_OFF")
                } else if (state == BluetoothAdapter.STATE_TURNING_OFF) {
                    Log.e("Bluetooth", "status : STATE_TURNING_OFF")
                } else if (state == BluetoothAdapter.STATE_ON) {
                    Log.e("Bluetooth", "status : STATE_ON")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_TURNING_ON) {
                    Log.e("Bluetooth", "status : STATE_TURNING_ON")
                } else if (state == BluetoothAdapter.STATE_CONNECTING) {
                    Log.e("Bluetooth", "status : STATE_CONNECTING")
                } else if (state == BluetoothAdapter.STATE_CONNECTED) {
                    Log.e("Bluetooth", "status : STATE_CONNECTED")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_DISCONNECTED) {
                    Log.e("Bluetooth", "status : STATE_DISCONNECTED")
                }
            }
        }
    }

    private fun prepareCylinderIssueNoteCreation() {
        val id = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
//        val id="SD-U101-19000847"
        if (id.length > 0) {
            val driverListRequest = CylinderIssueRequest(id, this@PODActivity)
            val onResultListener = OnResultListener()
            driverListRequest.setOnResultListener(onResultListener)
            driverListRequest.execute()
        } else {
            val spotSaleDeliveryId = preferenceUtils.getStringFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER, "")
            if (spotSaleDeliveryId.length > 0) {
                val driverListRequest = CylinderIssueRequest(spotSaleDeliveryId, this@PODActivity)
                val onResultListener = OnResultListener()
                driverListRequest.setOnResultListener(onResultListener)
                driverListRequest.execute()
            } else {
                showToast("Delivery id not found")
            }
        }
    }

    private fun OnResultListener(): OnResultListener {
        return OnResultListener { isError, cylinderIssueMainDO ->
            if (cylinderIssueMainDO != null) {
                hideLoader()
                if (isError) {
//                    showToast("Delivery Note pdf data error")
//                    showAppCompatAlert("Error", "Delivery Note pdf data error", "Ok", "", "", false)
                } else {
                    CylinderIssRecPdfBuilder.getBuilder(this@PODActivity).build(cylinderIssueMainDO, "Email")

                    printDocument(cylinderIssueMainDO, PrinterConstants.PrintCylinderIssue)

                }
            } else {
//                showToast("Delivery Note pdf data error")

//                showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
            }
        }

    }

    private fun moveBack() {
        val intent = Intent();
        intent.putExtra("CapturedReturns", true)
        setResult(12, intent)
        finish()
    }

    private fun saveDepartureData() {
        StorageManager.getInstance(this).saveDepartureData(this, podDo);
    }

    private fun podTimeCapture(id: String) {
        showLoader()

        var driverId = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "")
        var date = CalendarUtils.getDate()
        var time = CalendarUtils.getTime()
//        08-01-2019   10:50
        val files = File(Environment.getExternalStorageDirectory().getPath() + "/UserSignature/signature.png");
        if (files.exists()) {
            files.delete();
        }
        var doctype = ""
        var podDo = StorageManager.getInstance(this).getDepartureData(this);
        var arrivaldate = podDo.podTimeCaptureArrivalDate
        var arrivalTime = podDo.podTimeCaptureArrivalTime
        var startTime = podDo.podTimeCaptureStartLoadingTime
        var startdate = podDo.podTimeCaptureStartLoadingDate
        var cdTime = podDo.podTimeCaptureCaptureDeliveryTime
        var cdDate = podDo.podTimeCaptureCaptureDeliveryDate
        var validateTime = podDo.podTimeCaptureValidateDeliveryTime
        var validateDate = podDo.podTimeCaptureValidateDeliveryDate
        var endloadingtime = podDo.podTimeCaptureEndLoadingTime
        var endloadingdate = podDo.podTimeCaptureEndLoadingDate
        var departuredate = podDo.podTimeCapturpodDepartureDate
        var departuretime = podDo.podTimeCapturepodDepartureTime
        if (id.isEmpty()) {
            doctype = ""
        } else {
            doctype = "4"
        }
        val siteListRequest = PODTimeCaptureRequest(driverId, date, doctype, id, arrivaldate, arrivalTime, startTime, startdate,
                cdTime, cdDate, validateTime, validateDate, endloadingtime, endloadingdate, departuredate, departuretime, this)
        siteListRequest.setOnResultListener { isError, loginDo ->
            hideLoader()

            if (isError) {
                hideLoader()
                showToast(resources.getString(R.string.server_error))
            } else {
                hideLoader()
                if (loginDo != null) {
                    if (loginDo.flag == 1) {
                        departureClear()
                    } else {
                        showToast("Unable to departure, Contact your support team")
                    }
                } else {
                    hideLoader()
                    showToast(resources.getString(R.string.server_error))

                }


            }


        }

        siteListRequest.execute()
    }


    override fun onBackPressed() {
        AppConstants.CapturedReturns = false;
        super.onBackPressed()
    }

    private fun loadNonVehicleStockData() {
        if (Util.isNetworkAvailable(this)) {

            var nonScheduledRootId = "" + preferenceUtils.getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, "")
            if (nonScheduledRootId.length > 0) {
                val loadVanSaleRequest = NonScheduledLoadVanSaleRequest(nonScheduledRootId, this@PODActivity)
                showLoader()
                loadVanSaleRequest.setOnResultListener { isError, loadStockMainDo ->
                    hideLoader()

                    if (isError) {
                        showToast("Non Scheduled data not found.")
                        btnDeliveryDetails.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        btnDeliveryDetails.setClickable(false)
                        btnDeliveryDetails.setEnabled(false)


                        btnValidateDelivery.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        btnValidateDelivery.setClickable(false)
                        btnValidateDelivery.setEnabled(false)

                    } else {
                        hideLoader()
                        nonScheduleDos = loadStockMainDo.loadStockDOS;
                        if (nonScheduleDos == null || nonScheduleDos.size < 1) {


                            btnDeliveryDetails.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                            btnDeliveryDetails.setClickable(false)
                            btnDeliveryDetails.setEnabled(false)


                            btnValidateDelivery.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                            btnValidateDelivery.setClickable(false)
                            btnValidateDelivery.setEnabled(false)
                        }

                    }
                }
                loadVanSaleRequest.execute()
            }


        } else {
            showToast("No Internet connection, please try again later")
        }
    }

    private fun showAddItemDialog() {
        try {
            var dialog = AppCompatDialog(this, R.style.AppCompatAlertDialogStyle)
            dialog.supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
            val view = LayoutInflater.from(this).inflate(R.layout.item_custom, null)
//            applyFont(view, AppConstants.MONTSERRAT_REGULAR_TYPE_FACE)
            var etAdd = view.findViewById<View>(R.id.etAdd) as EditText

            dialog.setCancelable(true)
            dialog.setCanceledOnTouchOutside(true)
            val btnSubmit = view.findViewById<View>(R.id.btnSubmit) as Button




            btnSubmit.setOnClickListener {
                val itemName = etAdd.getText().toString().trim()

                if (itemName.equals("", ignoreCase = true)) {
                    showToast("Please Enter ID")
                } else {

                    preferenceUtils.saveString(PreferenceUtils.EMIRATES_ID, itemName);
                    dialog.dismiss()
                }
            }
            dialog.setContentView(view)
            if (!dialog.isShowing())
                dialog.show()
        } catch (e: Exception) {
        }

    }


}