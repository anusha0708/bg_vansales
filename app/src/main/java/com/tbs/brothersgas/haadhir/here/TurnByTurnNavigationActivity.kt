//
//
//package com.tbs.brothersgas.haadhir.here
//
//import android.Manifest
//import android.annotation.SuppressLint
//import android.content.Context
//import android.content.Intent
//import android.content.IntentSender
//import android.content.pm.PackageManager
//import android.location.*
//import android.location.LocationListener
//import android.net.Uri
//import android.os.Build
//import android.os.Bundle
//import android.os.Looper
//import android.webkit.PermissionRequest
//import android.widget.Toast
//import androidx.core.app.ActivityCompat
//import com.google.android.gms.common.api.ApiException
//import com.google.android.gms.common.api.ResolvableApiException
//import com.google.android.gms.location.*
//import com.karumi.dexter.Dexter
//import com.karumi.dexter.PermissionToken
//import com.karumi.dexter.listener.PermissionDeniedResponse
//import com.karumi.dexter.listener.PermissionGrantedResponse
//import com.karumi.dexter.listener.single.PermissionListener
//
//import com.tbs.brothersgas.haadhir.Activitys.BaseActivity
//import com.tbs.brothersgas.haadhir.R
//import java.text.DateFormat
//import java.util.*
//
//
///**
// * Main activity which launches map view and handles Android run-time requesting permission.
// */
//
//class TurnByTurnNavigationActivity : BaseActivity() {
//    private var mLastUpdateTime: String? = null
//    private val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 10000
//    private val FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS: Long = 5000
//    private val REQUEST_CHECK_SETTINGS = 100
//    private var mFusedLocationClient: FusedLocationProviderClient? = null
//    private var mSettingsClient: SettingsClient? = null
//    private var mLocationRequest: LocationRequest? = null
//    private var mLocationSettingsRequest: LocationSettingsRequest? = null
//    private var mLocationCallback: LocationCallback? = null
//    private var mCurrentLocation: Location? = null
//    private var mRequestingLocationUpdates: Boolean? = null
//    private var m_mapFragmentView: MapFragmentView? = null
//
//    private var isMapLoaded: Boolean = false
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.turn_by_turn_layout)
//        showLoader()
//      locationFetch()
//    }
//
//
//    private fun setupMapFragmentView() {
//        // All permission requests are being handled. Create map fragment view. Please note
//        // the HERE Mobile SDK requires all permissions defined above to operate properly.
//        m_mapFragmentView = MapFragmentView(this@TurnByTurnNavigationActivity)
//    }
//
//    public override fun onDestroy() {
//        if (m_mapFragmentView != null) {
//            m_mapFragmentView!!.onDestroy()
//        }
//        super.onDestroy()
//    }
//
//    override fun initialize() {
//
//
//    }
//
//    override fun initializeControls() {
//
//    }
//
//
//
//    companion object {
//
//        private val REQUEST_CODE_ASK_PERMISSIONS = 1
//        private val RUNTIME_PERMISSIONS = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.INTERNET, Manifest.permission.ACCESS_WIFI_STATE, Manifest.permission.ACCESS_NETWORK_STATE)
//    }
//    fun locationFetch() {
//        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
//        mSettingsClient = LocationServices.getSettingsClient(this)
//
//        mLocationCallback = object : LocationCallback() {
//            override fun onLocationResult(locationResult: LocationResult?) {
//                super.onLocationResult(locationResult)
//                // location is received
//                mCurrentLocation = locationResult!!.lastLocation
//                mLastUpdateTime = DateFormat.getTimeInstance().format(Date())
//
//                updateLocUI()
//            }
//        }
//
//        mRequestingLocationUpdates = false
//
//        mLocationRequest = LocationRequest()
//        mLocationRequest!!.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS)
//        mLocationRequest!!.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS)
//        mLocationRequest!!.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
//
//        val builder = LocationSettingsRequest.Builder()
//        builder.addLocationRequest(mLocationRequest!!)
//        mLocationSettingsRequest = builder.build()
//
//        Dexter.withActivity(this)
//                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
//                .withListener(object : PermissionListener {
//                    override fun onPermissionRationaleShouldBeShown(permission: com.karumi.dexter.listener.PermissionRequest?, token: PermissionToken?) {
//                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//                    }
//
//                    override fun onPermissionGranted(response: PermissionGrantedResponse) {
//                        mRequestingLocationUpdates = true
//                        startLocUpdates()
//                    }
//
//                    override fun onPermissionDenied(response: PermissionDeniedResponse) {
//                        if (response.isPermanentlyDenied()) {
//
//                            showToast("Location Permission denied")
//                            // open device settings when the permission is
//                            // denied permanently
////                            openSettings()
//                        }
//                    }
//
//                    fun onPermissionRationaleShouldBeShown(permission: PermissionRequest, token: PermissionToken) {
//                        token.continuePermissionRequest()
//                    }
//                }).check()
//    }
//
//    fun updateLocUI() {
//        if (mCurrentLocation != null) {
//            stopLocUpdates()
//            hideLoader()
//            hideLoader()
//
//
//            lattitudeFused = mCurrentLocation!!.getLatitude().toString()
//            longitudeFused = mCurrentLocation!!.getLongitude().toString()
//            var gcd = Geocoder(getBaseContext(), Locale.getDefault());
//            Constants.Src_Lattitude =  mCurrentLocation!!.getLatitude()
//            Constants.Src_Longitude =  mCurrentLocation!!.getLongitude()
//            if (!isMapLoaded) {
//                setupMapFragmentView()
//                isMapLoaded = true
//            }
//            try {
//                var addresses = gcd.getFromLocation(mCurrentLocation!!.getLatitude(),
//                        mCurrentLocation!!.getLongitude(), 1);
//                if (addresses.size > 0) {
//                    System.out.println(addresses.get(0).getLocality());
//                    var cityName = addresses.get(0).getLocality();
//                    addressFused = cityName
//                }
//            } catch (e: java.lang.Exception) {
//                e.printStackTrace();
//            }
//
//
//        }
//
//    }
//
//    @SuppressLint("MissingPermission")
//    private fun startLocUpdates() {
//        mSettingsClient!!
//                .checkLocationSettings(mLocationSettingsRequest)
//                .addOnSuccessListener(this) {
//                    //                    Log.i(TAG, "All location settings are satisfied.")
//
////                    Toast.makeText(applicationContext, "Started location updates!", Toast.LENGTH_SHORT).show()
//
//
//                    mFusedLocationClient!!.requestLocationUpdates(mLocationRequest,
//                            mLocationCallback, Looper.myLooper())
//
//                    updateLocUI()
//                }
//                .addOnFailureListener(this) { e ->
//                    val statusCode = (e as ApiException).statusCode
//                    when (statusCode) {
//                        LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
////                            Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " + "location settings ")
//                            try {
//                                // Show the dialog by calling startResolutionForResult(), and check the
//                                // result in onActivityResult().
//                                val rae = e as ResolvableApiException
//                                rae.startResolutionForResult(this@TurnByTurnNavigationActivity, REQUEST_CHECK_SETTINGS)
//                            } catch (sie: IntentSender.SendIntentException) {
////                                Log.i(TAG, "PendingIntent unable to execute request.")
//                            }
//
//                        }
//                        LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
//                            val errorMessage = "Location settings are inadequate, and cannot be " + "fixed here. Fix in Settings."
////                            Log.e(TAG, errorMessage)
//
//                            Toast.makeText(this@TurnByTurnNavigationActivity, errorMessage, Toast.LENGTH_LONG).show()
//                        }
//                    }
//
//                    updateLocUI()
//                }
//    }
//
//    fun stopLocUpdates() {
//        // Removing location updates
//        if (mFusedLocationClient != null && mLocationCallback != null) {
//            mFusedLocationClient!!
//                    .removeLocationUpdates(mLocationCallback)
//                    .addOnCompleteListener(this) {
//
//                    }
//        }
//
//    }
//}
