package com.tbs.brothersgas.haadhir.decanting

import android.app.DatePickerDialog
import android.content.Intent
import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import com.tbs.brothersgas.haadhir.Activitys.BaseActivity
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryDO
import com.tbs.brothersgas.haadhir.Model.CustomerDo
import com.tbs.brothersgas.haadhir.Model.VehicleDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.ExecutiveActiveDeliveryRequest
import com.tbs.brothersgas.haadhir.delivery_executive.DeliveryExecutiveShipmentDetailsActivity
import com.tbs.brothersgas.haadhir.history.DeliverysAdapter
import com.tbs.brothersgas.haadhir.listeners.ResultListner
import com.tbs.brothersgas.haadhir.pdfs.CYLDeliveryNotePDF
import com.tbs.brothersgas.haadhir.utils.Constants
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import kotlinx.android.synthetic.main.activity_delivery_executive_list.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class DecantingVehicleListActivity : BaseActivity() {
    private var screenType: Int = 1
    lateinit var userId: String
    lateinit var orderCode: String
    lateinit var llOrderHistory: View
    lateinit var customerDos: ArrayList<CustomerDo>
    private lateinit var btnAddMore: Button
    lateinit var llSearch: LinearLayout
    lateinit var etSearch: EditText
    private var cday: Int = 0
    private var cmonth: Int = 0
    private var cyear: Int = 0
    var startDate = ""
    var endDate = ""
    var to = ""

    companion object {
        var isRefreshed: Boolean = false
    }

    override fun initialize() {
        llOrderHistory = layoutInflater.inflate(R.layout.activity_delivery_executive_list, null)
        llBody.addView(llOrderHistory, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            val intent = Intent()
            setResult(1, intent)
            finish()
        }
        disableMenuWithBackButton()
        initializeControls()
        tvScreenTitle.setText(getString(R.string.select_vehicle))
        ll_dates.visibility = View.GONE
        getList();


    }

    private fun getList() {

        if (Util.isNetworkAvailable(this)) {
            val driverListRequest = DecantingVehicleListRequest(this)
            driverListRequest.setOnResultListener { isError, vehicleDOS ->
                hideLoader()
                if (isError) {
                    tvNoOrders.visibility = View.VISIBLE
                    recycleview.visibility = View.GONE
                    showAlert(resources.getString(R.string.server_error))
                } else {
                    if (vehicleDOS != null) {
                        if (vehicleDOS != null && vehicleDOS.size > 0) {
                            tvNoOrders.visibility = View.GONE
                            recycleview.visibility = View.VISIBLE
                            var deliverysAdapter = VehicleAdapter(this, vehicleDOS)
                            recycleview!!.adapter = deliverysAdapter
                            deliverysAdapter.deliveryExexutive(true, ResultListner { `object`, isSuccess ->
                                var vehicleDO = `object` as VehicleDO
                                preferenceUtils.saveString(PreferenceUtils.VEHICLE_CODE, vehicleDO.vehicle)
                                finish()
                            })
                        } else {
                            tvNoOrders.visibility = View.VISIBLE
                            recycleview.visibility = View.GONE
                        }
                    }

                }
            }
            driverListRequest.execute()

        } else {
            showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

        }
    }


    override fun initializeControls() {
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == 1) {
            finish()
        }
    }

    override fun onResume() {
        super.onResume()
        if (isRefreshed) {
            isRefreshed = false
            if (screenType == 1) {
                tvScreenTitle.setText(getString(R.string.to_do_list))
                ll_dates.visibility = View.GONE
                getList();
            }
        }
    }

}
