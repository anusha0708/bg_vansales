package com.tbs.brothersgas.haadhir.history

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.brothersgas.haadhir.Activitys.BaseActivity
import com.tbs.brothersgas.haadhir.Adapters.SpotSalesCustomerAdapter
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryDO
import com.tbs.brothersgas.haadhir.Model.CustomerDo
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.CustomerNewRequest
import com.tbs.brothersgas.haadhir.collector.CollectorCustomerAdapter
import com.tbs.brothersgas.haadhir.collector.CollectorCustomerNewRequest
import com.tbs.brothersgas.haadhir.collector.CollectorPreviewActivity
import com.tbs.brothersgas.haadhir.collector.TransactionListActivity
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.pdfs.utils.PDFConstants
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import java.util.*
import kotlin.collections.ArrayList

class CustomerActivity : BaseActivity() {
    lateinit var userId: String
    lateinit var orderCode: String
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var customerAdapter: CustomerAdapter
    lateinit var llOrderHistory: LinearLayout
     var customerDos: ArrayList<CustomerDo> = ArrayList<CustomerDo>()
    private lateinit var tvScreenTitles : TextView
    private lateinit var llSearch : LinearLayout
    private lateinit var etSearch : EditText
    private lateinit var ivClearSearch : ImageView
    private lateinit var ivGoBack : ImageView
    private lateinit var ivSearchs : ImageView
    private lateinit var tvNoOrders : TextView
    private lateinit var ivSync : ImageView




    override fun initialize() {
        llOrderHistory = layoutInflater.inflate(R.layout.site_screen, null) as LinearLayout
        llBody.addView(llOrderHistory, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        flToolbar.visibility = View.GONE
        disableMenuWithBackButton()
        initializeControls()
        tvScreenTitles.setText("Select Customer")

        tvScreenTitles.visibility = View.VISIBLE
        ivGoBack.setOnClickListener{
            finish()
        }
        ivSync.visibility=View.GONE
        ivSync.setOnClickListener {
            val driverListRequest = CustomerRequest(this@CustomerActivity)
            driverListRequest.setOnResultListener { isError, customerDOs ->
                hideLoader()
                if (isError) {
                    Toast.makeText(this@CustomerActivity, R.string.error_customer_list, Toast.LENGTH_SHORT).show()
                } else {
                    customerDos = customerDOs
                    hideLoader()

                    if (customerDos != null && customerDos.size > 0) {
//                        StorageManager.getInstance(this@CustomerActivity).saveSpotSalesCustomerList(this@CustomerActivity, customerDos);
                        customerAdapter = CustomerAdapter(this@CustomerActivity, customerDos,"")
                        recycleview.adapter = customerAdapter
                        tvNoOrders.visibility = View.GONE
                        recycleview.visibility = View.VISIBLE
                    } else {
                        tvNoOrders.visibility = View.VISIBLE
                        recycleview.visibility = View.GONE
                        showToast("No customers")
                    }
                }
            }
            driverListRequest.execute()
        }

        ivSearchs.setOnClickListener{
            //            if(llSearch.visibility == View.VISIBLE){
//                llSearch.visibility = View.INVISIBLE
//            }
//            else{
//            }
            tvScreenTitles.visibility= View.GONE
            llSearch.visibility = View.VISIBLE
        }

        ivClearSearch.setOnClickListener{
            etSearch.setText("")
            if(customerDos!=null &&customerDos.size>0){
                customerAdapter = CustomerAdapter(this@CustomerActivity, customerDos,"")
                recycleview.adapter = customerAdapter
                tvNoOrders.visibility = View.GONE
                recycleview.visibility = View.VISIBLE
            }
            else{
                tvNoOrders.visibility = View.VISIBLE
                recycleview.visibility = View.GONE
            }
        }

        etSearch.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(editable: Editable?) {
                if(etSearch.text.toString().equals("", true)){
                    if(customerDos!=null &&customerDos.size>0){
                        customerAdapter        =  CustomerAdapter(this@CustomerActivity, customerDos,"")
                        recycleview.adapter    = customerAdapter
                        tvNoOrders.visibility  = View.GONE
                        recycleview.visibility = View.VISIBLE
                    }
                    else{
                        tvNoOrders.visibility  = View.VISIBLE
                        recycleview.visibility = View.GONE
                    }
                }
                else if(etSearch.text.toString().length>2){
                    filter(etSearch.text.toString())
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })

        val driverListRequest = CustomerRequest(this@CustomerActivity)
//        customerDos = StorageManager.getInstance(this).getSpotSalesCustomerList(this)
//        customerDos = StorageManager.getInstance(this).getSpotSalesCustomerList(this)
//
//        if(customerDos!=null &&customerDos.size>0){
//            customerAdapter = CustomerAdapter(this@CustomerActivity, customerDos,"")
//            recycleview.adapter = customerAdapter
//            recycleview.visibility = View.VISIBLE
//            tvNoOrders.visibility = View.GONE
//        }
//        else{
            driverListRequest.setOnResultListener { isError, customerDOs ->
                hideLoader()
                if (isError) {
                    Toast.makeText(this@CustomerActivity, R.string.error_customer_list, Toast.LENGTH_SHORT).show()
                } else {
                    customerDos = customerDOs
                    if(customerDos!=null &&customerDos.size>0){
//                        StorageManager.getInstance(this).saveSpotSalesCustomerList(this, customerDOs);
                        customerAdapter        = CustomerAdapter(this@CustomerActivity, customerDos,"")
                        recycleview.adapter    = customerAdapter
                        recycleview.visibility = View.VISIBLE
                        tvNoOrders.visibility  = View.GONE
                    }
                    else{
                        recycleview.visibility = View.GONE
                        tvNoOrders.visibility  = View.VISIBLE
                        showToast("customers not found")
                    }
                }
            }
            driverListRequest.execute()
//        }
    }

    override fun initializeControls() {
        recycleview                 = findViewById<View>(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        llSearch                    = findViewById<View>(R.id.llSearch) as LinearLayout
        etSearch                    = findViewById<View>(R.id.etSearch) as EditText
        ivClearSearch               = findViewById<View>(R.id.ivClearSearch) as ImageView
        ivGoBack                    = findViewById<View>(R.id.ivGoBack) as ImageView
        ivSearchs                   = findViewById<View>(R.id.ivSearchs) as ImageView
        tvNoOrders                  = findViewById<View>(R.id.tvNoOrders) as TextView
        tvScreenTitles              = findViewById<View>(R.id.tvScreenTitles) as TextView
        ivSync                      = findViewById<View>(R.id.ivSync) as ImageView
        recycleview!!.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this@CustomerActivity, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
    }

    private fun filter(filtered : String) : ArrayList<CustomerDo>{
        val customerDOs = ArrayList<CustomerDo>()
        for (i in customerDos.indices){
            if(customerDos.get(i).customerName.contains(filtered, true)
                    ||customerDos.get(i).customerId.contains(filtered, true)){
                customerDOs.add(customerDos.get(i))
            }
        }
        if(customerDOs.size>0){
            customerAdapter.refreshAdapter(customerDOs)
            tvNoOrders.visibility = View.GONE
            recycleview.visibility = View.VISIBLE
        }
        else{
            tvNoOrders.visibility = View.VISIBLE
            recycleview.visibility = View.GONE
        }
        return customerDOs
    }


}
