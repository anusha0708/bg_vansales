package com.tbs.brothersgas.haadhir.decanting

import android.annotation.TargetApi
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.brothersgas.haadhir.Activitys.BaseActivity
import com.tbs.brothersgas.haadhir.Model.CustomerDo
import com.tbs.brothersgas.haadhir.Model.VehicleDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.delivery_executive.DeliveryExecutiveActivityReportActivity
import com.tbs.brothersgas.haadhir.delivery_executive.DeliveryExecutiveListActivity
import com.tbs.brothersgas.haadhir.listeners.ResultListner
import com.tbs.brothersgas.haadhir.utils.CalendarUtils
import com.tbs.brothersgas.haadhir.utils.Constants
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import kotlinx.android.synthetic.main.activity_dashboard_dcentive.*
import kotlinx.android.synthetic.main.activity_delivery_executive_list.*
import kotlinx.android.synthetic.main.dec_checkin_screen.*

class DecantingCheckinActivity : BaseActivity() {
    lateinit var userId: String
    lateinit var orderCode: String
    lateinit var llOrderHistory: View
    lateinit var customerDos: ArrayList<CustomerDo>
    private lateinit var btnAddMore: Button
    lateinit var llSearch: LinearLayout
    lateinit var etSearch: EditText
    var REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 200
    override fun initialize() {
        llOrderHistory = layoutInflater.inflate(R.layout.dec_checkin_screen, null)
        llBody.addView(llOrderHistory, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            finish()
        }
        disableMenuWithBackButton()
        initializeControls()



    }

    override fun initializeControls() {


        llVehicle.setOnClickListener {
            val intent = Intent(this, DecantingVehicleListActivity::class.java)
            startActivity(intent)
        }

        llCheckIn.setOnClickListener {
            showAppCompatAlert("Alert!", resources.getString(R.string.checkinconfirm),
                    "OK", "Cancel", "Checkin", true)


        }


    }

    private fun doCheckin() {

        if (Util.isNetworkAvailable(this)) {
            val request = DecantingCheckinRequest(this,1)
            request.setOnResultListener { isError, modelDO ->
                hideLoader()
                if (isError) {
                    showAlert(resources.getString(R.string.server_error))

                    //invoiceList()
                } else {
                    if (modelDO != null && modelDO.flag == 20) {
                        showToast("Updated Successfully...")
                        preferenceUtils.saveString(PreferenceUtils.CHECKIN_DATE,CalendarUtils.getCurrentDate())
                        finish()

                    } else {
                        showAlert(resources.getString(R.string.server_error))

                    }

                }
            }
            request.execute()

        } else {
            showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

        }
    }

    override fun onButtonYesClick(from: String) {
        super.onButtonYesClick(from)
        if(from.equals("Checkin")){
            doCheckin()
        }

    }

    override fun onResume() {
        var vehicleCode=preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_CODE,"")
        if(vehicleCode.isNotEmpty()){

            tvVehicleCodee.setText("Vehicle Selection \n"+vehicleCode)
        }
        if(preferenceUtils.getStringFromPreference(PreferenceUtils.CHECKIN_DATE,"").isNotEmpty()){
            llVehicle.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            llVehicle.setClickable(false)
            llVehicle.setEnabled(false)
            llCheckIn.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            llCheckIn.setClickable(false)
            llCheckIn.setEnabled(false)
        }
        tvScreenTitle.setText("Check in")
        super.onResume()
    }
}

