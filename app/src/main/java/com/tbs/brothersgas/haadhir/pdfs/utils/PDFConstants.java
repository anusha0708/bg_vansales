package com.tbs.brothersgas.haadhir.pdfs.utils;

/*
 * Created by developer on 3/2/19.
 */
public interface PDFConstants {
    String MAIN_LOGO_NAME = "brogas_logo.png";
    //    String MAIN_LOGO_NAME =/*"bgas.png"*/"bg.jpg";
    String TAQUAT_LOGO_NAME = "taqat_logo.jpg";

    String ISO_LOGO_NAME = "iso_logo.png";
    String AUTHOR = "TBS";
    String CREATOR = "Venu Appasani";
    String MAIL_TO = "tema.vansales@gmail.com";

    //CYLDeliveryNotePDF Constants
    String HEADER_1 = "Brothers Gas Bottling & Distribution Co. LLC";
    String HEADER_2 = "Delivery Note";
    String CYL_DELIVERY_NOTE_PDF_NAME = "CYLDeliveryNote.pdf";
    String CYL_NAME = "Brother's Gas - Sales Delivery Note";
    String D_R_NO = "D.N. No";
    String DELIVERY_TO = "Delivered To";
    String PAYMENT_TERM = "Payment Term";
    String VEHICLE_CODE   = "Vehicle Code";
    String USER_NAME      = "User Name";
    String RECIEVER_NUMBER= "Reciever No.";
    String RECIEVER_NAME   = "Receiver Name";
    String START_READING        = "Starting Reading";
    String END_READING      = "Ending Reading";
    String VEHICLE_NUMBER = "Plate No.";
    String SALES_NUMBER = "Order No.";
    String LPO_NUMBER = "LPO No.";
    String CUSTOMER_ID = "Customer code";
    String SIGN_DATE = "Signed Date";
    String SIGN_TIME = "Signed Time";


    String ADDRESS = "Address";
    String DATE = "Date";
    String TIME = "Time";
    String USER_ID = "User ID";
    String SR_NO = "Sr. No.";
    String ITEM = "Item";
    String QUANTITY = "Quantity";
    String COLOUMN = ":";

    //BG Invoice
    String BG_INVOICE_PDF_NAME = "BGInvoice.pdf";
    String HEADER_TEXT_BG_INVOICE = "Commercial and Tax Invoice";
    String INVOICE_NUMBER = "Invoice Number";
    String CUSTOMER_NAME = "Customer Name";
    String CUSTOMER_TRN = "Customer TRN";
    String CUSTOMER_ADDRESS = "Customer Address";
    String BG_INVOCE_NAME = "Brother's Gas - Sales Invoice";
    String PRE_INVOICE_NUMBER = "PreInvoice Number";
    String DELIVERY_NUMBER = "Delivery Number";

    //Taqat Invoice
    String TAQAT_INVOICE_PDF_NAME = "TaqatInvoice.pdf";
    String TAQAT_INVOCE_NAME = "Brother's Gas - Taqat Invoice Copy";
    String OPENING_READING = "Opening \n Reading";
    String ENDING_READING = "Ending \n Reading";
    String NET_QUANTITY = "Net Quantity";

    //RECEPIT
    String RECEICPT_NAME = "Brother's Gas - Payment Receipt";
    String RECEIPT_NO = "Receipt No.";
    String RECEIVED_FROM = "Received From";
    String RECEIPT_PDF_NAME = "TbsReceipt.pdf";


    //CylinderIssRec
    String CYLINDER_ISS_REC_PDF_NAME = "CylinderIssRec.pdf";
    String CYLINDER_ISS_REC = "Brother's Gas - Cylinder Issuance Receipt";

    //Bulk delivery note

    String BULK_DELIVERY_NOTE_PDF_NAME = "BulkDeliveryNote.pdf";

    //Common fields
    String SUPPLIER_NAME = "Supplier Name";
    String SUPPLIER_TRN = "Supplier TRN";
    String REGISTERED_ADDRESS = "Registered Address";
    String PARTICULARS = "Particulars";
    String QTY = "QTY";
    String UNIT_PRICE = "Unit Price (AED)";
    String AMOUNT = "Amount (AED)";
}
