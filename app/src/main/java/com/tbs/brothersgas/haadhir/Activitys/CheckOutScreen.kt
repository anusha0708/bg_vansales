package com.tbs.brothersgas.haadhir.Activitys

import android.app.Dialog
import android.content.Intent
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.RecyclerView
import android.view.Gravity
import android.view.ViewGroup
import android.widget.*
import com.tbs.brothersgas.haadhir.Model.DriverIdMainDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.Model.VehicleCheckInDo
import com.tbs.brothersgas.haadhir.Requests.CheckoutTimeCaptureRequest
import com.tbs.brothersgas.haadhir.utils.CalendarUtils
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util


class CheckOutScreen : BaseActivity() {
    private var llAboutUs: RelativeLayout? = null
    lateinit var confirmArrivalDialog: Dialog
    lateinit var confirmStartLoadingDialog: Dialog
    lateinit var confirmEndLoadingDialog: Dialog
    lateinit var scheduleddialog: Dialog
    lateinit var Nonscheduleddialog: Dialog
    lateinit var confirmCheckinDialog: Dialog

    lateinit var btnConfirmArrival: Button
    lateinit var btnStartUnLoading: Button
    lateinit var btnEndUnloading: Button
    lateinit var btnVehicleInspection: Button
    lateinit var btnGateInspection: Button
    lateinit var btnCheckOut: Button
    lateinit var btnUnloadVanSaleStock: Button

    lateinit var tvScheduledRoutingId: TextView
    lateinit var tvNonScheduledRoutingId: TextView
    lateinit var tvArrivalTime: TextView
    lateinit var tvStartUnLoadingDateTime: TextView
    lateinit var tvEndUnLoadingDateTime: TextView
    lateinit var tvNotes: TextView
    lateinit var driverIdMainDo: DriverIdMainDO
    lateinit var recyclerView: androidx.recyclerview.widget.RecyclerView
    lateinit var recyclerViewEmpty: androidx.recyclerview.widget.RecyclerView
    lateinit var tvCheckOutDateTime: TextView
    lateinit var vehicleCheckInDo: VehicleCheckInDo


    override fun initialize() {

        llAboutUs = layoutInflater.inflate(R.layout.checkout_screen, null) as RelativeLayout
        llBody.addView(llAboutUs, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, Gravity.LEFT)

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }

        vehicleCheckInDo = StorageManager.getInstance(this).getVehicleCheckOutData(this)
        //  disableMenuWithBackButton();

        if (vehicleCheckInDo != null) {
            if (!vehicleCheckInDo.arrivalCheckOutTime.equals("")) {
                tvArrivalTime.text = "Arrival : " + vehicleCheckInDo.arrivalCheckOutTime
                btnConfirmArrival.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnConfirmArrival.setClickable(false)
                btnConfirmArrival.setEnabled(false);
            }
            if (!vehicleCheckInDo.startUnLoadingTime.equals("")) {
                tvStartUnLoadingDateTime.text = "Start Load : " + vehicleCheckInDo.startUnLoadingTime
                btnStartUnLoading.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnStartUnLoading.setClickable(false)
                btnStartUnLoading.setEnabled(false);
            }
            if (!vehicleCheckInDo.unLoadStock.equals("")) {
                btnUnloadVanSaleStock.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnUnloadVanSaleStock.setClickable(false)
                btnUnloadVanSaleStock.setEnabled(false)
            }
            if (!vehicleCheckInDo.endUnLoadTime.equals("")) {
                tvEndUnLoadingDateTime.text = "End Load : " + vehicleCheckInDo.endUnLoadTime
                btnEndUnloading.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnEndUnloading.setClickable(false)
                btnEndUnloading.setEnabled(false)
            }
            if (!vehicleCheckInDo.vehicleCheckOutInspectionId.equals("")) {
                btnVehicleInspection.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnVehicleInspection.setClickable(false)
                btnVehicleInspection.setEnabled(false)
            }
            if (!vehicleCheckInDo.gateCheckOutInspectionId.equals("")) {
                btnGateInspection.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnGateInspection.setClickable(false)
                btnGateInspection.setEnabled(false)
            }

//            var notes = vehicleCheckInDo.notes
//            if (vehicleCheckInDo.notes!!.length > 0) {
//                val notess1 = notes!!.substring(Math.max(notes.length - 55, 55))
//                val notess2 = notess1.substring(0, notess1.length - 5)
//                tvNotes.text = "Instructions to Driver : " + notess2
//            }
            if (!vehicleCheckInDo.checkOutTime.equals("")) {
                btnCheckOut.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnCheckOut.setClickable(false)
                btnCheckOut.setEnabled(false)
                tvCheckOutDateTime.text = "Check In : " + vehicleCheckInDo.checkOutTime
            }

        } else {
            vehicleCheckInDo = VehicleCheckInDo()
        }

        btnGateInspection.setOnClickListener {
            clickOnGateInspection()
//
//            var id = preferenceUtils.getStringFromPreference(PreferenceUtils.CHECK_IN_GATE_INSPECTION, "")
//            if (id.equals(R.string.checkin_gate_inspection.toString())) {
//                showAppCompatAlert("Alert!", "Already Completed", "OK", "", "FAILURE", false)
//
//            } else {
//                var id = preferenceUtils.getStringFromPreference(PreferenceUtils.CHECK_IN_ARRIVAL, "")
//                if (id.equals(R.string.checkin_arrival.toString())) {
//
//                    var id = preferenceUtils.getStringFromPreference(PreferenceUtils.CHECK_IN_START_LOADING, "")
//                    if (id.equals(R.string.checkin_startload.toString())) {
//                        var id = preferenceUtils.getStringFromPreference(PreferenceUtils.CHECK_IN_SHEDULED, "")
//
//                        if (id.equals(R.string.checkin_scheduled.toString())) {
//                            var id = preferenceUtils.getStringFromPreference(PreferenceUtils.CHECK_IN_END_LOADING, "")
//
//                            if (id.equals(R.string.checkin_endload.toString())) {
//                                var id = preferenceUtils.getStringFromPreference(PreferenceUtils.CHECK_IN_VEHICLE_INSPECTION, "")
//
//                                if (id.equals(R.string.checkin_vehicle_inspection.toString())) {
//
//                                    val intent = Intent(this@CheckinScreen, GateInspectonActivity::class.java)
//                                    startActivity(intent)
//                                } else {
//                                    showAppCompatAlert("Alert!", "Please Complete Vehicle inspection", "OK", "", "FAILURE", false)
//
//                                }
//
//                            } else {
//                                showAppCompatAlert("Alert!", "Please Complete End Loading", "OK", "", "FAILURE", false)
//
//                            }
//                        } else {
//                            showAppCompatAlert("Alert!", "Please Select Routing Id", "OK", "", "FAILURE", false)
//
//                        }
//                    } else {
//                        showAppCompatAlert("Alert!", "Please Complete Start Loading", "OK", "", "FAILURE", false)
//
//                    }
//                } else {
//                    showAppCompatAlert("Alert!", "Please Complete Confirm Arrival", "OK", "", "FAILURE", false)
//
//                }
//            }

        }

        btnVehicleInspection.setOnClickListener {

            clickOnVehicleInspection()
//
//            var id = preferenceUtils.getStringFromPreference(PreferenceUtils.CHECK_IN_ARRIVAL, "")
//            if (id.equals(R.string.checkin_arrival.toString())) {
//
//                    var id = preferenceUtils.getStringFromPreference(PreferenceUtils.CHECK_IN_START_LOADING, "")
//                    if (id.equals(R.string.checkin_startload.toString())) {
//                        var id = preferenceUtils.getStringFromPreference(PreferenceUtils.CHECK_IN_SHEDULED, "")
//
//                        if (id.equals(R.string.checkin_scheduled.toString())) {
//                            var id = preferenceUtils.getStringFromPreference(PreferenceUtils.CHECK_IN_END_LOADING, "")
//
//                            if (id.equals(R.string.checkin_endload.toString())) {
//
//                                val intent = Intent(this@CheckinScreen, VehicleInspectionActivity::class.java)
//                                startActivity(intent)
//                            } else {
//                                showAppCompatAlert("Alert!", "Please Complete End Loading", "OK", "", "FAILURE", false)
//
//                            }
//                        } else {
//                            showAppCompatAlert("Alert!", "Please Select Routing Id", "OK", "", "FAILURE", false)
//
//                        }
//                    } else {
//                        showAppCompatAlert("Alert!", "Please Complete Start Loading", "OK", "", "FAILURE", false)
//
//                    }
//                } else {
//                    showAppCompatAlert("Alert!", "Please Complete Confirm Arrival", "OK", "", "FAILURE", false)
//
//                }
//            }

        }

        btnConfirmArrival.setOnClickListener {
            clickOnConfirmArrival()

//                confirmArrivalDialog = AppCompatDialog(this, R.style.AppCompatAlertDialogStyle)
//                confirmArrivalDialog.requestWindowFeature(1)
//                confirmArrivalDialog.setContentView(R.layout.dialog_simple_alert)
//                var message = confirmArrivalDialog.findViewById(R.id.tvMessage) as TextView
//                var btnOk = confirmArrivalDialog.findViewById(R.id.btnOk) as Button
//                var btnNo = confirmArrivalDialog.findViewById(R.id.btnNo) as Button
//                (confirmArrivalDialog.findViewById(R.id.tvTitle) as TextView).text = "Confirm Arrival At"
//                var stringBuilder = StringBuilder()
//                stringBuilder.append("")
//                stringBuilder.append(CalendarUtils.getCurrentDate())
//                stringBuilder.append(" ?")
//                message.text = stringBuilder.toString()
//                btnNo.setOnClickListener { confirmArrivalDialog.dismiss() }
//                btnOk.setOnClickListener {
//                    //  this@PODActivity.actualDepatureDate.setText(this@PODActivity.actualDepartureTime)
//                    tvArrivalTime.setText("Arrival    : " + CalendarUtils.getCurrentDate())
//
//                    preferenceUtils.saveString(PreferenceUtils.CHECK_IN_ARRIVAL, R.string.checkin_arrival.toString())
//                    preferenceUtils.saveString(PreferenceUtils.CHECK_IN_ARRIVAL_TIME, "Arrival    : " + CalendarUtils.getCurrentDate())
//
//                    confirmArrivalDialog.dismiss()
//
//                }
//                confirmArrivalDialog.show()
//            }


        }

        btnStartUnLoading.setOnClickListener {
            clickOnStartLoading()
//                var id = preferenceUtils.getStringFromPreference(PreferenceUtils.CHECK_IN_START_LOADING, "")
//                var id = preferenceUtils.getStringFromPreference(PreferenceUtils.CHECK_IN_ARRIVAL, "")
//                if (id.equals(R.string.checkin_arrival.toString())) {
//                    confirmStartLoadingDialog = AppCompatDialog(this, R.style.AppCompatAlertDialogStyle)
//                    confirmStartLoadingDialog.requestWindowFeature(1)
//                    confirmStartLoadingDialog.setContentView(R.layout.dialog_simple_alert)
//                    var message = confirmStartLoadingDialog.findViewById(R.id.tvMessage) as TextView
//                    var btnOk = confirmStartLoadingDialog.findViewById(R.id.btnOk) as Button
//                    var btnNo = confirmStartLoadingDialog.findViewById(R.id.btnNo) as Button
//                    (confirmStartLoadingDialog.findViewById(R.id.tvTitle) as TextView).text = "Start Loading At"
//                    var stringBuilder = StringBuilder()
//                    stringBuilder.append("")
//                    stringBuilder.append(CalendarUtils.getCurrentDate())
//                    stringBuilder.append(" ?")
//                    message.text = stringBuilder.toString()
//                    btnNo.setOnClickListener { confirmStartLoadingDialog.dismiss() }
//                    btnOk.setOnClickListener {
//                        //  this@PODActivity.actualDepatureDate.setText(this@PODActivity.actualDepartureTime)
//                        confirmStartLoadingDialog.dismiss()
//                        tvStartUnLoadingDateTime.setText("Start Load : " + CalendarUtils.getCurrentDate())
//                        preferenceUtils.saveString(PreferenceUtils.CHECK_IN_START_LOADING, R.string.checkin_startload.toString())
//
//                        preferenceUtils.saveString(PreferenceUtils.CHECK_IN_START_LOADING_TIME, "Start Load : " + CalendarUtils.getCurrentDate())
//
//                    }
//                    confirmStartLoadingDialog.show()
//                } else {
//                    showAppCompatAlert("Alert!", "Please Complete Confirm Arrival", "OK", "", "FAILURE", false)
//
//                }
//            }
        }

        btnUnloadVanSaleStock.setOnClickListener {

            clickOnStockLoaded()
//                var id = preferenceUtils.getStringFromPreference(PreferenceUtils.CHECK_IN_ARRIVAL, "")
//                if (id.equals(R.string.checkin_arrival.toString())) {
//
//                    var id = preferenceUtils.getStringFromPreference(PreferenceUtils.CHECK_IN_START_LOADING, "")
//                    if (id.equals(R.string.checkin_startload.toString())) {
//                        var id = preferenceUtils.getStringFromPreference(PreferenceUtils.CHECK_IN_SHEDULED, "")
//
//                        if (id.equals(R.string.checkin_scheduled.toString())) {
//                            val intent = Intent(this@CheckinScreen, LoadVanSaleStockTabActivity::class.java)
//                            startActivity(intent)
//                        } else {
//                            showAppCompatAlert("Alert!", "Please Select Routing Id", "OK", "", "FAILURE", false)
//
//                        }
//                    } else {
//                        showAppCompatAlert("Alert!", "Please Complete Start Loading", "OK", "", "FAILURE", false)
//
//                    }
//                } else {
//                    showAppCompatAlert("Alert!", "Please Complete Confirm Arrival", "OK", "", "FAILURE", false)
//
//                }

//            }
        }

        btnEndUnloading.setOnClickListener {

            clickOnEndLoading()
//
//                var id = preferenceUtils.getStringFromPreference(PreferenceUtils.CHECK_IN_END_LOADING, "")
//            if (id.equals(R.string.checkin_endload.toString())) {
//                showAppCompatAlert("Alert!", "Already Completed", "OK", "", "FAILURE", false)
//
//            } else {
//                var id = preferenceUtils.getStringFromPreference(PreferenceUtils.CHECK_IN_ARRIVAL, "")
//                if (id.equals(R.string.checkin_arrival.toString())) {
//
//                    var id = preferenceUtils.getStringFromPreference(PreferenceUtils.CHECK_IN_START_LOADING, "")
//                    if (id.equals(R.string.checkin_startload.toString())) {
//                        var id = preferenceUtils.getStringFromPreference(PreferenceUtils.CHECK_IN_SHEDULED, "")
//
//                        if (id.equals(R.string.checkin_scheduled.toString())) {
//
//                            var id = preferenceUtils.getStringFromPreference(PreferenceUtils.CHECK_IN_LOADED_STOCK, "")
//
//                            if (id.equals(R.string.loaded_stock.toString())) {
//
//                                confirmEndLoadingDialog = AppCompatDialog(this, R.style.AppCompatAlertDialogStyle)
//                                confirmEndLoadingDialog.requestWindowFeature(1)
//                                confirmEndLoadingDialog.setContentView(R.layout.dialog_simple_alert)
//                                var message = confirmEndLoadingDialog.findViewById(R.id.tvMessage) as TextView
//                                var btnOk = confirmEndLoadingDialog.findViewById(R.id.btnOk) as Button
//                                var btnNo = confirmEndLoadingDialog.findViewById(R.id.btnNo) as Button
//                                (confirmEndLoadingDialog.findViewById(R.id.tvTitle) as TextView).text = "End Loading At"
//                                var stringBuilder = StringBuilder()
//                                stringBuilder.append("")
//                                stringBuilder.append(CalendarUtils.getCurrentDate())
//                                stringBuilder.append(" ?")
//                                message.text = stringBuilder.toString()
//                                btnNo.setOnClickListener { confirmEndLoadingDialog.dismiss() }
//                                btnOk.setOnClickListener {
//                                    //  this@PODActivity.actualDepatureDate.setText(this@PODActivity.actualDepartureTime)
//                                    confirmEndLoadingDialog.dismiss()
//                                    tvEndUnLoadingDateTime.setText("End Load : " + CalendarUtils.getCurrentDate())
//                                    preferenceUtils.saveString(PreferenceUtils.CHECK_IN_END_LOADING, R.string.checkin_endload.toString())
//                                    preferenceUtils.saveString(PreferenceUtils.CHECK_IN_END_LOADING_TIME, "End Load : " + CalendarUtils.getCurrentDate())
//
//                                }
//                                confirmEndLoadingDialog.show()
//                            }else{
//                                showAppCompatAlert("Alert!", "Please Confirm the Load", "OK", "", "FAILURE", false)
//
//                            }
//                        } else {
//                            showAppCompatAlert("Alert!", "Please Select Routing Id", "OK", "", "FAILURE", false)
//
//                        }
//                    } else {
//                        showAppCompatAlert("Alert!", "Please Complete Start Loading", "OK", "", "FAILURE", false)
//
//                    }
//                } else {
//                    showAppCompatAlert("Alert!", "Please Complete Confirm Arrival", "OK", "", "FAILURE", false)
//
//                }

//            }
        }

        btnCheckOut.setOnClickListener {

            clickOnCheckIn()
//
//
//            var id = preferenceUtils.getStringFromPreference(PreferenceUtils.CHECK_IN_STATUS, "")
//            if (id.equals(R.string.checkin_vehicle.toString())) {
//                showAppCompatAlert("Alert!", "Already Checked In", "OK", "", "FAILURE", false)
//
//            } else {
//                var id = preferenceUtils.getStringFromPreference(PreferenceUtils.CHECK_IN_ARRIVAL, "")
//                if (id.equals(R.string.checkin_arrival.toString())) {
//
//                    var id = preferenceUtils.getStringFromPreference(PreferenceUtils.CHECK_IN_START_LOADING, "")
//                    if (id.equals(R.string.checkin_startload.toString())) {
//                        var id = preferenceUtils.getStringFromPreference(PreferenceUtils.CHECK_IN_SHEDULED, "")
//
//                        if (id.equals(R.string.checkin_scheduled.toString())) {
//                            var id = preferenceUtils.getStringFromPreference(PreferenceUtils.CHECK_IN_END_LOADING, "")
//
//                            if (id.equals(R.string.checkin_endload.toString())) {
//                                confirmCheckinDialog = AppCompatDialog(this, R.style.AppCompatAlertDialogStyle)
//                                confirmCheckinDialog.requestWindowFeature(1)
//                                confirmCheckinDialog.setContentView(R.layout.dialog_simple_alert)
//                                var message = confirmCheckinDialog.findViewById(R.id.tvMessage) as TextView
//                                var btnOk = confirmCheckinDialog.findViewById(R.id.btnOk) as Button
//                                var btnNo = confirmCheckinDialog.findViewById(R.id.btnNo) as Button
//                                (confirmCheckinDialog.findViewById(R.id.tvTitle) as TextView).text = "Checkin At"
//                                var stringBuilder = StringBuilder()
//                                stringBuilder.append("")
//                                stringBuilder.append(CalendarUtils.getCurrentDate())
//                                stringBuilder.append(" ?")
//                                message.text = stringBuilder.toString()
//                                btnNo.setOnClickListener { confirmCheckinDialog.dismiss() }
//                                btnOk.setOnClickListener {
//
//
//                                }
//                                confirmCheckinDialog.show()
//                            } else {
//                                showAppCompatAlert("Alert!", "Please Complete End Loading", "OK", "", "FAILURE", false)
//
//                            }
//                        } else {
//                            showAppCompatAlert("Alert!", "Please Select Routing Id", "OK", "", "FAILURE", false)
//
//                        }
//                    } else {
//                        showAppCompatAlert("Alert!", "Please Complete Start Loading", "OK", "", "FAILURE", false)
//
//                    }
//                } else {
//                    showAppCompatAlert("Alert!", "Please Complete Confirm Arrival", "OK", "", "FAILURE", false)
//
//                }


//            }
        }


    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 4 && resultCode == 4) {
            vehicleCheckInDo.unLoadStock = resources.getString(R.string.unloaded_stock)
            vehicleCheckInDo.checkOutTimeCaptureStockLoadingTime=CalendarUtils.getTime()
            vehicleCheckInDo.checkOutTimeCaptureStockLoadingDate=CalendarUtils.getDate()
            btnUnloadVanSaleStock.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnUnloadVanSaleStock.setClickable(false)
            btnUnloadVanSaleStock.setEnabled(false)
            //  refreshAdapter()
            StorageManager.getInstance(this).insertCheckOutData(this, vehicleCheckInDo)
        } else if (requestCode == 5 && resultCode == 5) {
            vehicleCheckInDo.vehicleCheckOutInspectionId = resources.getString(R.string.checkin_vehicle_inspection)
            vehicleCheckInDo.vehicleCheckOutInspectionTime = CalendarUtils.getCurrentDate()
            btnVehicleInspection.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            vehicleCheckInDo.checkinTimeCaptureVehicleInspectionTime=CalendarUtils.getTime()
            vehicleCheckInDo.checkOutTimeCapturVehicleInspectionDate=CalendarUtils.getDate()
            btnVehicleInspection.setClickable(false)
            btnVehicleInspection.setEnabled(false)
            StorageManager.getInstance(this).insertCheckOutData(this, vehicleCheckInDo)
        } else if (requestCode == 6 && resultCode == 6) {
            vehicleCheckInDo.gateCheckOutInspectionId = resources.getString(R.string.checkin_gate_inspection)
            vehicleCheckInDo.gateCheckOutInspectionTime = CalendarUtils.getCurrentDate()
            vehicleCheckInDo.checkOutTimeCaptureGateInspectionTime=CalendarUtils.getTime()
            vehicleCheckInDo.checkOutTimeCapturcheckOutDate=CalendarUtils.getDate()
            btnGateInspection.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnGateInspection.setClickable(false)
            btnGateInspection.setEnabled(false)
            StorageManager.getInstance(this).insertCheckOutData(this, vehicleCheckInDo)
        }
    }

    private fun clickOnConfirmArrival() {
        if (vehicleCheckInDo.confirmCheckOutArrival.equals(resources.getString(R.string.checkin_arrival))) {
            showAppCompatAlert("Alert!", "Arrival Already Confirmed.", "OK", "", "FAILURE", false)
        } else {
            var message = "Confirm arrival at \n" + CalendarUtils.getCurrentDate() + " ?"
            showAppCompatAlert("Alert!", message, "OK", "Cancel", "ArrivalSuccess", false)
        }
    }

    private fun clickOnStartLoading() {
        if (vehicleCheckInDo.startUnLoading.equals(resources.getString(R.string.checkin_startload))) {
            showAppCompatAlert("Alert!", "Already Completed", "OK", "", "FAILURE", false)
        } else if (vehicleCheckInDo.confirmCheckOutArrival.equals("")) {
            showAppCompatAlert("Alert!", "Please Complete Confirm Arrival", "OK", "", "FAILURE", false)
        } else {
            var message = "Start unLoading at \n" + CalendarUtils.getCurrentDate() + " ?"
            showAppCompatAlert("Alert!", "" + message, "OK", "Cancel", "StartLoadSuccess", false)
        }
    }


    private fun clickOnStockLoaded() {
        if (vehicleCheckInDo.unLoadStock.equals(resources.getString(R.string.unloaded_stock))) {
            showAppCompatAlert("Alert!", "Already UnLoaded", "OK", "", "FAILURE", false)
        } else if (vehicleCheckInDo.confirmCheckOutArrival.equals("")) {
            showAppCompatAlert("Alert!", "Please Complete Confirm Arrival", "OK", "", "FAILURE", false)
        } else if (vehicleCheckInDo.startUnLoading.equals("")) {
            showAppCompatAlert("Alert!", "Please Complete Start UnLoading", "OK", "", "FAILURE", false)
        } else {
            val vehicleCheckInDo = StorageManager.getInstance(this@CheckOutScreen).getVehicleCheckInData(this@CheckOutScreen);
            val status = vehicleCheckInDo.checkInStatus;
            if (!status!!.isEmpty()) {
                val intent = Intent(this@CheckOutScreen, CurrentVanSaleStockTabActivity::class.java)
                intent.putExtra("ScreenTitle", "UnLoad Van Sales Stock");
                intent.putExtra("FLAG",2);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivityForResult(intent, 4)
            } else {
                showAppCompatAlert("Alert!", "Please Finish Check-in Process", "OK", "", "FAILURE", false)

            }
        }
    }

    private fun clickOnEndLoading() {
        if (vehicleCheckInDo.endUnLoading.equals(resources.getString(R.string.checkin_endload))) {
            showAppCompatAlert("Alert!", "Already Loaded", "OK", "", "FAILURE", false)
        } else if (vehicleCheckInDo.confirmCheckOutArrival.equals("")) {
            showAppCompatAlert("Alert!", "Please Complete Confirm Arrival.", "OK", "", "FAILURE", false)
        } else if (vehicleCheckInDo.startUnLoading.equals("")) {
            showAppCompatAlert("Alert!", "Please Complete Start UnLoading.", "OK", "", "FAILURE", false)
        } else if (vehicleCheckInDo.unLoadStock.equals("")) {
            showAppCompatAlert("Alert!", "Please Complete UnLoad Stock.", "OK", "", "FAILURE", false)
        } else {
            var message = "End UnLoading At " + CalendarUtils.getCurrentDate() + " ?"
            showAppCompatAlert("Alert!", message, "OK", "Cancel", "EndUnLoadSuccess", false)
        }
    }

    private fun clickOnVehicleInspection() {
        if (vehicleCheckInDo.vehicleCheckOutInspectionId.equals(resources.getString(R.string.checkin_vehicle_inspection))) {
            showAppCompatAlert("Alert!", "Already Completed", "OK", "", "FAILURE", false)
        } else if (vehicleCheckInDo.confirmCheckOutArrival.equals("")) {
            showAppCompatAlert("Alert!", "Please Complete Confirm Arrival.", "OK", "", "FAILURE", false)
        }
//        else if (vehicleCheckInDo.startUnLoading.equals("")) {
//            showAppCompatAlert("Alert!", "Please Complete Start UnLoading", "OK", "", "FAILURE", false)
//        }
//        else if (vehicleCheckInDo.unLoadStock.equals("")) {
//            showAppCompatAlert("Alert!", "Please Complete UnLoad Stock", "OK", "", "FAILURE", false)
//        } else if (vehicleCheckInDo.endUnLoading.equals("")) {
//            showAppCompatAlert("Alert!", "Please Complete End UnLoading", "OK", "", "FAILURE", false)
//        }
        else {
            val intent = Intent(this@CheckOutScreen, VehicleInspectionActivity::class.java)
            startActivityForResult(intent, 5)
        }
    }

    private fun clickOnGateInspection() {
        if (vehicleCheckInDo.gateCheckOutInspectionId.equals(resources.getString(R.string.checkin_gate_inspection))) {
            showAppCompatAlert("Alert!", "Already Completed", "OK", "", "FAILURE", false)
        } else if (vehicleCheckInDo.confirmCheckOutArrival.equals("")) {
            showAppCompatAlert("Alert!", "Please Complete Confirm Arrival.", "OK", "", "FAILURE", false)
        } else if (vehicleCheckInDo.startUnLoading.equals("")) {
            showAppCompatAlert("Alert!", "Please Complete Start UnLoading", "OK", "", "FAILURE", false)
        } else if (vehicleCheckInDo.endUnLoading.equals("")) {
            showAppCompatAlert("Alert!", "Please Complete End UnLoading", "OK", "", "FAILURE", false)
        } else {
            val intent = Intent(this@CheckOutScreen, GateInspectonActivity::class.java)
            startActivityForResult(intent, 6)
        }
    }

    private fun clickOnCheckIn() {
        if (vehicleCheckInDo.checkOutStatus.equals("Vehicle Checked In")) {
            showAppCompatAlert("Alert!", "Already Checked Out", "OK", "", "FAILURE", false)
        } else if (vehicleCheckInDo.confirmCheckOutArrival.equals("")) {
            showAppCompatAlert("Alert!", "Please Complete Confirm Arrival", "OK", "", "FAILURE", false)
        } else if (vehicleCheckInDo.startUnLoading.equals("")) {
            showAppCompatAlert("Alert!", "Please Complete Start UnLoading", "OK", "", "FAILURE", false)
        } else if (vehicleCheckInDo.unLoadStock.equals("")) {
            showAppCompatAlert("Alert!", "Please Coplete UnLoad Stock.", "OK", "", "FAILURE", false)
        } else if (vehicleCheckInDo.endUnLoading.equals("")) {
            showAppCompatAlert("Alert!", "Please Complete End UnLoading", "OK", "", "FAILURE", false)
        } else {
            var stringBuilder = StringBuilder()
            stringBuilder.append("CheckOut At ")
            stringBuilder.append(CalendarUtils.getCurrentDate())
            stringBuilder.append(" ?")
            showAppCompatAlert("Alert!", "" + stringBuilder.toString(), "OK", "Cancel", "CheckInSuccess", false)
        }
    }


    override fun initializeControls() {

        tvScreenTitle.setText(R.string.check_out)
        btnUnloadVanSaleStock = findViewById<Button>(R.id.btnUnloadVanSaleStock)
        btnCheckOut = findViewById<Button>(R.id.btnCheckOut)
        btnConfirmArrival = findViewById<Button>(R.id.btnConfirmArrival)
        btnStartUnLoading = findViewById<Button>(R.id.btnStartUnLoading)
        btnEndUnloading = findViewById<Button>(R.id.btnEndUnloading)
        tvArrivalTime = findViewById<TextView>(R.id.tvArrivalDateTime)
        tvStartUnLoadingDateTime = findViewById<TextView>(R.id.tvStartUnLoadingDateTime)

        tvEndUnLoadingDateTime = findViewById<TextView>(R.id.tvEndUnLoadingDateTime)
        tvCheckOutDateTime = findViewById<TextView>(R.id.tvCheckOutDateTime)
        btnVehicleInspection = findViewById<Button>(R.id.btnVehicleInspection)
        btnGateInspection = findViewById<Button>(R.id.btnGateInspection)

//        tvArrivalTime.setText("" + preferenceUtils.getStringFromPreference(PreferenceUtils.CHECK_IN_ARRIVAL_TIME, ""))
//        tvStartUnLoadingDateTime.setText("" + preferenceUtils.getStringFromPreference(PreferenceUtils.CHECK_IN_START_LOADING_TIME, ""))
//        tvEndUnLoadingDateTime.setText("" + preferenceUtils.getStringFromPreference(PreferenceUtils.CHECK_IN_END_LOADING_TIME, ""))
//        tvCheckOutDateTime.setText("" + preferenceUtils.getStringFromPreference(PreferenceUtils.CHECK_IN_STATUS_TIME, ""))


    }

    override fun onButtonYesClick(from: String) {
        super.onButtonYesClick(from)
        if (from.equals("ArrivalSuccess")) {
            tvArrivalTime.text = "Arrival    : " + CalendarUtils.getCurrentDate()
            vehicleCheckInDo.confirmCheckOutArrival = resources.getString(R.string.checkin_arrival)
            vehicleCheckInDo.arrivalCheckOutTime = CalendarUtils.getCurrentDate()
            vehicleCheckInDo.checkOutTimeCaptureArrivalTime=CalendarUtils.getTime()
            vehicleCheckInDo.checkOutTimeCaptureArrivalDate=CalendarUtils.getDate()
            btnConfirmArrival.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnConfirmArrival.setClickable(false)
            btnConfirmArrival.setEnabled(false)

            StorageManager.getInstance(this).insertCheckOutData(this, vehicleCheckInDo)
        } else if (from.equals("StartLoadSuccess")) {
            tvStartUnLoadingDateTime.text = "Start UnLoad : " + CalendarUtils.getCurrentDate()
            vehicleCheckInDo.startUnLoading = resources.getString(R.string.checkin_startload)
            vehicleCheckInDo.startUnLoadingTime = CalendarUtils.getCurrentDate()
            vehicleCheckInDo.checkOutTimeCaptureStartLoadingTime=CalendarUtils.getTime()
            vehicleCheckInDo.checkOutTimeCaptureStartLoadingDate=CalendarUtils.getDate()
            btnStartUnLoading.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnStartUnLoading.setClickable(false)
            btnStartUnLoading.setEnabled(false)
            StorageManager.getInstance(this).insertCheckOutData(this, vehicleCheckInDo)
        } else if (from.equals("EndUnLoadSuccess")) {
            vehicleCheckInDo.endUnLoading = resources.getString(R.string.checkin_endload)
            vehicleCheckInDo.endUnLoadTime = CalendarUtils.getCurrentDate()
            tvEndUnLoadingDateTime.text = "End UnLoad : " + vehicleCheckInDo.endUnLoadTime
            vehicleCheckInDo.checkOutTimeCaptureEndLoadingTime=CalendarUtils.getTime()
            vehicleCheckInDo.checkOutTimeCaptureEndLoadingDate=CalendarUtils.getDate()
            btnEndUnloading.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnEndUnloading.setClickable(false)
            btnEndUnloading.setEnabled(false)
            StorageManager.getInstance(this).insertCheckOutData(this, vehicleCheckInDo)
        } else if (from.equals("CheckInSuccess")) {
            vehicleCheckInDo.checkOutTimeCapturecheckOutTime=CalendarUtils.getTime()
            vehicleCheckInDo.checkOutTimeCapturcheckOutDate=CalendarUtils.getDate()
            checkOutTimeCapture()
            clearDataLocalData()

            val intent = Intent(this@CheckOutScreen, DashBoardActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent . FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)

        }
    }

    private fun clearDataLocalData(){
        preferenceUtils.removeFromPreference(PreferenceUtils.CHECKIN_DATE)
        preferenceUtils.removeFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID)
//        preferenceUtils.removeFromPreference(PreferenceUtils.SHIPMENT_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.CUSTOMER)
        preferenceUtils.removeFromPreference(PreferenceUtils.LATTITUDE)
        preferenceUtils.removeFromPreference(PreferenceUtils.LONGITUDE)
        preferenceUtils.removeFromPreference(PreferenceUtils.VEHICLE_CHECKED_ROUTE_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.CARRIER_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.NOTES)

        preferenceUtils.removeFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.INVOICE_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.SHIPMENT)
        preferenceUtils.removeFromPreference(PreferenceUtils.SITE_NAME)
        preferenceUtils.removeFromPreference(PreferenceUtils.VOLUME)
        preferenceUtils.removeFromPreference(PreferenceUtils.WEIGHT)
        preferenceUtils.removeFromPreference(PreferenceUtils.CV_PLATE)
        preferenceUtils.removeFromPreference(PreferenceUtils.CA_DATE)
        preferenceUtils.removeFromPreference(PreferenceUtils.CA_TIME)
        preferenceUtils.removeFromPreference(PreferenceUtils.CD_DATE)
        preferenceUtils.removeFromPreference(PreferenceUtils.CVEHICLE_CODE)
        preferenceUtils.removeFromPreference(PreferenceUtils.CD_TIME)
        preferenceUtils.removeFromPreference(PreferenceUtils.CN_SHIPMENTS)
        preferenceUtils.removeFromPreference(PreferenceUtils.CHECK_IN_STATUS)
        preferenceUtils.removeFromPreference(PreferenceUtils.TOTAL_LOAD)
        preferenceUtils.removeFromPreference(PreferenceUtils.VEHICLE_CAPACITY)
        preferenceUtils.removeFromPreference(PreferenceUtils.SELECTION)

        StorageManager.getInstance(this).deleteCheckInData(this)
        StorageManager.getInstance(this).deleteCompletedShipments(this)
        StorageManager.getInstance(this).deleteSkipShipmentList(this)
        StorageManager.getInstance(this).deleteVanNonScheduleProducts(this);
        StorageManager.getInstance(this).deleteVanScheduleProducts(this);
        StorageManager.getInstance(this).deleteSpotSalesCustomerList(this);
        StorageManager.getInstance(this).deleteVehicleInspectionList(this);
        StorageManager.getInstance(this).deleteGateInspectionList(this);
        StorageManager.getInstance(this).deleteSiteListData(this);
        StorageManager.getInstance(this).deleteShipmentListData(this)
        StorageManager.getInstance(this).deleteCheckOutData(this)
        StorageManager.getInstance(this).deleteScheduledNonScheduledReturnData();// clearing all tables

    }

    private fun checkOutTimeCapture() {
        var driverId = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "")
        var date = CalendarUtils.getDate()
        var time = CalendarUtils.getTime()

        var vehicleCheckOutDo     = StorageManager.getInstance(this).getVehicleCheckOutData(this);
        var arrivaldate           = vehicleCheckOutDo.checkOutTimeCaptureArrivalTime
        var arrivalTime           = vehicleCheckOutDo.checkOutTimeCaptureArrivalDate
        var startTime             = vehicleCheckOutDo.checkOutTimeCaptureStartLoadingTime
        var startdate             = vehicleCheckOutDo.checkOutTimeCaptureStartLoadingDate
        var stockDate             = vehicleCheckOutDo.checkOutTimeCaptureStockLoadingTime
        var stockTime             = vehicleCheckOutDo.checkOutTimeCaptureStockLoadingDate
        var endloadingtime        = vehicleCheckOutDo.checkOutTimeCaptureEndLoadingTime
        var endloadingdate        = vehicleCheckOutDo.checkOutTimeCaptureEndLoadingDate
        var vehicleinspectiontime = vehicleCheckOutDo.checkOutTimeCaptureVehicleInspectionTime
        var vehicleinspectiondate = vehicleCheckOutDo.checkOutTimeCapturVehicleInspectionDate
        var gateinspectiontime = vehicleCheckOutDo.checkOutTimeCaptureGateInspectionTime
        var gateinspectiondate = vehicleCheckOutDo.checkOutTimeCapturGateInspectionDate
        var CheckOutdate = vehicleCheckOutDo.checkOutTimeCapturcheckOutDate
        var CheckOuttime = vehicleCheckOutDo.checkOutTimeCapturecheckOutTime
        if (Util.isNetworkAvailable(this)) {
            val siteListRequest = CheckoutTimeCaptureRequest(driverId, date, arrivaldate,arrivalTime, startTime, startdate,stockDate,stockTime, endloadingtime, endloadingdate,
                    vehicleinspectiontime,vehicleinspectiondate,gateinspectiontime,gateinspectiondate,CheckOuttime,CheckOutdate,this)
            siteListRequest.setOnResultListener { isError, loginDo ->
                hideLoader()
                if (loginDo != null) {
                    if (isError) {
                        hideLoader()
//                    Toast.makeText(this@CheckOutScreen, "Failed", Toast.LENGTH_SHORT).show()
                    } else {
                        hideLoader()
                        if (loginDo.flag.equals(1)) {
//                        showToast("Success")

                        } else {
//                        showToast("Success")

                        }

                    }
                } else {
                    hideLoader()
                }

            }

            siteListRequest.execute()
        } else {
            showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

        }


    }
}
