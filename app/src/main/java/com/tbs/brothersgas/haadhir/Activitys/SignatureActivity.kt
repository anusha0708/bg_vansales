package com.tbs.brothersgas.haadhir.Activitys

import android.Manifest
import android.annotation.SuppressLint
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.*
import android.content.pm.PackageManager
import android.graphics.*
import android.location.Geocoder
import android.location.Location
import android.os.Build
import android.os.Environment
import android.os.Looper
import androidx.core.app.ActivityCompat
import android.util.AttributeSet
import android.util.Log
import android.view.*
import android.webkit.PermissionRequest
import android.widget.*
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.single.PermissionListener
import com.tbs.brothersgas.haadhir.Adapters.ScheduledProductsAdapter
import com.tbs.brothersgas.haadhir.Model.CustomerManagementDO
import com.tbs.brothersgas.haadhir.Model.PodDo
import com.tbs.brothersgas.haadhir.Model.ValidateDeliveryDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.*
import com.tbs.brothersgas.haadhir.background.*
import com.tbs.brothersgas.haadhir.common.AppConstants
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.listeners.ResultListner
import com.tbs.brothersgas.haadhir.pdfs.BulkDeliveryNotePDF
import com.tbs.brothersgas.haadhir.prints.PrinterConnection
import com.tbs.brothersgas.haadhir.prints.PrinterConstants
import com.tbs.brothersgas.haadhir.utils.CalendarUtils
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.ProgressTask
import com.tbs.brothersgas.haadhir.utils.Util
import kotlinx.android.synthetic.main.activity_digital_signature.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.text.DateFormat
import java.util.*

/**
 *Created by VenuAppasani on 22-12-2018.
 *Copyright (C) 2018 TBS - All Rights Reserved
 */
class SignatureActivity : BaseActivity(), View.OnClickListener {


    lateinit var mClear: Button
    lateinit var mGetSign: Button
    lateinit var mCancel: Button
    lateinit var file: File
    lateinit var mContent: LinearLayout
    lateinit var view: View
    lateinit var mSignature: signature
    var bitmap: Bitmap? = null
    lateinit var savedPath: String
    lateinit var encodedImage: String
    lateinit var podDo: PodDo
    lateinit var customerManagementDO: CustomerManagementDO
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var tvShipmentNumber: TextView
    lateinit var tvPaymentTerm: TextView
    lateinit var tvDeliveryRemarks: TextView
    lateinit var validateDo: ValidateDeliveryDO


    // Creating Separate Directory for saving Generated Images
    var DIRECTORY = Environment.getExternalStorageDirectory().getPath() + "/UserSignature/"
    var pic_name = /*SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())*/"signature"
    var StoredPath = DIRECTORY + pic_name + ".png"
    private var mLastUpdateTime: String? = null
    private val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 10000
    private val FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS: Long = 5000
    private val REQUEST_CHECK_SETTINGS = 100
    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private var mSettingsClient: SettingsClient? = null
    private var mLocationRequest: LocationRequest? = null
    private var mLocationSettingsRequest: LocationSettingsRequest? = null
    private var mLocationCallback: LocationCallback? = null
    private var mCurrentLocation: Location? = null
    private var mRequestingLocationUpdates: Boolean? = null

    @SuppressLint("MissingPermission")
    override fun initializeControls() {
        location()
        mContent = findViewById<View>(R.id.canvasLayout) as LinearLayout
        mSignature = signature(applicationContext, null)
        mSignature.setBackgroundColor(Color.WHITE)
        recycleview = findViewById<View>(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        recycleview.setLayoutManager(androidx.recyclerview.widget.LinearLayoutManager(this))
        // Dynamically generating Layout through java code
        mContent.addView(mSignature, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        mClear = findViewById<View>(R.id.clear) as Button
        mGetSign = findViewById<View>(R.id.getsign) as Button
        podDo = StorageManager.getInstance(this).getDepartureData(this);
        tvShipmentNumber = findViewById<View>(R.id.tvShipmentNumber) as TextView
        tvPaymentTerm = findViewById<View>(R.id.tvPayementTerm) as TextView
        tvDeliveryRemarks = findViewById<View>(R.id.tvDeliveryRemarks) as TextView




        mGetSign.setEnabled(false)
        mCancel = findViewById<View>(R.id.cancel) as Button
        view = mContent
        mGetSign.setOnClickListener(this@SignatureActivity)
        mClear.setOnClickListener(this@SignatureActivity)
        mCancel.setOnClickListener(this@SignatureActivity)

        // Method to create Directory, if the Directory doesn't exists
        file = File(DIRECTORY)
        if (!file.exists()) {
            file.mkdir()
        }
//         podDo = StorageManager.getInstance(this).getDepartureData(this);

        if (podDo.signature.equals("")) {
            tvScreenTitle.setText("Confirm & Sign Here")
        } else {
            tvScreenTitle.setText("View Sign")
            tvScreenTitle.setOnClickListener {
                val intent = Intent(this@SignatureActivity, ViewImageActivity::class.java)
                startActivity(intent)
            }
        }
        if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
            llVTank.visibility = View.GONE
            tvCMeterReading1.setText(preferenceUtils.getStringFromPreference(PreferenceUtils.METER_1, ""))
            tvCMeterReading2.setText(preferenceUtils.getStringFromPreference(PreferenceUtils.METER_2, ""))
        }
        if (Util.isNetworkAvailable(this)) {
            shipmentDetails()

        } else {
            showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

        }


    }


    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.activity_digital_signature, null) as ScrollView
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()
        disableMenuWithBackButton()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            showToast("Please save signature")
        }
        ivCustomerLocation.setVisibility(View.VISIBLE)
        ivCustomerLocation.setOnClickListener {
            if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
                mDrawerLayout.closeDrawer(Gravity.LEFT)
            } else {
                var shipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, "")
                val popup = PopupMenu(this@SignatureActivity, ivCustomerLocation)
                popup.getMenuInflater().inflate(R.menu.skipmenu, popup.getMenu())
                popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                    override fun onMenuItemClick(item: MenuItem): Boolean {
                        if (item.title == "Force Skip") {
                            skipShipment(1)

                        }
                        return true
                    }
                })
                popup.show()
            }
        }
    }

    private fun skipShipment(type: Int) {
        if (type == 1) {
            if (Util.isNetworkAvailable(this)) {
                val siteListRequest = LeaveShipmentRequest(this@SignatureActivity)
                siteListRequest.setOnResultListener { isError, loginDo ->
                    hideLoader()
                    if (loginDo != null) {
                        if (isError) {
                            hideLoader()
                            showAppCompatAlert("Alert!", resources.getString(R.string.server_error), "OK", "", "", false)
                        } else {
                            hideLoader()
                            if (loginDo.flag == 20) {
                                showToast("Delivery has skipped")
                                StorageManager.getInstance(this).deleteActiveDeliveryMainDo(this)
                                StorageManager.getInstance(this).deleteDepartureData(this)
                                StorageManager.getInstance(this).deleteCurrentSpotSalesCustomer(this)
                                StorageManager.getInstance(this).deleteCurrentDeliveryItems(this)
                                StorageManager.getInstance(this).deleteReturnCylinders(this)
                                preferenceUtils.removeFromPreference(PreferenceUtils.SHIPMENT)
                                preferenceUtils.removeFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER)
                                preferenceUtils.removeFromPreference(PreferenceUtils.DELIVERY_NOTE)
                                preferenceUtils.removeFromPreference(PreferenceUtils.DELIVERY_NOTE_RESHEDULE)
                                preferenceUtils.removeFromPreference(PreferenceUtils.PRODUCT_APPROVAL);
                                preferenceUtils.removeFromPreference(PreferenceUtils.RESCHEDULE_APPROVAL);
                                preferenceUtils.removeFromPreference(PreferenceUtils.INVOICE_ID);
                                preferenceUtils.removeFromPreference(PreferenceUtils.PAYMENT_ID);
                                preferenceUtils.saveString(PreferenceUtils.FORCE_SKIP, "SUCCESS")

                                val intent = Intent();
                                intent.putExtra("SkipShipment", "Skip")
                                setResult(11, intent)
                                finish()

                            } else {
                                showToast("Delivery not skipped")
                            }

                        }
                    } else {
                        hideLoader()
                    }

                }

                siteListRequest.execute()
            } else {
                showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "", false)

            }
        }
    }

    private fun shipmentDetails() {
        var podDo = StorageManager.getInstance(this).getDepartureData(this);
        val id = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
        val spotSaleDeliveryId = preferenceUtils.getStringFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER, "")

        if (id.length > 0) {
            val driverListRequest = ActiveDeliveryRequest(id, this@SignatureActivity)
            driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                hideLoader()
                if (isError) {

                    Toast.makeText(this@SignatureActivity, R.string.server_error, Toast.LENGTH_SHORT).show()
                } else {
                    tvShipmentNumber.visibility = View.VISIBLE
                    tvPaymentTerm.visibility = View.VISIBLE
                    tvShipmentNumber.setText("Delivery No. - " + id)
                    tvPaymentTerm.setText("Payment Term - " + activeDeliveryDo.paymentTerm)
                    if (podDo.deliveryNotes!!.isNotEmpty()) {
                        tvDeliveryRemarks.visibility = View.VISIBLE
                        tvDeliveryRemarks.setText("Delivery Remarks - " + podDo.deliveryNotes)
                    }
//                    var aMonth: String = ""
//                    var ayear: String = ""
//                    var aDate: String = ""
//                    try {
//                        var date = activeDeliveryDo.deliveryDate
//                        aMonth = date.substring(4, 6)
//                        ayear = date.substring(0, 4)
//                        aDate = date.substring(Math.max(date.length - 2, 0))
//                    } catch (e: Exception) {
//                    }
//
//                    tvReference.setText("Date : " + aDate + "-" + aMonth + "-" + ayear)

                    var loadStockAdapter = ScheduledProductsAdapter(this@SignatureActivity, activeDeliveryDo.activeDeliveryDOS, "Shipments", "", 0)
                    recycleview.setAdapter(loadStockAdapter)
                }
            }
            driverListRequest.execute()

        } else if (spotSaleDeliveryId.length > 0) {
            val driverListRequest = ActiveDeliveryRequest(spotSaleDeliveryId, this@SignatureActivity)
            driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                hideLoader()
                if (isError) {

                    Toast.makeText(this@SignatureActivity, R.string.server_error, Toast.LENGTH_SHORT).show()
                } else {

                    tvShipmentNumber.visibility = View.VISIBLE
                    tvPaymentTerm.visibility = View.VISIBLE
                    tvShipmentNumber.setText("Delivery No. - " + spotSaleDeliveryId)
                    tvPaymentTerm.setText("Payment Term - " + activeDeliveryDo.paymentTerm)
                    if (podDo.deliveryNotes!!.isNotEmpty()) {
                        tvDeliveryRemarks.visibility = View.VISIBLE
                        tvDeliveryRemarks.setText("Delivery Remarks - " + podDo.deliveryNotes)
                    }
//                    var aMonth: String = ""
//                    var ayear: String = ""
//                    var aDate: String = ""
//                    try {
//                        var date = activeDeliveryDo.deliveryDate
//                        aMonth = date.substring(4, 6)
//                        ayear = date.substring(0, 4)
//                        aDate = date.substring(Math.max(date.length - 2, 0))
//                    } catch (e: Exception) {
//                    }
//
//                    tvReference.setText("Date : " + aDate + "-" + aMonth + "-" + ayear)
//                    tvShipmentNumber.setText("Shipment : " + spotSaleDeliveryId)

                    var loadStockAdapter = ScheduledProductsAdapter(this@SignatureActivity, activeDeliveryDo.activeDeliveryDOS, "Shipments", "", 0)
                    recycleview.setAdapter(loadStockAdapter)
                }
            }
            driverListRequest.execute()

        }

    }

    override fun onClick(v: View?) {
        // TODO Auto-generated method stub
        if (v === mClear) {
            mSignature.clear()
            mGetSign.setEnabled(false)
        } else if (v === mGetSign) {

            if (!lattitudeFused.isNullOrEmpty()) {
                stopLocationUpdates()

                if (Build.VERSION.SDK_INT >= 23) {
                    if (isStoragePermissionGranted()) {

                        showAppCompatAlert("", "Are you sure you want to \n Confirm Delivery?", "OK", "Cancel", "SUCCESS", true)

                    }
                } else {

                    showAppCompatAlert("", "Are you sure you want to \n Confirm Delivery?", "OK", "Cancel", "SUCCESS", true)

                }
            } else {
                showLoader()
                stopLocationUpdates()
                locationFetch()
//                showAppCompatAlert("", resources.getString(R.string.location_capture), "OK", "", "", false)
            }


        } else if (v === mCancel) {
            showToast("Please save signature")
        }
    }

    fun isStoragePermissionGranted(): Boolean {
        if (Build.VERSION.SDK_INT >= 23) {
            if (applicationContext.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                return true
            } else {
                ActivityCompat.requestPermissions(this, arrayOf<String>(Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)
                return false
            }
        } else {
            return true
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            view.setDrawingCacheEnabled(true)
            mSignature.save(view, StoredPath)
//            Toast.makeText(applicationContext, "Successfully Saved", Toast.LENGTH_SHORT).show()
            // Calling the same class
            recreate()
        } else {
            Toast.makeText(this, "The app was not allowed to write to your storage. Hence, it cannot function properly. Please consider granting it this permission", Toast.LENGTH_LONG).show()
        }
    }

    inner class signature(context: Context, attrs: AttributeSet?) : View(context, attrs) {
        private val paint = Paint()
        private val path = Path()
        private var lastTouchX: Float = 0.toFloat()
        private var lastTouchY: Float = 0.toFloat()
        private val dirtyRect = RectF()

        init {
            paint.setAntiAlias(true)
            paint.setColor(Color.BLACK)
            paint.setStyle(Paint.Style.STROKE)
            paint.setStrokeJoin(Paint.Join.ROUND)
            paint.setStrokeWidth(STROKE_WIDTH)
        }

        fun save(v: View, StoredPath: String) {
            Log.v("log_tag", "Width: " + v.getWidth())
            Log.v("log_tag", "Height: " + v.getHeight())
            if (bitmap == null) {
                bitmap = Bitmap.createBitmap(mContent.width, mContent.height, Bitmap.Config.RGB_565)
            }
            val canvas = Canvas(bitmap!!)
            try {
                val mFileOutStream = FileOutputStream(StoredPath)
                v.draw(canvas)
                bitmap!!.compress(Bitmap.CompressFormat.PNG, 2, mFileOutStream)
                mFileOutStream.flush()
                mFileOutStream.close()
                val bos = ByteArrayOutputStream();
                bitmap!!.compress(Bitmap.CompressFormat.JPEG, 2, bos);
                val bitmapdata = bos.toByteArray();
                Log.e("Signature Size : ", "Size : " + bitmapdata)
                encodedImage = android.util.Base64.encodeToString(bitmapdata, android.util.Base64.DEFAULT)
                podDo.signatureEncode = encodedImage;
                savedPath = StoredPath
                StorageManager.getInstance(context).saveDepartureData(this@SignatureActivity, podDo)
                if (Util.isNetworkAvailable(context)) {
                    validatedelivery()
                } else {
                    showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)
                }
            } catch (e: Exception) {
                Log.v("log_tag", e.toString())
            }
        }

        fun clear() {
            path.reset()
            invalidate()
            mGetSign.setEnabled(false)
        }

        protected override fun onDraw(canvas: Canvas) {
            canvas.drawPath(path, paint)
        }

        override fun onTouchEvent(event: MotionEvent): Boolean {
            val eventX = event.x
            val eventY = event.y
            mGetSign.setEnabled(true)

            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    path.moveTo(eventX, eventY)
                    lastTouchX = eventX
                    lastTouchY = eventY
                    return true
                }

                MotionEvent.ACTION_MOVE,

                MotionEvent.ACTION_UP -> {

                    resetDirtyRect(eventX, eventY)
                    val historySize = event.historySize
                    for (i in 0 until historySize) {
                        val historicalX = event.getHistoricalX(i)
                        val historicalY = event.getHistoricalY(i)
                        expandDirtyRect(historicalX, historicalY)
                        path.lineTo(historicalX, historicalY)
                    }
                    path.lineTo(eventX, eventY)
                }

                else -> {
                    debug("Ignored touch event: " + event.toString())
                    return false
                }
            }

            invalidate((dirtyRect.left - HALF_STROKE_WIDTH).toInt(),
                    (dirtyRect.top - HALF_STROKE_WIDTH).toInt(),
                    (dirtyRect.right + HALF_STROKE_WIDTH).toInt(),
                    (dirtyRect.bottom + HALF_STROKE_WIDTH).toInt())

            lastTouchX = eventX
            lastTouchY = eventY

            return true
        }

        private fun debug(string: String) {

            Log.v("log_tag", string)

        }

        private fun expandDirtyRect(historicalX: Float, historicalY: Float) {
            if (historicalX < dirtyRect.left) {
                dirtyRect.left = historicalX
            } else if (historicalX > dirtyRect.right) {
                dirtyRect.right = historicalX
            }

            if (historicalY < dirtyRect.top) {
                dirtyRect.top = historicalY
            } else if (historicalY > dirtyRect.bottom) {
                dirtyRect.bottom = historicalY
            }
        }

        private fun resetDirtyRect(eventX: Float, eventY: Float) {
            dirtyRect.left = Math.min(lastTouchX, eventX)
            dirtyRect.right = Math.max(lastTouchX, eventX)
            dirtyRect.top = Math.min(lastTouchY, eventY)
            dirtyRect.bottom = Math.max(lastTouchY, eventY)
        }

    }

    companion object {
        private val STROKE_WIDTH = 5f
        private val HALF_STROKE_WIDTH = STROKE_WIDTH / 2
    }

    private fun validatedelivery() {
        var validateDeliveryRequest = ValidateDeliveryRequest(0.0, "", "", "", "", "", "", this@SignatureActivity);
        val shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
        val shipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, getResources().getString(R.string.checkin_non_scheduled))
        val spotDelivery = preferenceUtils.getStringFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER, "")
        val lat = preferenceUtils.getDoubleFromPreference(PreferenceUtils.LATTITUDE, 0.0)
        val lng = preferenceUtils.getDoubleFromPreference(PreferenceUtils.LONGITUDE, 0.0)
        var distance = 0.0
        if (shipmentType.equals(getResources().getString(R.string.checkin_non_scheduled), true)) {
            var customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)

            var distance = CalculationByDistance(LatLng(lattitudeFused!!.toDouble(),longitudeFused!!.toDouble()),
                    LatLng(lng,lat))
            validateDeliveryRequest = ValidateDeliveryRequest(distance, encodedImage, podDo.deliveryNotes, spotDelivery, lattitudeFused, longitudeFused, addressFused, this@SignatureActivity)
        } else {

            var distance = CalculationByDistance(LatLng(lattitudeFused!!.toDouble(),longitudeFused!!.toDouble()),
                    LatLng(lng,lat))
            validateDeliveryRequest = ValidateDeliveryRequest(distance, encodedImage, podDo.deliveryNotes, shipmentId, lattitudeFused, longitudeFused, addressFused, this@SignatureActivity)
        }
        validateDeliveryRequest.setOnResultListener { isError, validateDO ->
            if (isError) {
                hideLoader()
//                showAppCompatAlert("", "Error in Service Response, Please try again", "Ok", "", "", false)
                showAlert("Error in Service Response, Please try again...")
            } else {
                stopLocUpdates()
                validateDo = validateDO
                if (validateDo.status == 20) {

                    StorageManager.getInstance(this).saveDepartureData(this, podDo)
                    preferenceUtils.saveString(PreferenceUtils.DELIVERY_NOTE, "SUCCESS")
                    podDo.podTimeCaptureValidateDeliveryTime = CalendarUtils.getTime()
                    podDo.podTimeCaptureValidateDeliveryDate = CalendarUtils.getDate()
                    if (validateDO.delEmailFlag == 20 || validateDO.delPrintFlag == 20) {
                        prepareDeliveryNoteCreation()
                    } else if (validateDO.cylEmailFlag == 20 || validateDO.cylPrintFlag == 20) {
                        prepareCylinderIssueNoteCreation()
                    } else if (validateDO.invoiceNumber != null && validateDO.invoiceNumber.length > 0) {
                        preferenceUtils.saveString(PreferenceUtils.INVOICE_ID, validateDO.invoiceNumber)
                        preferenceUtils.saveString(PreferenceUtils.INVOICE_AMOUNT, validateDO.amount)
                        preferenceUtils.saveString(PreferenceUtils.INVOICE_SHIPMENT_ID, validateDO.shipmentId)
                        preferenceUtils.saveString(PreferenceUtils.PREVIEW_INVOICE_ID, validateDO.invoiceNumber)

                        prepareInvoiceCreation()
                    } else {
                        finishActivity()
                    }

                } else {
                    if (validateDo.message.length > 0) {

                        if (validateDO.message.equals("You are away from the actual customer location, Please contact BG support team")) {

                            preferenceUtils.saveString(PreferenceUtils.SKIP_ERROR, "SUCCESS")
                            showAppCompatAlert("Info!", "You are away from the actual customer location, Please contact BG support team"
                                    , "OK", "   ", "FINISH", true)

                        } else {
                            showToast("" + validateDo.message)
                        }

                    } else {
                        showToast("Transaction failed, Please contact support team and try again")
                    }
                    hideLoader()
                }
            }
        }
        validateDeliveryRequest.execute()


    }

    private fun finishActivity() {
        hideLoader()
        hideLoader()
        hideLoader()
        hideLoader()
        hideLoader()
        hideLoader()
        hideLoader()
        if (validateDo.message.length > 0) {

            showAppCompatAlert("Info!", validateDo.message, "OK", "   ", "SAVE", true)

        }

    }


    private fun prepareInvoiceCreation() {
        if (Util.isNetworkAvailable(this)) {
            val siteListRequest = InvoiceDetailsRequest(validateDo.invoiceNumber, this@SignatureActivity)
            siteListRequest.setOnResultListener { isError, createPDFInvoiceDO ->
                hideLoader()
                if (createPDFInvoiceDO != null) {
                    if (isError) {
                        finishActivity()
                    } else {
                        if (validateDo.invEmailFlag == 20) {
                            captureInfo(createPDFInvoiceDO.invoiceEmail, ResultListner { `object`, isSuccess ->
                                if (isSuccess) {
                                    val intent = Intent(this@SignatureActivity, InvoiceEmailService::class.java)
                                    var gson = Gson()
                                    var invDO = gson.toJson(createPDFInvoiceDO)
                                    intent.putExtra("object", invDO);
                                    startService(intent);
                                    if (validateDo.invPrintFlag == 20) {
                                        printDocument(createPDFInvoiceDO, PrinterConstants.PrintInvoiceReport);
                                    }
                                    finishActivity()
                                }
                            })
                        } else if (validateDo.invPrintFlag == 20) {
                            printDocument(createPDFInvoiceDO, PrinterConstants.PrintInvoiceReport);
                            finishActivity()

                        } else {
                            finishActivity()

                        }

                    }
                } else {
                    finishActivity()
                }
            }
            siteListRequest.execute()
        } else {
            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "", false)
        }

    }


    private fun prepareDeliveryNoteCreation() {
        val driverListRequest = ActiveDeliveryRequest(validateDo.shipmentId, this@SignatureActivity)
        driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
            if (isError) {
                if (validateDo.cylEmailFlag == 20 || validateDo.cylPrintFlag == 20) {
                    prepareCylinderIssueNoteCreation()
                } else if (validateDo.invoiceNumber != null && validateDo.invoiceNumber.length > 0) {
                    prepareInvoiceCreation()
                    preferenceUtils.saveString(PreferenceUtils.INVOICE_ID, validateDo.invoiceNumber)
                    preferenceUtils.saveString(PreferenceUtils.INVOICE_AMOUNT, validateDo.amount)
                    preferenceUtils.saveString(PreferenceUtils.INVOICE_SHIPMENT_ID, validateDo.shipmentId)
                    preferenceUtils.saveString(PreferenceUtils.PREVIEW_INVOICE_ID, validateDo.invoiceNumber)
                } else {
                    finishActivity()
                }
            } else {
                if (activeDeliveryDo != null) {
                    if (validateDo.delEmailFlag == 20) {
                        if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").
                                equals(AppConstants.MeterReadingProduct)) {
                            captureInfo(activeDeliveryDo.deliveryEmail, ResultListner { `object`, isSuccess ->
                                if (isSuccess) {
                                    val intent = Intent(this@SignatureActivity, BulkEmailService::class.java)
                                    var gson = Gson()
                                    var activeDeDO = gson.toJson(activeDeliveryDo)
                                    intent.putExtra("object", activeDeDO);

                                    startService(intent);
                                    if (validateDo.delPrintFlag == 20) {
                                        if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
                                            printDocument(activeDeliveryDo, PrinterConstants.PrintBulkDeliveryNotes);
                                        } else {


                                            printDocument(activeDeliveryDo, PrinterConstants.PrintCylinderDeliveryNotes);

                                        }
                                    }
                                    if (validateDo.cylEmailFlag == 20 || validateDo.cylPrintFlag == 20) {
                                        prepareCylinderIssueNoteCreation()
                                    } else if (validateDo.invoiceNumber != null && validateDo.invoiceNumber.length > 0) {
                                        preferenceUtils.saveString(PreferenceUtils.INVOICE_ID, validateDo.invoiceNumber)
                                        preferenceUtils.saveString(PreferenceUtils.INVOICE_AMOUNT, validateDo.amount)
                                        preferenceUtils.saveString(PreferenceUtils.INVOICE_SHIPMENT_ID, validateDo.shipmentId)
                                        preferenceUtils.saveString(PreferenceUtils.PREVIEW_INVOICE_ID, validateDo.invoiceNumber)
                                        prepareInvoiceCreation()
                                    } else {
                                        finishActivity()
                                    }
                                }
                            })
//                            val intent = Intent(this@SignatureActivity, BulkEmailService::class.java)
//                            var gson = Gson()
//                            var activeDeDO = gson.toJson(activeDeliveryDo)
//                            intent.putExtra("object", activeDeDO);
//                            startService(intent);
                        } else {
                            captureInfo(activeDeliveryDo.deliveryEmail, ResultListner { `object`, isSuccess ->
                                if (isSuccess) {
                                    val intent = Intent(this@SignatureActivity, EmailService::class.java)
                                    var gson = Gson()
                                    var activeDeDO = gson.toJson(activeDeliveryDo)
                                    intent.putExtra("object", activeDeDO);
                                    startService(intent);

                                    if (validateDo.delPrintFlag == 20) {
                                        if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
                                            printDocument(activeDeliveryDo, PrinterConstants.PrintBulkDeliveryNotes);
                                        } else {


                                            printDocument(activeDeliveryDo, PrinterConstants.PrintCylinderDeliveryNotes);

                                        }
                                    }
                                    if (validateDo.cylEmailFlag == 20 || validateDo.cylPrintFlag == 20) {
                                        prepareCylinderIssueNoteCreation()
                                    } else if (validateDo.invoiceNumber != null && validateDo.invoiceNumber.length > 0) {
                                        preferenceUtils.saveString(PreferenceUtils.INVOICE_ID, validateDo.invoiceNumber)
                                        preferenceUtils.saveString(PreferenceUtils.INVOICE_AMOUNT, validateDo.amount)
                                        preferenceUtils.saveString(PreferenceUtils.INVOICE_SHIPMENT_ID, validateDo.shipmentId)
                                        preferenceUtils.saveString(PreferenceUtils.PREVIEW_INVOICE_ID, validateDo.invoiceNumber)
                                        prepareInvoiceCreation()
                                    } else {
                                        finishActivity()
                                    }

                                }
                                else{
                                    if (validateDo.delPrintFlag == 20) {
                                        if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
                                            printDocument(activeDeliveryDo, PrinterConstants.PrintBulkDeliveryNotes);
                                        } else {


                                            printDocument(activeDeliveryDo, PrinterConstants.PrintCylinderDeliveryNotes);

                                        }
                                    }
                                    if (validateDo.cylEmailFlag == 20 || validateDo.cylPrintFlag == 20) {
                                        prepareCylinderIssueNoteCreation()
                                    } else if (validateDo.invoiceNumber != null && validateDo.invoiceNumber.length > 0) {
                                        prepareInvoiceCreation()
                                        preferenceUtils.saveString(PreferenceUtils.INVOICE_ID, validateDo.invoiceNumber)
                                        preferenceUtils.saveString(PreferenceUtils.INVOICE_AMOUNT, validateDo.amount)
                                        preferenceUtils.saveString(PreferenceUtils.INVOICE_SHIPMENT_ID, validateDo.shipmentId)
                                        preferenceUtils.saveString(PreferenceUtils.PREVIEW_INVOICE_ID, validateDo.invoiceNumber)
                                    } else {
                                        finishActivity()
                                    }
                                }
                            })

//                            CYLDeliveryNotePDF(this).createDeliveryPDF(activeDeliveryDo, "Email").CreateDeliveryPDF().execute()
                        }



                    }
                    else if (validateDo.delPrintFlag == 20) {
                        if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
                            printDocument(activeDeliveryDo, PrinterConstants.PrintBulkDeliveryNotes);
                        } else {


                            printDocument(activeDeliveryDo, PrinterConstants.PrintCylinderDeliveryNotes);

                        }
                        if (validateDo.cylEmailFlag == 20 || validateDo.cylPrintFlag == 20) {
                            prepareCylinderIssueNoteCreation()
                        } else if (validateDo.invoiceNumber != null && validateDo.invoiceNumber.length > 0) {
                            preferenceUtils.saveString(PreferenceUtils.INVOICE_ID, validateDo.invoiceNumber)
                            preferenceUtils.saveString(PreferenceUtils.INVOICE_AMOUNT, validateDo.amount)
                            preferenceUtils.saveString(PreferenceUtils.INVOICE_SHIPMENT_ID, validateDo.shipmentId)
                            preferenceUtils.saveString(PreferenceUtils.PREVIEW_INVOICE_ID, validateDo.invoiceNumber)
                            prepareInvoiceCreation()
                        } else {
                            finishActivity()
                        }
                    } else if (validateDo.cylEmailFlag == 20 || validateDo.cylPrintFlag == 20) {
                        prepareCylinderIssueNoteCreation()
                    } else if (validateDo.invoiceNumber != null && validateDo.invoiceNumber.length > 0) {
                        prepareInvoiceCreation()
                        preferenceUtils.saveString(PreferenceUtils.INVOICE_ID, validateDo.invoiceNumber)
                        preferenceUtils.saveString(PreferenceUtils.INVOICE_AMOUNT, validateDo.amount)
                        preferenceUtils.saveString(PreferenceUtils.INVOICE_SHIPMENT_ID, validateDo.shipmentId)
                        preferenceUtils.saveString(PreferenceUtils.PREVIEW_INVOICE_ID, validateDo.invoiceNumber)
                    } else {
                        finishActivity()
                    }
                } else {
                    if (validateDo.cylEmailFlag == 20 || validateDo.cylPrintFlag == 20) {
                        prepareCylinderIssueNoteCreation()
                    } else if (validateDo.invoiceNumber != null && validateDo.invoiceNumber.length > 0) {
                        prepareInvoiceCreation()
                        preferenceUtils.saveString(PreferenceUtils.INVOICE_ID, validateDo.invoiceNumber)
                        preferenceUtils.saveString(PreferenceUtils.INVOICE_AMOUNT, validateDo.amount)
                        preferenceUtils.saveString(PreferenceUtils.INVOICE_SHIPMENT_ID, validateDo.shipmentId)
                        preferenceUtils.saveString(PreferenceUtils.PREVIEW_INVOICE_ID, validateDo.invoiceNumber)
                    } else {
                        finishActivity()
                    }
                }
            }
        }
        driverListRequest.execute()
    }

    private fun printDocument(obj: Any, from: Int) {
        showLoader()
//        ProgressTask.getInstance().showProgress(this@SignatureActivity,false,"Printing...")
        val printConnection = PrinterConnection.getInstance(this@SignatureActivity)
        if (printConnection.isBluetoothEnabled()) {
            printConnection.connectToPrinter(obj, from)
        } else {
            showToast("Please enable your mobile Bluetooth.")
            hideLoader()
        }
    }

    private fun pairedDevices() {
        val pairedDevices = BluetoothAdapter.getDefaultAdapter().getBondedDevices()
        if (pairedDevices.size > 0) {
            for (device in pairedDevices) {
                val deviceName = device.getName()
                val mac = device.getAddress() // MAC address
                StorageManager.getInstance(this).savePrinterMac(this, mac);
                Log.e("Bluetooth", "Name : " + deviceName + " Mac : " + mac)
                break
            }
        }
    }

    override fun onResume() {
        super.onResume()
        pairedDevices()
    }

    override fun onStart() {
        super.onStart()
        val filter = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        registerReceiver(mReceiver, filter)
    }

    override fun onDestroy() {
        unregisterReceiver(mReceiver)
        hideLoader()
        super.onDestroy()
    }

    private val mReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            if (BluetoothDevice.ACTION_FOUND == action) {
                val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE);
                PrinterConstants.bluetoothDevices.add(device)
                Log.e("Bluetooth", "Discovered => name : " + device!!.name + ", Mac : " + device.address)
            } else if (action.equals(BluetoothDevice.ACTION_ACL_CONNECTED)) {
                Log.e("Bluetooth", "status : ACTION_ACL_CONNECTED")
                pairedDevices()
            } else if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                val state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
                if (state == BluetoothAdapter.STATE_OFF) {
                    Log.e("Bluetooth", "status : STATE_OFF")
                } else if (state == BluetoothAdapter.STATE_TURNING_OFF) {
                    Log.e("Bluetooth", "status : STATE_TURNING_OFF")
                } else if (state == BluetoothAdapter.STATE_ON) {
                    Log.e("Bluetooth", "status : STATE_ON")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_TURNING_ON) {
                    Log.e("Bluetooth", "status : STATE_TURNING_ON")
                } else if (state == BluetoothAdapter.STATE_CONNECTING) {
                    Log.e("Bluetooth", "status : STATE_CONNECTING")
                } else if (state == BluetoothAdapter.STATE_CONNECTED) {
                    Log.e("Bluetooth", "status : STATE_CONNECTED")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_DISCONNECTED) {
                    Log.e("Bluetooth", "status : STATE_DISCONNECTED")
                }
            }
        }
    }

    private fun prepareCylinderIssueNoteCreation() {
        val driverListRequest = CylinderIssueRequest(validateDo.shipmentId, this@SignatureActivity)
        val onResultListener = OnResultListener()
        driverListRequest.setOnResultListener(onResultListener)
        driverListRequest.execute()
    }

    private fun OnResultListener(): OnResultListener {
        return OnResultListener { isError, cylinderIssueMainDO ->
            if (cylinderIssueMainDO != null) {
                if (isError) {
                    if (validateDo.invoiceNumber != null && validateDo.invoiceNumber.length > 0) {
                        prepareInvoiceCreation()
                        preferenceUtils.saveString(PreferenceUtils.INVOICE_ID, validateDo.invoiceNumber)
                        preferenceUtils.saveString(PreferenceUtils.INVOICE_AMOUNT, validateDo.amount)
                        preferenceUtils.saveString(PreferenceUtils.INVOICE_SHIPMENT_ID, validateDo.shipmentId)
                        preferenceUtils.saveString(PreferenceUtils.PREVIEW_INVOICE_ID, validateDo.invoiceNumber)
                    } else {
                        finishActivity()
                    }
                } else {
                    if (validateDo.cylEmailFlag == 20) {
                        captureInfo(cylinderIssueMainDO.email, ResultListner { `object`, isSuccess ->
                            if (isSuccess) {
                                val intent = Intent(this@SignatureActivity, CylinderEmailService::class.java)
                                var gson = Gson()
                                var cylDO = gson.toJson(cylinderIssueMainDO)
                                intent.putExtra("object", cylDO);
                                startService(intent);
                                if (validateDo.cylPrintFlag == 20) {
                                    printDocument(cylinderIssueMainDO, PrinterConstants.PrintCylinderIssue)
                                }
                                if (validateDo.invoiceNumber != null && validateDo.invoiceNumber.length > 0) {
                                    prepareInvoiceCreation()
                                    preferenceUtils.saveString(PreferenceUtils.INVOICE_ID, validateDo.invoiceNumber)
                                    preferenceUtils.saveString(PreferenceUtils.INVOICE_AMOUNT, validateDo.amount)
                                    preferenceUtils.saveString(PreferenceUtils.INVOICE_SHIPMENT_ID, validateDo.shipmentId)
                                    preferenceUtils.saveString(PreferenceUtils.PREVIEW_INVOICE_ID, validateDo.invoiceNumber)
                                } else {
                                    finishActivity()
                                }
                            }
                        })

                    }

                    else if (validateDo.cylPrintFlag == 20) {
                        printDocument(cylinderIssueMainDO, PrinterConstants.PrintCylinderIssue)
                        if (validateDo.invoiceNumber != null && validateDo.invoiceNumber.length > 0) {
                            prepareInvoiceCreation()
                            preferenceUtils.saveString(PreferenceUtils.INVOICE_ID, validateDo.invoiceNumber)
                            preferenceUtils.saveString(PreferenceUtils.INVOICE_AMOUNT, validateDo.amount)
                            preferenceUtils.saveString(PreferenceUtils.INVOICE_SHIPMENT_ID, validateDo.shipmentId)
                            preferenceUtils.saveString(PreferenceUtils.PREVIEW_INVOICE_ID, validateDo.invoiceNumber)
                        } else {
                            finishActivity()
                        }
                    }
                    else if (validateDo.invoiceNumber != null && validateDo.invoiceNumber.length > 0) {
                        prepareInvoiceCreation()
                        preferenceUtils.saveString(PreferenceUtils.INVOICE_ID, validateDo.invoiceNumber)
                        preferenceUtils.saveString(PreferenceUtils.INVOICE_AMOUNT, validateDo.amount)
                        preferenceUtils.saveString(PreferenceUtils.INVOICE_SHIPMENT_ID, validateDo.shipmentId)
                        preferenceUtils.saveString(PreferenceUtils.PREVIEW_INVOICE_ID, validateDo.invoiceNumber)
                    } else {
                        finishActivity()
                    }
                }
            } else {
                if (validateDo.invoiceNumber != null && validateDo.invoiceNumber.length > 0) {
                    prepareInvoiceCreation()
                    preferenceUtils.saveString(PreferenceUtils.INVOICE_ID, validateDo.invoiceNumber)
                    preferenceUtils.saveString(PreferenceUtils.INVOICE_AMOUNT, validateDo.amount)
                    preferenceUtils.saveString(PreferenceUtils.INVOICE_SHIPMENT_ID, validateDo.shipmentId)
                    preferenceUtils.saveString(PreferenceUtils.PREVIEW_INVOICE_ID, validateDo.invoiceNumber)
                } else {
                    finishActivity()

                }
            }
        }

    }

    override fun onBackPressed() {
        showToast("Please save signature")
    }

    override fun onButtonYesClick(from: String) {

        if ("FINISH".equals(from, ignoreCase = true)) {

            val intent = Intent()
            intent.putExtra("SIGNATURE", StoredPath)
            intent.putExtra("SignatureEncode", encodedImage)
            setResult(150, intent)
            finish()
        } else if ("SUCCESS".equals(from, ignoreCase = true)) {
            view.setDrawingCacheEnabled(true)
            mSignature.save(view, StoredPath)
        } else if ("SAVE".equals(from, ignoreCase = true)) {
            val intent = Intent()
            intent.putExtra("SIGNATURE", StoredPath)
            intent.putExtra("SignatureEncode", encodedImage)
            setResult(11, intent)
            finish()
        } else {

        }
    }

    override fun onButtonNoClick(from: String) {
        if ("SUCCESS".equals(from, ignoreCase = true)) {

        } else {
            val intent = Intent()
            intent.putExtra("SIGNATURE", StoredPath)
            intent.putExtra("SignatureEncode", encodedImage)
            setResult(11, intent)
            finish()
        }
    }


    fun locationFetch() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mSettingsClient = LocationServices.getSettingsClient(this)

        mLocationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                super.onLocationResult(locationResult)
                // location is received
                mCurrentLocation = locationResult!!.lastLocation
                mLastUpdateTime = DateFormat.getTimeInstance().format(Date())

                updateLocUI()
            }
        }

        mRequestingLocationUpdates = false

        mLocationRequest = LocationRequest()
        mLocationRequest!!.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS)
        mLocationRequest!!.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS)
        mLocationRequest!!.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)

        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(mLocationRequest!!)
        mLocationSettingsRequest = builder.build()

        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(object : PermissionListener {
                    override fun onPermissionRationaleShouldBeShown(permission: com.karumi.dexter.listener.PermissionRequest?, token: PermissionToken?) {
                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }

                    override fun onPermissionGranted(response: PermissionGrantedResponse) {
                        mRequestingLocationUpdates = true
                        startLocUpdates()
                    }

                    override fun onPermissionDenied(response: PermissionDeniedResponse) {
                        if (response.isPermanentlyDenied()) {

                            showToast("Location Permission denied")
                            // open device settings when the permission is
                            // denied permanently
//                            openSettings()
                        }
                    }

                    fun onPermissionRationaleShouldBeShown(permission: PermissionRequest, token: PermissionToken) {
                        token.continuePermissionRequest()
                    }
                }).check()
    }

    fun updateLocUI() {
        if (mCurrentLocation != null) {
            stopLocUpdates()
            hideLoader()

            lattitudeFused = mCurrentLocation!!.getLatitude().toString()
            longitudeFused = mCurrentLocation!!.getLongitude().toString()
            var gcd = Geocoder(getBaseContext(), Locale.getDefault());

            try {
                var addresses = gcd.getFromLocation(mCurrentLocation!!.getLatitude(),
                        mCurrentLocation!!.getLongitude(), 1);
                if (addresses.size > 0) {
                    System.out.println(addresses.get(0).getLocality());
                    var cityName = addresses.get(0).getLocality();
                    addressFused = cityName
                }
            } catch (e: java.lang.Exception) {
                e.printStackTrace();
            }
            showAppCompatAlert("", "Are You Sure You Want to \n Confirm Delivery?", "OK", "Cancel", "SUCCESS", true)


        }

    }

    @SuppressLint("MissingPermission")
    private fun startLocUpdates() {
        mSettingsClient!!
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this) {
                    //                    Log.i(TAG, "All location settings are satisfied.")

//                    Toast.makeText(applicationContext, "Started location updates!", Toast.LENGTH_SHORT).show()


                    mFusedLocationClient!!.requestLocationUpdates(mLocationRequest,
                            mLocationCallback, Looper.myLooper())

                    updateLocUI()
                }
                .addOnFailureListener(this) { e ->
                    val statusCode = (e as ApiException).statusCode
                    when (statusCode) {
                        LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
//                            Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " + "location settings ")
                            try {
                                // Show the dialog by calling startResolutionForResult(), and check the
                                // result in onActivityResult().
                                val rae = e as ResolvableApiException
                                rae.startResolutionForResult(this@SignatureActivity, REQUEST_CHECK_SETTINGS)
                            } catch (sie: IntentSender.SendIntentException) {
//                                Log.i(TAG, "PendingIntent unable to execute request.")
                            }

                        }
                        LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                            val errorMessage = "Location settings are inadequate, and cannot be " + "fixed here. Fix in Settings."
//                            Log.e(TAG, errorMessage)

                            Toast.makeText(this@SignatureActivity, errorMessage, Toast.LENGTH_LONG).show()
                        }
                    }

                    updateLocUI()
                }
    }

    fun stopLocUpdates() {
        // Removing location updates
//        try {
//            mFusedLocationClient!!
//                    .removeLocationUpdates(mLocationCallback)
//                    .addOnCompleteListener(this) {
//
//                    }
//        } catch (e: java.lang.Exception) {
//
//        }
        if (mFusedLocationClient != null && mLocationCallback != null) {
            mFusedLocationClient!!
                    .removeLocationUpdates(mLocationCallback)
                    .addOnCompleteListener(this) {

                    }
        }

    }
}