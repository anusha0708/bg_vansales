package com.tbs.brothersgas.haadhir.Activitys

import android.app.Dialog
import android.content.Intent
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.text.Html
import android.util.DisplayMetrics
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import android.widget.*
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.tbs.brothersgas.haadhir.Adapters.AssignedVrAdapter
import com.tbs.brothersgas.haadhir.Adapters.NonAssignedVRAdapter
import com.tbs.brothersgas.haadhir.Model.DriverDO
import com.tbs.brothersgas.haadhir.Model.DriverIdMainDO
import com.tbs.brothersgas.haadhir.Model.VehicleCheckInDo
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.CheckinTimeCaptureRequest
import com.tbs.brothersgas.haadhir.Requests.DriverIdRequest
import com.tbs.brothersgas.haadhir.Requests.NonScheduledDriverIdRequest
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.listeners.StringListner
import com.tbs.brothersgas.haadhir.utils.CalendarUtils
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import java.util.*


class CheckinScreen : BaseActivity() {
    private var llAboutUs: ScrollView? = null


    lateinit var btnConfirmArrival: Button
    lateinit var btnStartLoading: Button
    lateinit var btnEndLoading: Button
    lateinit var btnVehicleInspection: Button
    lateinit var btnGateInspection: Button
    lateinit var llCheckIn: LinearLayout
    lateinit var llConfirmStock: LinearLayout

    lateinit var tvScheduledRoutingId: TextView
    lateinit var tvNonScheduledRoutingId: TextView
    lateinit var tvArrivalTime: TextView
    lateinit var tvStartLoadingTime: TextView
    lateinit var tvEndLoadingTime: TextView
    lateinit var tvNotes: TextView

    lateinit var tvNotesLabel: TextView

    private var scheduledRouteList: DriverIdMainDO = DriverIdMainDO()
    private var nonScheduledRouteList: DriverIdMainDO = DriverIdMainDO()
    lateinit var tvCheckinDateTime: TextView
    lateinit var vehicleCheckInDo: VehicleCheckInDo


    override fun initialize() {
        llAboutUs = layoutInflater.inflate(R.layout.checkin_screen, null) as ScrollView
        llBody.addView(llAboutUs, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        disableMenuWithBackButton()
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            finish()
//            overridePendingTransition(R.anim.exit, R.anim.enter)
        }
        initializeControls()

        vehicleCheckInDo = StorageManager.getInstance(this).getVehicleCheckInData(this)
        if (vehicleCheckInDo != null) {
//            if (!vehicleCheckInDo.checkInStatus.equals(resources.getString(R.string.checkin_vehicle))) {
//                loadScheduledRouteList()
//            } else {
//                loadScheduledRouteList()
//            }

            if (!vehicleCheckInDo.arrivalTime.equals("")) {
                tvArrivalTime.text = "Arrival : " + vehicleCheckInDo.arrivalTime
                btnConfirmArrival.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnConfirmArrival.setClickable(false)
                btnConfirmArrival.setEnabled(false);
            }
            if (!vehicleCheckInDo.startLoadingTime.equals("")) {
                tvStartLoadingTime.text = "Start Load : " + vehicleCheckInDo.startLoadingTime
                btnStartLoading.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnStartLoading.setClickable(false)
                btnStartLoading.setEnabled(false);

            }
            if (!vehicleCheckInDo.loadStock.equals("")) {
                llConfirmStock.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                llConfirmStock.setClickable(false)
                llConfirmStock.setEnabled(false)
                tvScheduledRoutingId.setClickable(false)
                tvScheduledRoutingId.setEnabled(false)
                tvNonScheduledRoutingId.setClickable(false)
                tvNonScheduledRoutingId.setEnabled(false)
            }
            if (!vehicleCheckInDo.endLoadTime.equals("")) {
                tvEndLoadingTime.text = "End Load : " + vehicleCheckInDo.endLoadTime
                btnEndLoading.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnEndLoading.setClickable(false)
                btnEndLoading.setEnabled(false)
                tvScheduledRoutingId.setClickable(false)
                tvScheduledRoutingId.setEnabled(false)
                tvNonScheduledRoutingId.setClickable(false)
                tvNonScheduledRoutingId.setEnabled(false)
            }
            if (!vehicleCheckInDo.vehicleInspectionId.equals("")) {
                btnVehicleInspection.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnVehicleInspection.setClickable(false)
                btnVehicleInspection.setEnabled(false)
            }
            if (!vehicleCheckInDo.gateInspectionId.equals("")) {
                btnGateInspection.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnGateInspection.setClickable(false)
                btnGateInspection.setEnabled(false)
            }
            if (vehicleCheckInDo.scheduledRootId.equals("")) {
                tvScheduledRoutingId.text = "Schedule Routing Id"
            } else {
                tvScheduledRoutingId.text = "" + vehicleCheckInDo.scheduledRootId
            }
            if (vehicleCheckInDo.nonScheduledRootId.equals("")) {
                tvNonScheduledRoutingId.text = "Non Schedule Routing Id"
            } else {
                tvNonScheduledRoutingId.text = "" + vehicleCheckInDo.nonScheduledRootId
            }
            var notes = vehicleCheckInDo.notes
            if (vehicleCheckInDo.notes!!.length > 0) {
                tvNotes.text = "" + notes
            }
            if (!vehicleCheckInDo.checkInTime.equals("")) {
                llCheckIn.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                llCheckIn.setClickable(false)
                llCheckIn.setEnabled(false)
                tvScheduledRoutingId.setClickable(false)
                tvScheduledRoutingId.setEnabled(false)
                tvNonScheduledRoutingId.setClickable(false)
                tvNonScheduledRoutingId.setEnabled(false)
                tvCheckinDateTime.text = "Check In : " + vehicleCheckInDo.checkInTime
            }
            loadScheduledRouteList()

        } else {
            vehicleCheckInDo = VehicleCheckInDo()
        }

        btnGateInspection.setOnClickListener {
            clickOnGateInspection()
        }

        btnVehicleInspection.setOnClickListener {
            clickOnVehicleInspection()
        }

        btnConfirmArrival.setOnClickListener {
            clickOnConfirmArrival()
        }

        btnStartLoading.setOnClickListener {
            clickOnStartLoading()
        }

        llConfirmStock.setOnClickListener {

            var id = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "")
            var nID = preferenceUtils.getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, "")

            if (id.isEmpty() && nID.isEmpty()) {
                showAlert("Stock Not Assigned to Driver")
            } else {

                clickOnStockLoaded()
            }
        }

        btnEndLoading.setOnClickListener {
            clickOnEndLoading()
        }

        llCheckIn.setOnClickListener {
            clickOnCheckIn()
        }

        if (scheduledRouteList != null && scheduledRouteList.driverDOS.size > 0 && scheduledRouteList.driverDOS.get(0) != null && !scheduledRouteList.driverDOS.get(0).vehicleRouteId.equals("")) {
            updateScheduleRootId(scheduledRouteList.driverDOS.get(0).vehicleRouteId)
        }
        tvScheduledRoutingId.setOnClickListener {
            clickToSelectRoutId(resources.getString(R.string.checkin_scheduled), scheduledRouteList)
//            showRouteDetails(scheduledRouteList.driverDOS.get(0))
        }

        tvNonScheduledRoutingId.setOnClickListener {
            clickToSelectRoutId(resources.getString(R.string.checkin_non_scheduled), nonScheduledRouteList)
        }


    }

    fun updateScheduleRootId(vehicleRouteId: String) {
        vehicleCheckInDo.scheduledRootId = vehicleRouteId
        tvScheduledRoutingId.text = "" + vehicleRouteId
        StorageManager.getInstance(this).insertCheckInData(this, vehicleCheckInDo)
    }

    fun updateNonScheduleRootId(vehicleRouteId: String) {
        vehicleCheckInDo.nonScheduledRootId = vehicleRouteId
        tvNonScheduledRoutingId.text = "" + vehicleRouteId
        StorageManager.getInstance(this).insertCheckInData(this, vehicleCheckInDo)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 4 && resultCode == 4) {
            vehicleCheckInDo.loadStock = resources.getString(R.string.loaded_stock)
            llConfirmStock.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            llConfirmStock.setClickable(false)
            llConfirmStock.setEnabled(false)
            tvScheduledRoutingId.setClickable(false)
            tvScheduledRoutingId.setEnabled(false)
            tvNonScheduledRoutingId.setClickable(false)
            tvNonScheduledRoutingId.setEnabled(false)
            tvStatus.setText(vehicleCheckInDo.loadStock)
            StorageManager.getInstance(this).insertCheckInData(this, vehicleCheckInDo)
            refreshMenuAdapter()
        } else if (requestCode == 3 && resultCode == 3) {
            vehicleCheckInDo.vehicleInspectionId = resources.getString(R.string.checkin_vehicle_inspection)
            vehicleCheckInDo.vehicleInspectionTime = CalendarUtils.getCurrentDate()
            vehicleCheckInDo.checkinTimeCaptureVehicleInspectionTime = CalendarUtils.getTime()
            vehicleCheckInDo.checkinTimeCaptureVehicleInspectionDate = CalendarUtils.getDate()
            btnVehicleInspection.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnVehicleInspection.setClickable(false)
            btnVehicleInspection.setEnabled(false)
            StorageManager.getInstance(this).insertCheckInData(this, vehicleCheckInDo)
        } else if (requestCode == 6 && resultCode == 6) {
            vehicleCheckInDo.gateInspectionId = resources.getString(R.string.checkin_gate_inspection)
            vehicleCheckInDo.gateInspectionTime = CalendarUtils.getCurrentDate()
            vehicleCheckInDo.checkinTimeCaptureGateInspectionTime = CalendarUtils.getTime()
            vehicleCheckInDo.checkinTimeCaptureGateInspectionDate = CalendarUtils.getDate()
            btnGateInspection.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnGateInspection.setClickable(false)
            btnGateInspection.setEnabled(false)
            StorageManager.getInstance(this).insertCheckInData(this, vehicleCheckInDo)
        } else if (requestCode == 4 && resultCode == 10) {
//            btnLoadedStock.setBackgroundColor(resources.getColor(R.color.md_gray_light))
//            btnLoadedStock.setClickable(false)
//            btnLoadedStock.setEnabled(false)
            tvScheduledRoutingId.setClickable(false)
            tvScheduledRoutingId.setEnabled(false)
            tvScheduledRoutingId.setText("")
            tvScheduledRoutingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.refresh_button, 0);

        }
    }

    private fun clickOnConfirmArrival() =
            if (vehicleCheckInDo.confirmArrival.equals(resources.getString(R.string.checkin_arrival))) {
                showAppCompatAlert("", "Arrival Already Confirmed.", "OK", "", "FAILURE", false)
            } else {
                /*CalendarUtils.getCurrentDateFromFireBase(this, { value ->
                    var message = "Confirm arrival at \n" + value + " ?"
                    showAppCompatAlert("", message, "OK", "Cancel", "ArrivalSuccess", false)
                });*/
                var message = "Confirm arrival at \n" + CalendarUtils.getCurrentDate() + " ?"
                showAppCompatAlert("", message, "OK", "Cancel", "ArrivalSuccess", false)

            }

    private fun clickOnStartLoading() {
        if (vehicleCheckInDo.startLoading.equals(resources.getString(R.string.checkin_startload))) {
            showAppCompatAlert("", "Already Completed", "OK", "", "FAILURE", false)
        } else if (vehicleCheckInDo.confirmArrival.equals("")) {
            showAppCompatAlert("", "Please Complete Confirm Arrival", "OK", "", "FAILURE", false)
        } else {
            var message = "Start loading at \n" + CalendarUtils.getCurrentDate() + " ?"
            showAppCompatAlert("", "" + message, "OK", "Cancel", "StartLoadSuccess", false)
        }
    }

    private fun selectRouteIds(from: String, routeList: DriverIdMainDO) {
        val scheduleddialog = Dialog(this, R.style.NewDialog)
        scheduleddialog.setCancelable(true)
        scheduleddialog.setCanceledOnTouchOutside(true)
        scheduleddialog.setContentView(R.layout.assigned_route_list)
        val window = scheduleddialog.window
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)
        var ivBackC = scheduleddialog.findViewById(R.id.ivBackC) as ImageView
        ivBackC.setOnClickListener {
            scheduleddialog.dismiss()
        }
        val recyclerView = scheduleddialog!!.findViewById(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        recyclerView.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
        val recyclerViewEmpty = scheduleddialog.findViewById(R.id.recycleviewEmpty) as androidx.recyclerview.widget.RecyclerView
        recyclerViewEmpty.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
        var driverId = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "")
        recyclerView.isClickable = false
        recyclerViewEmpty.isClickable = false
        if (routeList != null) {
            var notes = preferenceUtils.getStringFromPreference(PreferenceUtils.NOTES, "")

            preferenceUtils.saveString(PreferenceUtils.DRIVER_NAME, routeList.driverName)
            if (notes.length > 0) {
                try {
                    var s = notes

                    s = s.substring(s.indexOf("$") + 1)
                    s = s.substring(0, s.indexOf("$"))

//                    val notess1 = notes.substring(Math.max(notes.length - 33, 33))
//                    val notess2 = notess1.substring(0, notess1.length - 10)
                    tvNotes.text = "" + s
                } catch (e: Exception) {
                }
                vehicleCheckInDo.notes = tvNotes.text.toString().trim()
                StorageManager.getInstance(this).insertCheckInData(this, vehicleCheckInDo);
            }
            if (routeList != null && routeList.driverDOS != null && routeList.driverDOS.size > 0) {
                val driverAdapter = AssignedVrAdapter(this, routeList.driverDOS, scheduleddialog, from)
                recyclerView.adapter = driverAdapter
                if (routeList != null && routeList.driverEmptyDOS != null && routeList.driverEmptyDOS.size > 0) {
                    val adapter = NonAssignedVRAdapter(this, routeList.driverEmptyDOS, scheduleddialog)
                    recyclerViewEmpty.adapter = adapter
                }
            }
        }
        scheduleddialog.show()
    }

    private fun loadScheduledRouteList() {
        var id = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "")
        if (Util.isNetworkAvailable(this)) {
            val driverListRequest = DriverIdRequest(id, this)
            showLoader()
            driverListRequest.setOnResultListener { isError, driverIdMainDO ->
                if (isError) {
                    loadNonScheduledRouteList()
                    // Toast.makeText(this, "Failed to get route list, please try again.", Toast.LENGTH_SHORT).show()
                } else {
                    scheduledRouteList = driverIdMainDO
                    scheduleData()
                }
            }
            driverListRequest.execute()
        } else {
            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "", false)
        }
    }

    private fun loadNonScheduledRouteList() {
        var id = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "")
        if (Util.isNetworkAvailable(this)) {

            val driverListRequest = NonScheduledDriverIdRequest(id, this)

            driverListRequest.setOnResultListener { isError, driverIdMainDO ->
                if (isError) {
                    hideLoader()
                } else {
                    nonScheduledRouteList = driverIdMainDO
                    nonScheduleData()
                }
            }
            driverListRequest.execute()
        } else {
            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "", false)

        }

    }

    private fun clickToSelectRoutId(from: String, routeList: DriverIdMainDO) {
        if (vehicleCheckInDo.confirmArrival.equals(resources.getString(R.string.checkin_arrival))) {
            if (vehicleCheckInDo.startLoading.equals(resources.getString(R.string.checkin_startload))) {
                if (routeList != null && routeList.driverDOS.size > 0) {
                    selectRouteIds(from, routeList)
                } else {
                    showAppCompatAlert("", "Failed to get route list, please try again.", "Retry", "Cancel", from, false)
                }
            } else {
                showAppCompatAlert("", "Please Complete Start Loading", "OK", "", "FAILURE", false)
            }
        } else {
            showAppCompatAlert("", "Please Complete Confirm Arrival", "OK", "", "FAILURE", false)
        }
    }

    private fun showRouteDetails(driverDo: DriverDO) {
        val shipmentDetailsDialog = Dialog(this)
        shipmentDetailsDialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        shipmentDetailsDialog.setContentView(R.layout.route_details_dialog_layout)
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        val width = displayMetrics.widthPixels
        val height = displayMetrics.heightPixels
        var name = preferenceUtils.getStringFromPreference(PreferenceUtils.LOGIN_DRIVER_NAME, "")

        shipmentDetailsDialog.window!!.setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT)
        shipmentDetailsDialog.setCancelable(true)
        shipmentDetailsDialog.setCanceledOnTouchOutside(true)
        val tvVehicleRouteId = shipmentDetailsDialog.findViewById<TextView>(R.id.tvVehicleRouteId)
        val tvVehicleCode = shipmentDetailsDialog.findViewById<TextView>(R.id.tvVehicleCode)
        val tvSite = shipmentDetailsDialog.findViewById<TextView>(R.id.tvSite)
        val tvNoOfShipments = shipmentDetailsDialog.findViewById<TextView>(R.id.tvNoOfShipments)
        val tvDriverName = shipmentDetailsDialog.findViewById<TextView>(R.id.tvDriverName)
        val tvPlate = shipmentDetailsDialog.findViewById<TextView>(R.id.tvPlate)
        val tvTripNumber = shipmentDetailsDialog.findViewById<TextView>(R.id.tvTripNumber)
        val tvNotes = shipmentDetailsDialog.findViewById<TextView>(R.id.tvNotes)
        val tvClose = shipmentDetailsDialog.findViewById<TextView>(R.id.tvClose)
        tvVehicleRouteId.text = "" + driverDo.vehicleRouteId
        tvVehicleCode.text = "" + driverDo.vehicleCode
        tvSite.text = "" + driverDo.site
        tvNoOfShipments.text = "" + driverDo.nShipments
        tvDriverName.text = "" + name
        tvPlate.text = "" + driverDo.plate
        tvTripNumber.text = "" + driverDo.tripNumber
        if (driverDo.notes.length > 0) {
            tvNotes.setVisibility(View.VISIBLE)
            tvNotesLabel.setVisibility(View.VISIBLE)

            try {
//                val notess1 = driverDo.notes.substring(Math.max(driverDo.notes.length - 33, 33))
//                val notess2 = notess1.substring(0, notess1.length - 10)
////                Html.toHtml(driverDo.notes[])

                var s = driverDo.notes

                s = s.substring(s.indexOf("$") + 1)
                s = s.substring(0, s.indexOf("$"))

//                    val notess1 = notes.substring(Math.max(notes.length - 33, 33))
//                    val notess2 = notess1.substring(0, notess1.length - 10)
                tvNotes.text = "" + s
//                tvNotes.text = "" + notess2
            } catch (e: Exception) {

            }
            vehicleCheckInDo.notes = tvNotes.text.toString().trim()
            StorageManager.getInstance(this).insertCheckInData(this, vehicleCheckInDo);
        } else {
            tvNotes.setVisibility(View.GONE)
            tvNotesLabel.setVisibility(View.GONE)
        }


        tvClose.setOnClickListener { shipmentDetailsDialog.dismiss() }

        shipmentDetailsDialog.show()
    }

    private fun clickOnStockLoaded() {
        if (vehicleCheckInDo.loadStock.equals(resources.getString(R.string.loaded_stock))) {
            showAppCompatAlert("", "Already Loaded", "OK", "", "FAILURE", false)
        }
//        else if (vehicleCheckInDo.confirmArrival.equals("")) {
//            showAppCompatAlert("", "Please Complete Confirm Arrival", "OK", "", "FAILURE", false)
//        }
//        else if (vehicleCheckInDo.startLoading.equals("")) {
//            showAppCompatAlert("", "Please Complete Start Loading", "OK", "", "FAILURE", false)
//        }
//        else if (vehicleCheckInDo.scheduledRootId.equals("")) {
//            showAppCompatAlert("", "Please Select Routing Id", "OK", "", "FAILURE", false)
//        }
        else {
            val intent = Intent(this@CheckinScreen, NewLoadVanSaleStockTabActivity::class.java)
            intent.putExtra("ScreenTitle", "Confirm Stock")
            startActivityForResult(intent, 4)
            overridePendingTransition(R.anim.enter, R.anim.exit)

        }
    }

    private fun clickOnEndLoading() {
        if (vehicleCheckInDo.endLoading.equals(resources.getString(R.string.checkin_endload))) {
            showAppCompatAlert("", "Already Loaded", "OK", "", "FAILURE", false)
        } else if (vehicleCheckInDo.confirmArrival.equals("")) {
            showAppCompatAlert("", "Please Complete Confirm Arrival.", "OK", "", "FAILURE", false)
        } else if (vehicleCheckInDo.startLoading.equals("")) {
            showAppCompatAlert("", "Please Complete Start Loading.", "OK", "", "FAILURE", false)
        } else if (vehicleCheckInDo.scheduledRootId.equals("")) {
            showAppCompatAlert("", "Please Select Routing Id", "OK", "", "FAILURE", false)
        } else if (vehicleCheckInDo.loadStock.equals("")) {
            showAppCompatAlert("", "Please Complete Loaded Stock.", "OK", "", "FAILURE", false)
        } else {
            var message = "End loading at \n" + CalendarUtils.getCurrentDate() + " ?"
            showAppCompatAlert("", message, "OK", "Cancel", "EndLoadSuccess", false)
        }
    }

    private fun clickOnVehicleInspection() {
        if (vehicleCheckInDo.vehicleInspectionId.equals(resources.getString(R.string.checkin_vehicle_inspection))) {
            showAppCompatAlert("", "Already Completed", "OK", "", "FAILURE", false)
        }
//        else if (vehicleCheckInDo.confirmArrival.equals("")) {
//            showAppCompatAlert("", "Please Complete Confirm Arrival.", "OK", "", "FAILURE", false)
//        }
//        else if (vehicleCheckInDo.startLoading.equals("")) {
//            showAppCompatAlert("", "Please Complete Start Loading", "OK", "", "FAILURE", false)
//        }
//        else if (vehicleCheckInDo.scheduledRootId.equals("")) {
//            showAppCompatAlert("", "Please Select Routing Id", "OK", "", "FAILURE", false)
//        } else if (vehicleCheckInDo.loadStock.equals("")) {
//            showAppCompatAlert("", "Please Complete Load Stock", "OK", "", "FAILURE", false)
//        } else if (vehicleCheckInDo.endLoading.equals("")) {
//            showAppCompatAlert("", "Please Complete End Loading", "OK", "", "FAILURE", false)
//        }
        else {
            val intent = Intent(this@CheckinScreen, VehicleInspectionActivity::class.java)
            startActivityForResult(intent, 3)
            overridePendingTransition(R.anim.enter, R.anim.exit)
        }
    }

    private fun clickOnGateInspection() {
        if (vehicleCheckInDo.gateInspectionId.equals(resources.getString(R.string.checkin_gate_inspection))) {
            showAppCompatAlert("", "Already Completed", "OK", "", "FAILURE", false)
        }
//        else if (vehicleCheckInDo.confirmArrival.equals("")) {
//            showAppCompatAlert("", "Please Complete Confirm Arrival.", "OK", "", "FAILURE", false)
//        }
//        else if (vehicleCheckInDo.startLoading.equals("")) {
//            showAppCompatAlert("", "Please Complete Start Loading", "OK", "", "FAILURE", false)
//        }
//        else if (vehicleCheckInDo.scheduledRootId.equals("")) {
//            showAppCompatAlert("", "Please Select Routing Id", "OK", "", "FAILURE", false)
//        } else if (vehicleCheckInDo.endLoading.equals("")) {
//            showAppCompatAlert("", "Please Complete End Loading", "OK", "", "FAILURE", false)
//        }
        else {
            val intent = Intent(this@CheckinScreen, GateInspectonActivity::class.java)
            startActivityForResult(intent, 6)
            overridePendingTransition(R.anim.enter, R.anim.exit)
        }
    }

    private fun clickOnCheckIn() {
        if (vehicleCheckInDo.checkInStatus.equals(resources.getString(R.string.checkin_vehicle))) {
            showAppCompatAlert("", "Already Checked In", "OK", "", "FAILURE", false)
        }
//        else if (vehicleCheckInDo.confirmArrival.equals("")) {
//            showAppCompatAlert("", "Please Complete Confirm Arrival", "OK", "", "FAILURE", false)
//        }
//        else if (vehicleCheckInDo.startLoading.equals("")) {
//            showAppCompatAlert("", "Please Complete Start Loading", "OK", "", "FAILURE", false)
//        }
        else if (vehicleCheckInDo.loadStock.equals("")) {
            showAppCompatAlert("", "Please Complete Load Stock", "OK", "", "FAILURE", false)
        }
//        else if (vehicleCheckInDo.scheduledRootId.equals("")) {
//            showAppCompatAlert("", "Please Select Root Id.", "OK", "", "FAILURE", false)
//        }
//        else if (vehicleCheckInDo.endLoading.equals("")) {
//            showAppCompatAlert("", "Please Complete End Loading", "OK", "", "FAILURE", false)
//        }
        else {
            var stringBuilder = StringBuilder()
            stringBuilder.append("Check in at ")
            stringBuilder.append(CalendarUtils.getCurrentDate())
            stringBuilder.append(" ?")
            showAppCompatAlert("", "" + stringBuilder.toString(), "OK", "Cancel", "CheckInSuccess", false)
        }
    }

    override fun initializeControls() {

        tvScreenTitle.setText(R.string.check_in)
        llConfirmStock = findViewById<LinearLayout>(R.id.llConfirmStock)
        llCheckIn = findViewById<LinearLayout>(R.id.llCheckIn)
        btnConfirmArrival = findViewById<Button>(R.id.btnConfirmArrival)
        btnStartLoading = findViewById<Button>(R.id.btnStartLoading)
        btnEndLoading = findViewById<Button>(R.id.btnEndLoading)
        tvArrivalTime = findViewById<TextView>(R.id.tvArrivalDateTime)
        tvStartLoadingTime = findViewById<TextView>(R.id.tvStartLoadingDateTime)
        tvScheduledRoutingId = findViewById<TextView>(R.id.tvScheduledVR)
        tvNonScheduledRoutingId = findViewById<TextView>(R.id.tvNonScheduledVR)
        tvEndLoadingTime = findViewById<TextView>(R.id.tvEndLoadingDateTime)
        tvNotes = findViewById<TextView>(R.id.tvNotes)
        tvNotesLabel = findViewById<TextView>(R.id.tvNotesLabel)
        tvCheckinDateTime = findViewById<TextView>(R.id.tvCheckinDateTime)
        btnVehicleInspection = findViewById<Button>(R.id.btnVehicleInspection)
        btnGateInspection = findViewById<Button>(R.id.btnGateInspection)

//        tvArrivalTime.setText("" + preferenceUtils.getStringFromPreference(PreferenceUtils.CHECK_IN_ARRIVAL_TIME, ""))
//        tvStartLoadingTime.setText("" + preferenceUtils.getStringFromPreference(PreferenceUtils.CHECK_IN_START_LOADING_TIME, ""))
//        tvEndLoadingTime.setText("" + preferenceUtils.getStringFromPreference(PreferenceUtils.CHECK_IN_END_LOADING_TIME, ""))
//        tvCheckinDateTime.setText("" + preferenceUtils.getStringFromPreference(PreferenceUtils.CHECK_IN_STATUS_TIME, ""))


    }

    override fun onButtonYesClick(from: String) {
        super.onButtonYesClick(from)
        if (from.equals("ArrivalSuccess")) {
            tvArrivalTime.text = "Arrival    : " + CalendarUtils.getCurrentDate()
            vehicleCheckInDo.confirmArrival = resources.getString(R.string.checkin_arrival)
            vehicleCheckInDo.checkinTimeCaptureArrivalTime = CalendarUtils.getTime()
            vehicleCheckInDo.checkinTimeCaptureArrivalDate = CalendarUtils.getDate()

            vehicleCheckInDo.arrivalTime = CalendarUtils.getCurrentDate()
            btnConfirmArrival.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnConfirmArrival.setClickable(false)
            btnConfirmArrival.setEnabled(false)

            StorageManager.getInstance(this).insertCheckInData(this, vehicleCheckInDo)
        } else if (from.equals("StartLoadSuccess")) {
            tvStartLoadingTime.text = "Start Load : " + CalendarUtils.getCurrentDate()
            vehicleCheckInDo.startLoading = resources.getString(R.string.checkin_startload)
            vehicleCheckInDo.startLoadingTime = CalendarUtils.getCurrentDate()
            vehicleCheckInDo.checkinTimeCaptureStartLoadingTime = CalendarUtils.getTime()
            vehicleCheckInDo.checkinTimeCaptureStartLoadingDate = CalendarUtils.getDate()
            btnStartLoading.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnStartLoading.setClickable(false)
            btnStartLoading.setEnabled(false)
            StorageManager.getInstance(this).insertCheckInData(this, vehicleCheckInDo)
        } else if (from.equals("EndLoadSuccess")) {
            vehicleCheckInDo.endLoading = resources.getString(R.string.checkin_endload)
            vehicleCheckInDo.endLoadTime = CalendarUtils.getCurrentDate()
            tvEndLoadingTime.text = "End Load : " + vehicleCheckInDo.endLoadTime
            vehicleCheckInDo.checkinTimeCaptureEndLoadingTime = CalendarUtils.getTime()
            vehicleCheckInDo.checkinTimeCaptureEndLoadingDate = CalendarUtils.getDate()
            btnEndLoading.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnEndLoading.setClickable(false)
            btnEndLoading.setEnabled(false)
            tvScheduledRoutingId.setClickable(false)
            tvScheduledRoutingId.setEnabled(false)
            tvNonScheduledRoutingId.setClickable(false)
            tvNonScheduledRoutingId.setEnabled(false)
            StorageManager.getInstance(this).insertCheckInData(this, vehicleCheckInDo)
        } else if (from.equals(resources.getString(R.string.checkin_scheduled))) {
            loadScheduledRouteList()
        } else if (from.equals(resources.getString(R.string.checkin_non_scheduled))) {
            loadNonScheduledRouteList()
        } else if (from.equals("CheckInSuccess")) {

            vehicleCheckInDo.checkInStatus = resources.getString(R.string.checkin_vehicle);//resources.getString(R.string.checkin_vehicle)
            vehicleCheckInDo.checkInTime = CalendarUtils.getCurrentDate()
            vehicleCheckInDo.notes = tvNotes.text.toString()
            tvCheckinDateTime.text = "Check In : " + CalendarUtils.getCurrentDate()
            vehicleCheckInDo.checkinTimeCaptureCheckinTime = CalendarUtils.getTime()
            vehicleCheckInDo.checkinTimeCapturCheckinDate = CalendarUtils.getDate()
            llCheckIn.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            llCheckIn.setClickable(false)
            llCheckIn.setEnabled(false)
            var routeId = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "")


//            var vehicleCheckinRequest = VehicleCheckinRequest(driver, routeId, carrierId, "" + 1, "" + 0, "" + 0, this@CheckinScreen)
//            vehicleCheckinRequest.setOnResultListener { isError, vehicleCheckMainDO ->
//                hideLoader()
//                if (isError) {
//                    Toast.makeText(this@CheckinScreen, resources.getString(R.string.error_checkin), Toast.LENGTH_SHORT).show()
//                }
//                else {
//
//
//                }
//            }
//            vehicleCheckinRequest.execute()
            preferenceUtils.saveString(PreferenceUtils.VEHICLE_CHECKED_ROUTE_ID, routeId)
//            var c = Calendar.getInstance()
//            var mYear = c.get(Calendar.YEAR)
//            var mMonth = c.get(Calendar.MONTH)
//            var mDay = c.get(Calendar.DAY_OF_MONTH)
//            if (Integer.parseInt(c.get(Calendar.MINUTE).toString()) < 10) {
//
//                var minute = "0" + c.get(Calendar.MINUTE).toString()
//                preferenceUtils.saveString(PreferenceUtils.CHECKIN_DATE, StringBuilder().append(mDay).append("-").append(mMonth + 1).append("-").append(mYear).append
//                ("   " + c.get(Calendar.HOUR_OF_DAY) + ":" + minute).toString())
//            } else {
//                preferenceUtils.saveString(PreferenceUtils.CHECKIN_DATE, StringBuilder().append(mDay).append("-").append(mMonth + 1).append("-").append(mYear).append
//                ("   " + c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE)).toString())
//            }
            var idd = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "")
            preferenceUtils.saveString(PreferenceUtils.CHECKIN_DATE, CalendarUtils.getCurrentDate())

            preferenceUtils.saveString(PreferenceUtils.BASE_VEHICLE_ROUTE_ID, idd)

            var vCode = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_CODE, "")
            var aTime = preferenceUtils.getStringFromPreference(PreferenceUtils.A_TIME, "")
            var aDate = preferenceUtils.getStringFromPreference(PreferenceUtils.A_DATE, "")
            var dDate = preferenceUtils.getStringFromPreference(PreferenceUtils.D_DATE, "")
            var dTime = preferenceUtils.getStringFromPreference(PreferenceUtils.D_TIME, "")
            var vPlate = preferenceUtils.getStringFromPreference(PreferenceUtils.V_PLATE, "")
            var nShipments = preferenceUtils.getStringFromPreference(PreferenceUtils.N_SHIPMENTS, "")
            preferenceUtils.saveString(PreferenceUtils.CVEHICLE_CODE, vCode)
            preferenceUtils.saveString(PreferenceUtils.CA_TIME, aTime)
            preferenceUtils.saveString(PreferenceUtils.CA_DATE, aDate)
            preferenceUtils.saveString(PreferenceUtils.CD_DATE, dDate)
            preferenceUtils.saveString(PreferenceUtils.CD_TIME, dTime)
            preferenceUtils.saveString(PreferenceUtils.CV_PLATE, vPlate)
            preferenceUtils.saveString(PreferenceUtils.CN_SHIPMENTS, nShipments)
            preferenceUtils.saveString(PreferenceUtils.STATUS, "" + resources.getString(R.string.checkin_vehicle))
            preferenceUtils.saveString(PreferenceUtils.CHECK_IN_STATUS, resources.getString(R.string.checkin_vehicle))

//                    populateExpandableList();
            StorageManager.getInstance(this).insertCheckInData(this, vehicleCheckInDo)
            if (Util.isNetworkAvailable(this)) {
                checkinTimeCapture(idd)

            } else {
                showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

            }

            val intent = Intent(this@CheckinScreen, DashBoardActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            overridePendingTransition(R.anim.enter, R.anim.exit)
            //refreshMenuAdapter()
        }

    }

    private fun checkinTimeCapture(id: String) {
        var driverId = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "")
        var date = CalendarUtils.getDate()
        var time = CalendarUtils.getTime()
//        08-01-2019   10:30


        var vehicleCheckInDo = StorageManager.getInstance(this).getVehicleCheckInData(this);
        var arrivaldate = vehicleCheckInDo.checkinTimeCaptureArrivalDate
        var arrivalTime = vehicleCheckInDo.checkinTimeCaptureArrivalTime
        var startTime = vehicleCheckInDo.checkinTimeCaptureStartLoadingTime
        var startdate = vehicleCheckInDo.checkinTimeCaptureStartLoadingDate
        var endloadingtime = vehicleCheckInDo.checkinTimeCaptureEndLoadingTime
        var endloadingdate = vehicleCheckInDo.checkinTimeCaptureEndLoadingDate
        var vehicleinspectiontime = vehicleCheckInDo.checkinTimeCaptureVehicleInspectionTime
        var vehicleinspectiondate = vehicleCheckInDo.checkinTimeCaptureVehicleInspectionDate
        var gateinspectiontime = vehicleCheckInDo.checkinTimeCaptureGateInspectionTime
        var gateinspectiondate = vehicleCheckInDo.checkinTimeCaptureGateInspectionDate
        var checkindate = vehicleCheckInDo.checkinTimeCapturCheckinDate
        var checkintime = vehicleCheckInDo.checkinTimeCaptureCheckinTime

        val siteListRequest = CheckinTimeCaptureRequest(driverId, date, id, arrivaldate, arrivalTime, startTime, startdate, endloadingtime, endloadingdate,
                vehicleinspectiontime, vehicleinspectiondate, gateinspectiontime, gateinspectiondate, checkintime, checkindate, this)
        siteListRequest.setOnResultListener { isError, loginDo ->
            hideLoader()
            if (loginDo != null) {
                if (isError) {
                    hideLoader()
//                    Toast.makeText(this@CheckinScreen, "Failed", Toast.LENGTH_SHORT).show()
                } else {
                    hideLoader()
                    if (loginDo.flag.equals(1)) {
//                        showToast("Success")

                    } else {
//                        showToast("Success")

                    }

                }
            } else {
                hideLoader()
            }

        }

        siteListRequest.execute()
    }

    fun scheduleData() {
        if (scheduledRouteList != null && scheduledRouteList.driverDOS.size > 0 && scheduledRouteList.driverDOS.get(0) != null && !scheduledRouteList.driverDOS.get(0).vehicleRouteId.equals("")) {
            updateScheduleRootId(scheduledRouteList.driverDOS.get(0).vehicleRouteId)
            val vehicleCheckInDo = StorageManager.getInstance(this).getVehicleCheckInData(this)
            preferenceUtils.saveString(PreferenceUtils.DOC_NUMBER, scheduledRouteList.driverDOS.get(0).srcNumber)
            preferenceUtils.saveString(PreferenceUtils.VEHICLE_ROUTE_ID, scheduledRouteList.driverDOS.get(0).vehicleRouteId)
            preferenceUtils.saveString(PreferenceUtils.VEHICLE_CODE, scheduledRouteList.driverDOS.get(0).vehicleCode)
            preferenceUtils.saveString(PreferenceUtils.CARRIER_ID, scheduledRouteList.driverDOS.get(0).vehicleCarrier)
            preferenceUtils.saveString(PreferenceUtils.LOCATION, scheduledRouteList.driverDOS.get(0).location)
            preferenceUtils.saveString(PreferenceUtils.VR_ID, scheduledRouteList.driverDOS.get(0).vehicleRouteId)
            preferenceUtils.saveString(PreferenceUtils.V_PLATE, scheduledRouteList.driverDOS.get(0).plate)
            preferenceUtils.saveString(PreferenceUtils.N_SHIPMENTS, scheduledRouteList.driverDOS.get(0).nShipments)
            preferenceUtils.saveInt(PreferenceUtils.UNIT, scheduledRouteList.driverDOS.get(0).unit)
            preferenceUtils.saveString(PreferenceUtils.CV_PLATE, scheduledRouteList.driverDOS.get(0).plate)

            if (scheduledRouteList.driverDOS.get(0).aTime.length > 0) {
                val arrivalTime = scheduledRouteList.driverDOS.get(0).aTime.substring(Math.max(scheduledRouteList.driverDOS.get(0).aTime.length - 2, 0))
                val departureTime = scheduledRouteList.driverDOS.get(0).dTime.substring(Math.max(scheduledRouteList.driverDOS.get(0).dTime.length - 2, 0))
                val arrivalTime2 = scheduledRouteList.driverDOS.get(0).aTime.substring(0, 2)
                val departureTime2 = scheduledRouteList.driverDOS.get(0).dTime.substring(0, 2)
                preferenceUtils.saveString(PreferenceUtils.A_TIME, arrivalTime2 + ":" + arrivalTime)
                preferenceUtils.saveString(PreferenceUtils.D_TIME, departureTime2 + ":" + departureTime)
                val aMonth = scheduledRouteList.driverDOS.get(0).aDate.substring(4, 6)
                val ayear = scheduledRouteList.driverDOS.get(0).aDate.substring(0, 4)
                val aDate = scheduledRouteList.driverDOS.get(0).aDate.substring(Math.max(scheduledRouteList.driverDOS.get(0).aDate.length - 2, 0))
                val dMonth = scheduledRouteList.driverDOS.get(0).dDate.substring(4, 6)
                val dyear = scheduledRouteList.driverDOS.get(0).dDate.substring(0, 4)
                val dDate = scheduledRouteList.driverDOS.get(0).dDate.substring(Math.max(scheduledRouteList.driverDOS.get(0).dDate.length - 2, 0))
                preferenceUtils.saveString(PreferenceUtils.A_DATE, aDate + "-" + aMonth + "-" + ayear)
                preferenceUtils.saveString(PreferenceUtils.D_DATE, dDate + "-" + dMonth + "-" + dyear)
            }
            if (scheduledRouteList.driverDOS.get(0).notes.length > 0) {
                tvNotes.setVisibility(View.VISIBLE)
                tvNotesLabel.setVisibility(View.VISIBLE)
                try {
                    var s = scheduledRouteList.driverDOS.get(0).notes
                    s = s.substring(s.indexOf("$") + 1)
                    s = s.substring(0, s.indexOf("$"))
                    tvNotes.text = "" + s
                } catch (e: Exception) {
                }
                vehicleCheckInDo.notes = tvNotes.text.toString().trim()
                StorageManager.getInstance(this).insertCheckInData(this, vehicleCheckInDo);
            } else {
                tvNotes.setVisibility(View.GONE)
                tvNotesLabel.setVisibility(View.GONE)
            }
            StorageManager.getInstance(this).insertCheckInData(this, vehicleCheckInDo);

        }
        loadNonScheduledRouteList()

    }

    fun nonScheduleData() {
        if (nonScheduledRouteList != null && nonScheduledRouteList.driverDOS.size > 0) {
            preferenceUtils.saveString(PreferenceUtils.Non_Scheduled_Route_Id, nonScheduledRouteList.driverDOS.get(0).vehicleRouteId)
            vehicleCheckInDo.nonScheduledRootId = nonScheduledRouteList.driverDOS.get(0).vehicleRouteId
            val vehicleCheckInDo = StorageManager.getInstance(this).getVehicleCheckInData(this)
            preferenceUtils.saveString(PreferenceUtils.NON_VEHICLE_CODE, nonScheduledRouteList.driverDOS.get(0).vehicleCode)
            preferenceUtils.saveString(PreferenceUtils.LOCATION, nonScheduledRouteList.driverDOS.get(0).location)
            preferenceUtils.saveString(PreferenceUtils.DOC_NUMBER, nonScheduledRouteList.driverDOS.get(0).vehicleRouteId)
            StorageManager.getInstance(this).insertCheckInData(this, vehicleCheckInDo)
            preferenceUtils.saveString(PreferenceUtils.NOTES, nonScheduledRouteList.driverDOS.get(0).notes)
            preferenceUtils.saveInt(PreferenceUtils.UNIT, nonScheduledRouteList.driverDOS.get(0).unit)
            preferenceUtils.saveString(PreferenceUtils.CV_PLATE, nonScheduledRouteList.driverDOS.get(0).plate)
            preferenceUtils.saveString(PreferenceUtils.VEHICLE_CODE, nonScheduledRouteList.driverDOS.get(0).vehicleCode)

            if (scheduledRouteList.driverDOS.size == 0) {
                preferenceUtils.saveString(PreferenceUtils.Non_Scheduled_Route_Id, nonScheduledRouteList.driverDOS.get(0).vehicleRouteId)
                preferenceUtils.saveString(PreferenceUtils.VEHICLE_CODE, nonScheduledRouteList.driverDOS.get(0).vehicleCode)
                preferenceUtils.saveString(PreferenceUtils.NON_VEHICLE_CODE, nonScheduledRouteList.driverDOS.get(0).vehicleCode)
                preferenceUtils.saveString(PreferenceUtils.CARRIER_ID, nonScheduledRouteList.driverDOS.get(0).vehicleCarrier)
                preferenceUtils.saveString(PreferenceUtils.VR_ID, nonScheduledRouteList.driverDOS.get(0).vehicleRouteId)
                preferenceUtils.saveString(PreferenceUtils.V_PLATE, nonScheduledRouteList.driverDOS.get(0).plate)
                preferenceUtils.saveString(PreferenceUtils.N_SHIPMENTS, nonScheduledRouteList.driverDOS.get(0).nShipments)
                preferenceUtils.saveString(PreferenceUtils.NOTES, nonScheduledRouteList.driverDOS.get(0).notes)
                preferenceUtils.saveInt(PreferenceUtils.UNIT, nonScheduledRouteList.driverDOS.get(0).unit)
                preferenceUtils.saveString(PreferenceUtils.CV_PLATE, nonScheduledRouteList.driverDOS.get(0).plate)

                if (nonScheduledRouteList.driverDOS.get(0).notes.length > 0) {
                    tvNotes.setVisibility(View.VISIBLE)
                    tvNotesLabel.setVisibility(View.VISIBLE)
                    try {
                        var s = nonScheduledRouteList.driverDOS.get(0).notes
                        s = s.substring(s.indexOf("$") + 1)
                        s = s.substring(0, s.indexOf("$"))
                        tvNotes.text = "" + s
                    } catch (e: Exception) {
                    }
                    vehicleCheckInDo.notes = tvNotes.text.toString().trim()
                } else {
                    tvNotes.setVisibility(View.GONE)
                    tvNotesLabel.setVisibility(View.GONE)
                }
                StorageManager.getInstance(this).insertCheckInData(this, vehicleCheckInDo);
            }
        }
        hideLoader()

    }
}
