package com.tbs.brothersgas.haadhir.Model;

import java.io.Serializable;

public class AddressDO implements Serializable {

    public String addressCode = "";
    public String addressDescription = "";
    public String emirates = "";
    public String city = "";
    public int creditFlag = 0;
    public Double creditAmount = 0.0;
    public Double lattitude           = 0.0;
    public Double longitude           = 0.0;



}
