package com.tbs.brothersgas.haadhir.Activitys

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import android.widget.*
import com.tbs.brothersgas.haadhir.Adapters.CurrentStockAdapter
import com.tbs.brothersgas.haadhir.Model.LoadStockDO
import com.tbs.brothersgas.haadhir.Model.LoadStockMainDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.LoadVanSaleRequest
import com.tbs.brothersgas.haadhir.common.AppConstants
import com.tbs.brothersgas.haadhir.listeners.LoadStockListener
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import java.util.ArrayList

//
class CurrentVanSalesActivity : BaseActivity(), LoadStockListener {
    override fun updateCount() {
        var count = preferenceUtils.getIntFromPreference(PreferenceUtils.STOCK_COUNT, 0)
        etUSMass.setText("" + count)
        etUSVolume.setText("" + mass)

    }

    var mass = 0.00

    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var loadStockAdapter: CurrentStockAdapter
    lateinit var loadStockDOs: ArrayList<LoadStockDO>
    lateinit var username: String
    lateinit var loadStockDO: LoadStockDO
    lateinit var etLoadingMass: EditText
    lateinit var etLoadingVolume: EditText
    lateinit var etUSMass: EditText
    lateinit var etUSVolume: EditText
    lateinit var tvVehicleMass: TextView
    lateinit var tvVehicleVolume: TextView
    lateinit var loadStockMainDO: LoadStockMainDO
    lateinit var tvMass: TextView
    lateinit var tvVolume: TextView

    override protected fun onResume() {
        super.onResume()
    }

    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.fragment_van_sales, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
        preferenceUtils = PreferenceUtils(this@CurrentVanSalesActivity)
        username = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "")
        if (Util.isNetworkAvailable(this)) {

            val loadVanSaleRequest = LoadVanSaleRequest(username, this@CurrentVanSalesActivity)
            //  showLoader();
            loadVanSaleRequest.setOnResultListener { isError, loadStockMainDo ->
                hideLoader()

                loadStockMainDO = loadStockMainDo
                if (isError) {
                    Toast.makeText(this@CurrentVanSalesActivity, R.string.error_checkin, Toast.LENGTH_SHORT).show()
                } else {
                    if (loadStockMainDo.loadStockDOS.size > 0) {
                        val contacts = ArrayList<LoadStockDO>()
                        AppConstants.listStockDO = loadStockMainDo.loadStockDOS
                        loadStockAdapter = CurrentStockAdapter(this@CurrentVanSalesActivity, loadStockMainDo.loadStockDOS, this@CurrentVanSalesActivity)
                        recycleview.setAdapter(loadStockAdapter)
                        tvVehicleMass.setText("" + loadStockMainDo.vehicleCapacity)
                        tvVehicleVolume.setText("" + loadStockMainDo.volume)
//                    etLoadingMass.setText("" + loadStockMainDo.vanMass)
//                    tvMass.setText("Mass(" + loadStockMainDo.massUnit+")")
                        tvVolume.setText("Volume(" + loadStockMainDo.volumeUnit+")")
                        etLoadingVolume.setText("" + loadStockMainDo.vanVolume)

                    } else {
                        showAlert("No Routing Id's Found")
                    }

                }
            }
            loadVanSaleRequest.execute()

        } else {
            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "",false)

        }


    }

    override fun initializeControls() {
        tvScreenTitle.setText(R.string.current_stock)
        recycleview = findViewById(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        etLoadingMass = findViewById(R.id.etLoadingMass) as EditText
        etLoadingVolume = findViewById(R.id.etLoadingVolume) as EditText
        tvVehicleMass = findViewById(R.id.tvVehicleMass) as TextView
        tvVehicleVolume = findViewById(R.id.tvVehicleVolume) as TextView
        tvMass = findViewById(R.id.tvMass) as TextView
        tvVolume = findViewById(R.id.tvVolume) as TextView
        etUSMass = findViewById(R.id.etUSMass) as EditText
        etUSVolume = findViewById(R.id.etUSVolume) as EditText

        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        recycleview.setLayoutManager(linearLayoutManager)

//        val btnLoad = findViewById(R.id.btnLoad) as Button
//
//        btnLoad.setOnClickListener {
//            val intent = Intent(this@LoadVanSalesActivity, DashBoardActivity::class.java);
//            startActivity(intent);
//        }
    }
}

private operator fun String.times(totalStockVolume: Double?): Double? {
    return totalStockVolume
}

