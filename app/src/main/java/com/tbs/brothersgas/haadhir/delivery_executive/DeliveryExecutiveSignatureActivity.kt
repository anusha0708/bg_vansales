package com.tbs.brothersgas.haadhir.delivery_executive

import android.Manifest
import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.*
import android.os.Build
import android.os.Environment
import android.text.TextUtils
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.ScrollView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import com.tbs.brothersgas.haadhir.Activitys.BaseActivity
import com.tbs.brothersgas.haadhir.Activitys.OnResultListener
import com.tbs.brothersgas.haadhir.Model.CustomerManagementDO
import com.tbs.brothersgas.haadhir.Model.PodDo
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.*
import com.tbs.brothersgas.haadhir.common.AppConstants
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.pdfs.BulkDeliveryNotePDF
import com.tbs.brothersgas.haadhir.pdfs.CYLDeliveryNotePDF
import com.tbs.brothersgas.haadhir.pdfs.CylinderIssRecPdfBuilder
import com.tbs.brothersgas.haadhir.prints.PrinterConnection
import com.tbs.brothersgas.haadhir.prints.PrinterConstants
import com.tbs.brothersgas.haadhir.utils.CalendarUtils
import com.tbs.brothersgas.haadhir.utils.Constants
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream

/**
 *Created by VenuAppasani on 22-12-2018.
 *Copyright (C) 2018 TBS - All Rights Reserved
 */
class DeliveryExecutiveSignatureActivity : BaseActivity(), View.OnClickListener {


    lateinit var mClear: Button
    lateinit var mGetSign: Button
    lateinit var mCancel: Button
    lateinit var file: File
    lateinit var mContent: LinearLayout
    lateinit var view: View
    lateinit var mSignature: signature
    var bitmap: Bitmap? = null
    var converetdImage: Bitmap? = null

    lateinit var savedPath: String
    lateinit var encodedImage: String
    lateinit var podDo: PodDo
    lateinit var customerManagementDO: CustomerManagementDO
    var shipmentId: String = ""
    var customerCode: String = ""

    // Creating Separate Directory for saving Generated Images
    /*  var DIRECTORY = Environment.getExternalStorageDirectory().getPath() + "/UserSignature/"
      var pic_name = *//*SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())*//*"signature"*/

    var DIRECTORY = Environment.getExternalStorageDirectory().getPath() + "/UserInvoiceSignature/"
    var pic_name = /*SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())*/"signature"
    var StoredPath = DIRECTORY + pic_name + ".png"
    @SuppressLint("MissingPermission")
    override fun initializeControls() {
        mContent = findViewById<View>(R.id.canvasLayout) as LinearLayout
        mSignature = signature(applicationContext, null)
        mSignature.setBackgroundColor(Color.WHITE)
        mContent.addView(mSignature, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        mClear = findViewById<View>(R.id.clear) as Button
        mGetSign = findViewById<View>(R.id.getsign) as Button
        podDo = StorageManager.getInstance(this).getDepartureData(this);

        mGetSign.setEnabled(false)
        mCancel = findViewById<View>(R.id.cancel) as Button
        view = mContent
        mGetSign.setOnClickListener(this@DeliveryExecutiveSignatureActivity)
        mClear.setOnClickListener(this@DeliveryExecutiveSignatureActivity)
        mCancel.setOnClickListener(this@DeliveryExecutiveSignatureActivity)

        // Method to create Directory, if the Directory doesn't exists
        file = File(DIRECTORY)
        if (!file.exists()) {
            file.mkdir()
        }
//         podDo = StorageManager.getInstance(this).getDepartureData(this);

        if (podDo.signature.equals("")) {
            tvScreenTitle.setText("Sign Here")
        }


    }


    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.activity_delivery_execute_digital_signature, null) as ScrollView
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            //showToast("Please save signature")
            finish()
        }
        disableMenuWithBackButton()
        initializeControls()
        shipmentId = intent.getStringExtra(Constants.SHIPMENT_ID)
        customerCode = intent.getStringExtra(Constants.CUSTOMER_CODE)
    }


    override fun onClick(v: View?) {
        // TODO Auto-generated method stub
        if (v === mClear) {
            mSignature.clear()
            mGetSign.setEnabled(false)
        } else if (v === mGetSign) {

            /*if (Build.VERSION.SDK_INT >= 23) {
                if (isStoragePermissionGranted()) {
                    showAppCompatAlert("", "Are You Sure You Want to \n Confirm Delivery?", "OK", "Cancel", "SUCCESS", true)
                }
            } else {
                showAppCompatAlert("", "Are You Sure You Want to \n Confirm Delivery?", "OK", "Cancel", "SUCCESS", true)
            }*/

            view.setDrawingCacheEnabled(true)
            mSignature.save(view, StoredPath)
        } else if (v === mCancel) {
            showToast("Please save signature")
        }
    }

    fun isStoragePermissionGranted(): Boolean {
        if (Build.VERSION.SDK_INT >= 23) {
            if (applicationContext.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                return true
            } else {
                ActivityCompat.requestPermissions(this, arrayOf<String>(Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)
                return false
            }
        } else {
            return true
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            view.setDrawingCacheEnabled(true)
            mSignature.save(view, StoredPath)
//            Toast.makeText(applicationContext, "Successfully Saved", Toast.LENGTH_SHORT).show()
            // Calling the same class
            recreate()
        } else {
            Toast.makeText(this, "The app was not allowed to write to your storage. Hence, it cannot function properly. Please consider granting it this permission", Toast.LENGTH_LONG).show()
        }
    }

    inner class signature(context: Context, attrs: AttributeSet?) : View(context, attrs) {
        private val paint = Paint()
        private val path = Path()

        private var lastTouchX: Float = 0.toFloat()
        private var lastTouchY: Float = 0.toFloat()
        private val dirtyRect = RectF()

        init {
            paint.setAntiAlias(true)
            paint.setColor(Color.BLACK)
            paint.setStyle(Paint.Style.STROKE)
            paint.setStrokeJoin(Paint.Join.ROUND)
            paint.setStrokeWidth(STROKE_WIDTH)
        }

        fun save(v: View, StoredPath: String) {
            Log.v("log_tag", "Width: " + v.getWidth())
            Log.v("log_tag", "Height: " + v.getHeight())
            if (bitmap == null) {
                bitmap = Bitmap.createBitmap(mContent.width, mContent.height, Bitmap.Config.RGB_565)
            }
            val canvas = Canvas(bitmap)
            try {
                val mFileOutStream = FileOutputStream(StoredPath)
                v.draw(canvas)
                bitmap!!.compress(Bitmap.CompressFormat.PNG, 2, mFileOutStream)
                mFileOutStream.flush()
                mFileOutStream.close()

                val bos = ByteArrayOutputStream();
                bitmap!!.compress(Bitmap.CompressFormat.JPEG, 2, bos);
                val bitmapdata = bos.toByteArray();
                Log.e("Signature Size : ", "Size : " + bitmapdata)
                encodedImage = android.util.Base64.encodeToString(bitmapdata, android.util.Base64.DEFAULT)
                podDo.signatureEncode = encodedImage;
                savedPath = StoredPath
                StorageManager.getInstance(context).saveDepartureData(this@DeliveryExecutiveSignatureActivity, podDo)
                if (Util.isNetworkAvailable(context)) {
                    validateDeliveryExecutive()
                } else {
                    showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

                }

            } catch (e: Exception) {
                Log.v("log_tag", e.toString())
            }

        }

        fun clear() {
            path.reset()
            invalidate()
            mGetSign.setEnabled(false)
        }

        fun scaleDown(realImage: Bitmap, maxImageSize: Float,
                      filter: Boolean): Bitmap {
            val ratio = Math.min(
                    maxImageSize.toFloat() / realImage.getWidth(),
                    maxImageSize.toFloat() / realImage.getHeight())
            val width = Math.round(ratio.toFloat() * realImage.getWidth())
            val height = Math.round(ratio.toFloat() * realImage.getHeight())
            val newBitmap = Bitmap.createScaledBitmap(realImage, width,
                    height, filter)
            return newBitmap
        }

        protected override fun onDraw(canvas: Canvas) {
            canvas.drawPath(path, paint)
        }

        override fun onTouchEvent(event: MotionEvent): Boolean {
            val eventX = event.x
            val eventY = event.y
            mGetSign.setEnabled(true)

            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    path.moveTo(eventX, eventY)
                    lastTouchX = eventX
                    lastTouchY = eventY
                    return true
                }

                MotionEvent.ACTION_MOVE,

                MotionEvent.ACTION_UP -> {

                    resetDirtyRect(eventX, eventY)
                    val historySize = event.historySize
                    for (i in 0 until historySize) {
                        val historicalX = event.getHistoricalX(i)
                        val historicalY = event.getHistoricalY(i)
                        expandDirtyRect(historicalX, historicalY)
                        path.lineTo(historicalX, historicalY)
                    }
                    path.lineTo(eventX, eventY)
                }

                else -> {
                    debug("Ignored touch event: " + event.toString())
                    return false
                }
            }

            invalidate((dirtyRect.left - HALF_STROKE_WIDTH).toInt(),
                    (dirtyRect.top - HALF_STROKE_WIDTH).toInt(),
                    (dirtyRect.right + HALF_STROKE_WIDTH).toInt(),
                    (dirtyRect.bottom + HALF_STROKE_WIDTH).toInt())

            lastTouchX = eventX
            lastTouchY = eventY

            return true
        }

        private fun debug(string: String) {

            Log.v("log_tag", string)

        }

        private fun expandDirtyRect(historicalX: Float, historicalY: Float) {
            if (historicalX < dirtyRect.left) {
                dirtyRect.left = historicalX
            } else if (historicalX > dirtyRect.right) {
                dirtyRect.right = historicalX
            }

            if (historicalY < dirtyRect.top) {
                dirtyRect.top = historicalY
            } else if (historicalY > dirtyRect.bottom) {
                dirtyRect.bottom = historicalY
            }
        }

        private fun resetDirtyRect(eventX: Float, eventY: Float) {
            dirtyRect.left = Math.min(lastTouchX, eventX)
            dirtyRect.right = Math.max(lastTouchX, eventX)
            dirtyRect.top = Math.min(lastTouchY, eventY)
            dirtyRect.bottom = Math.max(lastTouchY, eventY)
        }

    }

    fun getResizedBitmap(image: Bitmap, maxSize: Int): Bitmap {
        var width = image.getWidth()
        var height = image.getHeight()
        val bitmapRatio = width.toFloat() / height.toFloat()
        if (bitmapRatio > 1) {
            width = maxSize
            height = (width / bitmapRatio).toInt()
        } else {
            height = maxSize
            width = (height * bitmapRatio).toInt()
        }
        return Bitmap.createScaledBitmap(image, 250, 250, true)
    }

    companion object {
        private val STROKE_WIDTH = 5f
        private val HALF_STROKE_WIDTH = STROKE_WIDTH / 2
    }

    private fun validateDeliveryExecutive() {

        if (!TextUtils.isEmpty(shipmentId)) {
            showLoader()
            var validateDeliveryRequest = ValidateDeliveryExecutiveRequest(this, shipmentId, customerCode)
            validateDeliveryRequest.setOnResultListener { isError, validateDeliveryDO ->
                hideLoader()
                if (isError) {
                    hideLoader()
                    showAlert("Error in Service Response, Please try again")
                } else {
                    preferenceUtils.removeFromPreference(PreferenceUtils.POD_NAME);
                    preferenceUtils.removeFromPreference(PreferenceUtils.POD_NUMBER);
                    prepareDeliveryNoteCreation(shipmentId)

                }

            }

            validateDeliveryRequest.execute()
        }

        }

    private fun prepareDeliveryNoteCreation(id: String) {

        if (id.length > 0) {
            val driverListRequest = ExecutiveActiveDeliveryRequest(id, this)
            driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                if (activeDeliveryDo != null) {
                    if (isError) {
                    } else {

                        val note = CYLDeliveryNotePDF(this).createDeliveryPDF(activeDeliveryDo, "HISTORY");
                        note.CreateHistoryPDF().execute()
                        note.isFromDeliverySignatureToFinishActivity(true)
                    }
                } else {
                }
            }
            driverListRequest.execute()
        }
    }




    override fun onBackPressed() {
        showToast("Please save signature")
    }

    override fun onButtonYesClick(from: String) {

        if ("SUCCESS".equals(from, ignoreCase = true)) {
            view.setDrawingCacheEnabled(true)
            mSignature.save(view, StoredPath)
        } else {

        }
    }


}