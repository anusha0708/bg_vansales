package com.tbs.brothersgas.haadhir.Model;

import java.io.Serializable;

public class SalesInvoiceDO implements Serializable {


    public String documentNumber = "";
    public String customerName = "";
    public String customerId = "";
    public String product = "";
    public String productDescription = "";
    public String productUnit = "";
    public Double quantity = 0.0;
    public Double amount = 0.0;
    public String currency = "";




}
