package com.tbs.brothersgas.haadhir.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.brothersgas.haadhir.Activitys.BaseActivity;
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryDO;
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryMainDO;
import com.tbs.brothersgas.haadhir.common.AppConstants;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;
import com.tbs.brothersgas.haadhir.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class PendingOrdersActiveDeliveryRequest extends AsyncTask<String, Void, Boolean> {

    private ActiveDeliveryMainDO activeDeliveryMainDO;
    private Context mContext;
    private String id;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;
    ActiveDeliveryDO activeDeliveryDO;

    public PendingOrdersActiveDeliveryRequest(String id, Context mContext) {

        this.mContext = mContext;
        this.id = id;
    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        public void onCompleted(boolean isError, ActiveDeliveryMainDO activeDeliveryMainDO);

    }

    public boolean runRequest() {
        // System.out.println("CUSTOMER ID " + customerId);
        String NAMESPACE = "http://www.adonix.com/WSS";
        String METHOD_NAME = "run";
        String SOAP_ACTION = "CAdxWebServiceXmlCC";
        preferenceUtils = new PreferenceUtils(mContext);
        username = preferenceUtils.getStringFromPreference(PreferenceUtils.A_USER_NAME, "");
        password = preferenceUtils.getStringFromPreference(PreferenceUtils.A_PASSWORD, "");
        ip         = preferenceUtils.getStringFromPreference(PreferenceUtils.IP_ADDRESS, "");
        port = preferenceUtils.getStringFromPreference(PreferenceUtils.PORT, "");
        pool = preferenceUtils.getStringFromPreference(PreferenceUtils.ALIAS, "");
        String URL = "http://" + ip + ":" + port + "/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC";

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        // Set all input params                         X10CSAINV3
//        request.addProperty("publicName", "");
        request.addProperty("publicName", WebServiceConstants.PENDING_ORDER_ACTIVE_DELIVERY);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("I_YSOHNUM", id);

//            jsonObject.put("I_YSDHNUM", "SD-U302-19008357");

        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        request.addProperty("inputXml", jsonObject.toString());

        SoapObject callcontext = new SoapObject("", "callContext");
        // Set all input params
        callcontext.addProperty("codeLang", "ENG");
        callcontext.addProperty("poolAlias", pool);
        callcontext.addProperty("poolId", "");
        callcontext.addProperty("codeUser", username);
        callcontext.addProperty("password", password);
        callcontext.addProperty("requestConfig", "adxwss.trace.on=off");

        request.addSoapObject(callcontext);


        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.setOutputSoapObject(request);

        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
        androidHttpTransport.debug = true;

        try {
            List<HeaderProperty> headerList = new ArrayList<HeaderProperty>();
            headerList.add(new HeaderProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode((username + ":" + password).getBytes())));


            androidHttpTransport.call(SOAP_ACTION, envelope, headerList);


            SoapObject response = (SoapObject) envelope.getResponse();
            String resultXML = (String) response.getProperty("resultXml");
            if (resultXML != null && resultXML.length() > 0) {
                return parseXML(resultXML);
            } else {
                return false;
            }
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            activeDeliveryMainDO = new ActiveDeliveryMainDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        activeDeliveryMainDO.activeDeliveryDOS = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        activeDeliveryDO = new ActiveDeliveryDO();

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {


                        if (attribute.equalsIgnoreCase("I_YSDHNUM")) {
                            if (text.length() > 0) {

                                activeDeliveryMainDO.shipmentNumber = text;
                            }


                        } else if (attribute.equalsIgnoreCase("O_YBPC")) {
                            activeDeliveryMainDO.customer = text;

                        } else if (attribute.equalsIgnoreCase("O_YBPCSHO")) {
                            activeDeliveryMainDO.customerDescription = text;

                        } else if (attribute.equalsIgnoreCase("O_YREF")) {
                            if (text.length() > 0) {

                                activeDeliveryMainDO.referenceID = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YARTIM")) {
                            if (text.length() > 0) {

                                activeDeliveryMainDO.arrivalTime = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_XCRETIM")) {
                            if (text.length() > 0) {

                                activeDeliveryMainDO.createdTime = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_XCPYIMG")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.logo = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITEDES")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.siteDescription = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITADDLIG0")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.siteAddress1 = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITADDLIG1")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.siteAddress2 = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITADDLIG2")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.siteAddress3 = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITCRY")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.siteCountry = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITCTY")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.siteCity = text;

                            }
                        }

                        else if (attribute.equalsIgnoreCase("O_XDELEM")) {
                            if(text.length()>0){

                                activeDeliveryMainDO.deliveryEmail= text;
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_XSIGNATURE")) {
                            if(text.length()>0){

                                activeDeliveryMainDO.signature= text;
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_XINVEM")) {
                            if(text.length()>0){

                                activeDeliveryMainDO.invoiceEmail= text;
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_XPAYEM")) {
                            if(text.length()>0){

                                activeDeliveryMainDO.paymentEmail= text;
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_XCYLEM")) {
                            if(text.length()>0){

                                activeDeliveryMainDO.cylinderIssueEmail= text;
                            }
                        }

                        else if (attribute.equalsIgnoreCase("O_XSITPOS")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.sitePostalCode = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITLND")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.siteLandLine = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITMOB")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.siteMobile = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITFAX")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.siteFax = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_YBPADDLIG")) {


                            activeDeliveryMainDO.customerStreet = text;
                        } else if (attribute.equalsIgnoreCase("O_YBPADDLIG1")) {


                            activeDeliveryMainDO.customerLandMark = text;
                        } else if (attribute.equalsIgnoreCase("O_YBPADDLIG2")) {


                            activeDeliveryMainDO.customerTown = text;
                        } else if (attribute.equalsIgnoreCase("O_YCITY")) {


                            activeDeliveryMainDO.customerCity = text;
                        } else if (attribute.equalsIgnoreCase("O_YPOS")) {


                            activeDeliveryMainDO.customerPostalCode = text;
                        } else if (attribute.equalsIgnoreCase("O_XSITEML1")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.siteEmail1 = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITEML2")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.siteEmail2 = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITWEB")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.siteWebEmail = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_YARDAT")) {
                            activeDeliveryMainDO.arraivalDate = text;

                        } else if (attribute.equalsIgnoreCase("O_YDPTIM")) {
                            activeDeliveryMainDO.departureTime = text;

                        } else if (attribute.equalsIgnoreCase("O_YDPDAT")) {
                            activeDeliveryMainDO.departureDate = text;

                        } else if (attribute.equalsIgnoreCase("O_YDLVDAT")) {
                            activeDeliveryMainDO.deliveryDate = text;

                        }
                        else if (attribute.equalsIgnoreCase("O_XCREDAT")) {
                            activeDeliveryMainDO.createdDate = text;

                        }
                        else if (attribute.equalsIgnoreCase("O_YCRYNAME")) {
                            activeDeliveryMainDO.countryName = text;

                        }  else if (attribute.equalsIgnoreCase("O_YWEB")) {
                            activeDeliveryMainDO.webSite = text;

                        } else if (attribute.equalsIgnoreCase("O_YLD")) {
                            activeDeliveryMainDO.landLine = text;

                        } else if (attribute.equalsIgnoreCase("O_YEM")) {
                            activeDeliveryMainDO.email = text;

                        } else if (attribute.equalsIgnoreCase("O_YFAX")) {
                            activeDeliveryMainDO.fax = text;

                        } else if (attribute.equalsIgnoreCase("O_YMOB")) {
                            activeDeliveryMainDO.mobile = text;

                        } else if (attribute.equalsIgnoreCase("O_YLAT")) {
                            activeDeliveryMainDO.lattitude = text;

                        }
                        else if (attribute.equalsIgnoreCase("O_YVALFLG")) {
                            if(text.length()>0){

                                activeDeliveryMainDO.validateFlag = Integer.parseInt(text);
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YINVFLG")) {
                            if(text.length()>0){

                                activeDeliveryMainDO.invoiceFlag = Integer.parseInt(text);
                            }


                        }
                        else if (attribute.equalsIgnoreCase("O_YINVNUM")) {
                            if(text.length()>0){

                                activeDeliveryMainDO.invoiceNumber =text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_LON")) {
                            activeDeliveryMainDO.longitude = text;

                        }
                        else if (attribute.equalsIgnoreCase("O_YSHIDAT")) {
                            activeDeliveryMainDO.shipmentDate = text;

                        }
                        else if (attribute.equalsIgnoreCase("O_XCPYNAM")) {
                            activeDeliveryMainDO.companyCode = text;

                        }
                        else if (attribute.equalsIgnoreCase("O_XCPYDES")) {
                            activeDeliveryMainDO.companyDescription = text;

                        }
                        else if (attribute.equalsIgnoreCase("O_YITMREF")) {
                            activeDeliveryDO.product = text;
                            activeDeliveryDO.pType = "SCHEDULED";


                        } else if (attribute.equalsIgnoreCase("O_YITMDES")) {
                            activeDeliveryDO.productDescription = text;

                        }
                        else if (attribute.equalsIgnoreCase("O_YITMLOC")) {
                            activeDeliveryDO.location = text;

                        }

                        else if (attribute.equalsIgnoreCase("O_YSAU")) {
                            activeDeliveryDO.unit = text;

                        } else if (attribute.equalsIgnoreCase("O_YQTY")) {
                            if (!text.isEmpty()) {
                                activeDeliveryDO.actualQuantity = Double.parseDouble(text);

                                activeDeliveryDO.totalQuantity = Double.parseDouble(text);
                                activeDeliveryDO.orderedQuantity = Double.parseDouble(text);// to fix issue at added products showing full qty instead selected qty in add productlistactivity
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YREMARKS")) {
                            if (!text.isEmpty()) {

                                activeDeliveryMainDO.remarks = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YSDDLIN")) {
                            if (!text.isEmpty()) {

                                activeDeliveryDO.linenumber = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_XCREUSR")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.createUserID = text;

                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_XCREUSRNAM")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.createUserName = text;

                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_YCMTR")) {
                            if (!text.isEmpty()) {

                                activeDeliveryDO.openingQuantity = text;

                            }

                        } else if (attribute.equalsIgnoreCase("O_YOMTR")) {
                            if (!text.isEmpty()) {

                                activeDeliveryDO.endingQuantity = text;

                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YPAYTERM")) {
                            if(text.length()>0){

                                activeDeliveryMainDO.paymentTerm= text;
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_YSDHTYP")) {
                            if(text.length()>0){

                                activeDeliveryMainDO.deliveryType= text;
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_YOMTR1")) {
                            if (!text.isEmpty()) {

                                activeDeliveryDO.openingQuantityLong = text;

                            }

                        } else if (attribute.equalsIgnoreCase("O_YCMTR1")) {
                            if (!text.isEmpty()) {

                                activeDeliveryDO.endingQuantityLong = text;

                            }

                        }
//
                        else if (attribute.equalsIgnoreCase("O_YCPRI")) {
                            if (!text.isEmpty()) {

                                activeDeliveryDO.price = Double.parseDouble(text);
                            }

                        }

                        text = "";


                    }


                    if (endTag.equalsIgnoreCase("GRP3")) {
                        //   customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        activeDeliveryMainDO.activeDeliveryDOS.add(activeDeliveryDO);
                        if (activeDeliveryDO.productType.length() > 0) {

                            activeDeliveryDO.productType = AppConstants.ShipmentListView;
                        }

                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {

        super.onPreExecute();
        ((BaseActivity) mContext).showLoader();

        // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        ((BaseActivity) mContext).hideLoader();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, activeDeliveryMainDO);
        }
    }
}