package com.tbs.brothersgas.haadhir.Activitys

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.tbs.brothersgas.haadhir.Adapters.UnLoadStockAdapter
import com.tbs.brothersgas.haadhir.Model.LoadStockDO
import com.tbs.brothersgas.haadhir.R
import java.util.ArrayList

//
class UnloadVanSalesStockActivity : BaseActivity() {
    lateinit var recycleview : androidx.recyclerview.widget.RecyclerView
    lateinit var loadStockAdapter: UnLoadStockAdapter
    lateinit var loadStockDOs: ArrayList<LoadStockDO>

    override  protected fun onResume() {
        super.onResume()
    }
    override fun initialize() {
      var  llCategories = getLayoutInflater().inflate(R.layout.unload_van_sale_stock_screen, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
    }
    override fun initializeControls() {
        tvScreenTitle.setText(R.string.unload_vansale_stock)
        recycleview = findViewById(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)

        recycleview.setLayoutManager(linearLayoutManager)
        loadStockAdapter = UnLoadStockAdapter(this@UnloadVanSalesStockActivity,  null)
        recycleview.setAdapter(loadStockAdapter)
    }
}