package com.tbs.brothersgas.haadhir.Activitys

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import com.tbs.brothersgas.haadhir.Adapters.LoadStockAdapter
import com.tbs.brothersgas.haadhir.Adapters.LoadVanSaleStockPagerAdapter
import com.tbs.brothersgas.haadhir.Adapters.ScheduledProductsAdapter
import com.tbs.brothersgas.haadhir.Model.LoadStockDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.*
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import java.util.ArrayList

//
class NewAvailableStockActivity : BaseActivity() {
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    private var scheduleDos: ArrayList<LoadStockDO> = ArrayList()
    private var nonScheduleDos: ArrayList<LoadStockDO> = ArrayList()
    private var availableStockDos: ArrayList<LoadStockDO> = ArrayList()

    override protected fun onResume() {
        super.onResume()
    }

    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.available_stock, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()
        disableMenuWithBackButton()
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            setResult(14, null)
            finish()
        }
    }

    override fun initializeControls() {
        tvScreenTitle.setText("Available Stock")
        recycleview = findViewById<View>(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        recycleview.setLayoutManager(androidx.recyclerview.widget.LinearLayoutManager(this))

       loadNonVehicleStockData()


    }

    private fun loadVehicleStockData() {
        if (Util.isNetworkAvailable(this)) {
            var scheduledRootId = "" + preferenceUtils.getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, "")

//            var scheduledRootId = "" + preferenceUtils.getStringFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID, "")
            if (scheduledRootId.length > 0) {
                val loadVanSaleRequest = CurrentScheduledStockRequest(scheduledRootId, this@NewAvailableStockActivity)
                showLoader()

                loadVanSaleRequest.setOnResultListener { isError, loadStockMainDo ->
                    hideLoader()
                    if (isError) {
                        showToast("No vehicle data found.")
                    } else {
                        hideLoader()

                        scheduleDos = loadStockMainDo.loadStockDOS;

                    }
                    loadNonVehicleStockData()
                }
                loadVanSaleRequest.execute()
            } else {
                loadNonVehicleStockData()

            }


        } else {
            showToast("No Internet connection, please try again later")
        }
    }

    private fun loadNonVehicleStockData() {
        if (Util.isNetworkAvailable(this)) {
                val loadVanSaleRequest = CurrentVehicleStockRequest("", this@NewAvailableStockActivity)
                showLoader()
                loadVanSaleRequest.setOnResultListener { isError, loadStockMainDo ->
                    hideLoader()
                    if (isError) {
                        showToast("No vehicle data found.")
                    } else {
                        hideLoader()
                        nonScheduleDos = loadStockMainDo.loadStockDOS;
                        var isProductExisted = false;
                        availableStockDos = nonScheduleDos
                        var loadStockAdapter = LoadStockAdapter(this@NewAvailableStockActivity, availableStockDos, "Shipments")
                        recycleview.setAdapter(loadStockAdapter)
                    }
                }
                loadVanSaleRequest.execute()
        } else {
            showToast("No Internet connection, please try again later")
        }
    }
}