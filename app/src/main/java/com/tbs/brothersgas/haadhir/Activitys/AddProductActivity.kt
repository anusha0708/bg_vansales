package com.tbs.brothersgas.haadhir.Activitys

import android.app.Dialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.brothersgas.haadhir.Model.*
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.*
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import androidx.drawerlayout.widget.DrawerLayout
import android.view.Gravity
import com.tbs.brothersgas.haadhir.Adapters.*
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.database.models.SaveStockDBModel
import com.tbs.brothersgas.haadhir.listeners.DBProductsListener
import kotlin.collections.ArrayList


class AddProductActivity : BaseActivity() ,DBProductsListener {
    override fun updateData() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
    lateinit var nonScheduledProductMainDO: NonScheduledProductMainDO

    lateinit var tvSelection: TextView
    lateinit var dialog: Dialog
    lateinit var pickDos: ArrayList<PickUpDo>
    lateinit var createInvoiceDO: CreateInvoiceDO
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var llOrderHistory: RelativeLayout
    lateinit var productDOS: List<ProductDO>
    lateinit var DBProductsListener :DBProductsListener
    var customerId :String =""
    private lateinit var customProductDOSData: List<String>
    public  lateinit var btnAdd:Button
    lateinit var stockList : ArrayList<SaveStockDBModel>
    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.create_product_screen, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, Gravity.LEFT)

    }

    override fun initializeControls() {
        tvScreenTitle.setText("Select Products")
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
        recycleview = findViewById<View>(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        recycleview.setLayoutManager(androidx.recyclerview.widget.LinearLayoutManager(this))
        btnAdd = findViewById<View>(R.id.btnAdd) as Button
        if (intent.hasExtra("CustomerId")) {
            customerId = intent.extras.getString("CustomerId")
        }
        var preferenceUtils = PreferenceUtils(this)


        //  val date = preferenceUtils.getStringFromPreference(PreferenceUtils.CHECKIN_DATE, Date)
        val location = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "QUA02")

        val siteDO = SiteDO()

        /*val createDelivery = NonScheduledProductMainDOScheduledProductsRequest(name,"20181213","STO15",this)

        createDelivery.setOnResultListener { isError, deliveryMainDo ->
            hideLoader()
            nonScheduledProductMainDO = deliveryMainDo
            if (isError) {
                hideLoader()
                Toast.makeText(this, R.string.error_product_list, Toast.LENGTH_SHORT).show()
            } else {
                if (deliveryMainDo.nonSheduledProductDOS.size > 0) {

                    val driverAdapter = CreateDeliveryProductAdapter(this, deliveryMainDo.nonSheduledProductDOS)
                    recycleview.setAdapter(driverAdapter)
                } else {
                  hideLoader()
                    showAlert("" + R.string.error_NoData)
                }
            }
        }
        createDelivery.execute()

          btnAdd.setOnClickListener {
              val siteId = preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID, Date)

              val createDeliveryRequest = CreateDeliveryRequest(customerId,siteId,nonScheduledProductMainDO.nonSheduledProductDOS,this)

              createDeliveryRequest.setOnResultListener { isError, deliveryMainDo ->
                  hideLoader()
                var  createDeliveryMAinDo = deliveryMainDo
                  if (isError) {
                      hideLoader()
                      showAppCompatAlert("Error", "Delivery Not Created", "Ok", "", "SUCCESS", false)
                  } else {
                      if (createDeliveryMAinDo.createDeliveryDOS.size > 0) {
                         if(createDeliveryMAinDo.deliveryNumber.length>0){

                             showAppCompatAlert("Success", "Delivery Created", "Ok", "", "SUCCESS", false)
                         }else{
                             showAppCompatAlert("Error", "Delivery Not Created", "Ok", "", "SUCCESS", false)

                         }

//
//                          val driverAdapter = CreateDeliveryProductAdapter(this, deliveryMainDo.nonSheduledProductDOS)
//                          recycleview.setAdapter(driverAdapter)
                      } else {
                          hideLoader()
                          showAlert("" + R.string.error_NoData)
                      }
                  }
              }
              createDeliveryRequest.execute()
          }*/


        var driverID = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "QUA02")
        stockList= StorageManager.getInstance(this).getAllVechicleStockDetails(driverID)
        if (stockList!= null && stockList.size > 0) {
            val driverAdapter = OfflineDeliveryProductAdapter(this, stockList, object : ProductModifiedListner {
                override fun updateProductCount(customProductDOS: List<String>) {
                    customProductDOSData = customProductDOS;
                }
            })
            recycleview.setAdapter(driverAdapter)
        } else {
            hideLoader()
            showAlert("" +"Stock not available. Please load the stock to deliver it")
        }
        btnAdd.setOnClickListener {
            /*//TODO: Handle Checked items
                        for (i in customProductDOSData.indices) {
                            var selectedItem = customProductDOSData.get(i);
                        }
                        */

            var siteId = preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID, "")

//            val createDeliveryRequest = CreateDeliveryRequest(siteId, customerId, stockList, this)
//
//            createDeliveryRequest.setOnResultListener { isError, deliveryMainDo ->
//                hideLoader()
//                var createDeliveryMAinDo = deliveryMainDo
//                if (isError) {
//                    hideLoader()
//                    showAppCompatAlert("Error", "Delivery Not Created", "Ok", "", "SUCCESS", false)
//                } else {
//                    if (createDeliveryMAinDo.createDeliveryDOS.size > 0) {
//                        if (createDeliveryMAinDo.deliveryNumber.length > 0) {
//
//                            showAppCompatAlert("Success", "Delivery Created", "Ok", "", "SUCCESS", false)
//                        } else {
//                            showAppCompatAlert("Error", "Delivery Not Created", "Ok", "", "SUCCESS", false)
//
//                        }
//
////
////                          val driverAdapter = CreateDeliveryProductAdapter(this, deliveryMainDo.nonSheduledProductDOS)
////                          recycleview.setAdapter(driverAdapter)
//                    } else {
//                        hideLoader()
//                        showAlert("" + R.string.error_NoData)
//                    }
//                }
//            }
//            createDeliveryRequest.execute()
        }
//
//        btnAdd.setOnClickListener(View.OnClickListener {
//            var result: Int;
//            if(customProductDOSData != null && customProductDOSData.size > 0) {
//                for (i in customProductDOSData.indices) {
//                    var actualValue = Integer.parseInt(stockList.get(i).item_quantity)
//                    var updatedValue = Integer.parseInt(customProductDOSData.get(i).toString())
//                    result = actualValue - updatedValue
//
//                    var massValue = (java.lang.Double.parseDouble(stockList.get(i).item_weight.toString()) * result)
//                    StorageManager.getInstance(this).updateSingleStockDetails(driverID, stockList.get(i).item_id, result, massValue)
//                }
//            }
//            //Get All vehicle stock details
//            var stockList: ArrayList<SaveStockDBModel> = StorageManager.getInstance(this).getAllVechicleStockDetails(driverID)
//
////            StorageManager.getInstance(this).deleteStockDetails(driverID, itemid)
///*
//            //Update selected vehicle stock details
//            var saveStockModel: SaveStockDBModel  = SaveStockDBModel();
//            StorageManager.getInstance(activity).updateStockVehicleDetails(saveStockModel)
//
//            //Delete selected vehicle stock record details
//            StorageManager.getInstance(activity).deleteStockDetails("UserId", deliveryMainDo.nonSheduledProductDOS.get(0).item)*/
//        })
    }

    /*override fun updateProductCount(customProductDOS: MutableList<String>?) {
        this.customProductDOS = customProductDOS!!;
    }*/

    interface ProductModifiedListner {
        fun updateProductCount(customProductDOS: List<String>)
    }
}