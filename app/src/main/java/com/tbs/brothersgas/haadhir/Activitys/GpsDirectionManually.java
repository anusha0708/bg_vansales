//package com.tbs.brothersgas.brothersgas.Activitys;
//
//import android.Manifest;
//import android.app.Activity;
//import android.app.Dialog;
//import android.app.ProgressDialog;
//import android.content.Context;
//import android.content.Intent;
//import android.content.pm.PackageManager;
//import android.graphics.Color;
//import android.location.Address;
//import android.location.Geocoder;
//import android.location.Location;
//import android.location.LocationListener;
//import android.location.LocationManager;
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.support.v4.app.ActivityCompat;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.AutoCompleteTextView;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.google.android.gms.ads.AdRequest;
//import com.google.android.gms.ads.NativeExpressAdView;
//import com.google.android.gms.ads.VideoController;
//import com.google.android.gms.ads.VideoOptions;
//import com.google.android.gms.common.ConnectionResult;
//import com.google.android.gms.common.api.GoogleApiClient;
//import com.google.android.gms.common.api.Status;
//import com.google.android.gms.location.places.Place;
//import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
//import com.google.android.gms.location.places.ui.PlaceSelectionListener;
//import com.google.android.gms.maps.CameraUpdate;
//import com.google.android.gms.maps.CameraUpdateFactory;
//import com.google.android.gms.maps.GoogleMap;
//import com.google.android.gms.maps.MapFragment;
//import com.google.android.gms.maps.OnMapReadyCallback;
//import com.google.android.gms.maps.model.BitmapDescriptorFactory;
//import com.google.android.gms.maps.model.LatLng;
//import com.google.android.gms.maps.model.LatLngBounds;
//import com.google.android.gms.maps.model.Marker;
//import com.google.android.gms.maps.model.MarkerOptions;
//import com.google.android.gms.maps.model.Polyline;
//import com.google.android.gms.maps.model.PolylineOptions;
//import com.tbs.brothersgas.brothersgas.DirectionActivitys.AbstractRouting;
//import com.tbs.brothersgas.brothersgas.DirectionActivitys.Route;
//import com.tbs.brothersgas.brothersgas.DirectionActivitys.Routing;
//import com.tbs.brothersgas.brothersgas.DirectionActivitys.RoutingListener;
//import com.tbs.brothersgas.brothersgas.R;
//import com.tbs.brothersgas.brothersgas.utils.PreferenceUtils;
//
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Locale;
//
//
//
//public class GpsDirectionManually extends BaseActivity implements
//        OnMapReadyCallback, RoutingListener, View.OnClickListener,
//        GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {
//
//    RelativeLayout distanceRelative;
//    boolean isLayoutVisible = false;
//    AutoCompleteTextView startpoint, destinationpoint;
//    String string_start, string_dest;
//    GoogleMap gMap;
//
//
//    protected LatLng start;
//    protected LatLng end;
//    private ProgressDialog progressDialog;
//    private List<Polyline> polylines;
//    private static final int[] COLORS = new int[]{R.color.blue, R.color.blue, R.color.blue, R.color.blue, R.color.blue};
//
//    private Button getDirections;
//    Context mContext = this;
//    String currentaddress;
//    double lati;
//    double longi;
//    TextView tvDistance;
//    TextView tvTime;
//    int hours;
//    int minutes;
//    int seconds;
//
//    double lat = 0;
//    double lng = 0;
//
//
//    LatLng currentLocation;
//    private Button directionCar, directiontransist, directionWalk;
//
//    PlaceAutocompleteFragment StartautocompleteFragment;
//
//    Dialog dia;
//    ImageView ivGetCurrentLocation;
//
//
//    private LinearLayout nativeAdContainer;
//
//    LinearLayout adView;
//
//    private static String LOG_TAG = "EXAMPLE";
//
//    NativeExpressAdView mAdView;
//    VideoController mVideoController;
//
//
//
//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//        gMap = googleMap;
//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            // to handle the case where the user grants the permission. See the documentation
//            // for ActivityCompat#requestPermissions for more details.
//            return;
//        }
//        gMap.setMyLocationEnabled(true);
//
//        gMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
//            @Override
//            public boolean onMarkerClick(Marker marker) {
//                return false;
//            }
//        });
//    }
//
//    public LatLng getLocationFromAddress(Context context, String strAddress) {
//
//        Geocoder coder = new Geocoder(context);
//        List<Address> address;
//        LatLng p1 = null;
//
//        try {
//            // May throw an IOException
//            address = coder.getFromLocationName(strAddress, 5);
//            if (address == null) {
//                return null;
//            }
//            Address location = address.get(0);
//            location.getLatitude();
//            location.getLongitude();
//
//            p1 = new LatLng(location.getLatitude(), location.getLongitude());
//
//        } catch (IOException ex) {
//
//            ex.printStackTrace();
//        }
//
//        return p1;
//    }
//
//    public void route() {
//        double lat = Double.valueOf(preferenceUtils.getDoubleFromPreference(PreferenceUtils.LATTITUDE2, 0.0));
//        double lng = Double.valueOf(preferenceUtils.getDoubleFromPreference(PreferenceUtils.LONGITUDE2, 0.0));
//        double lat2= Double.valueOf(preferenceUtils.getDoubleFromPreference(PreferenceUtils.LATTITUDE2, 0.0));
//        double lng2 = Double.valueOf(preferenceUtils.getDoubleFromPreference(PreferenceUtils.LONGITUDE2, 0.0));
//
//        start  = new LatLng(lat,lng);
//        end  =  new LatLng(lat2,lng2);
//
//
//        if (start == null || end == null) {
//            if (start == null) {
//
//                Toast.makeText(this, "Please choose a starting point.", Toast.LENGTH_SHORT).show();
//            }
//
//            if (end == null) {
//
//                Toast.makeText(this, "Please choose a destination.", Toast.LENGTH_SHORT).show();
//
//            }
//        } else {
//
//            List<Integer> imageList = new ArrayList<Integer>();
//            imageList.add(R.drawable.arrow);
//            imageList.add(R.drawable.arrow);
//            imageList.add(R.drawable.arrow);
//            imageList.add(R.drawable.arrow);
////
////
////            progressDialogg = new FlipProgressDialog();
////            progressDialogg.setImageList(imageList);
////            progressDialogg.setOrientation("rotationY");
////            progressDialogg.setDuration(800);
////            progressDialogg.setBackgroundColor(Color.parseColor("#277be1"));
////            progressDialogg.setBackgroundAlpha(0.9f);
//         //   progressDialogg.show(getFragmentManager(), "Fetching route information.");//
//            showLoader();
//         /*   progressDialogg = FlipProgressDialog.show(this, "Please wait.",
//                    "Fetching route information.", true);*/
//            Routing routing = new Routing.Builder()
//                    .travelMode(AbstractRouting.TravelMode.DRIVING)
//                    .withListener(this)
//                    .alternativeRoutes(false)
//                    .waypoints(start, end)
//                    .build();
//            directionCar.setSelected(true);
//            directionCar.setBackgroundResource(R.mipmap.ic_launcher);
//            directionWalk.setBackgroundResource(R.mipmap.ic_launcher);
//            directiontransist.setBackgroundResource(R.mipmap.ic_launcher);
//            routing.execute();
//
//
//        }
//    }
//
//
//    @Override
//    public void onRoutingFailure(com.tbs.brothersgas.brothersgas.DirectionActivitys.RouteException e) {
//        hideLoader();
//        if (e != null) {
//            Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
//        } else {
//            Toast.makeText(this, "Something went wrong, Try again", Toast.LENGTH_SHORT).show();
//        }
//    }
//
//    @Override
//    public void onRoutingStart() {
//        // The Routing Request starts
//    }
//
//
//    @Override
//    public void onRoutingSuccess(List<Route> route, int shortestRouteIndex) {
//       hideLoader();
//        if (polylines.size() > 0) {
//            for (Polyline poly : polylines) {
//                poly.remove();
//            }
//        }
//
//        polylines = new ArrayList<>();
//        //add route(s) to the gps.
//        for (int i = 0; i < route.size(); i++) {
//
//            //In case of more than 5 alternative routes
//            int colorIndex = i % COLORS.length;
//
//            PolylineOptions polyOptions = new PolylineOptions();
//            polyOptions.color(getResources().getColor(COLORS[colorIndex]));
//
//            polyOptions.width(10 + i * 3);
//            polyOptions.addAll(route.get(i).getPoints());
//            Polyline polyline = gMap.addPolyline(polyOptions);
//            polylines.add(polyline);
//            int totalSeconds = route.get(0).getDurationValue();
//            float totaldistance = route.get(0).getDistanceValue();
//            float distanceString = totaldistance / 1000;
//
//            hours = totalSeconds / 3600;
//            minutes = (totalSeconds % 3600) / 60;
//            seconds = totalSeconds % 60;
//            String timer;
//            if (hours != 0) {
//                timer = hours + "hour" + " " + minutes + " " + "mins";
//            } else {
//                timer = minutes + " " + "mins";
//            }
//            Log.d("TIMER", timer);
//            String timeString = String.format("%02d:%02d:%02d", hours, minutes, seconds);
//            if (!isLayoutVisible) {
//                distanceRelative.setVisibility(View.VISIBLE);
//                isLayoutVisible = true;
//            } else {
//                distanceRelative.setVisibility(View.GONE);
//                isLayoutVisible = false;
//            }
//            tvTime.setText("Driving Time:" + timer);
//            tvDistance.setText("Distance:" + distanceString + " " + "KM");
//
//            LatLngBounds.Builder builder = new LatLngBounds.Builder();
//            builder.include(start);
//            builder.include(end);
//            LatLngBounds bounds = builder.build();
//
//            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 150);
//            gMap.animateCamera(cu, new GoogleMap.CancelableCallback() {
//                public void onCancel() {
//
//                }
//
//                public void onFinish() {
//
//                }
//            });
//
//        }
//
//        // Start marker
//        MarkerOptions options = new MarkerOptions();
//        options.position(start);
//        options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
//        gMap.addMarker(options);
//
//        // End marker
//        options = new MarkerOptions();
//        options.position(end);
//        options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
//        gMap.addMarker(options);
//        gMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
//            @Override
//            public boolean onMarkerClick(Marker marker) {
//                return true;
//            }
//        });
//
//    }
//
//    @Override
//    public void onRoutingCancelled() {
//        Log.i(LOG_TAG, "Routing was cancelled.");
//    }
//
//    void showMarker(LatLng currentLocation, String markerTitle) {
//
//
//        MarkerOptions markerOptions = new MarkerOptions();
//        markerOptions.position(currentLocation);
//        markerOptions.title(markerTitle);
//
//
//        gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 16.0f));
//        gMap.getMaxZoomLevel();
//        ;
//    }
//
//    @Override
//    public void onClick(View v) {
//        switch (v.getId()) {
//            case R.id.getDirections:
//                double lat = Double.valueOf(preferenceUtils.getDoubleFromPreference(PreferenceUtils.LATTITUDE2, 0.0));
//                double lng = Double.valueOf(preferenceUtils.getDoubleFromPreference(PreferenceUtils.LONGITUDE2, 0.0));
//                double lat2= Double.valueOf(preferenceUtils.getDoubleFromPreference(PreferenceUtils.LATTITUDE2, 0.0));
//                double lng2 = Double.valueOf(preferenceUtils.getDoubleFromPreference(PreferenceUtils.LONGITUDE2, 0.0));
//
//                start  = new LatLng(lat,lng);
//                end  =  new LatLng(lat2,lng2);
//                if (start == null || end == null) {
//                    if (start == null) {
//                        Toast.makeText(this, "Please choose a starting point.", Toast.LENGTH_SHORT).show();
//                    }
//                    if (end == null) {
//                        Toast.makeText(this, "Please choose a destination.", Toast.LENGTH_SHORT).show();
//
//                    }
//                } else {
//                 /*   Intent gotofullscreen = new Intent(getApplicationContext(), FullScreenDirections.class);
//                    gotofullscreen.putExtra("startingpoint", string_start);
//                    gotofullscreen.putExtra("destinationpoint", string_dest);
//                    startActivity(gotofullscreen);*/
//                }
//                break;
//
//
//            case R.id.directions_car:
//                double latt = Double.valueOf(preferenceUtils.getDoubleFromPreference(PreferenceUtils.LATTITUDE2, 0.0));
//                double lngg = Double.valueOf(preferenceUtils.getDoubleFromPreference(PreferenceUtils.LONGITUDE2, 0.0));
//                double latt2= Double.valueOf(preferenceUtils.getDoubleFromPreference(PreferenceUtils.LATTITUDE2, 0.0));
//                double lngg2 = Double.valueOf(preferenceUtils.getDoubleFromPreference(PreferenceUtils.LONGITUDE2, 0.0));
//
//                start  = new LatLng(latt,lngg);
//                end  =  new LatLng(latt2,lngg2);
//                if (start == null || end == null) {
//                    if (start == null) {
//                        Toast.makeText(this, "Please choose a starting point.", Toast.LENGTH_SHORT).show();
//                    }
//                    if (end == null) {
//                        Toast.makeText(this, "Please choose a destination.", Toast.LENGTH_SHORT).show();
//
//                    }
//                } else {
//                    directionCar.setSelected(true);
//                  showLoader();
//                    directionCar.setBackgroundResource(R.mipmap.ic_launcher);
//                    directionWalk.setBackgroundResource(R.mipmap.ic_launcher);
//                    directiontransist.setBackgroundResource(R.mipmap.ic_launcher);
//
//                    directiontransist.setSelected(false);
//                    directionCar.setSelected(true);
//                    directionWalk.setSelected(false);
//                    Routing routing = new Routing.Builder()
//                            .travelMode(AbstractRouting.TravelMode.DRIVING)
//                            .withListener(new RoutingListener() {
//
//
//                                @Override
//                                public void onRoutingFailure(com.tbs.brothersgas.brothersgas.DirectionActivitys.RouteException e) {
//                                    hideLoader();
//                                }
//
//                                @Override
//                                public void onRoutingStart() {
//
//                                }
//
//                                @Override
//                                public void onRoutingSuccess(List<Route> route, int shortestRouteIndex) {
//                                   hideLoader();
//
//
//                                    if (polylines.size() > 0) {
//                                        for (Polyline poly : polylines) {
//                                            poly.remove();
//                                        }
//                                    }
//
//                                    polylines = new ArrayList<>();
//                                    //add route(s) to the gps.
//                                    for (int i = 0; i < route.size(); i++) {
//
//                                        //In case of more than 5 alternative routes
//                                        int colorIndex = i % COLORS.length;
//
//                                        PolylineOptions polyOptions = new PolylineOptions();
//                                        polyOptions.color(getResources().getColor(COLORS[colorIndex]));
//                                        polyOptions.width(10 + i * 3);
//                                        polyOptions.addAll(route.get(i).getPoints());
//                                        Polyline polyline = gMap.addPolyline(polyOptions);
//                                        polylines.add(polyline);
//
//                                        Log.d("Time", String.valueOf(route.get(0).getDurationValue()));
//
//                                        int totalSeconds = route.get(0).getDurationValue();
//                                        float totaldistance = route.get(0).getDistanceValue();
//                                        float distanceString = totaldistance / 1000;
//
//                                        hours = totalSeconds / 3600;
//                                        minutes = (totalSeconds % 3600) / 60;
//                                        seconds = totalSeconds % 60;
//                                        String timer;
//                                        if (hours != 0) {
//                                            timer = hours + "hour" + " " + minutes + " " + "mins";
//                                        } else {
//                                            timer = minutes + " " + "mins";
//                                        }
//                                        Log.d("TIMER", timer);
//                                        String timeString = String.format("%02d:%02d:%02d", hours, minutes, seconds);
//                                        tvTime.setText("Driving Time:" + timer);
//                                        tvDistance.setText("Distance:" + distanceString + " " + "KM");
//
//
//                                    }
//
//                                    // Start marker
//                                    MarkerOptions options = new MarkerOptions();
//                                    options.position(start);
//                                    options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
//                                    gMap.addMarker(options);
//
//                                    // End marker
//                                    options = new MarkerOptions();
//                                    options.position(end);
//                                    options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
//                                    gMap.addMarker(options);
//                                    gMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
//                                        @Override
//                                        public boolean onMarkerClick(Marker marker) {
//                                            return true;
//                                        }
//                                    });
//                                    LatLngBounds.Builder builder = new LatLngBounds.Builder();
//                                    builder.include(start);
//                                    builder.include(end);
//                                    LatLngBounds bounds = builder.build();
//
//                                    CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 150);
//                                    gMap.animateCamera(cu, new GoogleMap.CancelableCallback() {
//                                        public void onCancel() {
//                                        }
//
//                                        public void onFinish() {
//
//                                        }
//                                    });
//
//                                }
//
//                                @Override
//                                public void onRoutingCancelled() {
//
//                                }
//                            })
//                            .alternativeRoutes(false)
//                            .waypoints(start, end)
//                            .build();
//                    routing.execute();
//                }
//                break;
//            case R.id.directions_transist:
//                 lat = Double.valueOf(preferenceUtils.getDoubleFromPreference(PreferenceUtils.LATTITUDE2, 0.0));
//                 lng = Double.valueOf(preferenceUtils.getDoubleFromPreference(PreferenceUtils.LONGITUDE2, 0.0));
//                 lat2= Double.valueOf(preferenceUtils.getDoubleFromPreference(PreferenceUtils.LATTITUDE2, 0.0));
//                 lng2 = Double.valueOf(preferenceUtils.getDoubleFromPreference(PreferenceUtils.LONGITUDE2, 0.0));
//
//                start  = new LatLng(lat,lng);
//                end  =  new LatLng(lat2,lng2);
//                if (start == null || end == null) {
//                    if (start == null) {
//
//                        Toast.makeText(this, "Please choose a starting point.", Toast.LENGTH_SHORT).show();
//
//                    }
//                    if (end == null) {
//
//                        Toast.makeText(this, "Please choose a destination.", Toast.LENGTH_SHORT).show();
//
//                    }
//                } else {
//                    directiontransist.setSelected(true);
//                  showLoader();
//                    directionCar.setBackgroundResource(R.mipmap.ic_launcher);
//                    directionWalk.setBackgroundResource(R.mipmap.ic_launcher);
//                    directiontransist.setBackgroundResource(R.mipmap.ic_launcher);
//                    Routing routing1 = new Routing.Builder()
//                            .travelMode(AbstractRouting.TravelMode.TRANSIT)
//                            .withListener(new RoutingListener() {
//
//                                @Override
//                                public void onRoutingFailure(com.tbs.brothersgas.brothersgas.DirectionActivitys.RouteException e) {
//                                    hideLoader();
//                                }
//
//                                @Override
//                                public void onRoutingStart() {
//
//                                }
//
//                                @Override
//                                public void onRoutingSuccess(List<Route> route, int shortestRouteIndex) {
//                                   hideLoader();
//                                    if (polylines.size() > 0) {
//                                        for (Polyline poly : polylines) {
//                                            poly.remove();
//                                        }
//                                    }
//
//                                    polylines = new ArrayList<>();
//                                    //add route(s) to the gps.
//                                    for (int i = 0; i < route.size(); i++) {
//
//                                        //In case of more than 5 alternative routes
//                                        int colorIndex = i % COLORS.length;
//
//                                        PolylineOptions polyOptions = new PolylineOptions();
//                                        polyOptions.color(getResources().getColor(COLORS[colorIndex]));
//                                        polyOptions.width(10 + i * 3);
//                                        polyOptions.addAll(route.get(i).getPoints());
//                                        Polyline polyline = gMap.addPolyline(polyOptions);
//                                        polylines.add(polyline);
////                                        Toast.makeText(getApplicationContext(), "Route " + (i + 1) + ": distance - " + route.get(i).getDistanceValue() + ": duration - " + route.get(i).getDurationValue(), Toast.LENGTH_SHORT).show();
//                                        int totalSeconds = route.get(0).getDurationValue();
//                                        float totaldistance = route.get(0).getDistanceValue();
//                                        float distanceString = totaldistance / 1000;
//
//                                        hours = totalSeconds / 3600;
//                                        minutes = (totalSeconds % 3600) / 60;
//                                        seconds = totalSeconds % 60;
//                                        String timer;
//                                        if (hours != 0) {
//                                            timer = hours + "hour" + " " + minutes + " " + "mins";
//                                        } else {
//                                            timer = minutes + " " + "mins";
//                                        }
//                                        Log.d("TIMER", timer);
//                                        String timeString = String.format("%02d:%02d:%02d", hours, minutes, seconds);
//                                        tvTime.setText("Driving Time:" + timer);
//                                        tvDistance.setText("Distance:" + distanceString + " " + "KM");
//
//                                    }
//
//                                    // Start marker
//                                    MarkerOptions options = new MarkerOptions();
//                                    options.position(start);
//                                    options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
//                                    gMap.addMarker(options);
//
//                                    // End marker
//                                    options = new MarkerOptions();
//                                    options.position(end);
//                                    options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
//                                    gMap.addMarker(options);
//
//                                    gMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
//                                        @Override
//                                        public boolean onMarkerClick(Marker marker) {
//                                            return true;
//                                        }
//                                    });
//                                    LatLngBounds.Builder builder = new LatLngBounds.Builder();
//                                    builder.include(start);
//                                    builder.include(end);
//                                    LatLngBounds bounds = builder.build();
//
//                                    CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 150);
//                                    gMap.animateCamera(cu, new GoogleMap.CancelableCallback() {
//                                        public void onCancel() {
//                                        }
//
//                                        public void onFinish() {
//
//                                        }
//                                    });
//
//
//                                }
//
//                                @Override
//                                public void onRoutingCancelled() {
//
//                                }
//                            })
//                            .alternativeRoutes(false)
//                            .waypoints(start, end)
//                            .build();
//                    routing1.execute();
//                }
//                break;
//
//            case R.id.directions_walk:
//                if (start == null || end == null) {
//                    if (start == null) {
//
//                        Toast.makeText(this, "Please choose a starting point.", Toast.LENGTH_SHORT).show();
//
//                    }
//                    if (end == null) {
//
//                        Toast.makeText(this, "Please choose a destination.", Toast.LENGTH_SHORT).show();
//
//                    }
//                } else {
//                    showLoader();
//                    directionWalk.setSelected(true);
//                    directionCar.setBackgroundResource(R.mipmap.ic_launcher);
//                    directionWalk.setBackgroundResource(R.mipmap.ic_launcher);
//                    directiontransist.setBackgroundResource(R.mipmap.ic_launcher);
//                    Routing routing3 = new Routing.Builder()
//                            .travelMode(AbstractRouting.TravelMode.WALKING)
//                            .withListener(new RoutingListener() {
//
//                                @Override
//                                public void onRoutingFailure(com.tbs.brothersgas.brothersgas.DirectionActivitys.RouteException e) {
//                                    hideLoader();
//                                }
//
//                                @Override
//                                public void onRoutingStart() {
//
//                                }
//
//                                @Override
//                                public void onRoutingSuccess(List<Route> route, int shortestRouteIndex) {
//                                  hideLoader();
//
//                                    if (polylines.size() > 0) {
//                                        for (Polyline poly : polylines) {
//                                            poly.remove();
//                                        }
//                                    }
//
//                                    polylines = new ArrayList<>();
//                                    //add route(s) to the gps.
//                                    for (int i = 0; i < route.size(); i++) {
//
//                                        //In case of more than 5 alternative routes
//                                        int colorIndex = i % COLORS.length;
//
//                                        PolylineOptions polyOptions = new PolylineOptions();
//                                        polyOptions.color(getResources().getColor(COLORS[colorIndex]));
//                                        polyOptions.width(10 + i * 3);
//                                        polyOptions.addAll(route.get(i).getPoints());
//                                        Polyline polyline = gMap.addPolyline(polyOptions);
//                                        polylines.add(polyline);
//                                        int totalSeconds = route.get(0).getDurationValue();
//                                        float totaldistance = route.get(0).getDistanceValue();
//                                        float distanceString = totaldistance / 1000;
//
//                                        hours = totalSeconds / 3600;
//                                        minutes = (totalSeconds % 3600) / 60;
//                                        seconds = totalSeconds % 60;
//                                        String timer;
//                                        if (hours != 0) {
//                                            timer = hours + "hour" + " " + minutes + " " + "mins";
//                                        } else {
//                                            timer = minutes + " " + "mins";
//                                        }
//                                        Log.d("TIMER", timer);
//                                        String timeString = String.format("%02d:%02d:%02d", hours, minutes, seconds);
//                                        tvTime.setText("Driving Time:" + timer);
//                                        tvDistance.setText("Distance:" + distanceString + " " + "KM");
//
//                                    }
//
//                                    // Start marker
//                                    MarkerOptions options = new MarkerOptions();
//                                    options.position(start);
//                                    options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
//                                    gMap.addMarker(options);
//
//                                    // End marker
//                                    options = new MarkerOptions();
//                                    options.position(end);
//                                    options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
//                                    gMap.addMarker(options);
//
//                                    gMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
//                                        @Override
//                                        public boolean onMarkerClick(Marker marker) {
//                                            return true;
//                                        }
//                                    });
//                                    LatLngBounds.Builder builder = new LatLngBounds.Builder();
//                                    builder.include(start);
//                                    builder.include(end);
//                                    LatLngBounds bounds = builder.build();
//
//                                    CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 150);
//                                    gMap.animateCamera(cu, new GoogleMap.CancelableCallback() {
//                                        public void onCancel() {
//                                        }
//
//                                        public void onFinish() {
//
//                                        }
//                                    });
//
//
//                                }
//
//                                @Override
//                                public void onRoutingCancelled() {
//
//                                }
//                            })
//                            .alternativeRoutes(false)
//                            .waypoints(start, end)
//                            .build();
//                    routing3.execute();
//                }
//                break;
//
//
//        }
//    }
//
//    @Override
//    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
//
//    }
//
//    @Override
//    public void onConnected(@Nullable Bundle bundle) {
//
//
//    }
//
//    @Override
//    public void onConnectionSuspended(int i) {
//
//    }
//
//
//    @Override
//    public void initialize() {
//        LinearLayout llOrderHistory = (LinearLayout) getLayoutInflater().inflate(R.layout.gps_directions_manually_findroute, null);
//        llBody.addView(llOrderHistory, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
//        initializeControls();
//        toolbar.setNavigationIcon(R.drawable.back);
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//            }
//        });
//    }
//
//    @Override
//    public void initializeControls() {
//
//        directionCar = (Button) findViewById(R.id.directions_car);
//        directiontransist = (Button) findViewById(R.id.directions_transist);
//        directionWalk = (Button) findViewById(R.id.directions_walk);
//        ivGetCurrentLocation = (ImageView) findViewById(R.id.getcurrentlocation);
//        distanceRelative = (RelativeLayout) findViewById(R.id.distanceLayout);
//        directionCar.setOnClickListener(this);
//        directiontransist.setOnClickListener(this);
//
//        directionWalk.setOnClickListener(this);
//
//
//        StartautocompleteFragment = (PlaceAutocompleteFragment)
//                getFragmentManager().findFragmentById(R.id.startplace_autocomplete_fragment);
//        StartautocompleteFragment.setHint("Enter Origin");
//
//        StartautocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
//
//            @Override
//            public void onPlaceSelected(Place place) {
//                gMap.clear();
//
//
//                string_start = (String) place.getName();
//
//                start = getLocationFromAddress(mContext, string_start);
//
//                if (end != null) {
//                    route();
//                } else {
//                    Toast.makeText(mContext, "Select Destination", Toast.LENGTH_SHORT).show();
//
//                }
//                Log.i("Serached ", "Place: " + place.getName());
//            }
//
//            @Override
//            public void onError(Status status) {
//                // TODO: Handle the error.
//                Log.i("Error", "An error occurred: " + status);
//            }
//        });
//
//
//        PlaceAutocompleteFragment DestautocompleteFragment = (PlaceAutocompleteFragment)
//                getFragmentManager().findFragmentById(R.id.destinationplace_autocomplete_fragment);
//        DestautocompleteFragment.setHint("Enter Destination");
//
//        DestautocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
//            @Override
//            public void onPlaceSelected(Place place) {
//                gMap.clear();
//                string_dest = (String) place.getName();
//
//                end = getLocationFromAddress(mContext, string_dest);
//                if (start != null) {
//                    route();
//                } else {
//                    Toast.makeText(mContext, "Select Start Point", Toast.LENGTH_SHORT).show();
//
//                }
//                Log.i("Serached ", "Place: " + place.getName());
//            }
//
//            @Override
//            public void onError(Status status) {
//                // TODO: Handle the error.
//                Log.i("Error", "An error occurred: " + status);
//            }
//        });
//
//
//        tvTime = (TextView) findViewById(R.id.directions_time);
//        tvDistance = (TextView) findViewById(R.id.directions_distance);
//
//
//        Intent intent = getIntent();
//        if (getIntent().getExtras() != null) {
///*
//            string_start = intent.getStringExtra("starting_point");
//            string_dest = intent.getStringExtra("detination_point");
//            start = getLocationFromAddress(mContext, string_start);
//            end = getLocationFromAddress(mContext, string_dest);
//            route();*/
//        } else {
//
//            string_start = "";
//            string_dest = "";
//        }
//
//
//        getDirections = (Button) findViewById(R.id.getDirections);
//        getDirections.setOnClickListener(this);
//
//
//        Log.d("origin", String.valueOf(start));
//        Log.d("destination", String.valueOf(end));
//        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.directionsmap);
//        mapFragment.getMapAsync(this);
//
//        polylines = new ArrayList<>();
//
//
//        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
//
//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            // to handle the case where the user grants the permission. See the documentation
//            // for ActivityCompat#requestPermissions for more details.
//            return;
//        }
//        locationManager.requestLocationUpdates(
//                LocationManager.NETWORK_PROVIDER, 60000, 0,
//                new LocationListener() {
//                    @Override
//                    public void onLocationChanged(Location location) {
//                        Log.d("Directtopns: ", "Present LatLong: " + location.getLatitude() + " | " + location.getLongitude());
//                        lat = location.getLatitude();
//                        lng = location.getLongitude();
//
//                        LatLng currentlocation = new LatLng(lat, lng);
//                        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.ENGLISH);
//                        try {
//                            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
//
//                            if (addresses != null) {
//                                Address returnedAddress = addresses.get(0);
//                                final StringBuilder strReturnedAddress = new StringBuilder();
//                                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
//                                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
//                                }
//                                currentaddress = strReturnedAddress.toString();
//                                Log.d("CURRENTADRESS", currentaddress);
//                                ivGetCurrentLocation.setOnClickListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View v) {
//
//                                        start = new LatLng(lat, lng);
//                                        string_start = currentaddress;
//
//                                        StartautocompleteFragment.setText("Your Location");
//                                        CameraUpdate center = CameraUpdateFactory.newLatLng(start);
//                                        CameraUpdate zoom = CameraUpdateFactory.zoomTo(16);
//
//                                        gMap.moveCamera(center);
//                                        gMap.animateCamera(zoom);
//                                        if (start != null || end == null) {
//                                            route();
//                                        }
//
//                                    }
//                                });
//
//                                CameraUpdate center = CameraUpdateFactory.newLatLng(currentlocation);
//                                CameraUpdate zoom = CameraUpdateFactory.zoomTo(13);
//
//                                gMap.moveCamera(center);
//                                gMap.animateCamera(zoom);
//
//                                Log.d("ADRESS", strReturnedAddress.toString());
//                            } else {
//
//                                Log.d("ADRESS", "No Address returned!");
//                            }
//                        } catch (IOException e) {
//                            // TODO Auto-generated catch block
//                            e.printStackTrace();
//                            Toast.makeText(getApplicationContext(), "Sorry!!Try Again", Toast.LENGTH_SHORT).show();
//                            Log.d("ADRESS", "Canont get Address!");
//
//                        }
//
//
//                    }
//
//                    @Override
//                    public void onStatusChanged(String provider, int status, Bundle extras) {
//
//                    }
//
//                    @Override
//                    public void onProviderEnabled(String provider) {
//
//                    }
//
//                    @Override
//                    public void onProviderDisabled(String provider) {
//
//                    }
//                });
//
//
//        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
//                3000, 0, new LocationListener() {
//                    @Override
//                    public void onLocationChanged(Location location) {
//                        lati = location.getLatitude();
//                        longi = location.getLongitude();
//                        currentLocation = new LatLng(lati, longi);
//                    }
//
//                    @Override
//                    public void onStatusChanged(String provider, int status, Bundle extras) {
//
//                    }
//
//                    @Override
//                    public void onProviderEnabled(String provider) {
//
//                    }
//
//                    @Override
//                    public void onProviderDisabled(String provider) {
//
//                    }
//                });
//
//    }
//}
