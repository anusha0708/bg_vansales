package com.tbs.brothersgas.haadhir.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tbs.brothersgas.haadhir.Activitys.BaseActivity;
import com.tbs.brothersgas.haadhir.Activitys.SkipActivity;
import com.tbs.brothersgas.haadhir.Model.ReasonDO;
import com.tbs.brothersgas.haadhir.R;

import java.util.ArrayList;

public class SingleReasonListAdapter extends RecyclerView.Adapter<SingleReasonListAdapter.SingleViewHolder> {

    private Context context;
    private ArrayList<ReasonDO> reasonDos;
    private int checkedPosition = -1;
    private int type;
    public boolean isClickable = true;

    public SingleReasonListAdapter(int Type, Context context, ArrayList<ReasonDO> reasonDOs) {
        this.context = context;
        this.reasonDos = reasonDOs;
        this.type = Type;

    }

    public void setreasonDos(ArrayList<ReasonDO> reasonDos) {
        this.reasonDos = new ArrayList<>();
        this.reasonDos = reasonDos;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public SingleViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.reason_item_cell, viewGroup, false);
        return new SingleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SingleViewHolder singleViewHolder, int position) {
        singleViewHolder.bind(reasonDos.get(position));
    }

    @Override
    public int getItemCount() {
        return reasonDos.size();
    }

    class SingleViewHolder extends RecyclerView.ViewHolder {

        private TextView textView;
        private ImageView imageView;

        SingleViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.tvName);
            imageView = itemView.findViewById(R.id.ivDoneTick);
        }

        void bind(final ReasonDO employee) {
            if (checkedPosition == -1) {
                imageView.setVisibility(View.GONE);
            } else {
                if (checkedPosition == getAdapterPosition()) {
                    imageView.setVisibility(View.VISIBLE);
                } else {
                    imageView.setVisibility(View.GONE);
                }
            }
            textView.setText(employee.reason);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (isClickable == true) {
                        if (type == 5) {

                            imageView.setVisibility(View.VISIBLE);
                            if (checkedPosition != getAdapterPosition()) {
                                notifyItemChanged(checkedPosition);
                                checkedPosition = getAdapterPosition();
                                ((SkipActivity) context).updateReqApprButton("" + reasonDos.get(checkedPosition).reason);

                            }
                        }

                    } else {
                        ((BaseActivity) context).showToast("Please process the order..!");

                    }

                }
            });
        }
    }

    public ReasonDO getSelected() {
        if (checkedPosition != -1) {
            return reasonDos.get(checkedPosition);
        }
        return null;
    }
}