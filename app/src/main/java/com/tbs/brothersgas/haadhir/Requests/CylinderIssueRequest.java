package com.tbs.brothersgas.haadhir.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.brothersgas.haadhir.Activitys.BaseActivity;
import com.tbs.brothersgas.haadhir.Activitys.OnResultListener;
import com.tbs.brothersgas.haadhir.Activitys.SignatureActivity;
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryDO;
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryMainDO;
import com.tbs.brothersgas.haadhir.Model.CylinderIssueDO;
import com.tbs.brothersgas.haadhir.Model.CylinderIssueMainDO;
import com.tbs.brothersgas.haadhir.common.AppConstants;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;
import com.tbs.brothersgas.haadhir.utils.ProgressTask;
import com.tbs.brothersgas.haadhir.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class CylinderIssueRequest extends AsyncTask<String, Void, Boolean> {

    private CylinderIssueMainDO cylinderIssueMainDO;
    private Context mContext;
    private String id;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;
    CylinderIssueDO cylinderIssueDO;

    public CylinderIssueRequest(String id, Context mContext) {

        this.mContext = mContext;
        this.id = id;
    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public boolean runRequest() {
        // System.out.println("CUSTOMER ID " + customerId);
        String NAMESPACE = "http://www.adonix.com/WSS";
        String METHOD_NAME = "run";
        String SOAP_ACTION = "CAdxWebServiceXmlCC";
        preferenceUtils = new PreferenceUtils(mContext);
        username = preferenceUtils.getStringFromPreference(PreferenceUtils.A_USER_NAME, "");
        password = preferenceUtils.getStringFromPreference(PreferenceUtils.A_PASSWORD, "");
        ip = preferenceUtils.getStringFromPreference(PreferenceUtils.IP_ADDRESS, "");
        port = preferenceUtils.getStringFromPreference(PreferenceUtils.PORT, "");
        pool = preferenceUtils.getStringFromPreference(PreferenceUtils.ALIAS, "");
        String URL = "http://" + ip + ":" + port + "/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC";

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        // Set all input params                         X10CSAINV3
        request.addProperty("publicName", WebServiceConstants.CYLINDER_ISSUE_DETAILS);
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("I_XSDHNUM", id);

        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        request.addProperty("inputXml", jsonObject.toString());

        SoapObject callcontext = new SoapObject("", "callContext");
        // Set all input params
        callcontext.addProperty("codeLang", "ENG");
        callcontext.addProperty("poolAlias", pool);
        callcontext.addProperty("poolId", "");
        callcontext.addProperty("codeUser", username);
        callcontext.addProperty("password", password);
        callcontext.addProperty("requestConfig", "adxwss.trace.on=off");

        request.addSoapObject(callcontext);


        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.setOutputSoapObject(request);

        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
        androidHttpTransport.debug = true;

        try {
            List<HeaderProperty> headerList = new ArrayList<HeaderProperty>();
            headerList.add(new HeaderProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode((username + ":" + password).getBytes())));


            androidHttpTransport.call(SOAP_ACTION, envelope, headerList);


            SoapObject response = (SoapObject) envelope.getResponse();
            String resultXML = (String) response.getProperty("resultXml");
            if (resultXML != null && resultXML.length() > 0) {
                return parseXML(resultXML);
            } else {
                return false;
            }
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }


    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            cylinderIssueMainDO = new CylinderIssueMainDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        cylinderIssueMainDO.cylinderIssueDOS = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        cylinderIssueDO = new CylinderIssueDO();

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {


                        if (attribute.equalsIgnoreCase("I_XSDHNUM")) {
                            if (text.length() > 0) {
                                cylinderIssueMainDO.shipmentNumber = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITE")) {
                            cylinderIssueMainDO.site = text;

                        } else if (attribute.equalsIgnoreCase("O_XSITEDES")) {
                            cylinderIssueMainDO.siteDescription = text;
                        } else if (attribute.equalsIgnoreCase("O_XSITADDLIG0")) {
                            cylinderIssueMainDO.siteAddress1 = text;
                        } else if (attribute.equalsIgnoreCase("O_XSITADDLIG1")) {
                            cylinderIssueMainDO.siteAddress2 = text;

                        } else if (attribute.equalsIgnoreCase("O_XSITADDLIG2")) {
                            cylinderIssueMainDO.siteAddress3 = text;

                        } else if (attribute.equalsIgnoreCase("O_XCRETIM")) {
                            if (text.length() > 0) {

                                cylinderIssueMainDO.createdTime = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_XCREUSR")) {
                            if (text.length() > 0) {
                                cylinderIssueMainDO.createUserID = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XCREUSRNAM")) {
                            if (text.length() > 0) {
                                cylinderIssueMainDO.createUserName = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XCREDAT")) {
                            cylinderIssueMainDO.createdDate = text;

                        } else if (attribute.equalsIgnoreCase("O_XBPCNUM")) {
                            cylinderIssueMainDO.customer = text;

                        } else if (attribute.equalsIgnoreCase("O_XBPCSHO")) {
                            cylinderIssueMainDO.customerDescription = text;
                        } else if (attribute.equalsIgnoreCase("O_XCUSCRY")) {
                            cylinderIssueMainDO.customercountryName = text;

                        } else if (attribute.equalsIgnoreCase("O_XBPADDLIG0")) {


                            cylinderIssueMainDO.customerStreet = text;
                        } else if (attribute.equalsIgnoreCase("O_XBPADDLIG1")) {


                            cylinderIssueMainDO.customerLandMark = text;
                        } else if (attribute.equalsIgnoreCase("O_XCPYDES")) {


                            cylinderIssueMainDO.company = text;
                        } else if (attribute.equalsIgnoreCase("O_XCPYNAM")) {


                            cylinderIssueMainDO.companyCode = text;
                        } else if (attribute.equalsIgnoreCase("O_XBPADDLIG2")) {


                            cylinderIssueMainDO.customerTown = text;
                        } else if (attribute.equalsIgnoreCase("O_XCUSCTY")) {


                            cylinderIssueMainDO.customerCity = text;
                        } else if (attribute.equalsIgnoreCase("O_XCUSPOS")) {


                            cylinderIssueMainDO.customerPostalCode = text;
                        } else if (attribute.equalsIgnoreCase("O_XCUSLD")) {
                            cylinderIssueMainDO.landLine = text;

                        } else if (attribute.equalsIgnoreCase("O_XCUSMOB")) {
                            cylinderIssueMainDO.mobile = text;

                        } else if (attribute.equalsIgnoreCase("O_XCUSFAX")) {
                            cylinderIssueMainDO.fax = text;

                        } else if (attribute.equalsIgnoreCase("O_XCUSEML1")) {
                            cylinderIssueMainDO.email = text;
                        } else if (attribute.equalsIgnoreCase("O_XCUSEML2")) {
                            cylinderIssueMainDO.email2 = text;
                        } else if (attribute.equalsIgnoreCase("O_XCPYIMG")) {
                            if (text.length() > 0) {
                                cylinderIssueMainDO.logo = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITCRY")) {
                            if (text.length() > 0) {
                                cylinderIssueMainDO.siteCountry = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITCTY")) {
                            if (text.length() > 0) {
                                cylinderIssueMainDO.siteCity = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITPOS")) {
                            if (text.length() > 0) {
                                cylinderIssueMainDO.sitePostalCode = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITLND")) {
                            if (text.length() > 0) {
                                cylinderIssueMainDO.siteLandLine = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITMOB")) {
                            if (text.length() > 0) {
                                cylinderIssueMainDO.siteMobile = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITFAX")) {
                            if (text.length() > 0) {
                                cylinderIssueMainDO.siteFax = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITEML1")) {
                            if (text.length() > 0) {
                                cylinderIssueMainDO.siteEmail1 = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITEML2")) {
                            if (text.length() > 0) {
                                cylinderIssueMainDO.siteEmail2 = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSIGNATURE")) {
                            if (text.length() > 0) {
                                cylinderIssueMainDO.signature = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITWEB")) {
                            if (text.length() > 0) {
                                cylinderIssueMainDO.siteWebEmail = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITWEB")) {
                            cylinderIssueMainDO.webSite = text;
                        } else if (attribute.equalsIgnoreCase("O_XITM")) {
                            cylinderIssueDO.product = text;
                        } else if (attribute.equalsIgnoreCase("O_XITMDES")) {
                            cylinderIssueDO.productDescription = text;
                        } else if (attribute.equalsIgnoreCase("O_XOQTY")) {
                            cylinderIssueDO.orderedQuantity = Integer.parseInt(text);
                        } else if (attribute.equalsIgnoreCase("O_XOUNI")) {
                            cylinderIssueDO.orderedQuantityunit = text;
                        } else if (attribute.equalsIgnoreCase("O_XISQTY")) {
                            cylinderIssueDO.issuedQuantity = Integer.parseInt(text);
                        } else if (attribute.equalsIgnoreCase("O_XISUNI")) {
                            cylinderIssueDO.issuedQuantityunit = text;
                        } else if (attribute.equalsIgnoreCase("O_XRCVDQTY")) {
                            cylinderIssueDO.recievedQuantity = Integer.parseInt(text);
                        } else if (attribute.equalsIgnoreCase("O_XRCVDUN")) {
                            cylinderIssueDO.recievedQuantityunit = text;
                        } else if (attribute.equalsIgnoreCase("O_XBALQTY")) {
                            cylinderIssueDO.balanceQuantity = Integer.parseInt(text);
                        } else if (attribute.equalsIgnoreCase("O_XBALUN")) {
                            cylinderIssueDO.balanceQuantityunit = text;
                        }

                        text = "";


                    }


                    if (endTag.equalsIgnoreCase("GRP3")) {
                        //   customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        cylinderIssueMainDO.cylinderIssueDOS.add(cylinderIssueDO);
                    }

                } else if (eventType == XmlPullParser.TEXT) {

                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (mContext instanceof SignatureActivity) {
            ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Cylinder issuance Details...");
        } else {
            ((BaseActivity) mContext).showLoader();

        }
        // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        if (mContext instanceof SignatureActivity) {
            ProgressTask.getInstance().closeProgress();
        } else {
            ((BaseActivity) mContext).hideLoader();

        }
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, cylinderIssueMainDO);
        }
    }
}