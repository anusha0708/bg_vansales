package com.tbs.brothersgas.haadhir.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class ActiveDeliveryMainDO implements Serializable {
    public String referenceID = "";
    public String vehicleCode = "";
    public String tripNumber = "";

    public String paymentTerm = "";
    public String salesOrderNumber = "";
    public String signatureDate = "";
    public String signatureTime = "";
    public String LPONumber = "";
    public int productType=0;
    public String shipmentNumber = "";
    public String customer = "";
    public String deliveryType = "";
    public String customerDescription = "";
    public String countryName = "";
    public String webSite = "";
    public String landLine = "";
    public String email = "";
    public String fax = "";
    public String mobile = "";
    public String lattitude = "";
    public String longitude = "";
    public String companyDescription = "";
    public String companyCode = "";
    public String remarks = "";
    public String createUserID = "";
    public String createUserName = "";
    public int validateFlag = 0;
    public int deliveryCategory = 0;

    public int invoiceFlag = 0;
    public String invoiceNumber = "";
    public String startRouteTime = "";
    public String podTime = "";
    public String CaptureReturnsTime = "";
    public String arrivalTime = "";
    public String arraivalDate = "";
    public String departureTime = "";
    public String departureDate = "";
    public String deliveryDate = "";
    public String shipmentDate = "";
    public String createdDate = "";
    public String createdTime = "";
    public String timeCaptureStartRouteTime = "";
    public String timeCaptureStartRouteDate = "";
    public String logo = "";
    public String site = "";
    public String accountedDate = "";

    public String siteDescription = "";
    public String siteAddress1 = "";
    public String siteAddress2 = "";
    public String siteAddress3 = "";
    public String siteCountry = "";
    public String siteCity = "";
    public String sitePostalCode = "";
    public String siteLandLine = "";
    public String siteMobile = "";
    public String siteFax = "";
    public String siteEmail1 = "";
    public String siteEmail2 = "";
    public String siteWebEmail = "";
    public String customerStreet = "";
    public String customerLandMark = "";
    public String customerTown = "";
    public String customerCity = "";
    public String customerPostalCode = "";
    public String signature = "";
    public String deliveryEmail = "";
    public String invoiceEmail = "";
    public String paymentEmail = "";
    public String cylinderIssueEmail = "";
    public String capturedName = "";
    public String capturedNumber = "";
    public String startReading = "";
    public String endReading = "";
    public int meterType = 0;
    public int stockType = 0;

    public String startReading3 = "";
    public String endReading3 = "";
    public String startReading2 = "";
    public String endReading2 = "";
    public String startReading4 = "";
    public String endReading4 = "";
    public String startReading5 = "";
    public String endReading5 = "";
    public String startReading6 = "";
    public String endReading6 = "";
    public String startReading7 = "";
    public String endReading7 = "";
    public String startReading8 = "";
    public String endReading8 = "";
    public String startReading9 = "";
    public String endReading9 = "";
    public String startReading10 = "";
    public String endReading10 = "";
    public ArrayList<ActiveDeliveryDO> activeDeliveryDOS = new ArrayList<>();


    @Override
    public String toString() {
        return "ActiveDeliveryMainDO{" +
                "shipmentNumber='" + shipmentNumber + '\'' +
                ", customer='" + customer + '\'' +
                ", customerDescription='" + customerDescription + '\'' +
                ", countryName='" + countryName + '\'' +
                ", webSite='" + webSite + '\'' +
                ", landLine='" + landLine + '\'' +
                ", email='" + email + '\'' +
                ", fax='" + fax + '\'' +
                ", mobile='" + mobile + '\'' +
                ", lattitude='" + lattitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", startRouteTime='" + startRouteTime + '\'' +
                ", podTime='" + podTime + '\'' +
                ", CaptureReturnsTime='" + CaptureReturnsTime + '\'' +
                ", arrivalTime='" + arrivalTime + '\'' +
                ", arraivalDate='" + arraivalDate + '\'' +
                ", deliveryDate='" + deliveryDate + '\'' +
                ", timeCaptureStartRouteTime='" + timeCaptureStartRouteTime + '\'' +
                ", timeCaptureStartRouteDate='" + timeCaptureStartRouteDate + '\'' +
                ", activeDeliveryDOS=" + activeDeliveryDOS +
                '}';
    }
}
