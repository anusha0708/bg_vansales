package com.tbs.brothersgas.haadhir.Activitys

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import com.tbs.brothersgas.haadhir.Adapters.ScheduledProductsAdapter
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.ActiveDeliveryRequest
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util

//
class ShipmentDetailsActivity : BaseActivity() {
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView

    override protected fun onResume() {
        super.onResume()
    }

    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.shipment_details, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()
        disableMenuWithBackButton()
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            setResult(14, null)
            finish()
        }
    }

    override fun initializeControls() {
        tvScreenTitle.setText(R.string.shipment_details)
        recycleview = findViewById<View>(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        recycleview.setLayoutManager(androidx.recyclerview.widget.LinearLayoutManager(this))
        var idd = "" + preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
        var tvCustomerName = findViewById<View>(R.id.tvCustomerName) as TextView
        var tvReference = findViewById<View>(R.id.tvReference) as TextView
        tvCustomerName.setText("Customer : " + preferenceUtils.getStringFromPreference(PreferenceUtils.CUSTOMER_NAME, ""))
        var tvShipmentNumber = findViewById<View>(R.id.tvShipmentNumber) as TextView

        tvShipmentNumber.setText("Shipment : " + preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, ""))

        if (idd.length > 0) {
            if (Util.isNetworkAvailable(this)) {
                val driverListRequest = ActiveDeliveryRequest(idd, this@ShipmentDetailsActivity)
                driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                    hideLoader()
                    if (isError) {

                        Toast.makeText(this@ShipmentDetailsActivity, R.string.error_customer_list, Toast.LENGTH_SHORT).show()
                    } else {
                        var aMonth: String = ""
                        var ayear: String = ""
                        var aDate: String = ""
                        try {
                            var date = activeDeliveryDo.deliveryDate
                            aMonth = date.substring(4, 6)
                            ayear = date.substring(0, 4)
                            aDate = date.substring(Math.max(date.length - 2, 0))
                        } catch (e: Exception) {
                        }

                        tvReference.setText("Date : " + aDate + "-" + aMonth + "-" + ayear)

                        var loadStockAdapter = ScheduledProductsAdapter(this@ShipmentDetailsActivity, activeDeliveryDo.activeDeliveryDOS, "Shipments", "",0)
                        recycleview.setAdapter(loadStockAdapter)
                    }
                }
                driverListRequest.execute()

            } else {
                showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

            }


        } else {

        }
    }

}