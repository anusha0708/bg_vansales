package com.tbs.brothersgas.haadhir.background

import android.app.IntentService
import android.content.Context
import android.content.Intent
import android.widget.Toast
import com.google.gson.Gson

import com.tbs.brothersgas.haadhir.Model.CylinderIssueMainDO
import com.tbs.brothersgas.haadhir.pdfs.CylinderIssRecPdfBuilder

class CylinderPrintService : IntentService("CylinderPrintService") {
    internal var cylinderIssueMainDO: CylinderIssueMainDO = CylinderIssueMainDO()
    internal var context: Context = this
    internal var resutlString = ""

    override fun onHandleIntent(intent: Intent?) {
        Toast.makeText(context, "Started.......", Toast.LENGTH_LONG).show()
        resutlString = intent!!.getStringExtra("object")

        cylinderIssueMainDO = Gson().fromJson(resutlString, CylinderIssueMainDO::class.java)

        CylinderIssRecPdfBuilder.getBuilder(this).build(cylinderIssueMainDO, "AUTO")
    }

}
