package com.tbs.brothersgas.haadhir.decanting;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.brothersgas.haadhir.Activitys.BaseActivity;
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryDO;
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryMainDO;
import com.tbs.brothersgas.haadhir.common.AppConstants;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;
import com.tbs.brothersgas.haadhir.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class DecantingDetailsRequest extends AsyncTask<String, Void, Boolean> {

    private ActiveDeliveryMainDO activeDeliveryMainDO;
    private Context mContext;
    private String docNum;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;
    ActiveDeliveryDO activeDeliveryDO;

    public DecantingDetailsRequest(Context mContext, String  docNum) {

        this.mContext = mContext;
        this.docNum = docNum;
    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        public void onCompleted(boolean isError, ActiveDeliveryMainDO activeDeliveryMainDO);

    }

    public boolean runRequest() {
        // System.out.println("CUSTOMER ID " + customerId);
        String NAMESPACE = "http://www.adonix.com/WSS";
        String METHOD_NAME = "run";
        String SOAP_ACTION = "CAdxWebServiceXmlCC";
        preferenceUtils = new PreferenceUtils(mContext);
        username = preferenceUtils.getStringFromPreference(PreferenceUtils.A_USER_NAME, "");
        password = preferenceUtils.getStringFromPreference(PreferenceUtils.A_PASSWORD, "");
        ip = preferenceUtils.getStringFromPreference(PreferenceUtils.IP_ADDRESS, "");
        port = preferenceUtils.getStringFromPreference(PreferenceUtils.PORT, "");
        pool = preferenceUtils.getStringFromPreference(PreferenceUtils.ALIAS, "");
        String URL = "http://" + ip + ":" + port + "/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC";

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        // Set all input params                         X10CSAINV3
        request.addProperty("publicName", WebServiceConstants.DECANTING_DETAILS);
        JSONObject jsonObject = new JSONObject();
        try {
            String driverID = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "");
            jsonObject.put("I_YDOCNUM", docNum);
        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        request.addProperty("inputXml", jsonObject.toString());

        SoapObject callcontext = new SoapObject("", "callContext");
        // Set all input params
        callcontext.addProperty("codeLang", "ENG");
        callcontext.addProperty("poolAlias", pool);
        callcontext.addProperty("poolId", "");
        callcontext.addProperty("codeUser", username);
        callcontext.addProperty("password", password);
        callcontext.addProperty("requestConfig", "adxwss.trace.on=off");

        request.addSoapObject(callcontext);


        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.setOutputSoapObject(request);

        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
        androidHttpTransport.debug = true;

        try {
            List<HeaderProperty> headerList = new ArrayList<HeaderProperty>();
            headerList.add(new HeaderProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode((username + ":" + password).getBytes())));


            androidHttpTransport.call(SOAP_ACTION, envelope, headerList);


            SoapObject response = (SoapObject) envelope.getResponse();
            String resultXML = (String) response.getProperty("resultXml");
            if (resultXML != null && resultXML.length() > 0) {
                return parseXML(resultXML);
            } else {
                return false;
            }
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            activeDeliveryMainDO = new ActiveDeliveryMainDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        activeDeliveryMainDO.activeDeliveryDOS = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        activeDeliveryDO = new ActiveDeliveryDO();

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {


                      if (attribute.equalsIgnoreCase("O_YTFCY")) {
                            if (text.length() > 0) {

                                activeDeliveryMainDO.site = text;
                            }


                        } else if (attribute.equalsIgnoreCase("O_YDOCNUM")) {
                            if (text.length() > 0) {

                                activeDeliveryMainDO.shipmentNumber = text;
                            }


                        } else if (attribute.equalsIgnoreCase("O_YCREDATE")) {
                            if (text.length() > 0) {

                                activeDeliveryMainDO.createdDate = text;
                            }


                        } else if (attribute.equalsIgnoreCase("O_YACCDATE")) {
                            if (text.length() > 0) {

                                activeDeliveryMainDO.accountedDate = text;
                            }


                        } else if (attribute.equalsIgnoreCase("O_YBPRNAM")) {
                            if (text.length() > 0) {

                                activeDeliveryMainDO.customerDescription = text;
                            }


                        }
                      else if (attribute.equalsIgnoreCase("O_YBPR")) {
                            if (text.length() > 0) {

                                activeDeliveryMainDO.customer = text;
                            }


                        } else if (attribute.equalsIgnoreCase("O_YADR")) {
                            if (text.length() > 0) {

                                activeDeliveryMainDO.siteAddress1 = text;
                            }


                        }
                      else if (attribute.equalsIgnoreCase("O_YVEHCODE")) {
                          if (text.length() > 0) {

                              activeDeliveryMainDO.vehicleCode = text;
                          }


                      }
                      else if (attribute.equalsIgnoreCase("O_YTRIPNO")) {
                          if (text.length() > 0) {

                              activeDeliveryMainDO.tripNumber = text;
                          }


                      }
                      else if (attribute.equalsIgnoreCase("O_YLINENO")) {
                          if (text.length() > 0) {

                              activeDeliveryDO.linenumber = text;
                          }


                      } else if (attribute.equalsIgnoreCase("O_YITMREF")) {
                          if (text.length() > 0) {

                              activeDeliveryDO.product = text;
                          }


                      }
                      else if (attribute.equalsIgnoreCase("O_YITMDES")) {
                          if (text.length() > 0) {

                              activeDeliveryDO.productDescription = text;
                          }


                      } else if (attribute.equalsIgnoreCase("O_YQTY")) {
                          if (text.length() > 0) {

                              activeDeliveryDO.totalQuantity = Double.valueOf(text);
                          }


                      }
                      else if (attribute.equalsIgnoreCase("O_YVCRNUM")) {
                          if (text.length() > 0) {

                              activeDeliveryDO.issueNumber = text;
                          }


                      } else if (attribute.equalsIgnoreCase("O_YVCRTYP")) {
                          if (text.length() > 0) {

                              activeDeliveryDO.issueType = Integer.parseInt(text);
                          }


                      }
                      else if (attribute.equalsIgnoreCase("O_YSTKINOUT")) {
                          if (text.length() > 0) {

                              activeDeliveryMainDO.stockType = Integer.parseInt(text);
                          }


                      } else if (attribute.equalsIgnoreCase("O_YLOC")) {
                          if (text.length() > 0) {

                              activeDeliveryDO.location = text;
                          }


                      }
                      else if (attribute.equalsIgnoreCase("O_YLOCTYP")) {
                          if (text.length() > 0) {

                              activeDeliveryDO.locationType =text;
                          }


                      }else if (attribute.equalsIgnoreCase("O_YUOM")) {
                          if (text.length() > 0) {

                              activeDeliveryDO.unit = text;
                          }


                      }
                        text = "";


                    }


                    if (endTag.equalsIgnoreCase("GRP3")) {
                        //   customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        activeDeliveryMainDO.activeDeliveryDOS.add(activeDeliveryDO);
                        if (activeDeliveryDO.productType.length() > 0) {

                            activeDeliveryDO.productType = AppConstants.ShipmentListView;
                        }

                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {

        super.onPreExecute();
        ((BaseActivity) mContext).showLoader();

        // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        ((BaseActivity) mContext).hideLoader();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, activeDeliveryMainDO);
        }
    }
}