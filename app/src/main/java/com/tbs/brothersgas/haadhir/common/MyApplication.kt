package com.tbs.brothersgas.haadhir.common

import android.app.Application
import android.content.Context
import com.tbs.brothersgas.haadhir.utils.AppPrefs


/*
 * Created by developer on 22/2/19.
 */
class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        AppPrefs.initPrefs(this);
//        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "font/montserrat_regular.ttf");

    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        //Multidex.install(this)

    }
}