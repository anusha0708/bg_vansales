package com.tbs.brothersgas.haadhir.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.brothersgas.haadhir.Activitys.BaseActivity;
import com.tbs.brothersgas.haadhir.Model.BankDO;
import com.tbs.brothersgas.haadhir.Model.BankMainDO;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;
import com.tbs.brothersgas.haadhir.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class BankListRequest extends AsyncTask<String, Void, Boolean> {

    private BankMainDO bankMainDO;
    private BankDO bankDO;
    private Context mContext;
    private String site;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;
    String customer;


    public BankListRequest(String typE, Context mContext) {

        this.mContext = mContext;
        this.site = typE;


    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        public void onCompleted(boolean isError, BankMainDO bankMainDO);

    }

    public boolean runRequest() {
        // System.out.println("CUSTOMER ID " + customerId);
        String NAMESPACE = "http://www.adonix.com/WSS";
        String METHOD_NAME = "run";
        String SOAP_ACTION = "CAdxWebServiceXmlCC";
//        String URL = "http://183.82.9.23:8124/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC";
        preferenceUtils = new PreferenceUtils(mContext);
        username = preferenceUtils.getStringFromPreference(PreferenceUtils.A_USER_NAME, "");
        password = preferenceUtils.getStringFromPreference(PreferenceUtils.A_PASSWORD, "");
        ip = preferenceUtils.getStringFromPreference(PreferenceUtils.IP_ADDRESS, "");
        port = preferenceUtils.getStringFromPreference(PreferenceUtils.PORT, "");
        pool = preferenceUtils.getStringFromPreference(PreferenceUtils.ALIAS, "");

        String URL = "http://"+ip+":"+port+"/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC";

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        // Set all input params
        request.addProperty("publicName", WebServiceConstants.BANK_LIST);
        JSONObject jsonObject = new JSONObject();

        try {

//            jsonObject.put("I_YBPC", "ZA002");

            jsonObject.put("I_YFCY", site);

        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        request.addProperty("inputXml", jsonObject.toString());

        SoapObject callcontext = new SoapObject("", "callContext");
        // Set all input params
        callcontext.addProperty("codeLang", "ENG");
        callcontext.addProperty("poolAlias", pool);
        callcontext.addProperty("poolId", "");
        callcontext.addProperty("codeUser", username);
        callcontext.addProperty("password", password);
        callcontext.addProperty("requestConfig", "adxwss.trace.on=off");

        request.addSoapObject(callcontext);


        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.setOutputSoapObject(request);

        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
        androidHttpTransport.debug = true;

        try {
            List<HeaderProperty> headerList = new ArrayList<HeaderProperty>();
            headerList.add(new HeaderProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode((username + ":" + password).getBytes())));


            androidHttpTransport.call(SOAP_ACTION, envelope, headerList);


            SoapObject response = (SoapObject) envelope.getResponse();
            String resultXML = (String) response.getProperty("resultXml");
            if (resultXML != null && resultXML.length() > 0) {
                return parseXML(resultXML);
            } else {
                return false;
            }
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            bankMainDO = new BankMainDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {
                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        bankMainDO.bankDOS = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        bankDO = new BankDO();

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {
                        if (attribute.equalsIgnoreCase("I_YBPC")) {
                            if(text.length()>0){

                                bankMainDO.site = text;
                            }


                        }
                        else if (attribute.equalsIgnoreCase("O_YBAN")) {
                            if(text.length()>0){

                                bankDO.bankId = text;
                            }

                        }else if (attribute.equalsIgnoreCase("O_YBANDES")) {
                            if(text.length()>0){

                                bankDO.bankName = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YCUR")) {
                            if(text.length()>0){

                                bankDO.currency = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YBANACC")) {
                            if(text.length()>0){

                                bankDO.bankAccount = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YCPY")) {
                            if(text.length()>0){

                                bankDO.company = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YCPYDES")) {
                            if(text.length()>0){

                                bankDO.companyDescription = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YABNCRY")) {
                            if(text.length()>0){

                                bankDO.countryId = text;
                            }

                        }

                    }

                    if (endTag.equalsIgnoreCase("GRP")) {
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        bankMainDO.bankDOS.add(bankDO);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    if(xpp.getText().length()>0){

                        text = xpp.getText();
                    }else {
                        text="";
                    }
                }


                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        ((BaseActivity)mContext).showLoader();
       // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        ((BaseActivity)mContext).hideLoader();

      //  ProgressTask.getInstance().closeProgress();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, bankMainDO);
        }
    }
}