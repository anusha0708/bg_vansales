package com.tbs.brothersgas.haadhir.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tbs.brothersgas.haadhir.Activitys.BaseActivity;
import com.tbs.brothersgas.haadhir.Activitys.MasterLocActivty;
import com.tbs.brothersgas.haadhir.Activitys.PODActivity;
import com.tbs.brothersgas.haadhir.Activitys.PODMRLocationsActivity;
import com.tbs.brothersgas.haadhir.Model.ProductDO;
import com.tbs.brothersgas.haadhir.R;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.MyViewHolder>  {
    Double distance = 0.0;
    Boolean podFlag;

    private List<ProductDO> productDOS;
    private Context context;

    public void refreshAdapter(@NotNull ArrayList<ProductDO> productDOs) {
        this.productDOS = productDOs;
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvProductName, tvProductId;
        private LinearLayout llDetails;

        public MyViewHolder(View view) {
            super(view);
            llDetails     = (LinearLayout) view.findViewById(R.id.llDetails);
            tvProductName = (TextView) view.findViewById(R.id.tvProductName);
            tvProductId   = (TextView) view.findViewById(R.id.tvProductId);



        }
    }


    public LocationAdapter(Context context, List<ProductDO> productDOS,Double distancE,Boolean b) {
        this.context = context;
        this.productDOS = productDOS;
        this.distance = distancE;
        this.podFlag = b;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_data, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        final ProductDO productDO = productDOS.get(position);
        holder.tvProductName.setText(productDO.productName);
        holder.tvProductId.setText("" + productDO.productId);

        holder.llDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(context instanceof PODMRLocationsActivity){
                    if(podFlag==true){
                        ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.MR_CODE, productDO.productId);
                        Intent intent = new Intent(context, PODActivity.class);
                        intent.putExtra("DISTANCE", distance);
                        ((PODMRLocationsActivity) context).startActivityForResult(intent, 12);
                    }  else   {
                        ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.MR_CODE, productDO.productId);
                        ((PODMRLocationsActivity) context).finish();
                    }

                }

                else {
                    Intent intent = new Intent(context, MasterLocActivty.class);
                    intent.putExtra("CODE", productDO.productId);
                    intent.putExtra("LATTITUDE", productDO.lattitude);
                    intent.putExtra("LONGITUDE", productDO.longitude);
                    context.startActivity(intent);
                }



            }
        });


    }

    @Override
    public int getItemCount() {
        return productDOS.size();
    }

}
