package com.tbs.brothersgas.haadhir.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.tbs.brothersgas.haadhir.Activitys.ActiveDeliveryActivity;
import com.tbs.brothersgas.haadhir.Activitys.BaseActivity;
import com.tbs.brothersgas.haadhir.Activitys.VehicleRoutingList;
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryDO;
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryMainDO;
import com.tbs.brothersgas.haadhir.Model.PickUpDo;
import com.tbs.brothersgas.haadhir.R;
import com.tbs.brothersgas.haadhir.Requests.ActiveDeliveryRequest;
import com.tbs.brothersgas.haadhir.common.AppConstants;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;
import com.tbs.brothersgas.haadhir.utils.Util;

import java.util.ArrayList;

public class VehicleRouteAdapter extends RecyclerView.Adapter<VehicleRouteAdapter.MyViewHolder> implements LocationListener {

    private ArrayList<PickUpDo> productDOS;
    private Context context;
    Double lat, lng;
    double dist;
    String arrivalTime2, departureTime2;
    String aMonth, ayear, aDate, dMonth, dyear, dDate;
    private ArrayList<ActiveDeliveryDO> activeDeliveryDOs;

    @Override
    public void onLocationChanged(Location location) {
        lat = location.getLatitude();
        lng = location.getLongitude();
        ((BaseActivity) context).preferenceUtils = new PreferenceUtils(context);


    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName, tvCount, tvTop, tvAddress, tvTime, tvDistance, tvShipmentNumber;
        private LinearLayout llDetails;
        private ImageView ivLocation, ivDone;

        public MyViewHolder(View view) {
            super(view);
            llDetails = (LinearLayout) view.findViewById(R.id.llDetails);
            tvName = (TextView) view.findViewById(R.id.tvName);
            tvCount = (TextView) view.findViewById(R.id.tvCount);
            tvDistance = (TextView) view.findViewById(R.id.tvDistance);
            tvShipmentNumber = (TextView) view.findViewById(R.id.tvShipmentNumber);
            tvTop = (TextView) view.findViewById(R.id.tvTop);
            tvAddress = (TextView) view.findViewById(R.id.tvAddress);
            tvTime = (TextView) view.findViewById(R.id.tvTime);
            ivDone = view.findViewById(R.id.ivDone);
            ivLocation = view.findViewById(R.id.ivLocation);


        }
    }


    public VehicleRouteAdapter(Context context, ArrayList<PickUpDo> pickUpDos) {
        this.context = context;
        this.productDOS = pickUpDos;
        for (int i = 0; i < pickUpDos.size(); i++) {
            if (pickUpDos.get(i).departuredFlag != 2
                    && pickUpDos.get(i).skipFlag != 2 && pickUpDos.get(i).leftFlag != 2) {
                AppConstants.EnableShipments = pickUpDos.get(i).shipmentNumber;
                break;
            }
        }
        productDOS = pickUpDos;
    }

    public void refreshAdapter(ArrayList<PickUpDo> pickUpDos) {
        for (int i = 0; i < pickUpDos.size(); i++) {
            if (pickUpDos.get(i).departuredFlag != 2
                    && pickUpDos.get(i).skipFlag != 2 && pickUpDos.get(i).leftFlag != 2) {
                AppConstants.EnableShipments = pickUpDos.get(i).shipmentNumber;
                break;
            }
        }
        productDOS = pickUpDos;
        notifyDataSetChanged();
    }

    @SuppressLint("MissingPermission")
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.vehicle_data, parent, false);
//        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
//        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
//        lat = location.getLongitude();
//        lng = location.getLatitude();
//        double lat2 = Double.valueOf(((BaseActivity) context).preferenceUtils.getDoubleFromPreference(PreferenceUtils.LATTITUDE, 0.0));
//        double lng3 = Double.valueOf(((BaseActivity) context).preferenceUtils.getDoubleFromPreference(PreferenceUtils.LONGITUDE, 0.0));
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final PickUpDo productDO = productDOS.get(position);

        holder.tvName.setText(productDO.customer);
        holder.tvAddress.setText(productDO.shipmentNumber);
        holder.tvShipmentNumber.setText("" + productDO.description);
        // distance(lat, lng, productDO.lattitude, productDO.longitude);
        double value = Double.parseDouble(String.format("%.2f", productDO.distance));
        holder.tvDistance.setText("" + value + " " + productDO.unit);

        PreferenceUtils preferenceUtils = new PreferenceUtils(context);

        // Double distance = mylocation.distanceTo(dest_location);//in meters
//        System.out.println(arrivalTime);
//        System.out.println(departureTime);
        if (productDO.arrivalTime.length() > 0 && productDO.departureTime.length() > 0) {
            String arrivalTime = productDO.arrivalTime.substring(Math.max(productDO.arrivalTime.length() - 2, 0));
            String departureTime = productDO.departureTime.substring(Math.max(productDO.departureTime.length() - 2, 0));

            arrivalTime2 = productDO.arrivalTime.substring(0, 2);
            departureTime2 = productDO.departureTime.substring(0, 2);
            holder.tvTime.setText("ETA : " + arrivalTime2 + ":" + arrivalTime + "  ETD : " + departureTime2 + ":" + departureTime);


            preferenceUtils.saveString(PreferenceUtils.SHIPMENT_DT, dDate + "-" + dMonth + "-" + dyear + "  " + departureTime2 + ":" + departureTime);
            preferenceUtils.saveString(PreferenceUtils.SHIPMENT_AT, aDate + "-" + aMonth + "-" + ayear + "  " + arrivalTime2 + ":" + arrivalTime);

        }
        if (productDOS.get(position).arrivalDate.length() > 0) {
            aMonth = productDOS.get(position).arrivalDate.substring(4, 6);
            ayear = productDOS.get(position).arrivalDate.substring(0, 4);
            aDate = productDOS.get(position).arrivalDate.substring(Math.max(productDOS.get(position).arrivalDate.length() - 2, 0));

        }
        if (productDOS.get(position).departureDate.length() > 0) {

            dMonth = productDOS.get(position).departureDate.substring(4, 6);
            dyear = productDOS.get(position).departureDate.substring(0, 4);
            dDate = productDOS.get(position).departureDate.substring(Math.max(productDOS.get(position).departureDate.length() - 2, 0));
        }
        preferenceUtils.saveDouble(PreferenceUtils.LATTITUDE, productDOS.get(position).lattitude);
        preferenceUtils.saveDouble(PreferenceUtils.LONGITUDE, productDOS.get(position).longitude);
        preferenceUtils.saveString(PreferenceUtils.PHONE, String.valueOf(productDOS.get(position).sequenceId));

        int pos = position + 1;
        if (pos == 1) {
            //   holder.tvTop.setVisibility(View.GONE);
        } else
            holder.tvTop.setVisibility(View.VISIBLE);


        if (productDO.departuredFlag == 2 || productDO.cancelFlag == 10 || productDO.cancelFlag == 20) {
            holder.ivDone.setImageResource(R.drawable.done_tick_green);
        } else if (productDO.skipFlag == 2) {
            holder.ivDone.setImageResource(R.drawable.skip_icon);
        }
        else if (productDO.leftFlag == 2) {
            holder.ivDone.setImageResource(R.drawable.skip_icon);
            holder.ivDone.setColorFilter(ContextCompat.getColor(context,
                    R.color.red));
        }
        else {
            if (AppConstants.EnableShipments.equalsIgnoreCase(productDO.shipmentNumber)) {//preferenceUtils.getStringFrom(PreferenceUtils.SHIPMENT, "");
                Animation vibrateAnimation = AnimationUtils.loadAnimation(context, R.anim.shake_animation);
                holder.ivLocation.setImageResource(R.drawable.marker_green);

                holder.ivLocation.startAnimation(vibrateAnimation);
                holder.ivDone.setImageResource(R.drawable.check_mark);
            } else {
                holder.ivLocation.clearAnimation();
                holder.ivLocation.setImageResource(R.drawable.markers);
                holder.ivDone.setImageResource(R.drawable.check_mark);
            }
        }
        holder.tvCount.setText(productDO.sequenceId + "");
        pos++;
        holder.llDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (productDO.volume.equals(context.getResources().getString(R.string.app_version))) {
                    String shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "");
                    if (productDO.departuredFlag == 2 || productDO.cancelFlag == 10 || productDO.cancelFlag == 20) {
                        showShipmentDetails(productDO);
                    } else if (shipmentId.equalsIgnoreCase(productDO.shipmentNumber)
                            || (shipmentId.equalsIgnoreCase("")
                            && (productDO.skipFlag == 2||productDO.leftFlag == 2 ||
                            AppConstants.EnableShipments.equalsIgnoreCase(productDO.shipmentNumber)))) {

                        PreferenceUtils preferenceUtils = ((BaseActivity) context).preferenceUtils;
                        preferenceUtils.saveDouble(PreferenceUtils.LATTITUDE, productDO.lattitude);
                        preferenceUtils.saveDouble(PreferenceUtils.LONGITUDE, productDO.longitude);
                        Intent intent = new Intent(context, ActiveDeliveryActivity.class);
                        intent.putExtra("SHIPMENT_ID", productDO.shipmentNumber);
                        preferenceUtils.saveString(PreferenceUtils.CUSTOMER, productDO.customer);
                        preferenceUtils.saveString(PreferenceUtils.CUSTOMER_NAME, productDO.description);
                        intent.putExtra("Scheduled_customer", productDO.customer);
                        preferenceUtils.saveDouble(PreferenceUtils.DISTANCE, value);
                        intent.putExtra("DISTANCE", value);
                        preferenceUtils.saveString(PreferenceUtils.CURRENCY, "" + productDO.currency);
                        preferenceUtils.saveString(PreferenceUtils.SHIPMENT, productDO.shipmentNumber);
                        ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.ShipmentProductsType, productDO.productType);
                        preferenceUtils.saveString(PreferenceUtils.ShipmentType, context.getResources().getString(R.string.checkin_scheduled));
                        ((BaseActivity) context).startActivityForResult(intent, 19);

                    } else {
                        ((BaseActivity) context).showToast("Please process the current shipment");
                    }
                } else {
                    ((VehicleRoutingList) context).showAppCompatAlert("" + context.getResources().getString(R.string.login_info), "" + context.getResources().getString(R.string.playstore_messsage), "" + context.getResources().getString(R.string.ok), "Cancel", "PLAYSTORE", true);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        if (productDOS.size() > 0) {
            return productDOS.size();
        } else {

            return 0;
        }
    }

    private void showShipmentDetails(PickUpDo productDO) {
        Dialog shipmentDetailsDialog = new Dialog(context);
        shipmentDetailsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        shipmentDetailsDialog.setContentView(R.layout.shipment_details_dialog_layout);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((BaseActivity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;

        shipmentDetailsDialog.getWindow().setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);
        shipmentDetailsDialog.setCancelable(true);
        shipmentDetailsDialog.setCanceledOnTouchOutside(true);
        TextView tvShipmentNumber = shipmentDetailsDialog.findViewById(R.id.tvShipmentNumber);
        TextView tvCustomerName = shipmentDetailsDialog.findViewById(R.id.tvCustomerName);
        ListView lvItems = shipmentDetailsDialog.findViewById(R.id.lvItems);
//        TextView tvDistance = shipmentDetailsDialog.findViewById(R.id.tvDistance);
//        TextView tvProductType = shipmentDetailsDialog.findViewById(R.id.tvProductType);
//        TextView tvDescription = shipmentDetailsDialog.findViewById(R.id.tvDescription);
        TextView tvClose = shipmentDetailsDialog.findViewById(R.id.tvClose);
//
        tvShipmentNumber.setText("" + productDO.shipmentNumber);
        tvCustomerName.setText("" + productDO.description);
//        tvCity.setText("" + productDO.city);
//        tvDistance.setText("" + productDO.distance);
//        tvProductType.setText("" + productDO.productType);
//        tvDescription.setText("" + productDO.description);

        if (Util.isNetworkAvailable(((BaseActivity) context))) {
            ActiveDeliveryRequest driverListRequest = new ActiveDeliveryRequest("" + productDO.shipmentNumber, context);
            driverListRequest.setOnResultListener(new ActiveDeliveryRequest.OnResultListener() {
                @Override
                public void onCompleted(boolean isError, ActiveDeliveryMainDO activeDeliveryMainDO) {
                    if (isError) {
                        Toast.makeText(context, R.string.server_error, Toast.LENGTH_SHORT).show();
                    } else {
                        activeDeliveryDOs = activeDeliveryMainDO.activeDeliveryDOS;
                        ProductListAdapter productListAdapter = new ProductListAdapter(context, activeDeliveryMainDO.activeDeliveryDOS);
                        lvItems.setAdapter(productListAdapter);
                    }
                }
            });
            driverListRequest.execute();
        } else {
            ((BaseActivity) context).showToast("No Internet connection, please try again later");
        }
        tvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shipmentDetailsDialog.dismiss();
            }
        });
        shipmentDetailsDialog.show();
    }

    private class ProductListAdapter extends BaseAdapter {

        private Context context;
        private ArrayList<ActiveDeliveryDO> activeDeliveoS;

        private ProductListAdapter(Context context, ArrayList<ActiveDeliveryDO> activeDeliveryDOS) {
            this.context = context;
            this.activeDeliveoS = activeDeliveryDOS;
        }

        @Override
        public int getCount() {
            return activeDeliveoS != null ? activeDeliveoS.size() : 0;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = LayoutInflater.from(context).inflate(R.layout.shipment_detail_data, null);
            TextView tvProductName = (TextView) view.findViewById(R.id.tvName);
            TextView tvDescription = (TextView) view.findViewById(R.id.tvDescription);
            TextView tvNumber = (TextView) view.findViewById(R.id.tvNumber);
            TextView tvAvailableQty = (TextView) view.findViewById(R.id.tvAvailableQty);
//            tvProductName.setText(activeDeliveoS.get(position).product);
            tvDescription.setText("" + activeDeliveoS.get(position).productDescription);
            tvNumber.setText("" + activeDeliveoS.get(position).totalQuantity + " " + activeDeliveoS.get(position).unit);
            tvAvailableQty.setVisibility(View.GONE);

            return view;
        }

    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }
}
