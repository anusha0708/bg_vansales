package com.tbs.brothersgas.haadhir.Model;

import java.io.Serializable;

public class PickUpDo implements Serializable {


    public String sequenceId = "";
    public String shipmentNumber = "";
    public String city = "";
    public String customer = "";
    public String productType = "";
    public String currency = "";

    public String description = "";
    public String netWeight = "";
    public String volume = "";
    public Double lattitude =0.0;
    public Double longitude =0.0;
    public String packages = "";
    public String departureDate = "";
    public String departureTime = "";
    public String arrivalDate = "";
    public String arrivalTime = "";
    public double distance = 0.0;
    public String status = "";
    public String unit = "";
    public int departuredFlag=0;
    public int leftFlag=0;


    public int validatedFlag=0;
    public int cancelFlag   =0;
    public int skipFlag     =0;

    public String city() {
        return city;
    }
}
