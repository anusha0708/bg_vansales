package com.tbs.brothersgas.haadhir.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class PaymentPdfDO implements Serializable {
    public String paymentNumber = "";
    public String customer = "";
    public String customerDescription = "";
    public String company = "";
    public String companyCode = "";
    public String createdDate = "";

    public String country = "";
    public String landline = "";
    public String mobile = "";
    public String fax = "";
    public String mail = "";
    public Double amount = 0.0;
    public String currency = "";
    public String bank = "";
    public String chequeNumber = "";
    public Double invoiceAmount = 0.0;

    public int chargetype = 0;
    public Double vatAmount = 0.0;
    public Double serviceCharges = 0.0;
    public String cardType = "";

    public String chequeDate = "";
    public String invoiceNumber = "";
    public String flag = "";
    public String logo = "";
    public String customerStreet = "";
    public String customerLandMark = "";
    public String customerTown = "";
    public String customerCity = "";
    public String customerPostalCode = "";
    public String createUserID = "";
    public String createUserName = "";

    public String siteDescription = "";
    public String siteAddress1 = "";
    public String siteAddress2 = "";
    public String siteAddress3 = "";
    public String siteCountry = "";
    public String siteCity = "";
    public String sitePostalCode = "";
    public String siteLandLine = "";
    public String siteMobile = "";
    public String siteFax = "";
    public String siteEmail1 = "";
    public String siteEmail2 = "";
    public String siteWebEmail = "";
    public String createdTime = "";


    public ArrayList<InvoiceNumberDos> invoiceDos = new ArrayList<>();

    @Override
    public String toString() {
        return "PaymentPdfDO{" +
                "paymentNumber='" + paymentNumber + '\'' +
                ", customer='" + customerDescription + '\'' +
                ", customerDescription='" + customerDescription + '\'' +

                ", country='" + country + '\'' +
                ", landline='" + landline + '\'' +
                ", mobile='" + mobile + '\'' +
                ", fax='" + fax + '\'' +
                ", mail='" + mail + '\'' +
                ", amount=" + amount +
                ", currency='" + currency + '\'' +
                ", bank='" + bank + '\'' +
                ", chequeNumber='" + chequeNumber + '\'' +
                ", chequeDate='" + chequeDate + '\'' +
                ", invoiceNumber='" + invoiceNumber + '\'' +
                ", flag='" + flag + '\'' +
                '}';
    }
}
