package com.tbs.brothersgas.haadhir.Activitys

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.database.Cursor
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.appcompat.widget.SearchView
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.brothersgas.haadhir.Adapters.LoadStockAdapter
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.LoadVanSaleRequest
import com.tbs.brothersgas.haadhir.common.AppConstants
import com.tbs.brothersgas.haadhir.listeners.LoadStockListener
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import java.util.ArrayList
import android.widget.Toast
import com.tbs.brothersgas.haadhir.Adapters.CustomLoadStockAdapter
import com.tbs.brothersgas.haadhir.Adapters.DBAdapter
import com.tbs.brothersgas.haadhir.Model.*
import com.tbs.brothersgas.haadhir.utils.Util


//
class LoadVanSalesActivity : BaseActivity(), LoadStockListener {
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var loadStockAdapter: LoadStockAdapter
    lateinit var loadStockDOs: ArrayList<LoadStockDO>
    lateinit var username: String
    lateinit var loadStockDO: LoadStockDO
    lateinit var cursor: Cursor
    lateinit var etLoadingMass: EditText
    lateinit var etLoadingVolume: EditText
    lateinit var tvVehicleMass: TextView
    lateinit var btnLoad: Button

    lateinit var tvVehicleVolume: TextView
    lateinit var loadStockMainDO: LoadStockMainDO
    lateinit var tvMass: TextView
    lateinit var tvVolume: TextView
    lateinit var customRecycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var rl1: RelativeLayout

    lateinit var list: ArrayList<CustomProductDO>
    lateinit var etUSMass: EditText
    lateinit var etUSVolume: EditText
    lateinit var db: DBAdapter
    override fun updateCount() {
        var weight = preferenceUtils.getDoubleFromPreference(PreferenceUtils.WEIGHT, 0.0)
        var volume = preferenceUtils.getDoubleFromPreference(PreferenceUtils.VOLUME, 0.0)

//        etUSMass.setText("" + weight)
//        etUSVolume.setText("" + volume)

    }

    var mass = 0.00


    override protected fun onResume() {
        super.onResume()
    }

    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.fragment_van_sales, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
        preferenceUtils = PreferenceUtils(this@LoadVanSalesActivity)
        username = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "")
        if (Util.isNetworkAvailable(this)) {
            val loadVanSaleRequest = LoadVanSaleRequest(username, this@LoadVanSalesActivity)
            //  showLoader();
            loadVanSaleRequest.setOnResultListener { isError, loadStockMainDo ->
                hideLoader()

                loadStockMainDO = loadStockMainDo
                if (isError) {
                    Toast.makeText(this@LoadVanSalesActivity, R.string.error_checkin, Toast.LENGTH_SHORT).show()
                } else {
                    if (loadStockMainDo.loadStockDOS.size > 0) {
                        rl1.setVisibility(View.VISIBLE)
                        btnLoad.setVisibility(View.VISIBLE)
                        AppConstants.listStockDO = loadStockMainDo.loadStockDOS
                        loadStockAdapter = LoadStockAdapter(this@LoadVanSalesActivity, loadStockMainDo.loadStockDOS, "")
                        recycleview.setAdapter(loadStockAdapter)

                        tvVolume.setText("Volume(" + loadStockMainDo.volumeUnit + ")")
                        etLoadingVolume.setText("" + loadStockMainDo.vanVolume)


                    } else {
                        showAlert("No Routing Id's Found")
                        rl1.setVisibility(View.GONE)
                        btnLoad.setVisibility(View.GONE)
                    }

                }
            }
            loadVanSaleRequest.execute()

        } else {
            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "",false)

        }



    }

    override fun initializeControls() {
        tvScreenTitleTop.setVisibility(View.VISIBLE)
        tvScreenTitleTop.setText(""+preferenceUtils.getStringFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID, ""))
        db = DBAdapter(this);
        db.openDatabase()
        ivAdd.setVisibility(View.VISIBLE)
        ivAdd.setOnClickListener {
            val intent = Intent(this@LoadVanSalesActivity, CreateProductActivity::class.java)
            startActivity(intent)
        }
        rl1       = findViewById(R.id.rl1) as RelativeLayout

        recycleview       = findViewById(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        customRecycleview = findViewById(R.id.customRecycleview) as androidx.recyclerview.widget.RecyclerView
        etLoadingMass     = findViewById(R.id.etLoadingMass) as EditText
        etLoadingVolume   = findViewById(R.id.etLoadingVolume) as EditText
        tvVehicleMass     = findViewById(R.id.tvVehicleMass) as TextView
        btnLoad   = findViewById(R.id.btnLoad) as Button

        tvVehicleVolume   = findViewById(R.id.tvVehicleVolume) as TextView
        tvMass            = findViewById(R.id.tvMass) as TextView
        tvVolume          = findViewById(R.id.tvVolume) as TextView
        etUSMass          = findViewById(R.id.etUSMass) as EditText
        etUSVolume        = findViewById(R.id.etUSVolume) as EditText
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        recycleview.setLayoutManager(linearLayoutManager)
         btnLoad.setOnClickListener {
             preferenceUtils.saveString(PreferenceUtils.STATUS, "Stock Loaded")

             val intent = Intent(this@LoadVanSalesActivity, DashBoardActivity::class.java)
             startActivity(intent)

         }
        ivSearch.setVisibility(View.VISIBLE)
        ivSearch.setOnClickListener {
            svSearch.setVisibility(View.VISIBLE)
            ivSearch.setVisibility(View.GONE)
            val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
            svSearch.setSearchableInfo(searchManager
                    .getSearchableInfo(componentName))
            svSearch.maxWidth = Integer.MAX_VALUE

            svSearch.setActivated(true);
            svSearch.setQueryHint("Type your keyword here");
            svSearch.onActionViewExpanded();
            svSearch.setIconified(false);
            svSearch.clearFocus();
            svSearch.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String): Boolean {
                    // filter recycler view when query submitted
                    if (query.length > 2) {

                        loadStockAdapter.getFilter().filter(query)

                        loadStockAdapter.notifyDataSetChanged()

                    }
//                    Toast.makeText(this@LoadVanSalesActivity, "submit", Toast.LENGTH_SHORT).show()

                    return false
                }

                override fun onQueryTextChange(query: String): Boolean {
                    if (query.length > 2) {
                        loadStockAdapter.getFilter().filter(query)
//                        Toast.makeText(this@LoadVanSalesActivity, "changed", Toast.LENGTH_SHORT).show()
                    }
                    return false
                }
            })


            fun onClose(): Boolean {
                Toast.makeText(this@LoadVanSalesActivity, "close", Toast.LENGTH_SHORT)
                //loadStockAdapter.refreshAdapter()
                return false
            }


        }


//        val btnLoad = findViewById(R.id.btnLoad) as Button
//
//        btnLoad.setOnClickListener {
//            val intent = Intent(this@LoadVanSalesActivity, DashBoardActivity::class.java);
//            startActivity(intent);
//        }


        val intent = getIntent();
        //  val args = intent.getBundleExtra("BUNDLE");
        if (intent.hasExtra("ProductObject")) {
            list = intent.getSerializableExtra("ProductObject") as ArrayList<CustomProductDO>

            val adapter = CustomLoadStockAdapter(this@LoadVanSalesActivity, list)
            val manager = androidx.recyclerview.widget.LinearLayoutManager(this)

            customRecycleview.setLayoutManager(manager)

            customRecycleview.setAdapter(adapter)
            for (i in 0 until list.size) {

                db.insertValues(list.get(i).productName, list.get(i).itemCount.toString());
                cursor=db.allValues
                cursor.count
                cursor.moveToPosition(i);

                var rowid = cursor.getString(1);
                var fname = cursor.getString(2);
                Log.d("DATABSE", rowid + fname);
            }


            // val data = args.getSerializable("ProductObject")
            // Log.d("TESTING", list.toString())
        }
    }
}

private operator fun String.times(totalStockVolume: Double?): Double? {
    return totalStockVolume
}

