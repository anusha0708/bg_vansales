package com.tbs.brothersgas.haadhir.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.brothersgas.haadhir.Activitys.BaseActivity;
import com.tbs.brothersgas.haadhir.Model.SuccessDO;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;
import com.tbs.brothersgas.haadhir.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class CheckoutTimeCaptureRequest extends AsyncTask<String, Void, Boolean> {

    private SuccessDO successDO;
    private Context mContext;
    private String userID, currentDate, arrivalDate,arrivalTime,startUnLoadingTime,startUnLoadingDate,stockUnloadingDate,stockUnLoadingTime,endloadingTime
            ,endLoadingDate,vehicleInspectionDate,vehicleInspectionTime,gateInspectionDate,gateInspectionTime,checkoutDate,checkoutTime;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;
    private int flaG;

    public CheckoutTimeCaptureRequest(String userId, String currentdate, String arrivaldate,String arrivaltime,
                                     String startloadingtime,String startloadingdate,String stockUnloadingtime,String stockUnloadingate,
                                     String endloadingtime,String endloadingdate,
                                     String vehicleinspectiontime,String vehicleinspectiondate,
                                     String gateinspectiontime,String gateinspectiondate,
                                     String checkoutdate,String checkouttime,Context mContext) {

        this.mContext = mContext;
        this.userID = userId;
        this.currentDate = currentdate;
        this.arrivalTime = arrivaltime;
        this.arrivalDate = arrivaldate;
        this.startUnLoadingTime = startloadingdate;
        this.startUnLoadingDate = startloadingtime;
        this.stockUnLoadingTime = stockUnloadingtime;
        this.stockUnloadingDate = stockUnloadingate;
        this.endloadingTime = endloadingtime;
        this.endLoadingDate = endloadingdate;
        this.vehicleInspectionDate = vehicleinspectiondate;
        this.vehicleInspectionTime = vehicleinspectiontime;
        this.gateInspectionDate = gateinspectiondate;
        this.gateInspectionTime = gateinspectiontime;
        this.checkoutDate = checkoutdate;
        this.checkoutTime = checkouttime;


    }


    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        public void onCompleted(boolean isError, SuccessDO createInvoiceDO);

    }

    public boolean runRequest() {
        // System.out.println("CUSTOMER ID " + customerId);
        String NAMESPACE = "http://www.adonix.com/WSS";
        String METHOD_NAME = "run";
        String SOAP_ACTION = "CAdxWebServiceXmlCC";
        preferenceUtils = new PreferenceUtils(mContext);
        username = preferenceUtils.getStringFromPreference(PreferenceUtils.A_USER_NAME, "");
        password = preferenceUtils.getStringFromPreference(PreferenceUtils.A_PASSWORD, "");
        ip = preferenceUtils.getStringFromPreference(PreferenceUtils.IP_ADDRESS, "");
        port = preferenceUtils.getStringFromPreference(PreferenceUtils.PORT, "");
        pool = preferenceUtils.getStringFromPreference(PreferenceUtils.ALIAS, "");
        String URL = "http://" + ip + ":" + port + "/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC";
        String doc_number = preferenceUtils.getStringFromPreference(PreferenceUtils.DOC_NUMBER, "");

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        // Set all input params                         X10CSAINV3
        request.addProperty("publicName", WebServiceConstants.CHECKOUT_TIME_CAPTURE);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("I_XSRCNUM", doc_number);
            jsonObject.put("I_X10CUSRID", userID);
            jsonObject.put("I_XDATE", currentDate);
            jsonObject.put("I_XDECONARRDAT", arrivalDate);
            jsonObject.put("I_XDECONARRTIM", arrivalTime);
            jsonObject.put("I_XDESTRUNLODDAT", startUnLoadingDate);
            jsonObject.put("I_XDESTRUNLODTIM", startUnLoadingTime);
            jsonObject.put("I_XDEUNLODSTKDAT", stockUnloadingDate);
            jsonObject.put("I_XDEUNLODSTKTIM", stockUnLoadingTime);
            jsonObject.put("I_XDEENDUNLODDAT", endLoadingDate);
            jsonObject.put("I_XDEENDUNLODTIM", endloadingTime);
            jsonObject.put("I_XDEVEHINPDAT", vehicleInspectionDate);
            jsonObject.put("I_XDEVEHINPTIM", vehicleInspectionTime);
            jsonObject.put("I_XDEGATINPDAT", gateInspectionDate);
            jsonObject.put("I_XDEGATINPTIM", gateInspectionTime);
            jsonObject.put("I_XDECHKOUTDAT", checkoutDate);
            jsonObject.put("I_XDECHKOUTTIM", checkoutTime);



        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        request.addProperty("inputXml", jsonObject.toString());

        SoapObject callcontext = new SoapObject("", "callContext");
        // Set all input params
        callcontext.addProperty("codeLang", "ENG");
        callcontext.addProperty("poolAlias", pool);
        callcontext.addProperty("poolId", "");
        callcontext.addProperty("codeUser", username);
        callcontext.addProperty("password", password);
        callcontext.addProperty("requestConfig", "adxwss.trace.on=off");

        request.addSoapObject(callcontext);


        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.setOutputSoapObject(request);

        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
        androidHttpTransport.debug = true;

        try {
            List<HeaderProperty> headerList = new ArrayList<HeaderProperty>();
            headerList.add(new HeaderProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode((username + ":" + password).getBytes())));


            androidHttpTransport.call(SOAP_ACTION, envelope, headerList);


            SoapObject response = (SoapObject) envelope.getResponse();
            String resultXML = (String) response.getProperty("resultXml");
            if (resultXML != null && resultXML.length() > 0) {
                return parseXML(resultXML);
            } else {
                return false;
            }
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            successDO = new SuccessDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        //    createPaymentDO.customerDetailsDos = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        //      createPaymentDO = new CustomerDetailsDo();

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {


                        if (attribute.equalsIgnoreCase("O_YFLG")) {
                            if (text.length() > 0) {

                                successDO.flag = Integer.parseInt(text);
                            }


                        }
                    }


                    if (endTag.equalsIgnoreCase("GRP")) {
                        // customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        // customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
//        ((BaseActivity) mContext).showLoader();

        // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

//        ((BaseActivity) mContext).hideLoader();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, successDO);
        }
    }
}