package com.tbs.brothersgas.haadhir.Activitys

import android.content.Intent
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.tbs.brothersgas.haadhir.Adapters.DamageScheduledProductsAdapter
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryDO
import com.tbs.brothersgas.haadhir.Model.LoadStockDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.ActiveDeliveryRequest
import com.tbs.brothersgas.haadhir.Requests.CreateNONBGRequest
import com.tbs.brothersgas.haadhir.utils.CalendarUtils
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util

//
class DamageProductsBGActivity : BaseActivity() {
    lateinit var loadStockAdapter: DamageScheduledProductsAdapter
    lateinit var loadStockDOs: ArrayList<LoadStockDO>
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var tvNoDataFound: TextView
//    lateinit var activeDeliveryDos: ArrayList<ActiveDeliveryDO>


    override fun initialize() {
        val llCategories = getLayoutInflater().inflate(R.layout.selected_nonbg, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, Gravity.LEFT)
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            finish()
        }
        tvScreenTitle.setText("Damage Cylinders")

        initializeControls()

    }

    override fun initializeControls() {

        recycleview = findViewById(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        tvNoDataFound = findViewById(R.id.tvNoDataFound) as TextView

        val btnConfirm = findViewById(R.id.btnConfirm) as Button
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
        recycleview.setLayoutManager(linearLayoutManager)

        var id = "" + preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
        var spotId = "" + preferenceUtils.getStringFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER, "")
        if (spotId.length > 0) {
            if (Util.isNetworkAvailable(this)) {

                val driverListRequest = ActiveDeliveryRequest(spotId, this@DamageProductsBGActivity)
                driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                    hideLoader()
                    if (isError) {
                        tvNoDataFound.setVisibility(View.VISIBLE)
                        recycleview.setVisibility(View.GONE)
                        btnConfirm.setVisibility(View.GONE)
//                showToast(resources.getString(R.string.error_NoData))
                    } else {


                        if (activeDeliveryDo.activeDeliveryDOS.size > 0) {
                            tvNoDataFound.setVisibility(View.GONE)
                            recycleview.setVisibility(View.VISIBLE)
                            btnConfirm.setVisibility(View.VISIBLE)
                            loadStockAdapter = DamageScheduledProductsAdapter(this@DamageProductsBGActivity, activeDeliveryDo.activeDeliveryDOS, "", "Damage")
                            recycleview.setAdapter(loadStockAdapter)
                        } else {
                            tvNoDataFound.setVisibility(View.VISIBLE)
                            recycleview.setVisibility(View.GONE)
                            btnConfirm.setVisibility(View.GONE)
                        }


                    }
                }
                driverListRequest.execute()
            } else {
                showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

            }

        } else {

            if (Util.isNetworkAvailable(this)) {
                val driverListRequest = ActiveDeliveryRequest(id, this@DamageProductsBGActivity)
                driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                    hideLoader()
                    if (isError) {
                        tvNoDataFound.setVisibility(View.VISIBLE)
                        recycleview.setVisibility(View.GONE)
                        btnConfirm.setVisibility(View.GONE)
//                showToast(resources.getString(R.string.error_NoData))
                    } else {

                        if (activeDeliveryDo.activeDeliveryDOS.size > 0) {
                            tvNoDataFound.setVisibility(View.GONE)
                            recycleview.setVisibility(View.VISIBLE)
                            btnConfirm.setVisibility(View.VISIBLE)
                            loadStockAdapter = DamageScheduledProductsAdapter(this@DamageProductsBGActivity, activeDeliveryDo.activeDeliveryDOS, "", "Damage")
                            recycleview.setAdapter(loadStockAdapter)
                        } else {
                            tvNoDataFound.setVisibility(View.VISIBLE)
                            recycleview.setVisibility(View.GONE)
                            btnConfirm.setVisibility(View.GONE)
                        }


                    }
                }
                driverListRequest.execute()
            } else {
                showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

            }


        }


        btnConfirm.setOnClickListener {


            if (loadStockAdapter != null) {
                val activeDeliveryDos = loadStockAdapter.getSelectedLoadStockDOs();



                if (activeDeliveryDos != null && !activeDeliveryDos.isEmpty() && activeDeliveryDos.size > 0) {

                    val intent = Intent(this@DamageProductsBGActivity, SelectedDamageBGActivity::class.java);
                    intent.putExtra("Products", activeDeliveryDos);

                    startActivityForResult(intent, 11)


                } else {
                    showToast("No  Items found")
                }
            } else {
                showToast("No  Items found")
            }

        }
    }

    fun nonBG(activeDeliveryDos: ArrayList<ActiveDeliveryDO>) {


        var recievedSite = preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID, "")
        var vehicleCode = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_CODE, "")

        val customer = preferenceUtils.getStringFromPreference(PreferenceUtils.CUSTOMER, "")
        val returnDate = CalendarUtils.getDate()

        val siteListRequest = CreateNONBGRequest(recievedSite, customer, vehicleCode, 1, activeDeliveryDos, this@DamageProductsBGActivity)
        showLoader()
        siteListRequest.setOnResultListener { isError, loanReturnMainDo ->
            hideLoader()
            if (loanReturnMainDo != null) {
                if (isError) {
                    showToast("Unable to Create Non Bg")
                } else {
                    if (loanReturnMainDo.status == 20) {
                        showToast("Updated Successfully...")

                        val intent = Intent(this@DamageProductsBGActivity, SignatureActivity::class.java);
                        startActivityForResult(intent, 11)
//                            val intent = Intent()
//                            intent.putExtra("CapturedReturns", true)
//                            setResult(11, intent)
//                            showToast(loanReturnMainDo.message)
//                            preferenceUtils.saveString(PreferenceUtils.NONBG, "NONBG")
//
//                            finish()
                    } else if (loanReturnMainDo.status == 10) {

                        showToast(loanReturnMainDo.message)

                    }

                }
            } else {
                showToast("Unable to Create Non Bg")
            }

        }
        siteListRequest.execute()


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 11 && resultCode == 11) {
            val intent = Intent();
            intent.putExtra("CapturedReturns", true)
            // AppConstants.CapturedReturns= true
            setResult(11, intent)
            finish()
        }


    }

    override fun onBackPressed() {
        showToast("Please confirm..!")

    }
}