package com.tbs.brothersgas.haadhir.Model;

import java.io.Serializable;

public class CustomerDo implements Serializable {

    public String customerId          = "";
    public String customerName        = "";
    public String postBox             = "";
    public String town                = "";
    public String landmark            = "";
    public String countryName         = "";
    public String city                = "";
    public String webSite             = "";
    public String postalCode          = "";
    public String landline            = "";
    public String currency            = "";
    public String mobile              = "";
    public String email               = "";
    public String fax                 = "";
    public String lattitude           = "";
    public String longitude           = "";
    public String isDelivered         = "";
    public int flag                   = 0;
    public String amount              = "";
    public String businessLine        = "";
    public String emirates            = "";
    public boolean isSelected;
    public String deliveryEmail       = "";
    public String invoiceEmail        = "";
    public String paymentEmail        = "";
    public String cylinderIssueEmail  = "";
    public String paymentTerm        = "";
    public int resFlag                   = 0;


    public void setSelected(boolean selected) {
        isSelected = selected;
    }
    public boolean isCreditAvailable = true;


    public void customerName(String text) {
        this.customerName = text;
    }

    public void customerId(String text) {
        this.customerId = text;
    }

    private boolean isChecked;

    public boolean getChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
