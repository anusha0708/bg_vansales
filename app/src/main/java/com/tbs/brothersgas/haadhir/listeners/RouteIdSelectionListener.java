package com.tbs.brothersgas.haadhir.listeners;

public interface RouteIdSelectionListener {
    void updateId();
}
