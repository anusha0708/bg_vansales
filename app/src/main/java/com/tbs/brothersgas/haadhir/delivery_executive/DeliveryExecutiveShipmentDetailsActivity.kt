package com.tbs.brothersgas.haadhir.delivery_executive

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.appcompat.app.AppCompatDialog
import com.tbs.brothersgas.haadhir.Activitys.BaseActivity
import com.tbs.brothersgas.haadhir.Adapters.ScheduledProductsAdapter
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryDO
import com.tbs.brothersgas.haadhir.Model.CustomerDo
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.ActiveDeliveryRequest
import com.tbs.brothersgas.haadhir.Requests.ExecutiveActiveDeliveryRequest
import com.tbs.brothersgas.haadhir.pdfs.CYLDeliveryNotePDF
import com.tbs.brothersgas.haadhir.utils.Constants
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import kotlinx.android.synthetic.main.activity_delivery_executive_shipment_details.*
import java.util.*

class DeliveryExecutiveShipmentDetailsActivity : BaseActivity() {
    var shipmentId: String = ""
    var customerCode: String = ""
    private var screenType: Int = 1
    lateinit var userId: String
    lateinit var orderCode: String
    lateinit var llOrderHistory: View
    lateinit var customerDos: ArrayList<CustomerDo>
    private lateinit var btnAddMore: Button
    lateinit var llSearch: LinearLayout
    lateinit var etSearch: EditText
    private var cday: Int = 0
    private var cmonth: Int = 0
    private var cyear: Int = 0
    var startDate = ""
    var endDate = ""
    var to = ""
    lateinit var itemName: String
    var itemSite: Int = 0
    lateinit var itemNumber: String
    companion object {
        var isRefreshed: Boolean = false
    }

    override fun initialize() {
        llOrderHistory = layoutInflater.inflate(R.layout.activity_delivery_executive_shipment_details, null)
        llBody.addView(llOrderHistory, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            val intent = Intent()
            setResult(1, intent)
            finish()
        }
        disableMenuWithBackButton()
        initializeControls()

        shipmentId = intent.getStringExtra(Constants.SHIPMENT_ID)
        tvScreenTitle.setText(shipmentId)
        customerCode = intent.getStringExtra(Constants.CUSTOMER_CODE)


        getList();

        btn_proceed_sign.setOnClickListener {
//            val intent = Intent(this@DeliveryExecutiveShipmentDetailsActivity, DeliveryExecutiveSignatureActivity::class.java);
//            intent.putExtra(Constants.SHIPMENT_ID, shipmentId)
//            intent.putExtra(Constants.CUSTOMER_CODE, customerCode)
//            startActivityForResult(intent, 11)
            if (preferenceUtils.getStringFromPreference(PreferenceUtils.POD_NUMBER, "").isEmpty()
                    || preferenceUtils.getStringFromPreference(PreferenceUtils.POD_NAME, "").isEmpty()) {
                captureInfo()

            } else {
                val intent = Intent(this@DeliveryExecutiveShipmentDetailsActivity, DeliveryExecutiveSignatureActivity::class.java);
                intent.putExtra(Constants.SHIPMENT_ID, shipmentId)
                intent.putExtra(Constants.CUSTOMER_CODE, customerCode)
                startActivityForResult(intent, 11)
            }

        }

    }
    fun captureInfo() {
        try {
            val dialog = AppCompatDialog(this, R.style.AppCompatAlertDialogStyle)
            dialog.supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
            val view = LayoutInflater.from(this).inflate(R.layout.info_capture, null)
            val etName = view.findViewById<EditText>(R.id.etName)
            val etNumber = view.findViewById<EditText>(R.id.etNumber)

            dialog.setCancelable(true)
            dialog.setCanceledOnTouchOutside(true)
            val btnSubmit = view.findViewById<Button>(R.id.btnSubmit)

            btnSubmit.setOnClickListener {
                itemName = etName.text.toString()
                itemNumber = etNumber.text.toString()

                if (itemName.isEmpty()) {
                    showToast("Please enter name")
                }
//                 else if (itemName.length<2) {
//                    showToast("Please enter valid name")
//
//                }
                else  if (itemNumber.isEmpty()) {
                    showToast("Please enter mobile number")

                } else {
//                     val REG = "^\\+[0-9]{10,14}\$"
//                    var PATTERN: java.util.regex.Pattern = java.util.regex.Pattern.compile(REG)
//                    fun CharSequence.isPhoneNumber() : Boolean = PATTERN.matcher(this).find()
//                    if (!itemNumber.isPhoneNumber()) {
//                        Toast.makeText(this, "Please enter " + "" + " valid phone number", Toast.LENGTH_SHORT).show();
//                    } else {
                    try {
                        val dialog2 = AppCompatDialog(this, R.style.AppCompatAlertDialogStyle)
                        dialog2.supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
                        val view = LayoutInflater.from(this).inflate(R.layout.confirm_info_capture, null)
                        val etName1 = view.findViewById<TextView>(R.id.etName)
                        val etNumber2 = view.findViewById<TextView>(R.id.etNumber)
                        etName1.setText(itemName)
                        etNumber2.setText(itemNumber)
                        dialog2.setCancelable(true)
                        dialog2.setCanceledOnTouchOutside(true)
                        val btnSubmit = view.findViewById<Button>(R.id.btnConfirm)
                        val btnBack = view.findViewById<Button>(R.id.btnBack)
                        btnBack.setOnClickListener {
                            dialog2.dismiss()
                        }

                        btnSubmit.setOnClickListener {
                            var itemName1 = etName1.text.toString()
                            var itemNumber1 = etNumber2.text.toString()

                            if (itemNumber1.isEmpty()) {
                                showToast("Please enter mobile number")

                            } else if (itemName1.isEmpty()) {
                                showToast("Please enter name")

                            } else {
                                dialog.dismiss()
                                dialog2.dismiss()
                                preferenceUtils.saveString(PreferenceUtils.POD_NAME, itemName)
                                preferenceUtils.saveString(PreferenceUtils.POD_NUMBER, itemNumber)
                                val intent = Intent(this@DeliveryExecutiveShipmentDetailsActivity, DeliveryExecutiveSignatureActivity::class.java);
                                intent.putExtra(Constants.SHIPMENT_ID, shipmentId)
                                intent.putExtra(Constants.CUSTOMER_CODE, customerCode)
                                startActivityForResult(intent, 11)

                            }
                        }


                        dialog2.setContentView(view)
                        if (!dialog2.isShowing)
                            dialog2.show()
                    } catch (e: Exception) {
//                        }
                    }
                }
            }


            dialog.setContentView(view)
            if (!dialog.isShowing)
                dialog.show()
        } catch (e: Exception) {
        }

    }
    private fun getList() {

        if (Util.isNetworkAvailable(this)) {
            val driverListRequest = ExecutiveActiveDeliveryRequest(shipmentId, this@DeliveryExecutiveShipmentDetailsActivity)
            driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                hideLoader()
                if (isError) {
                    llDetails.visibility = View.GONE
                    Toast.makeText(this@DeliveryExecutiveShipmentDetailsActivity, R.string.server_error, Toast.LENGTH_SHORT).show()
                } else {


                    llDetails.visibility = View.VISIBLE

                    if (activeDeliveryDo.deliveryDate.length > 0) {
                        val aMonth = activeDeliveryDo.deliveryDate.substring(4, 6)
                        val ayear = activeDeliveryDo.deliveryDate.substring(0, 4)
                        val aDate = activeDeliveryDo.deliveryDate.substring(Math.max(activeDeliveryDo.deliveryDate.length - 2, 0))
                        tvShipmentId.text = "" + activeDeliveryDo.shipmentNumber
                        tvSite.text = "" + activeDeliveryDo.siteDescription
                        tvDeliveryDate.text = "" + aDate + "-" + aMonth + "-" + ayear
                        tvCustomer.text = "" + activeDeliveryDo.customer + " - " + activeDeliveryDo.companyDescription
                        tvAddress.text = "" + activeDeliveryDo.siteAddress1 + " , " + activeDeliveryDo.siteAddress2 + " , " + activeDeliveryDo.siteAddress3
                        tvPayementTerm.setText(activeDeliveryDo.paymentTerm)
                        tvDeliveryRemarks.setText(activeDeliveryDo.remarks)

                    }

                    var loadStockAdapter = ScheduledProductsAdapter(this@DeliveryExecutiveShipmentDetailsActivity, activeDeliveryDo.activeDeliveryDOS, "Shipments", "", 0)
                    recycleview.setAdapter(loadStockAdapter)
                }
            }
            driverListRequest.execute()

        } else {
            showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

        }
    }


    override fun initializeControls() {
    }
/*
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 11 && resultCode == 11) {
            val intent = Intent();
            prepareDeliveryNoteCreation(intent.getStringExtra(Constants.SHIPMENT_ID))
        }
    }

   */


}
