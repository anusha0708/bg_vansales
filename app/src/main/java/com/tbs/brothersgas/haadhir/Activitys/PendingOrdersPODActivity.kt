@file:Suppress("NAME_SHADOWING")

package com.tbs.brothersgas.haadhir.Activitys

import android.annotation.SuppressLint
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.location.Geocoder
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import androidx.core.content.FileProvider
import androidx.appcompat.app.AppCompatDialog
import android.util.Base64
import android.util.Log
import android.view.*
import android.widget.*
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryDO
import com.tbs.brothersgas.haadhir.Model.CustomerManagementDO
import com.tbs.brothersgas.haadhir.Model.LoadStockDO
import com.tbs.brothersgas.haadhir.Model.PodDo
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.*
import com.tbs.brothersgas.haadhir.common.AppConstants
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.pdfs.BulkDeliveryNotePDF
import com.tbs.brothersgas.haadhir.pdfs.CYLDeliveryNotePDF
import com.tbs.brothersgas.haadhir.pdfs.CylinderIssRecPdfBuilder
import com.tbs.brothersgas.haadhir.prints.PrinterConnection
import com.tbs.brothersgas.haadhir.prints.PrinterConstants
import com.tbs.brothersgas.haadhir.utils.CalendarUtils
import com.tbs.brothersgas.haadhir.utils.LogUtils
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.util.*


//
class PendingOrdersPODActivity : BaseActivity(){

    lateinit var btnConfirmDeparture: LinearLayout
    lateinit var btnConfirmArrival: LinearLayout
    lateinit var btnDeliveryDetails: LinearLayout
    lateinit var btnGenerate: LinearLayout
    var distance: Double = 0.0
    private var nonScheduleDos: ArrayList<LoadStockDO> = ArrayList()
    lateinit var btnInvoice: LinearLayout
    lateinit var btnPayments: LinearLayout
    lateinit var btnValidateDelivery: Button
    lateinit var customerManagementDO: CustomerManagementDO

    lateinit var tvATime: TextView
    lateinit var tvCaptureListTime: TextView
    lateinit var tvDepartureTime: TextView
    lateinit var pendingShipmentId:String
    private var count: Int = 0
    var lat: Double? = null
    var lng: Double? = null

    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.pending_pod_screen, null) as ScrollView
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()

        disableMenuWithBackButton()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            StorageManager.getInstance(this).deleteORDERActiveDeliveryMainDo(this)
            preferenceUtils.removeFromPreference(PreferenceUtils.PENDING_DELIVERY_NUMBER)
            preferenceUtils.removeFromPreference(PreferenceUtils.PENDING_INVOICE)

            finish()
        }


        if (intent.hasExtra("shipmentId")) {
            pendingShipmentId = intent.extras.getString("shipmentId")
        }

        tvScreenTitle.setText(pendingShipmentId)
        btnDeliveryDetails.setOnClickListener {
                var intent = Intent(this@PendingOrdersPODActivity, PendingOrdersScheduledCaptureDeliveryActivity::class.java);
                intent.putExtra("shipmentId", pendingShipmentId)
                startActivityForResult(intent, 1);
        }
        btnInvoice.setOnClickListener {
            var intent = Intent(this@PendingOrdersPODActivity, PendingCreateInvoiceActivity::class.java)
            startActivityForResult(intent, 1010)
        }
        btnPayments.setOnClickListener {
            var intent = Intent(this@PendingOrdersPODActivity, PendingInvoiceListActivity::class.java)
            startActivity(intent)
        }

    }

    override fun initializeControls() {
        btnConfirmDeparture = findViewById(R.id.btnConfirmDeparture) as LinearLayout
        tvATime = findViewById(R.id.tvATime) as TextView
        tvCaptureListTime = findViewById(R.id.tvCaptureListTime) as TextView
        tvDepartureTime = findViewById(R.id.tvDepartureTime) as TextView

        btnConfirmArrival = findViewById(R.id.btnConfirmArrival) as LinearLayout
        btnDeliveryDetails = findViewById(R.id.btnDeliveryDetails) as LinearLayout
        btnValidateDelivery = findViewById(R.id.btnValidateDelivery) as Button
        btnGenerate = findViewById(R.id.btnGenerate) as LinearLayout
        btnInvoice = findViewById(R.id.btnInvoice) as LinearLayout
        btnPayments = findViewById(R.id.btnPayments) as LinearLayout

    }




    private var issuedQty: Double = 0.0
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == 1) {
            if (data!!.hasExtra("DeliveredProducts")) {//delivered product list  ArrayList<ActiveDeliveryDO>
                val deliveredProductsList = data.getSerializableExtra("DeliveredProducts") as ArrayList<ActiveDeliveryDO>
                if (deliveredProductsList != null) {
                    for (i in deliveredProductsList.indices) {
                        issuedQty = issuedQty + deliveredProductsList.get(i).orderedQuantity
                    }
                }
            }

            btnDeliveryDetails.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnDeliveryDetails.setClickable(false)
            btnDeliveryDetails.setClickable(false)


//            StorageManager.getInstance(this).saveDepartureData(this, podDo)
//            validatedelivery()

        } else if (requestCode == 98 && resultCode == 98) {


        } else if (requestCode == 2 && resultCode == 2) {
            btnGenerate.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnGenerate.setClickable(false)
            btnGenerate.setClickable(false)
        } else if (requestCode == 3 && resultCode == 3) {
            if (preferenceUtils.getStringFromPreference(PreferenceUtils.DELIVERY_NOTE, "").equals("SUCCESS", true)) {

            } else {
                showToast("You have not completed the delivery note process")
            }
        } else if (requestCode == 11 && resultCode == 11) {
//            btnReturnDetails.setBackgroundColor(resources.getColor(R.color.md_gray_light))
//            btnReturnDetails.setClickable(false)
//            btnReturnDetails.setClickable(false)

        } else if (requestCode == 1010 && resultCode == 10) {
//            podDo.capturedReturns = CalendarUtils.getCurrentDate();
            btnInvoice.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnInvoice.setClickable(false)
            btnInvoice.setClickable(false)

//            StorageManager.getInstance(this).saveDepartureData(this, podDo)
        } else if (requestCode == 120 && resultCode == Activity.RESULT_OK) {
            // Get String data from Intent
            val returnString = data!!.getStringExtra("DELIVERY_REMARKS")
            LogUtils.debug("DELIVERY_REMARKS", returnString)
        } else if (requestCode == 130 && resultCode == Activity.RESULT_OK) {
            // Get String data from Intent
            val returnString = data!!.getStringExtra("INVOICE_REMARKS")
            LogUtils.debug("INVOICE_REMARKS", returnString)
        } else if (requestCode == 13 && resultCode == Activity.RESULT_OK) {
            // Get String data from Intent
            val returnString = data!!.getStringExtra("SIGNATURE")
            val signatureEncode = data!!.getStringExtra("SignatureEncode")
            LogUtils.debug("SIGNATURE Path : ", returnString)
            LogUtils.debug("SignatureEncode : ", signatureEncode)

//            validatedelivery()
        }
    }



    override fun onButtonYesClick(from: String) {
        super.onButtonYesClick(from)

        if (from.equals("ConfirmArrival")) {

        }
    }






    override fun onResume() {
        super.onResume()
    }



    private fun moveBack() {
        val intent = Intent();
        intent.putExtra("CapturedReturns", true)
        setResult(12, intent)
        finish()
    }
    override fun onBackPressed() {
        AppConstants.CapturedReturns = false;
        StorageManager.getInstance(this).deleteORDERActiveDeliveryMainDo(this)
        preferenceUtils.removeFromPreference(PreferenceUtils.PENDING_DELIVERY_NUMBER)
        preferenceUtils.removeFromPreference(PreferenceUtils.PENDING_INVOICE)

        super.onBackPressed()
    }




}