package com.tbs.brothersgas.haadhir.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.brothersgas.haadhir.Activitys.BaseActivity;
import com.tbs.brothersgas.haadhir.Model.CashDO;
import com.tbs.brothersgas.haadhir.Model.ChequeDO;
import com.tbs.brothersgas.haadhir.Model.LoadStockDO;
import com.tbs.brothersgas.haadhir.Model.PaymentHistoryDO;
import com.tbs.brothersgas.haadhir.Model.PaymentHistoryMainDO;
import com.tbs.brothersgas.haadhir.Model.SalesInvoiceDO;
import com.tbs.brothersgas.haadhir.Model.SpotDeliveryDO;
import com.tbs.brothersgas.haadhir.Model.TransactionCashDO;
import com.tbs.brothersgas.haadhir.Model.TransactionCashMainDo;
import com.tbs.brothersgas.haadhir.Model.UserActivityReportMainDO;
import com.tbs.brothersgas.haadhir.R;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;
import com.tbs.brothersgas.haadhir.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class UserActivityReportRequest extends AsyncTask<String, Void, Boolean> {

    private UserActivityReportMainDO userActivityReportMainDO;
    private SpotDeliveryDO spotDeliveryDO;
    private SalesInvoiceDO salesInvoiceDO;
    private CashDO cashDO;
    private ChequeDO chequeDO;
    private LoadStockDO loadStockDO;

    private Context mContext;
    private String id;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;
    String transactionDate, endDATE;
    String site;


    public UserActivityReportRequest(String date, Context mContext, String site) {

        this.mContext = mContext;
        this.transactionDate = date;
        this.site = site;


    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        public void onCompleted(boolean isError, UserActivityReportMainDO userActivityReportMainDO);

    }

    public boolean runRequest() {
        // System.out.println("CUSTOMER ID " + customerId);
        String NAMESPACE = "http://www.adonix.com/WSS";
        String METHOD_NAME = "run";
        String SOAP_ACTION = "CAdxWebServiceXmlCC";
//        String URL = "http://183.82.9.23:8124/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC";
        preferenceUtils = new PreferenceUtils(mContext);
        username = preferenceUtils.getStringFromPreference(PreferenceUtils.A_USER_NAME, "");
        password = preferenceUtils.getStringFromPreference(PreferenceUtils.A_PASSWORD, "");
        ip = preferenceUtils.getStringFromPreference(PreferenceUtils.IP_ADDRESS, "");
        port = preferenceUtils.getStringFromPreference(PreferenceUtils.PORT, "");
        pool = preferenceUtils.getStringFromPreference(PreferenceUtils.ALIAS, "");

//        username = "ADMIN";
//        password = "admin";
//        ip = "47.91.105.187";
//        port = "8124";
//        pool = "BROSGST";


        String id = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "");
        String sites = preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID, "");
        String routingID = preferenceUtils.getStringFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID, "");
        String transactionID = preferenceUtils.getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, "");
        String stransactionID = preferenceUtils.getStringFromPreference(PreferenceUtils.DOC_NUMBER, "");

        String URL = "http://" + ip + ":" + port + "/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC";

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        // Set all input params
        request.addProperty("publicName", WebServiceConstants.USER_ACTIVITY_REPORT);
        JSONObject jsonObject = new JSONObject();

        try {
            String ShipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, mContext.getResources().getString(R.string.checkin_non_scheduled));
            if (ShipmentType.equals(mContext.getResources().getString(R.string.checkin_non_scheduled))) {
                jsonObject.put("I_YSCRNUM", transactionID);
            } else {
                jsonObject.put("I_YSCRNUM", stransactionID);

            }

            jsonObject.put("I_YVRNUM", routingID);
            jsonObject.put("I_YUSR", id);
            jsonObject.put("I_YTRNDATE", transactionDate);
            jsonObject.put("I_YFCY", site);

//            jsonObject.put("I_YUSR", "U102");
//            jsonObject.put("I_YTRNDATE", "20190421");
//            jsonObject.put("I_YFCY", "U102");
        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        request.addProperty("inputXml", jsonObject.toString());

        SoapObject callcontext = new SoapObject("", "callContext");
        // Set all input params
        callcontext.addProperty("codeLang", "ENG");
        callcontext.addProperty("poolAlias", pool);
        callcontext.addProperty("poolId", "");
        callcontext.addProperty("codeUser", username);
        callcontext.addProperty("password", password);
        callcontext.addProperty("requestConfig", "adxwss.trace.on=off");

        request.addSoapObject(callcontext);


        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.setOutputSoapObject(request);

        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL,60000);
        androidHttpTransport.debug = true;

        try {
            List<HeaderProperty> headerList = new ArrayList<HeaderProperty>();
            headerList.add(new HeaderProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode((username + ":" + password).getBytes())));


            androidHttpTransport.call(SOAP_ACTION, envelope, headerList);


            SoapObject response = (SoapObject) envelope.getResponse();
            String resultXML = (String) response.getProperty("resultXml");
            if (resultXML != null && resultXML.length() > 0) {
                return parseXML(resultXML);
            } else {
                return false;
            }
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            userActivityReportMainDO = new UserActivityReportMainDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    attribute = xpp.getAttributeValue(null, "ID");
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("TAB") && attribute.equalsIgnoreCase("GRP2")) {
                        preferenceUtils.saveString(PreferenceUtils.GRP, "GRP2");
                        userActivityReportMainDO.spotDeliveryDOS = new ArrayList<>();
                    } else if (startTag.equalsIgnoreCase("TAB") && attribute.equalsIgnoreCase("GRP3")) {
                        userActivityReportMainDO.salesInvoiceDOS = new ArrayList<>();
                        preferenceUtils.saveString(PreferenceUtils.GRP, "GRP3");
                    } else if (startTag.equalsIgnoreCase("TAB") && attribute.equalsIgnoreCase("GRP4")) {
                        userActivityReportMainDO.cashDOS = new ArrayList<>();
                        preferenceUtils.saveString(PreferenceUtils.GRP, "GRP4");
                    } else if (startTag.equalsIgnoreCase("TAB") && attribute.equalsIgnoreCase("GRP5")) {
                        userActivityReportMainDO.chequeDOS = new ArrayList<>();
                        preferenceUtils.saveString(PreferenceUtils.GRP, "GRP5");
                    } else if (startTag.equalsIgnoreCase("TAB") && attribute.equalsIgnoreCase("GRP6")) {
                        userActivityReportMainDO.productDOS = new ArrayList<>();
                        preferenceUtils.saveString(PreferenceUtils.GRP, "GRP6");
                    } else if (startTag.equalsIgnoreCase("TAB") && attribute.equalsIgnoreCase("GRP7")) {
                        userActivityReportMainDO.stockDOS = new ArrayList<>();
                        preferenceUtils.saveString(PreferenceUtils.GRP, "GRP7");
                    } else if (startTag.equalsIgnoreCase("TAB") && attribute.equalsIgnoreCase("GRP8")) {
                        userActivityReportMainDO.salesDOS = new ArrayList<>();
                        preferenceUtils.saveString(PreferenceUtils.GRP, "GRP8");
                    } else if (startTag.equalsIgnoreCase("TAB") && attribute.equalsIgnoreCase("GRP9")) {
                        userActivityReportMainDO.issueDOS = new ArrayList<>();
                        preferenceUtils.saveString(PreferenceUtils.GRP, "GRP9");
                    } else if (startTag.equalsIgnoreCase("TAB") && attribute.equalsIgnoreCase("GRP10")) {
                        userActivityReportMainDO.recieptDOS = new ArrayList<>();
                        preferenceUtils.saveString(PreferenceUtils.GRP, "GRP10");
                    } else if (startTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP2")) {
                        spotDeliveryDO = new SpotDeliveryDO();
                    } else if (startTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP3")) {
                        salesInvoiceDO = new SalesInvoiceDO();
                    } else if (startTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP4")) {
                        cashDO = new CashDO();
                    } else if (startTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP5")) {
                        chequeDO = new ChequeDO();
                    } else if (startTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP6")) {
                        loadStockDO = new LoadStockDO();
                    } else if (startTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP7")) {
                        loadStockDO = new LoadStockDO();
                    }
                    else if (startTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP8")) {
                        loadStockDO = new LoadStockDO();
                    }else if (startTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP9")) {
                        loadStockDO = new LoadStockDO();
                    }else if (startTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP10")) {
                        loadStockDO = new LoadStockDO();
                    }
//                    else if (startTag.equalsIgnoreCase("LIN")) {
//                        spotDeliveryDO = new SpotDeliveryDO();
//                        salesInvoiceDO = new SalesInvoiceDO();
//                        cashDO = new CashDO();
//                        chequeDO = new ChequeDO();
//
//                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {
                        if (attribute.equalsIgnoreCase("I_YUSR")) {
                            if (text.length() > 0) {

                                userActivityReportMainDO.userName = text;
                            }

                        } else if (attribute.equalsIgnoreCase("I_YTRNDATE")) {
                            if (text.length() > 0) {

                                userActivityReportMainDO.transactionDate = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YCPY")) {
                            if (text.length() > 0) {

                                userActivityReportMainDO.companyCode = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YCPYDES")) {
                            if (text.length() > 0) {

                                userActivityReportMainDO.companyDescription = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YOPNQTYFCY")) {
                            if (text.length() > 0) {

                                loadStockDO.site = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YOPNFCYDES")) {
                            if (text.length() > 0) {

                                loadStockDO.siteDescription = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YENDQTYFCY")) {
                            if (text.length() > 0) {

                                loadStockDO.site = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YENDFCYDES")) {
                            if (text.length() > 0) {

                                loadStockDO.siteDescription = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSDHNUM")) {
                            if (text.length() > 0) {

                                spotDeliveryDO.documentNumber = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSDHBPC")) {
                            if (text.length() > 0) {

                                spotDeliveryDO.customerId = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSDHBPCNAME")) {
                            if (text.length() > 0) {

                                spotDeliveryDO.customerName = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSDHITM")) {
                            if (text.length() > 0) {

                                spotDeliveryDO.product = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSDHITMDES")) {
                            if (text.length() > 0) {

                                spotDeliveryDO.productDescription = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSDHQTY")) {
                            if (text.length() > 0) {

                                spotDeliveryDO.quantity = Double.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSDHITMUN")) {
                            if (text.length() > 0) {

                                spotDeliveryDO.productUnit = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSIHNUM")) {
                            if (text.length() > 0) {

                                salesInvoiceDO.documentNumber = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSIHBPC")) {
                            if (text.length() > 0) {

                                salesInvoiceDO.customerId = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSIHBPCNAME")) {
                            if (text.length() > 0) {

                                salesInvoiceDO.customerName = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSIHITM")) {
                            if (text.length() > 0) {

                                salesInvoiceDO.product = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSIHITMDES")) {
                            if (text.length() > 0) {

                                salesInvoiceDO.productDescription = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSIHQTY")) {
                            if (text.length() > 0) {

                                salesInvoiceDO.quantity = Double.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSIHITMUN")) {
                            if (text.length() > 0) {

                                salesInvoiceDO.productUnit = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSIHAMT")) {
                            if (text.length() > 0) {

                                salesInvoiceDO.amount = Double.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSIHCUR")) {
                            if (text.length() > 0) {

                                salesInvoiceDO.currency = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YCSHPAYNUM")) {
                            if (text.length() > 0) {

                                cashDO.paymentId = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YCSHPAYBPC")) {
                            if (text.length() > 0) {

                                cashDO.customerId = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YCSHPAYBPCNAME")) {
                            if (text.length() > 0) {

                                cashDO.customerName = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YCSHPAYAMT")) {
                            if (text.length() > 0) {

                                cashDO.amount = Double.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YCSHPAYCUR")) {
                            if (text.length() > 0) {

                                cashDO.currecncy = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YCHQPAYNUM")) {
                            if (text.length() > 0) {

                                chequeDO.paymentId = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YCHQPAYBPC")) {
                            if (text.length() > 0) {

                                chequeDO.customerId = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YCHQPAYBPCNAME")) {
                            if (text.length() > 0) {

                                chequeDO.customerName = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YCHQPAYAMT")) {
                            if (text.length() > 0) {

                                chequeDO.amount = Double.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YCHQPAYCUR")) {
                            if (text.length() > 0) {

                                chequeDO.currecncy = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSDHTOTQTY")) {
                            if (text.length() > 0) {

                                userActivityReportMainDO.deliveryTotalQty = Double.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSIHTOTQTY")) {
                            if (text.length() > 0) {

                                userActivityReportMainDO.invoiceTotalQuantity = Double.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSIHTOTMAT")) {
                            if (text.length() > 0) {

                                userActivityReportMainDO.invoiceTotalAmount = Double.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YCSHTOTPAYAMT")) {
                            if (text.length() > 0) {

                                userActivityReportMainDO.cashTotalAmount = Double.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YCHQTOTPAYAMT")) {
                            if (text.length() > 0) {

                                userActivityReportMainDO.chequeTotalAmount = Double.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YOPENITM")) {
                            if (text.length() > 0) {

                                loadStockDO.product = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YOPENITMDES")) {
                            if (text.length() > 0) {

                                loadStockDO.productDescription = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YOPENITMSAU")) {
                            if (text.length() > 0) {

                                loadStockDO.stockUnit = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YOPENITMWEU")) {
                            if (text.length() > 0) {

                                loadStockDO.weightUnit = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YOPENITMQTY")) {
                            if (text.length() > 0) {

                                loadStockDO.quantity = Double.parseDouble(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YTOTOPENQTY")) {
                            if (text.length() > 0) {

                                userActivityReportMainDO.productsTotalQty = Double.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YTOTENDQTY")) {
                            if (text.length() > 0) {

                                userActivityReportMainDO.stockTotalQty = Double.valueOf(text);
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YTOTSALQTY")) {
                            if (text.length() > 0) {

                                userActivityReportMainDO.salesTotalQty = Double.valueOf(text);
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YTOTISSQTY")) {
                            if (text.length() > 0) {

                                userActivityReportMainDO.issueTotalQty = Double.valueOf(text);
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YTOTRCTQTY")) {
                            if (text.length() > 0) {

                                userActivityReportMainDO.recieptTotalQty = Double.valueOf(text);
                            }

                        }else if (attribute.equalsIgnoreCase("O_YENDITM")) {
                            if (text.length() > 0) {

                                loadStockDO.product = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YENDITMDES")) {
                            if (text.length() > 0) {

                                loadStockDO.productDescription = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YENDITMSAU")) {
                            if (text.length() > 0) {

                                loadStockDO.stockUnit = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YENDITWEU")) {
                            if (text.length() > 0) {

                                loadStockDO.weightUnit = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YENDITMQTY")) {
                            if (text.length() > 0) {

                                loadStockDO.quantity = Double.parseDouble(text);
                            }

                        }

                        else if (attribute.equalsIgnoreCase("O_YSALES_ITMDES")) {
                            if (text.length() > 0) {

                                loadStockDO.productDescription = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YEMPTYISS_ITMDES")) {
                            if (text.length() > 0) {

                                loadStockDO.productDescription = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YEMPTYRPT_ITMDES")) {
                            if (text.length() > 0) {

                                loadStockDO.productDescription = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YSALES_ITQTY")) {
                            if (text.length() > 0) {

                                loadStockDO.quantity = Double.parseDouble(text);
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YEMPTYISS_ITMQTY")) {
                            if (text.length() > 0) {

                                loadStockDO.quantity = Double.parseDouble(text);
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YEMPTYRPT_ITMQTY")) {
                            if (text.length() > 0) {

                                loadStockDO.quantity = Double.parseDouble(text);
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YSALES_ITMQTYUN")) {
                            if (text.length() > 0) {

                                loadStockDO.stockUnit = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YEMPTYISS_QTYUN")) {
                            if (text.length() > 0) {

                                loadStockDO.stockUnit = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YEMPTYRPT_QTYUN")) {
                            if (text.length() > 0) {

                                loadStockDO.stockUnit = text;
                            }

                        }else if (attribute.equalsIgnoreCase("O_YEMPTYISS_DOC")) {
                            if (text.length() > 0) {

                                loadStockDO.shipmentNumber = text;
                            }

                        }  else if (attribute.equalsIgnoreCase("O_YEMPTYRPT_DOC")) {
                            if (text.length() > 0) {

                                loadStockDO.shipmentNumber = text;
                            }

                        }




                        else if (attribute.equalsIgnoreCase("O_YFLG")) {
                            if (text.length() > 0) {

                                userActivityReportMainDO.flag = Integer.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YMESSAGE")) {
                            if (text.length() > 0) {

                                userActivityReportMainDO.message = text;
                            }

                        }
                        text = "";

                    }

                    if (endTag.equalsIgnoreCase("GRP")) {
                    } else if (endTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP2")) {
                        userActivityReportMainDO.spotDeliveryDOS.add(spotDeliveryDO);
                    } else if (endTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP3")) {
                        userActivityReportMainDO.salesInvoiceDOS.add(salesInvoiceDO);
                    } else if (endTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP4")) {
                        userActivityReportMainDO.cashDOS.add(cashDO);
                    } else if (endTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP5")) {
                        userActivityReportMainDO.chequeDOS.add(chequeDO);
                    } else if (endTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP6")) {
                        userActivityReportMainDO.productDOS.add(loadStockDO);
                    } else if (endTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP7")) {
                        userActivityReportMainDO.stockDOS.add(loadStockDO);
                    }
                    else if (endTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP8")) {
                        userActivityReportMainDO.salesDOS.add(loadStockDO);
                    }else if (endTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP9")) {
                        userActivityReportMainDO.issueDOS.add(loadStockDO);
                    }else if (endTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP10")) {
                        userActivityReportMainDO.recieptDOS.add(loadStockDO);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        ((BaseActivity) mContext).showLoader();
        // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        ((BaseActivity) mContext).hideLoader();

        //  ProgressTask.getInstance().closeProgress();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, userActivityReportMainDO);
        }
    }
}