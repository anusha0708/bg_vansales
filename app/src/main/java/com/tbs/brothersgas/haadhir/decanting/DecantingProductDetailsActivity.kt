package com.tbs.brothersgas.haadhir.decanting

import android.content.Intent
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import com.tbs.brothersgas.haadhir.Activitys.BaseActivity
import com.tbs.brothersgas.haadhir.Adapters.ProductAdapter
import com.tbs.brothersgas.haadhir.Adapters.ScheduledProductsAdapter
import com.tbs.brothersgas.haadhir.Adapters.SpotScheduledProductsAdapter
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryDO
import com.tbs.brothersgas.haadhir.Model.AddressDO
import com.tbs.brothersgas.haadhir.Model.CustomerDo
import com.tbs.brothersgas.haadhir.Model.ProductDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import kotlinx.android.synthetic.main.decanting_order_details.*
import java.util.*

class DecantingProductDetailsActivity : BaseActivity() {
    var shipmentId: String = ""
    var customerCode: String = ""
    lateinit var userId: String
    lateinit var orderCode: String
    lateinit var llOrderHistory: View
    lateinit var customerDos: ArrayList<CustomerDo>
 lateinit var customerDo:CustomerDo
    lateinit var addressDO: AddressDO
    lateinit var llSearch: LinearLayout
    lateinit var etSearch: EditText
    lateinit var loadStockAdapter:SpotScheduledProductsAdapter
    var startDate = ""
    var endDate = ""
    var to = ""
    var type=0
    lateinit var productDos: ArrayList<ActiveDeliveryDO>

    override fun initialize() {
        llOrderHistory = layoutInflater.inflate(R.layout.decanting_order_details, null)
        llBody.addView(llOrderHistory, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            val intent = Intent()
            setResult(1, intent)
            finish()
        }

        disableMenuWithBackButton()
        initializeControls()
        tvScreenTitle.setText("Select Product")
        llDetails.visibility = View.GONE
        getList();

        btn_proceed_sign.setOnClickListener {
            createDelivery()
        }

    }

    private fun createDelivery() {
            var site=preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID,"")

        if (Util.isNetworkAvailable(this)) {
               if(loadStockAdapter!=null&&loadStockAdapter.selectedactiveDeliveryDOs!=null&&loadStockAdapter.selectedactiveDeliveryDOs.size>0){
                   var createDeliveryRequest = DecantingSpotCreationRequest(customerDo.customerId,site,addressDO.addressCode, loadStockAdapter.selectedactiveDeliveryDOs,this)

                   createDeliveryRequest.setOnResultListener { isError, deliveryMainDo, message ->
                       hideLoader()
                       var createDeliveryMAinDo = deliveryMainDo
                       if (isError) {

                           hideLoader()

                           showAlert(resources.getString(R.string.server_error))
                       } else {
                           if (createDeliveryMAinDo.status == 2) {
                               if (createDeliveryMAinDo.miscNumber.length > 0) {
                                   if (createDeliveryMAinDo.message.length > 0) {
                                       showAppCompatAlert("Info!", "" + createDeliveryMAinDo.message + "-"
                                               + createDeliveryMAinDo.miscNumber,
                                               "Ok", "", "SUCCESS", false)

                                   }

                               } else {
                                   if (createDeliveryMAinDo.message.length > 0) {
                                       showAppCompatAlert("Error", "" + createDeliveryMAinDo.message, "Ok", "", "FAILURE", false)

                                   } else {
                                       showAppCompatAlert("Error", "Document Not Created ", "Ok", "", "FAILURE", false)

                                   }


                               }

//
//                          val driverAdapter = CreateDeliveryProductAdapter(this, deliveryMainDo.nonSheduledProductDOS)
//                          recycleview.setAdapter(driverAdapter)
                           } else {
                               hideLoader()
                               if (createDeliveryMAinDo.message.length > 0) {
                                   showAppCompatAlert("Error", "" + createDeliveryMAinDo.message, "Ok", "", "FAILURE", false)

                               } else {
                                   showAppCompatAlert("Error", "Document Not Created", "Ok", "", "FAILURE", false)

                               }

                           }
                       }
                   }
                   createDeliveryRequest.execute()
               }else{
                   showToast("Kindly select products to proceed")
               }

        } else {
            showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

        }

    }

    private fun getList() {

        if (Util.isNetworkAvailable(this)) {
            val driverListRequest = DecantingProductListRequest(this@DecantingProductDetailsActivity)
            driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                hideLoader()
                if (isError) {
                    llDetails.visibility = View.GONE
                    showAlert(resources.getString(R.string.server_error))
                } else {
                    productDos = activeDeliveryDo

                    llDetails.visibility = View.GONE
//                    var   productAdapter = ProductAdapter(this@DecantingProductDetailsActivity, productDos)
//                    recycleview.adapter = productAdapter
                    tvNoOrders.visibility = View.GONE
                    recycleview.visibility = View.VISIBLE
//                    tvShipmentId.text = "" + activeDeliveryDo.shipmentNumber
//                    tvSite.text = "" + activeDeliveryDo.site
//                    tvAddress.text = "" + activeDeliveryDo.siteAddress1


                     loadStockAdapter = SpotScheduledProductsAdapter(this@DecantingProductDetailsActivity, productDos, "AddProducts", "", 0)
                    recycleview.setAdapter(loadStockAdapter)
                }
            }
            driverListRequest.execute()

        } else {
            showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

        }
    }


    override fun initializeControls() {
        if (intent.hasExtra("customerDO")) {
            customerDo = intent.extras!!.getSerializable("customerDO") as CustomerDo
        }
        if (intent.hasExtra("addressDO")) {
            addressDO = intent.extras!!.getSerializable("addressDO") as AddressDO
        }
    }
    override fun onButtonYesClick(from: String) {
        super.onButtonYesClick(from)
        if ("SUCCESS".equals(from, ignoreCase = true)) {
            val intent = Intent()
            setResult(1, intent)
            finish()
        }

    }

}
