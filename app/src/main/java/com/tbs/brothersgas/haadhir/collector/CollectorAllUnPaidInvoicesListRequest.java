package com.tbs.brothersgas.haadhir.collector;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.brothersgas.haadhir.Activitys.BaseActivity;
import com.tbs.brothersgas.haadhir.Model.CustomerDo;
import com.tbs.brothersgas.haadhir.Model.UnPaidInvoiceDO;
import com.tbs.brothersgas.haadhir.Model.UnPaidInvoiceMainDO;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;
import com.tbs.brothersgas.haadhir.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class CollectorAllUnPaidInvoicesListRequest extends AsyncTask<String, Void, Boolean> {

    private UnPaidInvoiceMainDO unPaidInvoiceMainDO;
    private UnPaidInvoiceDO unPaidInvoiceDO;
    private Context mContext;
    private String id;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;
    String customer;

    ArrayList<CustomerDo> customerDos;

    public CollectorAllUnPaidInvoicesListRequest(ArrayList<CustomerDo> customer, Context mContext) {

        this.mContext = mContext;
        this.customerDos = customer;


    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        public void onCompleted(boolean isError, UnPaidInvoiceMainDO unPaidInvoiceMainDO);

    }

    public boolean runRequest() {
        // System.out.println("CUSTOMER ID " + customerId);
        String NAMESPACE = "http://www.adonix.com/WSS";
        String METHOD_NAME = "run";
        String SOAP_ACTION = "CAdxWebServiceXmlCC";
//        String URL = "http://183.82.9.23:8124/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC";
        preferenceUtils = new PreferenceUtils(mContext);
        username        = preferenceUtils.getStringFromPreference(PreferenceUtils.A_USER_NAME, "");
        password        = preferenceUtils.getStringFromPreference(PreferenceUtils.A_PASSWORD, "");
        ip              = preferenceUtils.getStringFromPreference(PreferenceUtils.IP_ADDRESS, "");
        port            = preferenceUtils.getStringFromPreference(PreferenceUtils.PORT, "");
        pool            = preferenceUtils.getStringFromPreference(PreferenceUtils.ALIAS, "");

        String URL = "http://"+ip+":"+port+"/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC";

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        // Set all input params
        request.addProperty("publicName", WebServiceConstants.All_UNPAID_INVOICES_LIST);
        JSONObject jsonObject = new JSONObject();

        try {

//            jsonObject.put("I_YBPC", "ZA002");
            if (customerDos != null && customerDos.size() > 0) {
//                for (int i = 0; i < customerDos.size(); i++) {
//                    JSONObject jsonObject1 = new JSONObject();
//
//
//                 /*jsonArray.put("I_YQTY",createDeliveryDoS.get(i).totalQuantity);
//                    jsonArray.put("I_YITMREF",createDeliveryDoS.get(i).item);*/
//                }
                jsonObject.put("I_YBPC", customerDos.get(0).customerId);

            }

        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        request.addProperty("inputXml", jsonObject.toString());

        SoapObject callcontext = new SoapObject("", "callContext");
        // Set all input params
        callcontext.addProperty("codeLang", "ENG");
        callcontext.addProperty("poolAlias", pool);
        callcontext.addProperty("poolId", "");
        callcontext.addProperty("codeUser", username);
        callcontext.addProperty("password", password);
        callcontext.addProperty("requestConfig", "adxwss.trace.on=off");

        request.addSoapObject(callcontext);


        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.setOutputSoapObject(request);

        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL,500000);
        androidHttpTransport.debug = true;

        try {
            List<HeaderProperty> headerList = new ArrayList<HeaderProperty>();
            headerList.add(new HeaderProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode((username + ":" + password).getBytes())));


            androidHttpTransport.call(SOAP_ACTION, envelope, headerList);


            SoapObject response = (SoapObject) envelope.getResponse();
            String resultXML = (String) response.getProperty("resultXml");
            if (resultXML != null && resultXML.length() > 0) {
                return parseXML(resultXML);
            } else {
                return false;
            }
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("Unpaid xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            unPaidInvoiceMainDO = new UnPaidInvoiceMainDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        unPaidInvoiceMainDO.unPaidInvoiceDOS = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        unPaidInvoiceDO = new UnPaidInvoiceDO();

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {
                        if (attribute.equalsIgnoreCase("I_YBPC")) {
                            unPaidInvoiceMainDO.customer = text;


                        }
                        else if (attribute.equalsIgnoreCase("O_YOTOTAMT")) {
                            if(text.length()>0){

                                unPaidInvoiceMainDO.totalAmount = Double.valueOf(text);
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YPAYTYP")) {
                            if(text.length()>0){

                                unPaidInvoiceMainDO.type = Integer.valueOf(text);
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YBPCDES")) {
                            unPaidInvoiceDO.customerId = text;

                        }else if (attribute.equalsIgnoreCase("O_YSIH")) {
                            unPaidInvoiceDO.invoiceId = text;

                        }
                        else if (attribute.equalsIgnoreCase("O_YDATE")) {
                            unPaidInvoiceDO.accountingDate = text;

                        }
                        else if (attribute.equalsIgnoreCase("O_YAMT")) {
                            unPaidInvoiceDO.amount = Double.valueOf(text);

                        }

                        else if (attribute.equalsIgnoreCase("O_YDUDAYS")) {
                            unPaidInvoiceDO.dueDays = Integer.parseInt(text);

                        }
                        else if (attribute.equalsIgnoreCase("O_YOAMT")) {
                            unPaidInvoiceDO.outstandingAmount = Double.parseDouble(text);

                        }
                        else if (attribute.equalsIgnoreCase("O_YOCUR")) {
                            unPaidInvoiceDO.outstandingAmountCurrency = text;

                        }
                        else if (attribute.equalsIgnoreCase("O_YPAMT")) {
                            unPaidInvoiceDO.paidAmount = Double.parseDouble(text);

                        }
                        else if (attribute.equalsIgnoreCase("O_YPACUR")) {
                            unPaidInvoiceDO.paidAmountCurrency = text;

                        }
                        else if (attribute.equalsIgnoreCase("O_YFCY")) {
                            unPaidInvoiceDO.site = text;

                        }
                        else if (attribute.equalsIgnoreCase("O_YFCYDES")) {
                            unPaidInvoiceDO.siteId = text;

                        }
                        else if (attribute.equalsIgnoreCase("O_YCUR")) {
                            unPaidInvoiceDO.currency = text;

                        }

                    }

                    if (endTag.equalsIgnoreCase("GRP")) {
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        unPaidInvoiceMainDO.unPaidInvoiceDOS.add(unPaidInvoiceDO);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    if(xpp.getText().length()>0){

                        text = xpp.getText();
                    }else {
                        text="";
                    }
                }


                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
//        ((BaseActivity)mContext).showLoader();
       // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

      //  ProgressTask.getInstance().closeProgress();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, unPaidInvoiceMainDO);
        }
//        ((BaseActivity)mContext).hideLoader();

    }
}