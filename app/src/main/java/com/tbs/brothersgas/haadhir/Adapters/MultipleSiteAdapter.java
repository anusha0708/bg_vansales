package com.tbs.brothersgas.haadhir.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.tbs.brothersgas.haadhir.Activitys.CreatePaymentActivity;
import com.tbs.brothersgas.haadhir.Activitys.OnAccountCreatePaymentActivity;
import com.tbs.brothersgas.haadhir.Model.BankDO;
import com.tbs.brothersgas.haadhir.Model.FinantialSiteDO;
import com.tbs.brothersgas.haadhir.R;
import com.tbs.brothersgas.haadhir.listeners.ResultListner;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;

import java.util.ArrayList;

public class MultipleSiteAdapter extends RecyclerView.Adapter<MultipleSiteAdapter.MyViewHolder> {

    private ArrayList<FinantialSiteDO> finantialSiteDOS;
    private Context context;
    private ResultListner resultListner;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName;

        public MyViewHolder(View view) {
            super(view);
            tvName = (TextView) view.findViewById(R.id.tvName);


        }
    }


    public MultipleSiteAdapter(Context context, ArrayList<FinantialSiteDO> finantialSiteDOS, ResultListner resultListner) {
        this.context = context;
        this.finantialSiteDOS = finantialSiteDOS;
        this.resultListner = resultListner;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.simple_text, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final FinantialSiteDO finantialSiteDO = finantialSiteDOS.get(position);

        holder.tvName.setText(finantialSiteDO.site + " - " + finantialSiteDO.siteDescription);

        holder.itemView.setOnClickListener(view -> {
            resultListner.onResultListner(finantialSiteDO.site, true);
        });


    }

    @Override
    public int getItemCount() {
        return finantialSiteDOS.size();
    }

}
