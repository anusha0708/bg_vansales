package com.tbs.brothersgas.haadhir.Activitys

import android.content.Intent
import androidx.appcompat.app.AppCompatDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import com.tbs.brothersgas.haadhir.Adapters.LoanReturnAdapter
import com.tbs.brothersgas.haadhir.Adapters.SalesReturnAdapter
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryDO
import com.tbs.brothersgas.haadhir.Model.CustomerDo
import com.tbs.brothersgas.haadhir.Model.LoadStockDO
import com.tbs.brothersgas.haadhir.Model.LoanReturnDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.*
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.utils.CalendarUtils
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import org.json.JSONArray
import org.json.JSONObject
import kotlin.collections.ArrayList
import kotlin.collections.LinkedHashMap

//
class SelectedSalesReturnDetailsActivity : BaseActivity() {
    lateinit var invoiceAdapter: SalesReturnAdapter
    lateinit var loadStockDOs: ArrayList<LoadStockDO>
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var tvNoDataFound: TextView
    lateinit var tvBalanceQty: TextView
    lateinit var tvReturnedQty: TextView
    lateinit var tvIssuedQty: TextView
    lateinit var tvOpeningQty: TextView
    lateinit var openingQty: String
    private var issuedQty: Int = 0
    private var type: Int = 1
    lateinit var btnRequestApproval: Button
    lateinit var tvApprovalStatus: TextView
    private var returnQty: Double = 0.0
    private var balanceQty: Double = 0.0
    private lateinit var btnConfirm: Button
    private lateinit var btnComment: Button

    private lateinit var rbEmptyCylinder: RadioButton
    private lateinit var rbSalesReturn: RadioButton
    private var loanReturnDOsMap: LinkedHashMap<String, ArrayList<LoanReturnDO>> = LinkedHashMap()
    private var loanReturnDos: ArrayList<LoanReturnDO> = ArrayList()
    private var isSentForApproval: Boolean = false

    override fun initialize() {
        val llCategories = getLayoutInflater().inflate(R.layout.selected_salesreturn_details, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            if (isSentForApproval) {
                showToast("Please process the order..!")
            } else {
                finish()

            }
        }
//        ivAdd.setVisibility(View.VISIBLE)
        ivRefresh.visibility = View.VISIBLE
        if (intent.hasExtra("IssuedQty"))
            issuedQty = intent.extras.getInt("IssuedQty", 0)

        if (intent.hasExtra("Products")) {
            loanReturnDos = intent.getSerializableExtra("Products") as ArrayList<LoanReturnDO>
        }
        if (intent.hasExtra("openingQty")) {
            openingQty = intent.extras.getString("openingQty", "")

        }

        initializeControls()
        val rgType = findViewById(R.id.rgType) as RadioGroup

        rgType.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {
                when (checkedId) {
                    R.id.rbEmptyCylinder -> {
                        type = 1
                        refreshAdapter(type)
                    }
                    R.id.rbSalesReturn -> {
                        type = 2
                        refreshAdapter(type)
                    }

                }
            }
        })
        tvOpeningQty.setText("" + openingQty)
        tvIssuedQty.setText("" + issuedQty)

        loanReturnDOsMap = LinkedHashMap<String, ArrayList<LoanReturnDO>>();

        bindData(loanReturnDos);

        btnConfirm = findViewById(R.id.btnConfirm) as Button
        btnRequestApproval = findViewById(R.id.btnRequestApproval) as Button
        tvApprovalStatus = findViewById(R.id.tvApprovalStatus) as TextView
        btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
        btnConfirm.isClickable = false
        btnConfirm.isEnabled = false

//        invoiceAdapter = InvoiceAdapter(this@SalesInvoiceActivity,  null)
//        recycleview.setAdapter(invoiceAdapter)
//
        btnRequestApproval.setOnClickListener {

            approvalMechanismRequst("", "", 4)


        }
        ivRefresh.setOnClickListener {
            if (tvApprovalStatus.getText().toString().contains("Requested")) {
                preferenceUtils.saveString(PreferenceUtils.PRODUCT_APPROVAL, tvApprovalStatus.getText().toString())
//                updateStatus()
                approvalMechanismCheckStatus()
            }
        }

        btnConfirm.setOnClickListener {
            if (loanReturnDOsMap != null && loanReturnDOsMap.size > 0) {
                StorageManager.getInstance(this).saveReturnCylinders(this, loanReturnDos)
                val returnCount = StorageManager.getInstance(this).saveAllReturnStockValue(loanReturnDos);
                if (returnCount > 0) {
                    var recievedSite = preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID, "")
                    var vehicleCode = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_CODE, "")

                    val customer = preferenceUtils.getStringFromPreference(PreferenceUtils.CUSTOMER, "")
                    if (Util.isNetworkAvailable(this)) {
                        val siteListRequest = CreateSalesReturnRequest(recievedSite, customer, vehicleCode, type, loanReturnDOsMap, this@SelectedSalesReturnDetailsActivity)
                        showLoader()
                        siteListRequest.setOnResultListener { isError, loanReturnMainDo ->
                            hideLoader()
                            if (loanReturnMainDo != null) {
                                if (isError) {
                                    showToast("Unable to Create Sales Return")
                                } else {
                                    if (loanReturnMainDo.status == 20) {
                                        val intent = Intent()
                                        intent.putExtra("CapturedReturns", true)
                                        setResult(11, intent)
                                        showToast("Sales Return Created")

                                        finish()
                                    } else {
                                        showToast("Unable to Create Sales Return")
                                    }

                                }
                            } else {
                                showToast("Unable to Create Sales Return")
                            }

                        }
                        siteListRequest.execute()

                    } else {
                        showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

                    }


                } else {
                    showToast("Unable to Create Loan")
                }
            }
        }


        setReturnQty(loanReturnDos)
    }

    private fun refreshAdapter(type: Int) {
        if (type == 2) {
            if (invoiceAdapter == null) {
                invoiceAdapter = SalesReturnAdapter(this@SelectedSalesReturnDetailsActivity, loanReturnDOsMap, "")
                recycleview.adapter = invoiceAdapter
            } else {
                invoiceAdapter.refreshAdapter(loanReturnDOsMap, "");
            }
        } else {
            if (invoiceAdapter == null) {
                invoiceAdapter = SalesReturnAdapter(this@SelectedSalesReturnDetailsActivity, loanReturnDOsMap, "salesreturn")
                recycleview.adapter = invoiceAdapter
            } else {
                invoiceAdapter.refreshAdapter(loanReturnDOsMap, "salesreturn");
            }
        }
    }

    private fun setReturnQty(loanReturnDos: ArrayList<LoanReturnDO>) {
        if (loanReturnDos != null && loanReturnDos.size > 0) {
            for (i in loanReturnDos.indices) {
                returnQty = returnQty + loanReturnDos.get(i).qty
            }
        }
        tvReturnedQty.setText("" + returnQty)
        openingQty.toDouble()
        tvBalanceQty.setText("" + (openingQty.toDouble() + issuedQty - returnQty))
        balanceQty = tvBalanceQty.text.toString().toDouble()
    }

    override fun initializeControls() {
        tvScreenTitle.setText("Sales Return")
        recycleview = findViewById(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
        tvNoDataFound = findViewById(R.id.tvNoDataFound) as TextView
        tvOpeningQty = findViewById(R.id.tvOpeningQty) as TextView
        tvIssuedQty = findViewById(R.id.tvIssuedQty) as TextView
        tvReturnedQty = findViewById(R.id.tvReturnedQty) as TextView
        tvBalanceQty = findViewById(R.id.tvBalanceQty) as TextView
        recycleview.setLayoutManager(linearLayoutManager)
        btnConfirm = findViewById(R.id.btnConfirm) as Button
        rbEmptyCylinder = findViewById(R.id.rbEmptyCylinder) as RadioButton
        rbSalesReturn = findViewById(R.id.rbSalesReturn) as RadioButton
//        btnComment = findViewById(R.id.btnComment) as Button
//        btnComment.setOnClickListener {
//            showAddItemDialog()
//
//        }


//        showLoader()
//        val driverListRequest = LoanReturnRequest(this@SelectedReturnDetailsActivity)
//        driverListRequest.setOnResultListener { isError, invoiceHistoryMainDOs ->
//            hideLoader()
//            if (isError) {
//                tvNoDataFound.setVisibility(View.VISIBLE)
//                recycleview.setVisibility(View.GONE)
//                Toast.makeText(this@SelectedReturnDetailsActivity, R.string.error_customer_list, Toast.LENGTH_SHORT).show()
//            }
//            else {
//
//
//            }
//        }
//        driverListRequest.execute()

    }

    private fun bindData(loanReturnDos: ArrayList<LoanReturnDO>) {

        val shipmentIds = ArrayList<String>()
        if (loanReturnDos != null && loanReturnDos.size > 0) {
            for (i in loanReturnDos.indices) {
                if (!shipmentIds.contains(loanReturnDos.get(i).shipmentNumber)) {
                    shipmentIds.add(loanReturnDos.get(i).shipmentNumber)
                }
            }
            setReturnQty(loanReturnDos)
        }
        if (shipmentIds.size > 0) {
            for (j in shipmentIds.indices) {
                val returnList = ArrayList<LoanReturnDO>()
                for (i in loanReturnDos.indices) {
                    if (loanReturnDos.get(i).shipmentNumber.equals(shipmentIds.get(j))) {
                        returnList.add(loanReturnDos.get(i))
                    }
                }
                loanReturnDOsMap.put(shipmentIds.get(j), returnList);
            }
        }
        if (loanReturnDOsMap.size > 0) {
            tvNoDataFound.setVisibility(View.GONE)
            recycleview.setVisibility(View.VISIBLE)
            invoiceAdapter = SalesReturnAdapter(this@SelectedSalesReturnDetailsActivity, loanReturnDOsMap, "salesreturn")
            recycleview.adapter = invoiceAdapter
        } else {
            tvNoDataFound.setVisibility(View.VISIBLE)
            recycleview.setVisibility(View.GONE)
            btnConfirm.setVisibility(View.GONE)
        }

    }

    private fun showAddItemDialog() {
        try {
            var dialog = AppCompatDialog(this, R.style.AppCompatAlertDialogStyle)
            dialog.supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
            val view = LayoutInflater.from(this).inflate(R.layout.comment_custom, null)
//            applyFont(view, AppConstants.MONTSERRAT_REGULAR_TYPE_FACE)
            var etAdd = view.findViewById<View>(R.id.etAdd) as EditText

            dialog.setCancelable(true)
            dialog.setCanceledOnTouchOutside(true)
            val btnSubmit = view.findViewById<View>(R.id.btnSubmit) as Button




            btnSubmit.setOnClickListener {
                val itemName = etAdd.getText().toString().trim()

                if (itemName.equals("", ignoreCase = true)) {
                    showToast("Please Enter Comment")
                } else {

                    preferenceUtils.saveString(PreferenceUtils.COMMENT_ID, itemName);
                    dialog.dismiss()
                }
            }
            dialog.setContentView(view)
            if (!dialog.isShowing())
                dialog.show()
        } catch (e: Exception) {
        }

    }


    private fun approvalMechanismCheckStatus() {
        val approvalRequest = ApprovalMechanismCheckStatusRequest(this)
        approvalRequest.setOnResultListener { isError, approvalDO ->
            hideLoader()
            if (approvalDO != null) {
                if (isError) {
                    Toast.makeText(this@SelectedSalesReturnDetailsActivity, "Approval under progress", Toast.LENGTH_SHORT).show()
                } else {
//                    approvalDO.status = "Approved"
                    if (approvalDO.flag == 20) {
//                        loadDeliveryData()
                        isSentForApproval = true;
                        tvApprovalStatus.setText("Status : " + approvalDO.status)
                        btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
                        btnConfirm.isClickable = true
                        btnConfirm.isEnabled = true
                        btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        btnRequestApproval.isClickable = false
                        btnRequestApproval.isEnabled = false


                    } else if (approvalDO.flag == 30) {
//                        rejectProducts()
                        isSentForApproval = false;
//                        loadDeliveryData()
                        tvApprovalStatus.setText("Status : " + approvalDO.status)
                        btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        btnConfirm.isClickable = false
                        btnConfirm.isEnabled = false
                        btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        btnRequestApproval.isClickable = false
                        btnRequestApproval.isEnabled = false

                    } else {
                        Toast.makeText(this@SelectedSalesReturnDetailsActivity, approvalDO.status, Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
        approvalRequest.execute()
    }


    private fun approvalMechanismRequst(rescheduleDate: String, comments: String, docType: Int) {
        if (Util.isNetworkAvailable(this)) {

            var customer = ""
            val appUser = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "")
            val site = preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID, "")
            val shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
            val stransactionID = preferenceUtils.getStringFromPreference(PreferenceUtils.DOC_NUMBER, "")
            var activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)
            var activeDeliveryDOS = java.util.ArrayList<ActiveDeliveryDO>()
            val ShipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, getResources().getString(R.string.checkin_non_scheduled))
            if (ShipmentType.equals(getResources().getString(R.string.checkin_non_scheduled), true)) {
                var customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)
                customer = customerDo.customerId
            } else {
                customer = activeDeliverySavedDo.customer
            }
            val keySet = ArrayList<String>(loanReturnDOsMap.keys)

            for (q in 0 until loanReturnDOsMap.size) {
                val loanReturnDos = loanReturnDOsMap.get(keySet.get(q))
                if (loanReturnDos != null && loanReturnDos!!.size > 0) {
                    for (i in loanReturnDos!!.indices) {
                        val activeDeliveryDO = ActiveDeliveryDO()
                        activeDeliveryDO.shipmentId = loanReturnDos!!.get(i).shipmentNumber
                        activeDeliveryDO.line = loanReturnDos!!.get(i).lineNumber
                        activeDeliveryDO.product = loanReturnDos!!.get(i).productName
                        activeDeliveryDO.orderedQuantity = loanReturnDos!!.get(i).qty
                        activeDeliveryDOS.add(activeDeliveryDO)
                    }
                }

            }

            val approvalMechanismRequst = ApprovalMechanismRequestedRequest(stransactionID, appUser, customer
                    , site, shipmentId, rescheduleDate, comments, docType, activeDeliveryDOS, this@SelectedSalesReturnDetailsActivity)
            approvalMechanismRequst.setOnResultListener { isError, approveDO ->
                hideLoader()
                if (approveDO != null) {
                    if (isError) {
                        Log.e("RequestAproval", "Response : " + isError)
                    } else {
                        btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        btnRequestApproval.isClickable = false
                        btnRequestApproval.isEnabled = false
                        if (approveDO.flag == 20) {
                            tvApprovalStatus.setText("Status : Requested")
                            isSentForApproval = true;
                            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                            btnRequestApproval.isClickable = false
                            btnRequestApproval.isEnabled = false

                            preferenceUtils.saveString(PreferenceUtils.REQUESTED_NUMBER, approveDO.requestedNumber)

                        } else {
                            Toast.makeText(this@SelectedSalesReturnDetailsActivity, approveDO.message, Toast.LENGTH_SHORT).show()
                            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_green))
                            btnRequestApproval.isClickable = true
                            btnRequestApproval.isEnabled = true
                            tvApprovalStatus.setText("Status : Request Not Sent")
                        }

                    }
                }
            }
            approvalMechanismRequst.execute()
        } else {
            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "", false)

        }


    }


}