package com.tbs.brothersgas.haadhir.prints;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.aidcapps.arabic864.arabic864;
import com.honeywell.mobility.print.LinePrinter;
import com.honeywell.mobility.print.LinePrinterException;
import com.honeywell.mobility.print.PrintProgressEvent;
import com.honeywell.mobility.print.PrintProgressListener;
import com.honeywell.mobility.print.PrinterException;
import com.itextpdf.text.Phrase;
import com.tbs.brothersgas.haadhir.Activitys.BaseActivity;
import com.tbs.brothersgas.haadhir.Activitys.SignatureActivity;
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryDO;
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryMainDO;
import com.tbs.brothersgas.haadhir.Model.CashDO;
import com.tbs.brothersgas.haadhir.Model.ChequeDO;
import com.tbs.brothersgas.haadhir.Model.CreateInvoicePaymentDO;
import com.tbs.brothersgas.haadhir.Model.CustomerDo;
import com.tbs.brothersgas.haadhir.Model.CylinderIssueDO;
import com.tbs.brothersgas.haadhir.Model.CylinderIssueMainDO;
import com.tbs.brothersgas.haadhir.Model.LoadStockDO;
import com.tbs.brothersgas.haadhir.Model.PaymentDO;
import com.tbs.brothersgas.haadhir.Model.PaymentPdfDO;
import com.tbs.brothersgas.haadhir.Model.PdfInvoiceDo;
import com.tbs.brothersgas.haadhir.Model.PodDo;
import com.tbs.brothersgas.haadhir.Model.ProductDO;
import com.tbs.brothersgas.haadhir.Model.SalesInvoiceDO;
import com.tbs.brothersgas.haadhir.Model.SpotDeliveryDO;
import com.tbs.brothersgas.haadhir.Model.TransactionCashDO;
import com.tbs.brothersgas.haadhir.Model.TransactionCashMainDo;
import com.tbs.brothersgas.haadhir.Model.TransactionChequeDO;
import com.tbs.brothersgas.haadhir.Model.TransactionChequeMainDo;
import com.tbs.brothersgas.haadhir.Model.UserActivityReportMainDO;
import com.tbs.brothersgas.haadhir.R;
import com.tbs.brothersgas.haadhir.common.AppConstants;
import com.tbs.brothersgas.haadhir.database.StorageManager;
import com.tbs.brothersgas.haadhir.pdfs.utils.PDFOperations;
import com.tbs.brothersgas.haadhir.utils.CalendarUtils;
import com.tbs.brothersgas.haadhir.utils.NumberToWord;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;
import com.tbs.brothersgas.haadhir.utils.Util;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class PrintTask extends AsyncTask<String, Integer, String> {

    private static final String TAG = "PrintTask";
    private Context context;
    private PrintConfigDo printConfigDo;
    private String sResult = "";
    private Object object;
    private String driverId = "", driverName = "";
    private static final String printData1Start = "\u001BEZ{PRINT:\n";
    private static final String printDataEnd = "}{LP}";// + "\n" + "\n" + "\n";


    public PrintTask(Context context, PrintConfigDo printConfigDo) {
        this.context = context;
        this.driverId = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "");
        this.driverName = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.LOGIN_DRIVER_NAME, "");
        driverId = driverId.substring(0, Math.min(driverId.length(), 30));
        driverName = driverName.substring(0, Math.min(driverName.length(), 15));
        this.printConfigDo = printConfigDo;
        this.object = printConfigDo.printData;

        if (printConfigDo.printerID.contains(":")) {

        }
    }

    @Override
    protected void onPreExecute() {
        // Shows a progress icon on the title bar to indicate
        // it is working on something.
        if (context instanceof SignatureActivity) {

        } else {
            ((BaseActivity) context).showLoader();

        }
    }

    @Override
    protected String doInBackground(String... args) {

        String sPrinterURI = "bt://" + printConfigDo.printerID;
//			String sPrinterURI = "bt://" + "00:06:66:82:C3:09";

        LinePrinter.ExtraSettings exSettings = new LinePrinter.ExtraSettings();

        exSettings.setContext(context);

        PrintProgressListener progressListener = new PrintProgressListener() {
            @Override
            public void receivedStatus(PrintProgressEvent aEvent) {
                // Publishes updates on the UI thread.
                publishProgress(aEvent.getMessageType());
            }
        };

        if (printConfigDo.printFrom == PrinterConstants.PrintCylinderDeliveryNotes) {
            ActiveDeliveryMainDO activeDeliveryMainDO = (ActiveDeliveryMainDO) printConfigDo.printData;
//            PrinterConstants.base64LogoPng = activeDeliveryMainDO.logo;
            printDeliveryNotes(sPrinterURI, exSettings, progressListener, activeDeliveryMainDO);
        } else if (printConfigDo.printFrom == PrinterConstants.PrintBulkDeliveryNotes) {
            ActiveDeliveryMainDO activeDeliveryMainDO = (ActiveDeliveryMainDO) printConfigDo.printData;
//            PrinterConstants.base64LogoPng = activeDeliveryMainDO.logo;
            printDeliveryNotes(sPrinterURI, exSettings, progressListener, activeDeliveryMainDO);
        } else if (printConfigDo.printFrom == PrinterConstants.PrintInvoiceReport) {
            CreateInvoicePaymentDO invoiceDo = (CreateInvoicePaymentDO) printConfigDo.printData;
//            PrinterConstants.base64LogoPng = invoiceDo.logo;
            printInvoiceReport(sPrinterURI, exSettings, progressListener, invoiceDo);
        } else if (printConfigDo.printFrom == PrinterConstants.PrintCylinderIssue) {
            CylinderIssueMainDO cylinderIssueMainDO = (CylinderIssueMainDO) printConfigDo.printData;
//            PrinterConstants.base64LogoPng = cylinderIssueMainDO.logo;
            printCylinderIssue(sPrinterURI, exSettings, progressListener, cylinderIssueMainDO);
        } else if (printConfigDo.printFrom == PrinterConstants.PrintPaymentReport) {
            PaymentPdfDO paymentDO = (PaymentPdfDO) printConfigDo.printData;
//            PrinterConstants.base64LogoPng = paymentDO.logo;
            printPaymentReport(sPrinterURI, exSettings, progressListener, paymentDO);
        } else if (printConfigDo.printFrom == PrinterConstants.PrintActivityReport) {
            ArrayList<Serializable> transLists = (ArrayList<Serializable>) printConfigDo.printData;
//            PrinterConstants.base64LogoPng = paymentDO.logo;
            printActivityReport(sPrinterURI, exSettings, progressListener, transLists);
        } else if (printConfigDo.printFrom == PrinterConstants.PrintUserActivityReport) {
//            ArrayList<Serializable> transLists = (ArrayList<Serializable>) printConfigDo.printData;
            UserActivityReportMainDO userActivityReportMainDO = (UserActivityReportMainDO) printConfigDo.printData;

//            PrinterConstants.base64LogoPng = paymentDO.logo;
            printUserActivityReport(sPrinterURI, exSettings, progressListener, userActivityReportMainDO);
        } else if (printConfigDo.printFrom == PrinterConstants.PrintLoanReturn) {
            CylinderIssueMainDO cylinderIssueMainDO = (CylinderIssueMainDO) printConfigDo.printData;
//            PrinterConstants.base64LogoPng = cylinderIssueMainDO.logo;
            printLoanReturnReport(sPrinterURI, exSettings, progressListener, cylinderIssueMainDO);
        }
        if (printConfigDo.printFrom == PrinterConstants.PrintDelivetryExecutiveReport) {
            ActiveDeliveryMainDO activeDeliveryMainDO = (ActiveDeliveryMainDO) printConfigDo.printData;
//            PrinterConstants.base64LogoPng = activeDeliveryMainDO.logo;
            printDeliveryExecutivereportNotes(sPrinterURI, exSettings, progressListener, activeDeliveryMainDO);
        }
        return sResult;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        // Access the values array.
        int progress = values[0];

        switch (progress) {
            case PrintProgressEvent.MessageTypes.CANCEL:
                Log.e("" + this.getClass().getName(), PrinterConstants.PROGRESS_CANCEL_MSG);
                break;
            case PrintProgressEvent.MessageTypes.COMPLETE:
                Log.e("" + this.getClass().getName(), PrinterConstants.PROGRESS_COMPLETE_MSG);
                break;
            case PrintProgressEvent.MessageTypes.ENDDOC:
                Log.e("" + this.getClass().getName(), PrinterConstants.PROGRESS_ENDDOC_MSG);
                break;
            case PrintProgressEvent.MessageTypes.FINISHED:
                Log.e("" + this.getClass().getName(), PrinterConstants.PROGRESS_FINISHED_MSG);
                break;
            case PrintProgressEvent.MessageTypes.STARTDOC:
                Log.e("" + this.getClass().getName(), PrinterConstants.PROGRESS_STARTDOC_MSG);
                break;
            default:
                Log.e("" + this.getClass().getName(), PrinterConstants.PROGRESS_NONE_MSG);
                break;
        }
    }

    @Override
    protected void onPostExecute(String result) {
        // Displays the result (number of bytes sent to the printer or
        // exception message) in the Progress and Status text box.
        if (context instanceof SignatureActivity) {
            ((BaseActivity) context).hideLoader();

        } else {
            ((BaseActivity) context).hideLoader();

        }
        if (result != null) {
            Log.e("" + this.getClass().getName(), "onPostExecute() : " + result);
        }
    }

    private void printDeliveryNotes(String sPrinterURI, LinePrinter.ExtraSettings exSettings,
                                    PrintProgressListener progressListener, ActiveDeliveryMainDO activeDeliveryMainDO) {
        LinePrinter lp = null;
        try {

            lp = new LinePrinter(printConfigDo.jsonCmdAttribStr, printConfigDo.printerName, sPrinterURI, exSettings);
            lp.addPrintProgressListener(progressListener);
            int numtries = 0;
            int maxretry = 2;
            while (numtries < maxretry) {
                try {
                    lp.connect();  // Connects to the printer
                    break;
                } catch (LinePrinterException ex) {
                    numtries++;
                    Thread.sleep(1000);
                }
            }
            if (numtries == maxretry) lp.connect();//Final retry
            int[] results = lp.getStatus();
            if (results != null) {
                for (int err = 0; err < results.length; err++) {
                    if (results[err] == 223) {
                        throw new BadPrinterStateException("Paper out");
                    } else if (results[err] == 227) {
                        throw new BadPrinterStateException("Printer lid open");
                    }
                }
            }


            if (activeDeliveryMainDO.companyCode.equalsIgnoreCase("U2") || activeDeliveryMainDO.companyCode.equalsIgnoreCase("U3")) {
                lp.writeGraphicBase64(PrinterConstants.base64Footer, LinePrinter.GraphicRotationDegrees.DEGREE_0, 300, 250, 250);

            } else {
                lp.writeGraphicBase64(PrinterConstants.base64LogoPng, LinePrinter.GraphicRotationDegrees.DEGREE_0, 300, 250, 250);

            }
            Thread.sleep(100);
            lp.newLine(1);
            byte[] easymode = new byte[]{0x1b, 0x45, 0x5a}; // [ESC]EZ
            lp.write(easymode);
            int topSpace = 0;
            String printData = "";
            String startCommand = "\u001BEZ{PRINT:\n";
            String endCommand = "}{LP}\n";

            int leftSpace = 0;//225
            if (activeDeliveryMainDO.companyDescription.toLowerCase().contains("taqat")) {
                leftSpace = 320;
            } else {
                leftSpace = 250;
            }
            String shipmentProductsType = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "");

            printData = startCommand + "@" + topSpace + "," + leftSpace + ":PE203,HMULT1,VMULT2|" + activeDeliveryMainDO.companyDescription + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand + "@" + topSpace + ",350:PE203,HMULT1,VMULT2|Delivery Note|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            String dMonth = activeDeliveryMainDO.createdDate.substring(4, 6);
            String dyear = activeDeliveryMainDO.createdDate.substring(0, 4);
            String dDate = activeDeliveryMainDO.createdDate.substring(Math.max(activeDeliveryMainDO.createdDate.length() - 2, 0));
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|D.N. No|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.shipmentNumber + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Date|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + dDate + "-" + dMonth + "-" + dyear + "|" + endCommand;
//            "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + CalendarUtils.getPDFDate() + "|" + endCommand;

            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            activeDeliveryMainDO.customerDescription = activeDeliveryMainDO.customerDescription.substring(0, Math.min(activeDeliveryMainDO.customerDescription.length(), 30));
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Delivered To|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.customerDescription + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Time|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
//                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + CalendarUtils.getPDFTime() + "|" + endCommand;
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.createdTime + "|" + endCommand;

            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|User ID|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.createUserID + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|User Name|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.createUserName + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            PreferenceUtils preferenceUtils = new PreferenceUtils(context);
            String vehicleNumber = preferenceUtils.getStringFromPreference(PreferenceUtils.CV_PLATE, "");
            String vehicleCode = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_CODE, "");

            topSpace = 0;
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Vehicle Code|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + vehicleCode + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Plate No.|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + vehicleNumber + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);

            topSpace = 0;
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Payment Term|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.paymentTerm + "|" +
                    endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Address|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + getAddress(activeDeliveryMainDO) + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);

            topSpace = 0;
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Reciever Name|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.capturedName + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Reciever No.|" +
                    "@" + topSpace + ",610:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",640:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.capturedNumber + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            if (shipmentProductsType.equals(AppConstants.MeterReadingProduct)) {

                topSpace = 0;
                printData = startCommand +
                        "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Starting Tank Reading|" +
                        "@" + topSpace + ",216:PE203,HMULT1,VMULT1|:|" +
                        "@" + topSpace + ",230:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.startReading + "|" +
                        "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Closing Tank Reading|" +
                        "@" + topSpace + ",710:PE203,HMULT1,VMULT1|:|" +
                        "@" + topSpace + ",725:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.endReading + "|" + endCommand;
                lp.write(printData.getBytes());
                Thread.sleep(100);
                if (!activeDeliveryMainDO.startReading2.isEmpty() && !activeDeliveryMainDO.endReading2.isEmpty()) {
                    topSpace = 0;
                    printData = startCommand +
                            "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Starting Tank Reading|" +
                            "@" + topSpace + ",216:PE203,HMULT1,VMULT1|:|" +
                            "@" + topSpace + ",230:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.startReading2 + "|" +
                            "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Closing Tank Reading|" +
                            "@" + topSpace + ",710:PE203,HMULT1,VMULT1|:|" +
                            "@" + topSpace + ",725:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.endReading2 + "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(100);
                }
                if (!activeDeliveryMainDO.startReading3.isEmpty() && !activeDeliveryMainDO.endReading3.isEmpty()) {
                    topSpace = 0;
                    printData = startCommand +
                            "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Starting Tank Reading|" +
                            "@" + topSpace + ",216:PE203,HMULT1,VMULT1|:|" +
                            "@" + topSpace + ",230:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.startReading3 + "|" +
                            "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Closing Tank Reading|" +
                            "@" + topSpace + ",710:PE203,HMULT1,VMULT1|:|" +
                            "@" + topSpace + ",725:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.endReading3 + "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(100);
                }
                if (!activeDeliveryMainDO.startReading4.isEmpty() && !activeDeliveryMainDO.endReading4.isEmpty()) {
                    topSpace = 0;
                    printData = startCommand +
                            "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Starting Tank Reading|" +
                            "@" + topSpace + ",216:PE203,HMULT1,VMULT1|:|" +
                            "@" + topSpace + ",230:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.startReading4 + "|" +
                            "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Closing Tank Reading|" +
                            "@" + topSpace + ",710:PE203,HMULT1,VMULT1|:|" +
                            "@" + topSpace + ",725:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.endReading4 + "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(100);
                }
                if (!activeDeliveryMainDO.startReading5.isEmpty() && !activeDeliveryMainDO.endReading5.isEmpty()) {
                    topSpace = 0;
                    printData = startCommand +
                            "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Starting Tank Reading|" +
                            "@" + topSpace + ",216:PE203,HMULT1,VMULT1|:|" +
                            "@" + topSpace + ",230:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.startReading5 + "|" +
                            "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Closing Tank Reading|" +
                            "@" + topSpace + ",710:PE203,HMULT1,VMULT1|:|" +
                            "@" + topSpace + ",725:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.endReading5 + "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(100);
                }
                if (!activeDeliveryMainDO.startReading6.isEmpty() && !activeDeliveryMainDO.endReading6.isEmpty()) {
                    topSpace = 0;
                    printData = startCommand +
                            "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Starting Tank Reading|" +
                            "@" + topSpace + ",216:PE203,HMULT1,VMULT1|:|" +
                            "@" + topSpace + ",230:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.startReading6 + "|" +
                            "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Closing Tank Reading|" +
                            "@" + topSpace + ",710:PE203,HMULT1,VMULT1|:|" +
                            "@" + topSpace + ",725:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.endReading6 + "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(100);
                }
                if (!activeDeliveryMainDO.startReading7.isEmpty() && !activeDeliveryMainDO.endReading7.isEmpty()) {
                    topSpace = 0;
                    printData = startCommand +
                            "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Starting Tank Reading|" +
                            "@" + topSpace + ",216:PE203,HMULT1,VMULT1|:|" +
                            "@" + topSpace + ",230:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.startReading7 + "|" +
                            "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Closing Tank Reading|" +
                            "@" + topSpace + ",710:PE203,HMULT1,VMULT1|:|" +
                            "@" + topSpace + ",725:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.endReading7 + "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(100);
                }
                if (!activeDeliveryMainDO.startReading8.isEmpty() && !activeDeliveryMainDO.endReading8.isEmpty()) {
                    topSpace = 0;
                    printData = startCommand +
                            "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Starting Tank Reading|" +
                            "@" + topSpace + ",216:PE203,HMULT1,VMULT1|:|" +
                            "@" + topSpace + ",230:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.startReading8 + "|" +
                            "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Closing Tank Reading|" +
                            "@" + topSpace + ",710:PE203,HMULT1,VMULT1|:|" +
                            "@" + topSpace + ",725:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.endReading8 + "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(100);
                }
                if (!activeDeliveryMainDO.startReading9.isEmpty() && !activeDeliveryMainDO.endReading9.isEmpty()) {
                    topSpace = 0;
                    printData = startCommand +
                            "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Starting Tank Reading|" +
                            "@" + topSpace + ",216:PE203,HMULT1,VMULT1|:|" +
                            "@" + topSpace + ",230:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.startReading9 + "|" +
                            "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Closing Tank Reading|" +
                            "@" + topSpace + ",710:PE203,HMULT1,VMULT1|:|" +
                            "@" + topSpace + ",725:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.endReading9 + "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(100);
                }
                if (!activeDeliveryMainDO.startReading10.isEmpty() && !activeDeliveryMainDO.endReading10.isEmpty()) {
                    topSpace = 0;
                    printData = startCommand +
                            "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Starting Tank Reading|" +
                            "@" + topSpace + ",216:PE203,HMULT1,VMULT1|:|" +
                            "@" + topSpace + ",230:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.startReading10 + "|" +
                            "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Closing Tank Reading|" +
                            "@" + topSpace + ",710:PE203,HMULT1,VMULT1|:|" +
                            "@" + topSpace + ",725:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.endReading10 + "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(100);
                }

            }
            topSpace = 0;
            if (shipmentProductsType.equals(AppConstants.MeterReadingProduct)) {
                    String type="";
                    if(activeDeliveryMainDO.meterType==1){
                        type="LTR";
                    }else {
                        type="GAL";
                    }

                    topSpace = 30;
                    printData = startCommand +
                            "@" + (topSpace) + ",12:PE203,HMULT1,VMULT1|Sr.No.|" +
                            "@" + (topSpace) + ",110:PE203,HMULT1,VMULT1|Item|" +
                            "@" + (topSpace) + ",310:PE203,HMULT1,VMULT1|Opening Reading"+"("+type+")|" +
                            "@" + (topSpace) + ",520:PE203,HMULT1,VMULT1|Ending Reading" +"("+type+")|"+
                            "@" + (topSpace) + ",730:PE203,HMULT1,VMULT1|Quantity|" + endCommand;
                    lp.write(printData.getBytes());
//                topSpace = 30;
//                printData = startCommand +
//                        "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|Sr.No.|" +
//                        "@" + (topSpace + 5) + ",110:PE203,HMULT1,VMULT1|Item|" +
//                        "@" + (topSpace + 5) + ",360:PE203,HMULT1,VMULT1|Opening Reading|" +
//                        "@" + (topSpace + 5) + ",540:PE203,HMULT1,VMULT1|Ending Reading|" +
//                        "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT1|Quantity|" + endCommand;
//                lp.write(printData.getBytes());
                Thread.sleep(100);
                topSpace = 0;
                for (int i = 0; i < activeDeliveryMainDO.activeDeliveryDOS.size(); i++) {
                    ActiveDeliveryDO activeDeliveryDO = activeDeliveryMainDO.activeDeliveryDOS.get(i);
                    activeDeliveryDO.productDescription = activeDeliveryDO.productDescription.substring(0, Math.min(activeDeliveryDO.productDescription.length(), 25));
                    printData = startCommand +
                            "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|" + (i + 1) + "|" +
                            "@" + (topSpace + 5) + ",110:PE203,HMULT1,VMULT1|" + activeDeliveryDO.productDescription + "|" +
                            "@" + (topSpace + 5) + ",360:PE203,HMULT1,VMULT1|" + activeDeliveryDO.openingQuantityLong + "|" +
                            "@" + (topSpace + 5) + ",540:PE203,HMULT1,VMULT1|" + activeDeliveryDO.endingQuantityLong + "|" +
                            "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT1|" + activeDeliveryDO.orderedQuantity + " " + activeDeliveryDO.unit + "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(100);
                    topSpace = 0;
                }
                if (activeDeliveryMainDO.referenceID != null) {
//                    note = podDo.getNotes();
                    topSpace = topSpace + 40;

                    printData = startCommand + "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|" + "Delivery Reference :  " + activeDeliveryMainDO.referenceID + "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(100);

                }
//                printData = startCommand + "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|Remarks :- |" + endCommand;
//                PodDo podDo = StorageManager.getInstance(context).getDepartureData(context);
//                String note = "";
                if (activeDeliveryMainDO.remarks != null) {
//                    note = podDo.getNotes();
                    topSpace = topSpace + 40;
                    String remarks1 = "";
                    String remarks2 = "";
                    if (activeDeliveryMainDO.remarks.length() > 70) {
                        remarks1 = activeDeliveryMainDO.remarks.substring(0, 70);
                        remarks2 = activeDeliveryMainDO.remarks.substring(70, activeDeliveryMainDO.remarks.length());
                    } else {
                        remarks1 = activeDeliveryMainDO.remarks;

                    }
                    printData = startCommand + "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|" + "Remarks :  " + remarks1 + "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(100);
                    if (remarks2.length() > 0) {
                        printData = startCommand + "@" + (5) + ",12:PE203,HMULT1,VMULT1|" + "" + remarks2 + "|" + endCommand;
                        lp.write(printData.getBytes());
                        Thread.sleep(100);
                    }
                }

            } else {
                topSpace = 30;
                printData = startCommand +
                        "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|Sr.No.|" +
                        "@" + (topSpace + 5) + ",110:PE203,HMULT1,VMULT1|Item|" +
                        "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT1|Quantity|" + endCommand;
                lp.write(printData.getBytes());
                Thread.sleep(100);
                topSpace = 0;
                for (int i = 0; i < activeDeliveryMainDO.activeDeliveryDOS.size(); i++) {
                    ActiveDeliveryDO activeDeliveryDO = activeDeliveryMainDO.activeDeliveryDOS.get(i);
                    activeDeliveryDO.productDescription = activeDeliveryDO.productDescription.substring(0, Math.min(activeDeliveryDO.productDescription.length(), 50));
                    printData = startCommand +
                            "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|" + (i + 1) + "|" +
                            "@" + (topSpace + 5) + ",110:PE203,HMULT1,VMULT1|" + activeDeliveryDO.productDescription + "|" +
                            "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT1|" + activeDeliveryDO.orderedQuantity + " " + activeDeliveryDO.unit + "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(100);
                    topSpace = 0;
                }

//                printData = startCommand + "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|Remarks :- |" + endCommand;
//                PodDo podDo = StorageManager.getInstance(context).getDepartureData(context);
//                String note = "";
//                if (podDo != null && !TextUtils.isEmpty(podDo.getNotes())) {
//                    note = podDo.getNotes();
//                    printData = startCommand + "@" + (topSpace + 5) + ",180:PE203,HMULT1,VMULT1|Remarks :- |";
//                    topSpace = topSpace + 40;
//                    printData = printData + "@" + (topSpace + 5) + ",180:PE203,HMULT1,VMULT1|" + note + "|" + endCommand;
//                }
//                lp.write(printData.getBytes());
//                Thread.sleep(100);
                topSpace = topSpace + 10;

                if (activeDeliveryMainDO.referenceID != null) {
//                    note = podDo.getNotes();

                    printData = startCommand + "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|" + "Delivery Reference :  " + activeDeliveryMainDO.referenceID + "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(100);

                }
                if (activeDeliveryMainDO.remarks != null) {
//                    note = podDo.getNotes();
                    String remarks1 = "";
                    String remarks2 = "";
                    if (activeDeliveryMainDO.remarks.length() > 70) {
                        remarks1 = activeDeliveryMainDO.remarks.substring(0, 70);
                        remarks2 = activeDeliveryMainDO.remarks.substring(70, activeDeliveryMainDO.remarks.length());
                    } else {
                        remarks1 = activeDeliveryMainDO.remarks;

                    }
                    printData = startCommand + "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|" + "Remarks :  " + remarks1 + "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(100);
                    if (remarks2.length() > 0) {
                        printData = startCommand + "@" + (5) + ",12:PE203,HMULT1,VMULT1|" + "" + remarks2 + "|" + endCommand;
                        lp.write(printData.getBytes());
                        Thread.sleep(100);
                    }
                }
            }

            printSignature(activeDeliveryMainDO.signature, lp, progressListener);
            printDynamicFooter(activeDeliveryMainDO, lp);
            lp.newLine(7);

            sResult = "Number of bytes sent to printer: " + lp.getBytesWritten();

        } catch (BadPrinterStateException ex) {
            // Stop listening for printer events.
            lp.removePrintProgressListener(progressListener);
            sResult = "Printer error detected: " + ex.getMessage() + ". Please correct the error and try again.";
        } catch (LinePrinterException ex) {
            sResult = "LinePrinterException: " + ex.getMessage();
        } catch (Exception ex) {
            if (ex.getMessage() != null)
                sResult = "Unexpected exception: " + ex.getMessage();
            else
                sResult = "Unexpected exception.";
        } finally {
            ((BaseActivity) context).hideLoader();
            if (lp != null) {
                try {
                    lp.disconnect();  // Disconnects from the printer
                    lp.close();  // Releases resources
                    ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.DELIVERY_NOTE, "SUCCESS");

                } catch (Exception ex) {
                }
            }
        }
    }

    private void printLoanReturnReport(String sPrinterURI, LinePrinter.ExtraSettings exSettings, PrintProgressListener progressListener, CylinderIssueMainDO cylinderIssueMainDO) {
        LinePrinter lp = null;
        try {
            lp = new LinePrinter(printConfigDo.jsonCmdAttribStr, printConfigDo.printerName, sPrinterURI, exSettings);
            lp.addPrintProgressListener(progressListener);
            int numtries = 0;
            int maxretry = 2;
            while (numtries < maxretry) {
                try {
                    lp.connect();  // Connects to the printer
                    break;
                } catch (LinePrinterException ex) {
                    numtries++;
                    Thread.sleep(1000);
                }
            }
            if (numtries == maxretry) lp.connect();//Final retry
            // Check the state of the printer and abort printing if there are
            // any critical errors detected.
            int[] results = lp.getStatus();
            if (results != null) {
                for (int err = 0; err < results.length; err++) {
                    if (results[err] == 223) {
                        // Paper out.
                        throw new BadPrinterStateException("Paper out");
                    } else if (results[err] == 227) {
                        // Lid open.
                        throw new BadPrinterStateException("Printer lid open");
                    }
                }
            }

            int topSpace = 0;
            String printData = "";
            String startCommand = "\u001BEZ{PRINT:\n";
            String endCommand = "}{LP}\n";

            if (cylinderIssueMainDO.companyCode.equalsIgnoreCase("U2") || cylinderIssueMainDO.companyCode.equalsIgnoreCase("U3")) {
                lp.writeGraphicBase64(PrinterConstants.base64Footer, LinePrinter.GraphicRotationDegrees.DEGREE_0, 300, 250, 250);

            } else {
                lp.writeGraphicBase64(PrinterConstants.base64LogoPng, LinePrinter.GraphicRotationDegrees.DEGREE_0, 300, 250, 250);

            }
            Thread.sleep(100);
            lp.newLine(1);
            byte[] easymode = new byte[]{0x1b, 0x45, 0x5a}; // [ESC]EZ
            lp.write(easymode);
            int leftSpace = 0;//225
            if (cylinderIssueMainDO.company.toLowerCase().contains("taqat")) {
//                leftSpace = 320 - (cylinderIssueMainDO.company.length()/2);
                leftSpace = 320;
            } else {
                leftSpace = 245;
            }
            printData = startCommand + "@" + topSpace + "," + leftSpace + ":PE203,HMULT1,VMULT2|" + cylinderIssueMainDO.company + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand + "@" + topSpace + ",250:PE203,HMULT1,VMULT2|Cylinder Issuance/Receipt Document|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            String dMonth = cylinderIssueMainDO.createdDate.substring(4, 6);
            String dyear = cylinderIssueMainDO.createdDate.substring(0, 4);
            String dDate = cylinderIssueMainDO.createdDate.substring(Math.max(cylinderIssueMainDO.createdDate.length() - 2, 0));

            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|D.N. No|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + cylinderIssueMainDO.shipmentNumber + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Date|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + dDate + "-" + dMonth + "-" + dyear + "|" +
                    "" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);

            topSpace = 0;
            cylinderIssueMainDO.customerDescription = cylinderIssueMainDO.customerDescription.substring(0, Math.min(cylinderIssueMainDO.customerDescription.length(), 30));
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Delivered To|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + cylinderIssueMainDO.customerDescription + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Time|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + cylinderIssueMainDO.createdTime + "|" +
                    "" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|User ID|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + cylinderIssueMainDO.createUserID + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|User Name|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + cylinderIssueMainDO.createUserName + "|" +
                    "" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            PreferenceUtils preferenceUtils = new PreferenceUtils(context);
            String vehicleNumber = preferenceUtils.getStringFromPreference(PreferenceUtils.CV_PLATE, "");
            String vehicleCode = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_CODE, "");

            topSpace = 0;
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Vehicle Code|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + vehicleCode + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Plate No.|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + vehicleNumber + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Address|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + getAddress(cylinderIssueMainDO) + "|" +
                    "" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand +
                    "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|Sr.No.|" +
                    "@" + (topSpace + 5) + ",90:PE203,HMULT1,VMULT1|Item|" +
                    "@" + (topSpace + 5) + ",390:PE203,HMULT1,VMULT1|Open QTY|" +
                    "@" + (topSpace + 5) + ",490:PE203,HMULT1,VMULT1|Issued Qty|" +
                    "@" + (topSpace + 5) + ",600:PE203,HMULT1,VMULT1|Received Qty|" +
                    "@" + (topSpace + 5) + ",730:PE203,HMULT1,VMULT1|Balance|" + "" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            for (int i = 0; i < cylinderIssueMainDO.cylinderIssueDOS.size(); i++) {
                CylinderIssueDO cylinderIssueDO = cylinderIssueMainDO.cylinderIssueDOS.get(i);
                cylinderIssueDO.productDescription = cylinderIssueDO.productDescription.substring(0, Math.min(cylinderIssueDO.productDescription.length(), 25));
                printData = startCommand +
                        "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|" + (i + 1) + "|" +
                        "@" + (topSpace + 5) + ",90:PE203,HMULT1,VMULT1|" + cylinderIssueDO.productDescription + "|" +
                        "@" + (topSpace + 5) + ",390:PE203,HMULT1,VMULT1|" + cylinderIssueDO.orderedQuantity + "|" +
                        "@" + (topSpace + 5) + ",490:PE203,HMULT1,VMULT1|" + cylinderIssueDO.issuedQuantity + "|" +
                        "@" + (topSpace + 5) + ",600:PE203,HMULT1,VMULT1|" + cylinderIssueDO.recievedQuantity + "|" +
                        "@" + (topSpace + 5) + ",730:PE203,HMULT1,VMULT1|" + cylinderIssueDO.balanceQuantity + "|";// +
                lp.write((printData + endCommand).getBytes());
                Thread.sleep(100);
                topSpace = 0;
            }
            PodDo podDo = StorageManager.getInstance(context).getDepartureData(context);
            String note = "";
            if (podDo != null && !TextUtils.isEmpty(podDo.getNotes())) {
                note = podDo.getNotes();
            }
            printData = startCommand + "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|Remarks :-  " + note + "|" + endCommand;
            topSpace = topSpace + 120;//tvs = 50+40 = 90,
            lp.write(printData.getBytes());
            Thread.sleep(100);
//            printSignature(printConfigDo.signatureBase64, lp, progressListener);
            printDynamicFooter(cylinderIssueMainDO, lp);
            lp.newLine(7);
            sResult = "Number of bytes sent to printer: " + lp.getBytesWritten();
        } catch (BadPrinterStateException ex) {
            lp.removePrintProgressListener(progressListener);
            sResult = "Printer error detected: " + ex.getMessage() + ". Please correct the error and try again.";
        } catch (LinePrinterException ex) {
            sResult = "LinePrinterException: " + ex.getMessage();
        } catch (Exception ex) {
            if (ex.getMessage() != null)
                sResult = "Unexpected exception: " + ex.getMessage();
            else
                sResult = "Unexpected exception.";
        } finally {
            ((BaseActivity) context).hideLoader();
            if (lp != null) {
                try {
                    lp.disconnect();  // Disconnects from the printer
                    lp.close();  // Releases resources
                } catch (Exception ex) {
                }
            }
        }
    }

    private void printCylinderIssue(String sPrinterURI, LinePrinter.ExtraSettings exSettings, PrintProgressListener progressListener, CylinderIssueMainDO cylinderIssueMainDO) {
        LinePrinter lp = null;
        try {
            lp = new LinePrinter(printConfigDo.jsonCmdAttribStr, printConfigDo.printerName, sPrinterURI, exSettings);
            lp.addPrintProgressListener(progressListener);
            int numtries = 0;
            int maxretry = 2;
            while (numtries < maxretry) {
                try {
                    lp.connect();  // Connects to the printer
                    break;
                } catch (LinePrinterException ex) {
                    numtries++;
                    Thread.sleep(1000);
                }
            }
            if (numtries == maxretry) lp.connect();//Final retry
            // Check the state of the printer and abort printing if there are
            // any critical errors detected.
            int[] results = lp.getStatus();
            if (results != null) {
                for (int err = 0; err < results.length; err++) {
                    if (results[err] == 223) {
                        // Paper out.
                        throw new BadPrinterStateException("Paper out");
                    } else if (results[err] == 227) {
                        // Lid open.
                        throw new BadPrinterStateException("Printer lid open");
                    }
                }
            }

            int topSpace = 0;
            String printData = "";
            String startCommand = "\u001BEZ{PRINT:\n";
            String endCommand = "}{LP}\n";

            if (cylinderIssueMainDO.companyCode.equalsIgnoreCase("U2") || cylinderIssueMainDO.companyCode.equalsIgnoreCase("U3")) {
                lp.writeGraphicBase64(PrinterConstants.base64Footer, LinePrinter.GraphicRotationDegrees.DEGREE_0, 300, 250, 250);

            } else {
                lp.writeGraphicBase64(PrinterConstants.base64LogoPng, LinePrinter.GraphicRotationDegrees.DEGREE_0, 300, 250, 250);

            }
            Thread.sleep(100);
            lp.newLine(1);
            byte[] easymode = new byte[]{0x1b, 0x45, 0x5a}; // [ESC]EZ
            lp.write(easymode);
            int leftSpace = 0;//225
            if (cylinderIssueMainDO.company.toLowerCase().contains("taqat")) {
//                leftSpace = 320 - (cylinderIssueMainDO.company.length()/2);
                leftSpace = 320;
            } else {
                leftSpace = 245;
            }
            printData = startCommand + "@" + topSpace + "," + leftSpace + ":PE203,HMULT1,VMULT2|" + cylinderIssueMainDO.company + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand + "@" + topSpace + ",250:PE203,HMULT1,VMULT2|Cylinder Issuance/Receipt Document|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            String dMonth = cylinderIssueMainDO.createdDate.substring(4, 6);
            String dyear = cylinderIssueMainDO.createdDate.substring(0, 4);
            String dDate = cylinderIssueMainDO.createdDate.substring(Math.max(cylinderIssueMainDO.createdDate.length() - 2, 0));

            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|D.N. No|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + cylinderIssueMainDO.shipmentNumber + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Date|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + dDate + "-" + dMonth + "-" + dyear + "|" +
                    "" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);

            topSpace = 0;
            cylinderIssueMainDO.customerDescription = cylinderIssueMainDO.customerDescription.substring(0, Math.min(cylinderIssueMainDO.customerDescription.length(), 30));
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Delivered To|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + cylinderIssueMainDO.customerDescription + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Time|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + cylinderIssueMainDO.createdTime + "|" +
                    "" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|User ID|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + cylinderIssueMainDO.createUserID + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|User Name|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + cylinderIssueMainDO.createUserName + "|" +
                    "" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            PreferenceUtils preferenceUtils = new PreferenceUtils(context);
            String vehicleNumber = preferenceUtils.getStringFromPreference(PreferenceUtils.CV_PLATE, "");
            String vehicleCode = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_CODE, "");

            topSpace = 0;
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Vehicle Code|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + vehicleCode + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Plate No.|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + vehicleNumber + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Address|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +

                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + getAddress(cylinderIssueMainDO) + "|" +
                    "" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;

            printData = startCommand +
                    "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|Sr.No.|" +
                    "@" + (topSpace + 5) + ",90:PE203,HMULT1,VMULT1|Item|" +
                    "@" + (topSpace + 5) + ",390:PE203,HMULT1,VMULT1|Open QTY|" +
                    "@" + (topSpace + 5) + ",490:PE203,HMULT1,VMULT1|Issued Qty|" +
                    "@" + (topSpace + 5) + ",600:PE203,HMULT1,VMULT1|Received Qty|" +
                    "@" + (topSpace + 5) + ",730:PE203,HMULT1,VMULT1|Balance|" + "" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            for (int i = 0; i < cylinderIssueMainDO.cylinderIssueDOS.size(); i++) {
                CylinderIssueDO cylinderIssueDO = cylinderIssueMainDO.cylinderIssueDOS.get(i);
                cylinderIssueDO.productDescription = cylinderIssueDO.productDescription.substring(0, Math.min(cylinderIssueDO.productDescription.length(), 25));
                printData = startCommand +
                        "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|" + (i + 1) + "|" +
                        "@" + (topSpace + 5) + ",90:PE203,HMULT1,VMULT1|" + cylinderIssueDO.productDescription + "|" +
                        "@" + (topSpace + 5) + ",390:PE203,HMULT1,VMULT1|" + cylinderIssueDO.orderedQuantity + "|" +
                        "@" + (topSpace + 5) + ",490:PE203,HMULT1,VMULT1|" + cylinderIssueDO.issuedQuantity + "|" +
                        "@" + (topSpace + 5) + ",600:PE203,HMULT1,VMULT1|" + cylinderIssueDO.recievedQuantity + "|" +
                        "@" + (topSpace + 5) + ",730:PE203,HMULT1,VMULT1|" + cylinderIssueDO.balanceQuantity + "|";// +
                lp.write((printData + endCommand).getBytes());
                Thread.sleep(100);
                topSpace = 0;
            }
            PodDo podDo = StorageManager.getInstance(context).getDepartureData(context);
            String note = "";
            if (podDo != null && !TextUtils.isEmpty(podDo.getNotes())) {
                note = podDo.getNotes();
            }
            printData = startCommand + "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|Remarks :-  " + note + "|" + endCommand;
            topSpace = topSpace + 120;//tvs = 50+40 = 90,
            lp.write(printData.getBytes());
            Thread.sleep(100);
            printSignature(cylinderIssueMainDO.signature, lp, progressListener);
            printDynamicFooter(cylinderIssueMainDO, lp);
            lp.newLine(7);
            sResult = "Number of bytes sent to printer: " + lp.getBytesWritten();
        } catch (BadPrinterStateException ex) {
            lp.removePrintProgressListener(progressListener);
            sResult = "Printer error detected: " + ex.getMessage() + ". Please correct the error and try again.";
        } catch (LinePrinterException ex) {
            sResult = "LinePrinterException: " + ex.getMessage();
        } catch (Exception ex) {
            if (ex.getMessage() != null)
                sResult = "Unexpected exception: " + ex.getMessage();
            else
                sResult = "Unexpected exception.";
        } finally {
            ((BaseActivity) context).hideLoader();
            if (lp != null) {
                try {
                    lp.disconnect();  // Disconnects from the printer
                    lp.close();  // Releases resources
                } catch (Exception ex) {
                }
            }
        }
    }

    private static void printSignature(String signatureBase64, LinePrinter lp, PrintProgressListener progressListener) {
        try {
            if (signatureBase64 != null && !signatureBase64.equalsIgnoreCase("")) {
                lp.writeGraphicBase64(signatureBase64, LinePrinter.GraphicRotationDegrees.DEGREE_0, 520, 250, 80);
                Thread.sleep(100);
                lp.newLine(1);
            }
            lp.setUnderline(true);
            String printData = printData1Start + "@0,540:PE203,HMULT1,VMULT1|Signature|" + printDataEnd;
            lp.write(printData.getBytes());
            lp.setUnderline(false);
            lp.newLine(1);
        } catch (Exception e) {
            e.printStackTrace();
            lp.removePrintProgressListener(progressListener);
            Log.e(TAG, "Printer error detected: " + e.getMessage() + ". Please correct the error and try again.");
        }
    }

    private void printDynamicFooter(Object object, LinePrinter lp) {
        try {
            String site = "", siteDescr = "", street = "", landmark = "", town = "", country = "", website = "";
            String city = "", pinCode = "", landLine = "", mobile = "", fax = "", siteEmail1 = "", siteEmail2 = "";

            if (object instanceof ActiveDeliveryMainDO) {
                ActiveDeliveryMainDO activeDeliveryMainDO = (ActiveDeliveryMainDO) object;
                site = activeDeliveryMainDO.siteDescription;//
                siteDescr = activeDeliveryMainDO.siteAddress1;//
                country = activeDeliveryMainDO.siteCountry;//
                city = activeDeliveryMainDO.siteCity;//
                pinCode = activeDeliveryMainDO.sitePostalCode;
                mobile = activeDeliveryMainDO.siteMobile;
                fax = activeDeliveryMainDO.siteFax;
                siteEmail1 = activeDeliveryMainDO.siteEmail1;
                website = activeDeliveryMainDO.siteWebEmail;

                street = activeDeliveryMainDO.siteAddress2;//
                landmark = activeDeliveryMainDO.siteAddress3;
                town = activeDeliveryMainDO.customerTown;
                landLine = activeDeliveryMainDO.siteLandLine;
                siteEmail2 = activeDeliveryMainDO.siteEmail2;
            } else if (object instanceof CylinderIssueMainDO) {
                CylinderIssueMainDO cylinderIssueMainDO = (CylinderIssueMainDO) object;
                site = cylinderIssueMainDO.siteDescription;//
                siteDescr = cylinderIssueMainDO.siteAddress1;//
                country = cylinderIssueMainDO.siteCountry;//
                city = cylinderIssueMainDO.siteCity;//
                pinCode = cylinderIssueMainDO.sitePostalCode;
                mobile = cylinderIssueMainDO.siteMobile;
                fax = cylinderIssueMainDO.siteFax;
                siteEmail1 = cylinderIssueMainDO.siteEmail1;
                website = cylinderIssueMainDO.siteWebEmail;
//				street = cylinderIssueMainDO.street;//
//				landmark = cylinderIssueMainDO.landMark;
//				town = cylinderIssueMainDO.town;
//                landLine = cylinderIssueMainDO.siteLandLine;
//                siteEmail2 = cylinderIssueMainDO.siteEmail2;
            } else if (object instanceof CreateInvoicePaymentDO) {
                CreateInvoicePaymentDO invoiceDo = (CreateInvoicePaymentDO) object;
                site = invoiceDo.siteDescription;//
                siteDescr = invoiceDo.siteAddress1;//
                country = invoiceDo.siteCountry;//
                city = invoiceDo.siteCity;//
                pinCode = invoiceDo.sitePostalCode;
                mobile = invoiceDo.siteMobile;
                fax = invoiceDo.siteFax;
                siteEmail1 = invoiceDo.siteEmail1;
                website = invoiceDo.siteWebEmail;
                street = invoiceDo.siteStreet;//
                landmark = invoiceDo.siteLandMark;
                town = invoiceDo.siteTown;
                landLine = invoiceDo.siteLandLine;
                siteEmail2 = invoiceDo.siteEmail2;
            } else if (object instanceof PaymentPdfDO) {
                PaymentPdfDO paymentPdfDO = (PaymentPdfDO) object;
                site = paymentPdfDO.siteDescription;//
                siteDescr = paymentPdfDO.siteAddress1;//
                country = paymentPdfDO.siteCountry;//
                city = paymentPdfDO.siteCity;//
                pinCode = paymentPdfDO.sitePostalCode;
                mobile = paymentPdfDO.siteMobile;
                fax = paymentPdfDO.siteFax;
                siteEmail1 = paymentPdfDO.siteEmail1;
                website = paymentPdfDO.siteWebEmail;
                street = paymentPdfDO.siteAddress2;//
                landmark = paymentPdfDO.siteLandLine;
                town = paymentPdfDO.customerTown;
                landLine = paymentPdfDO.siteLandLine;
                siteEmail2 = paymentPdfDO.siteEmail2;
            }

            int topSpace = 10;
            siteEmail1 = siteEmail1.substring(0, Math.min(siteEmail1.length(), 34));
            String printData = "\u001BEZ{PRINT:\n";
            printData = printData + "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Registered office|\n";
            topSpace = topSpace + 40;
            printData = printData + "@" + topSpace + ",10:PE203,HMULT1,VMULT1|" + site + "|\n";
            topSpace = topSpace + 40;
            printData = printData + "@" + topSpace + ",10:PE203,HMULT1,VMULT1|" + siteDescr + ", P.O Box " + pinCode + ", " + city + ", " + country + "|\n";
            topSpace = topSpace + 40;
            printData = printData + "@" + topSpace + ",10:PE203,HMULT1,VMULT1|T: " + mobile + "|\n";
            printData = printData + "@" + topSpace + ",410:PE203,HMULT1,VMULT1|F: " + fax + "|\n";
            topSpace = topSpace + 40;
            printData = printData + "@" + topSpace + ",10:PE203,HMULT1,VMULT1|E: " + siteEmail1 + "|\n";
            printData = printData + "@" + topSpace + ",410:PE203,HMULT1,VMULT1|website: " + website + "|\n";
            printData = printData + "}{LP}\n";
            lp.write(printData.getBytes());
            Thread.sleep(100);
        } catch (Exception e) {
            e.printStackTrace();
            sResult = "Printer error detected: " + e.getMessage() + ". Please correct the error and try again.";
        }
    }

    private void printInvoiceReport(String sPrinterURI, LinePrinter.ExtraSettings exSettings, PrintProgressListener progressListener, CreateInvoicePaymentDO invoiceDo) {
        LinePrinter lp = null;
        try {
            lp = new LinePrinter(printConfigDo.jsonCmdAttribStr, printConfigDo.printerName, sPrinterURI, exSettings);
            lp.addPrintProgressListener(progressListener);
            int numtries = 0;
            int maxretry = 2;
            while (numtries < maxretry) {
                try {
                    lp.connect();  // Connects to the printer
                    break;
                } catch (LinePrinterException ex) {
                    numtries++;
                    Thread.sleep(1000);
                }
            }
            if (numtries == maxretry) lp.connect();//Final retry
            int[] results = lp.getStatus();
            if (results != null) {
                for (int err = 0; err < results.length; err++) {
                    if (results[err] == 223) {
                        // Paper out.
                        throw new BadPrinterStateException("Paper out");
                    } else if (results[err] == 227) {
                        // Lid open.
                        throw new BadPrinterStateException("Printer lid open");
                    }
                }
            }
            if (invoiceDo.companyCode.equalsIgnoreCase("U2") || invoiceDo.companyCode.equalsIgnoreCase("U3")) {
                lp.writeGraphicBase64(PrinterConstants.base64Footer, LinePrinter.GraphicRotationDegrees.DEGREE_0, 300, 250, 250);

            } else {
                lp.writeGraphicBase64(PrinterConstants.base64LogoPng, LinePrinter.GraphicRotationDegrees.DEGREE_0, 300, 250, 250);

            }
            Thread.sleep(100);
            lp.newLine(1);
            byte[] easymode = new byte[]{0x1b, 0x45, 0x5a}; // [ESC]EZ
            lp.write(easymode);
            int topSpace = 0;
            String printData = "";
            String startCommand = "\u001BEZ{PRINT:\n";
            String endCommand = "}{LP}\n";
            int leftSpace = 0;//225
            if (invoiceDo.supplierName.toLowerCase().contains("taqat")) {
//                leftSpace = 320 - (invoiceDo.company.length()/2);
                leftSpace = 320;
            } else {
                leftSpace = 245;
            }
            printData = startCommand + "@" + topSpace + "," + leftSpace + ":PE203,HMULT1,VMULT2|" + invoiceDo.supplierName + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand + "@" + topSpace + ",290:PE203,HMULT1,VMULT2|Commercial and Tax Invoice|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            String dMonth = invoiceDo.createdDate.substring(4, 6);
            String dyear = invoiceDo.createdDate.substring(0, 4);
            String dDate = invoiceDo.createdDate.substring(Math.max(invoiceDo.createdDate.length() - 2, 0));

            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Invoice  No|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + invoiceDo.invoiceNumber + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Date|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + dDate + "-" + dMonth + "-" + dyear + "|" + endCommand;
            lp.write(printData.getBytes());

            topSpace = 0;
            invoiceDo.customerDescription = invoiceDo.customerDescription.substring(0, Math.min(invoiceDo.customerDescription.length(), 30));
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Customer Name|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + invoiceDo.customerDescription + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Time|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + invoiceDo.createdTime + "|" + endCommand;
            lp.write(printData.getBytes());
            topSpace = 0;
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Customer TRN|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + invoiceDo.customerTrn + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|User ID|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + invoiceDo.createUserID + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);

            topSpace = 0;
            String address = getAddress(invoiceDo);
            String addresses1 = "";
            String addresses2 = "";
            if (address.length() > 32) {
                addresses1 = address.substring(0, 32);
                addresses2 = address.substring(32, address.length());
            } else {
                addresses1 = address;
            }

            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Cust Address|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + addresses1 + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|User Name|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + invoiceDo.createUserName + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            if (addresses2.length() > 0) {
                printData = startCommand +
                        "@" + topSpace + ",10:PE203,HMULT1,VMULT1||" +
                        "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + addresses2 + "|" + endCommand;
                lp.write(printData.getBytes());
                Thread.sleep(100);
                topSpace = 0;
            }
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Supplier Name|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + invoiceDo.supplierName + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);

            topSpace = 0;
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Supplier TRN|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + invoiceDo.supplierTrn + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Delivery No|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + invoiceDo.shipmentID + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);

//            topSpace = 0;
//            printData = startCommand +
//                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Supplier TRN|" +
//                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
//                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + invoiceDo.supplierTrn + "|" + endCommand;
//            lp.write(printData.getBytes());
            topSpace = 0;
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Payment Term|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + invoiceDo.paymentTerm + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            PreferenceUtils preferenceUtils = new PreferenceUtils(context);
            String vehicleNumber = preferenceUtils.getStringFromPreference(PreferenceUtils.CV_PLATE, "");
            String vehicleCode = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_CODE, "");

            topSpace = 0;
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Vehicle Code|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + vehicleCode + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Plate No.|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + vehicleNumber + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Reg Address|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + getRegisteredAddress(invoiceDo) + "|" + "" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 30;
            printData = startCommand +
                    "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|Sr.No.|" +
                    "@" + (topSpace + 5) + ",110:PE203,HMULT1,VMULT1|Particulars|" +
                    "@" + (topSpace + 5) + ",360:PE203,HMULT1,VMULT1|Quantity|" +
                    "@" + (topSpace + 5) + ",540:PE203,HMULT1,VMULT1|Unit Price|" +
                    "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT1|Amount(AED)|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            for (int i = 0; i < invoiceDo.pdfInvoiceDos.size(); i++) {
                PdfInvoiceDo pdfInvoiceDo = invoiceDo.pdfInvoiceDos.get(i);
                pdfInvoiceDo.productDesccription = pdfInvoiceDo.productDesccription.substring(0, Math.min(pdfInvoiceDo.productDesccription.length(), 23));
                printData = startCommand +
                        "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|" + (i + 1) + "|" +
                        "@" + (topSpace + 5) + ",110:PE203,HMULT1,VMULT1|" + pdfInvoiceDo.productDesccription + "|" +
                        "@" + (topSpace + 5) + ",360:PE203,HMULT1,VMULT1|" + pdfInvoiceDo.deliveredQunatity + " " + pdfInvoiceDo.quantityUnits + "|" +
                        "@" + (topSpace + 5) + ",540:PE203,HMULT1,VMULT1|" + pdfInvoiceDo.grossPrice + "|" +
                        "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT1|" + pdfInvoiceDo.amount + "|";
                lp.write((printData + endCommand).getBytes());
                Thread.sleep(100);
                topSpace = 0;
            }

            printData = startCommand +
                    "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|Discount|" +
                    "@" + (topSpace + 5) + ",720:PE203,HMULT1,VMULT1|" + invoiceDo.totalDiscount + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand +
                    "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|Total Excluding VAT|" +
                    "@" + (topSpace + 5) + ",720:PE203,HMULT1,VMULT1|" + invoiceDo.excludingTax + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand +
                    "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|VAT|" +
                    "@" + (topSpace + 5) + ",720:PE203,HMULT1,VMULT1|" + invoiceDo.totalTax + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand +
                    "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|Total|" +
                    "@" + (topSpace + 5) + ",720:PE203,HMULT1,VMULT1|" + invoiceDo.includingTax + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);

            //
            double totalAmount = getTotalAmount(invoiceDo);
            int afterDcimalVal = PDFOperations.getInstance().anyMethod(totalAmount);
//            String printWords = startCommand + "@" + topSpace + ",1:PE203,HMULT1,VMULT1|AED : |"+getAmountInLetters(totalAmount, false) + " and " + getAmountInLetters(afterDcimalVal, true) + "" + endCommand;
//            lp.write(printWords.getBytes());
//            Thread.sleep(100);

            printData = startCommand + "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|AED Total:  " + getAmountInLetters(totalAmount, false) + " and " + getAmountInLetters(afterDcimalVal, true) + "" + "|" + endCommand;
            topSpace = 0;
            lp.write(printData.getBytes());
            Thread.sleep(100);


            if (!invoiceDo.deliveryRemarks.isEmpty()) {
//                    note = podDo.getNotes();
                topSpace = topSpace + 40;
                String remarks1 = "";
                String remarks2 = "";
                if (invoiceDo.deliveryRemarks.length() > 70) {
                    remarks1 = invoiceDo.deliveryRemarks.substring(0, 70);
                    remarks2 = invoiceDo.deliveryRemarks.substring(70, invoiceDo.deliveryRemarks.length());
                } else {
                    remarks1 = invoiceDo.deliveryRemarks;

                }
                printData = startCommand + "@" + (5) + ",12:PE203,HMULT1,VMULT1|" + "Delivery Remarks :  " + remarks1 + "|" + endCommand;
                lp.write(printData.getBytes());
                Thread.sleep(100);
                if (remarks2.length() > 0) {
                    printData = startCommand + "@" + (5) + ",12:PE203,HMULT1,VMULT1|" + "" + remarks2 + "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(100);
                }
            }
            if (invoiceDo.remarks != null) {
//                    note = podDo.getNotes();
                topSpace = topSpace + 40;
                String remarks1 = "";
                String remarks2 = "";
                if (invoiceDo.remarks.length() > 70) {
                    remarks1 = invoiceDo.remarks.substring(0, 70);
                    remarks2 = invoiceDo.remarks.substring(70, invoiceDo.remarks.length());
                } else {
                    remarks1 = invoiceDo.remarks;

                }
                printData = startCommand + "@" + (5) + ",12:PE203,HMULT1,VMULT1|" + "Invoice Remarks :  " + remarks1 + "|" + endCommand;
                lp.write(printData.getBytes());
                Thread.sleep(100);
                if (remarks2.length() > 0) {
                    printData = startCommand + "@" + (5) + ",12:PE203,HMULT1,VMULT1|" + "" + remarks2 + "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(100);
                }
            }
            CustomerDo customerDo = StorageManager.getInstance(context).getCurrentSpotSalesCustomer(context);
            String shipmentProductsType = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "");
            if (shipmentProductsType.equalsIgnoreCase(AppConstants.FixedQuantityProduct) && customerDo.resFlag == 2) {
                if (invoiceDo.expiryDate.length() > 0) {
                    String dqMonth = invoiceDo.expiryDate.substring(4, 6);
                    String dqyear = invoiceDo.expiryDate.substring(0, 4);
                    String dqDate = invoiceDo.expiryDate.substring(Math.max(invoiceDo.expiryDate.length() - 2, 0));
                    printData = startCommand + "@" + (5) + ",12:PE203,HMULT1,VMULT1|" + "1.  " + context.getResources().getString(R.string.eng_qsn1)
                            + "  -    " + dqDate + "-" + dqMonth + "-" + dqyear + "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(100);

                    String s = "  " + context.getResources().getString(R.string.arb_qsn1);
                    StringBuffer c = new StringBuffer(s);
                    String s1 = String.valueOf(c.reverse());
                    lp.write(new byte[]{0x1B, 'w', '%'});
                    lp.write(s1.getBytes("ISO-8859-6"));

//                    lp.write(startCommand + "@" + (5) + ",12:PE203,HMULT1,VMULT1|" + s1.getBytes("ISO-8859-6")
//                            + "  -    " + dqDate + "-" + dqMonth + "-" + dqyear + "|" + endCommand);

                }

                if (invoiceDo.qsn2 == 2) {

                    String qsn2 = "YES";

                    printData = startCommand + "@" + (1) + ",12:PE203,HMULT1,VMULT1|" + "2.  " + context.getResources().getString(R.string.eng_qsn2)
                            + "                    -     " + qsn2 + "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(100);





                    String sa2 = "  " + context.getResources().getString(R.string.arb_qsn2);
                    StringBuffer csa2 = new StringBuffer(sa2);
                    String sa21 = String.valueOf(csa2.reverse());
                    lp.write(new byte[]{0x1B, 'w', '%'});
                    lp.write(sa21.getBytes("ISO-8859-6"));
//                    lp.write(startCommand + "@" + (5) + ",12:PE203,HMULT1,VMULT1|" + "2.  " + sa21.getBytes("ISO-8859-6")
//                            + "                    -     " + qsn2 + "|" + endCommand);



                } else {
                    String qsn2 = "NO";

                    printData = startCommand + "@" + (1) + ",12:PE203,HMULT1,VMULT1|" + "2.  " + context.getResources().getString(R.string.eng_qsn2) + "                  -     " + qsn2 + "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(100);

                    String s = "  " + context.getResources().getString(R.string.arb_qsn2);
                    StringBuffer c = new StringBuffer(s);
                    String s1 = String.valueOf(c.reverse());


//                    String arabT="صلاحية أسطوانة الغاز";
                    lp.write(new byte[]{0x1B, 'w', '%'});
                    lp.write(s1.getBytes("ISO-8859-6"));
                }


                if (invoiceDo.qsn3 == 2) {

                    String qsn3 = "YES";

                    printData = startCommand + "@" + (1) + ",12:PE203,HMULT1,VMULT1|" + "3.  " + context.getResources().getString(R.string.eng_qsn3) + "               -     "
                            + qsn3 + "|" + endCommand;
                    lp.write(printData.getBytes());



                    String sa3 = "  " + context.getResources().getString(R.string.arb_qsn3);
                    StringBuffer csa3 = new StringBuffer(sa3);
                    String scsa3 = String.valueOf(csa3.reverse());
                    lp.write(new byte[]{0x1B, 'w', '%'});
//                    lp.write(startCommand + "@" + (5) + ",12:PE203,HMULT1,VMULT1|" + "2.  " + scsa3.getBytes("ISO-8859-6")
//                            + "                    -     " + qsn3 + "|" + endCommand);
                    lp.write(scsa3.getBytes("ISO-8859-6"));

                    Thread.sleep(100);

                } else {
                    String qsn3 = "NO";

                    printData = startCommand + "@" + (5) + ",12:PE203,HMULT1,VMULT1|" + "3.  " + context.getResources().getString(R.string.eng_qsn3) + "               -    " + qsn3 + "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(100);
                }


                if (invoiceDo.qsn4 == 2) {

                    String qsn4 = "YES";

                    printData = startCommand + "@" + (1) + ",12:PE203,HMULT1,VMULT1|" + "4.  " + context.getResources().getString(R.string.eng_qsn4) + " -     "
                            + qsn4 + "|" + endCommand;
                    lp.write(printData.getBytes());


                    String scsa4 = "  " + context.getResources().getString(R.string.arb_qsn4);
                    StringBuffer cscsa4 = new StringBuffer(scsa4);
                    String s1scsa4 = String.valueOf(cscsa4.reverse());
                    lp.write(new byte[]{0x1B, 'w', '%'});
//                    lp.write(startCommand + "@" + (5) + ",12:PE203,HMULT1,VMULT1|" + "2.  " + s1scsa4.getBytes("ISO-8859-6")
//                            + "                    -     " + qsn4 + "|" + endCommand);
                    lp.write(s1scsa4.getBytes("ISO-8859-6"));

                    Thread.sleep(100);


                } else {
                    String qsn4 = "NO";

                    printData = startCommand + "@" + (5) + ",12:PE203,HMULT1,VMULT1|" + "4.  " + context.getResources().getString(R.string.eng_qsn4) + " -     " + qsn4 + "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(100);

                }

                if (invoiceDo.qsn5 == 2) {

                    String qsn5 = "YES";

                    printData = startCommand + "@" + (1) + ",12:PE203,HMULT1,VMULT1|" + "5.  " + context.getResources().getString(R.string.eng_qsn5) + " -     "
                            + qsn5 + "|" + endCommand;
                    lp.write(printData.getBytes());


                    String sscsa5 = "  " + context.getResources().getString(R.string.arb_qsn5);
                    StringBuffer cscsa5 = new StringBuffer(sscsa5);
                    String s1scsa5 = String.valueOf(cscsa5.reverse());
                    lp.write(new byte[]{0x1B, 'w', '%'});
//                    lp.write(startCommand + "@" + (5) + ",12:PE203,HMULT1,VMULT1|" + "2.  " + s1scsa5.getBytes("ISO-8859-6")
//                            + "                    -     " + qsn5 + "|" + endCommand);

                    lp.write(s1scsa5.getBytes("ISO-8859-6"));

                    Thread.sleep(100);


                } else {
                    String qsn5 = "NO";

                    printData = startCommand + "@" + (5) + ",12:PE203,HMULT1,VMULT1|" + "5.  " + context.getResources().getString(R.string.eng_qsn5) + " -     " + qsn5 + "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(100);

                }

            }


//
//            printData = startCommand + "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|Remarks :-  " + invoiceDo.remarks + "|" + endCommand;
//            topSpace = topSpace + 120;
//            lp.write(printData.getBytes());
//            Thread.sleep(100);

            String printNote = startCommand + "@" + topSpace + ",1:PE203,HMULT1,VMULT1|this is computer generated invoice no signature or stamp required|" + endCommand;
            lp.write(printNote.getBytes());
            topSpace = 0;
            printDynamicFooter(invoiceDo, lp);
            printSignature(invoiceDo.signature, lp, progressListener);

            printQRCode(context, lp, invoiceDo.invoiceNumber);
            lp.newLine(7);
            sResult = "Number of bytes sent to printer: " + lp.getBytesWritten();
        } catch (BadPrinterStateException ex) {
            lp.removePrintProgressListener(progressListener);
            sResult = "Printer error detected: " + ex.getMessage() + ". Please correct the error and try again.";
        } catch (LinePrinterException ex) {
            sResult = "LinePrinterException: " + ex.getMessage();
        } catch (Exception ex) {
            if (ex.getMessage() != null)
                sResult = "Unexpected exception: " + ex.getMessage();
            else
                sResult = "Unexpected exception.";
        } finally {
            ((BaseActivity) context).hideLoader();
            if (lp != null) {
                try {
                    lp.disconnect();  // Disconnects from the printer
                } catch (Exception ex) {
                }
            }
        }
    }

    private void printActivityReport(String sPrinterURI, LinePrinter.ExtraSettings exSettings,
                                     PrintProgressListener progressListener, ArrayList<Serializable> transactionList) {
        LinePrinter lp = null;
        try {
            String name = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.LOGIN_DRIVER_NAME, "");
            String id = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "");

            lp = new LinePrinter(printConfigDo.jsonCmdAttribStr, printConfigDo.printerName, sPrinterURI, exSettings);
            lp.addPrintProgressListener(progressListener);
            int numtries = 0;
            int maxretry = 2;
            while (numtries < maxretry) {
                try {
                    lp.connect();  // Connects to the printer
                    break;
                } catch (LinePrinterException ex) {
                    numtries++;
                    Thread.sleep(1000);
                }
            }
            if (numtries == maxretry) lp.connect();//Final retry
            int[] results = lp.getStatus();
            if (results != null) {
                for (int err = 0; err < results.length; err++) {
                    if (results[err] == 223) {
                        throw new BadPrinterStateException("Paper out");
                    } else if (results[err] == 227) {
                        throw new BadPrinterStateException("Printer lid open");
                    }
                }
            }
//
//
//            if(transactionMainDo.companyCode.equalsIgnoreCase("U2")||transactionMainDo.companyCode.equalsIgnoreCase("U3")){
//                lp.writeGraphicBase64(PrinterConstants.base64Footer, LinePrinter.GraphicRotationDegrees.DEGREE_0, 280, 250,250);
//
//            }else {
//                lp.writeGraphicBase64(PrinterConstants.base64LogoPng, LinePrinter.GraphicRotationDegrees.DEGREE_0, 280, 250,250);
//
//            }
            TransactionCashMainDo transactionCashMainDo = (TransactionCashMainDo) transactionList.get(0);
            TransactionChequeMainDo transactionChequeMainDo = (TransactionChequeMainDo) transactionList.get(1);

            lp.writeGraphicBase64(PrinterConstants.base64LogoPng, LinePrinter.GraphicRotationDegrees.DEGREE_0, 280, 250, 250);

            Thread.sleep(100);
            lp.newLine(1);
            byte[] easymode = new byte[]{0x1b, 0x45, 0x5a}; // [ESC]EZ
            lp.write(easymode);
            int topSpace = 0;
            String printData = "";
            String startCommand = "\u001BEZ{PRINT:\n";
            String endCommand = "}{LP}\n";
            int leftSpace = 0;//225
            printData = startCommand + "@" + topSpace + ",225:PE203,HMULT1,VMULT2|Brothers Gas Bottling & Distribution Co.LLC|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand + "@" + topSpace + ",340:PE203,HMULT1,VMULT2|Payment History|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|User Id|" +
                    "@" + topSpace + ",250:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",270:PE203,HMULT1,VMULT1|" + id + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|User Name|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + name + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Total Cash Reciepts|" +
                    "@" + topSpace + ",250:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",270:PE203,HMULT1,VMULT1|" + transactionCashMainDo.cashReciepts + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Date|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + CalendarUtils.getPDFDate() + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Total Cheque Reciepts|" +
                    "@" + topSpace + ",250:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",270:PE203,HMULT1,VMULT1|" + transactionChequeMainDo.chequeReciepts + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Time|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + CalendarUtils.getPDFTime() + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;

            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|From Date|" +
                    "@" + topSpace + ",250:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",270:PE203,HMULT1,VMULT1|" + AppConstants.Trans_From_Date + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|To Date|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + AppConstants.Trans_To_Date + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 30;
            if (transactionCashMainDo.transactionCashDOS.size() > 0) {
                printData = startCommand +
                        "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT2|Rec.No.|" +
                        "@" + (topSpace + 5) + ",220:PE203,HMULT1,VMULT2|Customer Name|" +
                        "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT2|Cash|" + endCommand;
                lp.write(printData.getBytes());
                Thread.sleep(100);
            }

            topSpace = 0;
            for (int i = 0; i < transactionCashMainDo.transactionCashDOS.size(); i++) {
                TransactionCashDO transactionCashDO = transactionCashMainDo.transactionCashDOS.get(i);
                printData = startCommand +
                        "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|" + transactionCashDO.cashPayment + "|" +
                        "@" + (topSpace + 5) + ",220:PE203,HMULT1,VMULT1|" + transactionCashDO.cashCustomer + "|" +
                        "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT1|" + transactionCashDO.cashAmount + "|" + endCommand;
                lp.write(printData.getBytes());
                Thread.sleep(100);
                topSpace = 0;

            }
            if (transactionCashMainDo.transactionCashDOS.size() > 0) {
                printData = startCommand +
                        "@" + topSpace + ",10:PE203,HMULT1,VMULT2|Total Amount Paid|" +
                        "@" + topSpace + ",240:PE203,HMULT1,VMULT2|:|" +
                        "@" + topSpace + ",650:PE203,HMULT1,VMULT2|" + transactionCashMainDo.totalAmount + " AED" + "|" + endCommand;
                lp.write(printData.getBytes());
                Thread.sleep(100);
            }

            topSpace = 0;
            if (transactionChequeMainDo.transactionChequeDOS.size() > 0) {

                printData = startCommand +
                        "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT2|Rec.No.|" +
                        "@" + (topSpace + 5) + ",220:PE203,HMULT1,VMULT2|Customer Name|" +
                        "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT2|Cheque|" + endCommand;
                lp.write(printData.getBytes());
                Thread.sleep(100);
            }

            topSpace = 0;
            for (int i = 0; i < transactionChequeMainDo.transactionChequeDOS.size(); i++) {
                TransactionChequeDO transactionChequeDO = transactionChequeMainDo.transactionChequeDOS.get(i);
                printData = startCommand +
                        "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|" + transactionChequeDO.chequePayment + "|" +
                        "@" + (topSpace + 5) + ",220:PE203,HMULT1,VMULT1|" + transactionChequeDO.chequeCustomer + "|" +
                        "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT1|" + transactionChequeDO.chequeAmount + "|" + endCommand;
                lp.write(printData.getBytes());
                Thread.sleep(150);
                topSpace = 0;
            }
            if (transactionChequeMainDo.transactionChequeDOS.size() > 0) {
                printData = startCommand +
                        "@" + topSpace + ",10:PE203,HMULT1,VMULT2|Total Amount Paid|" +
                        "@" + topSpace + ",240:PE203,HMULT1,VMULT2|:|" +
                        "@" + topSpace + ",650:PE203,HMULT1,VMULT2|" + transactionChequeMainDo.totalAmount + " AED" + "|" + endCommand;
                lp.write(printData.getBytes());
                Thread.sleep(100);
            }


            lp.newLine(7);
            sResult = "Number of bytes sent to printer: " + lp.getBytesWritten();
        } catch (BadPrinterStateException ex) {
            // Stop listening for printer events.
            lp.removePrintProgressListener(progressListener);
            sResult = "Printer error detected: " + ex.getMessage() + ". Please correct the error and try again.";
        } catch (LinePrinterException ex) {
            sResult = "LinePrinterException: " + ex.getMessage();
        } catch (Exception ex) {
            if (ex.getMessage() != null)
                sResult = "Unexpected exception: " + ex.getMessage();
            else
                sResult = "Unexpected exception.";
        } finally {
            ((BaseActivity) context).hideLoader();
            if (lp != null) {
                try {
                    lp.disconnect();  // Disconnects from the printer
                    lp.close();  // Releases resources
                    ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.DELIVERY_NOTE, "SUCCESS");

                } catch (Exception ex) {
                }
            }
        }
    }

    private void printUserActivityReport(String sPrinterURI, LinePrinter.ExtraSettings exSettings,
                                         PrintProgressListener progressListener, UserActivityReportMainDO userActivityReportMainDO) {
        LinePrinter lp = null;
        try {
            String name = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.LOGIN_DRIVER_NAME, "");
            String id = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "");
            String siteId = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID, "");
            String svCode = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.CV_PLATE, "");
            String nvCode = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.NON_VEHICLE_CODE, "");

            lp = new LinePrinter(printConfigDo.jsonCmdAttribStr, printConfigDo.printerName, sPrinterURI, exSettings);
            lp.addPrintProgressListener(progressListener);
            int numtries = 0;
            int maxretry = 2;
            while (numtries < maxretry) {
                try {
                    lp.connect();  // Connects to the printer
                    break;
                } catch (LinePrinterException ex) {
                    numtries++;
                    Thread.sleep(1000);
                }
            }
            if (numtries == maxretry) lp.connect();//Final retry
            int[] results = lp.getStatus();
            if (results != null) {
                for (int err = 0; err < results.length; err++) {
                    if (results[err] == 223) {
                        throw new BadPrinterStateException("Paper out");
                    } else if (results[err] == 227) {
                        throw new BadPrinterStateException("Printer lid open");
                    }
                }
            }
//
//
            if (userActivityReportMainDO.companyCode.equalsIgnoreCase("U2") || userActivityReportMainDO.companyCode.equalsIgnoreCase("U3")) {
                lp.writeGraphicBase64(PrinterConstants.base64Footer, LinePrinter.GraphicRotationDegrees.DEGREE_0, 280, 250, 250);

            } else {
                lp.writeGraphicBase64(PrinterConstants.base64LogoPng, LinePrinter.GraphicRotationDegrees.DEGREE_0, 280, 250, 250);

            }
//            UserActivityReportMainDO transactionCashMainDo = (TransactionCashMainDo) transactionList.get(0);
//            TransactionChequeMainDo transactionChequeMainDo = (TransactionChequeMainDo) transactionList.get(1);

//            lp.writeGraphicBase64(PrinterConstants.base64Footer, LinePrinter.GraphicRotationDegrees.DEGREE_0, 280, 250, 250);

            Thread.sleep(100);
            lp.newLine(1);
            byte[] easymode = new byte[]{0x1b, 0x45, 0x5a}; // [ESC]EZ
            lp.write(easymode);
            int topSpace = 0;
            String printData = "";
            String startCommand = "\u001BEZ{PRINT:\n";
            String endCommand = "}{LP}\n";
            int leftSpace = 0;//225
//            if (userActivityReportMainDO.companyDescription.toLowerCase().contains("taqat")) {
////                leftSpace = 320 - (paymentPdfDO.company.length()/2);
//                leftSpace = 320;
//            } else {
//                leftSpace = 245;
//            }
//            printData = startCommand + "@" + topSpace + "," + leftSpace + ":PE203,HMULT1,VMULT2|" + userActivityReportMainDO.companyDescription + "|" + endCommand;
//            lp.write(printData.getBytes());
//            Thread.sleep(100);
            topSpace = 0;
            topSpace = 0;
            printData = startCommand + "@" + topSpace + ",240:PE203,HMULT1,VMULT2|Van Sales User Activity Report|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|User|" +
                    "@" + topSpace + ",180:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",200:PE203,HMULT1,VMULT1|" + id + " - " + driverName + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Site|" +
                    "@" + topSpace + ",620:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",632:PE203,HMULT1,VMULT1|" + siteId + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            String dMonth = userActivityReportMainDO.transactionDate.substring(4, 6);
            String dyear = userActivityReportMainDO.transactionDate.substring(0, 4);
            String dDate = userActivityReportMainDO.transactionDate.substring(Math.max(userActivityReportMainDO.transactionDate.length() - 2, 0));

//            String dMonth = userActivityReportMainDO.transactionDate.substring(4, 6);
//            String dyear =userActivityReportMainDO.transactionDate.substring(0, 4);
//            String dDate = userActivityReportMainDO.transactionDate.substring(Math.max(userActivityReportMainDO.transactionDate.length() - 2, 0));

            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Transaction Date|" +
                    "@" + topSpace + ",180:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",200:PE203,HMULT1,VMULT1|" + dDate + "-" + dMonth + "-" + dyear + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Print Date|" +
                    "@" + topSpace + ",620:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",632:PE203,HMULT1,VMULT1|" + CalendarUtils.getPDFDate() + "|" + endCommand;
            topSpace = 0;
            lp.write(printData.getBytes());
            Thread.sleep(100);

            PreferenceUtils preferenceUtils = new PreferenceUtils(context);
            String vehicleNumber = preferenceUtils.getStringFromPreference(PreferenceUtils.CV_PLATE, "");
            String vehicleCode = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_CODE, "");

            topSpace = 0;
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Vehicle Code|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + vehicleCode + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Plate No.|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + vehicleNumber + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
//            if (svCode.isEmpty()) {
//                printData = startCommand +
//                        "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Vehicle Number|" +
//                        "@" + topSpace + ",180:PE203,HMULT1,VMULT1|:|" +
//                        "@" + topSpace + ",200:PE203,HMULT1,VMULT1|" + svCode + "|" + endCommand;
//            } else {
//                printData = startCommand +
//                        "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Vehicle Number|" +
//                        "@" + topSpace + ",180:PE203,HMULT1,VMULT1|:|" +
//                        "@" + topSpace + ",200:PE203,HMULT1,VMULT1|" + svCode + "|" + endCommand;
//            }
//
//
//            lp.write(printData.getBytes());
//            Thread.sleep(100);

//            lp.write(printData.getBytes());
//            Thread.sleep(100);

            if (userActivityReportMainDO.spotDeliveryDOS != null && userActivityReportMainDO.spotDeliveryDOS.size() > 0) {
                topSpace = 30;
                printData = startCommand + "@" + topSpace + ",340:PE203,HMULT1,VMULT2|Sales Deliveries|" + endCommand;
                lp.write(printData.getBytes());
                Thread.sleep(100);
                topSpace = 10;
                printData = startCommand +
                        "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT2|Doc.No.|" +
                        "@" + (topSpace + 5) + ",220:PE203,HMULT1,VMULT2|Customer Name|" +
                        "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT2|Quantity|" + endCommand;
                lp.write(printData.getBytes());
                Thread.sleep(100);
                topSpace = 0;
                for (int i = 0; i < userActivityReportMainDO.spotDeliveryDOS.size(); i++) {
                    SpotDeliveryDO spotDeliveryDO = userActivityReportMainDO.spotDeliveryDOS.get(i);
                    printData = startCommand +
                            "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|" + spotDeliveryDO.documentNumber + "|" +
                            "@" + (topSpace + 5) + ",220:PE203,HMULT1,VMULT1|" + spotDeliveryDO.customerName + "|" +
                            "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT1|" + spotDeliveryDO.quantity + "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(100);
                    topSpace = 0;

                }
                printData = startCommand +
                        "@" + topSpace + ",10:PE203,HMULT1,VMULT2|Total|" +
                        "@" + topSpace + ",240:PE203,HMULT1,VMULT2|:|" +
                        "@" + topSpace + ",650:PE203,HMULT1,VMULT2|" + userActivityReportMainDO.deliveryTotalQty + " " + userActivityReportMainDO.spotDeliveryDOS.get(0).productUnit + "|" + endCommand;
                lp.write(printData.getBytes());
                Thread.sleep(100);
            }


            printData = startCommand + "@" + topSpace + ",340:PE203,HMULT1,VMULT2|Sales Invoices|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 10;
            printData = startCommand +
                    "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT2|Doc.No.|" +
                    "@" + (topSpace + 5) + ",220:PE203,HMULT1,VMULT2|Customer Name|" +
                    "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT2|Amount|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            if (userActivityReportMainDO.salesInvoiceDOS != null && userActivityReportMainDO.salesInvoiceDOS.size() > 0) {
                for (int i = 0; i < userActivityReportMainDO.salesInvoiceDOS.size(); i++) {
                    SalesInvoiceDO salesInvoiceDO = userActivityReportMainDO.salesInvoiceDOS.get(i);
                    printData = startCommand +
                            "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|" + salesInvoiceDO.documentNumber + "|" +
                            "@" + (topSpace + 5) + ",220:PE203,HMULT1,VMULT1|" + salesInvoiceDO.customerName + "|" +
                            "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT1|" + salesInvoiceDO.amount + "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(150);
                    topSpace = 0;
                }

            }
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT2|Total|" +
                    "@" + topSpace + ",240:PE203,HMULT1,VMULT2|:|" +
                    "@" + topSpace + ",650:PE203,HMULT1,VMULT2|" + userActivityReportMainDO.invoiceTotalAmount + " AED" + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            printData = startCommand + "@" + topSpace + ",340:PE203,HMULT1,VMULT2|Cash Receipts|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 10;
            printData = startCommand +
                    "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT2|Doc.No.|" +
                    "@" + (topSpace + 5) + ",220:PE203,HMULT1,VMULT2|Customer Name|" +
                    "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT2|Amount|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            if (userActivityReportMainDO.cashDOS != null && userActivityReportMainDO.cashDOS.size() > 0) {

                topSpace = 0;
                for (int i = 0; i < userActivityReportMainDO.cashDOS.size(); i++) {
                    CashDO cashDO = userActivityReportMainDO.cashDOS.get(i);
                    printData = startCommand +
                            "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|" + cashDO.paymentId + "|" +
                            "@" + (topSpace + 5) + ",220:PE203,HMULT1,VMULT1|" + cashDO.customerName + "|" +
                            "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT1|" + cashDO.amount + "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(150);
                    topSpace = 0;
                }
            }
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT2|Total|" +
                    "@" + topSpace + ",240:PE203,HMULT1,VMULT2|:|" +
                    "@" + topSpace + ",650:PE203,HMULT1,VMULT2|" + userActivityReportMainDO.cashTotalAmount + " AED" + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);


            printData = startCommand + "@" + topSpace + ",340:PE203,HMULT1,VMULT2|Cheque Receipts|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 10;
            printData = startCommand +
                    "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT2|Doc.No.|" +
                    "@" + (topSpace + 5) + ",220:PE203,HMULT1,VMULT2|Customer Name|" +
                    "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT2|Amount|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            if (userActivityReportMainDO.chequeDOS != null && userActivityReportMainDO.chequeDOS.size() > 0) {
                topSpace = 0;
                for (int i = 0; i < userActivityReportMainDO.chequeDOS.size(); i++) {
                    ChequeDO chequeDO = userActivityReportMainDO.chequeDOS.get(i);
                    printData = startCommand +
                            "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|" + chequeDO.paymentId + "|" +
                            "@" + (topSpace + 5) + ",220:PE203,HMULT1,VMULT1|" + chequeDO.customerName + "|" +
                            "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT1|" + chequeDO.amount + "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(150);
                    topSpace = 0;
                }
            }
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT2|Total|" +
                    "@" + topSpace + ",240:PE203,HMULT1,VMULT2|:|" +
                    "@" + topSpace + ",650:PE203,HMULT1,VMULT2|" + userActivityReportMainDO.chequeTotalAmount + " AED" + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);


            printData = startCommand + "@" + topSpace + ",340:PE203,HMULT1,VMULT2|Opening Stock|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 10;

            if (userActivityReportMainDO.productDOS != null && userActivityReportMainDO.productDOS.size() > 0) {
                printData = startCommand +
                        "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT2|Product|" +
                        "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT2|Quantity|" +
                        endCommand;
                lp.write(printData.getBytes());
                Thread.sleep(100);
                topSpace = 0;
                for (int i = 0; i < userActivityReportMainDO.productDOS.size(); i++) {
                    LoadStockDO loadStockDO = userActivityReportMainDO.productDOS.get(i);
                    printData = startCommand +
                            "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|" + loadStockDO.productDescription + "|" +
                            "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT1|" + loadStockDO.quantity + "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(150);
                    topSpace = 0;
                }
                printData = startCommand +
                        "@" + topSpace + ",10:PE203,HMULT1,VMULT2|Total|" +
                        "@" + topSpace + ",240:PE203,HMULT1,VMULT2|:|" +
                        "@" + topSpace + ",650:PE203,HMULT1,VMULT2|" + userActivityReportMainDO.productsTotalQty + " " + userActivityReportMainDO.productDOS.get(0).stockUnit + "|" + endCommand;
                lp.write(printData.getBytes());
                Thread.sleep(100);
            }




            if (userActivityReportMainDO.salesDOS != null && userActivityReportMainDO.salesDOS.size() > 0) {
                printData = startCommand + "@" + topSpace + ",340:PE203,HMULT1,VMULT2|Sales Summary|" + endCommand;
                lp.write(printData.getBytes());
                Thread.sleep(100);
                topSpace = 10;
                printData = startCommand +
                        "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT2|Product|" +
                        "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT2|Quantity|" +
                        endCommand;

                lp.write(printData.getBytes());
                Thread.sleep(100);
                topSpace = 0;
                for (int i = 0; i < userActivityReportMainDO.salesDOS.size(); i++) {
                    LoadStockDO loadStockDO = userActivityReportMainDO.salesDOS.get(i);
                    printData = startCommand +
                            "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|" + loadStockDO.productDescription + "|" +
                            "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT1|" + loadStockDO.quantity + "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(150);
                    topSpace = 0;
                }
                printData = startCommand +
                        "@" + topSpace + ",10:PE203,HMULT1,VMULT2|Total|" +
                        "@" + topSpace + ",240:PE203,HMULT1,VMULT2|:|" +
                        "@" + topSpace + ",650:PE203,HMULT1,VMULT2|" + userActivityReportMainDO.salesTotalQty + " " + userActivityReportMainDO.salesDOS.get(0).stockUnit + "|" + endCommand;
                lp.write(printData.getBytes());
                Thread.sleep(100);
            }


            if (userActivityReportMainDO.issueDOS != null && userActivityReportMainDO.issueDOS.size() > 0) {
                topSpace = 30;
                printData = startCommand + "@" + topSpace + ",340:PE203,HMULT1,VMULT2|Empty Cylinder Loan Issue|" + endCommand;
                lp.write(printData.getBytes());
                Thread.sleep(100);
                topSpace = 10;
                printData = startCommand +
                        "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT2|Doc.No.|" +
                        "@" + (topSpace + 5) + ",220:PE203,HMULT1,VMULT2|Product|" +
                        "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT2|Quantity|" + endCommand;
                lp.write(printData.getBytes());
                Thread.sleep(100);
                topSpace = 0;
                for (int i = 0; i < userActivityReportMainDO.issueDOS.size(); i++) {
                    LoadStockDO spotDeliveryDO = userActivityReportMainDO.issueDOS.get(i);
                    printData = startCommand +
                            "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|" + spotDeliveryDO.shipmentNumber + "|" +
                            "@" + (topSpace + 5) + ",220:PE203,HMULT1,VMULT1|" + spotDeliveryDO.productDescription + "|" +
                            "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT1|" + spotDeliveryDO.quantity + "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(100);
                    topSpace = 0;

                }
                printData = startCommand +
                        "@" + topSpace + ",10:PE203,HMULT1,VMULT2|Total|" +
                        "@" + topSpace + ",240:PE203,HMULT1,VMULT2|:|" +
                        "@" + topSpace + ",650:PE203,HMULT1,VMULT2|" + userActivityReportMainDO.issueTotalQty + " " + userActivityReportMainDO.issueDOS.get(0).productUnit + "|" + endCommand;
                lp.write(printData.getBytes());
                Thread.sleep(100);
            }


            if (userActivityReportMainDO.recieptDOS != null && userActivityReportMainDO.recieptDOS.size() > 0) {
                topSpace = 30;
                printData = startCommand + "@" + topSpace + ",340:PE203,HMULT1,VMULT2|Empty Cylinder Loan Reciept|" + endCommand;
                lp.write(printData.getBytes());
                Thread.sleep(100);
                topSpace = 10;
                printData = startCommand +
                        "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT2|Doc.No.|" +
                        "@" + (topSpace + 5) + ",220:PE203,HMULT1,VMULT2|Product|" +
                        "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT2|Quantity|" + endCommand;
                lp.write(printData.getBytes());
                Thread.sleep(100);
                topSpace = 0;
                for (int i = 0; i < userActivityReportMainDO.recieptDOS.size(); i++) {
                    LoadStockDO spotDeliveryDO = userActivityReportMainDO.recieptDOS.get(i);
                    printData = startCommand +
                            "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|" + spotDeliveryDO.shipmentNumber + "|" +
                            "@" + (topSpace + 5) + ",220:PE203,HMULT1,VMULT1|" + spotDeliveryDO.productDescription + "|" +
                            "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT1|" + spotDeliveryDO.quantity + "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(100);
                    topSpace = 0;

                }
                printData = startCommand +
                        "@" + topSpace + ",10:PE203,HMULT1,VMULT2|Total|" +
                        "@" + topSpace + ",240:PE203,HMULT1,VMULT2|:|" +
                        "@" + topSpace + ",650:PE203,HMULT1,VMULT2|" + userActivityReportMainDO.recieptTotalQty + " " + userActivityReportMainDO.recieptDOS.get(0).productUnit + "|" + endCommand;
                lp.write(printData.getBytes());
                Thread.sleep(100);
            }


            if (userActivityReportMainDO.stockDOS != null && userActivityReportMainDO.stockDOS.size() > 0) {
                printData = startCommand + "@" + topSpace + ",340:PE203,HMULT1,VMULT2|Closing Stock|" + endCommand;
                lp.write(printData.getBytes());
                Thread.sleep(100);
                topSpace = 10;
                printData = startCommand +
                        "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT2|Product|" +
                        "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT2|Quantity|" +
                        endCommand;

                lp.write(printData.getBytes());
                Thread.sleep(100);
                topSpace = 0;
                for (int i = 0; i < userActivityReportMainDO.stockDOS.size(); i++) {
                    LoadStockDO loadStockDO = userActivityReportMainDO.stockDOS.get(i);
                    printData = startCommand +
                            "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|" + loadStockDO.productDescription + "|" +
                            "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT1|" + loadStockDO.quantity + "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(150);
                    topSpace = 0;
                }
                printData = startCommand +
                        "@" + topSpace + ",10:PE203,HMULT1,VMULT2|Total|" +
                        "@" + topSpace + ",240:PE203,HMULT1,VMULT2|:|" +
                        "@" + topSpace + ",650:PE203,HMULT1,VMULT2|" + userActivityReportMainDO.stockTotalQty + " " + userActivityReportMainDO.productDOS.get(0).stockUnit + "|" + endCommand;
                lp.write(printData.getBytes());
                Thread.sleep(100);
            }

            lp.newLine(7);
            sResult = "Number of bytes sent to printer: " + lp.getBytesWritten();
        } catch (BadPrinterStateException ex) {
            // Stop listening for printer events.
            lp.removePrintProgressListener(progressListener);
            sResult = "Printer error detected: " + ex.getMessage() + "Please correct the error and try again.";
        } catch (LinePrinterException ex) {
            sResult = "LinePrinterException: " + ex.getMessage();
        } catch (Exception ex) {
            if (ex.getMessage() != null)
                sResult = "Unexpected exception: " + ex.getMessage();
            else
                sResult = "Unexpected exception.";
        } finally {
            ((BaseActivity) context).hideLoader();
            if (lp != null) {
                try {
                    lp.disconnect();  // Disconnects from the printer
                    lp.close();  // Releases resources
                    ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.DELIVERY_NOTE, "SUCCESS");

                } catch (Exception ex) {
                }
            }
        }
    }


    private static void printQRCode(Context context, LinePrinter lp, String invoiceNumber) {

        try {
            Bitmap qrCode = Util.encodeAsBitmap(context, invoiceNumber);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            Bitmap b = Bitmap.createScaledBitmap(qrCode, 120, 120, false);
            b.compress(Bitmap.CompressFormat.JPEG, 30, stream);
            byte[] byteArray = stream.toByteArray();
            String encodedImage = Base64.encodeToString(byteArray, Base64.DEFAULT);

            if (encodedImage != null) {
                lp.writeGraphicBase64(encodedImage, LinePrinter.GraphicRotationDegrees.DEGREE_0,
                        280,  // Offset in printhead dots from the left of the page
                        150, // Desired graphic width on paper in printhead dots
                        150); // Desired graphic height on paper in printhead dots
            }
            Thread.sleep(100);
        } catch (Exception e) {

        }
    }

    private void printPaymentReport(String sPrinterURI, LinePrinter.ExtraSettings exSettings, PrintProgressListener progressListener, PaymentPdfDO paymentPdfDO) {
        LinePrinter lp = null;
        try {
            lp = new LinePrinter(printConfigDo.jsonCmdAttribStr, printConfigDo.printerName, sPrinterURI, exSettings);
            lp.addPrintProgressListener(progressListener);
            int numtries = 0;
            int maxretry = 2;
            while (numtries < maxretry) {
                try {
                    lp.connect();  // Connects to the printer
                    break;
                } catch (LinePrinterException ex) {
                    numtries++;
                    Thread.sleep(1000);
                }
            }
            if (numtries == maxretry) lp.connect();//Final retry
            int[] results = lp.getStatus();
            if (results != null) {
                for (int err = 0; err < results.length; err++) {
                    if (results[err] == 223) {
                        throw new BadPrinterStateException("Paper out");
                    } else if (results[err] == 227) {
                        throw new BadPrinterStateException("Printer lid open");
                    }
                }
            }
            if (paymentPdfDO.companyCode.equalsIgnoreCase("U2") || paymentPdfDO.companyCode.equalsIgnoreCase("U3")) {
                lp.writeGraphicBase64(PrinterConstants.base64Footer, LinePrinter.GraphicRotationDegrees.DEGREE_0, 300, 250, 250);
            } else {
                lp.writeGraphicBase64(PrinterConstants.base64LogoPng, LinePrinter.GraphicRotationDegrees.DEGREE_0, 300, 250, 250);
            }
            Thread.sleep(100);
            lp.newLine(1);

            int topSpace = 0;
            String printData = "";
            String startACommand = "\u001BEZ{PRINT:\n";
            String endCommand = "}{LP}\n";
            byte[] easymode = new byte[]{0x1b, 0x45, 0x5a}; // [ESC]EZ
            lp.write(easymode);
            int leftSpace = 0;//225
            if (paymentPdfDO.company.toLowerCase().contains("taqat")) {
//                leftSpace = 320 - (paymentPdfDO.company.length()/2);
                leftSpace = 320;
            } else {
                leftSpace = 245;
            }
            printData = startACommand + "@" + topSpace + "," + leftSpace + ":PE203,HMULT1,VMULT2|" + paymentPdfDO.company + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startACommand + "@" + topSpace + ",330:PE203,HMULT1,VMULT2|Receipt Voucher|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            String dMonth = paymentPdfDO.createdDate.substring(4, 6);
            String dyear = paymentPdfDO.createdDate.substring(0, 4);
            String dDate = paymentPdfDO.createdDate.substring(Math.max(paymentPdfDO.createdDate.length() - 2, 0));

            printData = startACommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Receipt No.|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + paymentPdfDO.paymentNumber + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Date|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + dDate + "-" + dMonth + "-" + dyear + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);


            topSpace = 0;
            paymentPdfDO.customerDescription = paymentPdfDO.customerDescription.substring(0, Math.min(paymentPdfDO.customerDescription.length(), 30));
            printData = startACommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Received From|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + paymentPdfDO.customerDescription + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Time|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + paymentPdfDO.createdTime + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startACommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|User ID|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + paymentPdfDO.createUserID +"- V1"+ "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|User Name|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + paymentPdfDO.createUserName + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
//            PreferenceUtils preferenceUtils = new PreferenceUtils(context);
//            String vehicleNumber = preferenceUtils.getStringFromPreference(PreferenceUtils.CV_PLATE, "");
//            String vehicleCode = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_CODE, "");

//            topSpace = 0;
//            printData = startACommand +
//                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Vehicle Code|" +
//                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
//                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + vehicleCode + "|" +
//                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Plate No.|" +
//                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
//                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + vehicleNumber + "|" + endCommand;
//            lp.write(printData.getBytes());
//            Thread.sleep(100);
            topSpace = 0;
            printData = startACommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Address|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + getAddress(paymentPdfDO) + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 20;
            printData = startACommand +
                    "@" + topSpace + ",1:HLINE,Length820,Thick2|\n" +
//                    "@" + topSpace + ",2:VLINE,Length40,Thick2|\n" +
//                    "@" + topSpace + ",820:VLINE,Length40,Thick2|\n" +
                    "\n";
            if (paymentPdfDO.cardType.isEmpty()) {
                printData = printData +
                        "@" + (topSpace + 5) + ",10:PE203,HMULT1,VMULT1|Amount|\n" +
                        "@" + (topSpace + 5) + ",170:PE203,HMULT1,VMULT1|:|\n" +
                        "@" + (topSpace + 5) + ",190:PE203,HMULT1,VMULT1|AED " + paymentPdfDO.amount + "|\n";

            } else {
                Double amt = paymentPdfDO.invoiceAmount;

                printData = printData +
                        "@" + (topSpace + 5) + ",10:PE203,HMULT1,VMULT1|Amount|\n" +
                        "@" + (topSpace + 5) + ",170:PE203,HMULT1,VMULT1|:|\n" +
                        "@" + (topSpace + 5) + ",190:PE203,HMULT1,VMULT1|AED " + amt + "|\n";
                topSpace = topSpace + 40;
//                printData = printData +
//                        "@" + topSpace + ",2:VLINE,Length40,Thick2|\n" +
//                        "@" + topSpace + ",820:VLINE,Length40,Thick2|\n" +
//                        "\n";
                printData = printData +
                        "@" + (topSpace + 5) + ",10:PE203,HMULT1,VMULT1|Service Charges|\n" +
                        "@" + (topSpace + 5) + ",170:PE203,HMULT1,VMULT1|:|\n" +
                        "@" + (topSpace + 5) + ",190:PE203,HMULT1,VMULT1|AED " + paymentPdfDO.serviceCharges + "|\n";
                topSpace = topSpace + 40;
//                printData = printData +
//                        "@" + topSpace + ",2:VLINE,Length40,Thick2|\n" +
//                        "@" + topSpace + ",820:VLINE,Length40,Thick2|\n" +
//                        "\n";
                printData = printData +
                        "@" + (topSpace + 5) + ",10:PE203,HMULT1,VMULT1|Vat|\n" +
                        "@" + (topSpace + 5) + ",170:PE203,HMULT1,VMULT1|:|\n" +
                        "@" + (topSpace + 5) + ",190:PE203,HMULT1,VMULT1|AED " + paymentPdfDO.vatAmount + "|\n";

                topSpace = topSpace + 40;
//                printData = printData +
//                        "@" + topSpace + ",2:VLINE,Length40,Thick2|\n" +
//                        "@" + topSpace + ",820:VLINE,Length40,Thick2|\n" +
//                        "\n";
                printData = printData +
                        "@" + (topSpace + 5) + ",10:PE203,HMULT1,VMULT1|Total|\n" +
                        "@" + (topSpace + 5) + ",170:PE203,HMULT1,VMULT1|:|\n" +
                        "@" + (topSpace + 5) + ",190:PE203,HMULT1,VMULT1|AED " + paymentPdfDO.amount + "|\n";


            }
            topSpace = topSpace + 40;
//            printData = printData +
//                    "@" + topSpace + ",2:VLINE,Length40,Thick2|\n" +
//                    "@" + topSpace + ",820:VLINE,Length40,Thick2|\n" +
//                    "\n";
            try {
                double amt = paymentPdfDO.amount;
                int afterDcimalVal = PDFOperations.getInstance().anyMethod(amt);

                printData = printData +
                        "@" + topSpace + ",10:PE203,HMULT1,VMULT1|The Sum of |\n" +
                        "@" + topSpace + ",170:PE203,HMULT1,VMULT1|:|\n" +
                        "@" + topSpace + ",190:PE203,HMULT1,VMULT1|AED " + getAmountInLetters(amt, false) + " and " + getAmountInLetters(afterDcimalVal, true) + "" + "|\n" +
                        "\n" + "\n";

                topSpace = topSpace + 40;
                String paymentType = "";
                if (!TextUtils.isEmpty(paymentPdfDO.chequeNumber)) {
                    paymentType = "By Cheque";
                } else {
                    if (!TextUtils.isEmpty(paymentPdfDO.cardType)) {
                        paymentType = "By Card";
                    } else {
                        paymentType = "By Cash";

                    }
                }
//                printData = printData +
//                        "@" + topSpace + ",2:VLINE,Length40,Thick2|\n" +
//                        "@" + topSpace + ",820:VLINE,Length40,Thick2|\n" +
//                        "\n";
//

                if (paymentType.equalsIgnoreCase("By Cheque")) {
                    printData = printData +
                            "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Payment Method|\n" +
                            "@" + topSpace + ",170:PE203,HMULT1,VMULT1|:|\n" +
                            "@" + topSpace + ",190:PE203,HMULT1,VMULT1|" + paymentType + "|\n" +
                            "@" + topSpace + ",400:PE203,HMULT1,VMULT1|Bank : |\n" +
                            "@" + topSpace + ",500:PE203,HMULT1,VMULT1|" + paymentPdfDO.bank + "|\n" +
                            "\n";
                } else {
                    printData = printData +
                            "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Payment Method|\n" +
                            "@" + topSpace + ",170:PE203,HMULT1,VMULT1|:|\n" +
                            "@" + topSpace + ",190:PE203,HMULT1,VMULT1|" + paymentType + "|\n";
                }


//                printData = printData +
//                        "@" + topSpace + ",2:VLINE,Length40,Thick2|\n" +
//                        "@" + topSpace + ",820:VLINE,Length40,Thick2|\n" ;
//                topSpace=topSpace+40;
                if (paymentType.equalsIgnoreCase("By Cheque")) {
                    topSpace = 40 + topSpace;
                    if (paymentPdfDO.chequeDate.length() > 0) {
                        String aMonth = paymentPdfDO.chequeDate.substring(4, 6);
                        String ayear = paymentPdfDO.chequeDate.substring(0, 4);
                        String aDate = paymentPdfDO.chequeDate.substring(Math.max(paymentPdfDO.chequeDate.length() - 2, 0));
                        if (!TextUtils.isEmpty(paymentPdfDO.chequeNumber)) {
                            paymentPdfDO.chequeNumber = paymentPdfDO.chequeNumber.substring(0, Math.min(paymentPdfDO.chequeNumber.length(), 20));
                            printData = printData +
                                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Cheque No.|\n" +
                                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|:|\n" +
                                    "@" + topSpace + ",190:PE203,HMULT1,VMULT1|" + paymentPdfDO.chequeNumber + "|\n" +
                                    "@" + topSpace + ",400:PE203,HMULT1,VMULT1|Cheque Date: |\n" +
                                    "@" + topSpace + ",580:PE203,HMULT1,VMULT1|" + aDate + "/" + aMonth + "/" + ayear + "|\n";
                        } else {
                            printData = printData +
                                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Cheque No.|\n" +
                                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|:|\n" +
                                    "@" + topSpace + ",190:PE203,HMULT1,VMULT1|" + paymentPdfDO.chequeNumber + "|\n" +
                                    "@" + topSpace + ",400:PE203,HMULT1,VMULT1|Cheque Date: |\n" +
                                    "@" + topSpace + ",580:PE203,HMULT1,VMULT1|" + " " + "|\n" +
                                    "\n";
                        }
//                        printData = printData +
//                                "@" + topSpace + ",2:VLINE,Length40,Thick2|\n" +
//                                "@" + topSpace + ",820:VLINE,Length40,Thick2|\n" ;
                    }
                }

                printData = printData + endCommand;

                lp.write(printData.getBytes());
                Thread.sleep(100);
                topSpace = 0;
                int count = 0;
                if (paymentPdfDO.invoiceDos.size() > 1) {
                    for (int i = 0; i < paymentPdfDO.invoiceDos.size(); i += 3) {
                        if (i == 0) {
                            count = 0;

                        } else {
                            count = count + 3;

                        }
                        int size = paymentPdfDO.invoiceDos.size() - count;
                        topSpace = 0;
                        if (i == paymentPdfDO.invoiceDos.size() - 1) {
                            String pId = "";
                            printData = startACommand;
                            pId = pId + paymentPdfDO.invoiceDos.get(i).invoiceNumber + "";
                            printData = printData +
                                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1||\n" +
                                    "@" + topSpace + ",200:PE203,HMULT1,VMULT1||\n" +
                                    "@" + topSpace + ",220:PE203,HMULT1,VMULT1|" + pId + "|\n";
//                            printData = printData +
//                                    "@" + topSpace + ",2:VLINE,Length40,Thick2|\n" +
//                                    "@" + topSpace + ",820:VLINE,Length40,Thick2|\n";
                            printData = printData + endCommand;

                            lp.write(printData.getBytes());
                            Thread.sleep(10);
//                            topSpace = topSpace +1;

                        } else {
                            String pId = "";
                            printData = startACommand;
                            if (i == 0) {
                                if (size >= 3) {
                                    pId = pId + paymentPdfDO.invoiceDos.get(i).invoiceNumber + " , " + paymentPdfDO.invoiceDos.get(i + 1).invoiceNumber + " , " + paymentPdfDO.invoiceDos.get(i + 2).invoiceNumber + " , ";

                                } else if (size == 2) {
                                    pId = pId + paymentPdfDO.invoiceDos.get(i).invoiceNumber + " , " + paymentPdfDO.invoiceDos.get(i + 1).invoiceNumber + "  ";

                                } else {
                                    pId = pId + paymentPdfDO.invoiceDos.get(i).invoiceNumber + "  ";

                                }
                                printData = printData +
                                        "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Being Paid Against|\n" +
                                        "@" + topSpace + ",200:PE203,HMULT1,VMULT1|:|\n" +
                                        "@" + topSpace + ",220:PE203,HMULT1,VMULT1|" + pId + "|\n";
//                                printData = printData +
//                                        "@" + topSpace + ",2:VLINE,Length40,Thick2|\n" +
//                                        "@" + topSpace + ",820:VLINE,Length40,Thick2|\n";

                            } else {
                                if (size >= 3) {
                                    pId = pId + paymentPdfDO.invoiceDos.get(i).invoiceNumber + " , " + paymentPdfDO.invoiceDos.get(i + 1).invoiceNumber + " , " + paymentPdfDO.invoiceDos.get(i + 2).invoiceNumber + " , ";
                                    printData = printData +
                                            "@" + topSpace + ",10:PE203,HMULT1,VMULT1||\n" +
                                            "@" + topSpace + ",200:PE203,HMULT1,VMULT1||\n" +
                                            "@" + topSpace + ",220:PE203,HMULT1,VMULT1|" + pId + "|\n";
//                                    printData = printData +
//                                            "@" + topSpace + ",2:VLINE,Length40,Thick2|\n" +
//                                            "@" + topSpace + ",820:VLINE,Length40,Thick2|\n";
                                } else if (size == 2) {
                                    pId = pId + paymentPdfDO.invoiceDos.get(i).invoiceNumber + " , " + paymentPdfDO.invoiceDos.get(i + 1).invoiceNumber + "  ";
                                    printData = printData +
                                            "@" + topSpace + ",10:PE203,HMULT1,VMULT1||\n" +
                                            "@" + topSpace + ",200:PE203,HMULT1,VMULT1||\n" +
                                            "@" + topSpace + ",220:PE203,HMULT1,VMULT1|" + pId + "|\n";
//                                    printData = printData +
//                                            "@" + topSpace + ",2:VLINE,Length40,Thick2|\n" +
//                                            "@" + topSpace + ",820:VLINE,Length40,Thick2|\n";
                                } else {
                                    pId = pId + paymentPdfDO.invoiceDos.get(i).invoiceNumber + "  ";
                                    printData = printData +
                                            "@" + topSpace + ",10:PE203,HMULT1,VMULT1||\n" +
                                            "@" + topSpace + ",200:PE203,HMULT1,VMULT1||\n" +
                                            "@" + topSpace + ",220:PE203,HMULT1,VMULT1|" + pId + "|\n";
//                                    printData = printData +
//                                            "@" + topSpace + ",2:VLINE,Length40,Thick2|\n" +
//                                            "@" + topSpace + ",820:VLINE,Length40,Thick2|\n";
                                }

                            }

                            printData = printData + endCommand;

                            lp.write(printData.getBytes());
                            Thread.sleep(10);
                        }

                    }
                } else {
                    if (paymentPdfDO.invoiceDos.size() > 0 && paymentPdfDO.invoiceDos.get(0).invoiceNumber.length() > 0) {
                        printData = startACommand +
                                "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Being Paid Against |" +
                                "@" + topSpace + ",200:PE203,HMULT1,VMULT1|:|\n" +
                                "@" + topSpace + ",220:PE203,HMULT1,VMULT1|" + paymentPdfDO.invoiceDos.get(0).invoiceNumber + "|";
//                        printData = printData +
//                                "@" + topSpace + ",2:VLINE,Length40,Thick2|" +
//                                "@" + topSpace + ",820:VLINE,Length40,Thick2|";
                        printData = printData + endCommand;
                        lp.write(printData.getBytes());
                        Thread.sleep(10);
                    }
                }
//                topSpace = topSpace + 10;
                printData = startACommand +
                        "@" + topSpace + ",1:HLINE,Length820,Thick2|\n";
                PodDo podDo = StorageManager.getInstance(context).getDepartureData(context);
                String note = "";
                if (podDo != null && !TextUtils.isEmpty(podDo.getNotes())) {
                    note = podDo.getNotes();
                }
                if (!note.isEmpty()) {


                    printData = printData +
//                            "@" + topSpace + ",1:VLINE,Length120,Thick2|\n" +
                            "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|Remarks :-  " + note + "|" +
//                            "@" + topSpace + ",820:VLINE,Length120,Thick2|\n" +
                            "\n";
//                    printData = printData + "@" + topSpace + ",1:HLINE,Length820,Thick2|\n";
                    topSpace = topSpace + 120;//tvs = 50+40 = 90,

                }

                printData = printData + endCommand;
                lp.write(printData.getBytes());
                Thread.sleep(100);
//                lp.newLine(2);

//                addQRCode(paymentPdfDO.paymentNumber);


            } catch (PrinterException e) {
                e.printStackTrace();
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            printDynamicFooter(paymentPdfDO, lp);
            printQRCode(context, lp, paymentPdfDO.paymentNumber);
//            Bitmap qrCode = Util.encodeAsBitmap(context, paymentPdfDO.paymentNumber);
//            ByteArrayOutputStream stream = new ByteArrayOutputStream();
//            Bitmap b = Bitmap.createScaledBitmap(qrCode, 120, 120, false);
//            b.compress(Bitmap.CompressFormat.JPEG, 100, stream);
//            byte[] byteArray = stream.toByteArray();
//            String encodedImage = Base64.encodeToString(byteArray, Base64.DEFAULT);
//
//            if (encodedImage != null) {
//
//                Thread.sleep(100);
//                lp.writeGraphicBase64(encodedImage, LinePrinter.GraphicRotationDegrees.DEGREE_0,
//                        280,  // Offset in printhead dots from the left of the page
//                        150, // Desired graphic width on paper in printhead dots
//                        150); // Desired graphic height on paper in printhead dots
//            }

            lp.newLine(7);
            sResult = "Number of bytes sent to printer: " + lp.getBytesWritten();
        } catch (BadPrinterStateException ex) {
            // Stop listening for printer events.
            lp.removePrintProgressListener(progressListener);
            sResult = "Printer error detected: " + ex.getMessage() + ". Please correct the error and try again.";
        } catch (LinePrinterException ex) {
            sResult = "LinePrinterException: " + ex.getMessage();
        } catch (Exception ex) {
            if (ex.getMessage() != null)
                sResult = "Unexpected exception: " + ex.getMessage();
            else
                sResult = "Unexpected exception.";
        } finally {
            ((BaseActivity) context).hideLoader();
            if (lp != null) {
                try {
                    lp.disconnect();  // Disconnects from the printer
                    lp.close();  // Releases resources
                } catch (Exception ex) {
                }
            }
        }
    }

    public class BadPrinterStateException extends Exception {
        static final long serialVersionUID = 1;

        private BadPrinterStateException(String message) {
            super(message);
        }
    }

    private String getAddress(ActiveDeliveryMainDO activeDeliveryMainDO) {
        String postal = activeDeliveryMainDO.customerStreet;
        String countryName = activeDeliveryMainDO.customerLandMark;
        String town = activeDeliveryMainDO.customerTown;
        String city = activeDeliveryMainDO.customerCity;
        String postalcode = activeDeliveryMainDO.customerPostalCode;

        String finalString = "";

        if (!TextUtils.isEmpty(postal)) {
            finalString += postal + ", ";
        }
        if (!TextUtils.isEmpty(city)) {
            finalString += city + ", ";
        }
        if (!TextUtils.isEmpty(town)) {
            finalString += town + ", ";
        }
        if (!TextUtils.isEmpty(countryName)) {
            finalString += countryName + ", ";
        }
        if (!TextUtils.isEmpty(postalcode)) {
            finalString += postalcode;
        }
        return finalString;
    }

    private String getAddress(CylinderIssueMainDO activeDeliveryMainDO) {


        String postal = activeDeliveryMainDO.customerStreet;
        String countryName = activeDeliveryMainDO.customerLandMark;
        String town = activeDeliveryMainDO.customerTown;
        String city = activeDeliveryMainDO.customerCity;
        String postalcode = activeDeliveryMainDO.customerPostalCode;

        String finalString = "";

        if (!TextUtils.isEmpty(postal)) {
            finalString += postal + ", ";
        }
        if (!TextUtils.isEmpty(city)) {
            finalString += city + ", ";
        }
        if (!TextUtils.isEmpty(town)) {
            finalString += town + ", ";
        }
        if (!TextUtils.isEmpty(countryName)) {
            finalString += countryName + ", ";
        }
        if (!TextUtils.isEmpty(postalcode)) {
            finalString += postalcode;
        }
        return finalString;
    }

    private String getAddress(PaymentPdfDO activeDeliveryMainDO) {


        String postal = activeDeliveryMainDO.customerStreet;
        String countryName = activeDeliveryMainDO.customerLandMark;
        String town = activeDeliveryMainDO.customerTown;
        String city = activeDeliveryMainDO.customerCity;
        String postalcode = activeDeliveryMainDO.customerPostalCode;

        String finalString = "";

        if (!TextUtils.isEmpty(postal)) {
            finalString += postal + ", ";
        }
        if (!TextUtils.isEmpty(city)) {
            finalString += city + ", ";
        }
        if (!TextUtils.isEmpty(town)) {
            finalString += town + ", ";
        }
        if (!TextUtils.isEmpty(countryName)) {
            finalString += countryName + ", ";
        }
        if (!TextUtils.isEmpty(postalcode)) {
            finalString += postalcode;
        }
        return finalString;
    }

    private String getAddress(CreateInvoicePaymentDO activeDeliveryMainDO) {


        String postal = activeDeliveryMainDO.customerStreet;
        String countryName = activeDeliveryMainDO.customerLandMark;
        String town = activeDeliveryMainDO.customerTown;
        String city = activeDeliveryMainDO.customerCity;
        String postalcode = activeDeliveryMainDO.customerPostalCode;

        String finalString = "";

        if (!TextUtils.isEmpty(postal)) {
            finalString += postal + ", ";
        }
        if (!TextUtils.isEmpty(city)) {
            finalString += city + ", ";
        }
        if (!TextUtils.isEmpty(town)) {
            finalString += town + ", ";
        }
        if (!TextUtils.isEmpty(countryName)) {
            finalString += countryName + ", ";
        }
        if (!TextUtils.isEmpty(postalcode)) {
            finalString += postalcode;
        }
        return finalString;
    }

    private String getRegisteredAddress(CreateInvoicePaymentDO invoiceDO) {

        String street, landMark, town, postal, city, countryName;
        street = invoiceDO.siteAddress1;
        landMark = invoiceDO.siteAddress2;
        town = invoiceDO.siteAddress3;
        city = invoiceDO.siteCity;
        postal = invoiceDO.sitePostalCode;
        countryName = invoiceDO.siteCountry;
        String finalString = "";

        if (!TextUtils.isEmpty(street)) {
            finalString += street + ", ";
        }
        if (!TextUtils.isEmpty(landMark)) {
            finalString += landMark + ", ";
        }
        if (!TextUtils.isEmpty(town)) {
            finalString += town + ", ";
        }
        if (!TextUtils.isEmpty(city)) {
            finalString += city + ", ";
        }
        if (!TextUtils.isEmpty(postal)) {
            finalString += postal + ", ";
        }
        return finalString + countryName;
    }

    private String getAmountInLetters(double amount, boolean fills) {
        if (fills) {
            return new NumberToWord().convert((int) amount) + " Fills";
        }
        return new NumberToWord().convert((int) amount);
        //return NumberToWords.getNumberToWords().getTextFromNumbers(amount);
    }

    private double getTotalAmount(CreateInvoicePaymentDO invoicePaymentDO) {
        double totalAmount = 0.0f;
        try {
            /*totalAmount = getTotalDiscount(pdfInvoiceDos) -
                    getTotalGrossAmount(pdfInvoiceDos) +
                    getTotalExlTax(pdfInvoiceDos) -
                    getTotalVat(pdfInvoiceDos);*/
            return Double.parseDouble(invoicePaymentDO.includingTax);

        } catch (Exception e) {
            return totalAmount;
        }

    }


    private void printDeliveryExecutivereportNotes(String sPrinterURI, LinePrinter.ExtraSettings exSettings,
                                                   PrintProgressListener progressListener, ActiveDeliveryMainDO activeDeliveryMainDO) {
        LinePrinter lp = null;
        try {

            lp = new LinePrinter(printConfigDo.jsonCmdAttribStr, printConfigDo.printerName, sPrinterURI, exSettings);
            lp.addPrintProgressListener(progressListener);
            int numtries = 0;
            int maxretry = 2;
            while (numtries < maxretry) {
                try {
                    lp.connect();  // Connects to the printer
                    break;
                } catch (LinePrinterException ex) {
                    numtries++;
                    Thread.sleep(1000);
                }
            }
            if (numtries == maxretry) lp.connect();//Final retry
            int[] results = lp.getStatus();
            if (results != null) {
                for (int err = 0; err < results.length; err++) {
                    if (results[err] == 223) {
                        throw new BadPrinterStateException("Paper out");
                    } else if (results[err] == 227) {
                        throw new BadPrinterStateException("Printer lid open");
                    }
                }
            }


            if (activeDeliveryMainDO.companyCode.equalsIgnoreCase("U2") || activeDeliveryMainDO.companyCode.equalsIgnoreCase("U3")) {
                lp.writeGraphicBase64(PrinterConstants.base64Footer, LinePrinter.GraphicRotationDegrees.DEGREE_0, 300, 250, 250);

            } else {
                lp.writeGraphicBase64(PrinterConstants.base64LogoPng, LinePrinter.GraphicRotationDegrees.DEGREE_0, 300, 250, 250);

            }
            Thread.sleep(100);
            lp.newLine(1);
            byte[] easymode = new byte[]{0x1b, 0x45, 0x5a}; // [ESC]EZ
            lp.write(easymode);
            int topSpace = 0;
            String printData = "";
            String startCommand = "\u001BEZ{PRINT:\n";
            String endCommand = "}{LP}\n";

            int leftSpace = 0;//225
            if (activeDeliveryMainDO.companyDescription.toLowerCase().contains("taqat")) {
                leftSpace = 320;
            } else {
                leftSpace = 250;
            }
            String shipmentProductsType = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "");

            printData = startCommand + "@" + topSpace + "," + leftSpace + ":PE203,HMULT1,VMULT2|" + activeDeliveryMainDO.companyDescription + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand + "@" + topSpace + ",350:PE203,HMULT1,VMULT2|Delivery Note|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            String dMonth = activeDeliveryMainDO.createdDate.substring(4, 6);
            String dyear = activeDeliveryMainDO.createdDate.substring(0, 4);
            String dDate = activeDeliveryMainDO.createdDate.substring(Math.max(activeDeliveryMainDO.createdDate.length() - 2, 0));
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|D.N. No|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.shipmentNumber + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Date|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + dDate + "-" + dMonth + "-" + dyear + " (" + activeDeliveryMainDO.createdTime + ")" + "|" + endCommand;
//            "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + CalendarUtils.getPDFDate() + "|" + endCommand;

            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            activeDeliveryMainDO.customerDescription = activeDeliveryMainDO.customerDescription.substring(0, Math.min(activeDeliveryMainDO.customerDescription.length(), 30));
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Delivered To|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.customerDescription + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Customer Code|" +
                    "@" + topSpace + ",640:PE203,HMULT1,VMULT1|:|" +
//                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + CalendarUtils.getPDFTime() + "|" + endCommand;
                    "@" + topSpace + ",660:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.customer + "|" + endCommand;

            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|User ID|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.createUserID + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|User Name|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.createUserName + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            PreferenceUtils preferenceUtils = new PreferenceUtils(context);
            String vehicleNumber = preferenceUtils.getStringFromPreference(PreferenceUtils.CV_PLATE, "");
            String vehicleCode = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_CODE, "");

            topSpace = 0;
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Vehicle Code|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + vehicleCode + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Plate No.|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + vehicleNumber + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Payment Term|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.paymentTerm + "|" +
//                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Customer code|" +
//                    "@" + topSpace + ",610:PE203,HMULT1,VMULT1|:|" +
//                    "@" + topSpace + ",640:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.customer + "|"  +
                    endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Address|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + getAddress(activeDeliveryMainDO) + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);

            topSpace = 0;
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Reciever Name|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.capturedName + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Reciever No.|" +
                    "@" + topSpace + ",610:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",640:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.capturedNumber + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);

            topSpace = 0;
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Order No.|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.salesOrderNumber + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|LPO No.|" +
                    "@" + topSpace + ",610:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",640:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.LPONumber + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            if (!activeDeliveryMainDO.signatureDate.isEmpty()) {
                String dSMonth = activeDeliveryMainDO.signatureDate.substring(4, 6);
                String dSyear = activeDeliveryMainDO.signatureDate.substring(0, 4);
                String dSDate = activeDeliveryMainDO.signatureDate.substring(Math.max(activeDeliveryMainDO.signatureDate.length() - 2, 0));

                topSpace = 0;
                printData = startCommand +
                        "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Signed Date|" +
                        "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                        "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + dSDate + "-" + dSMonth + "-" + dSyear + "|" +
                        "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Signed Time|" +
                        "@" + topSpace + ",610:PE203,HMULT1,VMULT1|:|" +
                        "@" + topSpace + ",640:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.signatureTime + "|" + endCommand;
                lp.write(printData.getBytes());
                Thread.sleep(100);
            }

            if (shipmentProductsType.equals(AppConstants.MeterReadingProduct)) {
                topSpace = 0;
                printData = startCommand +
                        "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Starting Tank Reading|" +
                        "@" + topSpace + ",216:PE203,HMULT1,VMULT1|:|" +
                        "@" + topSpace + ",230:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.startReading + "|" +
                        "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Closing Tank Reading|" +
                        "@" + topSpace + ",710:PE203,HMULT1,VMULT1|:|" +
                        "@" + topSpace + ",725:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.endReading + "|" + endCommand;
                lp.write(printData.getBytes());
                Thread.sleep(100);
            }
            topSpace = 0;
            if (shipmentProductsType.equals(AppConstants.MeterReadingProduct)) {
                String type="";
                if(activeDeliveryMainDO.meterType==1){
                    type="Ltr";
                }else {
                    type="Gallon";
                }

                topSpace = 30;
                printData = startCommand +
                        "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|Sr.No.|" +
                        "@" + (topSpace + 5) + ",110:PE203,HMULT1,VMULT1|Item|" +
                        "@" + (topSpace + 5) + ",360:PE203,HMULT1,VMULT1|Opening Reading|"+"("+type+")" +
                        "@" + (topSpace + 5) + ",540:PE203,HMULT1,VMULT1|Ending Reading|" +"("+type+")"+
                        "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT1|Quantity|" + endCommand;
                lp.write(printData.getBytes());
                Thread.sleep(100);
                topSpace = 0;
                for (int i = 0; i < activeDeliveryMainDO.activeDeliveryDOS.size(); i++) {
                    ActiveDeliveryDO activeDeliveryDO = activeDeliveryMainDO.activeDeliveryDOS.get(i);
                    activeDeliveryDO.productDescription = activeDeliveryDO.productDescription.substring(0, Math.min(activeDeliveryDO.productDescription.length(), 25));
                    printData = startCommand +
                            "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|" + (i + 1) + "|" +
                            "@" + (topSpace + 5) + ",110:PE203,HMULT1,VMULT1|" + activeDeliveryDO.productDescription + "|" +
                            "@" + (topSpace + 5) + ",360:PE203,HMULT1,VMULT1|" + activeDeliveryDO.openingQuantityLong + "|" +
                            "@" + (topSpace + 5) + ",540:PE203,HMULT1,VMULT1|" + activeDeliveryDO.endingQuantityLong + "|" +
                            "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT1|" + activeDeliveryDO.orderedQuantity + " " + activeDeliveryDO.unit + "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(100);
                    topSpace = 0;
                }
                if (activeDeliveryMainDO.referenceID != null) {
//                    note = podDo.getNotes();
                    topSpace = topSpace + 40;

                    printData = startCommand + "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|" + "Delivery Reference :  " + activeDeliveryMainDO.referenceID + "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(100);

                }
//                printData = startCommand + "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|Remarks :- |" + endCommand;
//                PodDo podDo = StorageManager.getInstance(context).getDepartureData(context);
//                String note = "";
                if (activeDeliveryMainDO.remarks != null) {
//                    note = podDo.getNotes();
                    topSpace = topSpace + 40;
                    String remarks1 = "";
                    String remarks2 = "";
                    if (activeDeliveryMainDO.remarks.length() > 70) {
                        remarks1 = activeDeliveryMainDO.remarks.substring(0, 70);
                        remarks2 = activeDeliveryMainDO.remarks.substring(70, activeDeliveryMainDO.remarks.length());
                    } else {
                        remarks1 = activeDeliveryMainDO.remarks;

                    }
                    printData = startCommand + "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|" + "Remarks :  " + remarks1 + "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(100);
                    if (remarks2.length() > 0) {
                        printData = startCommand + "@" + (5) + ",12:PE203,HMULT1,VMULT1|" + "" + remarks2 + "|" + endCommand;
                        lp.write(printData.getBytes());
                        Thread.sleep(100);
                    }
                }

            } else {
                topSpace = 30;
                printData = startCommand +
                        "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|Sr.No.|" +
                        "@" + (topSpace + 5) + ",110:PE203,HMULT1,VMULT1|Item|" +
                        "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT1|Quantity|" + endCommand;
                lp.write(printData.getBytes());
                Thread.sleep(100);
                topSpace = 0;
                for (int i = 0; i < activeDeliveryMainDO.activeDeliveryDOS.size(); i++) {
                    ActiveDeliveryDO activeDeliveryDO = activeDeliveryMainDO.activeDeliveryDOS.get(i);
                    activeDeliveryDO.productDescription = activeDeliveryDO.productDescription.substring(0, Math.min(activeDeliveryDO.productDescription.length(), 50));
                    printData = startCommand +
                            "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|" + (i + 1) + "|" +
                            "@" + (topSpace + 5) + ",110:PE203,HMULT1,VMULT1|" + activeDeliveryDO.product + " - " + activeDeliveryDO.productDescription + "|" +
                            "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT1|" + activeDeliveryDO.orderedQuantity + " " + activeDeliveryDO.unit + "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(100);
                    topSpace = 0;
                }

//                printData = startCommand + "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|Remarks :- |" + endCommand;
//                PodDo podDo = StorageManager.getInstance(context).getDepartureData(context);
//                String note = "";
//                if (podDo != null && !TextUtils.isEmpty(podDo.getNotes())) {
//                    note = podDo.getNotes();
//                    printData = startCommand + "@" + (topSpace + 5) + ",180:PE203,HMULT1,VMULT1|Remarks :- |";
//                    topSpace = topSpace + 40;
//                    printData = printData + "@" + (topSpace + 5) + ",180:PE203,HMULT1,VMULT1|" + note + "|" + endCommand;
//                }
//                lp.write(printData.getBytes());
//                Thread.sleep(100);
                topSpace = topSpace + 10;

                if (activeDeliveryMainDO.referenceID != null) {
//                    note = podDo.getNotes();

                    printData = startCommand + "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|" + "Delivery Reference :  " + activeDeliveryMainDO.referenceID + "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(100);

                }
                if (activeDeliveryMainDO.remarks != null) {
                    topSpace = 0;
//                    note = podDo.getNotes();
                    String remarks1 = "";
                    String remarks2 = "";
                    if (activeDeliveryMainDO.remarks.length() > 70) {
                        remarks1 = activeDeliveryMainDO.remarks.substring(0, 70);
                        remarks2 = activeDeliveryMainDO.remarks.substring(70, activeDeliveryMainDO.remarks.length());
                    } else {
                        remarks1 = activeDeliveryMainDO.remarks;

                    }
                    printData = startCommand + "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|" + "Remarks :  " + remarks1 + "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(100);
                    if (remarks2.length() > 0) {
                        printData = startCommand + "@" + (5) + ",12:PE203,HMULT1,VMULT1|" + "" + remarks2 + "|" + endCommand;
                        lp.write(printData.getBytes());
                        Thread.sleep(100);
                    }
                }
            }

            printSignature(activeDeliveryMainDO.signature, lp, progressListener);
            printDynamicFooter(activeDeliveryMainDO, lp);
            lp.newLine(7);

            sResult = "Number of bytes sent to printer: " + lp.getBytesWritten();

        } catch (BadPrinterStateException ex) {
            // Stop listening for printer events.
            lp.removePrintProgressListener(progressListener);
            sResult = "Printer error detected: " + ex.getMessage() + ". Please correct the error and try again.";
        } catch (LinePrinterException ex) {
            sResult = "LinePrinterException: " + ex.getMessage();
        } catch (Exception ex) {
            if (ex.getMessage() != null)
                sResult = "Unexpected exception: " + ex.getMessage();
            else
                sResult = "Unexpected exception.";
        } finally {
            ((BaseActivity) context).hideLoader();
            if (lp != null) {
                try {
                    lp.disconnect();  // Disconnects from the printer
                    lp.close();  // Releases resources
                    ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.DELIVERY_NOTE, "SUCCESS");

                } catch (Exception ex) {
                }
            }
        }
    }
}
