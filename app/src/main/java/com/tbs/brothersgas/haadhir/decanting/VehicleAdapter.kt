package com.tbs.brothersgas.haadhir.decanting

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context
import androidx.core.content.res.ResourcesCompat
import androidx.appcompat.app.AppCompatDialog
import androidx.recyclerview.widget.RecyclerView
import android.text.Editable
import android.text.InputFilter
import android.text.Spanned
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*

import com.tbs.brothersgas.haadhir.Activitys.BaseActivity
import com.tbs.brothersgas.haadhir.Activitys.ScheduledCaptureDeliveryActivity
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryDO
import com.tbs.brothersgas.haadhir.Model.ReasonDO
import com.tbs.brothersgas.haadhir.Model.ReasonMainDO
import com.tbs.brothersgas.haadhir.Model.VehicleDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.ActiveDeliveryRequest
import com.tbs.brothersgas.haadhir.common.AppConstants
import com.tbs.brothersgas.haadhir.listeners.LoadStockListener
import com.tbs.brothersgas.haadhir.listeners.ResultListner
import com.tbs.brothersgas.haadhir.pdfs.BulkDeliveryNotePDF
import com.tbs.brothersgas.haadhir.pdfs.CYLDeliveryNotePDF
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils

import java.util.ArrayList

class VehicleAdapter(private val context: Context, var activeDeliveryDOS: ArrayList<VehicleDO>?) : androidx.recyclerview.widget.RecyclerView.Adapter<VehicleAdapter.MyViewHolder>() {

    internal var mass = 0.0
    internal var type = 0
    var fromDeliveryExecutive: Boolean = false
    var resultListner: ResultListner? = null

    fun refreshAdapter(activeDeliveryDOS: ArrayList<VehicleDO>, Type: Int) {
        this.activeDeliveryDOS = activeDeliveryDOS
        this.type = Type
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.dec_data, parent, false)
        return MyViewHolder(itemView)
    }
    fun deliveryExexutive(fromDeliveryExecutive: Boolean, resultListner: ResultListner) {
        this.fromDeliveryExecutive = fromDeliveryExecutive
        this.resultListner = resultListner

    }
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val activeDeliveryDO = activeDeliveryDOS!![position]


        holder.tvVehicleCode.text = "" + activeDeliveryDO.vehicle
        holder.tvPlate.text = "" + activeDeliveryDO.plate




        holder.itemView.setOnClickListener {
            resultListner?.onResultListner(activeDeliveryDO, true)
        }


    }

    override fun getItemCount(): Int {
        return if (activeDeliveryDOS != null) activeDeliveryDOS!!.size else 0
    }

    inner class MyViewHolder(view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
        var tvVehicleCode: TextView
        var tvPlate: TextView


        init {
            tvVehicleCode = view.findViewById<View>(R.id.tvVehicleCode) as TextView
            tvPlate = view.findViewById<View>(R.id.tvPlate) as TextView


        }
    }


}
