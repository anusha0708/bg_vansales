package com.tbs.brothersgas.haadhir.utils;

import android.app.Application;
import androidx.multidex.MultiDexApplication;

/*
 * Created by developer on 7/3/19.
 */
public class BaseApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        AppPrefs.initPrefs(this);

    }
}
