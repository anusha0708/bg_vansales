package com.tbs.brothersgas.haadhir.Activitys

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.View
import android.view.View.*
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import com.tbs.brothersgas.haadhir.Adapters.DamageReasonListAdapter
import com.tbs.brothersgas.haadhir.Adapters.DamageScheduledProductsAdapter
import com.tbs.brothersgas.haadhir.Adapters.ReasonListAdapter
import com.tbs.brothersgas.haadhir.Adapters.ScheduledProductsAdapter
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryDO
import com.tbs.brothersgas.haadhir.Model.ReasonMainDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.*
import com.tbs.brothersgas.haadhir.common.AppConstants
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.utils.CalendarUtils
import com.tbs.brothersgas.haadhir.utils.LogUtils
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import java.util.*


//
class DamageCylindersActivity : BaseActivity() {


    private var loadStockDOs: ArrayList<ActiveDeliveryDO>? = ArrayList()
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var btnRequestApproval: Button
    lateinit var btnConfirm: Button
    lateinit var tvNoDataFound: TextView
    lateinit var tvApprovalStatus: TextView
    private var shipmentType: String = ""
    var status = "";
    var customerId = "";
    var isProductExisted = false;

    var damageId = 0
    lateinit var dialog: Dialog
    private var shipmentId: String = ""
    private var loadStockAdapter: DamageScheduledProductsAdapter? = null
    private var reasonMainDO: ReasonMainDO = ReasonMainDO()

    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.scheduled_capture_delivery, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        disableMenuWithBackButton()
        toolbar.setNavigationIcon(R.drawable.back)
        hideKeyBoard(llBody)
        toolbar.setNavigationOnClickListener {

            if (loadStockDOs != null && !loadStockDOs!!.isEmpty() && loadStockDOs!!.size > 0) {
                if (isProductExisted) {
                    StorageManager.getInstance(this@DamageCylindersActivity).deleteDamageDeliveryItems(this@DamageCylindersActivity);
                    var intent = Intent()
                    var args = Bundle();
                    intent.putExtra("BUNDLE", args);
                    setResult(56, intent)
                    finish()
                } else {
                    showToast("Please confirm")

                }
            } else {
                StorageManager.getInstance(this@DamageCylindersActivity).deleteDamageDeliveryItems(this@DamageCylindersActivity);
                var intent = Intent()
                var args = Bundle();
                intent.putExtra("BUNDLE", args);
                setResult(56, intent)
                finish()
            }


        }

        initializeControls()
        shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
        shipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, "")
        if (intent.hasExtra("DAMAGE")) {
            damageId = intent.extras.getInt("DAMAGE")
        }
        if (intent.hasExtra("CustomerId")) {
            customerId = intent.extras.getString("CustomerId")
        }
        ivAdd.setVisibility(View.VISIBLE)
        tvScreenTitle.setText("Damage Cylinders")


        btnConfirm.setText("Confirm")
        btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
        btnConfirm.isClickable = false
        btnConfirm.isEnabled = false


        ivAdd.setOnClickListener {
            val intent = Intent(this, DamageAddProductActivity::class.java);
            // need condition
            intent.putExtra("FROM", "SPOTSALES")
            startActivityForResult(intent, 1);
        }

        btnConfirm.setOnClickListener {

            if (loadStockDOs != null && !loadStockDOs!!.isEmpty() && loadStockDOs!!.size > 0) {
                var isProductExisted = false;

                for (k in loadStockDOs!!.indices) {
                    if (loadStockDOs!!.get(k).reasonId == 0) {
                        isProductExisted = true
                        break
                    }
                }

                if (isProductExisted) {
                    showToast("Please select reason")

                } else {
                    updateDamageCylinders()

                }


            } else {
                showToast("No  Items found")
            }
        }


    }


    private fun updateDamageCylinders() {
        if (Util.isNetworkAvailable(this)) {

            val request = UpdateDamageCylindersRequest(shipmentId, loadStockDOs, this@DamageCylindersActivity)
            request.setOnResultListener { isError, approveDO ->
                hideLoader()

                if (approveDO != null) {
                    if (isError) {
                        showAppCompatAlert("", " please try again.", "Ok", "Cancel", "", false)
                        isProductExisted = true

                    } else {
                        if (approveDO.flag == 1) {
                            showToast("Updated Successfully")
                            setResult(98, intent)
                            preferenceUtils.saveString(PreferenceUtils.DAMAGE, "DAMAGE")

                            finish()
                        } else {
                            isProductExisted = true
                            showAppCompatAlert("", " please try again.", "Ok", "Cancel", "", false)

                        }
                    }
                }
            }
            request.execute()
        } else {
            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "", false)

        }

    }

    lateinit var loadStockDO: ActiveDeliveryDO;

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == 1) {
            tvApprovalStatus.setVisibility(View.GONE)
//            loadStockDOs = data!!.getSerializableExtra("AddedProducts") as ArrayList<ActiveDeliveryDO>
            loadStockDOs = StorageManager.getInstance(this@DamageCylindersActivity).getDamageDeliveryItems(this@DamageCylindersActivity);
            loadStockAdapter = DamageScheduledProductsAdapter(this, loadStockDOs, "", "Damage");
            recycleview.adapter = loadStockAdapter;
            if (loadStockDOs!!.size > 0) {
                btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
                btnConfirm.isClickable = true
                btnConfirm.isEnabled = true
                recycleview.visibility = VISIBLE
                tvNoDataFound.visibility = GONE
            }
//
//                    var isProductExisted = false;
//                    for (i in loadStockDOs!!.indices) {
//                        for (k in shipmentProductsList!!.indices) {
//                            if ( loadStockDOs!!.get(i).product.equals(shipmentProductsList!!.get(k).product, true) ) {
//                                shipmentProductsList!!.get(k).orderedQuantity = loadStockDOs!!.get(i).orderedQuantity
//                                isProductExisted = true
//                                shipmentProductsList!!.add(loadStockDOs!!.get(i))
//
//                                break
//                            }
//                        }
//
//                        if (isProductExisted) {
//                            isProductExisted = false
//                            continue
//                        } else {
//                            shipmentProductsList!!.add(loadStockDOs!!.get(i))
//                        }
//                    }
//                        loadStockAdapter = DamageScheduledProductsAdapter(this, shipmentProductsList, "", "Damage");
//                        recycleview.adapter = loadStockAdapter;
//
//            }
        }
    }


    override fun initializeControls() {
        recycleview = findViewById<View>(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        btnRequestApproval = findViewById<View>(R.id.btnRequestApproval) as Button
        btnConfirm = findViewById<View>(R.id.btnConfirm) as Button
        tvNoDataFound = findViewById<View>(R.id.tvNoDataFound) as TextView
        tvApprovalStatus = findViewById(R.id.tvApprovalStatus) as TextView
        tvApprovalStatus.setVisibility(GONE)
        btnRequestApproval.visibility = GONE

        recycleview.setLayoutManager(androidx.recyclerview.widget.LinearLayoutManager(this, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false))

    }

    override fun onBackPressed() {

        if (loadStockDOs != null && !loadStockDOs!!.isEmpty() && loadStockDOs!!.size > 0) {
            if (isProductExisted) {
                StorageManager.getInstance(this@DamageCylindersActivity).deleteDamageDeliveryItems(this@DamageCylindersActivity);
                var intent = Intent()
                var args = Bundle();
                intent.putExtra("BUNDLE", args);
                setResult(56, intent)
                finish()
            } else {
                showToast("Please confirm")

            }
        } else {
            StorageManager.getInstance(this@DamageCylindersActivity).deleteDamageDeliveryItems(this@DamageCylindersActivity);
            var intent = Intent()
            var args = Bundle();
            intent.putExtra("BUNDLE", args);
            setResult(56, intent)
            finish()
        }


    }

    override fun onDestroy() {
        StorageManager.getInstance(this@DamageCylindersActivity).deleteDamageDeliveryItems(this@DamageCylindersActivity);

        super.onDestroy()

    }

}