package com.tbs.brothersgas.haadhir.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.tbs.brothersgas.haadhir.Model.LoadStockDO;
import com.tbs.brothersgas.haadhir.Model.SpotDeliveryDO;
import com.tbs.brothersgas.haadhir.R;

import java.util.ArrayList;

public class UserActivityLoanAdapter extends RecyclerView.Adapter<UserActivityLoanAdapter.MyViewHolder> {

    private ArrayList<LoadStockDO> spotDeliveryDOS;
    private Context context;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvRecieptNumber, tvCustomerName;
        private TextView tvAmount;

        public MyViewHolder(View view) {
            super(view);
            tvAmount = (TextView) view.findViewById(R.id.tvAmount);
            tvRecieptNumber = (TextView) view.findViewById(R.id.tvRecieptNumber);
            tvCustomerName = (TextView) view.findViewById(R.id.tvCustomerName);


        }
    }


    public UserActivityLoanAdapter(Context context, ArrayList<LoadStockDO> siteDOS) {
        this.context = context;
        this.spotDeliveryDOS = siteDOS;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.transaction_data_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final LoadStockDO spotDeliveryDO = spotDeliveryDOS.get(position);

        String amount = String.valueOf(spotDeliveryDO.quantity);

        holder.tvRecieptNumber.setText(spotDeliveryDO.shipmentNumber);
        holder.tvCustomerName.setText(spotDeliveryDO.productDescription);
        holder.tvAmount.setText("" + amount);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });


    }

    @Override
    public int getItemCount() {
        return spotDeliveryDOS.size();
    }

}
