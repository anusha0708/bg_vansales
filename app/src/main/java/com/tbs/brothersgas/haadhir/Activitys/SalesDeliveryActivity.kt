package com.tbs.brothersgas.haadhir.Activitys

import android.content.Intent
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.tbs.brothersgas.haadhir.Adapters.DeliveryAdapter
import com.tbs.brothersgas.haadhir.Model.LoadStockDO
import com.tbs.brothersgas.haadhir.R
import java.util.ArrayList


class SalesDeliveryActivity : BaseActivity() {
    lateinit var invoiceAdapter: DeliveryAdapter
    lateinit var loadStockDOs: ArrayList<LoadStockDO>
    lateinit var recycleview : androidx.recyclerview.widget.RecyclerView
    override  protected fun onResume() {
        super.onResume()
    }
    override fun initialize() {
      var  llCategories = getLayoutInflater().inflate(R.layout.sales_delivery, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
    }
    override fun initializeControls() {
        tvScreenTitle.setText(R.string.sales_delivery)
        recycleview = findViewById(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)

        recycleview.setLayoutManager(linearLayoutManager)
        invoiceAdapter = DeliveryAdapter(this@SalesDeliveryActivity,  null)
        recycleview.setAdapter(invoiceAdapter)
        val btnCreate = findViewById(R.id.btnCreate) as Button

        btnCreate.setOnClickListener {
            val intent = Intent(this@SalesDeliveryActivity, CreateDeliveryActivity::class.java);
            startActivity(intent);
        }
    }

}