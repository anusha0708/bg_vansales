package com.tbs.brothersgas.haadhir.Model;

import java.io.Serializable;

public class CreatePaymentDO implements Serializable {


    public String paymentNumber = "";
    public String site = "";
    public String message = "";

    public String customer = "";
    public String control = "";
    public String account = "";
    public String accountingDate = "";
    public String bank = "";
    public String currency = "";
    public String amount = "";
    public String checkNumber = "";
    public String address = "";
    public String bankAmount = "";
    public String invoiceNumber = "";
    public int doc = 0;
    public int email = 0;
    public int print = 0;


}
