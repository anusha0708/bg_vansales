package com.tbs.brothersgas.haadhir.Requests;

/**
 * Created by VenuAppasani on 06-01-2019
 */

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.tbs.brothersgas.haadhir.Activitys.BaseActivity;
import com.tbs.brothersgas.haadhir.Activitys.SignatureActivity;
import com.tbs.brothersgas.haadhir.Model.CreateInvoicePaymentDO;
import com.tbs.brothersgas.haadhir.Model.PdfInvoiceDo;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;
import com.tbs.brothersgas.haadhir.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class InvoiceDetailsRequest extends AsyncTask<String, Void, Boolean> {
    private PdfInvoiceDo pdfInvoiceDo;

    private CreateInvoicePaymentDO createInvoiceDO;
    private Context mContext;
    private String id;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;

    public InvoiceDetailsRequest(String id, Context mContext) {
        this.mContext = mContext;
        this.id = id;
    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        public void onCompleted(boolean isError, CreateInvoicePaymentDO createInvoiceDO);

    }

    public boolean runRequest() {
        String NAMESPACE = "http://www.adonix.com/WSS";
        String METHOD_NAME = "run";
        String SOAP_ACTION = "CAdxWebServiceXmlCC";
        preferenceUtils = new PreferenceUtils(mContext);

        username = preferenceUtils.getStringFromPreference(PreferenceUtils.A_USER_NAME, "");
        password = preferenceUtils.getStringFromPreference(PreferenceUtils.A_PASSWORD, "");
        ip = preferenceUtils.getStringFromPreference(PreferenceUtils.IP_ADDRESS, "");
        port = preferenceUtils.getStringFromPreference(PreferenceUtils.PORT, "");
        pool = preferenceUtils.getStringFromPreference(PreferenceUtils.ALIAS, "");
        String URL = "http://" + ip + ":" + port + "/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC";

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        request.addProperty("publicName", WebServiceConstants.INVOICE_DETAILS);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("I_XSIHNUM", id);

//            jsonObject.put("I_XSIHNUM", "CNV-U102-20000084");
        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        request.addProperty("inputXml", jsonObject.toString());

        SoapObject callcontext = new SoapObject("", "callContext");
        // Set all input params
        callcontext.addProperty("codeLang", "ENG");
        callcontext.addProperty("poolAlias", pool);
        callcontext.addProperty("poolId", "");
        callcontext.addProperty("codeUser", username);
        callcontext.addProperty("password", password);
        callcontext.addProperty("requestConfig", "adxwss.trace.on=off");

        request.addSoapObject(callcontext);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.setOutputSoapObject(request);

        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
        androidHttpTransport.debug = true;

        try {
            List<HeaderProperty> headerList = new ArrayList<HeaderProperty>();
            headerList.add(new HeaderProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode((username + ":" + password).getBytes())));
            androidHttpTransport.call(SOAP_ACTION, envelope, headerList);
            SoapObject response = (SoapObject) envelope.getResponse();
            String resultXML = (String) response.getProperty("resultXml");
            if (resultXML != null && resultXML.length() > 0) {
                return parseXML(resultXML);
            } else {
                return false;
            }
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();

            createInvoiceDO = new CreateInvoicePaymentDO();
            pdfInvoiceDo = new PdfInvoiceDo();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        createInvoiceDO.pdfInvoiceDos = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        pdfInvoiceDo = new PdfInvoiceDo();

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null &&
                            startTag.equalsIgnoreCase("FLD") && text.length() > 0) {


                        if (attribute.equalsIgnoreCase("O_YDLVDAT")) {


                            if (text.length() > 0) {

                                createInvoiceDO.deliveryCreatedDate = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XBPCNUM")) {


                            if (text.length() > 0) {

                                createInvoiceDO.customer = text;
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_XCYLEXPDATE")) {


                            if (text.length() > 0) {

                                createInvoiceDO.expiryDate = text;
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_XGASHOSEFLG")) {


                            if (text.length() > 0) {

                                createInvoiceDO.qsn2 = Integer.parseInt(text);
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_XGASREGFLG")) {


                            if (text.length() > 0) {

                                createInvoiceDO.qsn3 = Integer.parseInt(text);
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_XCLIPHOSFLG")) {


                            if (text.length() > 0) {

                                createInvoiceDO.qsn4 = Integer.parseInt(text);
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_XGASLEAKFLG")) {


                            if (text.length() > 0) {

                                createInvoiceDO.qsn5 = Integer.parseInt(text);
                            }
                        }


                        else if (attribute.equalsIgnoreCase("O_XCREDAT")) {


                            if (text.length() > 0) {

                                createInvoiceDO.createdDate = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XCRETIM")) {
                            if (text.length() > 0) {

                                createInvoiceDO.createdTime = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_XSDHNUM")) {
                            if (text.length() > 0) {

                                createInvoiceDO.shipmentID = text;
                            }

                        }else if (attribute.equalsIgnoreCase("O_XCREUSR")) {
                            if (text.length() > 0) {
                                createInvoiceDO.createUserID = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XCREUSRNAM")) {
                            if (text.length() > 0) {
                                createInvoiceDO.createUserName = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XBPCSHO")) {


                            if (text.length() > 0) {

                                createInvoiceDO.customerDescription = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XCPYCRY")) {


                            if (text.length() > 0) {

                                createInvoiceDO.countryname = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YWEB")) {


                            if (text.length() > 0) {

                                createInvoiceDO.website = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XSIGNATURE")) {


                            if (text.length() > 0) {

                                createInvoiceDO.signature = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITADDLIG0")) {


                            if (text.length() > 0) {

                                createInvoiceDO.siteAddress1 = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITADDLIG0")) {


                            if (text.length() > 0) {

                                createInvoiceDO.siteAddress2 = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITADDLIG0")) {


                            if (text.length() > 0) {

                                createInvoiceDO.siteAddress3 = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITCTY")) {


                            if (text.length() > 0) {

                                createInvoiceDO.siteCity = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YWEB")) {
                            if (text.length() > 0) {

                                createInvoiceDO.website = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XBPADDLIG0")) {


                            if (text.length() > 0) {

                                createInvoiceDO.customerStreet = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XBPADDLIG1")) {


                            if (text.length() > 0) {

                                createInvoiceDO.customerLandMark = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XBPADDLIG2")) {

                            if (text.length() > 0) {

                                createInvoiceDO.customerTown = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_XREMARKS")) {
                            if (!text.isEmpty()) {

                                createInvoiceDO.remarks = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_XSDHREMARKS")) {
                            if (!text.isEmpty()) {

                                createInvoiceDO.deliveryRemarks = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_XPAYTERM")) {
                            if (!text.isEmpty()) {

                                createInvoiceDO.paymentTerm = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_XCUSCTY")) {

                            if (text.length() > 0) {

                                createInvoiceDO.customerCity = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_XCUSPOS")) {


                            if (text.length() > 0) {

                                createInvoiceDO.customerPostalCode = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XCUSLD")) {


                            if (text.length() > 0) {

                                createInvoiceDO.landline = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YLAT")) {

                            if (text.length() > 0) {

                                createInvoiceDO.latitude = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_LON")) {


                            if (text.length() > 0) {

                                createInvoiceDO.longitude = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YSALFCY")) {


                            if (text.length() > 0) {

                                createInvoiceDO.salesSite = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XITAX")) {


                            if (text.length() > 0) {

                                pdfInvoiceDo.includingTax = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XETAX")) {

                            if (text.length() > 0) {

                                pdfInvoiceDo.excludingTax = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YARDAT")) {


                            if (text.length() > 0) {

                                createInvoiceDO.arrivalDate = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YARDAT")) {


                            if (text.length() > 0) {

                                createInvoiceDO.estimatedDate = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XBPADDLIG0")) {


                            if (text.length() > 0) {

                                createInvoiceDO.customerStreet = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XBPADDLIG1")) {
                            if (text.length() > 0) {

                                createInvoiceDO.customerLandMark = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XBPADDLIG2")) {


                            if (text.length() > 0) {

                                createInvoiceDO.customerTown = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YARTIM")) {


                            if (text.length() > 0) {

                                createInvoiceDO.estimatedTime = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XITM")) {


                            if (text.length() > 0) {

                                pdfInvoiceDo.product = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XITMDES")) {


                            if (text.length() > 0) {

                                pdfInvoiceDo.productDesccription = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XDQTY")) {


                            if (text.length() > 0) {

                                pdfInvoiceDo.deliveredQunatity = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XGRPRI")) {


                            if (text.length() > 0) {

                                pdfInvoiceDo.grossPrice = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XNETPRI")) {

                            if (text.length() > 0) {

                                pdfInvoiceDo.netPrice = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_XNETPRI")) {


                            if (text.length() > 0) {

                                pdfInvoiceDo.costPrice = Double.valueOf(text);
                            }
                        } else if (attribute.equalsIgnoreCase("O_XNETPRI")) {


                            if (text.length() > 0) {

                                pdfInvoiceDo.vatPercentage = Double.valueOf(text);
                            }
                        } else if (attribute.equalsIgnoreCase("O_YMRG")) {


                        } else if (attribute.equalsIgnoreCase("O_XAMT")) {


                            if (text.length() > 0) {

                                pdfInvoiceDo.amount = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XDISCOUNT")) {


                            if (text.length() > 0) {

                                pdfInvoiceDo.discount = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XVATPERCENT")) {


                            if (text.length() > 0) {

                                pdfInvoiceDo.vatPercentage = Double.valueOf(text);
                            }
                        } else if (attribute.equalsIgnoreCase("O_YMRG")) {

                            if (text.length() > 0) {

                                createInvoiceDO.margin = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_XSIHNUM")) {

                            if (text.length() > 0) {

                                createInvoiceDO.invoiceNumber = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_XCUSINVEM")) {

                            if (text.length() > 0) {

                                createInvoiceDO.invoiceEmail = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_XCPYNAM")) {
                            createInvoiceDO.companyCode = text;

                        } else if (attribute.equalsIgnoreCase("O_XCPYDES")) {

                            if (text.length() > 0) {

                                createInvoiceDO.supplierName = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_XCUSTTRN")) {


                            if (text.length() > 0) {

                                createInvoiceDO.customerTrn = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XSUPPLTRN")) {


                            if (text.length() > 0) {

                                createInvoiceDO.supplierTrn = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XTAX")) {


                            if (text.length() > 0) {

                                createInvoiceDO.tax = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XRATE")) {

                            if (text.length() > 0) {

                                createInvoiceDO.taxRate = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_XTAXAMT")) {

                            if (text.length() > 0) {

                                createInvoiceDO.taxAmount = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_XTOTGRSS")) {


                            if (text.length() > 0) {

                                createInvoiceDO.totalGrossAmount = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XTOTDIC")) {

                            if (text.length() > 0) {

                                createInvoiceDO.totalDiscount = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_XAMTNOT")) {

                            if (text.length() > 0) {

                                createInvoiceDO.excludingTax = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_XTOTVAT")) {


                            if (text.length() > 0) {

                                createInvoiceDO.totalTax = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XAMTATI")) {


                            if (text.length() > 0) {

                                createInvoiceDO.includingTax = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XCPYIMG")) {
                            if (text.length() > 0) {
                                createInvoiceDO.logo = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITEDES")) {
                            if (text.length() > 0) {
                                createInvoiceDO.siteDescription = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITADDLIG0")) {
                            if (text.length() > 0) {
                                createInvoiceDO.siteAddress1 = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITADDLIG1")) {
                            if (text.length() > 0) {
                                createInvoiceDO.siteAddress2 = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITADDLIG2")) {
                            if (text.length() > 0) {
                                createInvoiceDO.siteAddress3 = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITCRY")) {
                            if (text.length() > 0) {
                                createInvoiceDO.siteCountry = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITCTY")) {
                            if (text.length() > 0) {
                                createInvoiceDO.siteCity = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITPOS")) {
                            if (text.length() > 0) {
                                createInvoiceDO.sitePostalCode = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITLND")) {
                            if (text.length() > 0) {
                                createInvoiceDO.siteLandLine = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITMOB")) {
                            if (text.length() > 0) {
                                createInvoiceDO.siteMobile = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITFAX")) {
                            if (text.length() > 0) {
                                createInvoiceDO.siteFax = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITEML1")) {
                            if (text.length() > 0) {
                                createInvoiceDO.siteEmail1 = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITEML2")) {
                            if (text.length() > 0) {
                                createInvoiceDO.siteEmail2 = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITWEB")) {
                            if (text.length() > 0) {
                                createInvoiceDO.siteWebEmail = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITFAX")) {


                            if (text.length() > 0) {

                                createInvoiceDO.fax = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITLND")) {


                            if (text.length() > 0) {

                                createInvoiceDO.mobile = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XITMUNIT")) {

                            if (text.length() > 0) {
                                pdfInvoiceDo.quantityUnits = text;

                            }
                        }
                        text = "";

                    }

                    if (endTag.equalsIgnoreCase("GRP")) {
                        // customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        createInvoiceDO.pdfInvoiceDos.add(pdfInvoiceDo);
                    }
                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (mContext instanceof SignatureActivity) {

        } else {
            ((BaseActivity) mContext).showLoader();

        }
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        if (mContext instanceof SignatureActivity) {

        } else {
            ((BaseActivity) mContext).hideLoader();

        }
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, createInvoiceDO);
        }
    }
}