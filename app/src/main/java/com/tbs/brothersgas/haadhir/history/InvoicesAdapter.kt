package com.tbs.brothersgas.haadhir.history

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context
import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView

import com.tbs.brothersgas.haadhir.Activitys.CreatePaymentActivity
import com.tbs.brothersgas.haadhir.Activitys.InvoiceListActivity
import com.tbs.brothersgas.haadhir.Model.UnPaidInvoiceDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.ActiveDeliveryRequest
import com.tbs.brothersgas.haadhir.Requests.InvoiceDetailsRequest
import com.tbs.brothersgas.haadhir.common.AppConstants
import com.tbs.brothersgas.haadhir.pdfs.BGInvoicePdf
import com.tbs.brothersgas.haadhir.pdfs.BulkDeliveryNotePDF
import com.tbs.brothersgas.haadhir.pdfs.CYLDeliveryNotePDF
import com.tbs.brothersgas.haadhir.pdfs.utils.PDFConstants
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util

import java.util.ArrayList

class InvoicesAdapter(private val context: Context, private var siteDOS: ArrayList<UnPaidInvoiceDO>?) : androidx.recyclerview.widget.RecyclerView.Adapter<InvoicesAdapter.MyViewHolder>() {


    inner class MyViewHolder(view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
        var tvShipmentId: TextView
        var tvSite: TextView
        var tvDeliveryDate: TextView
        var tvCustomer: TextView
        var tvAddress: TextView


        init {
            tvShipmentId = view.findViewById<View>(R.id.tvShipmentId) as TextView
            tvSite = view.findViewById<View>(R.id.tvSite) as TextView
            tvDeliveryDate = view.findViewById<View>(R.id.tvDeliveryDate) as TextView
            tvCustomer = view.findViewById<View>(R.id.tvCustomer) as TextView
            tvAddress = view.findViewById<View>(R.id.tvAddress) as TextView


        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.invoice_history_data, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val unPaidInvoiceDO = siteDOS!![position]
        if (unPaidInvoiceDO.accountingDate.length > 0) {
            val aMonth = unPaidInvoiceDO.accountingDate.substring(4, 6)
            val ayear = unPaidInvoiceDO.accountingDate.substring(0, 4)
            val aDate = unPaidInvoiceDO.accountingDate.substring(Math.max(unPaidInvoiceDO.accountingDate.length - 2, 0))
            holder.tvShipmentId.text = unPaidInvoiceDO.invoiceId
            holder.tvSite.text = unPaidInvoiceDO.site
            holder.tvDeliveryDate.text =  aDate + "-" + aMonth + "-" + ayear
            holder.tvCustomer.text = unPaidInvoiceDO.customerId +" - "+unPaidInvoiceDO.customer
            holder.tvAddress.text = unPaidInvoiceDO.address1+" , "+unPaidInvoiceDO.address2+" , "+unPaidInvoiceDO.address3
        }



        holder.itemView.setOnClickListener {
            prepareInvoiceCreation(unPaidInvoiceDO.invoiceId)
        }


    }

    fun refreshAdapter(siteDoS: ArrayList<UnPaidInvoiceDO>) {
        for (i in siteDoS.indices) {

        }
        siteDOS = siteDoS
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return siteDOS!!.size
    }

    private fun prepareInvoiceCreation(id:String) {

        if (id!!.length > 0) {
            if (Util.isNetworkAvailable(context)) {

                val siteListRequest = InvoiceDetailsRequest(id, context)
                siteListRequest.setOnResultListener { isError, createPDFInvoiceDO ->

                    if (createPDFInvoiceDO != null) {
                        if (isError) {
//                            showAppCompatAlert("Error", "Invoice pdf api error", "Ok", "", "", false)
                        } else {

                            BGInvoicePdf.getBuilder(context).createHistoryPDF(createPDFInvoiceDO, "Preview");


                        }
                    } else {
//                        showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
                    }
                }
                siteListRequest.execute()
            } else {
//                showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "",false)

            }

        } else {
//            showAppCompatAlert("Error", "No Invoice Id Found", "Ok", "", "Failure", false)

        }

    }
}
