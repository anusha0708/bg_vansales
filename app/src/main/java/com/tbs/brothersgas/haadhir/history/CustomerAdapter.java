package com.tbs.brothersgas.haadhir.history;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tbs.brothersgas.haadhir.Model.CustomerDo;
import com.tbs.brothersgas.haadhir.R;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class CustomerAdapter extends RecyclerView.Adapter<CustomerAdapter.MyViewHolder> {

    private ArrayList<CustomerDo> customerDos;
    private Context context;
    private String from;


    public void refreshAdapter(@NotNull ArrayList<CustomerDo> customerDos) {
        this.customerDos = customerDos;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvCustomerName, tvCustomerCode, tvBuisinessLine, tvAddressCode, tvDescription, tvCity, tvEmirates, tvAddressLine;
        public TextView btnPay;
        private LinearLayout llDetails;
        public TextView tvOrderId;

        public MyViewHolder(View view) {
            super(view);
            llDetails       = (LinearLayout) view.findViewById(R.id.llDetails);
            tvCustomerName  = itemView.findViewById(R.id.tvCustomerName);
            tvCustomerCode  = itemView.findViewById(R.id.tvCustomerCode);
            tvBuisinessLine = itemView.findViewById(R.id.tvBuisinessLine);
            tvAddressCode   = itemView.findViewById(R.id.tvAddressCode);
            tvAddressLine   = itemView.findViewById(R.id.tvAddressLine);


        }
    }


    public CustomerAdapter(Context context, ArrayList<CustomerDo> listOrderDos, String froM) {
        this.context = context;
        this.customerDos = listOrderDos;
        this.from = froM;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.history_customer_data, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final CustomerDo customerDo = customerDos.get(position);

        holder.tvCustomerCode.setText(customerDo.customerId);
        holder.tvCustomerName.setText(customerDo.customerName);
        holder.tvBuisinessLine.setText(customerDo.businessLine);
        holder.tvAddressLine.setText(customerDo.postBox);
//        holder.tvAddressCode.setText(customerDo.amount);

        holder.llDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, TabListActivity.class);
                intent.putExtra("CODE", customerDo.customerId);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
//        if (filterList != null && filterList.size() > 0) {
//            return filterList.size();
//
//        } else {
//        }
        return customerDos != null ? customerDos.size() : 0;

    }

}
