package com.tbs.brothersgas.haadhir.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class PendingOrderDO implements Serializable {

    public String salesOrderNumber    = "";
    public String customer            = "";
    public String customerDescription = "";
    public String orderDate           = "";

}
