package com.tbs.brothersgas.haadhir.Activitys

import android.content.Intent
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast

import com.tbs.brothersgas.haadhir.Adapters.LocationAdapter
import com.tbs.brothersgas.haadhir.Adapters.ProductAdapter
import com.tbs.brothersgas.haadhir.Adapters.SpotSalesCustomerAdapter
import com.tbs.brothersgas.haadhir.Model.CustomerDo
import com.tbs.brothersgas.haadhir.Model.PriceDetailMainDO
import com.tbs.brothersgas.haadhir.Model.ProductDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.MRlocationListRequest
import com.tbs.brothersgas.haadhir.Requests.PODMRlocationListRequest
import com.tbs.brothersgas.haadhir.Requests.ProductListRequest
import com.tbs.brothersgas.haadhir.utils.Constants
import kotlinx.android.synthetic.main.base_layout.*

import java.util.ArrayList

class PODMRLocationsActivity : BaseActivity() {
    private val userId: String? = null
    private val orderCode: String? = null
    private var recycleview: androidx.recyclerview.widget.RecyclerView? = null
    private var locationAdapter: LocationAdapter? = null
    private var llOrderHistory: LinearLayout? = null
    private var tvScreenTitles: TextView? = null
    private var tvNoOrders: TextView? = null
    private var productDOS: List<ProductDO>? = ArrayList()
    private var llSearch: LinearLayout? = null
    private var etSearch: EditText? = null
    private var ivClearSearch: ImageView? = null
    private var ivSearchs: ImageView? = null
    private var ivGoBack: ImageView? = null
    var distance: Double = 0.0
    var podflag : Boolean=true

    override fun initialize() {
        llOrderHistory = layoutInflater.inflate(R.layout.loc_screen, null) as LinearLayout
        llBody.addView(llOrderHistory, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        disableMenuWithBackButton()
        flToolbar.visibility = View.GONE
        if (intent.hasExtra("DISTANCE")) {
            distance = intent.getExtras().getDouble("DISTANCE")
        }
        if (!intent.getBooleanExtra(Constants.IS_FROM_POD, false)) {
            podflag=true
        }else{
            podflag=false

        }
        initializeControls()
        tvScreenTitles!!.visibility = View.VISIBLE
        tvScreenTitles!!.text = "Select Location"
        recycleview!!.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)

        ivGoBack!!.setOnClickListener { finish() }
        ivSearch!!.setOnClickListener {
            //                if(llSearch.visibility == View.VISIBLE){
            //                llSearch.visibility = View.INVISIBLE
            //            }
            //            else{
            //            }
            tvScreenTitles!!.visibility = View.GONE
            llSearch!!.visibility = View.VISIBLE
        }

        ivClearSearch!!.setOnClickListener {
            etSearch!!.setText("")
            if (productDOS != null && productDOS!!.size > 0) {
                locationAdapter = LocationAdapter(this@PODMRLocationsActivity, productDOS,distance,podflag)
                recycleview!!.adapter = locationAdapter
                tvNoOrders!!.visibility = View.GONE
                recycleview!!.visibility = View.VISIBLE
            } else {
                tvNoOrders!!.visibility = View.VISIBLE
                recycleview!!.visibility = View.GONE
            }
        }

        etSearch!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }
            override fun afterTextChanged(editable: Editable?) {
                if(etSearch!!.text.toString().equals("", true)){
                    if(productDOS!=null &&productDOS!!.size>0){
                        locationAdapter        =  LocationAdapter(this@PODMRLocationsActivity, productDOS,distance,podflag)
                        recycleview!!.adapter    = locationAdapter
                        tvNoOrders!!.visibility  = View.GONE
                        recycleview!!.visibility = View.VISIBLE
                    }
                    else{
                        tvNoOrders!!.visibility  = View.VISIBLE
                        recycleview!!.visibility = View.GONE
                    }
                }
                else if(etSearch!!.text.toString().length>2){
                    filter(etSearch!!.text.toString())
                }
            }

        })


        // new CommonBL(OrderHistoryActivity.this, OrderHistoryActivity.this).orderHistory(userId);

    }

    override fun initializeControls() {
        recycleview = findViewById<View>(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        tvNoOrders = findViewById<View>(R.id.tvNoOrders) as TextView
        tvScreenTitles = findViewById<View>(R.id.tvScreenTitles) as TextView
        llSearch = findViewById(R.id.llSearch)
        etSearch = findViewById(R.id.etSearch)
        ivClearSearch = findViewById(R.id.ivClearSearch)
        ivGoBack = findViewById(R.id.ivGoBack)
        ivSearchs = findViewById(R.id.ivSearchs)
        ivSearchs!!.setOnClickListener{
            //            if(llSearch.visibility == View.VISIBLE){
//                llSearch.visibility = View.INVISIBLE
//            }
//            else{
//            }
            tvScreenTitles!!.visibility= View.GONE
            llSearch!!.visibility = View.VISIBLE
        }
    }

//    private fun filter(filtered: String): ArrayList<ProductDO> {
//        var productDOs = ArrayList<ProductDO>()
//        for (i in productDOS!!.indices) {
//            if (productDOs.get(i).productName.contains(filtered, true) || productDOs.get(i).productId.contains(filtered, true)) {
//                productDOs.add(productDOS!![i])
//            }
//        }
//
//        if (productDOs.size > 0) {
//            locationAdapter = LocationAdapter(this@MRLocationsActivity, productDOs)
//            recycleview!!.adapter = locationAdapter
//            //            locationAdapter.refreshAdapter(productDOs);
//            tvNoOrders!!.visibility = View.GONE
//            recycleview!!.visibility = View.VISIBLE
//        } else {
//            tvNoOrders!!.visibility = View.VISIBLE
//            recycleview!!.visibility = View.GONE
//        }
//        return productDOs
//    }
    private fun filter(filtered : String) : ArrayList<ProductDO>{
        val productDoS = ArrayList<ProductDO>()
        for (i in productDOS!!.indices){
            if(productDOS!!.get(i).productName.contains(filtered, true) ||productDOS!!.get(i).productId.contains(filtered, true)){
                productDoS.add(productDOS!!.get(i))
            }
        }
        if(productDoS!!.size>0){
            locationAdapter!!.refreshAdapter(productDoS)
            tvNoOrders!!.visibility = View.GONE
            recycleview!!.visibility = View.VISIBLE
        }
        else{
            tvNoOrders!!.visibility = View.VISIBLE
            recycleview!!.visibility = View.GONE
        }
        return productDoS
    }
    override fun onResume() {
        val siteListRequest = PODMRlocationListRequest(this@PODMRLocationsActivity)
        siteListRequest.setOnResultListener { isError, locationDOS ->
            hideLoader()
            if (isError) {
                hideLoader()
                tvNoOrders!!.visibility = View.VISIBLE
                recycleview!!.visibility = View.GONE
                Toast.makeText(this@PODMRLocationsActivity, R.string.error_product_list, Toast.LENGTH_SHORT).show()
            } else {
                hideLoader()
                productDOS = locationDOS
                if (productDOS!!.size > 0) {
                    tvNoOrders!!.visibility = View.GONE
                    recycleview!!.visibility = View.VISIBLE
                    locationAdapter = LocationAdapter(this@PODMRLocationsActivity, productDOS,distance,podflag)
                    recycleview!!.adapter = locationAdapter
                } else {
                    showAlert("" + R.string.error_NoData)
                    tvNoOrders!!.visibility = View.VISIBLE
                    recycleview!!.visibility = View.GONE
                }
            }
        }

        siteListRequest.execute()
        super.onResume()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 12) {
            val intent = Intent();
            setResult(resultCode, intent)
            finish()
        }else{
            super.onActivityResult(requestCode, resultCode, data)

        }
    }

}
