package com.tbs.brothersgas.haadhir.Activitys

import android.content.Intent
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.brothersgas.haadhir.Adapters.InvoiceAdapter
import com.tbs.brothersgas.haadhir.Model.LoadStockDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.InvoiceHistoryRequest
import com.tbs.brothersgas.haadhir.utils.Util
import java.util.ArrayList


class SalesInvoiceActivity : BaseActivity() {
    lateinit var invoiceAdapter: InvoiceAdapter
    lateinit var loadStockDOs: ArrayList<LoadStockDO>
    lateinit var recycleview : androidx.recyclerview.widget.RecyclerView
    lateinit var tvNoDataFound:TextView
    var fromId= 0

    override  protected fun onResume() {
        super.onResume()
    }
    override fun initialize() {
      var  llCategories = getLayoutInflater().inflate(R.layout.sales_invoice_list, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
    }
    override fun initializeControls() {
        tvScreenTitle.setText(R.string.sales_invoice)
        recycleview = findViewById(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        tvNoDataFound = findViewById(R.id.tvNoDataFound) as TextView
        if (intent.hasExtra("Sales")) {
            fromId = intent.extras.getInt("Sales")
        }
        recycleview.setLayoutManager(linearLayoutManager)

//        invoiceAdapter = InvoiceAdapter(this@SalesInvoiceActivity,  null)
//        recycleview.setAdapter(invoiceAdapter)
//

        if (Util.isNetworkAvailable(this)) {

            val driverListRequest = InvoiceHistoryRequest("",this@SalesInvoiceActivity)
            driverListRequest.setOnResultListener { isError, invoiceHistoryMainDO ->
                hideLoader()
                if (isError) {
                    tvNoDataFound.setVisibility(View.VISIBLE)
                    recycleview.setVisibility(View.GONE)
                    Toast.makeText(this@SalesInvoiceActivity, R.string.error_NoData, Toast.LENGTH_SHORT).show()
                } else {

                    if(invoiceHistoryMainDO.invoiceHistoryDOS.size>0){
                        tvNoDataFound.setVisibility(View.GONE)
                        recycleview.setVisibility(View.VISIBLE)
                        invoiceAdapter = InvoiceAdapter(this@SalesInvoiceActivity, invoiceHistoryMainDO.invoiceHistoryDOS)
                        recycleview!!.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this@SalesInvoiceActivity)

                        recycleview!!.adapter = invoiceAdapter
                    }else{
                        tvNoDataFound.setVisibility(View.VISIBLE)
                        recycleview.setVisibility(View.GONE)
                    }


                }
            }
            driverListRequest.execute()
        } else {
            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "",false)

        }

        val btnCreate = findViewById(R.id.btnCreate) as Button

        btnCreate.setOnClickListener {
            val intent = Intent(this@SalesInvoiceActivity, CreateInvoiceActivity::class.java);
            intent.putExtra("Sales",fromId)
            startActivity(intent);
        }
    }

}