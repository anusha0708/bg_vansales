//package com.tbs.brothersgas.haadhir.Activitys
//
//
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import com.tbs.brothersgas.haadhir.Adapters.LoanReturnAdapter;
//import com.tbs.brothersgas.haadhir.Model.LoadStockDO;
//import com.tbs.brothersgas.haadhir.Model.LoanReturnDO;
//import com.tbs.brothersgas.haadhir.R;
//
//import java.util.ArrayList;
//import java.util.LinkedHashMap;
//
////
//public class AddMoreReturnProductsActivity extends BaseActivity {
//
//    private LoanReturnAdapter invoiceAdapter;
//    private ArrayList<LoadStockDO> loadStockDOs;
//    private RecyclerView recycleview;
//    private TextView tvNoDataFound, tvBalanceQty,tvReturnedQty, tvIssuedQty, tvOpeningQty;
//    private String openingQty = "";
//    private int issuedQty =0;
//    private double returnQty, balanceQty;
//
//    @Override
//    public void initialize() {
//      View llCategories = getLayoutInflater().inflate(R.layout.selected_return_details, null);
//        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
//        toolbar.setNavigationIcon(R.drawable.back);
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//            }
//        });
//        initializeControls();
//        if(getIntent().hasExtra("IssuedQty"))
//            issuedQty = getIntent().extras.getInt("IssuedQty", 0)
//
//        if (getIntent().hasExtra("Products")) {
//            loanReturnDos = (ArrayList<LoanReturnDO>)getIntent().getSerializableExtra("Products");
//        }
//        if (intent.hasExtra("openingQty")) {
//            openingQty = intent.extras.getString("openingQty", "")
//
//        }
//        tvOpeningQty.setText(""+openingQty)
//        tvIssuedQty.setText(""+issuedQty)
//        setReturnQty(loanReturnDos)
//    }
//
//    private fun setReturnQty(loanReturnDos : ArrayList<LoanReturnDO>){
//        if(loanReturnDos!=null && loanReturnDos.size>0){
//            for (i in loanReturnDos.indices){
//                returnQty = returnQty + loanReturnDos.get(i).qty
//            }
//        }
//        tvReturnedQty.setText(""+returnQty)
//        tvBalanceQty.setText(""+(Integer.parseInt(openingQty)+issuedQty-returnQty))
//        balanceQty = Double.parseDouble(tvBalanceQty.getText().toString());
//    }
//
//    private LinkedHashMap<String, ArrayList<LoanReturnDO>> loanReturnDOsMap = new LinkedHashMap();
//    private ArrayList<LoanReturnDO> loanReturnDos = new ArrayList();
//
//    override fun initializeControls() {
//        tvScreenTitle.setText(R.string.inventory_count)
//        recycleview = findViewById(R.id.recycleview) as RecyclerView
//        val linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
//        tvNoDataFound = findViewById(R.id.tvNoDataFound) as TextView
//        tvOpeningQty = findViewById(R.id.tvOpeningQty) as TextView
//        tvIssuedQty = findViewById(R.id.tvIssuedQty) as TextView
//        tvReturnedQty = findViewById(R.id.tvReturnedQty) as TextView
//        tvBalanceQty = findViewById(R.id.tvBalanceQty) as TextView
//        recycleview.setLayoutManager(linearLayoutManager)
//        showLoader()
//        val driverListRequest = LoanReturnRequest(this@AddMoreReturnProductsActivity)
//        driverListRequest.setOnResultListener { isError, invoiceHistoryMainDO ->
//            hideLoader()
//            if (isError) {
//                tvNoDataFound.setVisibility(View.VISIBLE)
//                recycleview.setVisibility(View.GONE)
//                Toast.makeText(this@AddMoreReturnProductsActivity, R.string.error_customer_list, Toast.LENGTH_SHORT).show()
//            }
//            else {
//                loanReturnDOsMap = LinkedHashMap<String, ArrayList<LoanReturnDO>>();
//                var shipmentIds = ArrayList<String>()
//
//                if(loanReturnDos!=null && loanReturnDos.size>0){
//                    for (i in loanReturnDos.indices){
//                        if(!shipmentIds.contains(loanReturnDos.get(i).shipmentNumber)){
//                            shipmentIds.add(loanReturnDos.get(i).shipmentNumber)
//
//                        }
//                    }
//                }
//                if(shipmentIds.size>0){
//                    for (j in shipmentIds.indices){
//                        val returnList = ArrayList<LoanReturnDO>()
//                        for (i in loanReturnDos.indices) {
//                            if (loanReturnDos.get(i).shipmentNumber.equals(shipmentIds.get(j))) {
//                                returnList.add(loanReturnDos.get(i))
//                            }
//                        }
//                        loanReturnDOsMap.put(shipmentIds.get(j), returnList);
//                    }
//                }
//
//
////                if(invoiceHistoryMainDO.loanReturnDOS.size>0){
//                    tvNoDataFound.setVisibility(View.GONE)
//                    recycleview.setVisibility(View.VISIBLE)
//                    invoiceAdapter = LoanReturnAdapter(this@AddMoreReturnProductsActivity, loanReturnDOsMap,"")
//                    recycleview.adapter = invoiceAdapter
////                }else{
////                    tvNoDataFound.setVisibility(View.VISIBLE)
////                    recycleview.setVisibility(View.GONE)
////                }
//
//
//            }
//        }
//        driverListRequest.execute()
//        val btnConfirm = findViewById(R.id.btnConfirm) as Button
//
//        btnConfirm.setOnClickListener {
//            if(loanReturnDOsMap!=null && loanReturnDOsMap.size>0){
//                StorageManager.getInstance(this).saveReturnCylinders(this, loanReturnDos)
//                val returnCount = StorageManager.getInstance(this).saveAllReturnStockValue(loanReturnDos);
//                if(returnCount>0){
//                   var recievedSite = preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID, "")
//                    var returnDate = CalendarUtils.getDate()
//
//                    val siteListRequest = CreateReturnRequest( recievedSite,returnDate,"ZA001",loanReturnDOsMap,this@AddMoreReturnProductsActivity)
//                    showLoader()
//                    siteListRequest.setOnResultListener { isError, loanReturnMainDo ->
//                        hideLoader()
//                        if (loanReturnMainDo != null) {
//                            if (isError) {
////                                Toast.makeText(this@SelectedReturnDetailsActivity, "Failed", Toast.LENGTH_SHORT).show()
//                            } else {
//                                if(loanReturnMainDo.loanReturnDOS.size>0){
//                                    val intent = Intent()
//                                    intent.putExtra("CapturedReturns", true)
//                                    setResult(11, intent)
//                                    finish()
//                                }else{
//                                    showToast("Unable to Create Loan")
//                                }
//
//                            }
//                        } else {
//                            showToast("Unable to Create Loan")
//                        }
//
//                    }
//                    siteListRequest.execute()
//
//
//                }
//                else{
//                    showToast("Unable to Create Loan")
//                }
//            }
//        }
//    }
//
//}