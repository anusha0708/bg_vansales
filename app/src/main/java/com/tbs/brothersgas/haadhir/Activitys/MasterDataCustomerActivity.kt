package com.tbs.brothersgas.haadhir.Activitys

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.brothersgas.haadhir.Adapters.SpotSalesCustomerAdapter
import com.tbs.brothersgas.haadhir.Model.CustomerDo
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.CustomerNewRequest
import com.tbs.brothersgas.haadhir.Requests.MasterCustomerNewRequest
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.utils.Util

class MasterDataCustomerActivity : BaseActivity() {
    lateinit var  userId: String
    lateinit var orderCode: String
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var customerAdapter: SpotSalesCustomerAdapter
    lateinit var llOrderHistory: View
//    lateinit var customerDos: ArrayList<CustomerDo>
    private var customerDos = java.util.ArrayList<CustomerDo>()
    lateinit var llSearch : LinearLayout
    lateinit var etSearch : EditText
    lateinit var ivClearSearch : ImageView
    lateinit var ivGoBack : ImageView
    lateinit var ivSearchs : ImageView
    lateinit var tvNoOrders : TextView
    private lateinit var tvScreenTitles : TextView

    override fun initialize() {
        llOrderHistory = layoutInflater.inflate(R.layout.customer_screen, null)
        llBody.addView(llOrderHistory, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        flToolbar.visibility = View.GONE
        disableMenuWithBackButton()
        initializeControls()
        tvScreenTitles.setText("Customer List")
        tvScreenTitles.visibility = View.VISIBLE
        ivGoBack.setOnClickListener{
            finish()
        }

        ivSearchs.setOnClickListener{
//            if(llSearch.visibility == View.VISIBLE){
//                llSearch.visibility = View.INVISIBLE
//            }
//            else{
//            }
            tvScreenTitles.visibility= View.GONE
            llSearch.visibility = View.VISIBLE
    }
        ivClearSearch.setOnClickListener{
            etSearch.setText("")
            if(customerDos!=null &&customerDos.size>0){
                customerAdapter = SpotSalesCustomerAdapter(this@MasterDataCustomerActivity, customerDos,"MASTER")
                recycleview.adapter = customerAdapter
                tvNoOrders.visibility = View.GONE
                recycleview.visibility = View.VISIBLE
            }
            else{
                tvNoOrders.visibility = View.VISIBLE
                recycleview.visibility = View.GONE
            }
        }

        etSearch.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(editable: Editable?) {
                if(etSearch.text.toString().equals("", true)){
                    if(customerDos!=null &&customerDos.size>0){
                        customerAdapter = SpotSalesCustomerAdapter(this@MasterDataCustomerActivity, customerDos,"MASTER")
                        recycleview.adapter = customerAdapter
                        tvNoOrders.visibility = View.GONE
                        recycleview.visibility = View.VISIBLE
                    }
                    else{
                        tvNoOrders.visibility = View.VISIBLE
                        recycleview.visibility = View.GONE
                    }
                }
                else if(etSearch.text.toString().length>2){
                    filter(etSearch.text.toString())
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })

        if (Util.isNetworkAvailable(this)) {

                        val driverListRequest = MasterCustomerNewRequest(this@MasterDataCustomerActivity)

                        driverListRequest.setOnResultListener { isError, customerDOs ->

                            if (isError) {
                                hideLoader()
                                Toast.makeText(this@MasterDataCustomerActivity, R.string.server_error, Toast.LENGTH_SHORT).show()
                            } else {
                                customerDos = customerDOs
                                if (customerDos != null && customerDos.size > 0) {
                                    customerAdapter = SpotSalesCustomerAdapter(this@MasterDataCustomerActivity, customerDos, "MASTER")
                                    recycleview!!.adapter  = customerAdapter
                                    tvNoOrders.visibility  = View.GONE
                                    recycleview.visibility = View.VISIBLE

                                } else {
                                    tvNoOrders.visibility  = View.VISIBLE
                                    recycleview.visibility = View.GONE
                                    showToast("No customers")
                                    hideLoader()
                                }
                            }
                        }
                        driverListRequest.execute()

                }
        else {
            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "",false)

        }

    }

    override fun initializeControls() {
        recycleview = findViewById<View>(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        llSearch             = findViewById<View>(R.id.llSearch) as LinearLayout
        etSearch             = findViewById<View>(R.id.etSearch) as EditText
        ivClearSearch        = findViewById<View>(R.id.ivClearSearch) as ImageView
        ivGoBack             = findViewById<View>(R.id.ivGoBack) as ImageView
        ivSearchs            = findViewById<View>(R.id.ivSearchs) as ImageView
        tvNoOrders           = findViewById<View>(R.id.tvNoOrders) as TextView
        tvScreenTitles       = findViewById<View>(R.id.tvScreenTitles) as TextView
        recycleview.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this@MasterDataCustomerActivity, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
    }

    private fun filter(filtered : String) : ArrayList<CustomerDo>{
        val customerDOs = ArrayList<CustomerDo>()
        for (i in customerDos.indices){
            if(customerDos.get(i).customerName.contains(filtered, true)
                    ||customerDos.get(i).customerId.contains(filtered, true)){
                customerDOs.add(customerDos.get(i))
            }
        }
        if(customerDOs.size>0){
            customerAdapter.refreshAdapter(customerDOs)
            tvNoOrders.visibility = View.GONE
            recycleview.visibility = View.VISIBLE
        }
        else{
            tvNoOrders.visibility = View.VISIBLE
            recycleview.visibility = View.GONE
        }
        return customerDOs
    }

    override fun onBackPressed() {
        if (!svSearch.isIconified) {
            svSearch.isIconified = true
            return
        }
        super.onBackPressed()
    }

}
