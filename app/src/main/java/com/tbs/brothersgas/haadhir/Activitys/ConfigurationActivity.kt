package com.tbs.brothersgas.haadhir.Activitys

import android.content.Intent
import androidx.drawerlayout.widget.DrawerLayout
import android.view.Gravity
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.*
import com.tbs.brothersgas.haadhir.BuildConfig
import com.tbs.brothersgas.haadhir.Model.ConfigurationDo
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.ConfigurationRequest
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.utils.CustomProgressDialog
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util

class ConfigurationActivity : BaseActivity() {
    lateinit var llSplash: RelativeLayout
    private var saveBtn: Button? = null
    private var resetBtn: Button? = null
    private var ipEditText: EditText? = null
    private var portEditText: EditText? = null
    private var poolEditText: EditText? = null
    private var userNameEditText: EditText? = null
    private var passEditText: EditText? = null
    private var backButton: ImageView? = null
    internal lateinit var ip: String
    internal lateinit var port: String
    internal lateinit var pool: String
    internal lateinit var dbUser: String
    internal lateinit var dbPass: String


    internal var customProgressDialog: CustomProgressDialog? = null

    override fun initializeControls() {
        flToolbar.visibility = View.GONE
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, Gravity.LEFT)

        val container = findViewById<View>(R.id.configcont)
        //  CustomBranding.setCustomBG(container);
        preferenceUtils  = PreferenceUtils(this@ConfigurationActivity)
        backButton       = findViewById<View>(R.id.ivBackC) as ImageView
        resetBtn         = findViewById<View>(R.id.btnReset) as Button
        saveBtn          = findViewById<View>(R.id.btnSave) as Button
        ipEditText       = findViewById<View>(R.id.etIpAddress) as EditText
        portEditText     = findViewById<View>(R.id.etPortNumber) as EditText
        poolEditText     = findViewById<View>(R.id.etAlias) as EditText
        userNameEditText = findViewById<View>(R.id.etUserName) as EditText
        passEditText     = findViewById<View>(R.id.etPassword) as EditText

        var uname = preferenceUtils.getStringFromPreference(PreferenceUtils.A_USER_NAME, "")
        var pwd   = preferenceUtils.getStringFromPreference(PreferenceUtils.A_PASSWORD, "")
        var ipp   = preferenceUtils.getStringFromPreference(PreferenceUtils.IP_ADDRESS, "")
        var pport = preferenceUtils.getStringFromPreference(PreferenceUtils.PORT, "")
        var ppool = preferenceUtils.getStringFromPreference(PreferenceUtils.ALIAS, "")

        ipEditText!!.setText(ipp)
        portEditText!!.setText(pport)
        poolEditText!!.setText(ppool)
        userNameEditText!!.setText(uname)
        passEditText!!.setText(pwd)


        backButton!!.setOnClickListener { performBack() }

        resetBtn!!.setOnClickListener {
            ipEditText!!.setText("")
            portEditText!!.setText("")
            poolEditText!!.setText("")
            userNameEditText!!.setText("")
            passEditText!!.setText("")
        }
        ip = ipEditText!!.text.toString()
        port = portEditText!!.text.toString()
        pool = poolEditText!!.text.toString()
        dbUser = userNameEditText!!.text.toString()
        dbPass = passEditText!!.text.toString()
        saveBtn!!.setOnClickListener(OnClickListener {
            ip = ipEditText!!.text.toString().trim { it <= ' ' }
            if (ip.length == 0) {
                showAlert("" + resources.getString(R.string.config_enter_ip))
                return@OnClickListener
            }

            port = portEditText!!.text.toString().trim { it <= ' ' }
            if (port.length == 0) {
                showAlert("" + resources.getString(R.string.config_port))

                return@OnClickListener
            }

            pool = poolEditText!!.text.toString().trim { it <= ' ' }
            if (pool.length == 0) {
                showAlert("" + resources.getString(R.string.config_pool))

                return@OnClickListener
            }

            dbUser = userNameEditText!!.text.toString().trim { it <= ' ' }
            if (dbUser.length == 0) {
                showAlert("" + resources.getString(R.string.config_user_name))

                return@OnClickListener
            }

            dbPass = passEditText!!.text.toString().trim { it <= ' ' }
            if (dbPass.length == 0) {
                showAlert("" + resources.getString(R.string.config_password))

                return@OnClickListener
            }

            if (Util.isNetworkAvailable(this)) {
                customProgressDialog = CustomProgressDialog(this@ConfigurationActivity, "" + resources.getString(R.string.config_wait))
                customProgressDialog!!.show()
                val configRequest = ConfigurationRequest()
                configRequest.setOnResultListener { isError, isSuccess ->
                    println("isError $isError isSuccess $isSuccess")
                    if (customProgressDialog != null && customProgressDialog!!.isShowing) {
                        customProgressDialog!!.dismiss()
                    }
                    if (isError) {
                        showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.config_auth_error), "" + resources.getString(R.string.ok), "", "" + resources.getString(R.string.from_failure), false)
                    } else {
                        if (isSuccess) {
                            val configurationDo = ConfigurationDo()
                            configurationDo.ipAddress = ip;
                            configurationDo.portNumber = port;
                            configurationDo.poolAlias = pool;
                            configurationDo.userName = dbUser;
                            configurationDo.password = dbPass;
                            preferenceUtils.saveString(PreferenceUtils.CONFIG_SUCCESS, "SUCCESS")

                            StorageManager.getInstance(this).saveConfigurationDetails(this@ConfigurationActivity, configurationDo);
                            preferenceUtils.saveString(PreferenceUtils.IP_ADDRESS, ip)
                            preferenceUtils.saveString(PreferenceUtils.PORT, port)
                            preferenceUtils.saveString(PreferenceUtils.ALIAS, pool)
                            preferenceUtils.saveString(PreferenceUtils.A_USER_NAME, dbUser)
                            preferenceUtils.saveString(PreferenceUtils.A_PASSWORD, dbPass)

                            showAppCompatAlert("" + resources.getString(R.string.success), "" + resources.getString(R.string.config_success), "" + resources.getString(R.string.ok), "", "" + resources.getString(R.string.from_success), false)

                            // Util.storeAppUrl(ConfigurationActivity.this, ip, port, pool, dbUser, dbPass);

                        } else {
                            showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.config_auth_error), "" + resources.getString(R.string.ok), "", "" + resources.getString(R.string.from_failure), false)
                        }
                    }
                }


                configRequest.execute(ip, port, pool, dbUser, dbPass)
            } else {
                showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.internet_connection), "" + resources.getString(R.string.ok), "", "" + resources.getString(R.string.from_failure), false)

            }
        })

    }

    override fun initialize() {
        llSplash = layoutInflater.inflate(R.layout.configuration, null) as RelativeLayout
        llBody.addView(llSplash, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()
        tvScreenTitle.setText("" + resources.getString(R.string.config_title))


//        ipEditText!!.setText("192.168.1.200")
//        ipEditText!!.setText("183.82.9.23")
//        portEditText!!.setText("8124")
//        poolEditText!!.setText("VANSDEV")
//        userNameEditText!!.setText("RENUKA")
//        passEditText!!.setText("TU024")
//
//        ipEditText!!.setText("47.91.104.128")
//        portEditText!!.setText("8124")
//        poolEditText!!.setText("WBGTEST")
//        userNameEditText!!.setText("VANSALES")
//        passEditText!!.setText("12345")

//        ipEditText!!.setText("47.91.105.187")
//        portEditText!!.setText("8124")
//        poolEditText!!.setText("BROSGST")
//        userNameEditText!!.setText("ADMIN")
//        passEditText!!.setText("admin")
//        preferenceUtils.saveString(PreferenceUtils.IP_ADDRESS, "47.91.104.128")
//        preferenceUtils.saveString(PreferenceUtils.PORT, "8124")
//        preferenceUtils.saveString(PreferenceUtils.ALIAS, "BROSGAS")
//        preferenceUtils.saveString(PreferenceUtils.A_USER_NAME, "VANSALES")
//        preferenceUtils.saveString(PreferenceUtils.A_PASSWORD, "12345")

        ipEditText!!.setText("47.91.104.128")
        portEditText!!.setText("8124")
        poolEditText!!.setText(BuildConfig.ALIAS)
        userNameEditText!!.setText("VANSALES")
        passEditText!!.setText("12345")
    }


    override fun onButtonNoClick(from: String) {
        if ("SUCCESS".equals(from, ignoreCase = true)) {
            finish()
        }
    }

    override fun onButtonYesClick(from: String) {

        if ("SUCCESS".equals(from, ignoreCase = true)) {
            val intent = Intent(this@ConfigurationActivity, LoginActivity::class.java)
            startActivity(intent)
            finish()

        } else
            if ("FAILURE".equals(from, ignoreCase = true)) {

            }

    }

    override fun onBackPressed() {
        performBack()
    }

    private fun performBack() {
        val intent = Intent(this@ConfigurationActivity, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }


}
