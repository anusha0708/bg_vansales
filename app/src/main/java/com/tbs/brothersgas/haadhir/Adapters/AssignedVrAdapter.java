package com.tbs.brothersgas.haadhir.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.tbs.brothersgas.haadhir.Activitys.CheckinScreen;
import com.tbs.brothersgas.haadhir.Model.DriverDO;
import com.tbs.brothersgas.haadhir.R;
import com.tbs.brothersgas.haadhir.database.StorageManager;
import com.tbs.brothersgas.haadhir.Model.VehicleCheckInDo;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;

import java.util.ArrayList;

public class AssignedVrAdapter extends RecyclerView.Adapter<AssignedVrAdapter.MyViewHolder> {

    private ArrayList<DriverDO> pickUpDos;
    private Context context;

    private RadioButton lastCheckedRB = null;


    private int lastSelectedPosition = -1;
    private String from = "Schedule";


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvSiteName, tvSiteId, tvTripNumber, tvVehicleCode, tvPlate, tvDriver, tvDDT;
        private LinearLayout llDetails;
        private RadioButton rb;

        public MyViewHolder(View view) {
            super(view);
            llDetails  = view.findViewById(R.id.llDetails);
            tvSiteName = view.findViewById(R.id.tvSiteName);
            tvSiteId      = view.findViewById(R.id.tvVrId);
            rb            = view.findViewById(R.id.rb);
            tvTripNumber  = view.findViewById(R.id.tvTripNumber);
            tvDriver      = view.findViewById(R.id.tvDriver);
            tvVehicleCode = view.findViewById(R.id.tvVehicleCode);
            tvPlate       = view.findViewById(R.id.tvPlate);
            tvDDT         = view.findViewById(R.id.tvDDT);


        }
    }

    private Dialog  scheduleddialog;

    public AssignedVrAdapter(Context context, ArrayList<DriverDO> driverDOS, Dialog  scheduleddialog, String from) {
        this.context = context;
        this.pickUpDos = driverDOS;
        this.scheduleddialog = scheduleddialog;
        this.from = from;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.route_id_data, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final DriverDO driverDO = pickUpDos.get(0);
        PreferenceUtils preferenceUtils = new PreferenceUtils(context);
        VehicleCheckInDo vehicleCheckInDo = StorageManager.getInstance(context).getVehicleCheckInData(context);
        String id = "";
        if(from.equalsIgnoreCase(context.getResources().getString(R.string.checkin_scheduled))){
            id = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "");
        }
        else if(from.equalsIgnoreCase(context.getResources().getString(R.string.checkin_non_scheduled))){
            id = preferenceUtils.getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, "");
        }

        if (id.length() > 0 && id.equals(driverDO.vehicleRouteId)) {
            holder.rb.setChecked(true);

        } else {
            holder.rb.setChecked(false);

        }
        String status = vehicleCheckInDo.getCheckInStatus();//preferenceUtils.getStringFromPreference(PreferenceUtils.CHECK_IN_STATUS, "");
        if (status.isEmpty()) {
            holder.llDetails.setClickable(true);
            holder. llDetails.setEnabled(true);
        } else {
            holder.llDetails.setClickable(false);
            holder. llDetails.setEnabled(false);

        }

        holder.rb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.llDetails.performClick();
            }
        });


        if(from.equalsIgnoreCase(context.getResources().getString(R.string.checkin_scheduled))){
            preferenceUtils.saveString(PreferenceUtils.VEHICLE_CODE, driverDO.vehicleCode);

            preferenceUtils.saveString(PreferenceUtils.VEHICLE_ROUTE_ID, driverDO.vehicleRouteId);
        }
        else if(from.equalsIgnoreCase(context.getResources().getString(R.string.checkin_non_scheduled))){
            preferenceUtils.saveString(PreferenceUtils.Non_Scheduled_Route_Id, driverDO.vehicleRouteId);
        }
//                preferenceUtils.saveString(PreferenceUtils.EM_CARRIER_ID, String.valueOf(pickUpDos.get(pos).emptyVehicle
        preferenceUtils.saveString(PreferenceUtils.CARRIER_ID,driverDO.vehicleCarrier);
        if(from.equalsIgnoreCase(context.getResources().getString(R.string.checkin_scheduled))){
            ((CheckinScreen)context).updateScheduleRootId(driverDO.vehicleRouteId);
        }
        else if(from.equalsIgnoreCase(context.getResources().getString(R.string.checkin_non_scheduled))){
            ((CheckinScreen)context).updateNonScheduleRootId(driverDO.vehicleRouteId);
        }


        preferenceUtils.saveString(PreferenceUtils.VR_ID, driverDO.vehicleRouteId);

        preferenceUtils.saveString(PreferenceUtils.V_PLATE, driverDO.plate);
        if(from.equalsIgnoreCase(context.getResources().getString(R.string.checkin_scheduled))){
            preferenceUtils.saveString(PreferenceUtils.N_SHIPMENTS, driverDO.nShipments);
        }

        if (driverDO.aTime.length()>0){

            String arrivalTime = driverDO.aTime.substring(Math.max(driverDO.aTime.length() - 2, 0));
            String departureTime = driverDO.dTime.substring(Math.max(driverDO.dTime.length() - 2, 0));
            String arrivalTime2 =driverDO.aTime.substring(0, 2);
            String departureTime2 = driverDO.dTime.substring(0, 2);


            preferenceUtils.saveString(PreferenceUtils.A_TIME, arrivalTime2+":"+arrivalTime);
            preferenceUtils.saveString(PreferenceUtils.D_TIME, departureTime2+":"+departureTime);
            String aMonth = driverDO.aDate.substring(4, 6);
            String ayear = driverDO.aDate.substring(0, 4);
            String aDate =driverDO.aDate.substring(Math.max(driverDO.aDate.length() - 2, 0));
            String dMonth = driverDO.dDate.substring(4, 6);
            String dyear = driverDO.dDate.substring(0, 4);
            String dDate =driverDO.dDate.substring(Math.max(driverDO.dDate.length() - 2, 0));

            preferenceUtils.saveString(PreferenceUtils.A_DATE, aDate+"-"+aMonth+"-"+ayear);
            preferenceUtils.saveString(PreferenceUtils.D_DATE, dDate+"-"+dMonth+"-"+dyear);
        }

        holder.llDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String vid = "";
                if(from.equalsIgnoreCase(context.getResources().getString(R.string.checkin_scheduled))){
                    vid = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "");
                }
                else if(from.equalsIgnoreCase(context.getResources().getString(R.string.checkin_non_scheduled))){
                    vid = preferenceUtils.getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, "");
                }
                if (vid.length() > 0 && !vid.equalsIgnoreCase(driverDO.vehicleRouteId)) {
                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case DialogInterface.BUTTON_POSITIVE:
                                    try {
                                    lastSelectedPosition = holder.getAdapterPosition();
                                    notifyDataSetChanged();
                                    holder.rb.setChecked(lastSelectedPosition == position);

                                    String id = holder.tvSiteName.getText().toString();
                                    if(from.equalsIgnoreCase(context.getResources().getString(R.string.checkin_scheduled))){
                                        ((CheckinScreen)context).updateScheduleRootId(driverDO.vehicleRouteId);
                                    }
                                    else if(from.equalsIgnoreCase(context.getResources().getString(R.string.checkin_non_scheduled))){
                                        ((CheckinScreen)context).updateNonScheduleRootId(driverDO.vehicleRouteId);
                                    }
                                    // Do nothing but close the dialog
                                    preferenceUtils.saveString(PreferenceUtils.CARRIER_ID,driverDO.vehicleCarrier);

                                    if(from.equalsIgnoreCase(context.getResources().getString(R.string.checkin_scheduled))){
                                        preferenceUtils.saveString(PreferenceUtils.VEHICLE_CODE, driverDO.vehicleCode);

                                        preferenceUtils.saveString(PreferenceUtils.VEHICLE_ROUTE_ID, driverDO.vehicleRouteId);
                                    }
                                    else if(from.equalsIgnoreCase(context.getResources().getString(R.string.checkin_non_scheduled))){
                                        preferenceUtils.saveString(PreferenceUtils.Non_Scheduled_Route_Id, driverDO.vehicleRouteId);
                                    }
                                    preferenceUtils.saveString(PreferenceUtils.VR_ID, driverDO.vehicleRouteId);

                                    preferenceUtils.saveString(PreferenceUtils.V_PLATE, driverDO.plate);
                                        if(from.equalsIgnoreCase(context.getResources().getString(R.string.checkin_scheduled))){
                                            preferenceUtils.saveString(PreferenceUtils.N_SHIPMENTS, driverDO.nShipments);
                                        }

                                    String arrivalTime = driverDO.aTime.substring(Math.max(driverDO.aTime.length() - 2, 0));
                                    String departureTime = driverDO.dTime.substring(Math.max(driverDO.dTime.length() - 2, 0));
                                    String arrivalTime2 =driverDO.aTime.substring(0, 2);
                                    String departureTime2 = driverDO.dTime.substring(0, 2);


                                    preferenceUtils.saveString(PreferenceUtils.A_TIME, arrivalTime2+":"+arrivalTime);
                                    preferenceUtils.saveString(PreferenceUtils.D_TIME, departureTime2+":"+departureTime);

                                    String aMonth = null;
                                    String ayear = null;
                                    String aDate = null;
                                    String dMonth = null;
                                    String dyear = null;
                                    String dDate = null;

                                        aMonth = driverDO.aDate.substring(4, 6);
                                        ayear = driverDO.aDate.substring(0, 4);
                                        aDate = driverDO.aDate.substring(Math.max(driverDO.aDate.length() - 2, 0));
                                        dMonth = driverDO.dDate.substring(4, 6);
                                        dyear = driverDO.dDate.substring(0, 4);
                                        dDate = driverDO.dDate.substring(Math.max(driverDO.dDate.length() - 2, 0));
                                                                        preferenceUtils.saveString(PreferenceUtils.A_DATE, aDate+"-"+aMonth+"-"+ayear);
                                    preferenceUtils.saveString(PreferenceUtils.D_DATE, dDate+"-"+dMonth+"-"+dyear);


                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    finally {
                                        if(scheduleddialog!=null){
                                            scheduleddialog.dismiss();
                                        }
                                    }
                                    break;

                                case DialogInterface.BUTTON_NEGATIVE:
                                    //No button clicked
                                    notifyDataSetChanged();
                                    break;
                            }
                        }
                    };

                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setMessage("Are you sure You want to replace the Previous Id?").setPositiveButton("Yes", dialogClickListener)
                            .setNegativeButton("No", dialogClickListener).show();
                }
                else {
                    lastSelectedPosition = holder.getAdapterPosition();
                    notifyDataSetChanged();
                    holder.rb.setChecked(lastSelectedPosition == position);

                    if(from.equalsIgnoreCase(context.getResources().getString(R.string.checkin_scheduled))){
                        preferenceUtils.saveString(PreferenceUtils.VEHICLE_CODE, driverDO.vehicleCode);

                        preferenceUtils.saveString(PreferenceUtils.VEHICLE_ROUTE_ID, driverDO.vehicleRouteId);
                    }
                    else if(from.equalsIgnoreCase(context.getResources().getString(R.string.checkin_non_scheduled))){
                        preferenceUtils.saveString(PreferenceUtils.Non_Scheduled_Route_Id, driverDO.vehicleRouteId);
                    }
//                preferenceUtils.saveString(PreferenceUtils.EM_CARRIER_ID, String.valueOf(pickUpDos.get(pos).emptyVehicle
                    preferenceUtils.saveString(PreferenceUtils.CARRIER_ID,driverDO.vehicleCarrier);
                    if(from.equalsIgnoreCase(context.getResources().getString(R.string.checkin_scheduled))){
                        ((CheckinScreen)context).updateScheduleRootId(driverDO.vehicleRouteId);
                    }
                    else if(from.equalsIgnoreCase(context.getResources().getString(R.string.checkin_non_scheduled))){
                        ((CheckinScreen)context).updateNonScheduleRootId(driverDO.vehicleRouteId);
                    }


                    preferenceUtils.saveString(PreferenceUtils.VR_ID, driverDO.vehicleRouteId);

                    preferenceUtils.saveString(PreferenceUtils.V_PLATE, driverDO.plate);
                    if(from.equalsIgnoreCase(context.getResources().getString(R.string.checkin_scheduled))){
                        preferenceUtils.saveString(PreferenceUtils.N_SHIPMENTS, driverDO.nShipments);
                    }

                       if (driverDO.aTime.length()>0){

                           String arrivalTime = driverDO.aTime.substring(Math.max(driverDO.aTime.length() - 2, 0));
                           String departureTime = driverDO.dTime.substring(Math.max(driverDO.dTime.length() - 2, 0));
                           String arrivalTime2 =driverDO.aTime.substring(0, 2);
                           String departureTime2 = driverDO.dTime.substring(0, 2);


                           preferenceUtils.saveString(PreferenceUtils.A_TIME, arrivalTime2+":"+arrivalTime);
                           preferenceUtils.saveString(PreferenceUtils.D_TIME, departureTime2+":"+departureTime);
                           String aMonth = driverDO.aDate.substring(4, 6);
                           String ayear = driverDO.aDate.substring(0, 4);
                           String aDate =driverDO.aDate.substring(Math.max(driverDO.aDate.length() - 2, 0));
                           String dMonth = driverDO.dDate.substring(4, 6);
                           String dyear = driverDO.dDate.substring(0, 4);
                           String dDate =driverDO.dDate.substring(Math.max(driverDO.dDate.length() - 2, 0));

                           preferenceUtils.saveString(PreferenceUtils.A_DATE, aDate+"-"+aMonth+"-"+ayear);
                           preferenceUtils.saveString(PreferenceUtils.D_DATE, dDate+"-"+dMonth+"-"+dyear);
                       }

//                    preferenceUtils.saveString(PreferenceUtils.CHECK_IN_SHEDULED, String.valueOf(R.string.checkin_scheduled));


                    if(scheduleddialog!=null){
                        scheduleddialog.dismiss();
                    }
                }


                //  Toast.makeText(AssignedVrAdapter.this.context, "Selected " + tvSiteName.getText(), Toast.LENGTH_LONG).show();
            }
        });
        //  holder.rb.setChecked(lastSelectedPosition == position);
        holder.tvSiteName.setText("Site : " + driverDO.site);
        holder.tvSiteId.setText(driverDO.vehicleRouteId);

        //  holder.tvSiteId.setText("" + driverDO.siteId);
        holder.tvVehicleCode.setText("Vehicle Code : " + driverDO.vehicleCode);
        holder.tvPlate.setText("Name Plate : " + driverDO.plate);
        holder.tvTripNumber.setText("Trip Number : " + driverDO.tripNumber);
        holder.tvDriver.setText("Driver : " + driverDO.driver);
        //holder.tvPlate.setText(""+driverDO.dDate+"  "+driverDO.plate);

        if (driverDO.notes.length() > 0) {

                String notess1 = driverDO.notes.substring(Math.max(driverDO.notes.length() - 55, 55));
                String notess2 = notess1.substring(0, notess1.length() - 10);
                ((CheckinScreen)context).tvNotes.setText(notess2);
            preferenceUtils.saveString(PreferenceUtils.NOTES, notess2);

            StorageManager.getInstance(context).insertCheckInData(context, vehicleCheckInDo);
        }

    }

    @Override
    public int getItemCount() {
        return pickUpDos.size();
    }

}
