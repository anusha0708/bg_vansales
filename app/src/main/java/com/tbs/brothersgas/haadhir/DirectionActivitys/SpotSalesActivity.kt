package com.tbs.brothersgas.pod.Activitys

import android.annotation.TargetApi
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.ScrollView
import android.widget.TextView
import com.tbs.brothersgas.haadhir.Activitys.ActiveDeliveryActivity
import com.tbs.brothersgas.haadhir.Activitys.BaseActivity
import com.tbs.brothersgas.haadhir.Activitys.SalesInvoiceActivity
import com.tbs.brothersgas.haadhir.Activitys.SpotSalesCustomerActivity
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils



class SpotSalesActivity : BaseActivity() {
    var REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 200


    override fun initialize() {
      var  llCategories = getLayoutInflater().inflate(R.layout.spotsales_layout, null) as ScrollView
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        disableMenuWithBackButton()
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            finish()
//            overridePendingTransition(R.anim.exit, R.anim.enter)
        }
        initializeControls()

    }


    override fun initializeControls() {

        tvScreenTitle.setText("Spot Sales")
        var  llDelivery = findViewById<View>(R.id.llDelivery) as LinearLayout
        var  llActiveDelivery = findViewById<View>(R.id.llActiveDelivery) as LinearLayout

        llDelivery.setOnClickListener {


            val vehicleCheckOutDo = StorageManager.getInstance(this@SpotSalesActivity).getVehicleCheckOutData(this@SpotSalesActivity)
            if (vehicleCheckOutDo.confirmCheckOutArrival.equals("", true)) {
                val vehicleCheckInDo = StorageManager.getInstance(this@SpotSalesActivity).getVehicleCheckInData(this@SpotSalesActivity);
                if (!vehicleCheckInDo.checkInStatus!!.isEmpty()) {
                    var shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
                    if (shipmentId == null || shipmentId.equals("")) {
                        val intent = Intent(this@SpotSalesActivity, SpotSalesCustomerActivity::class.java)
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        startActivity(intent)
                    } else {
                        showToast("Please process the current schedule " + shipmentId + " shipment")
                    }
                } else {
                    showAppCompatAlert("Alert!", "Please Finish Check-in Process", "OK", "", "FAILURE", false)
                }
            } else {
                showAppCompatAlert("", "You have arrived for CheckOut, you cannot access at this time", "OK", "", "", false)
            }
//            val intent = Intent(this@SpotSalesActivity, SpotSalesCustomerActivity::class.java)
//            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//            startActivity(intent)
        }
        llActiveDelivery.setOnClickListener {
            val vehicleCheckOutDo = StorageManager.getInstance(this@SpotSalesActivity).getVehicleCheckOutData(this@SpotSalesActivity)
            if (vehicleCheckOutDo.confirmCheckOutArrival.equals("", true)) {
                val vehicleCheckInDo = StorageManager.getInstance(this@SpotSalesActivity).getVehicleCheckInData(this@SpotSalesActivity);
                if (!vehicleCheckInDo.checkInStatus!!.isEmpty()) {
                    var shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
                    if (shipmentId == null || shipmentId.equals("")) {
                        val customerDo = StorageManager.getInstance(this@SpotSalesActivity).getCurrentSpotSalesCustomer(this@SpotSalesActivity)
                        if (!customerDo.customerId.equals("")) {
                            val intent = Intent(this@SpotSalesActivity, ActiveDeliveryActivity::class.java)
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            startActivity(intent)
                        }else{
                            showAppCompatAlert("", "There is No active delivery", "OK", "", "", false)

                        }
                    } else {
                        showAppCompatAlert("", "Please process the current schedule " + shipmentId + " shipment", "OK", "", "FAILURE", false)
                    }

                } else {
                    showAppCompatAlert("", "Please Finish Check-in Process", "OK", "", "FAILURE", false)
                }
            } else {
                showAppCompatAlert("", "You have arrived for CheckOut, you cannot access at this time", "OK", "", "", false)
            }
        }


    }
    override  protected fun onResume() {
        super.onResume()
    }


}