package com.tbs.brothersgas.haadhir.Activitys

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.*
import android.content.pm.PackageManager
import android.graphics.*
import android.location.Geocoder
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.Looper
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.webkit.PermissionRequest
import android.widget.*
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.single.PermissionListener
import com.tbs.brothersgas.haadhir.Adapters.ScheduledProductsAdapter
import com.tbs.brothersgas.haadhir.Model.CreateInvoiceDO
import com.tbs.brothersgas.haadhir.Model.CreateInvoicePaymentDO
import com.tbs.brothersgas.haadhir.Model.CustomerManagementDO
import com.tbs.brothersgas.haadhir.Model.PodDo
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.*
import com.tbs.brothersgas.haadhir.common.AppConstants
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.pdfs.BGInvoicePdf
import com.tbs.brothersgas.haadhir.pdfs.BulkDeliveryNotePDF
import com.tbs.brothersgas.haadhir.pdfs.CYLDeliveryNotePDF
import com.tbs.brothersgas.haadhir.pdfs.CylinderIssRecPdfBuilder
import com.tbs.brothersgas.haadhir.prints.PrinterConnection
import com.tbs.brothersgas.haadhir.prints.PrinterConstants
import com.tbs.brothersgas.haadhir.utils.CalendarUtils
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.text.DateFormat
import java.util.*

/**
 *Created by VenuAppasani on 22-12-2018.
 *Copyright (C) 2018 TBS - All Rights Reserved
 */
class PendingInvoiceSignatureActivity : BaseActivity(), View.OnClickListener {


    lateinit var mClear: Button
    lateinit var mGetSign: Button
    lateinit var mCancel: Button
    lateinit var file: File
    lateinit var mContent: LinearLayout
    lateinit var view: View
    lateinit var mSignature: signature
    var bitmap: Bitmap? = null
    lateinit var data: String

    lateinit var savedPath: String
    lateinit var encodedImage: String
    private val permissionRequestCode = 34

    lateinit var createPDFInvoiceDo: CreateInvoicePaymentDO
    lateinit var createInvoiceDO: CreateInvoiceDO
    lateinit var customerManagementDO: CustomerManagementDO
    private var mLastUpdateTime: String? = null
    private val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 10000
    private val FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS: Long = 5000
    private val REQUEST_CHECK_SETTINGS = 100
    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private var mSettingsClient: SettingsClient? = null
    private var mLocationRequest: LocationRequest? = null
    private var mLocationSettingsRequest: LocationSettingsRequest? = null
    private var mLocationCallback: LocationCallback? = null
    private var mCurrentLocation: Location? = null
    private var mRequestingLocationUpdates: Boolean? = null

    // Creating Separate Directory for saving Generated Images
    var DIRECTORY = Environment.getExternalStorageDirectory().getPath() + "/UserInvoiceSignature/"
    var pic_name = /*SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())*/"signature"
    var StoredPath = DIRECTORY + pic_name + ".png"

    @SuppressLint("MissingPermission")
    override fun initializeControls() {
        location()
        mContent = findViewById<View>(R.id.canvasLayout) as LinearLayout
        mSignature = signature(applicationContext, null)
        mSignature.setBackgroundColor(Color.WHITE)

        // Dynamically generating Layout through java code
        mContent.addView(mSignature, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        mClear = findViewById<View>(R.id.clear) as Button
        mGetSign = findViewById<View>(R.id.getsign) as Button

//        tvCustomerName.setText("Customer : " + preferenceUtils.getStringFromPreference(PreferenceUtils.CUSTOMER_NAME, ""))

        if (intent.hasExtra("DATA")) {
            data = intent.extras.getString("DATA")
        }

        mGetSign.setEnabled(false)
        mCancel = findViewById<View>(R.id.cancel) as Button
        view = mContent
        mGetSign.setOnClickListener(this@PendingInvoiceSignatureActivity)
        mClear.setOnClickListener(this@PendingInvoiceSignatureActivity)
        mCancel.setOnClickListener(this@PendingInvoiceSignatureActivity)

        // Method to create Directory, if the Directory doesn't exists
        file = File(DIRECTORY)
        if (!file.exists()) {
            file.mkdir()
        }
//         podDo = StorageManager.getInstance(this).getDepartureData(this);
        tvScreenTitle.setText("Sign & Save Here")




    }



    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.activity_invoice_signature, null) as ScrollView
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()
        disableMenuWithBackButton()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            //            showToast("Please save signature")
            finish()
        }
    }


    override fun onClick(v: View?) {


        // TODO Auto-generated method stub
        if (v === mClear) {
            mSignature.clear()
            mGetSign.setEnabled(false)
        } else if (v === mGetSign) {


            if (!lattitudeFused.isNullOrEmpty()) {
                stopLocationUpdates()
                if (Build.VERSION.SDK_INT >= 23) {
                    if (isStoragePermissionGranted()) {
                        showAppCompatAlert("", "Are You Sure You Want to \n Confirm Invoice?", "OK", "Cancel", "SUCCESS", true)

                    }
                } else {
                    showAppCompatAlert("", "Are You Sure You Want to \n Confirm Invoice?", "OK", "Cancel", "SUCCESS", true)

                }
//                        invoiceCreation()
            } else {
                showLoader()
                stopLocationUpdates()
                locationFetch()
//                showAppCompatAlert("", resources.getString(R.string.location_capture), "OK", "", "", false)
            }


        } else if (v === mCancel) {
            showToast("Please save signature")
        }
    }

    fun isStoragePermissionGranted(): Boolean {
        if (Build.VERSION.SDK_INT >= 23) {
            if (applicationContext.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                return true
            } else {
                ActivityCompat.requestPermissions(this, arrayOf<String>(Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)
                return false
            }
        } else {
            return true
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            view.setDrawingCacheEnabled(true)
            mSignature.save(view, StoredPath)
//            Toast.makeText(applicationContext, "Successfully Saved", Toast.LENGTH_SHORT).show()
            // Calling the same class
            recreate()
        }
        if (requestCode == permissionRequestCode) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                createInvoicePDF(createPDFInvoiceDo)
            } else {
//                Log.e(TAG, "WRITE_PERMISSION_DENIED")
            }
        } else {
            Toast.makeText(this, "The app was not allowed to write to your storage. Hence, it cannot function properly. Please consider granting it this permission", Toast.LENGTH_LONG).show()
        }
    }

    inner class signature(context: Context, attrs: AttributeSet?) : View(context, attrs) {
        private val paint = Paint()
        private val path = Path()

        private var lastTouchX: Float = 0.toFloat()
        private var lastTouchY: Float = 0.toFloat()
        private val dirtyRect = RectF()

        init {
            paint.setAntiAlias(true)
            paint.setColor(Color.BLACK)
            paint.setStyle(Paint.Style.STROKE)
            paint.setStrokeJoin(Paint.Join.ROUND)
            paint.setStrokeWidth(STROKE_WIDTH)
        }

        fun save(v: View, StoredPath: String) {
            Log.v("log_tag", "Width: " + v.getWidth())
            Log.v("log_tag", "Height: " + v.getHeight())
            if (bitmap == null) {
                bitmap = Bitmap.createBitmap(mContent.width, mContent.height, Bitmap.Config.RGB_565)
            }
            val canvas = Canvas(bitmap)
            try {
//                // Output the file
                val mFileOutStream = FileOutputStream(StoredPath)
                v.draw(canvas)
//
//                // Convert the output file to Image such as .png
//                bitmap!!.compress(Bitmap.CompressFormat.PNG, 2, mFileOutStream)
////                val intent = Intent(this@SignatureActivity, CaptureReturnDetailsActivity::class.java)
////                intent.putExtra("imagePath", StoredPath)
////                startActivity(intent)
//
                mFileOutStream.flush()
                mFileOutStream.close()
//
//                val baos = ByteArrayOutputStream();
//                bitmap!!.compress(Bitmap.CompressFormat.JPEG, 2, baos); //bm is the bitmap object
//
//                val byteArrayImage = baos.toByteArray();
//                encodedImage = android.util.Base64.encodeToString(byteArrayImage, android.util.Base64.DEFAULT)
//                var podDo = StorageManager.getInstance(context).getDepartureData(context);
//
//                intent.putExtra("SIGNATURE", StoredPath)
//                intent.putExtra("SignatureEncode", encodedImage)
//                podDo.signatureEncode = encodedImage;
//                savedPath = StoredPath
//                StorageManager.getInstance(context).saveDepartureData(this@SignatureActivity, podDo)
//                var scaledBitmap = scaleDown(bitmap!!, 2f, true)
//
//                val baoos = ByteArrayOutputStream();
//                scaledBitmap!!.compress(Bitmap.CompressFormat.JPEG, 2, baoos); //bm is the bitmap object
//
//                val byteArrayImagee = baos.toByteArray();
//                var encodedImagee = android.util.Base64.encodeToString(byteArrayImagee, android.util.Base64.DEFAULT)

                var bos = ByteArrayOutputStream();
                bitmap = getResizedBitmap(bitmap!!, 200)
                bitmap!!.compress(Bitmap.CompressFormat.PNG, 10, bos);
                var bitmapdata = bos.toByteArray();
                Log.e("Signature Size : ", "Size : " + bitmapdata)
                encodedImage = android.util.Base64.encodeToString(bitmapdata, android.util.Base64.DEFAULT)

                savedPath = StoredPath
                if (Util.isNetworkAvailable(context)) {
                    invoiceCreation()


//                    if (!lattitudeFused.isNullOrEmpty()) {
//                        stopLocationUpdates()
//
////                        invoiceCreation()
//                    } else {
//                        location()
//                        showAppCompatAlert("", resources.getString(R.string.location_capture), "OK", "", "", false)
//                    }
                } else {
                    showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

                }


            } catch (e: Exception) {
                Log.v("log_tag", e.toString())
            }

        }

        fun clear() {
            path.reset()
            invalidate()
            mGetSign.setEnabled(false)
        }

        fun scaleDown(realImage: Bitmap, maxImageSize: Float,
                      filter: Boolean): Bitmap {
            val ratio = Math.min(
                    maxImageSize.toFloat() / realImage.getWidth(),
                    maxImageSize.toFloat() / realImage.getHeight())
            val width = Math.round(ratio.toFloat() * realImage.getWidth())
            val height = Math.round(ratio.toFloat() * realImage.getHeight())
            val newBitmap = Bitmap.createScaledBitmap(realImage, width,
                    height, filter)
            return newBitmap
        }

        protected override fun onDraw(canvas: Canvas) {
            canvas.drawPath(path, paint)
        }

        override fun onTouchEvent(event: MotionEvent): Boolean {
            val eventX = event.x
            val eventY = event.y
            mGetSign.setEnabled(true)

            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    path.moveTo(eventX, eventY)
                    lastTouchX = eventX
                    lastTouchY = eventY
                    return true
                }

                MotionEvent.ACTION_MOVE,

                MotionEvent.ACTION_UP -> {

                    resetDirtyRect(eventX, eventY)
                    val historySize = event.historySize
                    for (i in 0 until historySize) {
                        val historicalX = event.getHistoricalX(i)
                        val historicalY = event.getHistoricalY(i)
                        expandDirtyRect(historicalX, historicalY)
                        path.lineTo(historicalX, historicalY)
                    }
                    path.lineTo(eventX, eventY)
                }

                else -> {
                    debug("Ignored touch event: " + event.toString())
                    return false
                }
            }

            invalidate((dirtyRect.left - HALF_STROKE_WIDTH).toInt(),
                    (dirtyRect.top - HALF_STROKE_WIDTH).toInt(),
                    (dirtyRect.right + HALF_STROKE_WIDTH).toInt(),
                    (dirtyRect.bottom + HALF_STROKE_WIDTH).toInt())

            lastTouchX = eventX
            lastTouchY = eventY

            return true
        }

        private fun debug(string: String) {

            Log.v("log_tag", string)

        }

        private fun expandDirtyRect(historicalX: Float, historicalY: Float) {
            if (historicalX < dirtyRect.left) {
                dirtyRect.left = historicalX
            } else if (historicalX > dirtyRect.right) {
                dirtyRect.right = historicalX
            }

            if (historicalY < dirtyRect.top) {
                dirtyRect.top = historicalY
            } else if (historicalY > dirtyRect.bottom) {
                dirtyRect.bottom = historicalY
            }
        }

        private fun resetDirtyRect(eventX: Float, eventY: Float) {
            dirtyRect.left = Math.min(lastTouchX, eventX)
            dirtyRect.right = Math.max(lastTouchX, eventX)
            dirtyRect.top = Math.min(lastTouchY, eventY)
            dirtyRect.bottom = Math.max(lastTouchY, eventY)
        }

    }

    companion object {
        private val STROKE_WIDTH = 5f
        private val HALF_STROKE_WIDTH = STROKE_WIDTH / 2
    }


    private fun customerManagement(id: String, type: Int) {
        if (Util.isNetworkAvailable(this)) {
            val request = CustomerManagementRequest(id, type, this)
            request.setOnResultListener { isError, customerDo ->
                hideLoader()
                if (customerDo != null) {
                    if (isError) {
                        showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
                    } else {
                        if (customerDo.flag.equals("Customer authorized for this transaction", true)) {
                            invoiceCreation()
                        } else {
                            showAppCompatAlert("Alert!", "Customer not authorized for this transaction", "Ok", "", "", false)

                        }
                    }
                } else {
                    showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
                }
            }
            request.execute()
        } else {
            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "FAILURE", false)

        }

    }

    private fun invoiceCreation() {
        if (Util.isNetworkAvailable(this)) {
            if (data.length > 0) {
                val siteListRequest = CreateInvoiceRequest(encodedImage,"", data, lattitudeFused, longitudeFused, addressFused, this@PendingInvoiceSignatureActivity)
                siteListRequest.setOnResultListener { isError, createPaymentDO ->
                    hideLoader()
                    if (createPaymentDO != null) {
                        createInvoiceDO = createPaymentDO
                        if (isError) {
                            showAppCompatAlert("Info!", "Error in Service Response, Please try again...", "Ok", "", "", false)
                        } else {
                            if (createInvoiceDO.flag == 2) {
                                stopLocUpdates()

                                showToast("Invoice Created - " + createInvoiceDO.salesInvoiceNumber)

//                            showAppCompatAlert("Success", "Invoice Created - " + createInvoiceDO.salesInvoiceNumber, "Ok", "", "SUCCESS", false)
                                //Toast.makeText(this@CreateInvoiceActivity, "Invoice Created", Toast.LENGTH_SHORT).show()
                                preferenceUtils.saveString(PreferenceUtils.PENDING_INVOICE, createPaymentDO.salesInvoiceNumber)
//                                preferenceUtils.saveString(PreferenceUtils.INVOICE_AMOUNT, createPaymentDO.amount)
//                                preferenceUtils.saveString(PreferenceUtils.INVOICE_SHIPMENT_ID, createPaymentDO.o_shipmentNumber)
//                                preferenceUtils.saveString(PreferenceUtils.PREVIEW_INVOICE_ID, createPaymentDO.salesInvoiceNumber)
//                                podDo.invoice = createPaymentDO.salesInvoiceNumber;
//                                StorageManager.getInstance(this).saveDepartureData(this, podDo)
                                mGetSign.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                                mGetSign.setClickable(false)
                                mGetSign.setEnabled(false)

                                val intent = Intent()
                                intent.putExtra("INVOICE", createPaymentDO.salesInvoiceNumber)
                                setResult(10, intent)
                                finish()

                                prepareInvoiceCreation()


                            } else if (createInvoiceDO.flag == 4) {
                                if (createInvoiceDO.message.length > 0) {
                                    showAppCompatAlert("Error", "" + createInvoiceDO.message, "Ok", "", "", false)

                                } else {
                                    showAppCompatAlert("Error", "Invoice Not Created!", "Ok", "", "", false)

                                }

                            } else {
                                if (createInvoiceDO.message.length > 0) {
                                    showAppCompatAlert("Error", "" + createInvoiceDO.message, "Ok", "", "", false)

                                } else {
                                    showAppCompatAlert("Error", "Invoice Not Created!", "Ok", "", "", false)

                                }

                            }

                        }
                    } else {
                        showAppCompatAlert("Error", "Invoice Not Created!", "Ok", "", "", false)


                    }
                }

                siteListRequest.execute()
            } else {
                //   mGetSign.isClickable=false
            }
        } else {
            showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

        }

    }

    private fun prepareInvoiceCreation() {
        var id = preferenceUtils.getStringFromPreference(PreferenceUtils.PENDING_INVOICE, "")
//        var id="CDC-U101-19000091"//
        if (id.length > 0) {
            if (Util.isNetworkAvailable(this)) {
                val siteListRequest = InvoiceDetailsRequest(id, this)
                siteListRequest.setOnResultListener { isError, createPDFInvoiceDO ->
                    hideLoader()
                    if (createPDFInvoiceDO != null) {
                        if (isError) {
                            showToast("Unable to send Email");
//                        showAppCompatAlert("Error", "Unable to send Email", "Ok", "", "", false)
                        } else {
                            createPDFInvoiceDo = createPDFInvoiceDO
                            if (createInvoiceDO.email == 10) {
                                createInvoicePDF(createPDFInvoiceDO)
                            } else {
//                                showToast("you are not authorized to send email")
                            }
                            if (createInvoiceDO.print == 10) {
                                if (checkFilePermission()) {
                                    printDocument(createPDFInvoiceDO, PrinterConstants.PrintInvoiceReport);
                                } else {
                                    showToast("permission not enabled")

                                }
                            } else {
//                                showToast("you are not authorized to print invoice")

                            }


                        }
                    } else {
                        showToast("Unable to send Email");

//                    showAppCompatAlert("Error", "Unable to send Email!!", "Ok", "", "", false)
                    }
                }
                siteListRequest.execute()
            } else {
//            showAppCompatAlert("Error", "Unable to send Email", "Ok", "", "Failure", false)
            }
        } else {
            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "", false)

        }


    }

    private fun checkFilePermission(): Boolean {
        if (ContextCompat.checkSelfPermission(this@PendingInvoiceSignatureActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this@PendingInvoiceSignatureActivity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), permissionRequestCode)
        } else {
            return true
        }
        return false
    }

    private fun createInvoicePDF(createPDFInvoiceDO: CreateInvoicePaymentDO) {
        BGInvoicePdf.getBuilder(this).build(createPDFInvoiceDO, "Email");
    }


    private fun printDocument(obj: Any, from: Int) {
        val printConnection = PrinterConnection.getInstance(this@PendingInvoiceSignatureActivity)
        if (printConnection.isBluetoothEnabled()) {
            printConnection.connectToPrinter(obj, from)
        } else {
//            PrinterConstants.PRINTER_MAC_ADDRESS = ""
            showToast("Please enable your mobile Bluetooth.")
//            showAppCompatAlert("", "Please enable your mobile Bluetooth.", "Enable", "Cancel", "EnableBluetooth", false)
        }
    }

    private fun pairedDevices() {
        val pairedDevices = BluetoothAdapter.getDefaultAdapter().getBondedDevices()
        if (pairedDevices.size > 0) {
            for (device in pairedDevices) {
                val deviceName = device.getName()
                val mac = device.getAddress() // MAC address
//                if (StorageManager.getInstance(this).getPrinterMac(this).equals("")) {
                StorageManager.getInstance(this).savePrinterMac(this, mac);
//                }
                Log.e("Bluetooth", "Name : " + deviceName + " Mac : " + mac)
                break
            }
        }
    }

    override fun onResume() {
        super.onResume()
        pairedDevices()
    }

    override fun onStart() {
        super.onStart()
        val filter = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        registerReceiver(mReceiver, filter)
//        val filter = IntentFilter(Intent.PAIRIN.ACTION_FOUND Intent. "android.bluetooth.device.action.PAIRING_REQUEST");
//        registerReceiver(mReceiver, filter)
//        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
//        mBluetoothAdapter.startDiscovery()
//        val filter2 = IntentFilter( "android.bluetooth.device.action.PAIRING_REQUEST")
//        registerReceiver(mReceiver, filter2)

    }

    override fun onDestroy() {
        unregisterReceiver(mReceiver)
        hideLoader()
//        PrinterConstants.PRINTER_MAC_ADDRESS = ""
        super.onDestroy()
    }

    private val mReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            if (BluetoothDevice.ACTION_FOUND == action) {
                val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE);
                PrinterConstants.bluetoothDevices.add(device)
                Log.e("Bluetooth", "Discovered => name : " + device.name + ", Mac : " + device.address)
            } else if (action.equals(BluetoothDevice.ACTION_ACL_CONNECTED)) {
                Log.e("Bluetooth", "status : ACTION_ACL_CONNECTED")
                pairedDevices()
            } else if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                val state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
                if (state == BluetoothAdapter.STATE_OFF) {
                    Log.e("Bluetooth", "status : STATE_OFF")
                } else if (state == BluetoothAdapter.STATE_TURNING_OFF) {
                    Log.e("Bluetooth", "status : STATE_TURNING_OFF")
                } else if (state == BluetoothAdapter.STATE_ON) {
                    Log.e("Bluetooth", "status : STATE_ON")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_TURNING_ON) {
                    Log.e("Bluetooth", "status : STATE_TURNING_ON")
                } else if (state == BluetoothAdapter.STATE_CONNECTING) {
                    Log.e("Bluetooth", "status : STATE_CONNECTING")
                } else if (state == BluetoothAdapter.STATE_CONNECTED) {
                    Log.e("Bluetooth", "status : STATE_CONNECTED")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_DISCONNECTED) {
                    Log.e("Bluetooth", "status : STATE_DISCONNECTED")
                }
            }
        }
    }

    override fun onBackPressed() {
        finish()

        super.onBackPressed()
    }

    override fun onButtonYesClick(from: String) {

        if ("SUCCESS".equals(from, ignoreCase = true)) {
            view.setDrawingCacheEnabled(true)
            mSignature.save(view, StoredPath)
        } else {

        }
    }

    fun getResizedBitmap(image: Bitmap, size: Int): Bitmap {

        return Bitmap.createScaledBitmap(image, size, size, true)
    }


    fun locationFetch() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mSettingsClient = LocationServices.getSettingsClient(this)

        mLocationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                super.onLocationResult(locationResult)
                // location is received
                mCurrentLocation = locationResult!!.lastLocation
                mLastUpdateTime = DateFormat.getTimeInstance().format(Date())

                updateLocUI()
            }
        }

        mRequestingLocationUpdates = false

        mLocationRequest = LocationRequest()
        mLocationRequest!!.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS)
        mLocationRequest!!.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS)
        mLocationRequest!!.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)

        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(mLocationRequest!!)
        mLocationSettingsRequest = builder.build()

        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(object : PermissionListener {
                    override fun onPermissionRationaleShouldBeShown(permission: com.karumi.dexter.listener.PermissionRequest?, token: PermissionToken?) {
                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }

                    override fun onPermissionGranted(response: PermissionGrantedResponse) {
                        mRequestingLocationUpdates = true
                        startLocUpdates()
                    }

                    override fun onPermissionDenied(response: PermissionDeniedResponse) {
                        if (response.isPermanentlyDenied()) {

                            showToast("Location Permission denied")
                            // open device settings when the permission is
                            // denied permanently
//                            openSettings()
                        }
                    }

                    fun onPermissionRationaleShouldBeShown(permission: PermissionRequest, token: PermissionToken) {
                        token.continuePermissionRequest()
                    }
                }).check()
    }

    fun updateLocUI() {
        if (mCurrentLocation != null) {
            stopLocUpdates()
            hideLoader()
            showAppCompatAlert("", "Are You Sure You Want to \n Confirm Invoice?", "OK", "Cancel", "SUCCESS", true)

            lattitudeFused = mCurrentLocation!!.getLatitude().toString()
            longitudeFused = mCurrentLocation!!.getLongitude().toString()
            var gcd = Geocoder(getBaseContext(), Locale.getDefault());

            try {
                var addresses = gcd.getFromLocation(mCurrentLocation!!.getLatitude(),
                        mCurrentLocation!!.getLongitude(), 1);
                if (addresses.size > 0) {
                    System.out.println(addresses.get(0).getLocality());
                    var cityName = addresses.get(0).getLocality();
                    addressFused = cityName
                }
            } catch (e: java.lang.Exception) {
                e.printStackTrace();
            }


        }

    }

    @SuppressLint("MissingPermission")
    private fun startLocUpdates() {
        mSettingsClient!!
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this) {
                    //                    Log.i(TAG, "All location settings are satisfied.")

//                    Toast.makeText(applicationContext, "Started location updates!", Toast.LENGTH_SHORT).show()


                    mFusedLocationClient!!.requestLocationUpdates(mLocationRequest,
                            mLocationCallback, Looper.myLooper())

                    updateLocUI()
                }
                .addOnFailureListener(this) { e ->
                    val statusCode = (e as ApiException).statusCode
                    when (statusCode) {
                        LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
//                            Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " + "location settings ")
                            try {
                                // Show the dialog by calling startResolutionForResult(), and check the
                                // result in onActivityResult().
                                val rae = e as ResolvableApiException
                                rae.startResolutionForResult(this@PendingInvoiceSignatureActivity, REQUEST_CHECK_SETTINGS)
                            } catch (sie: IntentSender.SendIntentException) {
//                                Log.i(TAG, "PendingIntent unable to execute request.")
                            }

                        }
                        LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                            val errorMessage = "Location settings are inadequate, and cannot be " + "fixed here. Fix in Settings."
//                            Log.e(TAG, errorMessage)

                            Toast.makeText(this@PendingInvoiceSignatureActivity, errorMessage, Toast.LENGTH_LONG).show()
                        }
                    }

                    updateLocUI()
                }
    }

    fun stopLocUpdates() {
        // Removing location updates
        if(mFusedLocationClient!=null&&mLocationCallback!=null){
            mFusedLocationClient!!
                    .removeLocationUpdates(mLocationCallback)
                    .addOnCompleteListener(this) {

                    }
        }

    }
}