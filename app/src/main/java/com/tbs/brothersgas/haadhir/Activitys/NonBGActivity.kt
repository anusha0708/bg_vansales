package com.tbs.brothersgas.haadhir.Activitys

import android.content.Intent
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.tbs.brothersgas.haadhir.Adapters.LoanReturnAdapter
import com.tbs.brothersgas.haadhir.Adapters.NonBGProductsAdapter
import com.tbs.brothersgas.haadhir.Adapters.ScheduledProductsAdapter
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryDO
import com.tbs.brothersgas.haadhir.Model.LoadStockDO
import com.tbs.brothersgas.haadhir.Model.LoanReturnDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.ActiveDeliveryRequest
import com.tbs.brothersgas.haadhir.Requests.CreateNONBGRequest
import com.tbs.brothersgas.haadhir.Requests.LoanReturnRequest
import com.tbs.brothersgas.haadhir.utils.CalendarUtils
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util

//
class NonBGActivity : BaseActivity() {
    lateinit var loadStockAdapter: NonBGProductsAdapter
    lateinit var loadStockDOs: ArrayList<LoadStockDO>
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var tvNoDataFound: TextView
//    lateinit var activeDeliveryDos: ArrayList<ActiveDeliveryDO>

    private var isSentForApproval: Boolean = false

    override fun initialize() {
        val llCategories = getLayoutInflater().inflate(R.layout.selected_nonbg, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, Gravity.LEFT)
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            showToast("Please confirm..!")
        }
        tvScreenTitle.setText("Non BG Cylinder Return")

        initializeControls()

    }

    override fun initializeControls() {

        recycleview = findViewById(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        tvNoDataFound = findViewById(R.id.tvNoDataFound) as TextView

        val btnConfirm = findViewById(R.id.btnConfirm) as Button
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
        recycleview.setLayoutManager(linearLayoutManager)

        var id = "" + preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
        var spotId = "" + preferenceUtils.getStringFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER, "")
        if (spotId.length > 0) {

            if (Util.isNetworkAvailable(this)) {

                val driverListRequest = ActiveDeliveryRequest(spotId, this@NonBGActivity)
                driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                    hideLoader()
                    if (isError) {
                        tvNoDataFound.setVisibility(View.VISIBLE)
                        recycleview.setVisibility(View.GONE)
                        btnConfirm.setVisibility(View.GONE)
//                showToast(resources.getString(R.string.error_NoData))
                    } else {


                        if (activeDeliveryDo.activeDeliveryDOS.size > 0) {
                            tvNoDataFound.setVisibility(View.GONE)
                            recycleview.setVisibility(View.VISIBLE)
                            btnConfirm.setVisibility(View.VISIBLE)
                            loadStockAdapter = NonBGProductsAdapter(this@NonBGActivity, activeDeliveryDo.activeDeliveryDOS, "NONBG", "")
                            recycleview.setAdapter(loadStockAdapter)
                        } else {
                            tvNoDataFound.setVisibility(View.VISIBLE)
                            recycleview.setVisibility(View.GONE)
                            btnConfirm.setVisibility(View.GONE)
                        }


                    }
                }
                driverListRequest.execute()
            } else {
                showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

            }

        } else {
            if (Util.isNetworkAvailable(this)) {
                val driverListRequest = ActiveDeliveryRequest(id, this@NonBGActivity)
                driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                    hideLoader()
                    if (isError) {
                        tvNoDataFound.setVisibility(View.VISIBLE)
                        recycleview.setVisibility(View.GONE)
                        btnConfirm.setVisibility(View.GONE)
//                showToast(resources.getString(R.string.error_NoData))
                    } else {

                        if (activeDeliveryDo.activeDeliveryDOS.size > 0) {
                            tvNoDataFound.setVisibility(View.GONE)
                            recycleview.setVisibility(View.VISIBLE)
                            btnConfirm.setVisibility(View.VISIBLE)
                            loadStockAdapter = NonBGProductsAdapter(this@NonBGActivity, activeDeliveryDo.activeDeliveryDOS, "NONBG", "")
                            recycleview.setAdapter(loadStockAdapter)
                        } else {
                            tvNoDataFound.setVisibility(View.VISIBLE)
                            recycleview.setVisibility(View.GONE)
                            btnConfirm.setVisibility(View.GONE)
                        }


                    }
                }
                driverListRequest.execute()
            } else {
                showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

            }


        }


        btnConfirm.setOnClickListener {


            if (loadStockAdapter != null) {
                val activeDeliveryDos = loadStockAdapter.getSelectedLoadStockDOs();



                if (activeDeliveryDos != null && !activeDeliveryDos.isEmpty() && activeDeliveryDos.size > 0) {
                    var isProductExisted = false;

                    for (k in activeDeliveryDos.indices) {
                        if (activeDeliveryDos.get(k).orderedQuantity > 0) {
                            isProductExisted = true
                            break
                        }
                    }

                    if (isProductExisted) {
                        nonBG(activeDeliveryDos)
                    } else {
                        val intent = Intent(this@NonBGActivity, SignatureActivity::class.java);
                        startActivityForResult(intent, 11)
                    }


                } else {
                    showToast("No  Items found")
                }
            } else {
                showToast("No  Items found")
            }

        }
    }

    fun nonBG(activeDeliveryDos: ArrayList<ActiveDeliveryDO>) {


        var recievedSite = preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID, "")
        var vehicleCode = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_CODE, "")

        val customer = preferenceUtils.getStringFromPreference(PreferenceUtils.CUSTOMER, "")
        val returnDate = CalendarUtils.getDate()

        val siteListRequest = CreateNONBGRequest(recievedSite, customer, vehicleCode, 1, activeDeliveryDos, this@NonBGActivity)
        siteListRequest.setOnResultListener { isError, loanReturnMainDo ->
            hideLoader()

            if (isError) {
                showAppCompatAlert("", "Error in Service Response, Please try again", "Ok", "Cancel", "", false)
                isSentForApproval = false
            } else {
                if (loanReturnMainDo != null) {
                    if (loanReturnMainDo.status == 20) {
                        showToast("Updated Successfully...")
                        val intent = Intent(this@NonBGActivity, SignatureActivity::class.java);
                        startActivityForResult(intent, 11)
                    } else if (loanReturnMainDo.status == 10) {
                        isSentForApproval = false
                        showToast(loanReturnMainDo.message)
                    }
                } else {
                    showToast("Unable to Create Non Bg")
                }

            }

        }
        siteListRequest.execute()


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 11 && resultCode == 11) {
            val intent = Intent();
            intent.putExtra("CapturedReturns", true)
            // AppConstants.CapturedReturns= true
            setResult(11, intent)
            finish()
        }else{
            finish()
        }


    }

    override fun onBackPressed() {
        if (isSentForApproval) {
            showToast("Please confirm..!")

        }

    }
}