package com.tbs.brothersgas.haadhir.decanting

import android.content.Intent
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.brothersgas.haadhir.Adapters.AddressAdapter
import com.tbs.brothersgas.haadhir.Adapters.InvoiceAdapter
import com.tbs.brothersgas.haadhir.Model.CustomerDo
import com.tbs.brothersgas.haadhir.Model.LoadStockDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.AddressListRequest
import com.tbs.brothersgas.haadhir.Requests.InvoiceHistoryRequest
import com.tbs.brothersgas.haadhir.common.AppConstants
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import java.util.ArrayList
import androidx.core.os.HandlerCompat.postDelayed
import com.tbs.brothersgas.haadhir.Activitys.BaseActivity
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.utils.CalendarUtils
import java.util.logging.Handler


class DecantingAddressListActivity : BaseActivity() {
    lateinit var invoiceAdapter: AddressAdapter
    lateinit var loadStockDOs: ArrayList<LoadStockDO>
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var tvNoDataFound: TextView
    var fromId = 0
    lateinit var code: String
    lateinit var customer: String
    lateinit var customerDo: CustomerDo
    lateinit var tvColorFlag : TextView

    override protected fun onResume() {
        initializeControls()
        super.onResume()
    }

    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.address_list, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
//        initializeControls()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
    }

    override fun initializeControls() {
        tvScreenTitle.setText("Address List")
        recycleview = findViewById(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        tvNoDataFound = findViewById(R.id.tvNoDataFound) as TextView
        tvColorFlag = findViewById(R.id.tvColorFlag) as TextView
        tvColorFlag.visibility=View.GONE
        recycleview.setLayoutManager(linearLayoutManager)
        recycleview!!.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this@DecantingAddressListActivity)
        if (intent.hasExtra("CODE")) {
            customer = intent.extras!!.getString("CODE")!!
        } else {
            customer = ""
        }
        if (intent.hasExtra("CUSTOMERDO")) {
            customerDo = intent.extras!!.getSerializable("CUSTOMERDO") as CustomerDo
        }
        if (Util.isNetworkAvailable(this)) {

            val addressListRequest = DecantingAddressListRequest(customer, this@DecantingAddressListActivity)
            addressListRequest.setOnResultListener { isError, invoiceHistoryMainDO ->
                hideLoader()
                if (isError) {
                    tvNoDataFound.setVisibility(View.VISIBLE)
                    recycleview.setVisibility(View.GONE)
                    Toast.makeText(this@DecantingAddressListActivity, R.string.error_NoData, Toast.LENGTH_SHORT).show()
                } else {
                    if (invoiceHistoryMainDO.addressDOS.size > 0) {
                        tvNoDataFound.setVisibility(View.GONE)

                        invoiceAdapter = AddressAdapter(this@DecantingAddressListActivity, invoiceHistoryMainDO.addressDOS, "DECANTING", customer, customerDo)
                        recycleview!!.adapter = invoiceAdapter
                    } else {
                        tvNoDataFound.setVisibility(View.VISIBLE)
                        recycleview.setVisibility(View.GONE)
                    }
                }
            }
            addressListRequest.execute()
        } else {
            showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == 1) {
            finish()
        }
        else if (requestCode == 63 && resultCode == 63) {
            finish()
        }
        else if (requestCode == 63 && resultCode == 52) {
            finish()
        }
        else if (requestCode == 63 && resultCode == 19) {
            if (data != null && data.hasExtra("CapturedReturns") && data.getBooleanExtra("CapturedReturns", false)) {

                val intent = Intent()
                intent.putExtra("CapturedReturns", AppConstants.CapturedReturns)
                setResult(19, intent)
                finish()
                // uncomment if required
            }
        }
    }

    override fun onButtonYesClick(from: String) {

        if ("SUCCESS".equals(from, ignoreCase = true)) {
            finish()

        } else
            if ("FAILURE".equals(from, ignoreCase = true)) {
            }

    }

    override fun onButtonNoClick(from: String) {
        if ("CREDIT_SUCCESS".equals(from, ignoreCase = true)) {
            finish()
        }
        super.onButtonNoClick(from)
    }
}