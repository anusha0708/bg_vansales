package com.tbs.brothersgas.haadhir.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class UserActivityReportMainDO implements Serializable {

    public Double totalAmount                        = 0.0;
    public String userName                           = "";
    public String transactionDate                    = "";
    public String currency                           = "";
    public ArrayList<SpotDeliveryDO> spotDeliveryDOS = new ArrayList<>();
    public ArrayList<SalesInvoiceDO> salesInvoiceDOS = new ArrayList<>();
    public ArrayList<CashDO> cashDOS                 = new ArrayList<>();
    public ArrayList<ChequeDO> chequeDOS             = new ArrayList<>();
    public ArrayList<LoadStockDO> productDOS         = new ArrayList<>();
    public ArrayList<LoadStockDO> stockDOS           = new ArrayList<>();
    public ArrayList<LoadStockDO> salesDOS         = new ArrayList<>();
    public ArrayList<LoadStockDO> issueDOS         = new ArrayList<>();
    public ArrayList<LoadStockDO> recieptDOS         = new ArrayList<>();

    public Double deliveryTotalQty                   = 0.0;
    public Double invoiceTotalQuantity               = 0.0;
    public Double invoiceTotalAmount                 = 0.0;
    public Double cashTotalAmount                    = 0.0;
    public Double chequeTotalAmount                  = 0.0;
    public Double productsTotalQty                   = 0.0;
    public Double stockTotalQty                      = 0.0;
    public Double salesTotalQty                      = 0.0;
    public Double issueTotalQty                      = 0.0;
    public Double recieptTotalQty                      = 0.0;

    public int flag                                  = 0;
    public String message                            = "";
    public String companyCode                           = "";
    public String companyDescription                           = "";



}
