package com.tbs.brothersgas.haadhir.Model;

import java.io.Serializable;

public class CylinderIssueDO implements Serializable {

    public String product = "";
    public String productDescription = "";
    public int orderedQuantity = 0;
    public String orderedQuantityunit = "";
    public int issuedQuantity = 0;
    public String issuedQuantityunit = "";
    public int recievedQuantity = 0;
    public String recievedQuantityunit = "";
    public int balanceQuantity = 0;
    public String balanceQuantityunit = "";

}
