package com.tbs.brothersgas.haadhir.collector

import android.app.DatePickerDialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.brothersgas.haadhir.Activitys.BaseActivity
import com.tbs.brothersgas.haadhir.Adapters.InvoiceAdapter
import com.tbs.brothersgas.haadhir.Model.LoadStockDO
import com.tbs.brothersgas.haadhir.Model.PaymentPdfDO
import com.tbs.brothersgas.haadhir.Model.TransactionCashMainDo
import com.tbs.brothersgas.haadhir.Model.TransactionChequeMainDo
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.PaymentChequeTransactionListRequest
import com.tbs.brothersgas.haadhir.Requests.PaymentCashTransactionListRequest
import com.tbs.brothersgas.haadhir.common.AppConstants
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.pdfs.PaymentReceiptPDF
import com.tbs.brothersgas.haadhir.prints.PrinterConnection
import com.tbs.brothersgas.haadhir.prints.PrinterConstants
import com.tbs.brothersgas.haadhir.utils.CalendarUtils
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import java.io.Serializable
import java.util.*
import kotlin.collections.ArrayList


class TransactionListActivity : BaseActivity() {
    lateinit var invoiceAdapter: InvoiceAdapter
    lateinit var loadStockDOs: ArrayList<LoadStockDO>
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var recycleview2: androidx.recyclerview.widget.RecyclerView
    lateinit var btnCreate: Button
    private var transactionCashMainDO: TransactionCashMainDo = TransactionCashMainDo()
    private var transactionChequeMainDO: TransactionChequeMainDo = TransactionChequeMainDo()

    lateinit var tvNoDataFound: TextView
    var fromId = 0
    var startDate = ""
    var endDate = ""
    var from = ""
    var to = ""
    lateinit var tvTotalAmount: TextView
    lateinit var tvCashReciepts: TextView
    lateinit var tvChequeReciepts: TextView
    lateinit var tvTotalChequeAmount: TextView
    private lateinit var siteListRequestCash: PaymentCashTransactionListRequest

    private var cday: Int = 0
    private var cmonth: Int = 0
    private var cyear: Int = 0
    lateinit var tvFrom: TextView
    lateinit var tvTO: TextView
    lateinit var tvUserId: TextView
    lateinit var tvUserName: TextView

    lateinit var llItem1: LinearLayout
    lateinit var llItem2: LinearLayout
    lateinit var llCash: LinearLayout
    lateinit var llCheque: LinearLayout
    lateinit var llCashAmount: LinearLayout
    lateinit var llChequeAmount: LinearLayout
    lateinit var llUserId: LinearLayout
    lateinit var llName: LinearLayout
    lateinit var view1: View
    lateinit var view2: View
    lateinit var view3: View
    lateinit var view4: View
    lateinit var view5: View
    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.transactions_list, null) as View
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
    }

    override fun initializeControls() {
        val name = preferenceUtils.getStringFromPreference(PreferenceUtils.LOGIN_DRIVER_NAME, "")
        val id = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "")

        tvScreenTitle.setText(name)
        recycleview = findViewById(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        recycleview2 = findViewById(R.id.recycleview2) as androidx.recyclerview.widget.RecyclerView
        recycleview.setNestedScrollingEnabled(false);
        recycleview2.setNestedScrollingEnabled(false);
        view1 = findViewById(R.id.view1) as View
        view2 = findViewById(R.id.view2) as View
        view3 = findViewById(R.id.view3) as View
        view4 = findViewById(R.id.view4) as View
        view5 = findViewById(R.id.view5) as View

        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        tvNoDataFound = findViewById(R.id.tvNoData) as TextView
        tvFrom = findViewById(R.id.tvFrom) as TextView
        tvTO = findViewById(R.id.tvTO) as TextView
        tvTotalAmount = findViewById(R.id.tvTotalAmount) as TextView
        tvCashReciepts = findViewById(R.id.tvCashReciepts) as TextView
        tvChequeReciepts = findViewById(R.id.tvChequeReciepts) as TextView
        tvTotalChequeAmount = findViewById(R.id.tvTotalChequeAmount) as TextView
        tvUserId = findViewById(R.id.tvUserId) as TextView
        tvUserName = findViewById(R.id.tvUserName) as TextView
        tvUserId.setText(id)
        tvUserName.setText(name)
        llUserId = findViewById(R.id.llUserId) as LinearLayout
        llName = findViewById(R.id.llName) as LinearLayout
        llItem1 = findViewById(R.id.llItem) as LinearLayout
        llItem2 = findViewById(R.id.llItem2) as LinearLayout
        llCash = findViewById(R.id.llCash) as LinearLayout
        llCheque = findViewById(R.id.llCheque) as LinearLayout
        llCashAmount = findViewById(R.id.llCashAmount) as LinearLayout
        llChequeAmount = findViewById(R.id.llChequeAmount) as LinearLayout
        var from = CalendarUtils.getDate()

        val aMonth = from.substring(4, 6)
        val ayear = from.substring(0, 4)
        val aDate = from.substring(Math.max(from.length - 2, 0))


        tvFrom.setText("" + aDate + " - " + aMonth + " - " + ayear)
        tvTO.setText("" + aDate + " - " + aMonth + " - " + ayear)
        startDate = ayear + aMonth + aDate
        endDate = ayear + aMonth + aDate

        if (intent.hasExtra("Sales")) {
            fromId = intent.extras.getInt("Sales")
        }
        if (intent.hasExtra("START")) {
            startDate = intent.extras.getString("START")
        }
        if (intent.hasExtra("END")) {
            endDate = intent.extras.getString("END")
        }
        if (intent.hasExtra("FROM")) {
            from = intent.extras.getString("FROM")
        }
        if (intent.hasExtra("TO")) {
            to = intent.extras.getString("TO")
        }
//        tvFrom.setText("From : "+from)
//        tvTO.setText("To : "+to)
        recycleview.setLayoutManager(linearLayoutManager)


        btnCreate = findViewById(R.id.btnCreate) as Button
        val btnSearch = findViewById(R.id.btnSearch) as Button

        btnSearch.setOnClickListener {
            transactionCashMainDO.transactionCashDOS.clear()
            transactionChequeMainDO.transactionChequeDOS.clear()
            var frm = tvFrom.text.toString().trim();
            var to = tvTO.text.toString().trim()
            if (frm.isNotEmpty() && to.isNotEmpty()) {

                selectInvoiceList()
            } else {
                showToast("Please select From and To dates")
            }
        }
        val c = Calendar.getInstance()
        val cyear = c.get(Calendar.YEAR)
        val cmonth = c.get(Calendar.MONTH)
        val cday = c.get(Calendar.DAY_OF_MONTH)
        tvFrom.setOnClickListener {
            val datePicker = DatePickerDialog(this@TransactionListActivity, datePickerListener, cyear, cmonth, cday)

            datePicker.show()
        }

        tvTO.setOnClickListener {
            val datePicker = DatePickerDialog(this@TransactionListActivity, endDatePickerListener, cyear, cmonth, cday)

            datePicker.show()
        }

        btnCreate.setOnClickListener {

            prepareActivityReport()
        }

    }

    private fun selectInvoiceList() {


//        var tvInvoiceId = findViewById(R.id.tvInvoiceId) as TextView
        var tvNoData = findViewById(R.id.tvNoData) as TextView


        recycleview.setLayoutManager(androidx.recyclerview.widget.LinearLayoutManager(this));
        recycleview2.setLayoutManager(androidx.recyclerview.widget.LinearLayoutManager(this));

        if (Util.isNetworkAvailable(this)) {
            siteListRequestCash = PaymentCashTransactionListRequest(startDate, endDate, this@TransactionListActivity)

            siteListRequestCash.setOnResultListener { isError, unPaidInvoiceMainDO ->
                hideLoader()
                if (unPaidInvoiceMainDO != null && unPaidInvoiceMainDO.transactionCashDOS.size > 0) {

                    if (isError) {
                        recycleview.setVisibility(View.GONE)
                        llItem1.setVisibility(View.GONE)
                        llCash.setVisibility(View.GONE)
                        llCashAmount.setVisibility(View.GONE)
                        view1.setVisibility(View.GONE)
                        view2.setVisibility(View.GONE)
                        view3.setVisibility(View.GONE)
                        if (transactionChequeMainDO.transactionChequeDOS.size > 0) {
                            btnCreate.setVisibility(View.VISIBLE)
                            tvNoData.setVisibility(View.GONE)
                        }

//                    if(btnCreate.visibility != View.VISIBLE){
//                        btnCreate.setVisibility(View.GONE)
//                    }
                        Toast.makeText(this@TransactionListActivity, resources.getString(R.string.error_NoData), Toast.LENGTH_SHORT).show()
                    } else {
                        transactionCashMainDO = unPaidInvoiceMainDO
                        llItem1.setVisibility(View.VISIBLE)
                        llCash.setVisibility(View.VISIBLE)
                        llCashAmount.setVisibility(View.VISIBLE)
                        view1.setVisibility(View.VISIBLE)
                        view2.setVisibility(View.VISIBLE)
                        view3.setVisibility(View.VISIBLE)
                        recycleview.setVisibility(View.VISIBLE)
                        btnCreate.setVisibility(View.VISIBLE)
                        tvNoData.setVisibility(View.GONE)
                        var siteAdapter = TransactionPaymentAdapter(this@TransactionListActivity, unPaidInvoiceMainDO.transactionCashDOS, "")
                        recycleview.setAdapter(siteAdapter)
                        tvCashReciepts.setText("" + unPaidInvoiceMainDO.cashReciepts)
                        tvTotalAmount.setText("" + unPaidInvoiceMainDO.totalAmount + " AED")
                        llName.setVisibility(View.VISIBLE)
                        llUserId.setVisibility(View.VISIBLE)

                    }
                } else {

                    recycleview.setVisibility(View.GONE)
                    llItem1.setVisibility(View.GONE)
                    llCash.setVisibility(View.GONE)
                    llCashAmount.setVisibility(View.GONE)
                    view1.setVisibility(View.GONE)
                    view2.setVisibility(View.GONE)
                    view3.setVisibility(View.GONE)
                    if (transactionChequeMainDO.transactionChequeDOS.size > 0) {
                        btnCreate.setVisibility(View.VISIBLE)
                        tvNoData.setVisibility(View.GONE)
                    } else {
                        btnCreate.setVisibility(View.GONE)
                        tvNoData.setVisibility(View.VISIBLE)
                    }
//                if(btnCreate.visibility != View.VISIBLE){
//                    btnCreate.setVisibility(View.GONE)
//                }
                    hideLoader()

                }

            }

            siteListRequestCash.execute()
        } else {
//            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "",false)
            showToast("" + resources.getString(R.string.internet_connection))
        }

        if (Util.isNetworkAvailable(this)) {
            var siteListRequet = PaymentChequeTransactionListRequest(startDate, endDate, this@TransactionListActivity)

            siteListRequet.setOnResultListener { isError, unPaidInvoiceMainDO ->
                hideLoader()
                if (unPaidInvoiceMainDO != null && unPaidInvoiceMainDO.transactionChequeDOS.size > 0) {

                    if (isError) {
                        recycleview2.setVisibility(View.GONE)
                        view4.setVisibility(View.GONE)
                        view5.setVisibility(View.GONE)
                        if (transactionCashMainDO.transactionCashDOS.size > 0) {
                            btnCreate.setVisibility(View.VISIBLE)
                            tvNoData.setVisibility(View.GONE)
                        }
                        llCheque.setVisibility(View.GONE)
                        llItem2.setVisibility(View.GONE)
                        llChequeAmount.setVisibility(View.GONE)


                        Toast.makeText(this@TransactionListActivity, resources.getString(R.string.error_NoData), Toast.LENGTH_SHORT).show()
                    } else {
                        transactionChequeMainDO = unPaidInvoiceMainDO
                        tvNoData.setVisibility(View.GONE)
                        view4.setVisibility(View.VISIBLE)
                        view5.setVisibility(View.VISIBLE)
                        btnCreate.setVisibility(View.VISIBLE)
                        llCheque.setVisibility(View.VISIBLE)
                        llItem2.setVisibility(View.VISIBLE)
                        llChequeAmount.setVisibility(View.VISIBLE)
                        recycleview2.setVisibility(View.VISIBLE)
                        var siteAdapter = TransactionChequePaymentAdapter(this@TransactionListActivity, unPaidInvoiceMainDO.transactionChequeDOS, "")
                        recycleview2.setAdapter(siteAdapter)
                        tvChequeReciepts.setText("" + unPaidInvoiceMainDO.chequeReciepts)
                        tvTotalChequeAmount.setText("" + unPaidInvoiceMainDO.totalAmount + " AED")
                        llName.setVisibility(View.VISIBLE)
                        llUserId.setVisibility(View.VISIBLE)

                    }
                } else {

                    recycleview2.setVisibility(View.GONE)
                    view4.setVisibility(View.GONE)
                    view5.setVisibility(View.GONE)

                    llCheque.setVisibility(View.GONE)
                    llItem2.setVisibility(View.GONE)
                    llChequeAmount.setVisibility(View.GONE)
                    if (transactionCashMainDO.transactionCashDOS.size > 0) {
                        btnCreate.setVisibility(View.VISIBLE)
                        tvNoData.setVisibility(View.GONE)
                    } else {
                        btnCreate.setVisibility(View.GONE)
                        tvNoData.setVisibility(View.VISIBLE)
                    }
                    hideLoader()

                }

            }

            siteListRequet.execute()
        } else {
            showToast("" + resources.getString(R.string.internet_connection))
//            showAppCompatAlert("Alert!", , "OK", "", "",false)

        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == 1) {
//            selectInvoiceList()
        }
    }


    private val datePickerListener: DatePickerDialog.OnDateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        cday = dayOfMonth
        cmonth = monthOfYear + 1
        cyear = year
        var monthString = cmonth.toString()
        if (monthString.length == 1) {
            monthString = "0$monthString"
        } else {
            monthString = cmonth.toString()
        }

        var dayString = cday.toString()
        if (dayString.length == 1) {
            dayString = "0$dayString"
        } else {
            dayString = cday.toString()
        }
        startDate = "" + cyear + monthString + dayString;
        tvFrom.setText("" + dayString + " - " + monthString + " - " + cyear)
        from = tvFrom.text.toString()

    }
    private val endDatePickerListener: DatePickerDialog.OnDateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        cday = dayOfMonth
        cmonth = monthOfYear + 1
        cyear = year
        var monthString = cmonth.toString()
        if (monthString.length == 1) {
            monthString = "0$monthString"
        } else {
            monthString = cmonth.toString()
        }

        var dayString = cday.toString()
        if (dayString.length == 1) {
            dayString = "0$dayString"
        } else {
            dayString = cday.toString()
        }
        endDate = "" + cyear + monthString + dayString;
        tvTO.setText("" + dayString + " - " + monthString + " - " + cyear)
        to = tvTO.text.toString()

    }

    private fun prepareActivityReport() {

        val objLlist = ArrayList<Serializable>();
        objLlist.add(transactionCashMainDO);
        objLlist.add(transactionChequeMainDO)
        AppConstants.Trans_From_Date = tvFrom.text.toString().trim().replace("-", "/");
        AppConstants.Trans_To_Date = tvTO.text.toString().trim().replace("-", "/");
        printDocument(objLlist, PrinterConstants.PrintActivityReport)

    }


    private fun printDocument(obj: Any, from: Int) {
        val printConnection = PrinterConnection.getInstance(this@TransactionListActivity)
        if (printConnection.isBluetoothEnabled()) {
            printConnection.connectToPrinter(obj, from)
        } else {
//            PrinterConstants.PRINTER_MAC_ADDRESS = ""
            showToast("Please enable your mobile Bluetooth.")
//            showAppCompatAlert("", "Please enable your mobile Bluetooth.", "Enable", "Cancel", "EnableBluetooth", false)
        }
    }

    private fun pairedDevices() {
        val pairedDevices = BluetoothAdapter.getDefaultAdapter().getBondedDevices()
        if (pairedDevices.size > 0) {
            for (device in pairedDevices) {
                val deviceName = device.getName()
                val mac = device.getAddress() // MAC address
//                if (StorageManager.getInstance(this).getPrinterMac(this).equals("")) {
                StorageManager.getInstance(this).savePrinterMac(this, mac);
//                }
                Log.e("Bluetooth", "Name : " + deviceName + " Mac : " + mac)
                break
            }
        }
    }

    override fun onResume() {
        super.onResume()
        pairedDevices()
    }

    override fun onStart() {
        super.onStart()
        val filter = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        registerReceiver(mReceiver, filter)
//        val filter = IntentFilter(Intent.PAIRIN.ACTION_FOUND Intent. "android.bluetooth.device.action.PAIRING_REQUEST");
//        registerReceiver(mReceiver, filter)
//        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
//        mBluetoothAdapter.startDiscovery()
//        val filter2 = IntentFilter( "android.bluetooth.device.action.PAIRING_REQUEST")
//        registerReceiver(mReceiver, filter2)

    }

    override fun onDestroy() {
        unregisterReceiver(mReceiver)
//        PrinterConstants.PRINTER_MAC_ADDRESS = ""
        super.onDestroy()
    }

    private val mReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            if (BluetoothDevice.ACTION_FOUND == action) {
                val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE);
                PrinterConstants.bluetoothDevices.add(device)
                Log.e("Bluetooth", "Discovered => name : " + device.name + ", Mac : " + device.address)
            } else if (action.equals(BluetoothDevice.ACTION_ACL_CONNECTED)) {
                Log.e("Bluetooth", "status : ACTION_ACL_CONNECTED")
                pairedDevices()
            } else if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                val state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);

                if (state == BluetoothAdapter.STATE_OFF) {
                    Log.e("Bluetooth", "status : STATE_OFF")
                } else if (state == BluetoothAdapter.STATE_TURNING_OFF) {
                    Log.e("Bluetooth", "status : STATE_TURNING_OFF")
                } else if (state == BluetoothAdapter.STATE_ON) {
                    Log.e("Bluetooth", "status : STATE_ON")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_TURNING_ON) {
                    Log.e("Bluetooth", "status : STATE_TURNING_ON")
                } else if (state == BluetoothAdapter.STATE_CONNECTING) {
                    Log.e("Bluetooth", "status : STATE_CONNECTING")
                } else if (state == BluetoothAdapter.STATE_CONNECTED) {
                    Log.e("Bluetooth", "status : STATE_CONNECTED")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_DISCONNECTED) {
                    Log.e("Bluetooth", "status : STATE_DISCONNECTED")
                }
            }
        }
    }

    private fun createPaymentPDF(createPDFInvoiceDO: PaymentPdfDO) {
        if (createPDFInvoiceDO != null) {
            PaymentReceiptPDF(this).createReceiptPDF(createPDFInvoiceDO, "Email")
            return

        } else {
            //Display error message
        }
    }
}