package com.tbs.brothersgas.haadhir.Activitys

import android.content.Intent
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.tbs.brothersgas.haadhir.Adapters.LoanReturnAdapter
import com.tbs.brothersgas.haadhir.Model.LoadStockDO
import com.tbs.brothersgas.haadhir.Model.LoanReturnDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.LoanReturnRequest
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util

//
class CaptureReturnDetailsActivity : BaseActivity() {
    lateinit var loanReturnAdapter: LoanReturnAdapter
    lateinit var loadStockDOs: ArrayList<LoadStockDO>
    lateinit var recycleview : androidx.recyclerview.widget.RecyclerView
    lateinit var tvNoDataFound:TextView
    lateinit var tvOpeningQty:TextView
    lateinit var tvIssuedQty:TextView
    private var openingQty: String? = ""
    private var issuedQty: Int? = 0

    override fun initialize() {
      val  llCategories = getLayoutInflater().inflate(R.layout.capture_return_details, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, Gravity.LEFT)
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
        tvScreenTitle.setText("Loan Return Details")
        if(intent.hasExtra("IssuedQty"))
            issuedQty = intent.extras.getInt("IssuedQty", 0)
        initializeControls()

        tvIssuedQty.setText("Issued Qty : "+issuedQty)
    }

    override fun initializeControls() {

        recycleview             = findViewById(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        tvNoDataFound           = findViewById(R.id.tvNoDataFound) as TextView
        tvOpeningQty            = findViewById(R.id.tvOpeningQty) as TextView
        tvIssuedQty             = findViewById(R.id.tvIssuedQty) as TextView
        val tvCustomerName = findViewById<View>(R.id.tvCustomerName) as TextView
        val btnConfirm      = findViewById(R.id.btnConfirm) as Button
        val linearLayoutManager     = androidx.recyclerview.widget.LinearLayoutManager(this, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
        recycleview.setLayoutManager(linearLayoutManager)

        tvCustomerName.setText("Customer : "+preferenceUtils.getStringFromPreference(PreferenceUtils.CUSTOMER,""))
        if (Util.isNetworkAvailable(this)) {

            val driverListRequest = LoanReturnRequest(this@CaptureReturnDetailsActivity)
            driverListRequest.setOnResultListener { isError, loanReturnMainDO ->
                hideLoader()
                if (isError) {
                    tvNoDataFound.setVisibility(View.VISIBLE)
                    recycleview.setVisibility(View.GONE)
                    btnConfirm.setVisibility(View.GONE)
                    showToast(resources.getString(R.string.error_NoData))
                } else {
                    openingQty = loanReturnMainDO.openingQty.toString();
                    val loanReturnDOsMap = LinkedHashMap<String, ArrayList<LoanReturnDO>>();
                    var shipmentIds = ArrayList<String>()
                    if(loanReturnMainDO!=null && loanReturnMainDO.loanReturnDOS.size>0){
                        for (i in loanReturnMainDO.loanReturnDOS.indices){
                            if(!shipmentIds.contains(loanReturnMainDO.loanReturnDOS.get(i).shipmentNumber)){
                                shipmentIds.add(loanReturnMainDO.loanReturnDOS.get(i).shipmentNumber)
                            }
                        }
                    }

                    if(shipmentIds.size>0){
                        for (j in shipmentIds.indices){//4
                            var loanReturnDOS = ArrayList<LoanReturnDO>()
                            for (i in loanReturnMainDO.loanReturnDOS.indices) {//10
                                if (loanReturnMainDO.loanReturnDOS.get(i).shipmentNumber.equals(shipmentIds.get(j))) {
                                    loanReturnDOS.add(loanReturnMainDO.loanReturnDOS.get(i))
                                }
                            }
                            loanReturnDOsMap.put(shipmentIds.get(j), loanReturnDOS);
                        }
                    }

                    if(loanReturnDOsMap.size>0){
                        tvNoDataFound.setVisibility(View.GONE)
                        recycleview.setVisibility(View.VISIBLE)
                        btnConfirm.setVisibility(View.VISIBLE)
                        loanReturnAdapter = LoanReturnAdapter(this@CaptureReturnDetailsActivity, loanReturnDOsMap,"Products")
                        recycleview.adapter = loanReturnAdapter
                        tvOpeningQty.setText("Opening Qty : "+loanReturnMainDO.openingQty)
                    }else{
                        tvNoDataFound.setVisibility(View.VISIBLE)
                        recycleview.setVisibility(View.GONE)
                        btnConfirm.setVisibility(View.GONE)
                    }


                }
            }
            driverListRequest.execute()
        } else {
            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "",false)

        }


        btnConfirm.setOnClickListener {


            if(loanReturnAdapter!=null){
                val loanReturnDos = loanReturnAdapter.getSelectedLoadStockDOs();
                if(loanReturnDos!=null && !loanReturnDos.isEmpty()){
                    val intent = Intent(this@CaptureReturnDetailsActivity, SelectedReturnDetailsActivity::class.java);
                    intent.putExtra("Products", loanReturnDos);
                    intent.putExtra("openingQty", openingQty);
                    intent.putExtra("IssuedQty", issuedQty);
                    startActivityForResult(intent, 11);
                }
                else{
                    showToast("Please select items")
                }
            }
            else{
                showToast("No Return Items found")
            }

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 11 && resultCode == 11){
            val intent = Intent();
            intent.putExtra("CapturedReturns", true)
           // AppConstants.CapturedReturns= true
            setResult(11, intent)
            finish()
        }


    }
}