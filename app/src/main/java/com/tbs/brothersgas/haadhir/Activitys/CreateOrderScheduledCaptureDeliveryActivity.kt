package com.tbs.brothersgas.haadhir.Activitys

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.View
import android.view.View.*
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import com.tbs.brothersgas.haadhir.Adapters.CreateOrderScheduledProductsAdapter
import com.tbs.brothersgas.haadhir.Adapters.DamageReasonListAdapter
import com.tbs.brothersgas.haadhir.Adapters.ReasonListAdapter
import com.tbs.brothersgas.haadhir.Adapters.ScheduledProductsAdapter
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryDO
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryMainDO
import com.tbs.brothersgas.haadhir.Model.CustomerDo
import com.tbs.brothersgas.haadhir.Model.ReasonMainDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.*
import com.tbs.brothersgas.haadhir.common.AppConstants
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.pdfs.BulkDeliveryNotePDF
import com.tbs.brothersgas.haadhir.pdfs.CYLDeliveryNotePDF
import com.tbs.brothersgas.haadhir.utils.CalendarUtils
import com.tbs.brothersgas.haadhir.utils.LogUtils
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import java.util.*


//
class CreateOrderScheduledCaptureDeliveryActivity : BaseActivity() {


    lateinit var reasonMainDo: ReasonMainDO
    private var shipmentProductsList: ArrayList<ActiveDeliveryDO>? = ArrayList()
    private var cloneShipmentProductsList: ArrayList<ActiveDeliveryDO> = ArrayList()
    private var type: Int = 1
    lateinit var shipmentProductsType: String
    lateinit var customerDo: CustomerDo
    lateinit var activeDeliverySavedDo: ActiveDeliveryMainDO

    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var btnRequestApproval: Button
    lateinit var btnConfirm: Button
    lateinit var tvNoDataFound: TextView
    lateinit var tvShipmentId: TextView
    lateinit var tvPaymentTerm: TextView

    lateinit var tvApprovalStatus: TextView
    private var shipmentType: String = ""
    lateinit var cbNonBGSelected: CheckBox
    var status = "";
    var customerId = "";
    var foc = "";

    lateinit var rbNormal: RadioButton
    lateinit var rbLoan: RadioButton
    lateinit var rgType: RadioGroup
    lateinit var dialog: Dialog
    private var shipmentId: String = ""
    private var loadStockAdapter: CreateOrderScheduledProductsAdapter? = null
    private var reasonMainDO: ReasonMainDO = ReasonMainDO()
    var nonBgType = 0

    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.create_order_scheduled_capture_delivery, null) as LinearLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        disableMenuWithBackButton()
        toolbar.setNavigationIcon(R.drawable.back)
        hideKeyBoard(llBody)
        toolbar.setNavigationOnClickListener {

            if (isSentForApproval) {
                showToast("Please process the order..!")
            } else {
                var data = tvApprovalStatus.text.toString()
                var intent = Intent()
                intent.putExtra("Status", data)
                var args = Bundle();
                args.putSerializable("List", shipmentProductsList)
                intent.putExtra("BUNDLE", args);
                setResult(56, intent)
                finish()
            }
        }

        initializeControls()

        customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)
        activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)

        shipmentProductsType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "")

        if (shipmentProductsType.equals(AppConstants.MeterReadingProduct, true)) {
            rbLoan.isClickable = false
            rbLoan.isEnabled = false
            rbLoan.isSelected = false
            type = 1
            cbNonBGSelected.visibility = GONE


        }
        var approval = tvApprovalStatus.text.toString()
        status = preferenceUtils.getStringFromPreference(PreferenceUtils.PRODUCT_APPROVAL, "")
        if (approval.equals("NA")) {
            tvApprovalStatus.setText("NA")

        } else {
            tvApprovalStatus.setText(status)

        }


        shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
        if (shipmentId.isNotEmpty()) {
            tvShipmentId.visibility = View.VISIBLE
            tvShipmentId.setText(activeDeliverySavedDo.customer + " - " + activeDeliverySavedDo.customerDescription + "\nDelivery number - " + shipmentId)
            tvPaymentTerm.setText("Payment Term - " + activeDeliverySavedDo.paymentTerm)
        } else {
            tvShipmentId.visibility = View.VISIBLE
            tvShipmentId.setText(customerDo.customerId + " - " + customerDo.customerName)
            tvPaymentTerm.setText("Payment Term - " + customerDo.paymentTerm)

        }
        shipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, "")

        if (intent.hasExtra("CustomerId")) {
            customerId = intent.extras.getString("CustomerId")
        }
        tvScreenTitle.setText("Confirm Products")
        rlAdd.setVisibility(View.VISIBLE)

        var podDo = StorageManager.getInstance(this).getDepartureData(this);
        if (podDo.deliveryStatus?.get(0)!!.contains("Requested", true)) {
            tvApprovalStatus.setVisibility(VISIBLE)
            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnConfirm.isClickable = false
            btnConfirm.isEnabled = false
            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnRequestApproval.isClickable = false
            btnRequestApproval.isEnabled = false
            tvApprovalStatus.setText(podDo.deliveryStatus?.get(0))
        } else if (podDo.deliveryStatus?.get(0)!!.contains("Approved", true)) {
            tvApprovalStatus.setVisibility(VISIBLE)

            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnRequestApproval.isClickable = false
            btnRequestApproval.isEnabled = false
            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
            btnConfirm.isClickable = true
            btnConfirm.isEnabled = true
            tvApprovalStatus.setText(podDo.deliveryStatus?.get(0))

        } else {
            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_green))
            btnRequestApproval.isClickable = true
            btnRequestApproval.isEnabled = true
            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
            btnConfirm.isClickable = true
            btnConfirm.isEnabled = true
            tvApprovalStatus.setText(podDo.deliveryStatus?.get(0))
        }
        if (shipmentType.equals(resources.getString(R.string.checkin_scheduled), true)) {
            if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
                rlAdd.setVisibility(View.GONE)
                btnRequestApproval.setVisibility(View.GONE)
                cbNonBGSelected.setVisibility(GONE)
            } else {
                rlAdd.setVisibility(View.VISIBLE)
                cbNonBGSelected.setVisibility(VISIBLE)

            }
        } else if (shipmentType.equals(resources.getString(R.string.checkin_non_scheduled), true)) {
            rlAdd.setVisibility(View.VISIBLE)
            ivRefresh.setVisibility(View.VISIBLE)
        } else {
            rlAdd.setVisibility(View.GONE)
            ivRefresh.setVisibility(View.GONE)
        }
        if (shipmentType.equals(resources.getString(R.string.checkin_scheduled), true)) {
            tvShipmentId.visibility = View.VISIBLE
            tvPaymentTerm.visibility = View.VISIBLE
            if (tvApprovalStatus.text.toString().contains("Requested")) {
                btnConfirm.setText("Confirm")
                btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnConfirm.isClickable = false
                btnConfirm.isEnabled = false
                btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnRequestApproval.isClickable = false
                btnRequestApproval.isEnabled = false
            } else {
                if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
                    btnConfirm.setText("Confirm")
                    btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                    btnConfirm.isClickable = false
                    btnConfirm.isEnabled = false
                    cbNonBGSelected.setVisibility(GONE)
                    btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                    btnRequestApproval.isClickable = false
                    btnRequestApproval.isEnabled = false
                } else {
                    btnConfirm.setText("Confirm")
                    btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
                    btnConfirm.isClickable = true
                    btnConfirm.isEnabled = true
                    cbNonBGSelected.setVisibility(VISIBLE)
                    ivRefresh.visibility = View.VISIBLE
                    btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                    btnRequestApproval.isClickable = false
                    btnRequestApproval.isEnabled = false
                }

            }
            loadScheduleData()

        } else {
            tvShipmentId.visibility = View.GONE
            tvPaymentTerm.visibility = View.GONE
            loadStockAdapter = CreateOrderScheduledProductsAdapter(this, ArrayList(), "", "", type)
            recycleview.adapter = loadStockAdapter;
            btnConfirm.setText("Create Order")
            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnConfirm.isClickable = false
            btnConfirm.isEnabled = false
            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnRequestApproval.isClickable = false
            btnRequestApproval.isEnabled = false
            ivRefresh.setVisibility(View.GONE)
            btnRequestApproval.setVisibility(View.GONE)
        }
        rgType.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {

                val shipmentProductsType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "")

                if (shipmentProductsType.equals(AppConstants.MeterReadingProduct, true)) {
                    rbLoan.isClickable = false
                    rbLoan.isEnabled = false
                    rbLoan.isSelected = false
                    type = 1

                } else {
                    when (checkedId) {
                        R.id.rbNormalyCylinder -> {
                            type = 1
                            loadStockAdapter!!.refreshAdapter(shipmentProductsList, type)
                            ivRefresh.visibility = View.GONE
                            btnRequestApproval.visibility = GONE
                            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
                            btnConfirm.isClickable = true
                            btnConfirm.isEnabled = true
                        }
                        R.id.rbLoanReturn -> {
                            type = 2
                            loadStockAdapter!!.refreshAdapter(shipmentProductsList, type)
                            ivRefresh.visibility = View.GONE
                            btnRequestApproval.visibility = GONE
//                            btnRequestApproval.visibility = VISIBLE
//                            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_green))
//                            btnRequestApproval.isClickable = true
//                            btnRequestApproval.isEnabled = true
//                            ivRefresh.visibility = View.VISIBLE
                            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
                            btnConfirm.isClickable = true
                            btnConfirm.isEnabled = true

                        }

                    }
                }

            }
        })


        ivRefresh.setOnClickListener {
            if (tvApprovalStatus.getText().toString().contains("Requested")) {
                preferenceUtils.saveString(PreferenceUtils.PRODUCT_APPROVAL, tvApprovalStatus.getText().toString())
//                updateStatus()
                approvalMechanismCheckStatus()
            }
        }
        rlAdd.setOnClickListener {
            if (isSentForApproval) {
                showToast("Please process the order..!")
            } else {
                val intent = Intent(this, ScheduledAddProductActivity::class.java);
                intent.putExtra("FROM", "SPOTSALES")
                startActivityForResult(intent, 1);
            }
        }
        cbNonBGSelected?.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                nonBgType = 1
            } else {
                nonBgType = 0
            }
        }
        btnRequestApproval.setOnClickListener {
            var text = btnRequestApproval.text.toString()
            if (text.equals("Check Status")) {
                checkStatus()
            } else {
                if (foc.equals("FOC")) {
                    approvalMechanismRequst("", preferenceUtils.getStringFromPreference(PreferenceUtils.FOC, ""), 8)

                } else {
                    if (shipmentType.equals(resources.getString(R.string.checkin_scheduled), true)) {
                        approvalMechanismRequst("", "", 1)

                    } else {
                        if (type == 2) {
                            approvalMechanismRequst("", "", 3)

                        } else {
                            approvalMechanismRequst("", "", 2)

                        }
                    }
                }
            }


        }

        btnConfirm.setOnClickListener {

            var text = btnConfirm.text.toString()
            if (text.equals("Create Order")) {
                createOrder()
            } else {
                shipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, "")
                if (shipmentType.equals(resources.getString(R.string.checkin_scheduled), true)) {
                    if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {

                        updateQtyToServer("METER")
                        //  updateMeterQtyToServer()
                    } else {
                        updateQtyToServer("")
                    }
                } else {
                    if (tvApprovalStatus.text.toString().contains("Rejected")) {

                        if (type == 2) {
                            customerManagement(customerDo.customerId, 1)

                        } else {
                            showAppCompatAlert("", "Your request was rejected and you are creating" +
                                    " a delivery with the original price, do you want to continue?", "OK", "Cancel", "REJECTSUCCESS", true)


                        }

                    } else {
                        customerManagement(customerDo.customerId, 1)

                    }

                }
            }


        }
    }

    private fun checkStatus() {
     var salesOrderID= preferenceUtils.getStringFromPreference(PreferenceUtils.SALES_ORDER_NUMBER,"")
        val approvalRequest = CheckSalesOrderStatusRequest(salesOrderID,this)
        approvalRequest.setOnResultListener { isError, approvalDO ->
            hideLoader()
            if (approvalDO != null) {
                if (isError) {
                    showAppCompatAlert("Info!", "Error in Service Response, Please try again...", "Ok", "", "", false)
                } else {

                    if (approvalDO.flag == 20) {
                        isSentForApproval = true;
                        tvApprovalStatus.setText("Status : " + approvalDO.status)
                        btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
                        btnConfirm.isClickable = true
                        btnConfirm.isEnabled = true
                        btnConfirm.setText("Create Delivery")
                        btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        btnRequestApproval.isClickable = false
                        btnRequestApproval.isEnabled = false
                    } else {
                        Toast.makeText(this@CreateOrderScheduledCaptureDeliveryActivity, approvalDO.status, Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
        approvalRequest.execute()
    }

    private fun approvalMechanismCheckStatus() {
        hideLoader()
        val approvalRequest = ApprovalMechanismCheckStatusRequest(this)
        approvalRequest.setOnResultListener { isError, approvalDO ->
            hideLoader()
            if (approvalDO != null) {
                if (isError) {
                    showAppCompatAlert("Info!", "Error in Service Response, Please try again...", "Ok", "", "", false)
                } else {
                    preferenceUtils.removeFromPreference(PreferenceUtils.FOC);
//                    approvalDO.status = "Approved"
                    if (approvalDO.flag == 20) {
//                        loadDeliveryData()

                        if (approvalDO.docType == 8) {
                            isSentForApproval = false;
                            tvApprovalStatus.setText("Status : " + approvalDO.status)
                            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
                            btnConfirm.isClickable = true
                            btnConfirm.isEnabled = true
                            loadStockAdapter!!.isClickable = false

                            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                            btnRequestApproval.isClickable = false
                            btnRequestApproval.isEnabled = false
                            for (k in shipmentProductsList!!.indices) {
                                if (shipmentProductsList!!.get(k).reasonFOC.isNotEmpty()) {
                                    shipmentProductsList!!.get(k).priceTag = 0.0
                                    shipmentProductsList!!.get(k).focFlag = 2

                                }
                            }

                        } else {
                            isSentForApproval = true;
                            tvApprovalStatus.setText("Status : " + approvalDO.status)
                            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
                            btnConfirm.isClickable = true
                            btnConfirm.isEnabled = true
                            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                            btnRequestApproval.isClickable = false
                            btnRequestApproval.isEnabled = false
                            loadStockAdapter!!.isClickable = true
                        }


                    } else if (approvalDO.flag == 30) {

//                        rejectProducts()
                        if (approvalDO.docType == 1) {
                            isSentForApproval = false;
                            loadDeliveryData()
                            tvApprovalStatus.setText("Status : " + approvalDO.status)
                            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
                            btnConfirm.isClickable = true
                            btnConfirm.isEnabled = true
                            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                            btnRequestApproval.isClickable = false
                            btnRequestApproval.isEnabled = false
                        } else if (approvalDO.docType == 2) {
                            isSentForApproval = false;
                            loadStockAdapter!!.refreshAdapter(shipmentProductsList, 230)
                            tvApprovalStatus.setText("Status : " + approvalDO.status)
                            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
                            btnConfirm.isClickable = true
                            btnConfirm.isEnabled = true
                            loadStockAdapter!!.isClickable = true
                            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                            btnRequestApproval.isClickable = false
                            btnRequestApproval.isEnabled = false
                            for (k in shipmentProductsList!!.indices) {
                                if (shipmentProductsList!!.get(k).priceTag != null) {
                                    shipmentProductsList!!.get(k).priceTag = 0.0

                                }
                            }
                        } else if (approvalDO.docType == 8) {
                            isSentForApproval = false;
                            tvApprovalStatus.setText("Status : " + approvalDO.status)
                            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
                            btnConfirm.isClickable = true
                            btnConfirm.isEnabled = true
                            loadStockAdapter!!.isClickable = true

                            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                            btnRequestApproval.isClickable = false
                            btnRequestApproval.isEnabled = false
//                            for (k in shipmentProductsList!!.indices) {
//                                if (shipmentProductsList!!.get(k).reasonFOC.isNotEmpty()) {
//                                    shipmentProductsList!!.get(k).priceTag=0.0
//                                }
//                            }
                        } else {
                            isSentForApproval = false;
                            tvApprovalStatus.setText("Status : " + approvalDO.status)
                            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                            btnConfirm.isClickable = false
                            btnConfirm.isEnabled = false
                            loadStockAdapter!!.isClickable = false



                            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                            btnRequestApproval.isClickable = false
                            btnRequestApproval.isEnabled = false
                        }


                    } else {
                        Toast.makeText(this@CreateOrderScheduledCaptureDeliveryActivity, approvalDO.status, Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
        approvalRequest.execute()
    }

    private fun approvalMechanismRequst(rescheduleDate: String, comments: String, docType: Int) {
        if (Util.isNetworkAvailable(this)) {
            var transactionNumber = ""
            var customer = ""

            val appUser = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "")
            val site = preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID, "")
            val routingID = preferenceUtils.getStringFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID, "")
            val transactionID = preferenceUtils.getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, "")
            val stransactionID = preferenceUtils.getStringFromPreference(PreferenceUtils.DOC_NUMBER, "")
            if (shipmentType.equals(getResources().getString(R.string.checkin_non_scheduled), true)) {
                var customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)
                customer = customerDo.customerId
            } else {
                customer = activeDeliverySavedDo.customer
            }
            if (shipmentType == getResources().getString(R.string.checkin_non_scheduled)) {
                transactionNumber = transactionID

            } else {
                transactionNumber = stransactionID

            }

            val approvalMechanismRequst = ApprovalMechanismRequestedRequest(transactionNumber, appUser, customer
                    , site, shipmentId, rescheduleDate, comments, docType, shipmentProductsList, this@CreateOrderScheduledCaptureDeliveryActivity)
            approvalMechanismRequst.setOnResultListener { isError, approveDO ->
                hideLoader()

                if (isError) {
                    Log.e("RequestAproval", "Response : " + isError)
                    showAppCompatAlert("Info!", "Error in Service Response, Please try again...", "Ok", "", "", false)

                } else {
                    preferenceUtils.removeFromPreference(PreferenceUtils.FOC);

                    btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                    btnRequestApproval.isClickable = false
                    btnRequestApproval.isEnabled = false
                    if (approveDO.flag == 20) {
                        tvApprovalStatus.setText("Status : Requested")
                        isSentForApproval = true;
                        btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        btnRequestApproval.isClickable = false
                        btnRequestApproval.isEnabled = false
                        loadStockAdapter!!.isClickable = false
                        rbLoan.isClickable = false
                        rbLoan.isEnabled = false
                        rbNormal.isClickable = false
                        rbNormal.isEnabled = false
                        preferenceUtils.saveString(PreferenceUtils.REQUESTED_NUMBER, approveDO.requestedNumber)

                        preferenceUtils.saveString(PreferenceUtils.CREQUESTED_NUMBER, approveDO.requestedNumber)

                    } else {
                        Toast.makeText(this@CreateOrderScheduledCaptureDeliveryActivity, "" + approveDO.message, Toast.LENGTH_SHORT).show()
                        btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_green))
                        btnRequestApproval.isClickable = true
                        btnRequestApproval.isEnabled = true
                        tvApprovalStatus.setText("Status : Request Not Sent")
                    }

                }

            }
            approvalMechanismRequst.execute()
        } else {
            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "", false)

        }


    }

    private fun customerManagement(id: String, type: Int) {
        if (Util.isNetworkAvailable(this)) {

            val request = CustomerManagementRequest(id, type, this)
            request.setOnResultListener { isError, customerDo ->
                hideLoader()

                if (isError) {
                    isSentForApproval = false

                    showAppCompatAlert("Info!", "Error in Service Response, Please try again...", "Ok", "", "", false)
                } else {
                    if (customerDo != null) {
                        if (customerDo.flag.equals("Customer authorized for this transaction", true)) {
                            createDelivery(id)
                        } else {
                            showAppCompatAlert("Alert!", "Customer not authorized for this transaction", "Ok", "", "", false)
                            isSentForApproval = false
                            preferenceUtils.saveString(PreferenceUtils.DELIVERY_NOTE, "SUCCESS")


                        }
                    } else {
                        isSentForApproval = false

                        showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
                    }
                }

            }
            request.execute()
        } else {
            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "", false)

        }

    }


    private fun createDelivery(id: String) {


        if (Util.isNetworkAvailable(this)) {

            var createDeliveryRequest = CreateDeliveryRequest(0.0,type, shipmentProductsList, this)

            createDeliveryRequest.setOnResultListener { isError, deliveryMainDo, message ->
                hideLoader()
                var createDeliveryMAinDo = deliveryMainDo
                if (isError) {

                    hideLoader()
                    preferenceUtils.saveString(PreferenceUtils.DELIVERY_NOTE, "SUCCESS")

                    isSentForApproval = false
                    showAppCompatAlert("Error", "Delivery Not Created  due to " + message, "Ok", "", "SUCCESS", false)
                } else {
                    if (createDeliveryMAinDo.status == 20) {
                        if (createDeliveryMAinDo.deliveryNumber.length > 0) {
                            preferenceUtils.saveString(PreferenceUtils.SPOT_DELIVERY_NUMBER, createDeliveryMAinDo.deliveryNumber)
                            if (createDeliveryMAinDo.message.length > 0) {
                                showToast("" + createDeliveryMAinDo.message)
                                updateTheProductsInLocal()
                                if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
                                    val intent = Intent(this@CreateOrderScheduledCaptureDeliveryActivity, SignatureActivity::class.java);
                                    startActivityForResult(intent, 11)
                                } else {
                                    if (nonBgType == 1) {
                                        val intent = Intent(this@CreateOrderScheduledCaptureDeliveryActivity, NonBGActivity::class.java);
                                        startActivityForResult(intent, 13)
                                    } else {
                                        val intent = Intent(this@CreateOrderScheduledCaptureDeliveryActivity, SignatureActivity::class.java);
                                        startActivityForResult(intent, 11)
                                    }
                                }


                            }


//                        updateTheProductsInLocal()

                        } else {
                            if (createDeliveryMAinDo.message.length > 0) {
                                showAppCompatAlert("Error", "" + createDeliveryMAinDo.message, "Ok", "", "SUCCESS", false)

                            } else {
                                showAppCompatAlert("Error", "Delivery Not Created ", "Ok", "", "SUCCESS", false)

                            }
                            isSentForApproval = false
                            preferenceUtils.saveString(PreferenceUtils.DELIVERY_NOTE, "SUCCESS")

                        }

//
//                          val driverAdapter = CreateDeliveryProductAdapter(this, deliveryMainDo.nonSheduledProductDOS)
//                          recycleview.setAdapter(driverAdapter)
                    } else {
                        hideLoader()
                        if (createDeliveryMAinDo.message.length > 0) {
                            showAppCompatAlert("Error", "" + createDeliveryMAinDo.message, "Ok", "", "SUCCESS", false)

                        } else {
                            showAppCompatAlert("Error", "Delivery Not Created", "Ok", "", "SUCCESS", false)

                        }
                        isSentForApproval = false
                        preferenceUtils.saveString(PreferenceUtils.DELIVERY_NOTE, "SUCCESS")

                    }
                }
            }
            createDeliveryRequest.execute()
        } else {
            showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

        }

    }

    private fun createOrder() {
        if (Util.isNetworkAvailable(this)) {
            var createDeliveryRequest = CreateOrderRequest(type, shipmentProductsList, this)

            createDeliveryRequest.setOnResultListener { isError, deliveryMainDo, message ->
                hideLoader()
                var createDeliveryMAinDo = deliveryMainDo
                if (isError) {

                    hideLoader()
                    preferenceUtils.saveString(PreferenceUtils.DELIVERY_NOTE, "SUCCESS")

                    isSentForApproval = false
                    showAppCompatAlert("Error", "Sales Order Not Created  due to " + message, "Ok", "", "SUCCESS", false)
                } else {
                    if (createDeliveryMAinDo.status == 20) {
                        if (createDeliveryMAinDo.salesOrderNumber.length > 0) {
                            preferenceUtils.saveString(PreferenceUtils.SALES_ORDER_NUMBER, createDeliveryMAinDo.salesOrderNumber)
                            btnConfirm.setText("Create Delivery")
                            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                            btnConfirm.isClickable = false
                            btnConfirm.isEnabled = false
                            loadStockAdapter!!.isClickable=false
                            btnRequestApproval.setVisibility(View.VISIBLE)
                            btnRequestApproval.setText("Check Status")
                            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_green))
                            btnRequestApproval.isClickable = true
                            btnRequestApproval.isEnabled = true
                            showToast("Sales Order created successfully")
                            rbLoan.isClickable = false
                            rbLoan.isEnabled = false
                            rbNormal.isClickable = false
                            rbNormal.isEnabled = false
                        } else {
                            if (createDeliveryMAinDo.message.length > 0) {
                                showAppCompatAlert("Error", "" + createDeliveryMAinDo.message, "Ok", "", "SUCCESS", false)
                            } else {
                                showAppCompatAlert("Error", "Sales Order Not Created ", "Ok", "", "SUCCESS", false)
                            }
                            isSentForApproval = false
                            preferenceUtils.saveString(PreferenceUtils.DELIVERY_NOTE, "SUCCESS")
                        }
                    } else {
                        hideLoader()
                        if (createDeliveryMAinDo.message.length > 0) {
                            showAppCompatAlert("Error", "" + createDeliveryMAinDo.message, "Ok", "", "SUCCESS", false)

                        } else {
                            showAppCompatAlert("Error", "Sales Order Not Created", "Ok", "", "SUCCESS", false)

                        }
                        isSentForApproval = false
                        preferenceUtils.saveString(PreferenceUtils.DELIVERY_NOTE, "SUCCESS")

                    }
                }
            }
            createDeliveryRequest.execute()
        } else {
            showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

        }

    }

    private fun updateQtyToServer(id: String) {
        if (id.equals("METER")) {
            updateMeterQtyToServer()
        } else {
            requestApproval()
//            updateQtyConfirmationToServer()

        }

    }

    private fun updateQtyConfirmationToServer() {
        if (Util.isNetworkAvailable(this)) {

            val request = ConfirmationQtyRequest(shipmentId, shipmentProductsList, this@CreateOrderScheduledCaptureDeliveryActivity)
            request.setOnResultListener { isError, approveDO ->
                hideLoader()

                if (isError) {
                    isSentForApproval = false

                    showAppCompatAlert("", "Error in Service Response, Please try again", "Ok", "Cancel", "", false)

                } else {
                    if (approveDO != null) {

                        if (approveDO.flag == 2) {
                            updateTheProductsInLocal()
                            showToast("Updated Successfully")
                            if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
                                val intent = Intent(this@CreateOrderScheduledCaptureDeliveryActivity, SignatureActivity::class.java);
                                startActivityForResult(intent, 11)
                            } else {
                                if (nonBgType == 1) {
                                    val intent = Intent(this@CreateOrderScheduledCaptureDeliveryActivity, NonBGActivity::class.java);
                                    startActivityForResult(intent, 13)
                                } else {
                                    val intent = Intent(this@CreateOrderScheduledCaptureDeliveryActivity, SignatureActivity::class.java);
                                    startActivityForResult(intent, 11)
                                }

                            }

                        } else if (approveDO.flag == 1) {
                            updateTheProductsInLocal()
                            showToast("Shipment Already Validated")
                            if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
                                val intent = Intent(this@CreateOrderScheduledCaptureDeliveryActivity, SignatureActivity::class.java);
                                startActivityForResult(intent, 11)
                            } else {
                                if (nonBgType == 1) {
                                    val intent = Intent(this@CreateOrderScheduledCaptureDeliveryActivity, NonBGActivity::class.java);
                                    startActivityForResult(intent, 13)
                                } else {
                                    val intent = Intent(this@CreateOrderScheduledCaptureDeliveryActivity, SignatureActivity::class.java);
                                    startActivityForResult(intent, 11)
                                }
                            }

                        } else {
                            showAppCompatAlert("", "Transaction failed, Please contact support team and try again", "Ok", "Cancel", "", false)
                            isSentForApproval = false

                        }
                    } else {
                        showAppCompatAlert("", "Transaction failed, Please contact support team and try again", "Ok", "Cancel", "", false)
                        isSentForApproval = false

                    }
                }

            }
            request.execute()
        } else {
            showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

        }

    }

    private fun updateMeterQtyToServer() {
        if (Util.isNetworkAvailable(this)) {

            val siteListRequest = ProductsModifyRequest(4, shipmentId, shipmentProductsList, this@CreateOrderScheduledCaptureDeliveryActivity)
            siteListRequest.setOnResultListener { isError, approveDO ->
                hideLoader()
                if (isError) {
                    Log.e("RequestAproval", "Response : " + isError)
                    isSentForApproval = false
                    updateQtyConfirmationToServer()

                } else {
                    if (approveDO != null) {
                        if (approveDO.flag == 4) {
                            updateQtyConfirmationToServer()

                        } else {
                            isSentForApproval = false
                            updateQtyConfirmationToServer()

                        }
                    } else {
                        updateQtyConfirmationToServer()

                    }


                }

            }
            siteListRequest.execute()

//            val request = UpdateRunQtyRequest(shipmentId, shipmentProductsList, this@ScheduledCaptureDeliveryActivity)
//            request.setOnResultListener { isError, approveDO ->
//                hideLoader()
//                //  updateTheProductsInLocal()
//                if (approveDO != null) {
//                    if (isError) {
//
//
//                        showAppCompatAlert("", " please try again.", "Ok", "Cancel", "", false)
//
//                    } else {
//                        if (approveDO != null) {
//                            if (approveDO.flag != null) {
//                                updateQtyConfirmationToServer()
//
////                            updateTheProductsInLocal()
//
//                                showToast("Updated Successfully")
//                            }
//                        } else {
//                            isSentForApproval = false
//
//                        }
//                    }
//                }
//            }
//            request.execute()
//


        } else {
            showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

        }

    }

    private fun updateTheProductsInLocal() {
        try {
            if (loadStockAdapter != null && loadStockAdapter!!.activeDeliveryDOS != null && loadStockAdapter!!.activeDeliveryDOS.size > 0) {

                val deliveryDos = loadStockAdapter!!.activeDeliveryDOS
                StorageManager.getInstance(this@CreateOrderScheduledCaptureDeliveryActivity).saveCurrentDeliveryItems(this, deliveryDos)
                val updatedRows = StorageManager.getInstance(this).updateTheProductsInLocal(loadStockAdapter!!.activeDeliveryDOS);
//                if (updatedRows > 0) {
                val intent = Intent()
                intent.putExtra("DeliveredProducts", loadStockAdapter!!.activeDeliveryDOS)
                intent.putExtra("Status", tvApprovalStatus.text.toString().trim())
                setResult(1, intent)
                finish()
//                } else {
//                    showToast("not confirmed")
//                }
            } else {
                showToast("There are no products to confirm")
            }
        } catch (e: Exception) {
        }
    }

    private fun loadScheduleData() {

        if (Util.isNetworkAvailable(this)) {
            if (shipmentId.length > 0) {
                val driverListRequest = ActiveDeliveryRequest(shipmentId, this)
                driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                    hideLoader()
                    if (isError) {
                        showAppCompatAlert("Info!", "Error in Service Response, Please try again...", "Ok", "", "", false)
                    } else {
                        StorageManager.getInstance(this).saveActiveDeliveryMainDo(this, activeDeliveryDo)
                        shipmentProductsList = activeDeliveryDo.activeDeliveryDOS;
                        if (shipmentProductsList != null && shipmentProductsList!!.size > 0) {
                            cloneShipmentProductsList.addAll(shipmentProductsList!!)
                            loadStockAdapter = CreateOrderScheduledProductsAdapter(this, shipmentProductsList, "", "", type)
                            recycleview.adapter = loadStockAdapter
                            recycleview.visibility = VISIBLE
                            if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
                                cbNonBGSelected.visibility = GONE

                            } else {
                                cbNonBGSelected.visibility = VISIBLE

                            }

                            tvNoDataFound.visibility = GONE
                            tvApprovalStatus.setVisibility(View.VISIBLE)
                            btnRequestApproval.setVisibility(View.VISIBLE)
                            btnConfirm.setVisibility(View.VISIBLE)

                        } else {
                            tvApprovalStatus.setVisibility(View.GONE)
                            btnRequestApproval.setVisibility(View.GONE)
                            btnConfirm.setVisibility(View.GONE)
                            recycleview.visibility = GONE
                            tvNoDataFound.visibility = VISIBLE
                            btnConfirm.isEnabled = false
                            btnConfirm.isClickable = false
                            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        }
                    }
                }
                driverListRequest.execute()


            }
        }


//        }
    }

    public fun deleteShipmentProducts(loadStockDO: ActiveDeliveryDO) {
        this.loadStockDO = loadStockDO;
        showAppCompatAlert("", "Do you want to delete this product from this shipment?", "Delete", "Cancel", "DeleteProduct", false)
    }

    lateinit var loadStockDO: ActiveDeliveryDO;
    override fun onButtonYesClick(from: String) {
        super.onButtonYesClick(from)
        if (from.equals("DeleteProduct", true)) {
            if (shipmentProductsList != null) {
                shipmentProductsList!!.remove(loadStockDO)
                updateConfirmRequestButtons("", shipmentProductsList!!)
                loadStockAdapter = CreateOrderScheduledProductsAdapter(this, shipmentProductsList, "", "", type)
                recycleview.adapter = loadStockAdapter;
//                loadStockAdapter.refreshAdapter(shipmentProductsList)
                showSnackbar("Deleted successfully")
            }
        }
        if (from.equals("REJECTSUCCESS", true)) {
            customerManagement(customerDo.customerId, 1)

        }
    }


    fun updateConfirmRequestButtons(type: String, shipmentProductsList: ArrayList<ActiveDeliveryDO>) {
//        if (!isSentForApproval) {
        foc = ""

        if (shipmentType.equals(resources.getString(R.string.checkin_scheduled), true)) {
            if (isProductsChanged(shipmentProductsList)) {
                btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnConfirm.isClickable = false
                btnConfirm.isEnabled = false
                btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_green))
                btnRequestApproval.isClickable = true
                btnRequestApproval.isEnabled = true
                btnRequestApproval.visibility = VISIBLE
                btnConfirm.visibility = VISIBLE
                tvApprovalStatus.setText("")
            } else {
                btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
                btnConfirm.isClickable = true
                btnConfirm.isEnabled = true
                btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnRequestApproval.isClickable = false
                btnRequestApproval.isEnabled = false
                btnRequestApproval.visibility = VISIBLE
                btnConfirm.visibility = VISIBLE
            }
        } else {

        }
        if (type.equals("ENABLE", true)) {
            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
            btnConfirm.isClickable = true
            btnConfirm.isEnabled = true
            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnRequestApproval.isClickable = false
            btnRequestApproval.isEnabled = false
            btnRequestApproval.visibility = VISIBLE
            btnConfirm.visibility = VISIBLE

        }
        if (type.equals("pc")) {
            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnConfirm.isClickable = false
            btnConfirm.isEnabled = false
            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_green))
            btnRequestApproval.isClickable = true
            btnRequestApproval.isEnabled = true
            btnRequestApproval.visibility = VISIBLE
            btnConfirm.visibility = VISIBLE
            ivRefresh.visibility = VISIBLE
            tvApprovalStatus.setText("")
            rbLoan.isClickable = false
            rbLoan.isEnabled = false
            isSentForApproval = false

        }
        if (type.equals("FOC")) {
            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnConfirm.isClickable = false
            btnConfirm.isEnabled = false
            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_green))
            btnRequestApproval.isClickable = true
            btnRequestApproval.isEnabled = true
            btnRequestApproval.visibility = VISIBLE
            btnConfirm.visibility = VISIBLE
            ivRefresh.visibility = VISIBLE
            tvApprovalStatus.setText("")
            rbLoan.isClickable = false
            rbLoan.isEnabled = false
            isSentForApproval = false
            foc = "FOC"
//            loadStockAdapter.isClickable=false

        }


//        }
    }

    fun enableDisableConfirm(enable: Boolean) {
        // incase the product type meter reading using this method
        if (enable) {
            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
        } else {
            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
        }

        btnConfirm.isClickable = enable
        btnConfirm.isEnabled = enable
        btnRequestApproval.visibility = GONE
    }


    override fun initializeControls() {
        recycleview = findViewById<View>(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        btnRequestApproval = findViewById<View>(R.id.btnRequestApproval) as Button
        btnConfirm = findViewById<View>(R.id.btnConfirm) as Button
        tvNoDataFound = findViewById<View>(R.id.tvNoDataFound) as TextView
        tvShipmentId = findViewById<View>(R.id.tvShipmentId) as TextView
        tvPaymentTerm = findViewById<View>(R.id.tvPayementTerm) as TextView

        tvApprovalStatus = findViewById(R.id.tvApprovalStatus) as TextView
        cbNonBGSelected = findViewById<View>(R.id.cbNonBGSelected) as CheckBox
        rgType = findViewById(R.id.rgType) as RadioGroup
        rbNormal = findViewById(R.id.rbNormalyCylinder) as RadioButton
        rbLoan = findViewById(R.id.rbLoanReturn) as RadioButton
//        btnCheckStatus = findViewById<View>(R.id.btnCheckStatus) as Button
        recycleview.setLayoutManager(androidx.recyclerview.widget.LinearLayoutManager(this, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false))
        if (Util.isNetworkAvailable(this)) {
            val siteListRequest = ReasonsListRequest(3, this)
            siteListRequest.setOnResultListener { isError, reasonMainDO ->
                hideLoader()
                if (reasonMainDO != null) {
                    if (isError) {
                        //                                        ((BaseActivity) context).showAppCompatAlert("", "No reasons found at the moment, please try again.", "Ok", "Cancel", "", false);
                    } else {

                        reasonMainDo = reasonMainDO

                    }
                } else {
                    hideLoader()
                    //                                    ((BaseActivity) context).showAppCompatAlert("", "No reasons found at the moment, please try again.", "Ok", "Cancel", "", false);
                }
            }
            siteListRequest.execute()
        } else {
            showAppCompatAlert("Alert!", getResources().getString(R.string.internet_connection), "OK", "", "", false)

        }
    }

    private fun requestApproval() {
        if (Util.isNetworkAvailable(this)) {
            val siteListRequest = ProductsModifyRequest(4, shipmentId, shipmentProductsList, this@CreateOrderScheduledCaptureDeliveryActivity)
            siteListRequest.setOnResultListener { isError, approveDO ->
                hideLoader()
                if (isError) {
                    Toast.makeText(this@CreateOrderScheduledCaptureDeliveryActivity, resources.getString(R.string.server_error), Toast.LENGTH_SHORT).show()

                    Log.e("RequestAproval", "Response : " + isError)
                } else {
                    if (approveDO != null) {

//                        btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
//                        btnRequestApproval.isClickable = false
//                        btnRequestApproval.isEnabled = false
                        if (approveDO.flag == 4) {
                            updateQtyConfirmationToServer()
                        } else {
                            Toast.makeText(this@CreateOrderScheduledCaptureDeliveryActivity, resources.getString(R.string.server_error), Toast.LENGTH_SHORT).show()
//                            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_green))
//                            btnRequestApproval.isClickable = true
//                            btnRequestApproval.isEnabled = true
//                            tvApprovalStatus.setText("Status : Request Not Sent")
                        }
                    } else {

                    }

                }

            }
            siteListRequest.execute()
        } else {
            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "", false)

        }


    }

    private var isSentForApproval: Boolean = false

    private fun updateStatus() {
        val siteListRequest = CancelRescheduleApprovalStatusRequest(shipmentId, this)
        siteListRequest.setOnResultListener { isError, approvalDO ->
            hideLoader()
            if (approvalDO != null) {
                if (isError) {
                    Toast.makeText(this@CreateOrderScheduledCaptureDeliveryActivity, "Approval under progress", Toast.LENGTH_SHORT).show()
                } else {
//                    approvalDO.status = "Approved"
                    if (approvalDO.status.equals("Approved", true)) {
                        loadDeliveryData()
                        isSentForApproval = true;
                        tvApprovalStatus.setText("Status : " + approvalDO.status)
                        btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
                        btnConfirm.isClickable = true
                        btnConfirm.isEnabled = true
                        btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        btnRequestApproval.isClickable = false
                        btnRequestApproval.isEnabled = false


                    } else if (approvalDO.status.equals("Rejected", true)) {
                        rejectProducts()
                        isSentForApproval = true;
                        tvApprovalStatus.setText("Status : " + approvalDO.status)
                        btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
                        btnConfirm.isClickable = true
                        btnConfirm.isEnabled = true
                        btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        btnRequestApproval.isClickable = false
                        btnRequestApproval.isEnabled = false

                    } else {
                        Toast.makeText(this@CreateOrderScheduledCaptureDeliveryActivity, "Approval under progress", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
        siteListRequest.execute()
    }

//    private fun checkStatus() {
//        if (Util.isNetworkAvailable(this)) {
//            shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
//            if (shipmentId.isEmpty()) {
//                shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER, "")
//            }
//            val siteListRequest = CheckApprovalStatusRequest(shipmentId, this)
//            siteListRequest.setOnResultListener { isError, approvalDO ->
//                hideLoader()
//                if (approvalDO != null) {
//                    if (isError) {
//                        showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.server_error), "" + resources.getString(R.string.ok), "", "" + resources.getString(R.string.from_failure), false)
//                    } else {
////                    approvalDO.status = "Approved"
//                        if (approvalDO.status.equals("Approved", true)) {
//                            isSentForApproval = true;
//                            tvApprovalStatus.setText("Status : " + approvalDO.status)
//                            btnCheckStatus.setBackgroundColor(resources.getColor(R.color.md_gray_light))
//                            btnCheckStatus.isClickable = false
//                            btnCheckStatus.isEnabled = false
//                            updateTheProductsInLocal()
//                            if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
//                                val intent = Intent(this@ScheduledCaptureDeliveryActivity, SignatureActivity::class.java);
//                                startActivityForResult(intent, 11)
//                            } else {
//                                if (nonBgType == 1) {
//                                    val intent = Intent(this@ScheduledCaptureDeliveryActivity, NonBGActivity::class.java);
//                                    startActivityForResult(intent, 13)
//                                } else {
//                                    val intent = Intent(this@ScheduledCaptureDeliveryActivity, SignatureActivity::class.java);
//                                    startActivityForResult(intent, 11)
//                                }
//                            }
//                        } else if (approvalDO.status.equals("Rejected", true)) {
//                            tvApprovalStatus.setText("Status : " + approvalDO.status)
//                            btnCheckStatus.setBackgroundColor(resources.getColor(R.color.md_green))
//                            btnCheckStatus.isClickable = true
//                            btnCheckStatus.isEnabled = true
//                            isSentForApproval = false
//                        } else {
//                            tvApprovalStatus.setText("Status : " + approvalDO.status)
//                            btnCheckStatus.setBackgroundColor(resources.getColor(R.color.md_green))
//                            btnCheckStatus.isClickable = true
//                            btnCheckStatus.isEnabled = true
////                            Toast.makeText(this@ScheduledCaptureDeliveryActivity, "Signature Approval under progress", Toast.LENGTH_SHORT).show()
//                        }
//                    }
//                } else {
//                    showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.server_error), "" + resources.getString(R.string.ok), "", "" + resources.getString(R.string.from_failure), false)
//
//                }
//            }
//            siteListRequest.execute()
//        } else {
//            showToast("" + resources.getString(R.string.internet_connection))
//        }
//    }

    private fun isProductsChanged(shipmentProductsList: ArrayList<ActiveDeliveryDO>): Boolean {
        if (cloneShipmentProductsList != null && shipmentProductsList != null) {
            if (shipmentProductsList.size != cloneShipmentProductsList.size) {
                return true;
            } else if (shipmentProductsList.size == cloneShipmentProductsList.size) {
                for (i in cloneShipmentProductsList.indices) {
                    if (shipmentProductsList.get(i).orderedQuantity != cloneShipmentProductsList.get(i).totalQuantity) {
                        return true
                    }
                }
            } else {
                return false
            }
        }
        return false
    }

    override fun onBackPressed() {

        if (isSentForApproval) {
            showToast("Please process the order..!")
        } else {
            LogUtils.debug("PAUSE", "tes")
            var data = tvApprovalStatus.text.toString()
            var intent = Intent()
            intent.putExtra("Status", data)
            intent.putExtra("List", shipmentProductsList)
            setResult(56, intent)
            finish()
            super.onBackPressed()
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == 1) {
            tvShipmentId.visibility = View.VISIBLE
            tvPaymentTerm.visibility = View.VISIBLE
            var from = data!!.getStringExtra("FROM")
            val activeDeliveryDOS = data!!.getSerializableExtra("AddedProducts") as ArrayList<ActiveDeliveryDO>
            if (activeDeliveryDOS.size > 0) {
                recycleview.visibility = VISIBLE
                tvNoDataFound.visibility = GONE

                if (loadStockAdapter == null) {
                    loadStockAdapter = CreateOrderScheduledProductsAdapter(this, activeDeliveryDOS, "", "", type);
                    recycleview.adapter = loadStockAdapter;
                } else {
                    var isProductExisted = false;
                    for (i in activeDeliveryDOS.indices) {
                        for (k in shipmentProductsList!!.indices) {
                            if (!shipmentProductsList!!.get(k).pType.equals("SCHEDULED") && activeDeliveryDOS.get(i).product.equals(shipmentProductsList!!.get(k).product, true)
                                    && activeDeliveryDOS.get(i).shipmentId.equals(shipmentProductsList!!.get(k).shipmentId, true)) {
                                shipmentProductsList!!.get(k).orderedQuantity = activeDeliveryDOS.get(i).orderedQuantity
                                isProductExisted = true
                                break
                            }
                        }
                        if (isProductExisted) {
                            isProductExisted = false
                            continue
                        } else {
                            shipmentProductsList!!.add(activeDeliveryDOS.get(i))
                        }
                    }

                    loadStockAdapter!!.refreshAdapter(shipmentProductsList, type)
                }
                if (!shipmentType.equals(resources.getString(R.string.checkin_scheduled), true)) {
                    btnConfirm.setText("Create Order")
                    val shipmentProductsType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "")

                    if (shipmentProductsType.equals(AppConstants.MeterReadingProduct, true)) {
                        btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
                        btnConfirm.isClickable = true
                        btnConfirm.isEnabled = true
                        btnConfirm.visibility = VISIBLE
                        btnRequestApproval.visibility = GONE
                        ivRefresh.visibility = GONE
                        rgType.setVisibility(VISIBLE)
                        cbNonBGSelected.visibility = GONE
                    } else {
                        btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
                        btnConfirm.isClickable = true
                        btnConfirm.isEnabled = true
                        btnConfirm.visibility = VISIBLE
                        btnRequestApproval.visibility = GONE
                        ivRefresh.visibility = GONE
                        rgType.setVisibility(VISIBLE)
                        cbNonBGSelected.visibility = VISIBLE
                        var sta = tvApprovalStatus.text.toString()
                        if (sta.contains("Rejected")) {
                            tvApprovalStatus.setText("")
                            rbLoan.isClickable = true
                            rbLoan.isEnabled = true
                            rbLoan.isSelected = true
                            rbNormal.isClickable = true
                            rbNormal.isEnabled = true
                            rbNormal.isSelected = true
                        }

                    }

                } else {
                    updateConfirmRequestButtons("", shipmentProductsList!!)
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        hideLoader()
    }

    private fun rejectProducts() {
        val activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)

        loadStockAdapter = CreateOrderScheduledProductsAdapter(this, activeDeliverySavedDo.activeDeliveryDOS, "", "", type)
        recycleview.adapter = loadStockAdapter
    }

    private fun loadDeliveryData() {

        if (Util.isNetworkAvailable(this)) {
            if (shipmentId.length > 0) {
                cloneShipmentProductsList.removeAll(shipmentProductsList!!)
                val driverListRequest = ActiveDeliveryRequest(shipmentId, this)
                driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                    hideLoader()
                    if (isError) {
                        Log.e("loadScheduleData", "Error at loading loadScheduleData()")
                    } else {
                        StorageManager.getInstance(this).saveActiveDeliveryMainDo(this, activeDeliveryDo)
                        shipmentProductsList = activeDeliveryDo.activeDeliveryDOS;
                        if (shipmentProductsList != null && shipmentProductsList!!.size > 0) {
                            cloneShipmentProductsList.addAll(shipmentProductsList!!)
                            loadStockAdapter = CreateOrderScheduledProductsAdapter(this, shipmentProductsList, "", "", type)
                            recycleview.adapter = loadStockAdapter
                            recycleview.visibility = VISIBLE
                            cbNonBGSelected.visibility = VISIBLE
                            tvNoDataFound.visibility = GONE
                            tvApprovalStatus.setVisibility(View.VISIBLE)
                            btnRequestApproval.setVisibility(View.VISIBLE)
                            btnConfirm.setVisibility(View.VISIBLE)
                        } else {
                            tvApprovalStatus.setVisibility(View.GONE)
                            btnRequestApproval.setVisibility(View.GONE)
                            btnConfirm.setVisibility(View.GONE)
                            recycleview.visibility = GONE
                            tvNoDataFound.visibility = VISIBLE
                            btnConfirm.isEnabled = false
                            btnConfirm.isClickable = false
                            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        }
                    }
                }
                driverListRequest.execute()


            }
        }


    }



}