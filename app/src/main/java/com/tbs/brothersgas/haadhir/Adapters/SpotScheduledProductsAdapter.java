package com.tbs.brothersgas.haadhir.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.app.Dialog;
import android.content.Context;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDialog;
import androidx.appcompat.widget.SwitchCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.tbs.brothersgas.haadhir.Activitys.BaseActivity;
import com.tbs.brothersgas.haadhir.Activitys.ScheduledCaptureDeliveryActivity;
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryDO;
import com.tbs.brothersgas.haadhir.Model.ReasonMainDO;
import com.tbs.brothersgas.haadhir.R;
import com.tbs.brothersgas.haadhir.common.AppConstants;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;

import java.math.BigDecimal;
import java.util.ArrayList;

public class SpotScheduledProductsAdapter extends RecyclerView.Adapter<SpotScheduledProductsAdapter.MyViewHolder> implements Filterable {
    public ArrayList<ActiveDeliveryDO> activeDeliveryDOS;
    ValueFilter valueFilter;
    private Context context;
    int type = 0;
    String unit = "";
    public boolean isClickable = true;
    Dialog dialog;
    ReasonMainDO reasonMAINDO;

    @Override
    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }

    private class ValueFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {
                ArrayList<ActiveDeliveryDO> filterList = new ArrayList<>();
                for (int i = 0; i < activeDeliveryDOS.size(); i++) {
                    if ((activeDeliveryDOS.get(i).product.toUpperCase()).contains(constraint.toString().toUpperCase())) {
                        filterList.add(activeDeliveryDOS.get(i));
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = activeDeliveryDOS.size();
                results.values = activeDeliveryDOS;
            }
            return results;

        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            activeDeliveryDOS = (ArrayList<ActiveDeliveryDO>) results.values;
            notifyDataSetChanged();
        }

    }

    public void refreshAdapter(ArrayList<ActiveDeliveryDO> activeDeliveryDOS, int Type) {
        this.activeDeliveryDOS = activeDeliveryDOS;
        this.type = Type;
        notifyDataSetChanged();
    }

    public ArrayList<ActiveDeliveryDO> selectedactiveDeliveryDOs = new ArrayList<>();
    private String from = "";

    public ArrayList<ActiveDeliveryDO> getSelectedactiveDeliveryDOs() {
        return selectedactiveDeliveryDOs;
    }

    public SpotScheduledProductsAdapter(Context context, ArrayList<ActiveDeliveryDO> activeDeliveryDOS, String from, String damagE, int Type) {
        this.context = context;
        this.activeDeliveryDOS = activeDeliveryDOS;
        this.from = from;
        this.type = Type;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.decanting_stock_data, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ActiveDeliveryDO activeDeliveryDO = activeDeliveryDOS.get(position);
        AppConstants.productDO = activeDeliveryDOS;
//        final ActiveDeliveryDO productDO = AppConstants.productDO.get(position);
        if (type == 1) {
            holder.tvSelection.setText("" + activeDeliveryDO.price);
        }else {
            holder.tvSelection.setText("" + activeDeliveryDO.price);

        }
        int param = ((BaseActivity) context).preferenceUtils.getIntFromPreference(PreferenceUtils.UNIT, 0);
        if (param == 2) {
            unit = "GALLON";
        } else {
            unit = "Litre";
        }
        if (from.equalsIgnoreCase("AddProducts")) {
            holder.tvProductName.setText(activeDeliveryDO.product);
            holder.tvAvailableQty.setVisibility(View.GONE);
        }else {
            holder.tvProductName.setText(activeDeliveryDO.product);

        }
        holder.tvDescription.setText(activeDeliveryDO.productDescription);
        activeDeliveryDO.orderedQuantity=1.0;
        if (from.equalsIgnoreCase("AddProducts")) {
            holder.llFoc.setVisibility(View.GONE);
            holder.llPriceTag.setVisibility(View.GONE);
            activeDeliveryDO.orderedQuantity = 1.0;
            holder.tvNumberET.setText("" + activeDeliveryDO.orderedQuantity);

        }



        String shipmentProductsType = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "");
        activeDeliveryDO.shipmentProductType = shipmentProductsType;
        if (from.equalsIgnoreCase("AddProducts")) {
            holder.cbSelected.setVisibility(View.VISIBLE);
            holder.llPriceTag.setVisibility(View.GONE);
            holder.llFoc.setVisibility(View.GONE);

        }

        holder.cbSelected.setOnCheckedChangeListener(null);
        holder.cbSelected.setChecked(activeDeliveryDO.isProductAdded);
        holder.cbSelected.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                activeDeliveryDO.isProductAdded = isChecked;
                if (isChecked) {
                    if (activeDeliveryDO.orderedQuantity > 0) {
                        selectedactiveDeliveryDOs.add(activeDeliveryDO);

                    }else {

                    }
                } else {
                    selectedactiveDeliveryDOs.remove(activeDeliveryDO);
                }
            }
        });
        final Double[] quantity = {activeDeliveryDO.orderedQuantity};//{Integer.parseInt(holder.tvNumberET.getText().toString())};
        holder.ivAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isClickable == true) {

                        int count = 0;
                        AppConstants.productDO.get(position).count = AppConstants.productDO.get(position).count + 1;
                        quantity[0] = quantity[0] + 1;
                        holder.tvNumberET.setText("" + quantity[0]++);
                        activeDeliveryDO.orderedQuantity = quantity[0];
                        if (context instanceof ScheduledCaptureDeliveryActivity) {
                            ((ScheduledCaptureDeliveryActivity) context).updateConfirmRequestButtons("", activeDeliveryDOS);

                                Double price = 0.0;
                                for (ActiveDeliveryDO activeDeliveryDO : AppConstants.productDO) {
                                    price = price + activeDeliveryDO.price * activeDeliveryDO.count;
                                    count = count + activeDeliveryDO.count;
                                }
                                if (AppConstants.productDO.get(position).count > 1) {
                                    ((ScheduledCaptureDeliveryActivity) context).tvItemCount.setText(count + " Items");
                                } else {
                                    ((ScheduledCaptureDeliveryActivity) context).tvItemCount.setText(count + " Item");
                                }
                                ((ScheduledCaptureDeliveryActivity) context).tvItemPrice.setText("AED " + price);
                                Double vat = price * 5 / 100;
                                Double total = price + vat;
                                ((ScheduledCaptureDeliveryActivity) context).tvItemVat.setText("AED " + vat);
                                ((ScheduledCaptureDeliveryActivity) context).tvItemTotal.setText("AED " + total);
                                Double amount = ((ScheduledCaptureDeliveryActivity) context).preferenceUtils.getDoubleFromPreference(PreferenceUtils.CREDIT_AMOUNT, 0.0);
                                if (total > amount) {
                                    ((ScheduledCaptureDeliveryActivity) context).btnConfirm.setText("Create Order");
                                    ((ScheduledCaptureDeliveryActivity) context).rbNormal.setText("Normal Order");
                                    ((ScheduledCaptureDeliveryActivity) context).rbLoan.setText("Loan Order");

                                    ((ScheduledCaptureDeliveryActivity) context).updateUI();

                                } else {
                                    ((ScheduledCaptureDeliveryActivity) context).btnConfirm.setText("Create Delivery");
                                    ((ScheduledCaptureDeliveryActivity) context).rbNormal.setText("Normal Delivery");
                                    ((ScheduledCaptureDeliveryActivity) context).rbLoan.setText("Loan Delivery");

                                    ((ScheduledCaptureDeliveryActivity) context).updateUI();

                                }
                                holder.tvSelection.setText("" + activeDeliveryDO.price * AppConstants.productDO.get(position).count);



                        }


                } else {
                    ((BaseActivity) context).showToast("Please process the order..!");

                }

            }
        });
        holder.ivRemove.setOnClickListener(view -> {
            if (isClickable == true) {

                if (quantity[0] > 1) {
                    int count = 0;
                    AppConstants.productDO.get(position).count = AppConstants.productDO.get(position).count - 1;
                    quantity[0] = quantity[0] - 1;
                    holder.tvNumberET.setText("" + quantity[0]);
                    activeDeliveryDO.orderedQuantity = quantity[0];
                    if (context instanceof ScheduledCaptureDeliveryActivity) {
                        ((ScheduledCaptureDeliveryActivity) context).updateConfirmRequestButtons("", activeDeliveryDOS);

                            Double price = 0.0;
                            for (ActiveDeliveryDO activeDeliveryDOO : AppConstants.productDO) {
                                price = price + activeDeliveryDOO.price * activeDeliveryDOO.count;
                                count = count + activeDeliveryDOO.count;
                            }
                            if (AppConstants.productDO.get(position).count > 1) {
                                ((ScheduledCaptureDeliveryActivity) context).tvItemCount.setText(count + " Items");
                            } else {
                                ((ScheduledCaptureDeliveryActivity) context).tvItemCount.setText(count + " Item");
                            }
                            ((ScheduledCaptureDeliveryActivity) context).tvItemPrice.setText("AED " + price);
                            Double vat = price * 5 / 100;
                            Double total = price + vat;
                            ((ScheduledCaptureDeliveryActivity) context).tvItemVat.setText("AED " + vat);
                            ((ScheduledCaptureDeliveryActivity) context).tvItemTotal.setText("AED " + total);
                            Double amount = ((ScheduledCaptureDeliveryActivity) context).preferenceUtils.getDoubleFromPreference(PreferenceUtils.CREDIT_AMOUNT, 0.0);
                            if (total > amount) {
                                ((ScheduledCaptureDeliveryActivity) context).btnConfirm.setText("Create Order");
                                ((ScheduledCaptureDeliveryActivity) context).rbNormal.setText("Normal Order");
                                ((ScheduledCaptureDeliveryActivity) context).rbLoan.setText("Loan Order");

                                ((ScheduledCaptureDeliveryActivity) context).updateUI();
                            } else {
                                ((ScheduledCaptureDeliveryActivity) context).btnConfirm.setText("Create Delivery");
                                ((ScheduledCaptureDeliveryActivity) context).rbNormal.setText("Normal Delivery");
                                ((ScheduledCaptureDeliveryActivity) context).rbLoan.setText("Loan Delivery");
                                ((ScheduledCaptureDeliveryActivity) context).updateUI();


                            }
                            holder.tvSelection.setText("" + activeDeliveryDO.price * AppConstants.productDO.get(position).count);


                    }
                }
            } else {
                ((BaseActivity) context).showToast("Please process the order..!");

            }
        });
        holder.tvNumberET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                        if (s.toString().equals(".") && s.toString().equalsIgnoreCase("")) {
                            holder.tvNumberET.setText("1.0");
                        }

                        if (!s.toString().equalsIgnoreCase("") || Double.parseDouble(s.toString()) == .0 || !s.toString().equals(".")) {
                            if ( Double.parseDouble(s.toString()) > 0) {
                                quantity[0] = Double.parseDouble(s.toString());
                                activeDeliveryDO.orderedQuantity = quantity[0];
                                if (context instanceof ScheduledCaptureDeliveryActivity) {
                                    ((ScheduledCaptureDeliveryActivity) context).updateConfirmRequestButtons("", activeDeliveryDOS);
                                }
                            } else {
                                holder.tvNumberET.setText("1.0");
                                if (s.length() > 1) {
                                    final String newText = s.toString().substring(0, s.length() - 1) + "";
                                    holder.tvNumberET.setText("" + newText);
                                }
                            }
                        } else {
                            holder.tvNumberET.setText("1.0");
                        }

                } catch (NumberFormatException e) {
                    holder.tvNumberET.setText("1.0");
                }
                if (context instanceof ScheduledCaptureDeliveryActivity) {


                        Double price = 0.0;
                        Double count = Double.valueOf((holder.tvNumberET.getText().toString()));
                        int convertedCount =(int) Math.round(count);
                        AppConstants.productDO.get(position).count=convertedCount;

                        for (ActiveDeliveryDO activeDeliveryDOO : AppConstants.productDO) {
                            price = price + activeDeliveryDOO.price * activeDeliveryDOO.count;
                            count = count + activeDeliveryDOO.count;
                        }

                        if (AppConstants.productDO.get(position).count > 1) {
                            ((ScheduledCaptureDeliveryActivity) context).tvItemCount.setText(count + " Items");
                        } else {
                            ((ScheduledCaptureDeliveryActivity) context).tvItemCount.setText(count + " Item");
                        }
                        ((ScheduledCaptureDeliveryActivity) context).tvItemPrice.setText("AED " + price);
                        Double vat = price * 5 / 100;
                        Double total = price + vat;
                        ((ScheduledCaptureDeliveryActivity) context).tvItemVat.setText("AED " + vat);
                        ((ScheduledCaptureDeliveryActivity) context).tvItemTotal.setText("AED " + total);
                        Double amount = ((ScheduledCaptureDeliveryActivity) context).preferenceUtils.getDoubleFromPreference(PreferenceUtils.CREDIT_AMOUNT, 0.0);
                        if (total > amount) {
                            ((ScheduledCaptureDeliveryActivity) context).btnConfirm.setText("Create Order");
                            ((ScheduledCaptureDeliveryActivity) context).rbNormal.setText("Normal Order");
                            ((ScheduledCaptureDeliveryActivity) context).rbLoan.setText("Loan Order");
                            ((ScheduledCaptureDeliveryActivity) context).updateUI();
                        } else {
                            ((ScheduledCaptureDeliveryActivity) context).btnConfirm.setText("Create Delivery");
                            ((ScheduledCaptureDeliveryActivity) context).rbNormal.setText("Normal Delivery");
                            ((ScheduledCaptureDeliveryActivity) context).rbLoan.setText("Loan Delivery");
                            ((ScheduledCaptureDeliveryActivity) context).updateUI();
                        }
                        holder.tvSelection.setText("" + activeDeliveryDO.price * AppConstants.productDO.get(position).count);


                }

            }
        });
        if (context instanceof ScheduledCaptureDeliveryActivity) {
            holder.ivDelete.setVisibility(View.GONE);
        } else {
            holder.ivDelete.setVisibility(View.GONE);
        }
        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (context instanceof ScheduledCaptureDeliveryActivity) {
                    ((ScheduledCaptureDeliveryActivity) context).deleteShipmentProducts(activeDeliveryDO);
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return activeDeliveryDOS != null ? activeDeliveryDOS.size() : 0;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvProductName, tvEdit, tvDescription, tvAvailableQty, tvInfo, tvNumber, etTotalMeterQty, tvSelection, tvProductUnit;
        public RelativeLayout rlRemove, rlAdd;
        public ImageView ivRemove, ivAdd, ivDelete;
        public LinearLayout llAddRemove, llMeterReadings, llPriceTag, llFoc;
        public EditText tvNumberET, etMeterReading1, etMeterReading2, etMeterReading3;
        public SwitchCompat swStatus;

        private CheckBox cbSelected;

        public MyViewHolder(View view) {
            super(view);
            tvProductName = (TextView) view.findViewById(R.id.tvName);
            tvDescription = (TextView) view.findViewById(R.id.tvDescription);
            tvNumber = (TextView) view.findViewById(R.id.tvNumber);
            ivRemove = (ImageView) view.findViewById(R.id.ivRemove);
            ivAdd = (ImageView) view.findViewById(R.id.ivAdd);
            tvNumberET = (EditText) view.findViewById(R.id.tvNumberET);
            swStatus = (SwitchCompat) view.findViewById(R.id.swStatusCustom);
            tvEdit = (TextView) view.findViewById(R.id.tvEdit);

            tvAvailableQty = (TextView) view.findViewById(R.id.tvAvailableQty);
            tvInfo = (TextView) view.findViewById(R.id.tvInfo);
            tvProductUnit = (TextView) view.findViewById(R.id.tvProductUnit);
            etMeterReading1 = (EditText) view.findViewById(R.id.etMeterReading1);
            etMeterReading2 = (EditText) view.findViewById(R.id.etMeterReading2);
            etMeterReading3 = (EditText) view.findViewById(R.id.etMeterReading3);
            etTotalMeterQty = (TextView) view.findViewById(R.id.etTotalMeterQty);

            llMeterReadings = (LinearLayout) view.findViewById(R.id.llMeterReadings);

            ivDelete = view.findViewById(R.id.ivDelete);
            cbSelected = view.findViewById(R.id.cbSelected);
            llAddRemove = (LinearLayout) view.findViewById(R.id.llAddRemove);
            tvSelection = (TextView) view.findViewById(R.id.tvSelection);
            llPriceTag = (LinearLayout) view.findViewById(R.id.llPriceTag);
            llFoc = (LinearLayout) view.findViewById(R.id.llFoc);

        }
    }

    class ReasonAdapter extends BaseAdapter {
        private TextView tvName;

        @Override
        public Object getItem(int i) {
            return i;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = inflater.inflate(R.layout.simple_text, null);
            tvName = (TextView) v.findViewById(R.id.tvName);
            tvName.setText(reasonMAINDO.reasonDOS.get(i).reason);


            return v;
        }

        @Override
        public int getCount() {
            if (reasonMAINDO.reasonDOS.size() > 0)
                return reasonMAINDO.reasonDOS.size();
            else
                return 0;
        }

    }

//    private Double getCartCount(){
//        Double price =0.0;
//        for(ActiveDeliveryDO activeDeliveryDO : AppConstants.productDO){
//            price = activeDeliveryDO.price*activeDeliveryDO.count;
//        }
//        return price;
//    }


}
