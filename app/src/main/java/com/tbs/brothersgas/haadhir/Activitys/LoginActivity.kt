package com.tbs.brothersgas.haadhir.Activitys

import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.drawerlayout.widget.DrawerLayout
import com.tbs.brothersgas.haadhir.Adapters.ProfileAdapter
import com.tbs.brothersgas.haadhir.BuildConfig
import com.tbs.brothersgas.haadhir.Model.LoginDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.AdminLoginRequest
import com.tbs.brothersgas.haadhir.Requests.LoginMultipleUsersRequest
import com.tbs.brothersgas.haadhir.Requests.LoginTimeCaptureRequest
import com.tbs.brothersgas.haadhir.Requests.ProfileListRequest
import com.tbs.brothersgas.haadhir.collector.CollectorCustomerActivity
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.decanting.DashBoardDeliveryDecantingActivity
import com.tbs.brothersgas.haadhir.delivery_executive.DashBoardDeliveryExecutiveActivity
import com.tbs.brothersgas.haadhir.utils.AppPrefs
import com.tbs.brothersgas.haadhir.utils.CalendarUtils
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import kotlinx.android.synthetic.main.activity_login.*
import java.text.SimpleDateFormat
import java.util.*


class LoginActivity : BaseActivity() {
    private lateinit var siteAdapter: ProfileAdapter
    internal lateinit var dbUser: String
    internal lateinit var dbPass: String
    lateinit var llSplash: ScrollView
    internal lateinit var companyId: String
    var booleanCollector: Boolean = false
    var booleanDriver: Boolean = true
    lateinit var dialog: Dialog

    override fun initializeControls() {
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun initialize() {

        llSplash = layoutInflater.inflate(R.layout.activity_login, null) as ScrollView
        llBody.addView(llSplash, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT))
        initializeControls()

        toolbar.visibility = View.GONE
        flToolbar.visibility = View.GONE
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, Gravity.LEFT)

        val btnLogin = findViewById(R.id.btnLogin) as Button
        val etUserName = findViewById(R.id.etUserName) as EditText
        val etPassword = findViewById(R.id.etPassword) as EditText
        val etCompanyId = findViewById(R.id.etCompanyId) as EditText
        val ivProfile = findViewById(R.id.ivProfile) as RelativeLayout

        var profileName = preferenceUtils.getStringFromPreference(PreferenceUtils.LOGIN_PROFILE_NAME, "")
        if (profileName.isEmpty()) {
            tvProfile.setText("Driver")

        } else {
            tvProfile.setText(profileName)

        }

        ivProfile.setOnClickListener {
            selectProfile()
//            var customDialog = Dialog(this);
//            customDialog.getWindow().setBackgroundDrawable( ColorDrawable(Color.TRANSPARENT));
//            customDialog.setContentView(R.layout.radio_dialog);
//            if(booleanCollector==true){
//                customDialog.rbCollector.isChecked = true
//                customDialog.rbDriver.isChecked = false
//                customDialog.rbCollector.setTextColor(getColor(R.color.colorPrimary))
//                customDialog.rbDriver.setTextColor(getColor(R.color.profile_text_color))
//            }else{
//                customDialog.rbDriver.isChecked = true
//                customDialog.rbCollector.isChecked = false
//                customDialog.rbDriver.setTextColor(getColor(R.color.colorPrimary))
//                customDialog.rbCollector.setTextColor(getColor(R.color.profile_text_color))
//            }
//            customDialog.show();
//            customDialog.rbDriver.setOnClickListener {
//                customDialog.rbDriver.isChecked = true
//                booleanCollector=false
//                booleanDriver=true
//
//                customDialog.rbDriver.setTextColor(getColor(R.color.colorPrimary))
//                customDialog.rbCollector.setTextColor(getColor(R.color.profile_text_color))
//            }
//            customDialog.rbCollector.setOnClickListener {
//                booleanCollector=true
//                booleanDriver=false
//                customDialog.rbCollector.isChecked = true
//
//                customDialog.rbCollector.setTextColor(getColor(R.color.colorPrimary))
//                customDialog.rbDriver.setTextColor(getColor(R.color.profile_text_color))
//
//            }
//            customDialog.button1.setOnClickListener {
//                customDialog.dismiss()
//            }
//            customDialog.button2.setOnClickListener {
//                customDialog.dismiss()
//            }
        }


        etCompanyId.setText("" + resources.getString(R.string.company_name))
        etUserName.requestFocus()
        if (preferenceUtils.getStringFromPreference(PreferenceUtils.CONFIG_SUCCESS, "").isEmpty()) {
            preferenceUtils.saveString(PreferenceUtils.IP_ADDRESS, "47.91.104.128")
            preferenceUtils.saveString(PreferenceUtils.PORT, "8124")
            preferenceUtils.saveString(PreferenceUtils.ALIAS, BuildConfig.ALIAS)
            preferenceUtils.saveString(PreferenceUtils.A_USER_NAME, "VANSALES")
            preferenceUtils.saveString(PreferenceUtils.A_PASSWORD, "12345")
        }

        etCompanyId.setText("Brothers Gas")
        btnLogin.setOnClickListener {
            dbUser = etUserName!!.text.toString().trim()
            dbPass = etPassword!!.text.toString().trim()
            companyId = etCompanyId!!.text.toString().trim()

            if (companyId.length == 0) {
                showAlert("" + resources.getString(R.string.login_company_id))
            } else if (dbUser.length == 0) {
                showAlert("" + resources.getString(R.string.login_username))
            } else if (dbPass.length == 0) {
                showAlert("" + resources.getString(R.string.login_password))
            }
//            else if (isExpired()) {
//                showAppCompatAlert("" + resources.getString(R.string.login_info),
//                        "" + resources.getString(R.string.login_licence),
//                        "" + resources.getString(R.string.ok), "",
//                        "" + resources.getString(R.string.from_expired), false);
//            }
            else {
                loginSuccess()
            }

        }

        val ivConfiguration = findViewById(R.id.ivConfiguration) as ImageView
        val imageView = findViewById(R.id.imageView) as ImageView
        var count = 0

        imageView.setOnClickListener {
            count = count + 1
            if (count > 9) {
                val intent = Intent(this@LoginActivity, ConfigurationActivity::class.java);
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                startActivity(intent);
                finish()
            } else {
//                showToast("Count : "+count)
//                imageView.isClickable=false
//                imageView.isEnabled=false
            }

        }

        ivConfiguration.setOnClickListener {
            val intent = Intent(this@LoginActivity, ConfigurationActivity::class.java);
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent);
            finish()
        }
    }

    fun selectProfile() {
        dialog = Dialog(this, R.style.NewDialog)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setContentView(R.layout.profile_dailog)
        val window = dialog.getWindow()
        window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)

        var recyclerView = dialog.findViewById(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        var btnCancel = dialog.findViewById(R.id.btnCancel) as Button
        var btnOK = dialog.findViewById(R.id.btnOk) as Button
        btnCancel.setOnClickListener {
            dialog.dismiss()
        }
        btnOK.setOnClickListener {


            if (siteAdapter != null) {
                for (listItem in siteAdapter.list) {
                    if (listItem.isSelected) {
                        preferenceUtils.saveString(PreferenceUtils.LOGIN_PROFILE_ID, listItem.productId)
                        preferenceUtils.saveString(PreferenceUtils.LOGIN_PROFILE_NAME, listItem.productName)
                        break
                    }
                }
            }
            tvProfile.setText(preferenceUtils.getStringFromPreference(PreferenceUtils.LOGIN_PROFILE_NAME, ""))
            dialog.dismiss()
        }
        recyclerView.setLayoutManager(androidx.recyclerview.widget.LinearLayoutManager(this));
        if (Util.isNetworkAvailable(this)) {
            val siteListRequest = ProfileListRequest(this)
            siteListRequest.setOnResultListener { isError, modelDO ->
                hideLoader()
                if (isError) {
                    Toast.makeText(this, resources.getString(R.string.server_error), Toast.LENGTH_SHORT).show()
                } else {
                    if (modelDO != null) {
                        for (productDO in modelDO) {
                            val name = preferenceUtils.getStringFromPreference(PreferenceUtils.LOGIN_PROFILE_NAME, "")
                            if (name.equals(productDO.productName)) {
                                for (i in modelDO.indices) {
                                    modelDO.get(i).isSelected = false
                                }
                                productDO.isSelected = true
//                                break
                            } else {
                                val name = preferenceUtils.getStringFromPreference(PreferenceUtils.LOGIN_PROFILE_NAME, "")
                                if(name.isEmpty()){

                                    if (productDO.role.equals(productDO.productId)) {
                                        for (i in modelDO.indices) {
                                            modelDO.get(i).isSelected = false
                                        }
                                        productDO.isSelected = true

                                    } else {
                                        productDO.isSelected = false

                                    }

                                }

                            }

                        }
                        siteAdapter = ProfileAdapter(this, modelDO)
                        recyclerView.setAdapter(siteAdapter)
//                    var siteAdapter = CreatePaymentAdapter(this@CreatePaymentActivity, createPaymentDo)
//                    recyclerView.setAdapter(siteAdapter)

                    } else {
                        hideLoader()
                        showAlert("" + resources.getString(R.string.server_error))
                    }
                }

            }

            siteListRequest.execute()
        } else {
            showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

        }
        dialog.show()

    }

    override fun onButtonYesClick(from: String) {
        if (from.equals(resources.getString(R.string.from_expired), true)) {

        }
        if (from.equals("PLAYSTORE", true)) {
            val appPackageName = packageName // getPackageName() from Context or Activity object
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")))
        }

    }

    private fun isExpired(): Boolean {
        try {
            val dateFormat = "dd/MM/yyyy"
            val expireDate = "01/10/2022"
            val sdf = SimpleDateFormat(dateFormat, Locale.getDefault()) // 28-01-2019
            val today = getToday(dateFormat)
            val eDate = sdf.parse(expireDate)
            val curDate = sdf.parse(today)
            if (eDate.compareTo(curDate) <= 0) {
                return true;
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return false
    }

    private fun getToday(format: String): String {
        val date = Date()
        return SimpleDateFormat(format).format(date)
    }

    private fun loginSuccess() {
        var role =preferenceUtils.getStringFromPreference(PreferenceUtils.LOGIN_PROFILE_ID,"")
//        if (booleanCollector == true) {
//            role = 2
//        } else {
//            role = 1
//        }
        if (Util.isNetworkAvailable(this)) {
            val adminLoginRequest = AdminLoginRequest(role, this@LoginActivity)
            adminLoginRequest.setOnResultListener(object : AdminLoginRequest.OnResultListener {
                override fun onCompleted(isError: Boolean, isValid: Boolean, loginDO: LoginDO) {
                    if (isError) {
                        if (preferenceUtils.getStringFromPreference(PreferenceUtils.IP_ADDRESS, "").isEmpty()) {
                            showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.config_error), "" + resources.getString(R.string.ok), "", "" + resources.getString(R.string.from_failure), false)
                        } else {
                            showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.server_error), "" + resources.getString(R.string.ok), "", "" + resources.getString(R.string.from_failure), false)
                        }
                    } else {
                        if (loginDO.flag == 1) {
                            if (loginDO.version.equals(resources.getString(R.string.app_version))) {
                                AppPrefs.putString(AppPrefs.DRIVER_ID, loginDO.driverCode)
                                preferenceUtils = PreferenceUtils(this@LoginActivity)
                                preferenceUtils.saveString(PreferenceUtils.USER_NAME, dbUser)
                                preferenceUtils.saveString(PreferenceUtils.PASSWORD, dbPass)
                                preferenceUtils.saveString(PreferenceUtils.PROFILE_PICTURE, loginDO.Image)
                                preferenceUtils.saveString(PreferenceUtils.DRIVER_ID, loginDO.driverCode)
                                preferenceUtils.saveString(PreferenceUtils.B_SITE_ID, loginDO.site)
                                preferenceUtils.saveString(PreferenceUtils.MAIN_SIIDE_ID, loginDO.site)
                                preferenceUtils.saveString(PreferenceUtils.LOGIN_DRIVER_NAME, loginDO.driverName)
                                preferenceUtils.saveString(PreferenceUtils.B_SITE_NAME, loginDO.siteDescription)
                                preferenceUtils.saveString(PreferenceUtils.COMPANY, loginDO.company)
                                preferenceUtils.saveString(PreferenceUtils.COMPANY_DES, loginDO.companyDescription)
                                preferenceUtils.saveString(PreferenceUtils.COMPANY, companyId)
                                preferenceUtils.saveString(PreferenceUtils.LOGIN_EMAIL, loginDO.email)
                                preferenceUtils.saveString(PreferenceUtils.APP_VERSION, loginDO.version)
                                preferenceUtils.saveInt(PreferenceUtils.USER_ROLE, loginDO.role)
                                preferenceUtils.saveInt(PreferenceUtils.CHEQUE_DATE, loginDO.chequeDays)
                                preferenceUtils.saveInt(PreferenceUtils.USER_AUTHORIZED, loginDO.authorized)
                                preferenceUtils.saveString(PreferenceUtils.LOGIN_SUCCESS, "" + resources.getString(R.string.from_success))
                                loginTimeCapture()
                                var role = preferenceUtils.getIntFromPreference(PreferenceUtils.USER_ROLE, 0)
                                if (role == 10) {
                                    val intent = Intent(this@LoginActivity, DashBoardActivity::class.java)
                                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                                    startActivity(intent)
                                    finish()
                                } else if (role == 20) {
                                    val intent = Intent(this@LoginActivity, DashBoardActivity::class.java)
                                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                                    startActivity(intent)
                                    finish()
                                } else if (role == 30) {
                                    val intent = Intent(this@LoginActivity, CollectorCustomerActivity::class.java)
                                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                                    startActivity(intent)
                                    finish()
                                } else if (role == 40) {
                                    val intent = Intent(this@LoginActivity, DashBoardDeliveryExecutiveActivity::class.java)
                                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                                    startActivity(intent)
                                    finish()
                                }
                                else if (role == 50) {
                                    val intent = Intent(this@LoginActivity, DashBoardDeliveryDecantingActivity::class.java)
                                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                                    startActivity(intent)
                                    finish()
                                } else {
                                    showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.login_user_not_available), "" + resources.getString(R.string.ok), "", "" + resources.getString(R.string.from_failure), false)
                                }


                            } else {
                                showAppCompatAlert("" + resources.getString(R.string.login_info), "" + resources.getString(R.string.playstore_messsage), "" + resources.getString(R.string.ok), "Cancel", "PLAYSTORE", true)
                            }


                        } else if (loginDO.flag == 0) {
                            showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.login_invalid), "" + resources.getString(R.string.ok), "", "" + resources.getString(R.string.from_failure), false)

                        } else if (loginDO.flag == 2) {
                            showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.login_inactive), "" + resources.getString(R.string.ok), "", "" + resources.getString(R.string.from_failure), false)

                        } else if (loginDO.flag == 3) {
                            showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.login_another_device), "" + resources.getString(R.string.ok), "", "" + resources.getString(R.string.from_failure), false)

                        } else {
                            if (preferenceUtils.getStringFromPreference(PreferenceUtils.IP_ADDRESS, "").isEmpty()) {
                                showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.config_error), "" + resources.getString(R.string.ok), "", "" + resources.getString(R.string.from_failure), false)

                            } else {
                                showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.server_error), "" + resources.getString(R.string.ok), "", "" + resources.getString(R.string.from_failure), false)

                            }
                        }
                    }
                }
            })
            adminLoginRequest.execute(dbUser, dbPass)
        } else {
            showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.internet_connection), "" + resources.getString(R.string.ok), "", "", false)

        }
    }

    private fun loginTimeCapture() {
        var driverId = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "")
        var date = CalendarUtils.getDate()
        var time = CalendarUtils.getTime()
        if (Util.isNetworkAvailable(this)) {
            val siteListRequest = LoginTimeCaptureRequest(driverId, date, date, time, this@LoginActivity)
            siteListRequest.setOnResultListener { isError, loginDo ->
                hideLoader()
                if (loginDo != null) {
                    if (isError) {
                        hideLoader()
//                    Toast.makeText(this@LoginActivity, "Failed", Toast.LENGTH_SHORT).show()
                    } else {
                        hideLoader()
                        if (loginDo.flag == 1) {
//                       showToast("Success")

                        } else {
//                        showToast("Success")

                        }

                    }
                } else {
                    hideLoader()
                }

            }

            siteListRequest.execute()
        } else {
//            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), ""+resources.getString(R.string.ok), "", "",false)
            showToast("" + resources.getString(R.string.internet_connection))
        }


    }

    private fun loginMultipleUsersCapture() {
        var driverId = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "")
        var date = CalendarUtils.getDate()
        var time = CalendarUtils.getTime()
        if (Util.isNetworkAvailable(this)) {
            val siteListRequest = LoginMultipleUsersRequest(driverId, companyId, date, time, this@LoginActivity)
            siteListRequest.setOnResultListener { isError, loginDo ->
                hideLoader()
                if (loginDo != null) {
                    if (isError) {
                        hideLoader()
                        showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.server_error), "" + resources.getString(R.string.ok), "", "" + resources.getString(R.string.from_failure), false)
                    } else {
                        hideLoader()
                        if (loginDo.flag == 1) {

                        } else if (loginDo.flag == 0) {
                            showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.server_error), "" + resources.getString(R.string.ok), "", "" + resources.getString(R.string.from_failure), false)

                        } else if (loginDo.flag == 5) {
                            showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.login_inactive), "" + resources.getString(R.string.ok), "", "" + resources.getString(R.string.from_failure), false)

                        } else if (loginDo.flag == 3) {
                            showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.login_another_device), "" + resources.getString(R.string.ok), "", "" + resources.getString(R.string.from_failure), false)

                        } else if (loginDo.flag == 4) {
                            showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.login_limit_exceeded), "" + resources.getString(R.string.ok), "", "" + resources.getString(R.string.from_failure), false)

                        } else {
                            showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.login_inactive), "" + resources.getString(R.string.ok), "", "" + resources.getString(R.string.from_failure), false)

                        }

                    }
                } else {
                    hideLoader()
                    showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.server_error), "" + resources.getString(R.string.ok), "", "" + resources.getString(R.string.from_failure), false)

                }

            }

            siteListRequest.execute()
        } else {
            showToast("" + resources.getString(R.string.internet_connection))

        }


    }

    override fun onBackPressed() {
    }
}