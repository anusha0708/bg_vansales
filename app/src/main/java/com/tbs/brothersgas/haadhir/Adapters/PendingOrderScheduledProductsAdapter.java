package com.tbs.brothersgas.haadhir.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.app.Dialog;
import android.content.Context;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDialog;
import androidx.appcompat.widget.SwitchCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.tbs.brothersgas.haadhir.Activitys.BaseActivity;
import com.tbs.brothersgas.haadhir.Activitys.ScheduledCaptureDeliveryActivity;
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryDO;
import com.tbs.brothersgas.haadhir.Model.ReasonMainDO;
import com.tbs.brothersgas.haadhir.R;
import com.tbs.brothersgas.haadhir.common.AppConstants;
import com.tbs.brothersgas.haadhir.listeners.LoadStockListener;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;

import java.util.ArrayList;

public class PendingOrderScheduledProductsAdapter extends RecyclerView.Adapter<PendingOrderScheduledProductsAdapter.MyViewHolder> implements Filterable {
    public ArrayList<ActiveDeliveryDO> activeDeliveryDOS;
    private String imageURL;
    private double weight = 0;
    private double volume = 0;
    ValueFilter valueFilter;
    private Context context;
    //    private ReasonMainDO reasonMainDo;
    private LoadStockListener loadStockListener;
    double mass = 0.0;
    int type = 0;
    String unit = "";
    public boolean isClickable = true;
    Dialog dialog;
     ReasonMainDO reasonMAINDO;

    @Override
    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }

    private class ValueFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {
                ArrayList<ActiveDeliveryDO> filterList = new ArrayList<>();
                for (int i = 0; i < activeDeliveryDOS.size(); i++) {
                    if ((activeDeliveryDOS.get(i).product.toUpperCase()).contains(constraint.toString().toUpperCase())) {
                        filterList.add(activeDeliveryDOS.get(i));
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = activeDeliveryDOS.size();
                results.values = activeDeliveryDOS;
            }
            return results;

        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            activeDeliveryDOS = (ArrayList<ActiveDeliveryDO>) results.values;
            notifyDataSetChanged();
        }

    }

    public void refreshAdapter(ArrayList<ActiveDeliveryDO> activeDeliveryDOS, int Type) {
        this.activeDeliveryDOS = activeDeliveryDOS;
        this.type = Type;
        notifyDataSetChanged();
    }

    public ArrayList<ActiveDeliveryDO> selectedactiveDeliveryDOs = new ArrayList<>();
    private String from = "";

    public PendingOrderScheduledProductsAdapter(Context context, ArrayList<ActiveDeliveryDO> activeDeliveryDOS, String from, String damagE, int Type) {
        this.context = context;
        this.activeDeliveryDOS = activeDeliveryDOS;
        this.from = from;
        this.type = Type;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.pending_load_stock_data, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final ActiveDeliveryDO activeDeliveryDO = activeDeliveryDOS.get(position);

        holder.tvProductName.setText(activeDeliveryDO.product);
        holder.tvDescription.setText(activeDeliveryDO.productDescription);
        holder.tvAvailableQty.setText("Stock : " + activeDeliveryDO.totalQuantity + " " + activeDeliveryDO.unit);




    }

    @Override
    public int getItemCount() {
        return activeDeliveryDOS != null ? activeDeliveryDOS.size() : 0;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvProductName, tvDescription, tvAvailableQty, tvInfo, tvNumber, etTotalMeterQty, etKGTotalMeterQty, tvSelection, tvProductUnit, tvKGProductUnit;
        public RelativeLayout rlRemove, rlAdd;
        public ImageView ivRemove, ivAdd, ivDelete;
        public LinearLayout llAddRemove, llMeterReadings, llKGMeterReadings, llPriceTag, llFoc;
        public EditText tvNumberET, etMeterReading1, etMeterReading2, etMeterReading3, etKGMeterReading1, etKGMeterReading2, etKGMeterReading3;
        public SwitchCompat swStatus;

        private CheckBox cbSelected;

        public MyViewHolder(View view) {
            super(view);
            tvProductName = (TextView) view.findViewById(R.id.tvName);
            tvDescription = (TextView) view.findViewById(R.id.tvDescription);
            tvNumber = (TextView) view.findViewById(R.id.tvNumber);
            ivRemove = (ImageView) view.findViewById(R.id.ivRemove);
            ivAdd = (ImageView) view.findViewById(R.id.ivAdd);
            tvNumberET = (EditText) view.findViewById(R.id.tvNumberET);
            swStatus = (SwitchCompat) view.findViewById(R.id.swStatusCustom);

            tvAvailableQty = (TextView) view.findViewById(R.id.tvAvailableQty);
            tvInfo = (TextView) view.findViewById(R.id.tvInfo);
            tvProductUnit = (TextView) view.findViewById(R.id.tvProductUnit);
            etMeterReading1 = (EditText) view.findViewById(R.id.etMeterReading1);
            etMeterReading2 = (EditText) view.findViewById(R.id.etMeterReading2);
            etMeterReading3 = (EditText) view.findViewById(R.id.etMeterReading3);
            etTotalMeterQty = (TextView) view.findViewById(R.id.etTotalMeterQty);
            etKGMeterReading1 = (EditText) view.findViewById(R.id.etKGMeterReading1);
            etKGMeterReading2 = (EditText) view.findViewById(R.id.etKGMeterReading2);
            etKGMeterReading3 = (EditText) view.findViewById(R.id.etKGMeterReading3);
            etKGTotalMeterQty = (TextView) view.findViewById(R.id.etKGTotalMeterQty);
            llMeterReadings = (LinearLayout) view.findViewById(R.id.llMeterReadings);
            tvKGProductUnit = (TextView) view.findViewById(R.id.tvKGProductUnit);
            llKGMeterReadings = (LinearLayout) view.findViewById(R.id.llKGMeterReadings);
            ivDelete = view.findViewById(R.id.ivDelete);
            cbSelected = view.findViewById(R.id.cbSelected);
            llAddRemove = (LinearLayout) view.findViewById(R.id.llAddRemove);
            tvSelection = (TextView) view.findViewById(R.id.tvSelection);
            llPriceTag = (LinearLayout) view.findViewById(R.id.llPriceTag);
            llFoc = (LinearLayout) view.findViewById(R.id.llFoc);

        }
    }

    class ReasonAdapter extends BaseAdapter {
        private TextView tvName;

        @Override
        public Object getItem(int i) {
            return i;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = inflater.inflate(R.layout.simple_text, null);
            tvName = (TextView) v.findViewById(R.id.tvName);
            tvName.setText(reasonMAINDO.reasonDOS.get(i).reason);


            return v;
        }

        @Override
        public int getCount() {
            if (reasonMAINDO.reasonDOS.size() > 0)
                return reasonMAINDO.reasonDOS.size();
            else
                return 0;
        }

    }


}
