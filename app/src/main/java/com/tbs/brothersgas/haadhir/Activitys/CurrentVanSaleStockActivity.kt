package com.tbs.brothersgas.haadhir.Activitys

import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.tbs.brothersgas.haadhir.R

//
class CurrentVanSaleStockActivity : BaseActivity() {

    override  protected fun onResume() {
        super.onResume()
    }
    override fun initialize() {
      var  llCategories = getLayoutInflater().inflate(R.layout.current_van_sales_stock, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
    }
    override fun initializeControls() {
        tvScreenTitle.setText(R.string.current_stock)

    }
}