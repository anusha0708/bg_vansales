package com.tbs.brothersgas.haadhir.Activitys

import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import android.widget.ImageView
import com.tbs.brothersgas.haadhir.R


/**
 *Created by VenuAppasani on 22-12-2018.
 *Copyright (C) 2018 TBS - All Rights Reserved
 */
class DisplayBillDetailsActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var signatureButton: Button
    lateinit var signImage: ImageView
    lateinit var checkBox: CheckBox

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.display_invoice_bill_details)
        signatureButton = findViewById<View>(R.id.getSign) as Button
        signImage = findViewById<View>(R.id.imageView1) as ImageView
        signatureButton.setOnClickListener(this@DisplayBillDetailsActivity)
        checkBox = findViewById<View>(R.id.checkbox) as CheckBox
        //disable button if checkbox is not checked else enable button
        checkBox.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                signatureButton.setEnabled(true)
            } else {
                signatureButton.setEnabled(false)
            }
        }
        //to get imagepath from SignatureActivity and set it on ImageView
        val image_path = intent.getStringExtra("imagePath")
        val bitmap = BitmapFactory.decodeFile(image_path)
        signImage.setImageBitmap(bitmap)
    }

    override fun onClick(p0: View?) {
        val i = Intent(this@DisplayBillDetailsActivity, TBSSignatureActivity::class.java)
        startActivity(i)
       /* if (checkBox.isChecked) {
            val i = Intent(this@DisplayBillDetailsActivity, TBSSignatureActivity::class.java)
            startActivity(i)
        } else {
            signatureButton.setEnabled(false)
        }*/
    }
}