package com.tbs.brothersgas.haadhir.fragments

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.tbs.brothersgas.haadhir.Activitys.BaseActivity
import com.tbs.brothersgas.haadhir.Adapters.LoadStockAdapter
import com.tbs.brothersgas.haadhir.Model.LoadStockDO
import com.tbs.brothersgas.haadhir.Model.LoadStockMainDO
import com.tbs.brothersgas.haadhir.Model.NonScheduledProductMainDO
import com.tbs.brothersgas.haadhir.Model.ProductDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils

/**
 * Created by sandy on 2/15/2018.
 */

class LoadStockScheduledFragmment : Fragment() {
    lateinit var productDOS: List<ProductDO>
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var btnCompleted: Button
    lateinit var loadStockMainDO: LoadStockMainDO
    lateinit var tvNoDataFound : TextView
    lateinit var loadStockDOs : ArrayList<LoadStockDO>;
    lateinit var nonScheduledProductMainDO: NonScheduledProductMainDO
    var driverID = "";

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadStockDOs = arguments!!.getSerializable("LoadData") as ArrayList<LoadStockDO>
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var v = inflater.inflate(R.layout.unsheduled_fragment, container, false)
        recycleview = v.findViewById<View>(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        tvNoDataFound = v.findViewById<View>(R.id.tvNoDataFound) as TextView
        recycleview.setLayoutManager(androidx.recyclerview.widget.LinearLayoutManager(activity, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false))
        val name = (activity as BaseActivity ).preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_NAME, "")
        val date = (activity as BaseActivity ).preferenceUtils.getStringFromPreference(PreferenceUtils.CHECKIN_DATE, "")
        driverID         = (activity as BaseActivity ).preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "QUA02")
        var routeId= (activity as BaseActivity ).preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "")

//        if(activity is CurrentVanSaleStockTabActivity){
//            loadStockDOs = StorageManager.getInstance(activity).getVehicleScheduleData()//
//        }
//        else{
//            loadStockDOs = StorageManager.getInstance(activity).getVanScheduleProducts(activity)
//        }

        if (loadStockDOs!=null && loadStockDOs.size>0) {
            var loadStockAdapter = LoadStockAdapter(context, loadStockDOs, "")
            recycleview.setAdapter(loadStockAdapter)
            tvNoDataFound.visibility = View.GONE
            recycleview.visibility = View.VISIBLE
        }
        else {
            tvNoDataFound.visibility = View.VISIBLE
            recycleview.visibility = View.GONE
        }
        return v
    }
}


private operator fun Double.plus(load: String?): Any {

    return load!!
}
