//package com.tbs.brothersgas.haadhir.collector
//
//import android.app.Dialog
//import android.content.Intent
//import android.support.v7.widget.LinearLayoutManager
//import android.support.v7.widget.RecyclerView
//import android.view.View
//import android.view.ViewGroup
//import android.view.WindowManager
//import android.widget.*
//import com.tbs.brothersgas.haadhir.Activitys.BaseActivity
//import com.tbs.brothersgas.haadhir.Activitys.CreatePaymentActivity
//import com.tbs.brothersgas.haadhir.Adapters.CreatePaymentAdapter
//import com.tbs.brothersgas.haadhir.Adapters.InvoiceAdapter
//import com.tbs.brothersgas.haadhir.Model.CustomerDo
//import com.tbs.brothersgas.haadhir.Model.LoadStockDO
//import com.tbs.brothersgas.haadhir.Model.LoanReturnDO
//import com.tbs.brothersgas.haadhir.R
//import com.tbs.brothersgas.haadhir.Requests.AllUnPaidInvoicesListRequest
//import com.tbs.brothersgas.haadhir.Requests.InvoiceHistoryRequest
//import com.tbs.brothersgas.haadhir.Requests.UnPaidInvoicesListRequest
//import com.tbs.brothersgas.haadhir.database.StorageManager
//import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
//import com.tbs.brothersgas.haadhir.utils.Util
//import java.util.ArrayList
//
//
//class CreditInvoiceListActivity : BaseActivity() {
//    lateinit var invoiceAdapter: com.tbs.brothersgas.haadhir.collector.CreatePaymentAdapter
//    lateinit var loadStockDOs: ArrayList<LoadStockDO>
//    lateinit var recycleview: RecyclerView
//    lateinit var tvNoDataFound: TextView
//    lateinit var view: LinearLayout
//    var fromId = 0
//    private lateinit var siteListRequest: CrediUnPaidInvoicesListRequest
//    private lateinit var siteListRequest2: CollectorAllUnPaidInvoicesListRequest
//    lateinit var tvAmount: TextView
//    lateinit var tvNoData: TextView
//
//    var customerid = ""
//    private lateinit var btnAddMore: Button
//    private var selectedCustomerDOs = ArrayList<CustomerDo>()
//
//    override fun onResume() {
//        selectInvoiceList()
//        super.onResume()
//    }
//
//    override fun initialize() {
//        var llCategories = getLayoutInflater().inflate(R.layout.credit_invoice_list, null) as RelativeLayout
//        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
//        initializeControls()
//
//        toolbar.setNavigationIcon(R.drawable.back)
//        toolbar.setNavigationOnClickListener { finish() }
//    }
//
//    override fun initializeControls() {
//        tvScreenTitle.setText("Select Invoice")
//        recycleview = findViewById(R.id.recycleview) as RecyclerView
//        val linearLayoutManager = LinearLayoutManager(this)
//        tvNoDataFound = findViewById(R.id.tvNoDataFound) as TextView
//        view = findViewById(R.id.view) as LinearLayout
//        btnAddMore = findViewById(R.id.btnAddMore) as Button
//
//        recycleview.setLayoutManager(linearLayoutManager)
//
//        selectInvoiceList()
//
//        ivViewAll.visibility = View.VISIBLE
//        ivViewAll.setOnClickListener {
//            selectALLInvoiceList()
//        }
//        if (intent.hasExtra("CODE")) {
//            customerid = intent.extras.getString("CODE")
//        }
//        val btnCreate = findViewById(R.id.btnCreate) as Button
//
//        btnCreate.setOnClickListener {
//            val intent = Intent(this@CreditInvoiceListActivity, com.tbs.brothersgas.haadhir.collector.CreatePaymentActivity::class.java);
//            intent.putExtra("Sales", 1)
//            intent.putExtra("CODE", customerid)
//
//            startActivity(intent);
//        }
//        btnAddMore.setOnClickListener {
//            if (invoiceAdapter != null) {
//                val selectedUnPaidInvoiceDOS = invoiceAdapter.getselectedUnPaidInvoiceDOS();
//                if (selectedUnPaidInvoiceDOS != null && !selectedUnPaidInvoiceDOS.isEmpty()) {
//                    val intent = Intent(this@CreditInvoiceListActivity, CreditInvoiceListActivity::class.java);
//                    intent.putExtra("selectedInvoiceDOs", selectedUnPaidInvoiceDOS);
//                    startActivityForResult(intent, 11);
//                } else {
//                    showToast("Please select Invoices")
//                }
//            } else {
//                showToast("No Invoices found")
//            }
//
//
//        }
//    }
//
//    private fun selectInvoiceList() {
//
//
////        var tvInvoiceId = findViewById(R.id.tvInvoiceId) as TextView
//         tvNoData = findViewById(R.id.tvNoDataFound) as TextView
//         tvAmount = findViewById(R.id.tvAmount) as TextView
//
//
//        recycleview.setLayoutManager(LinearLayoutManager(this));
//        val activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)
//        val customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)
//
//        if (intent.hasExtra("CODE")) {
//            customerid = intent.extras.getString("CODE")
//        }
//        if (intent.hasExtra("selectedCustomerDOs")) {
//            selectedCustomerDOs = intent.getSerializableExtra("selectedCustomerDOs") as ArrayList<CustomerDo>
//        }
//        val ShipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, getResources().getString(R.string.checkin_non_scheduled))
//
//        siteListRequest = CrediUnPaidInvoicesListRequest(selectedCustomerDOs, this@CreditInvoiceListActivity)
//
//        siteListRequest.setOnResultListener { isError, unPaidInvoiceMainDO ->
//            hideLoader()
//            if (unPaidInvoiceMainDO != null && unPaidInvoiceMainDO.unPaidInvoiceDOS.size > 0) {
//
////                    tvInvoiceId.setVisibility(View.VISIBLE)
//                tvAmount.setVisibility(View.VISIBLE)
//                tvAmount.setText("" + String.format("%.2f", unPaidInvoiceMainDO.totalAmount) + " " + unPaidInvoiceMainDO.unPaidInvoiceDOS.get(0).currency)
//
//                preferenceUtils.saveString(PreferenceUtils.TOTAL_AMOUNT, String.format("%.2f", unPaidInvoiceMainDO.totalAmount))
//                preferenceUtils.saveInt(PreferenceUtils.PAYMENT_TYPE, unPaidInvoiceMainDO.type)
//
//
//                tvNoData.setVisibility(View.GONE)
//                unPaidInvoiceMainDO
//                if (isError) {
//                    Toast.makeText(this@CreditInvoiceListActivity, resources.getString(R.string.error_NoData), Toast.LENGTH_SHORT).show()
//                } else {
//                    tvAmount.setVisibility(View.VISIBLE)
//                    view.setVisibility(View.VISIBLE)
//
//                    invoiceAdapter = com.tbs.brothersgas.haadhir.collector.CreatePaymentAdapter(this@CreditInvoiceListActivity, unPaidInvoiceMainDO.unPaidInvoiceDOS, customerid, "CREDIT")
//                    recycleview.setAdapter(invoiceAdapter)
//                    tvAmount.setText("" + String.format("%.2f", unPaidInvoiceMainDO.totalAmount) + " " + unPaidInvoiceMainDO.unPaidInvoiceDOS.get(0).currency)
//                    preferenceUtils.saveString(PreferenceUtils.TOTAL_AMOUNT, "" + String.format("%.2f", unPaidInvoiceMainDO.totalAmount))
//
////                    var siteAdapter = CreatePaymentAdapter(this@CreatePaymentActivity, createPaymentDo)
////                    recyclerView.setAdapter(siteAdapter)f
//
//                }
//            } else {
////                tvInvoiceId.setVisibility(View.VISIBLE)
//                tvAmount.setVisibility(View.GONE)
//
//                tvNoData.setVisibility(View.VISIBLE)
//                recycleview.setVisibility(View.GONE)
//                view.setVisibility(View.GONE)
//
//                hideLoader()
//
//            }
//
//        }
//
//        siteListRequest.execute()
//
//
//    }
//    private fun selectALLInvoiceList() {
//
//        val activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)
//        val customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)
//
//        if (Util.isNetworkAvailable(this)) {
//
//            val ShipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, getResources().getString(R.string.checkin_non_scheduled))
//            if (ShipmentType == getResources().getString(R.string.checkin_non_scheduled)) {
//
//                siteListRequest2 = CollectorAllUnPaidInvoicesListRequest(selectedCustomerDOs, this@CreditInvoiceListActivity)
//            } else {
//                siteListRequest2 = CollectorAllUnPaidInvoicesListRequest(selectedCustomerDOs, this@CreditInvoiceListActivity)
//            }
//            siteListRequest2.setOnResultListener { isError, unPaidInvoiceMainDO ->
//                hideLoader()
//                if (unPaidInvoiceMainDO != null && unPaidInvoiceMainDO.unPaidInvoiceDOS.size > 0) {
//
////                    tvInvoiceId.setVisibility(View.VISIBLE)
//                    tvAmount.setVisibility(View.VISIBLE)
//                    tvAmount.setText("" + String.format("%.2f", unPaidInvoiceMainDO.totalAmount) + " " + unPaidInvoiceMainDO.unPaidInvoiceDOS.get(0).currency)
//
//                    preferenceUtils.saveString(PreferenceUtils.TOTAL_AMOUNT, String.format("%.2f", unPaidInvoiceMainDO.totalAmount))
//                    preferenceUtils.saveInt(PreferenceUtils.PAYMENT_TYPE, unPaidInvoiceMainDO.type)
//
//
//                    tvNoData.setVisibility(View.GONE)
////                unPaidInvoiceMainDO
//                    if (isError) {
//                        hideLoader()
//                        hideLoader()
//
//                        Toast.makeText(this@CreditInvoiceListActivity, resources.getString(R.string.error_NoData), Toast.LENGTH_SHORT).show()
//                    } else {
//                        hideLoader()
//                        hideLoader()
//
//                        tvAmount.setVisibility(View.VISIBLE)
//                        view.setVisibility(View.VISIBLE)
//                        recycleview.setVisibility(View.VISIBLE)
//
//                        var siteAdapter = CreatePaymentAdapter(this@CreditInvoiceListActivity, unPaidInvoiceMainDO.unPaidInvoiceDOS)
//                        recycleview.setAdapter(siteAdapter)
//                        tvAmount.setText("" + String.format("%.2f", unPaidInvoiceMainDO.totalAmount) + " " + unPaidInvoiceMainDO.unPaidInvoiceDOS.get(0).currency)
//                        preferenceUtils.saveString(PreferenceUtils.TOTAL_AMOUNT, "" + String.format("%.2f", unPaidInvoiceMainDO.totalAmount))
//
////                    var siteAdapter = CreatePaymentAdapter(this@CreatePaymentActivity, createPaymentDo)
////                    recyclerView.setAdapter(siteAdapter)f
//
//                    }
//                } else {
////                tvInvoiceId.setVisibility(View.VISIBLE)
//                    tvAmount.setVisibility(View.GONE)
//
//                    tvNoData.setVisibility(View.VISIBLE)
//                    recycleview.setVisibility(View.GONE)
//                    view.setVisibility(View.VISIBLE)
//
//                    hideLoader()
//
//                }
//
//            }
//
//            siteListRequest2.execute()
//        } else {
//            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "", false)
//
//        }
//
//
//    }
//
//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        if (requestCode == 1 && resultCode == 1) {
//            selectInvoiceList()
//        }
//    }
//
//
//}