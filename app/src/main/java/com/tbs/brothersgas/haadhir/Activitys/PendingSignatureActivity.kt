package com.tbs.brothersgas.haadhir.Activitys

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.*
import android.content.pm.PackageManager
import android.graphics.*
import android.location.Geocoder
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.Looper
import androidx.core.app.ActivityCompat
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.webkit.PermissionRequest
import android.widget.*
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.single.PermissionListener
import com.tbs.brothersgas.haadhir.Adapters.ScheduledProductsAdapter
import com.tbs.brothersgas.haadhir.Model.CustomerManagementDO
import com.tbs.brothersgas.haadhir.Model.PodDo
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.*
import com.tbs.brothersgas.haadhir.common.AppConstants
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.pdfs.BulkDeliveryNotePDF
import com.tbs.brothersgas.haadhir.pdfs.CYLDeliveryNotePDF
import com.tbs.brothersgas.haadhir.pdfs.CylinderIssRecPdfBuilder
import com.tbs.brothersgas.haadhir.prints.PrinterConnection
import com.tbs.brothersgas.haadhir.prints.PrinterConstants
import com.tbs.brothersgas.haadhir.utils.CalendarUtils
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.text.DateFormat
import java.util.*

/**
 *Created by VenuAppasani on 22-12-2018.
 *Copyright (C) 2018 TBS - All Rights Reserved
 */
class PendingSignatureActivity : BaseActivity(), View.OnClickListener {


    lateinit var mClear: Button
    lateinit var mGetSign: Button
    lateinit var mCancel: Button
    lateinit var file: File
    lateinit var mContent: LinearLayout
    lateinit var view: View
    lateinit var mSignature: signature
    var bitmap: Bitmap? = null
    var converetdImage: Bitmap? = null

    lateinit var savedPath: String
    lateinit var encodedImage: String
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var tvShipmentNumber: TextView
    lateinit var tvPaymentTerm: TextView
    lateinit var tvDeliveryRemarks: TextView


    // Creating Separate Directory for saving Generated Images
    var DIRECTORY = Environment.getExternalStorageDirectory().getPath() + "/UserSignature/"
    var pic_name = /*SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())*/"signature"
    var StoredPath = DIRECTORY + pic_name + ".png"
    private var mLastUpdateTime: String? = null
    private val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 10000
    private val FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS: Long = 5000
    private val REQUEST_CHECK_SETTINGS = 100
    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private var mSettingsClient: SettingsClient? = null
    private var mLocationRequest: LocationRequest? = null
    private var mLocationSettingsRequest: LocationSettingsRequest? = null
    private var mLocationCallback: LocationCallback? = null
    private var mCurrentLocation: Location? = null
    private var mRequestingLocationUpdates: Boolean? = null
    @SuppressLint("MissingPermission")
    override fun initializeControls() {
        location()
        mContent = findViewById<View>(R.id.canvasLayout) as LinearLayout
        mSignature = signature(applicationContext, null)
        mSignature.setBackgroundColor(Color.WHITE)
        recycleview = findViewById<View>(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        recycleview.setLayoutManager(androidx.recyclerview.widget.LinearLayoutManager(this))
        // Dynamically generating Layout through java code
        mContent.addView(mSignature, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        mClear = findViewById<View>(R.id.clear) as Button
        mGetSign = findViewById<View>(R.id.getsign) as Button
        tvShipmentNumber = findViewById<View>(R.id.tvShipmentNumber) as TextView
        tvPaymentTerm = findViewById<View>(R.id.tvPayementTerm) as TextView
        tvDeliveryRemarks = findViewById<View>(R.id.tvDeliveryRemarks) as TextView




        mGetSign.setEnabled(false)
        mCancel = findViewById<View>(R.id.cancel) as Button
        view = mContent
        mGetSign.setOnClickListener(this@PendingSignatureActivity)
        mClear.setOnClickListener(this@PendingSignatureActivity)
        mCancel.setOnClickListener(this@PendingSignatureActivity)

        // Method to create Directory, if the Directory doesn't exists
        file = File(DIRECTORY)
        if (!file.exists()) {
            file.mkdir()
        }
        tvScreenTitle.setText("Confirm & Sign Here")

        if (Util.isNetworkAvailable(this)) {
            shipmentDetails()

        } else {
            showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

        }


    }


    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.activity_digital_signature, null) as ScrollView
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()
        disableMenuWithBackButton()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            showToast("Please save signature")
        }
    }


    private fun shipmentDetails() {
        val spotSaleDeliveryId = preferenceUtils.getStringFromPreference(PreferenceUtils.PENDING_DELIVERY_NUMBER, "")

        if (spotSaleDeliveryId.length > 0) {
            val driverListRequest = ActiveDeliveryRequest(spotSaleDeliveryId, this@PendingSignatureActivity)
            driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                hideLoader()
                if (isError) {

                    Toast.makeText(this@PendingSignatureActivity, R.string.server_error, Toast.LENGTH_SHORT).show()
                } else {
                    StorageManager.getInstance(this).saveORDERActiveDeliveryMainDo(this, activeDeliveryDo)
                    tvShipmentNumber.visibility = View.VISIBLE
                    tvPaymentTerm.visibility = View.VISIBLE
                    tvShipmentNumber.setText("Delivery No. - " + spotSaleDeliveryId)
                    tvPaymentTerm.setText("Payment Term - " + activeDeliveryDo.paymentTerm)


                    var loadStockAdapter = ScheduledProductsAdapter(this@PendingSignatureActivity, activeDeliveryDo.activeDeliveryDOS, "Shipments", "", 0)
                    recycleview.setAdapter(loadStockAdapter)
                }
            }
            driverListRequest.execute()

        }
    }

    override fun onClick(v: View?) {
        // TODO Auto-generated method stub
        if (v === mClear) {
            mSignature.clear()
            mGetSign.setEnabled(false)
        } else if (v === mGetSign) {

            if (!lattitudeFused.isNullOrEmpty()) {
                stopLocationUpdates()

                if (Build.VERSION.SDK_INT >= 23) {
                    if (isStoragePermissionGranted()) {

                        showAppCompatAlert("", "Are You Sure You Want to \n Confirm Delivery?", "OK", "Cancel", "SUCCESS", true)

                    }
                } else {

                    showAppCompatAlert("", "Are You Sure You Want to \n Confirm Delivery?", "OK", "Cancel", "SUCCESS", true)

                }
            } else {
                showLoader()
                stopLocationUpdates()
                locationFetch()
//                showAppCompatAlert("", resources.getString(R.string.location_capture), "OK", "", "", false)
            }


        } else if (v === mCancel) {
            showToast("Please save signature")
        }
    }

    fun isStoragePermissionGranted(): Boolean {
        if (Build.VERSION.SDK_INT >= 23) {
            if (applicationContext.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                return true
            } else {
                ActivityCompat.requestPermissions(this, arrayOf<String>(Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)
                return false
            }
        } else {
            return true
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            view.setDrawingCacheEnabled(true)
            mSignature.save(view, StoredPath)
//            Toast.makeText(applicationContext, "Successfully Saved", Toast.LENGTH_SHORT).show()
            // Calling the same class
            recreate()
        } else {
            Toast.makeText(this, "The app was not allowed to write to your storage. Hence, it cannot function properly. Please consider granting it this permission", Toast.LENGTH_LONG).show()
        }
    }

    inner class signature(context: Context, attrs: AttributeSet?) : View(context, attrs) {
        private val paint = Paint()
        private val path = Path()

        private var lastTouchX: Float = 0.toFloat()
        private var lastTouchY: Float = 0.toFloat()
        private val dirtyRect = RectF()

        init {
            paint.setAntiAlias(true)
            paint.setColor(Color.BLACK)
            paint.setStyle(Paint.Style.STROKE)
            paint.setStrokeJoin(Paint.Join.ROUND)
            paint.setStrokeWidth(STROKE_WIDTH)
        }

        fun save(v: View, StoredPath: String) {
            Log.v("log_tag", "Width: " + v.getWidth())
            Log.v("log_tag", "Height: " + v.getHeight())
            if (bitmap == null) {
                bitmap = Bitmap.createBitmap(mContent.width, mContent.height, Bitmap.Config.RGB_565)
            }
            val canvas = Canvas(bitmap)
            try {
                val mFileOutStream = FileOutputStream(StoredPath)
                v.draw(canvas)

                bitmap!!.compress(Bitmap.CompressFormat.PNG, 2, mFileOutStream)

                mFileOutStream.flush()
                mFileOutStream.close()

                val bos = ByteArrayOutputStream();
                bitmap!!.compress(Bitmap.CompressFormat.JPEG, 2, bos);
                val bitmapdata = bos.toByteArray();
                Log.e("Signature Size : ", "Size : " + bitmapdata)
                encodedImage = android.util.Base64.encodeToString(bitmapdata, android.util.Base64.DEFAULT)
                savedPath = StoredPath
                if (Util.isNetworkAvailable(context)) {
//                    if (!lattitudeFused.isNullOrEmpty()) {
//                        stopLocationUpdates()
//
//                    }else{
//                        location()
//                        showAppCompatAlert("", resources.getString(R.string.location_capture), "OK", "", "", false)
//                    }

                    validatedelivery()


                } else {

                    showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

                }

            } catch (e: Exception) {
                Log.v("log_tag", e.toString())
            }

        }

        fun clear() {
            path.reset()
            invalidate()
            mGetSign.setEnabled(false)
        }

        fun scaleDown(realImage: Bitmap, maxImageSize: Float,
                      filter: Boolean): Bitmap {
            val ratio = Math.min(
                    maxImageSize.toFloat() / realImage.getWidth(),
                    maxImageSize.toFloat() / realImage.getHeight())
            val width = Math.round(ratio.toFloat() * realImage.getWidth())
            val height = Math.round(ratio.toFloat() * realImage.getHeight())
            val newBitmap = Bitmap.createScaledBitmap(realImage, width,
                    height, filter)
            return newBitmap
        }

        protected override fun onDraw(canvas: Canvas) {
            canvas.drawPath(path, paint)
        }

        override fun onTouchEvent(event: MotionEvent): Boolean {
            val eventX = event.x
            val eventY = event.y
            mGetSign.setEnabled(true)

            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    path.moveTo(eventX, eventY)
                    lastTouchX = eventX
                    lastTouchY = eventY
                    return true
                }

                MotionEvent.ACTION_MOVE,

                MotionEvent.ACTION_UP -> {

                    resetDirtyRect(eventX, eventY)
                    val historySize = event.historySize
                    for (i in 0 until historySize) {
                        val historicalX = event.getHistoricalX(i)
                        val historicalY = event.getHistoricalY(i)
                        expandDirtyRect(historicalX, historicalY)
                        path.lineTo(historicalX, historicalY)
                    }
                    path.lineTo(eventX, eventY)
                }

                else -> {
                    debug("Ignored touch event: " + event.toString())
                    return false
                }
            }

            invalidate((dirtyRect.left - HALF_STROKE_WIDTH).toInt(),
                    (dirtyRect.top - HALF_STROKE_WIDTH).toInt(),
                    (dirtyRect.right + HALF_STROKE_WIDTH).toInt(),
                    (dirtyRect.bottom + HALF_STROKE_WIDTH).toInt())

            lastTouchX = eventX
            lastTouchY = eventY

            return true
        }

        private fun debug(string: String) {

            Log.v("log_tag", string)

        }

        private fun expandDirtyRect(historicalX: Float, historicalY: Float) {
            if (historicalX < dirtyRect.left) {
                dirtyRect.left = historicalX
            } else if (historicalX > dirtyRect.right) {
                dirtyRect.right = historicalX
            }

            if (historicalY < dirtyRect.top) {
                dirtyRect.top = historicalY
            } else if (historicalY > dirtyRect.bottom) {
                dirtyRect.bottom = historicalY
            }
        }

        private fun resetDirtyRect(eventX: Float, eventY: Float) {
            dirtyRect.left = Math.min(lastTouchX, eventX)
            dirtyRect.right = Math.max(lastTouchX, eventX)
            dirtyRect.top = Math.min(lastTouchY, eventY)
            dirtyRect.bottom = Math.max(lastTouchY, eventY)
        }

    }


    companion object {
        private val STROKE_WIDTH = 5f
        private val HALF_STROKE_WIDTH = STROKE_WIDTH / 2
    }

    private fun validatedelivery() {
        val shipmentNumber = preferenceUtils.getStringFromPreference(PreferenceUtils.PENDING_DELIVERY_NUMBER, "")

        val driverListRequest = ValidateDeliveryRequest(0.0,encodedImage,"", shipmentNumber, lattitudeFused, longitudeFused, addressFused, this@PendingSignatureActivity)
        driverListRequest.setOnResultListener { isError, validateDo ->
            hideLoader()
            if (isError) {
                hideLoader()

                showAppCompatAlert("", "Error in Service Response, Please try again", "Ok", "", "", false)
            } else {
                stopLocUpdates()
                if (validateDo.flag == 1 && validateDo.flag2 == 2) {
                    hideLoader()

                    if (validateDo.message.length > 0) {
                        showToast("" + validateDo.message)

                    } else {

                        Toast.makeText(this@PendingSignatureActivity, "Shipment Validated", Toast.LENGTH_SHORT).show()
                    }
                    prepareDeliveryNoteCreation()

                } else if (validateDo.flag == 1 && validateDo.flag2 == 1) {
                    hideLoader()

                    if (validateDo.message.length > 0) {
                        showToast("" + validateDo.message)

                    } else {

                        Toast.makeText(this@PendingSignatureActivity, "Shipment Validated", Toast.LENGTH_SHORT).show()
                    }
                    prepareDeliveryNoteCreation()


                } else if (validateDo.flag == 2 && validateDo.flag2 == 2) {

                    if (validateDo.message.length > 0) {
                        showToast("" + validateDo.message)

                    } else {

                        Toast.makeText(this@PendingSignatureActivity, "Shipment already validated", Toast.LENGTH_SHORT).show()
                    }
                   prepareDeliveryNoteCreation()

                } else {
                    if (validateDo.message.length > 0) {
                        showToast("" + validateDo.message)
                    } else {
                        showToast("Transaction failed, Please contact support team and try again")
                    }

                }

            }
        }
        driverListRequest.execute()


    }


    private fun prepareDeliveryNoteCreation() {
        val spotSaleDeliveryId = preferenceUtils.getStringFromPreference(PreferenceUtils.PENDING_DELIVERY_NUMBER, "")

            if (spotSaleDeliveryId.length > 0) {
                val driverListRequest = ActiveDeliveryRequest(spotSaleDeliveryId, this@PendingSignatureActivity)
                driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                    hideLoader()
                    if (activeDeliveryDo != null) {
                        if (isError) {
                            showToast("Server Error. Please Try again!!")
//                            showAppCompatAlert("Error", "Delivery Note pdf data error", "Ok", "", "", false)
                        } else {
                            printDocument(activeDeliveryDo, PrinterConstants.PrintCylinderDeliveryNotes);
                            CYLDeliveryNotePDF(this).createDeliveryPDF(activeDeliveryDo, "Email").CreateDeliveryPDF().execute()



                        }
                    } else {
                        showToast("Server Error. Please Try again!!")
                    }
                }
                driverListRequest.execute()


        }

        val intent = Intent()
        intent.putExtra("SIGNATURE", StoredPath)
        intent.putExtra("SignatureEncode", encodedImage)

        setResult(11, intent)
        finish()

    }

    private fun printDocument(obj: Any, from: Int) {
        val printConnection = PrinterConnection.getInstance(this@PendingSignatureActivity)
        if (printConnection.isBluetoothEnabled()) {
            printConnection.connectToPrinter(obj, from)
        } else {
//            PrinterConstants.PRINTER_MAC_ADDRESS = ""
            showToast("Please enable your mobile Bluetooth.")
//            showAppCompatAlert("", "Please enable your mobile Bluetooth.", "Enable", "Cancel", "EnableBluetooth", false)
        }
    }

    private fun pairedDevices() {
        val pairedDevices = BluetoothAdapter.getDefaultAdapter().getBondedDevices()
        if (pairedDevices.size > 0) {
            for (device in pairedDevices) {
                val deviceName = device.getName()
                val mac = device.getAddress() // MAC address
//                if (StorageManager.getInstance(this).getPrinterMac(this).equals("")) {
                StorageManager.getInstance(this).savePrinterMac(this, mac);
//                }
                Log.e("Bluetooth", "Name : " + deviceName + " Mac : " + mac)
                break
            }
        }
    }

    override fun onResume() {
        super.onResume()
        pairedDevices()
    }

    override fun onStart() {
        super.onStart()
        val filter = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        registerReceiver(mReceiver, filter)

    }

    override fun onDestroy() {
        unregisterReceiver(mReceiver)
//        PrinterConstants.PRINTER_MAC_ADDRESS = ""
        hideLoader()
        super.onDestroy()
    }

    private val mReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            if (BluetoothDevice.ACTION_FOUND == action) {
                val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE);
                PrinterConstants.bluetoothDevices.add(device)
                Log.e("Bluetooth", "Discovered => name : " + device.name + ", Mac : " + device.address)
            } else if (action.equals(BluetoothDevice.ACTION_ACL_CONNECTED)) {
                Log.e("Bluetooth", "status : ACTION_ACL_CONNECTED")
                pairedDevices()
            } else if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                val state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
                if (state == BluetoothAdapter.STATE_OFF) {
                    Log.e("Bluetooth", "status : STATE_OFF")
                } else if (state == BluetoothAdapter.STATE_TURNING_OFF) {
                    Log.e("Bluetooth", "status : STATE_TURNING_OFF")
                } else if (state == BluetoothAdapter.STATE_ON) {
                    Log.e("Bluetooth", "status : STATE_ON")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_TURNING_ON) {
                    Log.e("Bluetooth", "status : STATE_TURNING_ON")
                } else if (state == BluetoothAdapter.STATE_CONNECTING) {
                    Log.e("Bluetooth", "status : STATE_CONNECTING")
                } else if (state == BluetoothAdapter.STATE_CONNECTED) {
                    Log.e("Bluetooth", "status : STATE_CONNECTED")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_DISCONNECTED) {
                    Log.e("Bluetooth", "status : STATE_DISCONNECTED")
                }
            }
        }
    }



    override fun onBackPressed() {
        showToast("Please save signature")
    }

    override fun onButtonYesClick(from: String) {

        if ("SUCCESS".equals(from, ignoreCase = true)) {
            view.setDrawingCacheEnabled(true)
            mSignature.save(view, StoredPath)
        } else {

        }
    }


    fun locationFetch() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mSettingsClient = LocationServices.getSettingsClient(this)

        mLocationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                super.onLocationResult(locationResult)
                // location is received
                mCurrentLocation = locationResult!!.lastLocation
                mLastUpdateTime = DateFormat.getTimeInstance().format(Date())

                updateLocUI()
            }
        }

        mRequestingLocationUpdates = false

        mLocationRequest = LocationRequest()
        mLocationRequest!!.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS)
        mLocationRequest!!.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS)
        mLocationRequest!!.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)

        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(mLocationRequest!!)
        mLocationSettingsRequest = builder.build()

        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(object : PermissionListener {
                    override fun onPermissionRationaleShouldBeShown(permission: com.karumi.dexter.listener.PermissionRequest?, token: PermissionToken?) {
                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }

                    override fun onPermissionGranted(response: PermissionGrantedResponse) {
                        mRequestingLocationUpdates = true
                        startLocUpdates()
                    }

                    override fun onPermissionDenied(response: PermissionDeniedResponse) {
                        if (response.isPermanentlyDenied()) {

                            showToast("Location Permission denied")

                        }
                    }

                    fun onPermissionRationaleShouldBeShown(permission: PermissionRequest, token: PermissionToken) {
                        token.continuePermissionRequest()
                    }
                }).check()
    }

    fun updateLocUI() {
        if (mCurrentLocation != null) {
            stopLocUpdates()
            hideLoader()

            lattitudeFused = mCurrentLocation!!.getLatitude().toString()
            longitudeFused = mCurrentLocation!!.getLongitude().toString()
            var gcd = Geocoder(getBaseContext(), Locale.getDefault());

            try {
                var addresses = gcd.getFromLocation(mCurrentLocation!!.getLatitude(),
                        mCurrentLocation!!.getLongitude(), 1);
                if (addresses.size > 0) {
                    System.out.println(addresses.get(0).getLocality());
                    var cityName = addresses.get(0).getLocality();
                    addressFused = cityName
                }
            } catch (e: java.lang.Exception) {
                e.printStackTrace();
            }
            showAppCompatAlert("", "Are You Sure You Want to \n Confirm Delivery?", "OK", "Cancel", "SUCCESS", true)


        }

    }

    @SuppressLint("MissingPermission")
    private fun startLocUpdates() {
        mSettingsClient!!
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this) {
                    //                    Log.i(TAG, "All location settings are satisfied.")

//                    Toast.makeText(applicationContext, "Started location updates!", Toast.LENGTH_SHORT).show()


                    mFusedLocationClient!!.requestLocationUpdates(mLocationRequest,
                            mLocationCallback, Looper.myLooper())

                    updateLocUI()
                }
                .addOnFailureListener(this) { e ->
                    val statusCode = (e as ApiException).statusCode
                    when (statusCode) {
                        LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
//                            Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " + "location settings ")
                            try {
                                // Show the dialog by calling startResolutionForResult(), and check the
                                // result in onActivityResult().
                                val rae = e as ResolvableApiException
                                rae.startResolutionForResult(this@PendingSignatureActivity, REQUEST_CHECK_SETTINGS)
                            } catch (sie: IntentSender.SendIntentException) {
//                                Log.i(TAG, "PendingIntent unable to execute request.")
                            }

                        }
                        LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                            val errorMessage = "Location settings are inadequate, and cannot be " + "fixed here. Fix in Settings."
//                            Log.e(TAG, errorMessage)

                            Toast.makeText(this@PendingSignatureActivity, errorMessage, Toast.LENGTH_LONG).show()
                        }
                    }

                    updateLocUI()
                }
    }

    fun stopLocUpdates() {
        // Removing location updates
//        try {
//            mFusedLocationClient!!
//                    .removeLocationUpdates(mLocationCallback)
//                    .addOnCompleteListener(this) {
//
//                    }
//        } catch (e: java.lang.Exception) {
//
//        }
        if (mFusedLocationClient != null && mLocationCallback != null) {
            mFusedLocationClient!!
                    .removeLocationUpdates(mLocationCallback)
                    .addOnCompleteListener(this) {

                    }
        }

    }
}