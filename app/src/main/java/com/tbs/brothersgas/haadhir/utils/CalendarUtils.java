package com.tbs.brothersgas.haadhir.utils;

import android.content.Context;
import android.util.Log;

import com.tbs.brothersgas.haadhir.common.FireBaseOperations;
import com.tbs.brothersgas.haadhir.listeners.StringListner;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CalendarUtils {


    private static final String DATE_STD_PATTERN_PRINT = "dd-MM-yyyy";//2018-01-06
    private static final String DATE = "yyyyMMdd";//2018-01-06
    private static final String TIME = "HHmm";
    private static final String PDF_DATE = "dd/MM/yyyy";//2018-01-06
    public static final String PDF_TIME = "HH:mm:ss";


    public static void getCurrentDateFromFireBase(Context context,
                                                  StringListner dateString) {

        if (Util.isNetworkAvailable(context)) {
            FireBaseOperations.setCurrentUserTime();

            String formattedDate = "";
            FireBaseOperations.getUserTime((object, isSuccess) -> {
                if (isSuccess) {
                    Log.d("userTimings0--->", object + "");
                    SimpleDateFormat df = new SimpleDateFormat(DATE_STD_PATTERN_PRINT);
                    DateFormat d = new SimpleDateFormat("HH:mm");
                    Date date = (Date) object;
                    String time = d.format(date);
                    dateString.getString(df.format(date) + " " + time);
                } else {
                    dateString.getString(getCurrentDate());
                }
            });
        } else {
            dateString.getString(getCurrentDate());
        }
    }

    public static String getCurrentDate() {

        String formattedDate = "";
        try {
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat(DATE_STD_PATTERN_PRINT);
//            formattedDate = df.format(calendar.getTime()+" "+calendar.get(Calendar.HOUR_OF_DAY)+":"+calendar.get(Calendar.MINUTE));

            DateFormat d = new SimpleDateFormat("HH:mm");
            String date = d.format(Calendar.getInstance().getTime());
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            formattedDate = df.format(c.getTime()) + "   " + date;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return formattedDate;
    }

    public static String getTime() {


        return getTimeBasedOnPattern(TIME);
    }

    public static String getDate() {

        return getDateBasedOnPattern(DATE);
    }

    public static String getPDFDate() {

        return getDateBasedOnPattern(PDF_DATE);
    }

    public static String getPDFTime() {

        return getDateBasedOnPattern(PDF_TIME);
    }

    private static String getDateBasedOnPattern(String formate) {
        String formattedDate = "";
        try {
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat(formate);
            formattedDate = df.format(c.getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return formattedDate;
    }

    private static String getTimeBasedOnPattern(String formateTime) {
        String formattedDate = "";
        try {
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat(DATE_STD_PATTERN_PRINT);
//            formattedDate = df.format(calendar.getTime()+" "+calendar.get(Calendar.HOUR_OF_DAY)+":"+calendar.get(Calendar.MINUTE));

            DateFormat d = new SimpleDateFormat(formateTime);
            String date = d.format(Calendar.getInstance().getTime());
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            formattedDate = date;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return formattedDate;
    }

    public static String timeDifference(String time1, String time2) {
        String diffTime = "";
        try {
//            time1 = "16:00:00";
//            time2 = "19:00:00";

            SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
            Date date1 = format.parse(time1);
            Date date2 = format.parse(time2);
            long difference = date2.getTime() - date1.getTime();
            diffTime = "" + diffTime;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return diffTime;
    }
}



