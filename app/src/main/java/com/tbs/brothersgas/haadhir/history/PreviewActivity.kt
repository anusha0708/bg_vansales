package com.tbs.brothersgas.haadhir.history

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.util.Log
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.RelativeLayout

import com.github.barteksc.pdfviewer.PDFView
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle
import com.shockwave.pdfium.PdfDocument
import com.tbs.brothersgas.haadhir.Activitys.BaseActivity
import com.tbs.brothersgas.haadhir.Model.PaymentPdfDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.ActiveDeliveryRequest
import com.tbs.brothersgas.haadhir.Requests.ExecutiveActiveDeliveryRequest
import com.tbs.brothersgas.haadhir.Requests.InvoiceDetailsRequest
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.delivery_executive.DeliveryExecutiveListActivity
import com.tbs.brothersgas.haadhir.listeners.ResultListner
import com.tbs.brothersgas.haadhir.pdfs.BGInvoicePdf
import com.tbs.brothersgas.haadhir.pdfs.BulkDeliveryNotePDF
import com.tbs.brothersgas.haadhir.pdfs.CYLDeliveryNotePDF
import com.tbs.brothersgas.haadhir.pdfs.utils.PDFConstants
import com.tbs.brothersgas.haadhir.prints.PrinterConnection
import com.tbs.brothersgas.haadhir.prints.PrinterConstants
import com.tbs.brothersgas.haadhir.utils.Util

import java.io.File
import java.util.ArrayList

class PreviewActivity : BaseActivity(), OnPageChangeListener, OnLoadCompleteListener {
    private var createPDFpaymentDO: PaymentPdfDO = PaymentPdfDO();

    lateinit var pdfView: PDFView
    lateinit var ll1: LinearLayout
    lateinit var btnEmail: Button
    lateinit var btnPrint: Button
    internal var pageNumber: Int? = 0
    internal var pdfFileName: String? = null
    internal var id: String? = null
    internal var constant: Int? = 0

    internal var TAG = "PdfActivity"
    internal var position = -1
    lateinit var dir: File
    lateinit var type: String
    internal var name: String? = null
    override fun initialize() {
        val llCategories = getLayoutInflater().inflate(R.layout.collector_pdf, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            val intent = Intent()
            setResult(1, intent)
            DeliveryExecutiveListActivity.isRefreshed = true;
            finish()
        }
        disableMenuWithBackButton()
        tvScreenTitle.setText("Preview")

        initializeControls()
    }

    override fun initializeControls() {
        init()

    }


    private fun init() {
        pdfView = findViewById<PDFView>(R.id.pdfView)
        ll1 = findViewById<LinearLayout>(R.id.ll1)
        btnEmail = findViewById<Button>(R.id.btnEmail)
        btnPrint = findViewById<Button>(R.id.btnPrint)

        if (intent.hasExtra("COLLECTOR")) {
            type = intent!!.extras!!.getString("COLLECTOR")!!
        }

        btnEmail.setOnClickListener {
            if (pdfFileName.equals(PDFConstants.CYL_DELIVERY_NOTE_PDF_NAME)) {
                if(constant==9){
                    prepareExectiveDeliveryNoteCreation("Email")

                }else{
                    prepareDeliveryNoteCreation("Email")

                }

            }
            else if (pdfFileName.equals(PDFConstants.BULK_DELIVERY_NOTE_PDF_NAME)) {
                prepareDeliveryNoteCreation("Email")

            }else {
                prepareInvoiceCreation("Email")

            }
            btnEmail.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnEmail.setClickable(false)
            btnEmail.setEnabled(false)

        }
        btnPrint.setOnClickListener {
            if (pdfFileName.equals(PDFConstants.CYL_DELIVERY_NOTE_PDF_NAME)) {
                if(constant==9){
                    prepareExectiveDeliveryNoteCreation("Print")

                }else{
                    prepareDeliveryNoteCreation("Print")

                }

            } else if (pdfFileName.equals(PDFConstants.BULK_DELIVERY_NOTE_PDF_NAME)) {
                prepareDeliveryNoteCreation("Print")

            } else{
                prepareInvoiceCreation("Print")

            }
        }

        //        position = getIntent().getIntExtra("position",-1);
        displayFromSdcard()
    }


    private fun displayFromSdcard() {
        if (intent.hasExtra("KEY")) {
            pdfFileName = intent.extras!!.getString("KEY")
        }
        if (intent.hasExtra("CONSTANT")) {
            constant = intent.extras!!.getInt("CONSTANT")
        }
        if (intent.hasExtra("ID")) {
            id = intent.extras!!.getString("ID")
        }
        dir = File(Util.getAppPath(this@PreviewActivity))

        getfile(dir)

        //        File file= new File(String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath().endsWith(pdfFileName)));
        for (j in fileList.indices) {
            if (fileList[j].absoluteFile.name == pdfFileName) {
                pdfView.fromFile(fileList[j].absoluteFile)
                        .defaultPage(pageNumber!!)
                        .enableSwipe(true)
                        .swipeHorizontal(false)
                        .onPageChange(this)
                        .enableAnnotationRendering(true)
                        .onLoad(this)
                        .scrollHandle(DefaultScrollHandle(this))
                        .load()
            }
        }


    }


    fun getfile(dir: File): ArrayList<File> {

        val listFile = dir.listFiles()
        var i = 0
        if (listFile != null && listFile.size > 0) {
            i = 0
            while (i < listFile.size) {

                if (listFile[i].isDirectory) {
                    getfile(listFile[i])

                } else {

                    var booleanpdf = false
                    if (listFile[i].name.endsWith(pdfFileName!!)) {

                        for (j in fileList.indices) {
                            if (fileList[j].name == listFile[i].name) {
                                booleanpdf = true
                            } else {

                            }
                        }

                        if (booleanpdf) {
                            booleanpdf = false
                        } else {
                            fileList.add(listFile[i])

                        }
                    }
                }
                i++
            }
        }
        return fileList
    }

    override fun onPageChanged(page: Int, pageCount: Int) {
        pageNumber = page
        title = String.format("%s %s / %s", pdfFileName, page + 1, pageCount)
    }


    override fun loadComplete(nbPages: Int) {
        val meta = pdfView.documentMeta
        printBookmarksTree(pdfView.tableOfContents, "-")

    }

    fun printBookmarksTree(tree: List<PdfDocument.Bookmark>, sep: String) {
        for (b in tree) {

            Log.e(TAG, String.format("%s %s, p %d", sep, b.title, b.pageIdx))

            if (b.hasChildren()) {
                printBookmarksTree(b.children, "$sep-")
            }
        }
    }

    companion object {
        var fileList = ArrayList<File>()
    }


    private fun printDocument(obj: Any, from: Int) {
        val printConnection = PrinterConnection.getInstance(this@PreviewActivity)
        if (printConnection.isBluetoothEnabled()) {
            printConnection.connectToPrinter(obj, from)
        } else {
//            PrinterConstants.PRINTER_MAC_ADDRESS = ""
            showToast("Please enable your mobile Bluetooth.")
//            showAppCompatAlert("", "Please enable your mobile Bluetooth.", "Enable", "Cancel", "EnableBluetooth", false)
        }
    }

    private fun pairedDevices() {
        val pairedDevices = BluetoothAdapter.getDefaultAdapter().getBondedDevices()
        if (pairedDevices.size > 0) {
            for (device in pairedDevices) {
                val deviceName = device.getName()
                val mac = device.getAddress() // MAC address
//                if (StorageManager.getInstance(this).getPrinterMac(this).equals("")) {
                StorageManager.getInstance(this).savePrinterMac(this, mac);
//                }
                Log.e("Bluetooth", "Name : " + deviceName + " Mac : " + mac)
                break
            }
        }
    }

    override fun onResume() {
        super.onResume()
        pairedDevices()
    }

    override fun onStart() {
        super.onStart()
        val filter = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        registerReceiver(mReceiver, filter)
//        val filter = IntentFilter(Intent.PAIRIN.ACTION_FOUND Intent. "android.bluetooth.device.action.PAIRING_REQUEST");
//        registerReceiver(mReceiver, filter)
//        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
//        mBluetoothAdapter.startDiscovery()
//        val filter2 = IntentFilter( "android.bluetooth.device.action.PAIRING_REQUEST")
//        registerReceiver(mReceiver, filter2)

    }

    override fun onDestroy() {
        unregisterReceiver(mReceiver)
//        PrinterConstants.PRINTER_MAC_ADDRESS = ""
        super.onDestroy()
    }

    private val mReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            if (BluetoothDevice.ACTION_FOUND == action) {
                //
                val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE);
                PrinterConstants.bluetoothDevices.add(device)
                Log.e("Bluetooth", "Discovered => name : " + device!!.name + ", Mac : " + device!!.address)
            } else if (action.equals(BluetoothDevice.ACTION_ACL_CONNECTED)) {
                Log.e("Bluetooth", "status : ACTION_ACL_CONNECTED")
                pairedDevices()
            } else if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                val state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
                if (state == BluetoothAdapter.STATE_OFF) {
                    Log.e("Bluetooth", "status : STATE_OFF")
                } else if (state == BluetoothAdapter.STATE_TURNING_OFF) {
                    Log.e("Bluetooth", "status : STATE_TURNING_OFF")
                } else if (state == BluetoothAdapter.STATE_ON) {
                    Log.e("Bluetooth", "status : STATE_ON")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_TURNING_ON) {
                    Log.e("Bluetooth", "status : STATE_TURNING_ON")
                } else if (state == BluetoothAdapter.STATE_CONNECTING) {
                    Log.e("Bluetooth", "status : STATE_CONNECTING")
                } else if (state == BluetoothAdapter.STATE_CONNECTED) {
                    Log.e("Bluetooth", "status : STATE_CONNECTED")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_DISCONNECTED) {
                    Log.e("Bluetooth", "status : STATE_DISCONNECTED")
                }
            }
        }
    }


    private fun prepareDeliveryNoteCreation(from: String) {
        if (id!!.length > 0) {
            if (Util.isNetworkAvailable(this)) {

                val driverListRequest = ActiveDeliveryRequest(id, this@PreviewActivity)
                driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                    hideLoader()
                    if (activeDeliveryDo != null) {
                        if (isError) {
                            showAppCompatAlert("Error", "Delivery Note pdf data error", "Ok", "", "", false)
                        } else {
                            if (from.equals("Print", true)) {
                                if(pdfFileName.equals(PDFConstants.BULK_DELIVERY_NOTE_PDF_NAME)){
                                    printDocument(activeDeliveryDo, PrinterConstants.PrintBulkDeliveryNotes);

                                }else{
                                    printDocument(activeDeliveryDo, PrinterConstants.PrintCylinderDeliveryNotes);

                                }

                            } else {
                                if(pdfFileName.equals(PDFConstants.BULK_DELIVERY_NOTE_PDF_NAME)){
                                    captureInfo(activeDeliveryDo.deliveryEmail, ResultListner { `object`, isSuccess ->
                                        if (isSuccess) {
                                            BulkDeliveryNotePDF(this).createDeliveryPDF(activeDeliveryDo, "Email").CreateDeliveryPDF().execute()
                                        }
                                    })



                                }else{
                                    captureInfo(activeDeliveryDo.deliveryEmail, ResultListner { `object`, isSuccess ->
                                        if (isSuccess) {
                                            CYLDeliveryNotePDF(this).createDeliveryPDF(activeDeliveryDo, "Email").CreateDeliveryPDF().execute()
                                        }
                                    })



                                }

                            }
                        }
                    } else {
                        showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
                    }
                }
                driverListRequest.execute()

            } else {
                showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

            }

        }

    }
    private fun prepareExectiveDeliveryNoteCreation(from: String) {
        if (id!!.length > 0) {
            if (Util.isNetworkAvailable(this)) {

                val driverListRequest = ExecutiveActiveDeliveryRequest(id, this@PreviewActivity)
                driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                    hideLoader()
                    if (activeDeliveryDo != null) {
                        if (isError) {
                            showAppCompatAlert("Error", "Delivery Note pdf data error", "Ok", "", "", false)
                        } else {
                            if (from.equals("Print", true)) {

                                printDocument(activeDeliveryDo, PrinterConstants.PrintDelivetryExecutiveReport);

                            } else {
                                captureInfo(activeDeliveryDo.deliveryEmail, ResultListner { `object`, isSuccess ->
                                    if (isSuccess) {
                                        CYLDeliveryNotePDF(this).createDeliveryPDF(activeDeliveryDo, "Email").CreateDeliveryPDF().execute()
                                    }
                                })


                            }
                        }
                    } else {
                        showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
                    }
                }
                driverListRequest.execute()

            } else {
                showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

            }

        }

    }

    private fun prepareInvoiceCreation(from: String) {
        if (id!!.length > 0) {
            if (Util.isNetworkAvailable(this)) {
                val siteListRequest = InvoiceDetailsRequest(id, this)
                siteListRequest.setOnResultListener { isError, createPDFInvoiceDO ->
                    hideLoader()
                    if (createPDFInvoiceDO != null) {
                        if (isError) {
                            showAppCompatAlert("Error", "Invoice pdf api error", "Ok", "", "", false)
                        } else {
                            if (from.equals("Print", true)) {

                                printDocument(createPDFInvoiceDO, PrinterConstants.PrintInvoiceReport);
                            } else {
                                captureInfo(createPDFInvoiceDO.invoiceEmail,ResultListner { `object`, isSuccess ->
                                    if (isSuccess) {
                                        BGInvoicePdf.getBuilder(this).build(createPDFInvoiceDO, "Email");
                                    }
                                })

                            }
                        }
                    } else {
                        showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
                    }
                }
                siteListRequest.execute()
            } else {
                showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "", false)

            }


        } else {
            showAppCompatAlert("Error", "No Invoice Id Found", "Ok", "", "Failure", false)
        }
    }


}





