package com.tbs.brothersgas.haadhir.Activitys

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.brothersgas.haadhir.Adapters.InspectionAdapter
import com.tbs.brothersgas.haadhir.Model.InspectionDO
import com.tbs.brothersgas.haadhir.R
//import com.tbs.brothersgas.haadhir.Requests.GateInspectionRequest
import com.tbs.brothersgas.haadhir.database.StorageManager
import java.util.ArrayList


class GateInspectonActivity : BaseActivity() {

    lateinit var btnSkip:Button
    lateinit var btnCompleted:Button
    lateinit var recycleview : androidx.recyclerview.widget.RecyclerView
    lateinit var tvNoDataFound: TextView
    override  protected fun onResume() {
        super.onResume()
    }
    override fun initialize() {
        var  llCategories = layoutInflater.inflate(R.layout.gate_inspection, null) as RelativeLayout
        llBody!!.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
        disableMenuWithBackButton()
        initializeControls()
        btnCompleted.isEnabled = false;
        btnCompleted.isClickable= false;
        btnCompleted.setBackgroundColor(resources.getColor(R.color.md_gray_light))
//        val driverListRequest = GateInspectionRequest(this@GateInspectonActivity)
//        driverListRequest.setOnResultListener { isError, inspectionMainDo ->
//
//            if (isError) {
//                hideLoader()
//                recycleview.setVisibility(View.GONE)
//                tvNoDataFound.setVisibility(View.VISIBLE)
//                Toast.makeText(this@GateInspectonActivity, resources.getString(R.string.error_NoData), Toast.LENGTH_SHORT).show()
//            } else {
//               hideLoader()
//                recycleview.setVisibility(View.VISIBLE)
//                tvNoDataFound.setVisibility(View.GONE)
//                var inspectionAdapter = InspectionAdapter(this@GateInspectonActivity, inspectionMainDo.inspectionDOS)
//                recycleview.setLayoutManager(LinearLayoutManager(this@GateInspectonActivity))
//                StorageManager.getInstance(this).saveGateInspectionList(this, inspectionMainDo.inspectionDOS)
//                recycleview.setAdapter(inspectionAdapter)
//
//            }
//        }
//        showLoader()
//        driverListRequest.execute()
        btnSkip.setOnClickListener{
            finish()
        }
        btnCompleted.setOnClickListener {
            setResult(6, null)
            finish()
        }
    }
    override fun initializeControls() {
        tvScreenTitle.setText(R.string.gate_inspection)
        //ivBack.visibility = View.GONE

        btnSkip             = findViewById(R.id.btnSkip) as Button
        btnCompleted        = findViewById<Button>(R.id.btnCompleted)
        ivMenu.visibility = View.VISIBLE
        recycleview = findViewById(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        tvNoDataFound = findViewById(R.id.tvNoDataFound) as TextView

        recycleview.setLayoutManager(linearLayoutManager)
    }

    fun enableDisableComplete(inspectionDOs: ArrayList<InspectionDO>) {
        var icChanged = false;
        val inspectionDoS = StorageManager.getInstance(this).getGateInspectionList(this);
        if(inspectionDoS!=null && inspectionDoS.size>0){
            for (i in inspectionDoS.indices){
                if(inspectionDoS.get(i).isSelected != inspectionDOs.get(i).isSelected){
                    icChanged = true;
                    break;
                }
            }
            if(icChanged){
                btnCompleted.isEnabled = true;
                btnCompleted.isClickable= true;
                btnCompleted.setBackgroundColor(resources.getColor(R.color.md_green))
            }
            else{
                btnCompleted.isEnabled = false;
                btnCompleted.isClickable= false;
                btnCompleted.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            }
        }
    }
}