package com.tbs.brothersgas.haadhir.Activitys

import android.content.Intent
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.brothersgas.haadhir.Adapters.AddMoreReturnProductsAdapter
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.NonBgProductListRequest
import com.tbs.brothersgas.haadhir.utils.Util

//
class AddMoreReturnProductsActivity : BaseActivity() {

    private lateinit var recycleview : androidx.recyclerview.widget.RecyclerView
    private lateinit var tvNoDataFound:TextView
    private lateinit var btnAddMore: Button
    private lateinit var adapter: AddMoreReturnProductsAdapter

    override fun initialize() {
        val  llCategories = getLayoutInflater().inflate(R.layout.add_more_return_products_layout, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        tvScreenTitle.setText("Add More Return Products")
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, Gravity.LEFT)
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }

        initializeControls()
        if (Util.isNetworkAvailable(this)) {
            val nonBgRequest = NonBgProductListRequest(this@AddMoreReturnProductsActivity)
            nonBgRequest.setOnResultListener { isError, loanReturnMainDO ->
                hideLoader()
                if (isError) {
                    tvNoDataFound.setVisibility(View.VISIBLE)
                    recycleview.setVisibility(View.GONE)
                    Toast.makeText(this@AddMoreReturnProductsActivity, R.string.error_NoData, Toast.LENGTH_SHORT).show()
                } else {

                    if(loanReturnMainDO.loanReturnDOS.size>0){
                        tvNoDataFound.setVisibility(View.GONE)
                        recycleview.setVisibility(View.VISIBLE)
                        adapter  = AddMoreReturnProductsAdapter(this@AddMoreReturnProductsActivity, loanReturnMainDO.loanReturnDOS)
                        recycleview!!.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this@AddMoreReturnProductsActivity)
                        recycleview!!.adapter = adapter
                    }else{
                        tvNoDataFound.setVisibility(View.VISIBLE)
                        recycleview.setVisibility(View.GONE)
                    }


                }
            }
            nonBgRequest.execute()
        } else {
            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "",false)

        }



        btnAddMore.setOnClickListener {

            if(adapter != null){
                val selectedLoanReturnDOs = adapter.getSelectedNonBgDOs();
                if(selectedLoanReturnDOs!=null && !selectedLoanReturnDOs.isEmpty()){
                    val intent = Intent();
                    intent.putExtra("selectedLoanReturnDOs", selectedLoanReturnDOs);
                    setResult(177, intent);
                    finish()
                }
                else{
                    showToast("Please select items")
                }
            }
            else{
                showToast("No Return Items found")
            }

        }

    }

    override fun initializeControls() {

        recycleview             = findViewById(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        tvNoDataFound           = findViewById(R.id.tvNoDataFound) as TextView
        btnAddMore              = findViewById(R.id.btnAddMore) as Button
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
        recycleview.setLayoutManager(linearLayoutManager)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

}