package com.tbs.brothersgas.haadhir.Activitys

import android.content.Intent
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.tbs.brothersgas.haadhir.R

//
class StartRouteActivity : BaseActivity() {

    override  protected fun onResume() {
        super.onResume()
    }
    override fun initialize() {
      var  llCategories = getLayoutInflater().inflate(R.layout.start_route, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            setResult(13, null)
            finish() }
    }
    override fun initializeControls() {
        tvScreenTitle.setText(R.string.start_route)
        showAppCompatAlert("Directions", "Start navigation to Restaurent Mos Eisely?", "Start", "Cancel", "SUCCESS", false)


    }
    override fun onButtonYesClick(from: String) {

        if ("SUCCESS".equals(from, ignoreCase = true)) {
//            val intent = Intent(this@StartRouteActivity, Navigation::class.java)
//            startActivity(intent)


        }
    }

    override fun onButtonNoClick(from: String) {

        if ("SUCCESS".equals(from, ignoreCase = true)) {
          finish()

        }
    }

    }