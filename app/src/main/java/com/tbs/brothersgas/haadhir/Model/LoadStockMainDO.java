package com.tbs.brothersgas.haadhir.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class LoadStockMainDO implements Serializable {

    public String totalScheduledStock = "";
    public String volume = "";
    public String vehicleCapacityUnit = "";
    public String volumeUnit = "";
    public String vehicleCapacity = "";
    public String vanVolume = "";
    public String totalNonScheduledStock = "";
    public int status = 0;

    public ArrayList<LoadStockDO> loadStockDOS = new ArrayList<>();
}
