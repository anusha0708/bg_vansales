package com.tbs.brothersgas.pod.Activitys

import android.annotation.TargetApi
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.ScrollView
import android.widget.TextView
import com.tbs.brothersgas.haadhir.Activitys.ActiveDeliveryActivity
import com.tbs.brothersgas.haadhir.Activitys.BaseActivity
import com.tbs.brothersgas.haadhir.Activitys.SpotSalesCustomerActivity
import com.tbs.brothersgas.haadhir.Activitys.VehicleRoutingList
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils



class ScheduledSalesActivity : BaseActivity() {
    var REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 200


    override fun initialize() {
      var  llCategories = getLayoutInflater().inflate(R.layout.sheduled_sales_layout, null) as ScrollView
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        disableMenuWithBackButton()
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            finish()
//            overridePendingTransition(R.anim.exit, R.anim.enter)
        }
        initializeControls()

    }


    override fun initializeControls() {

        tvScreenTitle.setText("Scheduled Sales")
        var  llDeliveryList = findViewById<View>(R.id.llDeliveryList) as LinearLayout
        var  llActiveDelivery = findViewById<View>(R.id.llActiveDelivery) as LinearLayout
        llDeliveryList.setOnClickListener {
            val vehicleCheckOutDo = StorageManager.getInstance(this@ScheduledSalesActivity).getVehicleCheckOutData(this@ScheduledSalesActivity)
            if (vehicleCheckOutDo.confirmCheckOutArrival.equals("", true)) {
                val vehicleCheckInDo = StorageManager.getInstance(this@ScheduledSalesActivity).getVehicleCheckInData(this@ScheduledSalesActivity);
                if (!vehicleCheckInDo.checkInStatus!!.isEmpty()) {
                    val customerDo = StorageManager.getInstance(this@ScheduledSalesActivity).getCurrentSpotSalesCustomer(this@ScheduledSalesActivity)
                    if (customerDo.customerId.equals("")) {
                        val vehicleRoutId = preferenceUtils!!.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "")
                        if(vehicleRoutId.length>0){
                            val intent = Intent(this@ScheduledSalesActivity, VehicleRoutingList::class.java)
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            startActivity(intent)
                        }else{
                            showAppCompatAlert("Alert!", "No Shipments Found", "OK", "", "FAILURE", false)

                        }

                    } else {
                        showAppCompatAlert("", "Please process the current customer " + customerDo.customerId + "'s non schedule shipment", "OK", "", "FAILURE", false)
                    }
                } else {
                    showAppCompatAlert("", "Please Finish Check-in Process", "OK", "", "FAILURE", false)
                }
            } else {
                showAppCompatAlert("", "You have arrived for CheckOut, you cannot access at this time", "OK", "", "", false)
            }
        }
        llActiveDelivery.setOnClickListener {
            val vehicleCheckOutDo = StorageManager.getInstance(this@ScheduledSalesActivity).getVehicleCheckOutData(this@ScheduledSalesActivity)
            if (vehicleCheckOutDo.confirmCheckOutArrival.equals("", true)) {
                val vehicleCheckInDo = StorageManager.getInstance(this@ScheduledSalesActivity).getVehicleCheckInData(this@ScheduledSalesActivity);
                if (!vehicleCheckInDo.checkInStatus!!.isEmpty()) {
                    val shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
                    if (!shipmentId.isEmpty()) {
                        val intent = Intent(this@ScheduledSalesActivity, ActiveDeliveryActivity::class.java)
                        intent.putExtra("SHIPMENT_ID", shipmentId);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        startActivity(intent)
                    } else {
                        showAppCompatAlert("", "There is No active delivery", "OK", "", "", false)
                    }
                } else {
                    showAppCompatAlert("", "Please Finish Check-in Process", "OK", "", "", false)
                }
            } else {
                showAppCompatAlert("", "You have arrived for CheckOut, you cannot access at this time", "OK", "", "", false)
            }
        }



    }
    override  protected fun onResume() {
        super.onResume()
    }


}