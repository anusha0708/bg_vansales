package com.tbs.brothersgas.haadhir.Model;

import java.io.Serializable;

public class DriverEmptyDO implements Serializable {



    public String vehicleCode    = "";
    public String vehicleCarrier = "";

    public String site = "";
    public String driver = "";
    public String tripNumber = "";

    public String aDate = "";
    public String dDate = "";
    public String plate = "";

    public String dTime = "";
    public String aTime    = "";
    public String nNonAssignedShipments = "";

    public String emptyVehicleCode = "";

}
