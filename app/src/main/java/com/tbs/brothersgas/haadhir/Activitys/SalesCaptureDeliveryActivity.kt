package com.tbs.brothersgas.haadhir.Activitys

import android.content.Intent
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.tbs.brothersgas.haadhir.Model.NonScheduledProductMainDO
import com.tbs.brothersgas.haadhir.Model.ProductDO
import com.tbs.brothersgas.haadhir.R

//
class SalesCaptureDeliveryActivity : BaseActivity() {
    lateinit var productDOS: List<ProductDO>
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var btnCreateDelivery: Button
lateinit var nonScheduledProductMainDO: NonScheduledProductMainDO
    override protected fun onResume() {
        super.onResume()
    }

    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.sales_capture_delivery, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()
        toolbar.setNavigationIcon(R.drawable.back)
        hideKeyBoard(llBody)
        toolbar.setNavigationOnClickListener { finish() }
    }

    override fun initializeControls() {
        tvScreenTitle.setText(R.string.capture_delivery_details)

        recycleview = findViewById<View>(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        recycleview.setLayoutManager(androidx.recyclerview.widget.LinearLayoutManager(this))
        val btnScan = findViewById(R.id.btnScan) as Button
        btnScan.setOnClickListener {
            val intent = Intent(this@SalesCaptureDeliveryActivity, ScanProductsActivity::class.java);
            startActivity(intent);
        }
        ivAdd.setVisibility(View.VISIBLE)
       ivAdd.setOnClickListener {
            val intent = Intent(this, AddProductActivity::class.java);

            startActivity(intent);
        }
        btnCreateDelivery = findViewById(R.id.btnCreateDelivery) as Button

        btnCreateDelivery.setOnClickListener {


        }
    }

}