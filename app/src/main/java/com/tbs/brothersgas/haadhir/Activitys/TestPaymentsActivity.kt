package com.tbs.brothersgas.haadhir.Activitys

import android.content.Intent
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.brothersgas.haadhir.Adapters.PaymentAdapter
import com.tbs.brothersgas.haadhir.Model.LoadStockDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.CustomerManagementRequest
import com.tbs.brothersgas.haadhir.Requests.PaymentHistoryRequest
import com.tbs.brothersgas.haadhir.Requests.PaymentTest
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import java.util.ArrayList


class TestPaymentsActivity : BaseActivity() {
    lateinit var invoiceAdapter: PaymentAdapter
    lateinit var loadStockDOs: ArrayList<LoadStockDO>
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var tvNoDataFound: TextView
    var fromId = 0

    override protected fun onResume() {
        super.onResume()
    }

    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.test_payments_list, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
    }

    override fun initializeControls() {
        tvScreenTitle.setText(R.string.sales_payments)

        val btnCreate = findViewById(R.id.btnCreate) as Button

        btnCreate.setOnClickListener {
            if (Util.isNetworkAvailable(this)) {

                val driverListRequest = PaymentTest(this@TestPaymentsActivity)
                driverListRequest.setOnResultListener { isError, invoiceHistoryMainDO ->
                    hideLoader()
                    if (isError) {

                        Toast.makeText(this@TestPaymentsActivity, resources.getString(R.string.error_NoData), Toast.LENGTH_SHORT).show()
                    } else {




                    }
                }
                driverListRequest.execute()
            } else {
                showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "",false)

            }


        }


    }


}