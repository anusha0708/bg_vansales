package com.tbs.brothersgas.haadhir.Requests;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.tbs.brothersgas.haadhir.Model.PickUpDo;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;
import com.tbs.brothersgas.haadhir.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vijay Dhas on 26/05/16.
 */
public class DisplayPickupListRequest extends AsyncTask<String, Void, Boolean> {
    ArrayList<PickUpDo> pods;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;
    Context mContext;
    String id;


    public DisplayPickupListRequest(Context mContext, String date) {

        this.mContext = mContext;
        this.id = date;
    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        public void onCompleted(boolean isError, ArrayList<PickUpDo> pods);

    }

    public boolean runRequest() {
        String NAMESPACE = "http://www.adonix.com/WSS";
        String METHOD_NAME = "run";
        String SOAP_ACTION = "CAdxWebServiceXmlCC";
//        String URL = "http://183.82.9.23:8124/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC";
        preferenceUtils = new PreferenceUtils(mContext);
        username = preferenceUtils.getStringFromPreference(PreferenceUtils.A_USER_NAME, "");
        password = preferenceUtils.getStringFromPreference(PreferenceUtils.A_PASSWORD, "");
        ip = preferenceUtils.getStringFromPreference(PreferenceUtils.IP_ADDRESS, "");
        port = preferenceUtils.getStringFromPreference(PreferenceUtils.PORT, "");
        pool = preferenceUtils.getStringFromPreference(PreferenceUtils.ALIAS, "");

        String URL = "http://" + ip + ":" + port + "/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC";

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        request.addProperty("publicName", WebServiceConstants.DELIVERIES_LIST);
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("I_YVEHROU", id);
        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        request.addProperty("inputXml", jsonObject.toString());

        SoapObject callcontext = new SoapObject("", "callContext");
        // Set all input params
        callcontext.addProperty("codeLang", "ENG");
        callcontext.addProperty("poolAlias", pool);
        callcontext.addProperty("poolId", "");
        callcontext.addProperty("codeUser", username);
        callcontext.addProperty("password", password);
        callcontext.addProperty("requestConfig", "adxwss.trace.on=off");

        request.addSoapObject(callcontext);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.setOutputSoapObject(request);

        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
        androidHttpTransport.debug = true;

        try {
            List<HeaderProperty> headerList = new ArrayList<HeaderProperty>();
            headerList.add(new HeaderProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode((username + ":" + password).getBytes())));


            androidHttpTransport.call(SOAP_ACTION, envelope, headerList);


            SoapObject response = (SoapObject) envelope.getResponse();
            String resultXML = (String) response.getProperty("resultXml");
            if (resultXML != null && resultXML.length() > 0) {
                return parseXML(resultXML);
            } else {
                return false;
            }
        } catch (Exception e) {
            System.out.println("Exception " + e);
            e.printStackTrace();
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("Delivery List xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();
            PickUpDo pod = null;
            pods = new ArrayList<>();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        pod = new PickUpDo();
                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();
                    if (startTag != null && startTag.equalsIgnoreCase("FLD")) {
                        if (attribute.equalsIgnoreCase("O_YSEQ")) {
                            if (text.length() > 0) {
                                pod.sequenceId = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YSHPNUM")) {
                            if (text.length() > 0) {
                                pod.shipmentNumber = text;
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_YVALFLG")) {
                            if (text.length() > 0) {
                                pod.validatedFlag = Integer.parseInt(text);
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_YLEFTFLG")) {
                            if (text.length() > 0) {
                                pod.leftFlag = Integer.parseInt(text);
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_YDEPFLG")) {
                            if (text.length() > 0) {
                                pod.departuredFlag = Integer.parseInt(text);
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_YSKIPFLG")) {
                            if (text.length() > 0) {
                                pod.skipFlag = Integer.parseInt(text);
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_YSDHCANFLG")) {
                            if (text.length() > 0) {
                                pod.cancelFlag = Integer.parseInt(text);
                            }
                        }

                        else if (attribute.equalsIgnoreCase("O_YCITY")) {
                            if (text.length() > 0) {

                                pod.city = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YBP")) {
                            if (text.length() > 0) {
                                pod.customer = text;
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_YOCUR")) {
                            if (text.length() > 0) {
                                pod.currency = text;
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_YPROTYP")) {
                            if (text.length() > 0) {

                                pod.productType = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YBPDES")) {
                            if (text.length() > 0) {

                                pod.description = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YWGT")) {
                            if(text.length()>0) {

                                pod.netWeight = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YVOL")) {
                            if(text.length()>0) {

                                pod.volume = text;
                            }
                        }

                        else if (attribute.equalsIgnoreCase("O_YLON")) {
                            if(text.length()>0) {

                                pod.lattitude = Double.valueOf(text);
                            }
                        } else if (attribute.equalsIgnoreCase("O_YLAT")) {
                            if(text.length()>0) {

                                pod.longitude = Double.valueOf(text);
                            }
                        } else if (attribute.equalsIgnoreCase("O_YNOP")) {
                            if(text.length()>0) {

                                pod.packages = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YDPDT")) {
                            if(text.length()>0) {

                                pod.departureDate = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YDPTM")) {
                            if(text.length()>0) {

                                pod.departureTime = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YARDT")) {
                            if(text.length()>0) {

                                pod.arrivalDate = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YARTM")) {
                            if(text.length()>0) {

                                pod.arrivalTime = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YEDIS")) {
                            if (text.length() > 0) {
                                pod.distance = Double.parseDouble(text);
                            }
                        } else if (attribute.equalsIgnoreCase("O_YDISUN")) {
                            if (text.length() > 0) {

                                pod.unit = text;
                            }
                        }
                        text = "";
                    }
                    if (endTag.equalsIgnoreCase("LIN")) {
                        pods.add(pod);
                    }
                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }
                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            Log.e(this.getClass().getCanonicalName(), "parseXML() : " + e.getMessage());

            return false;
        }
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, pods);
        }
    }
}