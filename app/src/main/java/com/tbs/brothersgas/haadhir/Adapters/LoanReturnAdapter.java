package com.tbs.brothersgas.haadhir.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.tbs.brothersgas.haadhir.Model.LoanReturnDO;
import com.tbs.brothersgas.haadhir.R;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class LoanReturnAdapter extends RecyclerView.Adapter<LoanReturnAdapter.MyViewHolder> {
    int count = 0;
    private LinkedHashMap<String, ArrayList<LoanReturnDO>> loanReturnDosMap;
    private String from;
    private Context context;


    public void refreshAdapter(@NotNull LinkedHashMap<String, ArrayList<LoanReturnDO>> loanReturnDosMap, @NotNull String from) {
        this.loanReturnDosMap = loanReturnDosMap;
        this.from = from;
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvShipmentId;
        private RecyclerView rvReturnProducts;

        public MyViewHolder(View view) {
            super(view);
            tvShipmentId             = view.findViewById(R.id.tvShipmentId);
            rvReturnProducts         = view.findViewById(R.id.rvReturnProducts);

        }
    }


    public LoanReturnAdapter(Context context, LinkedHashMap<String, ArrayList<LoanReturnDO>> loanReturnDosMap,String from) {
        this.context = context;
        this.loanReturnDosMap = loanReturnDosMap;
        this.from=from;
    }

    private ArrayList<LoanReturnDO> selectedLoanReturnDOs = new ArrayList<>();

    public ArrayList<LoanReturnDO> getSelectedLoadStockDOs(){
        return selectedLoanReturnDOs;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.capture_return_list_cell, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        ArrayList<String> keySet = new ArrayList<>(loanReturnDosMap.keySet());
        ArrayList<LoanReturnDO> loanReturnDos = loanReturnDosMap.get(keySet.get(position));
        holder.tvShipmentId.setText("Shipment ID : "+keySet.get(position));
        holder.rvReturnProducts.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        holder.rvReturnProducts.setAdapter(new ReturnProductsListAdapter(context, loanReturnDos));

        if(keySet.get(position).equalsIgnoreCase("")){
            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.non_bg_products_color));
        }
        else if(loanReturnDos!=null&& loanReturnDos.size()>0&& loanReturnDos.get(0).type == 1){
            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.colorlightgray));
        }
        else {
            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.blue_light_grey));
        }
    }

    @Override
    public int getItemCount() {
        return loanReturnDosMap!=null?loanReturnDosMap.keySet().size():0;
    }

    private class ReturnProductsListAdapter extends RecyclerView.Adapter<ReturnListHolder>{
        private ArrayList<LoanReturnDO> loanReturnDos;
        private Context context;
        ReturnProductsListAdapter(Context context, ArrayList<LoanReturnDO> loanReturnDos){
            this.context = context;
            this.loanReturnDos = loanReturnDos;
        }
        @NonNull
        @Override
        public ReturnListHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
            View itemView = LayoutInflater.from(context).inflate(R.layout.loan_return_data, viewGroup, false);
            return new ReturnListHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull ReturnListHolder holder, int position) {
            final LoanReturnDO loanReturnDO = loanReturnDos.get(position);
            if(loanReturnDO.shipmentDate.length()>0){
                String dMonth = loanReturnDO.shipmentDate.substring(4, 6);
                String dyear = loanReturnDO.shipmentDate.substring(0, 4);
                String dDate = loanReturnDO.shipmentDate.substring(Math.max(loanReturnDO.shipmentDate.length() - 2, 0));
                holder.tvProductName.setText(loanReturnDO.productName+"\n"+ dDate + "-" + dMonth + "-" + dyear);

            }else {
                holder.tvProductName.setText(loanReturnDO.productName);

            }

            holder.tvProductDescription.setText(loanReturnDO.productDescription);
            Double qty = (Double) loanReturnDO.qty;
            holder.etNumberET.setText(qty+ "");
            holder.tvLineNumber.setText("Stock : "+qty + " "+loanReturnDO.unit);
            if(loanReturnDO.shipmentNumber.equalsIgnoreCase("")){
                holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.non_bg_products_color));
            }
            else if(loanReturnDos!=null&& loanReturnDos.size()>0&& loanReturnDos.get(0).type == 1){
                holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.colorlightgray));
            }
            else {
                holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.blue_light_grey));
            }
            if(from.equalsIgnoreCase("Products")){
                holder.cbSelected.setVisibility(View.VISIBLE);
                holder.cbNonBGSelected.setVisibility(View.GONE);
                holder.ivAdd.setVisibility(View.GONE);
                holder.ivRemove.setVisibility(View.GONE);
                holder.etNumberET.setVisibility(View.GONE);
                holder.ivRemove.setVisibility(View.GONE);
                holder.tvNumber.setVisibility(View.VISIBLE);
                holder.tvNumber.setText(""+qty+ " "+loanReturnDO.unit);
                holder.tvNumber.setEnabled(false);
            }
            else if(from.equalsIgnoreCase("salesreturn")){
                holder.cbSelected.setVisibility(View.GONE);
                holder.cbNonBGSelected.setVisibility(View.GONE);

// uncomment
//                holder.ivAdd.setVisibility(View.GONE);
//                holder.ivRemove.setVisibility(View.GONE);
//                holder.etNumberET.setEnabled(false);
            }
            else {
                holder.cbSelected.setVisibility(View.GONE);
                holder.cbNonBGSelected.setVisibility(View.GONE);

            }
            if(selectedLoanReturnDOs!=null && selectedLoanReturnDOs.contains(loanReturnDO)){
                holder.cbSelected.setChecked(true);
            }
            else {
                holder.cbSelected.setChecked(false);
            }
            holder.cbSelected.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked){
                        selectedLoanReturnDOs.add(loanReturnDO);
                    }
                    else {
                        selectedLoanReturnDOs.remove(loanReturnDO);
                    }
                }
            });
            holder.cbNonBGSelected.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked){
                        if(loanReturnDos!=null&& loanReturnDos.size()>0&& loanReturnDos.get(0).type == 1) {

                        }
                        loanReturnDO.nonBgType=2;
                    }
                    else {
                        loanReturnDO.nonBgType=1;

                    }
                }
            });
            int maxCount = (int) loanReturnDO.qty;//Integer.parseInt(holder.tvNumberET.getText().toString());
            final Double[] quantity = {(Double) loanReturnDO.qty};
            holder.ivAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        if(quantity[0] < maxCount){
                            quantity[0]= quantity[0]+1;
                            holder.etNumberET.setText(""+(quantity[0]++));
                            loanReturnDO.qty= quantity[0];
                        }
                    }catch (Exception e){

                    }

                }
            });
            holder.ivRemove.setOnClickListener(view -> {
                try {
                    if(quantity[0] > 1){
                        quantity[0]= quantity[0]-1;
                        holder.etNumberET.setText(""+quantity[0]);
                        loanReturnDO.qty= quantity[0];
                    }
                }catch (Exception e){

                }

            });
            holder.etNumberET.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    try {
                        if(!s.toString().equalsIgnoreCase("")){
                            if(Double.parseDouble(s.toString())<=maxCount && Double.parseDouble(s.toString())>0){
                                quantity[0] = Double.parseDouble(s.toString());
                                loanReturnDO.qty = quantity[0];
                            }
                            else {
                                final String newText = s.toString().substring(0, s.length()-1) + "";
                                holder.etNumberET.setText(""+newText);
                            }
                        }
                    }catch (Exception e){

                    }

                }
            });



        }

        @Override
        public int getItemCount() {
            return loanReturnDos!=null?loanReturnDos.size():0;
        }
    }

    private class ReturnListHolder extends RecyclerView.ViewHolder{
        private TextView tvShipmentId, tvProductName, tvProductDescription, tvLineNumber,tvNumber;
        private CheckBox cbSelected,cbNonBGSelected;
        private EditText etNumberET;
        private ImageView ivAdd, ivRemove;

        public ReturnListHolder(@NonNull View itemView) {
            super(itemView);
            tvProductName          =  itemView.findViewById(R.id.tvProductName);
            tvProductDescription   =  itemView.findViewById(R.id.tvProductDescription);
            etNumberET             =  itemView.findViewById(R.id.tvNumberET);
            cbSelected             =  itemView.findViewById(R.id.cbSelected);
            tvNumber               =  itemView.findViewById(R.id.tvNumber);
            tvLineNumber           =  itemView.findViewById(R.id.tvLineNumber);
            ivAdd                  =  itemView.findViewById(R.id.ivAdd);
            ivRemove               =  itemView.findViewById(R.id.ivRemove);
            cbNonBGSelected        =  itemView.findViewById(R.id.cbNonBGSelected);

        }
    }
}
