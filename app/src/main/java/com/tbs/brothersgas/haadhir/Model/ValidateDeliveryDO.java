package com.tbs.brothersgas.haadhir.Model;

import java.io.Serializable;

public class ValidateDeliveryDO implements Serializable {

    public String shipmentId = "";
    public int flag = 0;
    public int flag2 = 0;
    public String message = "";
    public int status = 0;
    public int delEmailFlag = 0;
    public int invEmailFlag = 0;
    public int delPrintFlag = 0;
    public int invPrintFlag = 0;
    public int cylEmailFlag = 0;
    public int cylPrintFlag = 0;
    public String invoiceNumber = "";
    public String currency = "";
    public String amount = "";
}
