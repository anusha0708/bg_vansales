package com.tbs.brothersgas.haadhir.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tbs.brothersgas.haadhir.Model.DriverEmptyDO;
import com.tbs.brothersgas.haadhir.R;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;

import java.util.ArrayList;

public class RouteIdsEMAdapter extends RecyclerView.Adapter<RouteIdsEMAdapter.MyViewHolder>  {

    private ArrayList<DriverEmptyDO> pickUpDos;
    private Context context;





    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvSiteName, tvSiteId, tvShipments, tvVehicleCode, tvPlate, tvADT, tvDDT;
        private LinearLayout llDetails;

        public MyViewHolder(View view) {
            super(view);
            llDetails = (LinearLayout) view.findViewById(R.id.llDetails);
            tvSiteName = (TextView) view.findViewById(R.id.tvSiteName);
            tvSiteId = (TextView) view.findViewById(R.id.tvSiteId);

            tvShipments = (TextView) view.findViewById(R.id.tvShipments);
            tvVehicleCode = (TextView) view.findViewById(R.id.tvVehicleCode);
            tvPlate = (TextView) view.findViewById(R.id.tvPlate);
            tvADT = (TextView) view.findViewById(R.id.tvADT);
            tvDDT = (TextView) view.findViewById(R.id.tvDDT);

        }
    }


    public RouteIdsEMAdapter(Context context, ArrayList<DriverEmptyDO> siteDOS) {
        this.context = context;
        this.pickUpDos = siteDOS;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.route_id_data, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final DriverEmptyDO siteDO = pickUpDos.get(position);
        holder.tvSiteName.setText("VR ID : "+siteDO.emptyVehicleCode);
      //  holder.tvSiteId.setText("" + siteDO.siteId);
        holder.tvVehicleCode.setText("Site : "+siteDO.site);
        holder.tvPlate.setText(""+siteDO.plate);
        holder.tvShipments.setText("No.of Shipments : "+siteDO.nNonAssignedShipments);
        holder.tvADT.setText(""+siteDO.aDate+"  "+siteDO.aTime);
        holder.tvDDT.setText(""+siteDO.dDate+"  "+siteDO.dTime);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = holder.getAdapterPosition();
                PreferenceUtils preferenceUtils = new PreferenceUtils(context);
//                preferenceUtils.saveString(PreferenceUtils.EM_CARRIER_ID, String.valueOf(pickUpDos.get(pos).emptyVehicleCode));
//
//                preferenceUtils.saveString(PreferenceUtils.EM_CARRIER_ID, String.valueOf(pickUpDos.get(pos).emptyVehicleCode));
//
//                ((CheckinActivity)context).tvSelection.setText(""+preferenceUtils.getStringFromPreference(PreferenceUtils.EM_CARRIER_ID, ""));
//                ((CheckinActivity)context).dialog.dismiss();

                // ((CheckinActivity)context).dialog.dismiss();
//                Intent intent = new Intent(context,CheckinActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//
//                context.startActivity(intent);

            }
        });


    }

    @Override
    public int getItemCount() {
        return pickUpDos.size();
    }

}
