package com.tbs.brothersgas.haadhir.Activitys

import android.app.Dialog
import android.view.ViewGroup
import android.widget.*
import com.tbs.brothersgas.haadhir.Model.DriverIdMainDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils


class RouteCheckinActivity : BaseActivity() {
    lateinit var tvSelection:TextView
    lateinit var dialog:Dialog
    lateinit var pickDos: DriverIdMainDO

    override fun initialize() {
      var  llCategories = getLayoutInflater().inflate(R.layout.create_invoice_screen, null) as LinearLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()
    }
    override fun initializeControls() {
        tvScreenTitle.setText(R.string.invoice)
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
        tvSelection           = findViewById(R.id.tvSelection) as TextView
        tvSelection.setText("Select Site")

        preferenceUtils = PreferenceUtils(this)

    }



}