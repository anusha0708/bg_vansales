package com.tbs.brothersgas.haadhir.Activitys

import android.content.Intent
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import android.widget.*
import com.tbs.brothersgas.haadhir.Adapters.DamageReasonListAdapter
import com.tbs.brothersgas.haadhir.Adapters.InvoiceAdapter
import com.tbs.brothersgas.haadhir.Model.LoadStockDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.DamageReasonsListRequest
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import java.util.ArrayList


class DamageReasonsActivity : BaseActivity() {


    lateinit var invoiceAdapter: InvoiceAdapter
    lateinit var loadStockDOs: ArrayList<LoadStockDO>
    lateinit var recycleview : androidx.recyclerview.widget.RecyclerView
    lateinit var tvNoDataFound:TextView
    var fromId= 0

    override  protected fun onResume() {
        super.onResume()
    }
    override fun initialize() {
      var  llCategories = getLayoutInflater().inflate(R.layout.damage_reasons_list, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            var intent = Intent()
            var id = preferenceUtils.getIntFromPreference(PreferenceUtils.DAMAGE_REASON_ID, 0)
            intent.putExtra("data", id)
            setResult(143, intent)
            finish()
        }
    }
    override fun initializeControls() {
        tvScreenTitle.setText(R.string.reason)
        recycleview = findViewById(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        tvNoDataFound = findViewById(R.id.tvNoDataFound) as TextView

        recycleview.setLayoutManager(linearLayoutManager)

//        invoiceAdapter = InvoiceAdapter(this@SalesInvoiceActivity,  null)
//        recycleview.setAdapter(invoiceAdapter)
//
        val siteListRequest = DamageReasonsListRequest(this@DamageReasonsActivity)
        siteListRequest.setOnResultListener { isError, unPaidInvoiceMainDO ->
            hideLoader()
            if (unPaidInvoiceMainDO != null) {
                if (isError) {
                    showAppCompatAlert("", "No reasons found at the movement, please try again.", "Ok", "Cancel", "", false)
                } else {
                    val siteAdapter = DamageReasonListAdapter(this@DamageReasonsActivity, unPaidInvoiceMainDO.reasonDOS)
                    recycleview.setAdapter(siteAdapter)


                }
            } else {
                hideLoader()
                showAppCompatAlert("", "No reasons found at the movement, please try again.", "Ok", "Cancel", "", false)
            }
        }
        siteListRequest.execute()

//        val driverListRequest = InvoiceHistoryRequest("",this@DamageReasonsActivity)
//        driverListRequest.setOnResultListener { isError, invoiceHistoryMainDO ->
//            hideLoader()
//            if (isError) {
//                tvNoDataFound.setVisibility(View.VISIBLE)
//                recycleview.setVisibility(View.GONE)
//                Toast.makeText(this@DamageReasonsActivity, R.string.error_NoData, Toast.LENGTH_SHORT).show()
//            } else {
//
//                  if(invoiceHistoryMainDO.invoiceHistoryDOS.size>0){
//                      tvNoDataFound.setVisibility(View.GONE)
//                      recycleview.setVisibility(View.VISIBLE)
//                      invoiceAdapter = InvoiceAdapter(this@DamageReasonsActivity, invoiceHistoryMainDO.invoiceHistoryDOS)
//                      recycleview!!.layoutManager = LinearLayoutManager(this@DamageReasonsActivity)
//
//                      recycleview!!.adapter = invoiceAdapter
//                  }else{
//                      tvNoDataFound.setVisibility(View.VISIBLE)
//                      recycleview.setVisibility(View.GONE)
//                  }
//
//
//            }
//        }
//        driverListRequest.execute()

    }

    override fun onBackPressed() {
        var intent = Intent()
        var id = preferenceUtils.getIntFromPreference(PreferenceUtils.DAMAGE_REASON_ID, 0)
        intent.putExtra("data", id)
        setResult(143, intent)
        finish()
        super.onBackPressed()
    }

}