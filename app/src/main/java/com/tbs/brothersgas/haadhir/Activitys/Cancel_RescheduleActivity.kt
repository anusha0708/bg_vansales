package com.tbs.brothersgas.haadhir.Activitys

import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import com.tbs.brothersgas.haadhir.Adapters.ReasonListAdapter
import com.tbs.brothersgas.haadhir.Model.ReasonMainDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.*
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.utils.CalendarUtils
import com.tbs.brothersgas.haadhir.utils.LogUtils
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import kotlinx.android.synthetic.main.cancel_reschedule_data.*
import java.text.DecimalFormat
import java.util.*
import kotlin.collections.ArrayList

//
class Cancel_RescheduleActivity : BaseActivity() {

    var status = "";

    var cday: Int = 0
    var cmonth: Int = 0
    var cyear: Int = 0
    lateinit var etComments: EditText
    lateinit var tvDatee: TextView
    lateinit var dialog: Dialog
    private var isSentForApproval: Boolean = false
    lateinit var yourRadioGroup: RadioGroup

    lateinit var tvSelection: TextView
    lateinit var llSelectReason: LinearLayout
    lateinit var date: String
    lateinit var reason: String
    lateinit var btnConfirm: Button
    lateinit var btnRequestApproval: Button
    lateinit var tvApprovalStatus: TextView
    lateinit var comments: String
    private var reasonMainDO: ReasonMainDO = ReasonMainDO()
    private val TAG = "RescheduleActivity";
    var type: Int = 1
    var modifyType: Int = 6
    override protected fun onResume() {
        super.onResume()
    }

    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.cancel_reschedule_data, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()
        toolbar.setNavigationIcon(R.drawable.back)
        hideKeyBoard(llBody)
        toolbar.setNavigationOnClickListener {
            if (isSentForApproval) {
                showToast("Please process the order..!")
            } else {
                LogUtils.debug("PAUSE", "tes")
                var data = tvApprovalStatus.text.toString()
                var comments = etComments.text.toString()
                var reason = tvSelection.text.toString()
                var date = tvDatee.text.toString()

                var intent = Intent()
                intent.putExtra("data", data + "," + comments + "," + reason + "," + date + "," + type)
                setResult(123, intent)
                finish()
            }

        }
    }


    override fun initializeControls() {
        tvScreenTitle.setText(R.string.cancel_reschedule)
        ivRefresh.setVisibility(View.VISIBLE)
        btnConfirm = findViewById(R.id.btnConfirm) as Button
        btnRequestApproval = findViewById(R.id.btnRequestApproval) as Button
        tvApprovalStatus = findViewById(R.id.tvApprovalStatus) as TextView
        llSelectReason = findViewById(R.id.llSelectReason) as LinearLayout
        etComments = findViewById(R.id.etComments) as EditText
        tvDatee = findViewById(R.id.tvDate) as TextView
        tvSelection = findViewById(R.id.tvSelection) as TextView
        yourRadioGroup = findViewById(R.id.radioGroup) as RadioGroup
        var rbCacel = findViewById(R.id.rbCacel) as RadioButton
        var rbReschedule = findViewById(R.id.rbReschedule) as RadioButton
        rbCacel.isChecked = true

        if (rbCacel.isChecked == true) {
            tvDatee.isClickable = false
            tvDatee.isEnabled = false
        } else {
            tvDatee.isClickable = true
            tvDatee.isEnabled = true
        }
        var approval = tvApprovalStatus.text.toString()
        status = preferenceUtils.getStringFromPreference(PreferenceUtils.RESCHEDULE_APPROVAL, "")
        if (approval.equals("NA")) {
            tvApprovalStatus.setText("NA")

        } else {
            var podDo = StorageManager.getInstance(this).getDepartureData(this);
            if (!podDo.rescheduleStatus!!.get(0)!!.equals("")) {
                tvSelection.setText("" + podDo.rescheduleStatus!![2])
                etComments.setText("" + podDo.rescheduleStatus!![1])
                tvDatee.setText("" + podDo.rescheduleStatus!![3])
                if (podDo.rescheduleStatus!!.get(4)!!.equals("1")) {
                    rbCacel.isChecked = true
                } else {
                    rbReschedule.isChecked = true
                }
                if (podDo.rescheduleStatus?.get(0)!!.contains("request", true)) {

                    tvApprovalStatus.setVisibility(View.VISIBLE)
                    tvApprovalStatus.setText("" + podDo.rescheduleStatus!![0])
                    btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                    btnConfirm.isClickable = false
                    btnConfirm.isEnabled = false
                    btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                    btnRequestApproval.isClickable = false
                    btnRequestApproval.isEnabled = false

                } else if (podDo.rescheduleStatus?.get(0)!!.contains("Approved", true)) {
                    tvApprovalStatus.setVisibility(View.VISIBLE)
                    tvApprovalStatus.setText("" + podDo.rescheduleStatus!![0])

                    btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                    btnRequestApproval.isClickable = false
                    btnRequestApproval.isEnabled = false
                    btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
                    btnConfirm.isClickable = true
                    btnConfirm.isEnabled = true

                } else {
                    tvApprovalStatus.setVisibility(View.VISIBLE)

                    tvApprovalStatus.setText(status)
                    tvApprovalStatus.setText("" + podDo.rescheduleStatus!![0])

                    btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                    btnConfirm.isClickable = false
                    btnConfirm.isEnabled = false
                    btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                    btnRequestApproval.isClickable = false
                    btnRequestApproval.isEnabled = false
                }

            } else {
                tvApprovalStatus.setVisibility(View.VISIBLE)

                tvApprovalStatus.setText(status)
                var c = Calendar.getInstance()
                var mYear = c.get(Calendar.YEAR)
                var mMonth = c.get(Calendar.MONTH)

                var mDay = c.get(Calendar.DAY_OF_MONTH)
                tvDatee.setText(CalendarUtils.getPDFDate())
                var data = preferenceUtils.getStringFromPreference(PreferenceUtils.REASON, "")
                tvSelection.setText("" + data)
                data = tvSelection.text.toString()

                btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnConfirm.isClickable = false
                btnConfirm.isEnabled = false
                btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnRequestApproval.isClickable = false
                btnRequestApproval.isEnabled = false
            }

        }
        yourRadioGroup.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {
                when (checkedId) {
                    R.id.rbCacel -> {
                        if (isSentForApproval) {
                            showToast("please process the order")
                        } else {
                            rbCacel.isChecked = true
                            type = 1
                            modifyType = 6
                            tvSelection.setHint("Select Reason")
                            tvSelection.setText("")
                            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                            btnConfirm.isClickable = false
                            btnConfirm.isEnabled = false
                            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                            btnRequestApproval.isClickable = false
                            btnRequestApproval.isEnabled = false
                            etComments.setText("")
                            tvApprovalStatus.setVisibility(View.GONE)
                            tvDatee.isClickable = false
                            tvDatee.isEnabled = false
                            tvDatee.setText("" + CalendarUtils.getPDFDate())

                        }

                    }
                    R.id.rbReschedule -> {
                        if (isSentForApproval) {
                            showToast("please process the order")
                        } else {
                            tvApprovalStatus.setVisibility(View.GONE)
                            etComments.setText("")
                            tvDatee.isClickable = true
                            tvDatee.isEnabled = true
                            rbReschedule.isChecked = true
                            type = 2
                            modifyType = 5
                            tvDatee.setText("")
                            tvDatee.setHint("Select Date")

                            tvSelection.setHint("Select Reason")
                            tvSelection.setText("")
                            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                            btnConfirm.isClickable = false
                            btnConfirm.isEnabled = false
                            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                            btnRequestApproval.isClickable = false
                            btnRequestApproval.isEnabled = false

                        }

                    }

                }
            }
        })


        tvDatee.setOnClickListener {
            if (isSentForApproval) {
                showToast("please process the order")
            } else {
                val calend = Calendar.getInstance()
                calend.add(Calendar.DAY_OF_MONTH, 1);
                val datePicker = DatePickerDialog(this@Cancel_RescheduleActivity, dateSetListener, calend.get(Calendar.YEAR), calend.get(Calendar.MONTH), calend.get(Calendar.DAY_OF_MONTH))
                datePicker.datePicker.setMinDate(calend.getTimeInMillis());
                datePicker.show()

            }

        }


        llSelectReason.setOnClickListener {
            //            reasonDialog()

            tvSelection.performClick()
        }

        tvSelection.setOnClickListener {
            if (isSentForApproval) {
                showToast("please process the order")
            } else {
                tvApprovalStatus.setVisibility(View.GONE)

                btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_green))
                btnRequestApproval.isClickable = true
                btnRequestApproval.isEnabled = true
                reasonDialog()
            }

        }
        ivRefresh.setOnClickListener {
            comments = etComments.text.toString().trim()
            date = tvDatee.text.toString().trim()
            reason = tvSelection.text.toString().trim()
            if (date.isEmpty()) {
                showToast("Please Select Date")
            } else if (reason.isEmpty()) {
                showToast("Please Select Reason")
            } else {
                if (tvApprovalStatus.getText().toString().contains("Your request has been sent")) {
                    preferenceUtils.saveString(PreferenceUtils.RESCHEDULE_APPROVAL, tvApprovalStatus.getText().toString())
                    approvalMechanismCheckStatus()
//                    updateStatus()
                } else if (tvApprovalStatus.getText().toString().contains("Requested")) {
                    preferenceUtils.saveString(PreferenceUtils.RESCHEDULE_APPROVAL, tvApprovalStatus.getText().toString())
                    approvalMechanismCheckStatus()

//                    updateStatus()

                }
            }
        }
        btnConfirm.setOnClickListener {
            comments = etComments.text.toString().trim()
            date = tvDatee.text.toString().trim()
            reason = tvSelection.text.toString().trim()

            if (date.isEmpty()) {
                showToast("Please Select Date")
            } else if (reason.isEmpty()) {
                showToast("Please Select Reason")
            } else {

                var status = tvApprovalStatus.text.toString()
                if (status != null) {
                    if (status.contains("Rejected") && modifyType == 6) {
                        finish()
                    } else if (status.contains("Rejected") && modifyType == 5) {
                        //modify
                        cancelOrResheduleApproval("Rejected")

                    } else {
                        if (Util.isNetworkAvailable(this)) {
                            cancel()

                        } else {
                            showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

                        }


                    }
                }

//                if (type == 1) {
//                    cancel()
//
//                } else {
//                    reshedule()
//                }
            }
        }
        btnRequestApproval.setOnClickListener {
            comments = etComments.text.toString().trim()
            date = tvDatee.text.toString().trim()
            reason = tvSelection.text.toString().trim()
            var splited = date.replace("/", "");
            if(splited!=null&&splited.length>0){
                var aDate = splited.substring(0, 2)
                var aMonth = splited.substring(2, 4)
                var ayear = splited.substring(4, 8)
                if (date.isEmpty()) {
                    showToast("Please Select Date")
                } else if (reason.isEmpty()) {
                    showToast("Please Select Reason")
                } else {
                    if (type == 1) {
                        approvalMechanismRequst(ayear + aMonth + aDate, reason, 7)

                    } else {
                        approvalMechanismRequst(ayear + aMonth + aDate, reason, 6)

                    }
                }
            }


        }
    }

    private fun approvalMechanismCheckStatus() {
        val approvalRequest = ApprovalMechanismCheckStatusRequest(this)
        approvalRequest.setOnResultListener { isError, approvalDO ->
            hideLoader()
            if (approvalDO != null) {
                if (isError) {
                    Toast.makeText(this@Cancel_RescheduleActivity, "Approval under progress", Toast.LENGTH_SHORT).show()
                } else {
//                    approvalDO.status = "Approved"
                    if (approvalDO.flag == 20) {
//                        loadDeliveryData()
                        isSentForApproval = true;
                        tvApprovalStatus.setText("Status : " + approvalDO.status)
                        btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
                        btnConfirm.isClickable = true
                        btnConfirm.isEnabled = true
                        btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        btnRequestApproval.isClickable = false
                        btnRequestApproval.isEnabled = false


                    } else if (approvalDO.flag == 30) {
//                        rejectProducts()
                        isSentForApproval = false;
                        rbCacel.isClickable = true
                        rbCacel.isEnabled = true
                        rbReschedule.isClickable = true
                        rbReschedule.isEnabled = true
                        yourRadioGroup.isClickable = true
                        yourRadioGroup.isEnabled = true
                        tvApprovalStatus.setText("Status : " + approvalDO.status)
                        btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        btnConfirm.isClickable = false
                        btnConfirm.isEnabled = false
                        btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        btnRequestApproval.isClickable = false
                        btnRequestApproval.isEnabled = false
                        android.os.Handler().postDelayed({
                            finish()
                        }, 5000)

                    } else {
                        Toast.makeText(this@Cancel_RescheduleActivity, approvalDO.status, Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
        approvalRequest.execute()
    }

    private fun approvalMechanismRequst(rescheduleDate: String, comments: String, docType: Int) {
        if (Util.isNetworkAvailable(this)) {
            val appUser = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "")
            val shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
            val stransactionID = preferenceUtils.getStringFromPreference(PreferenceUtils.DOC_NUMBER, "")
            var activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)

            val approvalMechanismRequst = ApprovalMechanismRequestedRequest(stransactionID, appUser, activeDeliverySavedDo.customer
                    , activeDeliverySavedDo.site, shipmentId, rescheduleDate, comments, docType, activeDeliverySavedDo.activeDeliveryDOS, this@Cancel_RescheduleActivity)
            approvalMechanismRequst.setOnResultListener { isError, approveDO ->
                hideLoader()

                if (isError) {
                    Toast.makeText(this@Cancel_RescheduleActivity, resources.getString(R.string.server_error), Toast.LENGTH_SHORT).show()
                    btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_green))
                    btnRequestApproval.isClickable = true
                    btnRequestApproval.isEnabled = true
                    tvApprovalStatus.setText("Status : Request Not Sent")
                } else {
                    if (approveDO != null) {
                        btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        btnRequestApproval.isClickable = false
                        btnRequestApproval.isEnabled = false
                        if (approveDO.flag == 20) {
                            tvApprovalStatus.visibility = View.VISIBLE
                            tvApprovalStatus.setText("Status : Requested")
                            isSentForApproval = true;
                            yourRadioGroup.isClickable = false
                            yourRadioGroup.isEnabled = false
                            rbCacel.isClickable = false
                            rbCacel.isEnabled = false
                            rbReschedule.isClickable = false
                            rbReschedule.isEnabled = false
                            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                            btnRequestApproval.isClickable = false
                            btnRequestApproval.isEnabled = false
                            preferenceUtils.saveString(PreferenceUtils.REQUESTED_NUMBER, approveDO.requestedNumber)


                        } else {
                            Toast.makeText(this@Cancel_RescheduleActivity, approveDO.message, Toast.LENGTH_SHORT).show()
                            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_green))
                            btnRequestApproval.isClickable = true
                            btnRequestApproval.isEnabled = true
                            tvApprovalStatus.setText("Status : Request Not Sent")
                        }

                    } else {
                        Toast.makeText(this@Cancel_RescheduleActivity, resources.getString(R.string.server_error), Toast.LENGTH_SHORT).show()
                        btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_green))
                        btnRequestApproval.isClickable = true
                        btnRequestApproval.isEnabled = true
                        tvApprovalStatus.setText("Status : Request Not Sent")
                    }
                }
            }
            approvalMechanismRequst.execute()
        } else {
            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "", false)

        }


    }

    var dateSetListener: DatePickerDialog.OnDateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        cday = dayOfMonth
        cmonth = monthOfYear + 1
        cyear = year

        val formatter = DecimalFormat("00")
        val montH = formatter.format(cmonth)
        val daY = formatter.format(cday)

        tvDatee.setText("" + daY + "/" + montH + "/" + year)

        var dateFormat = "" + year + montH + daY
        preferenceUtils.saveString(PreferenceUtils.DATE_FORMAT, dateFormat)

    }

//    private fun reshedule() {
//        var shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
//
//        val request = RescheduleShipmentRequest(shipmentId, this@Cancel_RescheduleActivity)
//        request.setOnResultListener { isError, approveDO ->
//            hideLoader()
//            if (approveDO != null) {
//                if (isError) {
//                    hideLoader()
//                    Log.e(TAG, "CancelOrRescheduleRequest : " + isError)
//                } else {
//                    hideLoader()
//                    if (approveDO.flag.equals(2)) {
//                        setResult(511)
//                        finish()
//                    } else {
//
//                    }
//
//                }
//            } else {
//                hideLoader()
//            }
//
//        }
//        request.execute()
//    }

    fun cancel() {
        var shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
        comments = etComments.text.toString().trim()
        date = tvDatee.text.toString().trim()
        reason = tvSelection.text.toString().trim()

        var splited = date.replace("/", "");
        var aDate = splited.substring(0, 2)
        var aMonth = splited.substring(2, 4)
        var ayear = splited.substring(4, 8)
        var code = preferenceUtils.getStringFromPreference(PreferenceUtils.Reason_CODE, "")

        if (type == 1) {
            val siteListRequest = CancelOrRescheduleRequest(shipmentId, type, CalendarUtils.getDate(), code, comments, this@Cancel_RescheduleActivity)
            siteListRequest.setOnResultListener { isError, unPaidInvoiceMainDO ->
                hideLoader()
                    if (isError) {
                        hideLoader()
                        showAppCompatAlert("Info!", "Error in Service Response, Please try again...", "Ok", "", "", false)
                    } else {
                        hideLoader()
                        if (unPaidInvoiceMainDO.flag==5) {
                            setResult(511)
                            preferenceUtils.saveString(PreferenceUtils.DELIVERY_NOTE_RESHEDULE, "NOT MANDATORY")

                            finish()
                        } else if (unPaidInvoiceMainDO.flag==4) {
                            showAlert("This Shipment is Already Validated")
                            preferenceUtils.saveString(PreferenceUtils.DELIVERY_NOTE_RESHEDULE, "NOT MANDATORY")

                        } else {
                            showAppCompatAlert("Info!", "Error in Service Response, Please try again...", "Ok", "", "", false)

                        }

                    }


            }

            siteListRequest.execute()
        } else {
            val siteListRequest = CancelOrRescheduleRequest(shipmentId, type, ayear + aMonth + aDate, code, comments, this@Cancel_RescheduleActivity)
            siteListRequest.setOnResultListener { isError, unPaidInvoiceMainDO ->
                hideLoader()
                    if (isError) {
                        hideLoader()
                        showAppCompatAlert("Info!", "Error in Service Response, Please try again...", "Ok", "", "", false)
                    } else {
                        hideLoader()
                        if (unPaidInvoiceMainDO.flag==5) {
                            preferenceUtils.saveString(PreferenceUtils.DELIVERY_NOTE_RESHEDULE, "NOT MANDATORY")

                            setResult(511)
                            finish()
                        } else if (unPaidInvoiceMainDO.flag==4) {
                            preferenceUtils.saveString(PreferenceUtils.DELIVERY_NOTE_RESHEDULE, "NOT MANDATORY")

                            showAlert("This Shipment is Already Validated")
                        } else {
                            showAppCompatAlert("Info!", "Error in Service Response, Please try again...", "Ok", "", "", false)

                        }

                    }

            }

            siteListRequest.execute()
        }

    }

    fun reasonDialog() {
        dialog = Dialog(this, R.style.NewDialog)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setContentView(R.layout.simple_list_dialog)
        val window = dialog.getWindow()
        window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        var recyclerView = dialog.findViewById(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        recyclerView.setLayoutManager(androidx.recyclerview.widget.LinearLayoutManager(this));
//        if (reasonMainDO != null && reasonMainDO.reasonDOS != null && reasonMainDO.reasonDOS.size > 0) {
//            val siteAdapter = ReasonListAdapter(this@Cancel_RescheduleActivity, reasonMainDO.reasonDOS)
//            recyclerView.setAdapter(siteAdapter)
//        } else {
        if (Util.isNetworkAvailable(this)) {
            val siteListRequest = ReasonsListRequest(type, this@Cancel_RescheduleActivity)
            siteListRequest.setOnResultListener { isError, unPaidInvoiceMainDO ->
                hideLoader()
                if (unPaidInvoiceMainDO != null) {
                    if (isError) {
                        showAppCompatAlert("", "No reasons found at the moment, please try again.", "Ok", "Cancel", "", false)
                    } else {
                        reasonMainDO = unPaidInvoiceMainDO
                        val siteAdapter = ReasonListAdapter(type, this@Cancel_RescheduleActivity, reasonMainDO.reasonDOS)
                        recyclerView.setAdapter(siteAdapter)
                    }
                } else {
                    hideLoader()
                    showAppCompatAlert("", "No reasons found at the moment, please try again.", "Ok", "Cancel", "", false)
                }
            }
            siteListRequest.execute()
        } else {
            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "", false)

        }





        dialog.show()


    }

    fun updateReqApprButton(selectedreason: String) {
        if (dialog != null) {
            dialog.dismiss()
        }
        tvSelection.setText(selectedreason)
        btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
        btnConfirm.isClickable = false
        btnConfirm.isEnabled = false
        btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_green))
        btnRequestApproval.isClickable = true
        btnRequestApproval.isEnabled = true
    }

    fun cancelOrResheduleApproval(rejected: String) {
        var shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")

        var formatdate = preferenceUtils.getStringFromPreference(PreferenceUtils.DATE_FORMAT, "")
        comments = etComments.text.toString().trim()
        date = tvDatee.text.toString().trim()
        reason = tvSelection.text.toString().trim()
        if (!rejected.isEmpty()) {
            var activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)

            formatdate = activeDeliverySavedDo.shipmentDate
            modifyType = 2
        }
        if (Util.isNetworkAvailable(this)) {
            val siteListRequest = CancelRescheduleModifyRequest(shipmentId, modifyType, formatdate, this@Cancel_RescheduleActivity)
            siteListRequest.setOnResultListener { isError, approveDO ->
                hideLoader()
                if (approveDO != null) {
                    if (isError) {
                        hideLoader()
//                    Toast.makeText(this@Cancel_RescheduleActivity, "Failed", Toast.LENGTH_SHORT).show()
                    } else {
                        hideLoader()
                        btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        btnRequestApproval.isClickable = false
                        btnRequestApproval.isEnabled = false
                        if (approveDO.flag == modifyType) {
                            if (modifyType == 2) {
                                finish()
                            } else {

                                tvApprovalStatus.setVisibility(View.VISIBLE)
                                tvApprovalStatus.setText("Status : Requested")
                            }

                        } else {
                            tvApprovalStatus.setVisibility(View.VISIBLE)
                            tvApprovalStatus.setText("Status : Request not Sent")
                        }
//                    if (approveDO.status.equals("Requested")) {
////                        Toast.makeText(this@Cancel_RescheduleActivity, "Success", Toast.LENGTH_SHORT).show()
//                        tvApprovalStatus.setVisibility(View.VISIBLE)
//                        tvApprovalStatus.setText("Status : " + approveDO.status)
//
//                    } else {
////                        Toast.makeText(this@Cancel_RescheduleActivity, "Failed", Toast.LENGTH_SHORT).show()
//
//                    }

                    }
                } else {
                    hideLoader()
                }

            }

            siteListRequest.execute()
        } else {
            showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

        }


    }

    fun updateStatus() {
        var shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")

        comments = etComments.text.toString().trim()
        date = tvDatee.text.toString().trim()
        reason = tvSelection.text.toString().trim()
        if (Util.isNetworkAvailable(this)) {
            val siteListRequest = CancelRescheduleApprovalStatusRequest(shipmentId, this)
            siteListRequest.setOnResultListener { isError, approvalDO ->
                hideLoader()
                if (approvalDO != null) {
                    if (isError) {
                        hideLoader()
                    } else {
                        hideLoader()
                        if (approvalDO.status != null) {
                            tvApprovalStatus.setText("Status : " + approvalDO.status)
                            if (approvalDO.status.equals("Approved", true)) {
                                btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
                                btnConfirm.setClickable(true)
                                btnConfirm.setEnabled(true)
                                btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                                btnRequestApproval.isClickable = false
                                btnRequestApproval.isEnabled = false

                            } else if (approvalDO.status.equals("Rejected", true)) {
                                btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
                                btnConfirm.setClickable(true)
                                btnConfirm.setEnabled(true)
                                btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                                btnRequestApproval.isClickable = false
                                btnRequestApproval.isEnabled = false

                            } else {
                                btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                                btnConfirm.setClickable(false)
                                btnConfirm.setEnabled(false)
                                btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                                btnRequestApproval.isClickable = false
                                btnRequestApproval.isEnabled = false

                            }


                        } else {

                        }
                    }
                } else {
                    hideLoader()
                }
            }

            siteListRequest.execute()
        } else {
            showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

        }


    }

    override fun onBackPressed() {
        if (isSentForApproval) {
            showToast("Please process the order..!")
        } else {
            LogUtils.debug("PAUSE", "tes")
            var data = tvApprovalStatus.text.toString()
            var comments = etComments.text.toString()
            var reason = tvSelection.text.toString()
            var date = tvDatee.text.toString()

            var intent = Intent()
            intent.putExtra("data", data + "," + comments + "," + reason + "," + date + "," + type)
            setResult(123, intent)
            finish()
            super.onBackPressed()
        }

    }
}