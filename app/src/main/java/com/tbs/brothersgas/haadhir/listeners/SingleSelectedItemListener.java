package com.tbs.brothersgas.haadhir.listeners;

/**
 * Created by Office on 10-05-2016.
 */
public interface SingleSelectedItemListener {
    void setSingleSelectedItem(Object object);
}
