package com.tbs.brothersgas.haadhir.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tbs.brothersgas.haadhir.Activitys.BaseActivity;
import com.tbs.brothersgas.haadhir.Activitys.PriceDetailsActivity;
import com.tbs.brothersgas.haadhir.Model.PriceItemDO;
import com.tbs.brothersgas.haadhir.R;

import java.util.ArrayList;

public class PriceItemAdapter extends RecyclerView.Adapter<PriceItemAdapter.MyViewHolder>  {

    private ArrayList<PriceItemDO> priceDOS;
    private Context context;





    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvPriceId, tvPriceValue,tvPriceName;
        private LinearLayout llDetails;

        public MyViewHolder(View view) {
            super(view);
            llDetails = (LinearLayout) view.findViewById(R.id.llDetails);
            tvPriceId = (TextView) view.findViewById(R.id.tvPriceId);
            tvPriceName = (TextView) view.findViewById(R.id.tvPriceName);
            tvPriceValue = (TextView) view.findViewById(R.id.tvPriceValue);

            ;



        }
    }


    public PriceItemAdapter(Context context, ArrayList<PriceItemDO> priceDOS) {
        this.context = context;
        this.priceDOS = priceDOS;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.price_item_data, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        if(priceDOS.size()>0){


        final PriceItemDO siteDO = priceDOS.get(position);
        holder. tvPriceId.setText("Record : " + priceDOS.get(position).record);
        holder. tvPriceName.setText("Start Date : " + priceDOS.get(position).startDate);
        holder.tvPriceValue.setText("End Date : " + priceDOS.get(position).endDate);
        holder.llDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, PriceDetailsActivity.class);
                intent.putExtra("Record", siteDO.record);
                context.startActivity(intent);


            }
        });

        }else {
            ((BaseActivity)context).showAlert("NO Records Found");
        }
    }

    @Override
    public int getItemCount() {
        return priceDOS.size();
    }

}
