package com.tbs.brothersgas.haadhir.collector

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.util.Log
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.RelativeLayout

import com.github.barteksc.pdfviewer.PDFView
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle
import com.shockwave.pdfium.PdfDocument
import com.tbs.brothersgas.haadhir.Activitys.BaseActivity
import com.tbs.brothersgas.haadhir.Model.PaymentPdfDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.PDFPaymentDetailsRequest
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.listeners.ResultListner
import com.tbs.brothersgas.haadhir.pdfs.PaymentReceiptPDF
import com.tbs.brothersgas.haadhir.prints.PrinterConnection
import com.tbs.brothersgas.haadhir.prints.PrinterConstants
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util

import java.io.File
import java.util.ArrayList

class CollectorPreviewActivity : BaseActivity(), OnPageChangeListener, OnLoadCompleteListener {
    private var createPDFpaymentDO: PaymentPdfDO = PaymentPdfDO();
    override fun initialize() {
        val llCategories = getLayoutInflater().inflate(R.layout.collector_pdf, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            val intent = Intent()
            setResult(1, intent)
            finish()
        }
        disableMenuWithBackButton()
        tvScreenTitle.setText("Payment Reciept")

        initializeControls()
    }

    override fun initializeControls() {
        init()

    }

    lateinit var pdfView: PDFView
    lateinit var ll1: LinearLayout
    lateinit var btnEmail: Button
    lateinit var btnPrint: Button
    internal var pageNumber: Int? = 0
    internal var pdfFileName: String? = null
    internal var TAG = "PdfActivity"
    internal var position = -1
    lateinit var dir: File
    lateinit var type: String
    internal var name: String? = null


    private fun init() {
        pdfView = findViewById<PDFView>(R.id.pdfView)
        ll1 = findViewById<LinearLayout>(R.id.ll1)
        btnEmail = findViewById<Button>(R.id.btnEmail)
        btnPrint = findViewById<Button>(R.id.btnPrint)

        if (intent.hasExtra("COLLECTOR")) {
            type = intent!!.extras!!.getString("COLLECTOR")
        }

        btnEmail.setOnClickListener {
            preparePaymentCreation(type,"Email")
        }
        btnPrint.setOnClickListener {
            preparePaymentCreation(type,"")
        }

        //        position = getIntent().getIntExtra("position",-1);
        displayFromSdcard()
    }

    private fun displayFromSdcard() {
        if (intent.hasExtra("KEY")) {
            pdfFileName = intent.extras!!.getString("KEY")
        }
        //        pdfFileName = PDFConstants.CYL_DELIVERY_NOTE_PDF_NAME;
        dir = File(Util.getAppPath(this@CollectorPreviewActivity))

        getfile(dir)

        //        File file= new File(String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath().endsWith(pdfFileName)));
        for (j in fileList.indices) {
            if (fileList[j].absoluteFile.name == pdfFileName) {
                pdfView.fromFile(fileList[j].absoluteFile)
                        .defaultPage(pageNumber!!)
                        .enableSwipe(true)
                        .swipeHorizontal(false)
                        .onPageChange(this)
                        .enableAnnotationRendering(true)
                        .onLoad(this)
                        .scrollHandle(DefaultScrollHandle(this))
                        .load()
            }
        }


    }


    fun getfile(dir: File): ArrayList<File> {

        val listFile = dir.listFiles()
        var i = 0
        if (listFile != null && listFile.size > 0) {
            i = 0
            while (i < listFile.size) {

                if (listFile[i].isDirectory) {
                    getfile(listFile[i])

                } else {

                    var booleanpdf = false
                    if (listFile[i].name.endsWith(pdfFileName!!)) {

                        for (j in fileList.indices) {
                            if (fileList[j].name == listFile[i].name) {
                                booleanpdf = true
                            } else {

                            }
                        }

                        if (booleanpdf) {
                            booleanpdf = false
                        } else {
                            fileList.add(listFile[i])

                        }
                    }
                }
                i++
            }
        }
        return fileList
    }

    override fun onPageChanged(page: Int, pageCount: Int) {
        pageNumber = page
        title = String.format("%s %s / %s", pdfFileName, page + 1, pageCount)
    }


    override fun loadComplete(nbPages: Int) {
        val meta = pdfView.documentMeta
        printBookmarksTree(pdfView.tableOfContents, "-")

    }

    fun printBookmarksTree(tree: List<PdfDocument.Bookmark>, sep: String) {
        for (b in tree) {

            Log.e(TAG, String.format("%s %s, p %d", sep, b.title, b.pageIdx))

            if (b.hasChildren()) {
                printBookmarksTree(b.children, "$sep-")
            }
        }
    }

    companion object {
        var fileList = ArrayList<File>()
    }

    private fun preparePaymentCreation(type: String,email:String) {
        val id = preferenceUtils.getStringFromPreference(PreferenceUtils.PAYMENT_ID, "")
//        id = "SD-U101-19000886";
        if (id.length > 0) {
            val siteListRequest = PDFPaymentDetailsRequest(id, this);
            val listener = PDFPaymentDetailsRequest.OnResultListener { isError, createPDFInvoiceDO ->
                hideLoader()
                if (createPDFInvoiceDO != null) {
                    createPDFpaymentDO = createPDFInvoiceDO

                    if (isError) {

                        showToast("Unable to send Email")

                    } else {
                        if(email.equals("Email")){
                            createPDFpaymentDO = createPDFInvoiceDO
//                            if (type.equals("Collector")) {
//                                createPaymentPDF(createPDFInvoiceDO)
//                            } else {
//                                showToast("Customer Not Authorized")
//
//                            }
                            createPaymentPDF(createPDFInvoiceDO)

                        }
                        else{

//                            if (type.equals("Collector")) {
//                            } else {
//                                showToast("Customer Not Authorized")
//
//                            }
                            printDocument(createPDFpaymentDO, PrinterConstants.PrintPaymentReport)

                        }




                    }
                } else {
                    showToast("Unable to send Email")
                }
            }
            siteListRequest.setOnResultListener(listener)
            siteListRequest.execute()
        } else {
            showToast("Unable to send Email")

        }

    }

    private fun preParePaymentCreation() {
        var id = preferenceUtils.getStringFromPreference(PreferenceUtils.PAYMENT_ID, "")
//        var id ="RMRC-U1011900056"
        if (id.length > 0) {
            val siteListRequest = PDFPaymentDetailsRequest(id, this)
            siteListRequest.setOnResultListener { isError, createPDFInvoiceDO ->
                hideLoader()
                if (createPDFInvoiceDO != null) {
                    if (isError) {
                        showAppCompatAlert("Error", "Payment pdf api error", "Ok", "", "", false)
                    } else {
                        createPDFpaymentDO = createPDFInvoiceDO
                        if (createPDFInvoiceDO != null) {
                            PaymentReceiptPDF(this).prepareCollectorPDF(createPDFInvoiceDO, "Preview")
                        } else {
                            //Display error message
                        }
                    }
                } else {
                    showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
                }
            }
            siteListRequest.execute()
        } else {
            showAppCompatAlert("Error", "No Payment Id found", "Ok", "", "", false)

        }

    }

    private fun printDocument(obj: Any, from: Int) {
        val printConnection = PrinterConnection.getInstance(this@CollectorPreviewActivity)
        if (printConnection.isBluetoothEnabled()) {
            printConnection.connectToPrinter(obj, from)
        } else {
//            PrinterConstants.PRINTER_MAC_ADDRESS = ""
            showToast("Please enable your mobile Bluetooth.")
//            showAppCompatAlert("", "Please enable your mobile Bluetooth.", "Enable", "Cancel", "EnableBluetooth", false)
        }
    }

    private fun pairedDevices() {
        val pairedDevices = BluetoothAdapter.getDefaultAdapter().getBondedDevices()
        if (pairedDevices.size > 0) {
            for (device in pairedDevices) {
                val deviceName = device.getName()
                val mac = device.getAddress() // MAC address
//                if (StorageManager.getInstance(this).getPrinterMac(this).equals("")) {
                    StorageManager.getInstance(this).savePrinterMac(this, mac);
//                }
                Log.e("Bluetooth", "Name : " + deviceName + " Mac : " + mac)
                break
            }
        }
    }

    override fun onResume() {
        super.onResume()
        pairedDevices()
    }

    override fun onStart() {
        super.onStart()
        val filter = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        registerReceiver(mReceiver, filter)
//        val filter = IntentFilter(Intent.PAIRIN.ACTION_FOUND Intent. "android.bluetooth.device.action.PAIRING_REQUEST");
//        registerReceiver(mReceiver, filter)
//        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
//        mBluetoothAdapter.startDiscovery()
//        val filter2 = IntentFilter( "android.bluetooth.device.action.PAIRING_REQUEST")
//        registerReceiver(mReceiver, filter2)

    }

    override fun onDestroy() {
        unregisterReceiver(mReceiver)
//        PrinterConstants.PRINTER_MAC_ADDRESS = ""
        super.onDestroy()
    }

    private val mReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            if (BluetoothDevice.ACTION_FOUND == action) {
                val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE);
                PrinterConstants.bluetoothDevices.add(device)
                Log.e("Bluetooth", "Discovered => name : " + device.name + ", Mac : " + device.address)
            } else if (action.equals(BluetoothDevice.ACTION_ACL_CONNECTED)) {
                Log.e("Bluetooth", "status : ACTION_ACL_CONNECTED")
                pairedDevices()
            } else if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                val state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);

                if (state == BluetoothAdapter.STATE_OFF) {
                    Log.e("Bluetooth", "status : STATE_OFF")
                } else if (state == BluetoothAdapter.STATE_TURNING_OFF) {
                    Log.e("Bluetooth", "status : STATE_TURNING_OFF")
                } else if (state == BluetoothAdapter.STATE_ON) {
                    Log.e("Bluetooth", "status : STATE_ON")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_TURNING_ON) {
                    Log.e("Bluetooth", "status : STATE_TURNING_ON")
                } else if (state == BluetoothAdapter.STATE_CONNECTING) {
                    Log.e("Bluetooth", "status : STATE_CONNECTING")
                } else if (state == BluetoothAdapter.STATE_CONNECTED) {
                    Log.e("Bluetooth", "status : STATE_CONNECTED")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_DISCONNECTED) {
                    Log.e("Bluetooth", "status : STATE_DISCONNECTED")
                }
            }
        }
    }

    private fun createPaymentPDF(createPDFInvoiceDO: PaymentPdfDO) {
        if (createPDFInvoiceDO != null) {
            captureInfo(createPDFInvoiceDO.mail, ResultListner { `object`, isSuccess ->
                if (isSuccess) {
                    PaymentReceiptPDF(this).createReceiptPDF(createPDFInvoiceDO, "Email")
                }
            })
            return

        } else {
            //Display error message
        }
    }

    override fun onBackPressed() {
        val intent = Intent()
        setResult(1, intent)
        finish()
        super.onBackPressed()
    }


}
