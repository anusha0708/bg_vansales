package com.tbs.brothersgas.haadhir.Activitys


import android.app.Activity
import android.content.Intent
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.res.ResourcesCompat
import com.tbs.brothersgas.haadhir.Adapters.LoadStockAdapter
import com.tbs.brothersgas.haadhir.Model.LoadStockDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.MultiCurrentNonScheduledStockRequest
import com.tbs.brothersgas.haadhir.Requests.MultiCurrentScheduledStockRequest
import com.tbs.brothersgas.haadhir.Requests.NewScheduledLoadVanSaleRequest
import com.tbs.brothersgas.haadhir.Requests.NonScheduledLoadVanSaleRequest
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.utils.CalendarUtils
import com.tbs.brothersgas.haadhir.utils.Constants
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import kotlinx.android.synthetic.main.new_load_vansales_tab.*
import java.util.*


class NewLoadVanSaleStockTabActivity : BaseActivity() {
    private var rlPartyActivity: ScrollView? = null
    lateinit var btnLoadVehicle: Button
    lateinit var tvTotal: TextView

    //    lateinit var btnLoadReject: Button
    private var scheduledRootId: String = ""
    private var nonScheduledRootId: String = ""
    private var availableStockDos: ArrayList<LoadStockDO> = ArrayList()
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    var nonPercntage = 0.0
    var percentage: Double = 0.0

    //    lateinit var loadStockMainDO : LoadStockMainDO
    var flag: Int = 0

    private var scheduleDos: ArrayList<LoadStockDO> = ArrayList()
    private var nonScheduleDos: ArrayList<LoadStockDO> = ArrayList()

    override fun initialize() {
        rlPartyActivity = layoutInflater.inflate(R.layout.new_load_vansales_tab, null) as ScrollView
        llBody.addView(rlPartyActivity, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        disableMenuWithBackButton()
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
        initializeControls()
        if (intent.hasExtra("ScreenTitle")) {
            tvScreenTitle.setText(intent.extras.getString("ScreenTitle"))

        } else {
            tvScreenTitle.text = "Check Load";
        }
//        setTypeFace(AppConstants.MONTSERRAT_LIGHT_TYPE_FACE, llBody)

//        tabLayout.addTab(tabLayout.newTab().setText("Available Stock"))
        val typeface = ResourcesCompat.getFont(this, R.font.bgs)
        tvTotal.setTypeface(typeface)

        if (intent.hasExtra("FLAG")) {
            flag = intent.extras.getInt("FLAG")
        }
        if (flag == 1) {
            btnLoadVehicle.setVisibility(View.GONE)
//            btnLoadReject.setVisibility(View.GONE)

        } else if (flag == 2) {
            btnLoadVehicle.setVisibility(View.VISIBLE)
            btnLoadVehicle.setText("UnLoad Vehicle")
//            btnLoadReject.setVisibility(View.GONE)

        } else {
//            btnLoadReject.setVisibility(View.GONE)
            btnLoadVehicle.setVisibility(View.VISIBLE)
        }
        val vehicleCheckInDo = StorageManager.getInstance(this@NewLoadVanSaleStockTabActivity).getVehicleCheckInData(this@NewLoadVanSaleStockTabActivity);
        scheduledRootId = vehicleCheckInDo.scheduledRootId!!
        nonScheduledRootId = vehicleCheckInDo.nonScheduledRootId!!

        if (!scheduledRootId.equals("", true)) {
//            scheduleDos = StorageManager.getInstance(this).getVanScheduleProducts(this);
        }
        if (!nonScheduledRootId.equals("", true)) {
//            nonScheduleDos = StorageManager.getInstance(this).getVanNonScheduleProducts(this);
        }

        if (scheduleDos == null || scheduleDos!!.size < 1) {
            loadVehicleStockData()
        } else if (nonScheduleDos == null || nonScheduleDos!!.size < 1) {
            loadNonScheduledVehicleStockData()
        } else {
            var isProductExisted = false;

            availableStockDos = nonScheduleDos
            for (i in scheduleDos.indices) {
                for (k in availableStockDos.indices) {
                    if (scheduleDos.get(i).product.equals(availableStockDos!!.get(k).product, true)) {
                        availableStockDos!!.get(k).quantity = availableStockDos!!.get(k).quantity + scheduleDos.get(i).quantity
                        isProductExisted = true
                        break
                    }
                }
                if (isProductExisted) {
                    isProductExisted = false
                    continue
                } else {
                    availableStockDos!!.add(scheduleDos.get(i))

                }

            }
            if (availableStockDos != null && availableStockDos.size > 0) {
                var loadStockAdapter = LoadStockAdapter(this@NewLoadVanSaleStockTabActivity, availableStockDos, "Shipments")
                recycleview.setAdapter(loadStockAdapter)
            }
        }
        btnLoadVehicle.setOnClickListener {
            var id = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "")
            var nID = preferenceUtils.getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, "")

            if (id.isEmpty() && nID.isEmpty()) {
                showAlert("Stock Not Assigned to Driver")
            } else {

                showAppCompatAlert("", "Are You Sure You Want to \n confirm load?", "OK", "Cancel", "SUCCESS", true)
            }


        }
        if(preferenceUtils.getbooleanFromPreference(PreferenceUtils.IS_MULTI_SITE,false)){
            floating_action_button_confirm.show()
        }else{
            floating_action_button_confirm.hide()
        }

        floating_action_button_confirm.setOnClickListener {
            val intent = Intent(this, MultiSiteListActivity::class.java)
            intent.putExtra(Constants.IS_FROM_DELIVERY, false)
            startActivityForResult(intent, 10)

        }
    }

    override fun initializeControls() {
        btnLoadVehicle = findViewById<Button>(R.id.btnLoadVehicle) as Button
        tvTotal = findViewById<TextView>(R.id.tvTotal) as TextView

//        btnLoadReject = findViewById<Button>(R.id.btnLoadReject) as Button
        recycleview = findViewById<View>(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        recycleview.setLayoutManager(androidx.recyclerview.widget.LinearLayoutManager(this))

//        btnLoadReject.setOnClickListener {
//            setResult(10, null)
//            finish()
//        }
    }

    private fun loadVehicleStockData() {
        if (Util.isNetworkAvailable(this)) {
            var scheduledRootId = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "")

            if (!scheduledRootId.equals("", true)) {
                val loadVanSaleRequest = NewScheduledLoadVanSaleRequest(scheduledRootId, this@NewLoadVanSaleStockTabActivity)
                showLoader()
                loadVanSaleRequest.setOnResultListener { isError, loadStockMainDo ->
                    hideLoader()
                    if (isError) {
                        showToast("No vehicle data found.")
                    } else {

                        scheduleDos = loadStockMainDo.loadStockDOS;
                        for (k in scheduleDos.indices) {
                            percentage = percentage + scheduleDos.get(k).percentage

                        }
                    }
                    loadNonScheduledVehicleStockData()
                }
                loadVanSaleRequest.execute()
            } else {
                loadNonScheduledVehicleStockData()
            }
        } else {
            showToast("No Internet connection, please try again later")
        }
    }

    private fun loadNonScheduledVehicleStockData() {
        if (Util.isNetworkAvailable(this)) {

            var nonScheduledRootId = preferenceUtils.getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, "")

            if (!nonScheduledRootId.equals("", true)) {
                val loadVanSaleRequest = NonScheduledLoadVanSaleRequest(nonScheduledRootId, this@NewLoadVanSaleStockTabActivity)
                showLoader()
                loadVanSaleRequest.setOnResultListener { isError, loadStockMainDo ->
                    hideLoader()
                    if (isError) {
                        showToast("No vehicle data found.")
                    } else {
                        nonScheduleDos = loadStockMainDo.loadStockDOS;
                        var isProductExisted = false;
                        availableStockDos = nonScheduleDos
                        for (i in scheduleDos.indices) {
                            for (k in availableStockDos.indices) {
                                if (scheduleDos.get(i).product.equals(availableStockDos!!.get(k).product, true)&&scheduleDos.get(i).site.equals(availableStockDos!!.get(k).site, true)) {
                                    availableStockDos!!.get(k).quantity = availableStockDos!!.get(k).quantity + scheduleDos.get(i).quantity
                                    isProductExisted = true
                                    break
                                }
                            }
                            if (isProductExisted) {
                                isProductExisted = false
                                continue
                            } else {
                                availableStockDos!!.add(scheduleDos.get(i))
                            }


                        }

//                        availableStockDos.addAll(nonScheduleDos)
                        if (availableStockDos != null && availableStockDos.size > 0) {
                            var loadStockAdapter = LoadStockAdapter(this@NewLoadVanSaleStockTabActivity, availableStockDos, "Shipments")
                            recycleview.setAdapter(loadStockAdapter)
                            btnLoadVehicle.setBackgroundColor(resources.getColor(R.color.md_green))
                            btnLoadVehicle.setClickable(true)
                            btnLoadVehicle.setEnabled(true)
                            for (k in nonScheduleDos.indices) {
                                nonPercntage = nonPercntage + nonScheduleDos.get(k).percentage

                            }
                            var totalPer = percentage + nonPercntage
                            if (totalPer != 0.0) {
                                tvTotal.visibility = View.VISIBLE

                                tvTotal.setText("" + totalPer + "%")

                            }
                        } else {
                            btnLoadVehicle.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                            btnLoadVehicle.setClickable(false)
                            btnLoadVehicle.setEnabled(false)
                            showToast("No Products found")
                            tvTotal.visibility = View.GONE

                        }

//                        var unit =preferenceUtils.getStringFromPreference(PreferenceUtils.UNIT_CAPACITY, "")
//
//                        tvCapacity.setText(""+preferenceUtils.getStringFromPreference(PreferenceUtils.TOTAL_VEHICLE_CAPACITY, "")+ " "+unit)
//
//                      //  tvNonScheduledStock.setText("Non Scheduled Stock : "+loadStockMainDo.totalNonScheduledStock +" "+loadStockMainDo.vehicleCapacityUnit)
//                        var scheduleTotal = 0.0;
//                        var nonScheduleTotal = 0.0;

//                        var scheduleStock =preferenceUtils.getStringFromPreference(PreferenceUtils.TOTAL_SCHEDULED_STOCK, "")
//                        var nonScheduleStock =loadStockMainDo.totalNonScheduledStock
//                        tvScheduledStock.setText(""+scheduleStock+" "+loadStockMainDo.vehicleCapacityUnit)
//                        tvNonScheduledStock.setText(""+nonScheduleStock+" "+loadStockMainDo.vehicleCapacityUnit)
//
//                        var total = 0.0
//                        tvCapacity.setText(preferenceUtils.getStringFromPreference(PreferenceUtils.TOTAL_VEHICLE_CAPACITY, "")+" "+unit)
//                        var  capacity=preferenceUtils.getStringFromPreference(PreferenceUtils.TOTAL_VEHICLE_CAPACITY,"")
//                           total=capacity.toDouble()
//                        scheduleTotal=scheduleStock.toDouble()
//                        preferenceUtils.saveString(PreferenceUtils.TOTAL_NON_SCHEDULED_STOCK, loadStockMainDo.totalNonScheduledStock)
//
//                        nonScheduleTotal=nonScheduleStock.toDouble()
//                        tvBalanceStock.setText(""+(total-scheduleTotal-nonScheduleTotal)+" "+ loadStockMainDo.vehicleCapacityUnit)

                    }
                }
                loadVanSaleRequest.execute()
            } else {
//                tvNonScheduledStock.setText("0")
                var loadStockAdapter = LoadStockAdapter(this@NewLoadVanSaleStockTabActivity, scheduleDos, "Shipments")
                recycleview.setAdapter(loadStockAdapter)
                if (percentage != 0.0) {
                    tvTotal.visibility = View.VISIBLE
                    tvTotal.setText("" + percentage + "%")

                }

            }
        } else {
            showToast("No Internet connection, please try again later")
        }
    }

    override fun onButtonNoClick(from: String) {

        if ("SUCCESS".equals(from, ignoreCase = true)) {
//            finish()

        }
    }

    override fun onButtonYesClick(from: String) {

        if ("SUCCESS".equals(from, ignoreCase = true)) {
            StorageManager.getInstance(this).saveScheduledVehicleStockInLocal(scheduleDos!!)
            StorageManager.getInstance(this).saveVanScheduleProducts(this@NewLoadVanSaleStockTabActivity, scheduleDos)
            StorageManager.getInstance(this).saveNonScheduledVehicleStockInLocal(nonScheduleDos!!)
            StorageManager.getInstance(this).saveVanNonScheduleProducts(this@NewLoadVanSaleStockTabActivity, nonScheduleDos)

            var vehicleCheckInDo = StorageManager.getInstance(this).getVehicleCheckInData(this)
            vehicleCheckInDo.loadStock = resources.getString(R.string.loaded_stock)
            vehicleCheckInDo.checkinTimeCaptureStockLoadingTime = CalendarUtils.getTime()
            vehicleCheckInDo.checkinTimeCaptureStockLoadingDate = CalendarUtils.getDate()

            if (StorageManager.getInstance(this).insertCheckInData(this, vehicleCheckInDo)) {
                refreshMenuAdapter()
                setResult(4, null)
                finish()
            } else {
                showToast("Unable to load stock")
            }

        } else
            if ("FAILURE".equals(from, ignoreCase = true)) {


                //finish()
            }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == 10) {
            if (data != null) {
                val site = data.getStringExtra("site_id");
                if (site.equals("clear", true)) {
                    tv_no_data_new.visibility = View.GONE
                    recycleview.visibility = View.VISIBLE
                    val loadStockAdapter = LoadStockAdapter(this@NewLoadVanSaleStockTabActivity, availableStockDos, "Shipments")
                    recycleview.setAdapter(loadStockAdapter)
                } else {
                    //Toast.makeText(this, site, Toast.LENGTH_LONG).show()
                    val availableStockDosLocal: ArrayList<LoadStockDO> = ArrayList()
                    Log.d("site--->", availableStockDos.size.toString())

                    for (items in availableStockDos) {
                        Log.d("site--->", items.site.toString())
                        if (items.site.equals(site, true)) {
                            availableStockDosLocal.add(items)

                        }
                    }
                    if (availableStockDosLocal.size > 0) {
                        tv_no_data_new.visibility = View.GONE
                        recycleview.visibility = View.VISIBLE
                        val loadStockAdapter = LoadStockAdapter(this@NewLoadVanSaleStockTabActivity, availableStockDosLocal, "Shipments")
                        recycleview.setAdapter(loadStockAdapter)
                    } else {
                        tv_no_data_new.visibility = View.VISIBLE
                        recycleview.visibility = View.GONE
                        tv_no_data_new.text = "No data found for site " + site
                    }
                }

            };
        }
    }
}