package com.tbs.brothersgas.haadhir.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.tbs.brothersgas.haadhir.Activitys.CustomerDetailsActivity;
import com.tbs.brothersgas.haadhir.Model.CustomerDo;
import com.tbs.brothersgas.haadhir.R;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;

import java.util.List;

public class CustomerAdapter extends RecyclerView.Adapter<CustomerAdapter.MyViewHolder>  {

    private List<CustomerDo> listOrderDos;
    private String imageURL;
    private Context context;
    private String userId, paymentType,cardId;
    PreferenceUtils preferenceUtils;




    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvCustomerName, tvCustomerId;
        public TextView btnPay;
        private LinearLayout llDetails;
        public TextView tvOrderId;

        public MyViewHolder(View view) {
            super(view);
            llDetails = (LinearLayout) view.findViewById(R.id.llDetails);
            tvCustomerName = (TextView) view.findViewById(R.id.tvCustomerName);
            tvCustomerId = (TextView) view.findViewById(R.id.tvCustomerID);



        }
    }


    public CustomerAdapter(Context context,List<CustomerDo> listOrderDos) {
        this.context = context;
        this.listOrderDos = listOrderDos;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.customer_data, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        final CustomerDo customerDo = listOrderDos.get(position);
        holder.tvCustomerName.setText(customerDo.customerName);
        holder.tvCustomerId.setText("" + customerDo.customerId);

        holder.llDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, CustomerDetailsActivity.class);
                intent.putExtra("Code", customerDo.customerId);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);

            }
        });


    }

    @Override
    public int getItemCount() {
        return listOrderDos.size();
    }

}
