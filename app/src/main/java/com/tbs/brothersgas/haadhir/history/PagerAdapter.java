package com.tbs.brothersgas.haadhir.history;

/**
 * Created by sandy on 2/15/2018.
 */

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;


import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryMainDO;
import com.tbs.brothersgas.haadhir.Model.UnPaidInvoiceMainDO;

public class PagerAdapter extends FragmentStatePagerAdapter  {
ActiveDeliveryMainDO activeDeliveryMainDO;
UnPaidInvoiceMainDO unPaidInvoiceMainDO;

    public PagerAdapter(FragmentManager fm, ActiveDeliveryMainDO activeDeliveryMainDo, UnPaidInvoiceMainDO unPaidInvoiceMainDo) {
        super(fm);

        this.activeDeliveryMainDO = activeDeliveryMainDo;
        this.unPaidInvoiceMainDO=unPaidInvoiceMainDo;

    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                DeliveryList deliveryList = new DeliveryList();
                Bundle vegBundle = new Bundle();
                vegBundle.putSerializable("DeliveryList",activeDeliveryMainDO.activeDeliveryDOS);
                deliveryList.setArguments(vegBundle);
                return deliveryList;
            case 1:
                InvoiceList invoiceList = new InvoiceList();
                Bundle nonVegBundle = new Bundle();
                nonVegBundle.putSerializable("InvoiceList",unPaidInvoiceMainDO.unPaidInvoiceDOS);
                invoiceList.setArguments(nonVegBundle);
                return invoiceList;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

}
