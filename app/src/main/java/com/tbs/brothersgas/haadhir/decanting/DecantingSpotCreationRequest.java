package com.tbs.brothersgas.haadhir.decanting;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.brothersgas.haadhir.Activitys.BaseActivity;
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryDO;
import com.tbs.brothersgas.haadhir.Model.CreateDeliveryMainDO;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;
import com.tbs.brothersgas.haadhir.utils.WebServiceConstants;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class DecantingSpotCreationRequest extends AsyncTask<String, Void, Boolean> {
    String message = "";
    private CreateDeliveryMainDO createDeliveryMainDO;
    private Context mContext;
    ArrayList<ActiveDeliveryDO> productDos;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;
    static int networkTimeOut = 60 * 3000;
    String site, customer;
    String address;

    public DecantingSpotCreationRequest(String customer, String site, String address, ArrayList<ActiveDeliveryDO> productDos, Context mContext) {

        this.mContext = mContext;
        this.customer = customer;
        this.site = site;
        this.address = address;
        this.productDos = productDos;


    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        public void onCompleted(boolean isError, CreateDeliveryMainDO createDeliveryMainDO, String message);

    }

    public boolean runRequest() {
        // System.out.println("CUSTOMER ID " + customerId);
        String NAMESPACE = "http://www.adonix.com/WSS";
        String METHOD_NAME = "run";
        String SOAP_ACTION = "CAdxWebServiceXmlCC";
//        String URL = "http://183.82.9.23:8124/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC";
        preferenceUtils = new PreferenceUtils(mContext);
        username = preferenceUtils.getStringFromPreference(PreferenceUtils.A_USER_NAME, "");
        password = preferenceUtils.getStringFromPreference(PreferenceUtils.A_PASSWORD, "");
        ip = preferenceUtils.getStringFromPreference(PreferenceUtils.IP_ADDRESS, "");
        port = preferenceUtils.getStringFromPreference(PreferenceUtils.PORT, "");
        pool = preferenceUtils.getStringFromPreference(PreferenceUtils.ALIAS, "");
        String vehicle = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_CODE, "");
        String role = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "");

        String URL = "http://" + ip + ":" + port + "/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC";

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        // Set all input params
        request.addProperty("publicName", WebServiceConstants.DECANTING_SPOT_CREATION);
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("I_YTFCY", site);
            jsonObject.put("I_YBPR", customer);
            jsonObject.put("I_YADR", address);
            jsonObject.put("I_YAPPUSR", role);
            jsonObject.put("I_YVEHCOD", vehicle);
            JSONArray jsonArray = new JSONArray();

            if (productDos != null && productDos.size() > 0) {
                for (int i = 0; i < productDos.size(); i++) {
                    JSONObject jsonObject1 = new JSONObject();
                    jsonObject1.put("I_YITMREF", productDos.get(i).product);
                    jsonObject1.put("I_YQTY", productDos.get(i).orderedQuantity);
                    jsonArray.put(i, jsonObject1);
                }
                jsonObject.put("GRP2", jsonArray);
            }
        } catch (Exception e) {
            System.out.println("Exception " + e);
        }


        request.addProperty("inputXml", jsonObject.toString());

        SoapObject callcontext = new SoapObject("", "callContext");
        // Set all input params
        callcontext.addProperty("codeLang", "ENG");
        callcontext.addProperty("poolAlias", pool);
        callcontext.addProperty("poolId", "");
        callcontext.addProperty("codeUser", username);
        callcontext.addProperty("password", password);
        callcontext.addProperty("requestConfig", "adxwss.trace.on=off");

        request.addSoapObject(callcontext);


        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
//        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(550);

        envelope.setOutputSoapObject(request);

        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL, networkTimeOut);
        androidHttpTransport.debug = true;

        try {
            List<HeaderProperty> headerList = new ArrayList<HeaderProperty>();
            headerList.add(new HeaderProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode((username + ":" + password).getBytes())));


            androidHttpTransport.call(SOAP_ACTION, envelope, headerList);

            SoapObject response = (SoapObject) envelope.getResponse();
            String resultXML = (String) response.getProperty("resultXml");
            if (resultXML != null && resultXML.length() > 0) {
                return parseXML(resultXML);
            } else {
                String error = response.getProperty(0).toString();
                String[] messagee = error.split("message=");
                error = messagee[1].replace("]", "");
                error = error.replace("}", "");
                error = error.replace(";", "");
                error = error.replace(".", "");
                error = error.replace("[", " ");
                error = error.replace("|", "");
                error = error.replace("\\", "");
                error = error.replace("{", "");

                message = error;

                return false;
            }
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            message = "Socket Time Out Exception Please try again";
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            message = " " + e;

            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("create delivery xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            createDeliveryMainDO = new CreateDeliveryMainDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {
                        if (attribute.equalsIgnoreCase("O_YDOCNUM")) {
                            if (text.length() > 0) {
                                createDeliveryMainDO.deliveryNumber = text;

                            }


                        } else if (attribute.equalsIgnoreCase("O_YMISNUM")) {
                            if (text.length() > 0) {
                                createDeliveryMainDO.miscNumber = text;

                            }


                        } else if (attribute.equalsIgnoreCase("O_YMESSAGE")) {
                            if (text.length() > 0) {
                                createDeliveryMainDO.message = text;

                            }


                        } else if (attribute.equalsIgnoreCase("O_YDSTATUS")) {
                            if (text.length() > 0) {
                                createDeliveryMainDO.status = Integer.parseInt(text);

                            }


                        } else if (attribute.equalsIgnoreCase("O_YMSTATUS")) {
                            if (text.length() > 0) {
                                createDeliveryMainDO.miscStatus = Integer.parseInt(text);

                            }


                        }
                        text = "";

                    }

                    if (endTag.equalsIgnoreCase("GRP")) {
                    }


                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);
//            message = "Exception " + e;

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        ((BaseActivity) mContext).showLoader();
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        ((BaseActivity) mContext).hideLoader();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, createDeliveryMainDO, message);
        }
    }
}