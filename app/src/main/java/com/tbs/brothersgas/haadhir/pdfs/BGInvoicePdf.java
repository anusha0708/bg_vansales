package com.tbs.brothersgas.haadhir.pdfs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.LayoutDirection;
import android.util.Log;
import android.widget.Toast;

import com.google.zxing.WriterException;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.FontProvider;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfDocument;
import com.itextpdf.text.pdf.PdfEncodings;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.tbs.brothersgas.haadhir.Activitys.BaseActivity;
import com.tbs.brothersgas.haadhir.Model.CreateInvoicePaymentDO;
import com.tbs.brothersgas.haadhir.Model.CustomerDo;
import com.tbs.brothersgas.haadhir.Model.PdfInvoiceDo;
import com.tbs.brothersgas.haadhir.R;
import com.tbs.brothersgas.haadhir.common.AppConstants;
import com.tbs.brothersgas.haadhir.database.StorageManager;
import com.tbs.brothersgas.haadhir.pdfs.utils.PDFConstants;
import com.tbs.brothersgas.haadhir.pdfs.utils.PDFOperations;
import com.tbs.brothersgas.haadhir.pdfs.utils.PreviewActivity;
import com.tbs.brothersgas.haadhir.utils.NumberToWord;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;
import com.tbs.brothersgas.haadhir.utils.Util;

import org.w3c.dom.Node;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import static com.itextpdf.text.pdf.PdfName.BORDER;
import static com.itextpdf.text.pdf.PdfName.DEST;
import static com.itextpdf.text.pdf.PdfName.FONT;

/**
 * Created by VenuAppasani on 04-11-2018.
 * Copyright (C) 2018 TBS - All Rights Reserved
 */
public class BGInvoicePdf {

    private static Context context;
    private Document document = null;
    private PdfPTable parentTable;

    private Font normalFont, boldFont;
    Font arbcFont,urduFont;

    private BGInvoicePdf() {

    }

    public static BGInvoicePdf getBuilder(Context mContext) {
        //create pdf here
        context = mContext;
        return new BGInvoicePdf();
    }

    private CreateInvoicePaymentDO createInvoicePaymentDO;

    @SuppressLint("StaticFieldLeak")
    public BGInvoicePdf build(CreateInvoicePaymentDO createInvoicePaymentDO, String type) {
        this.createInvoicePaymentDO = createInvoicePaymentDO;
        Log.d("crate--->", createInvoicePaymentDO + "");
        new AsyncTask<Void, Void, Void>() {

            ProgressDialog progressDialog = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = new ProgressDialog(context);
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setCancelable(false);
                progressDialog.setMessage("Generating Invoice Please wait...");
                if (!type.equalsIgnoreCase("AUTO")) {
                    if (!((Activity) context).isFinishing()) {
                        try {
                            progressDialog.show();

                        } catch (Exception e) {
                            Log.d("Try...", String.valueOf(e));
                        }
                    }
                }
            }

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    parentTable = new PdfPTable(1);
                    parentTable.setWidthPercentage(100);
                    init(type);

                    addHeaderLogo();

                    addHeaderLabel();
                    addCustomerDetailsToPdf();
//                    shipmentDetailsToPdf(); using in above method
                    addProductsTableLabels();
                    addProductsToPdf();
                    addSignature(createInvoicePaymentDO.signature);


                    addFooterToPdf();
                    addQRCode();

                    document.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (progressDialog.isShowing()) {
                    progressDialog.cancel();

                }
                sendPDfewDoc();
            }
        }.execute();
        return this;
    }

    @SuppressLint("StaticFieldLeak")
    public BGInvoicePdf createHistoryPDF(CreateInvoicePaymentDO createInvoicePaymentDO, String type) {
        this.createInvoicePaymentDO = createInvoicePaymentDO;
        Log.d("create--->", createInvoicePaymentDO + "");
        new AsyncTask<Void, Void, Void>() {

            ProgressDialog progressDialog = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = new ProgressDialog(context);
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setCancelable(false);
                progressDialog.setMessage("Preparing Invoice Please wait...");
//                progressDialog.show();
            }

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    parentTable = new PdfPTable(1);
                    parentTable.setWidthPercentage(100);
                    init(type);

                    addHeaderLogo();

                    addHeaderLabel();
                    addCustomerDetailsToPdf();
//                    shipmentDetailsToPdf(); using in above method
                    addProductsTableLabels();
                    addProductsToPdf();
                    addSignature(createInvoicePaymentDO.signature);

                    addFooterToPdf();
                    addQRCode();

                    document.close();

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                progressDialog.cancel();


                Intent intent = new Intent(context.getApplicationContext(), com.tbs.brothersgas.haadhir.history.PreviewActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("KEY", PDFConstants.BG_INVOICE_PDF_NAME);
                intent.putExtra("ID", createInvoicePaymentDO.invoiceNumber);

                context.startActivity(intent);
            }
        }.execute();
        return this;
    }

    @SuppressLint("StaticFieldLeak")
    public BGInvoicePdf createPDF(CreateInvoicePaymentDO createInvoicePaymentDO, String type) {
        this.createInvoicePaymentDO = createInvoicePaymentDO;
        Log.d("create--->", createInvoicePaymentDO + "");
        new AsyncTask<Void, Void, Void>() {

            ProgressDialog progressDialog = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = new ProgressDialog(context);
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setCancelable(false);
                progressDialog.setMessage("Preparing Invoice Please wait...");
                progressDialog.show();
            }

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    parentTable = new PdfPTable(1);
                    parentTable.setWidthPercentage(100);
                    init(type);

                    addHeaderLogo();

                    addHeaderLabel();
                    addCustomerDetailsToPdf();
//                    shipmentDetailsToPdf(); using in above method
                    addProductsTableLabels();
                    addProductsToPdf();
                    addSignature(createInvoicePaymentDO.signature);

                    addFooterToPdf();
                    addQRCode();

                    document.close();

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                progressDialog.cancel();


                Intent intent = new Intent(context.getApplicationContext(), PreviewActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("KEY", PDFConstants.BG_INVOICE_PDF_NAME);
                context.startActivity(intent);
            }
        }.execute();
        return this;
    }

    private void init(String type) {

        boldFont = new Font(Font.FontFamily.COURIER, 10.0f, Font.BOLD, BaseColor.BLACK);
        normalFont = new Font(Font.FontFamily.COURIER, 10.0f, Font.NORMAL, BaseColor.BLACK);
        if (type.equalsIgnoreCase("Preview")) {
            normalFont = boldFont;
        }
        arbcFont = FontFactory.getFont("assets/arabic.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        urduFont = FontFactory.getFont("assets/arabic.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

        File file = new File(Util.getAppPath(context) + "BGInvoice.pdf");
        if (file.exists())
            file.delete();
        try {
            file.createNewFile();
            document = new Document();
            // Location to save
            PdfWriter.getInstance(document, new FileOutputStream(file));
            // Open to write
            document.open();
            // Document Settings
            document.setPageSize(PageSize.A4);
            document.addCreationDate();
            document.addAuthor("TBS");
            document.addCreator("Kishore Ganji");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void addHeaderLogo() {
        try {
            document.add(PDFOperations.getInstance().
                    getHeadrLogoIOSCertificateLogo(context, document, createInvoicePaymentDO.companyCode));
        } catch (DocumentException e) {
            e.printStackTrace();
        }

    }


    private void addHeaderLabel() {
        try {
//            Font headerLabelFont = new Font(Font.FontFamily.COURIER, 11.0f, Font.BOLD, BaseColor.BLACK);
//            Paragraph headerLabel = new Paragraph(PDFConstants.HEADER_TEXT_BG_INVOICE, headerLabelFont);
//            headerLabel.setAlignment(Element.ALIGN_CENTER);
//            headerLabel.setPaddingTop(10);
//            document.add(headerLabel);
//            addEmptySpaceLine(1);

            Font headerLabelFont = new Font(Font.FontFamily.COURIER, 11.0f, Font.BOLD, BaseColor.BLACK);
//            Paragraph headerLabel = new Paragraph(PDFConstants.HEADER_1, headerLabelFont);
            Paragraph headerLabel = new Paragraph(createInvoicePaymentDO.supplierName, headerLabelFont);

            headerLabel.setAlignment(Element.ALIGN_CENTER);
            headerLabel.setPaddingTop(10);
            document.add(headerLabel);
//            Paragraph headerLabel1 = new Paragraph(PDFConstants.HEADER_2, headerLabelFont);

            Paragraph headerLabel1 = new Paragraph(PDFConstants.HEADER_TEXT_BG_INVOICE, headerLabelFont);
            headerLabel1.setAlignment(Element.ALIGN_CENTER);
            headerLabel1.setPaddingTop(10);
            document.add(headerLabel1);
            addEmptySpaceLine(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addCustomerDetailsToPdf() {
        try {

            PdfPTable headerParentTable = new PdfPTable(1);
            headerParentTable.setPaddingTop(10f);
            headerParentTable.setWidthPercentage(100);

            float[] columnWidths = {3, 0.3f, 5.5f, 2, 0.3f, 2.5f};
            PdfPTable headerTable = new PdfPTable(columnWidths);
            headerTable.setWidthPercentage(100);
            PdfPCell cellOne = new PdfPCell();
            cellOne.setBorder(Rectangle.NO_BORDER);
            PdfPCell cellOne1 = new PdfPCell();
            cellOne1.setBorder(Rectangle.NO_BORDER);

            PdfPCell cellTwo = new PdfPCell();
            cellTwo.setBorder(Rectangle.NO_BORDER);
            PdfPCell cellThree = new PdfPCell();
            cellThree.setBorder(Rectangle.NO_BORDER);
            PdfPCell cellThree1 = new PdfPCell();
            cellThree1.setBorder(Rectangle.NO_BORDER);
            PdfPCell cellFour = new PdfPCell();
            cellFour.setBorder(Rectangle.NO_BORDER);

            cellOne.addElement(new Phrase(PDFConstants.DELIVERY_NUMBER, boldFont));

            cellOne.addElement(new Phrase(PDFConstants.INVOICE_NUMBER, boldFont));
            cellOne.addElement(new Phrase(PDFConstants.CUSTOMER_NAME, boldFont));
            cellOne.addElement(new Phrase(PDFConstants.CUSTOMER_TRN, boldFont));


            cellOne.addElement(new Phrase(PDFConstants.CUSTOMER_ADDRESS, boldFont));

            cellOne1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellOne1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellOne1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellOne1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellTwo.addElement(new Phrase(createInvoicePaymentDO.shipmentID, normalFont));

            cellTwo.addElement(new Phrase(createInvoicePaymentDO.invoiceNumber, normalFont));
            cellTwo.addElement(new Phrase(createInvoicePaymentDO.customerDescription, normalFont));
            if (createInvoicePaymentDO.customerTrn.isEmpty()) {
                cellTwo.addElement(new Phrase("                     ", normalFont));

            } else {

                cellTwo.addElement(new Phrase(createInvoicePaymentDO.customerTrn, normalFont));
            }


            cellTwo.addElement(new Phrase(getAddress(true), normalFont));

            cellThree.addElement(new Phrase(PDFConstants.DATE, boldFont));
            cellThree.addElement(new Phrase(PDFConstants.TIME, boldFont));
            cellThree.addElement(new Phrase(PDFConstants.USER_ID, boldFont));
            cellThree.addElement(new Phrase(PDFConstants.USER_NAME, boldFont));
            cellThree.addElement(new Phrase(PDFConstants.VEHICLE_NUMBER, boldFont));
            cellThree1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));

            cellThree1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellThree1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellThree1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellThree1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            if (createInvoicePaymentDO.createdDate.length() > 0) {
                String dMonth = createInvoicePaymentDO.createdDate.substring(4, 6);
                String dyear = createInvoicePaymentDO.createdDate.substring(0, 4);
                String dDate = createInvoicePaymentDO.createdDate.substring(Math.max(createInvoicePaymentDO.createdDate.length() - 2, 0));

                cellFour.addElement(new Phrase(dDate + "-" + dMonth + "-" + dyear, normalFont));
            } else {
                cellFour.addElement(new Phrase("", normalFont));

            }
            PreferenceUtils preferenceUtils = new PreferenceUtils(context);

            cellFour.addElement(new Phrase(createInvoicePaymentDO.createdTime, normalFont));
//            cellFour.addElement(new Phrase(((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, ""), normalFont));
//            cellFour.addElement(new Phrase(((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.LOGIN_DRIVER_NAME, ""), normalFont));
            cellFour.addElement(new Phrase(createInvoicePaymentDO.createUserID, normalFont));
            cellFour.addElement(new Phrase(createInvoicePaymentDO.createUserName, normalFont));
            String vehicleNumber = preferenceUtils.getStringFromPreference(PreferenceUtils.CV_PLATE, "");

            cellFour.addElement(new Phrase(vehicleNumber, normalFont));

            headerTable.addCell(cellOne);
            headerTable.addCell(cellOne1);
            headerTable.addCell(cellTwo);
            headerTable.addCell(cellThree);
            headerTable.addCell(cellThree1);
            headerTable.addCell(cellFour);

            PdfPCell pdfPCell = new PdfPCell();
            pdfPCell.addElement(headerTable);


            float[] shipmentColumnsWidth = {3, 0.3f, 10};
            PdfPTable shipmentTable = new PdfPTable(shipmentColumnsWidth);
            shipmentTable.setWidthPercentage(100);
            PdfPCell shipCellOne = new PdfPCell();
            shipCellOne.setBorder(Rectangle.NO_BORDER);
            PdfPCell shipCellOne1 = new PdfPCell();
            shipCellOne1.setBorder(Rectangle.NO_BORDER);
            PdfPCell shipCellTwo = new PdfPCell();
            shipCellTwo.setBorder(Rectangle.NO_BORDER);


            shipCellOne.addElement(new Phrase(PDFConstants.SUPPLIER_NAME, boldFont));
            shipCellOne.addElement(new Phrase(PDFConstants.SUPPLIER_TRN, boldFont));
            shipCellOne.addElement(new Phrase(PDFConstants.PAYMENT_TERM, boldFont));
            shipCellOne.addElement(new Phrase(PDFConstants.VEHICLE_CODE, boldFont));

            shipCellOne.addElement(new Phrase(PDFConstants.REGISTERED_ADDRESS, boldFont));

            shipCellOne1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            shipCellOne1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            shipCellOne1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            shipCellOne1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            shipCellOne1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));

            shipCellTwo.addElement(new Phrase(createInvoicePaymentDO.siteDescription, normalFont));
            if (createInvoicePaymentDO.supplierTrn.isEmpty()) {
                shipCellTwo.addElement(new Phrase("                     ", normalFont));

            } else {

                shipCellTwo.addElement(new Phrase(createInvoicePaymentDO.supplierTrn, normalFont));
            }
            shipCellTwo.addElement(new Phrase(createInvoicePaymentDO.paymentTerm, normalFont));
            String vehicleCode = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_CODE, "");
            shipCellTwo.addElement(new Phrase(vehicleCode, normalFont));
            shipCellTwo.addElement(new Phrase(getAddress(false), normalFont));
            shipmentTable.addCell(shipCellOne);
            shipmentTable.addCell(shipCellOne1);
            shipmentTable.addCell(shipCellTwo);
            shipmentTable.addCell(getBorderlessCell("", Element.ALIGN_CENTER));

            pdfPCell.addElement(shipmentTable);
            headerParentTable.addCell(pdfPCell);
            document.add(headerParentTable);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void addProductsTableLabels() {
        try {

            float[] productColumnsWidth = {2.5f, 10, 3, 4, 4};
            PdfPTable productsTable = new PdfPTable(productColumnsWidth);
            productsTable.setWidthPercentage(100);
            PdfPCell cellOne = new PdfPCell();
            PdfPCell cellTwo = new PdfPCell();
            PdfPCell cellThree = new PdfPCell();
            PdfPCell cellFour = new PdfPCell();
            PdfPCell cellFive = new PdfPCell();

            Paragraph pOne = new Paragraph(PDFConstants.SR_NO, boldFont);
            pOne.setAlignment(Element.ALIGN_CENTER);
            cellOne.addElement(pOne);

            Paragraph pTwo = new Paragraph(PDFConstants.PARTICULARS, boldFont);
            pTwo.setAlignment(Element.ALIGN_LEFT);
            cellTwo.addElement(pTwo);

            Paragraph pThree = new Paragraph(PDFConstants.QTY, boldFont);
            pThree.setAlignment(Element.ALIGN_CENTER);
            cellThree.addElement(pThree);

            Paragraph pFour = new Paragraph(PDFConstants.UNIT_PRICE, boldFont);
            pFour.setAlignment(Element.ALIGN_CENTER);
            cellFour.addElement(pFour);

            Paragraph pFive = new Paragraph(PDFConstants.AMOUNT, boldFont);
            pFive.setAlignment(Element.ALIGN_CENTER);
            cellFive.addElement(pFive);


            productsTable.addCell(cellOne);
            productsTable.addCell(cellTwo);
            productsTable.addCell(cellThree);
            productsTable.addCell(cellFour);
            productsTable.addCell(cellFive);

            document.add(productsTable);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    private void addProductsToPdf() {

        try {

            float[] productColumnsWidth = {2.5f, 10, 3, 4, 4};
            PdfPTable productsTable = new PdfPTable(productColumnsWidth);
            productsTable.setWidthPercentage(100);

            if (createInvoicePaymentDO.pdfInvoiceDos != null && createInvoicePaymentDO.pdfInvoiceDos.size() > 0) {

                for (int i = 0; i < createInvoicePaymentDO.pdfInvoiceDos.size(); i++) {
                    PdfInvoiceDo pdfInvoiceDo = createInvoicePaymentDO.pdfInvoiceDos.get(i);
                    PdfPCell cellOne = new PdfPCell();
                    PdfPCell cellTwo = new PdfPCell();
                    PdfPCell cellThree = new PdfPCell();
                    PdfPCell cellFour = new PdfPCell();
                    PdfPCell cellFive = new PdfPCell();

                    cellOne.setHorizontalAlignment(Element.ALIGN_CENTER);
                    Paragraph prOne = new Paragraph("" + (i + 1), normalFont);
                    prOne.setAlignment(Element.ALIGN_CENTER);
                    cellOne.addElement(prOne);
                    cellOne.addElement(new Phrase("\n"));
                    cellOne.addElement(new Phrase("\n"));

                    cellTwo.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cellTwo.addElement(new Phrase("" + pdfInvoiceDo.productDesccription, normalFont));
                    cellTwo.addElement(new Phrase("Discount", normalFont));
                    cellTwo.addElement(new Phrase("VAT  @ " + createInvoicePaymentDO.taxRate + "%", normalFont));

                    cellThree.setHorizontalAlignment(Element.ALIGN_CENTER);
                    Paragraph prThree = new Paragraph("" + pdfInvoiceDo.deliveredQunatity + " " + pdfInvoiceDo.quantityUnits, normalFont);
                    prThree.setAlignment(Element.ALIGN_CENTER);
                    cellThree.addElement(prThree);
                    cellThree.addElement(new Phrase("\n"));
                    cellThree.addElement(new Phrase("\n"));

                    cellFour.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    Paragraph prFour = new Paragraph("" + pdfInvoiceDo.grossPrice, normalFont);
                    prFour.setAlignment(Element.ALIGN_RIGHT);
                    cellFour.addElement(prFour);
                    cellFour.addElement(new Phrase("\n"));
                    cellFour.addElement(new Phrase("\n"));

                    cellFive.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    Paragraph prFive = new Paragraph("" + pdfInvoiceDo.amount, normalFont);
                    prFive.setAlignment(Element.ALIGN_RIGHT);
                    cellFive.addElement(prFive);
                    Paragraph prFive1 = new Paragraph("" + pdfInvoiceDo.discount, normalFont);
                    prFive1.setAlignment(Element.ALIGN_RIGHT);
                    cellFive.addElement(prFive1);
                    Paragraph prFive2 = new Paragraph("" + pdfInvoiceDo.vatPercentage, normalFont);
                    prFive2.setAlignment(Element.ALIGN_RIGHT);
                    cellFive.addElement(prFive2);


                    productsTable.addCell(cellOne);
                    productsTable.addCell(cellTwo);
                    productsTable.addCell(cellThree);
                    productsTable.addCell(cellFour);
                    productsTable.addCell(cellFive);
                }
                document.add(productsTable);

                float[] productColumnsWidth1 = {2.5f, 10, 3, 4, 4};
                PdfPTable totalTable = new PdfPTable(productColumnsWidth1);
                totalTable.setWidthPercentage(100);

                PdfPCell cellOne = new PdfPCell();
                PdfPCell cellTwo = new PdfPCell();
                PdfPCell cellThree = new PdfPCell();
                PdfPCell cellFour = new PdfPCell();
                PdfPCell cellFive = new PdfPCell();

                cellOne.addElement(new Phrase("\n"));
                cellOne.addElement(new Phrase("\n"));
                cellOne.addElement(new Phrase("\n"));
                cellOne.addElement(new Phrase("\n"));
                cellOne.addElement(new Phrase("\n"));

                cellTwo.setHorizontalAlignment(Element.ALIGN_LEFT);
                cellTwo.addElement(new Phrase("Total Gross Amount", normalFont));
                cellTwo.addElement(new Phrase("Discount", normalFont));
                cellTwo.addElement(new Phrase("Total Excluding VAT", normalFont));
                cellTwo.addElement(new Phrase("VAT", normalFont));
                cellTwo.addElement(new Phrase("Total", normalFont));

                cellThree.addElement(new Phrase("\n"));
                cellThree.addElement(new Phrase("\n"));
                cellThree.addElement(new Phrase("\n"));
                cellThree.addElement(new Phrase("\n"));
                cellThree.addElement(new Phrase(""));

                cellFour.addElement(new Phrase("\n"));
                cellFour.addElement(new Phrase("\n"));
                cellFour.addElement(new Phrase("\n"));
                cellFour.addElement(new Phrase("\n"));
                cellFour.addElement(new Phrase("\n"));
                cellFour.addElement(new Phrase(""));

                cellFive.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cellFive.addElement(getParagraph("" + /*getTotalGrossAmount(createInvoicePaymentDO.pdfInvoiceDos)*/createInvoicePaymentDO.totalGrossAmount, Element.ALIGN_RIGHT));
                cellFive.addElement(getParagraph("" + createInvoicePaymentDO.totalDiscount, Element.ALIGN_RIGHT));
                cellFive.addElement(getParagraph("" + createInvoicePaymentDO.excludingTax, Element.ALIGN_RIGHT));
                cellFive.addElement(getParagraph("" + createInvoicePaymentDO.totalTax, Element.ALIGN_RIGHT));
                cellFive.addElement(getParagraph("" + createInvoicePaymentDO.includingTax, Element.ALIGN_RIGHT));

                totalTable.addCell(cellOne);
                totalTable.addCell(cellTwo);
                totalTable.addCell(cellThree);
                totalTable.addCell(cellFour);
                totalTable.addCell(cellFive);

                document.add(totalTable);

                PdfPTable noteTable = new PdfPTable(1);
                noteTable.setWidthPercentage(100);
                double totalAmount = getTotalAmount(createInvoicePaymentDO.pdfInvoiceDos);

                int afterDcimalVal = PDFOperations.getInstance().anyMethod(totalAmount);

                PdfPCell noteCell = new PdfPCell(new Phrase("AED : " + getAmountInLetters(totalAmount) + " and " + getAmountInLetters(afterDcimalVal) + " Fills", boldFont));
//                PdfPCell noteCell = new PdfPCell(new Phrase("AED : Eighteen Thousand Eight Hundred Forty Seven and 50/100 Only-", boldFont));
                noteCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                noteCell.setPadding(10);
                noteTable.addCell(noteCell);

                document.add(noteTable);
            }
            addEmptySpaceLine(1);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getDecimalAmount(String amount) {
        try {
            DecimalFormat df = new DecimalFormat("0.00");
            return df.format(amount);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private double getTotalGrossAmount(ArrayList<PdfInvoiceDo> pdfInvoiceDos) {
        double grossAmount = 0.0f;
        try {
            for (int i = 0; i < pdfInvoiceDos.size(); i++) {
                grossAmount = grossAmount + Double.parseDouble(pdfInvoiceDos.get(i).grossPrice);
            }
        } catch (Exception e) {
            return grossAmount;
        }

        return grossAmount;
    }

    private double getTotalDiscount(ArrayList<PdfInvoiceDo> pdfInvoiceDos) {
        double totalDiscountAmount = 0.0f;
        try {
            for (int i = 0; i < pdfInvoiceDos.size(); i++) {
                totalDiscountAmount = totalDiscountAmount + Double.parseDouble(pdfInvoiceDos.get(i).excludingTax);
            }
        } catch (Exception e) {
            return totalDiscountAmount;
        }

        return totalDiscountAmount;
    }

    private double getTotalExlTax(ArrayList<PdfInvoiceDo> pdfInvoiceDos) {
        double totalExlTax = 0.0f;
        try {
            for (int i = 0; i < pdfInvoiceDos.size(); i++) {
                totalExlTax = totalExlTax + Double.parseDouble(pdfInvoiceDos.get(i).excludingTax);
            }
        } catch (Exception e) {
            return totalExlTax;
        }

        return totalExlTax;
    }

    private double getTotalVat(ArrayList<PdfInvoiceDo> pdfInvoiceDos) {
        double totalVat = 0.0f;
        try {
            for (int i = 0; i < pdfInvoiceDos.size(); i++) {
                totalVat = totalVat + Double.parseDouble(pdfInvoiceDos.get(i).excludingTax);
            }
        } catch (Exception e) {
            return totalVat;
        }

        return totalVat;
    }

    private double getTotalAmount(ArrayList<PdfInvoiceDo> pdfInvoiceDos) {
        double totalAmount = 0.0f;
        try {
            /*totalAmount = getTotalDiscount(pdfInvoiceDos) -
                    getTotalGrossAmount(pdfInvoiceDos) +
                    getTotalExlTax(pdfInvoiceDos) -
                    getTotalVat(pdfInvoiceDos);*/
            return Double.parseDouble(createInvoicePaymentDO.includingTax);

        } catch (Exception e) {
            return totalAmount;
        }

    }

    private String getAmountInLetters(double amount) {
        return new NumberToWord().convert((int) amount);
        //return NumberToWords.getNumberToWords().getTextFromNumbers(amount);
    }

    private void addEmptySpaceLine(int noOfLines) {
        try {
            PdfPTable emptyTable = new PdfPTable(1);
            emptyTable.setWidthPercentage(100);
            for (int i = 0; i < noOfLines; i++) {
                PdfPCell emptyCell = new PdfPCell();
                emptyCell.setBorder(Rectangle.NO_BORDER);
                emptyCell.addElement(new Paragraph("\n"));
                emptyTable.addCell(emptyCell);
            }
            document.add(emptyTable);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    private Paragraph getParagraph(String content, int alignment) {
        Paragraph p1 = new Paragraph(content, normalFont);
        p1.setAlignment(alignment);
        return p1;
    }

    private void addQsns1() {


        try {
            float[] productColumnsWidth = {2.5f, 18, 5};
            PdfPTable productsTable = new PdfPTable(productColumnsWidth);
            productsTable.setWidthPercentage(100);
            PdfPCell cellOne   = new PdfPCell();
            PdfPCell cellTwo   = new PdfPCell();
            PdfPCell cellThree = new PdfPCell();
            Paragraph pOne = new Paragraph("1", boldFont);
            pOne.setAlignment(Element.ALIGN_CENTER);
            cellOne.addElement(pOne);

            cellTwo.addElement(new Phrase(context.getResources().getString(R.string.eng_qsn1), normalFont));
//            cellTwo.addElement(new Phrase( context.getResources().getString(R.string.arb_qsn1), arbcFont));
//            cellTwo.addElement(new Phrase( context.getResources().getString(R.string.urdu_qsn1), arbcFont));
            float[] productQsns2Width = {18};
            PdfPTable qsn2Table = new PdfPTable(productQsns2Width);
            qsn2Table.setWidthPercentage(100);
            PdfPCell two = new PdfPCell();
            two.setBorder(Rectangle.NO_BORDER);
            two.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
//            two.setHorizontalAlignment(Element.ALIGN_RIGHT);

            two.addElement(new Phrase( context.getResources().getString(R.string.arb_qsn1), arbcFont));

            Paragraph p3 = new Paragraph(context.getResources().getString(R.string.urdu_qsn1), urduFont);
            p3.setAlignment(Element.ALIGN_RIGHT);

            PdfPCell three = new PdfPCell();
            three.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
//            three.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);

            three.setBorder(Rectangle.NO_BORDER);
            three.addElement(p3);



            if (!createInvoicePaymentDO.expiryDate.isEmpty() && createInvoicePaymentDO.expiryDate.length() > 0) {
                String dMonth = createInvoicePaymentDO.expiryDate.substring(4, 6);
                String dyear = createInvoicePaymentDO .expiryDate.substring(0, 4);
                String dDate = createInvoicePaymentDO .expiryDate.substring(Math.max(createInvoicePaymentDO.expiryDate.length() - 2, 0));

                Paragraph pThree = new Paragraph(dDate + "-" + dMonth + "-" + dyear, boldFont);
                pThree.setAlignment(Element.ALIGN_CENTER);
                cellThree.addElement(pThree);
            }

            qsn2Table.addCell(two);
            qsn2Table.addCell(three);
            cellTwo.addElement(qsn2Table);

            productsTable.addCell(cellOne);
            productsTable.addCell(cellTwo);
            productsTable.addCell(cellThree);


            document.add(productsTable);

//            document.add(qsn2Table);
        } catch (DocumentException e) {
            e.printStackTrace();
        }

    }

    private void addQsns2() {
        try {
            float[] productColumnsWidth = {2.5f, 18, 5};
            PdfPTable productsTable = new PdfPTable(productColumnsWidth);
            productsTable.setWidthPercentage(100);
            PdfPCell cellOne   = new PdfPCell();
            PdfPCell cellTwo   = new PdfPCell();
            PdfPCell cellThree = new PdfPCell();
            Paragraph pOne = new Paragraph("2", boldFont);
            pOne.setAlignment(Element.ALIGN_CENTER);
            cellOne.addElement(pOne);

            cellTwo.addElement(new Phrase(context.getResources().getString(R.string.eng_qsn2), normalFont));
//            cellTwo.addElement(new Phrase( context.getResources().getString(R.string.arb_qsn1), arbcFont));
//            cellTwo.addElement(new Phrase( context.getResources().getString(R.string.urdu_qsn1), arbcFont));
            float[] productQsns2Width = {18};
            PdfPTable qsn2Table = new PdfPTable(productQsns2Width);
            qsn2Table.setWidthPercentage(100);
            PdfPCell two = new PdfPCell();
            two.setBorder(Rectangle.NO_BORDER);
            two.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
//            two.setHorizontalAlignment(Element.ALIGN_RIGHT);

            two.addElement(new Phrase( context.getResources().getString(R.string.arb_qsn2), arbcFont));
            Paragraph p3 = new Paragraph(context.getResources().getString(R.string.urdu_qsn2), urduFont);
            p3.setAlignment(Element.ALIGN_RIGHT);

            PdfPCell three = new PdfPCell();
            three.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);

            three.setBorder(Rectangle.NO_BORDER);
            three.addElement(p3);
            if (createInvoicePaymentDO.qsn2 == 2) {
                Paragraph pThree = new Paragraph("YES", boldFont);
                pThree.setAlignment(Element.ALIGN_CENTER);
                cellThree.addElement(pThree);

            } else {
                Paragraph pThree = new Paragraph("NO", boldFont);
                pThree.setAlignment(Element.ALIGN_CENTER);
                cellThree.addElement(pThree);


            }


            qsn2Table.addCell(two);
            qsn2Table.addCell(three);
            cellTwo.addElement(qsn2Table);

            productsTable.addCell(cellOne);
            productsTable.addCell(cellTwo);
            productsTable.addCell(cellThree);


            document.add(productsTable);

        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    private void addQsns3() {
        try {
            float[] productColumnsWidth = {2.5f, 18, 5};
            PdfPTable productsTable = new PdfPTable(productColumnsWidth);
            productsTable.setWidthPercentage(100);
            PdfPCell cellOne   = new PdfPCell();
            PdfPCell cellTwo   = new PdfPCell();
            PdfPCell cellThree = new PdfPCell();
            Paragraph pOne = new Paragraph("3", boldFont);
            pOne.setAlignment(Element.ALIGN_CENTER);
            cellOne.addElement(pOne);

            cellTwo.addElement(new Phrase(context.getResources().getString(R.string.eng_qsn3), normalFont));
//            cellTwo.addElement(new Phrase( context.getResources().getString(R.string.arb_qsn1), arbcFont));
//            cellTwo.addElement(new Phrase( context.getResources().getString(R.string.urdu_qsn1), arbcFont));
            float[] productQsns2Width = {18};
            PdfPTable qsn2Table = new PdfPTable(productQsns2Width);
            qsn2Table.setWidthPercentage(100);
            PdfPCell two = new PdfPCell();
            two.setBorder(Rectangle.NO_BORDER);
            two.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
//            two.setHorizontalAlignment(Element.ALIGN_RIGHT);

            two.addElement(new Phrase( context.getResources().getString(R.string.arb_qsn3), arbcFont));
            Paragraph p3 = new Paragraph(context.getResources().getString(R.string.urdu_qsn3), urduFont);
            p3.setAlignment(Element.ALIGN_RIGHT);

            PdfPCell three = new PdfPCell();
            three.setBorder(Rectangle.NO_BORDER);
            three.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);

            three.addElement(p3);
            if (createInvoicePaymentDO.qsn3 == 2) {
                Paragraph pThree = new Paragraph("YES", boldFont);
                pThree.setAlignment(Element.ALIGN_CENTER);
                cellThree.addElement(pThree);

            } else {
                Paragraph pThree = new Paragraph("NO", boldFont);
                pThree.setAlignment(Element.ALIGN_CENTER);
                cellThree.addElement(pThree);


            }


            qsn2Table.addCell(two);
            qsn2Table.addCell(three);
            cellTwo.addElement(qsn2Table);

            productsTable.addCell(cellOne);
            productsTable.addCell(cellTwo);
            productsTable.addCell(cellThree);


            document.add(productsTable);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    private void addQsns4() {
        try {
            float[] productColumnsWidth = {2.5f, 18, 5};
            PdfPTable productsTable = new PdfPTable(productColumnsWidth);
            productsTable.setWidthPercentage(100);
            PdfPCell cellOne   = new PdfPCell();
            PdfPCell cellTwo   = new PdfPCell();
            PdfPCell cellThree = new PdfPCell();
            Paragraph pOne = new Paragraph("4", boldFont);
            pOne.setAlignment(Element.ALIGN_CENTER);
            cellOne.addElement(pOne);

            cellTwo.addElement(new Phrase(context.getResources().getString(R.string.eng_qsn4), normalFont));
//            cellTwo.addElement(new Phrase( context.getResources().getString(R.string.arb_qsn1), arbcFont));
//            cellTwo.addElement(new Phrase( context.getResources().getString(R.string.urdu_qsn1), arbcFont));
            float[] productQsns2Width = {18};
            PdfPTable qsn2Table = new PdfPTable(productQsns2Width);
            qsn2Table.setWidthPercentage(100);
            PdfPCell two = new PdfPCell();
            two.setBorder(Rectangle.NO_BORDER);
            two.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
//            two.setHorizontalAlignment(Element.ALIGN_RIGHT);

            two.addElement(new Phrase( context.getResources().getString(R.string.arb_qsn4), arbcFont));
            Paragraph p3 = new Paragraph(context.getResources().getString(R.string.urdu_qsn4), urduFont);
            p3.setAlignment(Element.ALIGN_RIGHT);

            PdfPCell three = new PdfPCell();
            three.setBorder(Rectangle.NO_BORDER);
            three.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);

            three.addElement(p3);
            if (createInvoicePaymentDO.qsn2 == 2) {
                Paragraph pThree = new Paragraph("YES", boldFont);
                pThree.setAlignment(Element.ALIGN_CENTER);
                cellThree.addElement(pThree);

            } else {
                Paragraph pThree = new Paragraph("NO", boldFont);
                pThree.setAlignment(Element.ALIGN_CENTER);
                cellThree.addElement(pThree);


            }


            qsn2Table.addCell(two);
            qsn2Table.addCell(three);
            cellTwo.addElement(qsn2Table);

            productsTable.addCell(cellOne);
            productsTable.addCell(cellTwo);
            productsTable.addCell(cellThree);


            document.add(productsTable);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    private void addQsns5() {
        try {
            float[] productColumnsWidth = {2.5f, 18, 5};
            PdfPTable productsTable = new PdfPTable(productColumnsWidth);
            productsTable.setWidthPercentage(100);
            PdfPCell cellOne   = new PdfPCell();
            PdfPCell cellTwo   = new PdfPCell();
            PdfPCell cellThree = new PdfPCell();
            Paragraph pOne = new Paragraph("5", boldFont);
            pOne.setAlignment(Element.ALIGN_CENTER);
            cellOne.addElement(pOne);

            cellTwo.addElement(new Phrase(context.getResources().getString(R.string.eng_qsn5), normalFont));
//            cellTwo.addElement(new Phrase( context.getResources().getString(R.string.arb_qsn1), arbcFont));
//            cellTwo.addElement(new Phrase( context.getResources().getString(R.string.urdu_qsn1), arbcFont));
            float[] productQsns2Width = {18};
            PdfPTable qsn2Table = new PdfPTable(productQsns2Width);
            qsn2Table.setWidthPercentage(100);
            PdfPCell two = new PdfPCell();
            two.setBorder(Rectangle.NO_BORDER);
            two.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
//            two.setHorizontalAlignment(Element.ALIGN_RIGHT);

            two.addElement(new Phrase( context.getResources().getString(R.string.arb_qsn5), arbcFont));
            Paragraph p3 = new Paragraph(context.getResources().getString(R.string.urdu_qsn5), urduFont);
            p3.setAlignment(Element.ALIGN_RIGHT);

            PdfPCell three = new PdfPCell();
            three.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);

            three.setBorder(Rectangle.NO_BORDER);
            three.addElement(p3);
            if (createInvoicePaymentDO.qsn2 == 2) {
                Paragraph pThree = new Paragraph("YES", boldFont);
                pThree.setAlignment(Element.ALIGN_CENTER);
                cellThree.addElement(pThree);

            } else {
                Paragraph pThree = new Paragraph("NO", boldFont);
                pThree.setAlignment(Element.ALIGN_CENTER);
                cellThree.addElement(pThree);


            }


            qsn2Table.addCell(two);
            qsn2Table.addCell(three);
            cellTwo.addElement(qsn2Table);

            productsTable.addCell(cellOne);
            productsTable.addCell(cellTwo);
            productsTable.addCell(cellThree);


            document.add(productsTable);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }


    private void addQRCode() {
        try {

            float[] productColumnsWidth1 = {2, 6};
            PdfPTable sig = new PdfPTable(productColumnsWidth1);
            sig.setWidthPercentage(100);
            Image image = getQRCODE();
            if (image != null) {
                image.setBorder(Rectangle.NO_BORDER);
                PdfPCell pdfPCell = new PdfPCell();
                pdfPCell.setBorder(Rectangle.NO_BORDER);
                pdfPCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                pdfPCell.setVerticalAlignment(Element.ALIGN_CENTER);
                pdfPCell.addElement(image);
                sig.addCell(pdfPCell);
                PdfPCell pCell = new PdfPCell();
                pCell.setBorder(Rectangle.NO_BORDER);
                sig.addCell(pCell);
                document.add(sig);

            }

            PdfPTable custTable = new PdfPTable(1);
            custTable.setWidthPercentage(100);


            PdfPCell pdfPCellSig = new PdfPCell(new Phrase("     Scan here", normalFont));
            pdfPCellSig.setBorder(Rectangle.NO_BORDER);
            pdfPCellSig.setHorizontalAlignment(Element.ALIGN_LEFT);
            custTable.addCell(pdfPCellSig);
            document.add(custTable);

            addEmptySpaceLine(1);


        } catch (BadElementException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    private void addSignature(String sign) {
        try {
//

            float[] productColumnsWidth1 = {2, 6};
            PdfPTable sig = new PdfPTable(productColumnsWidth1);
            sig.setWidthPercentage(100);
            Image image = PDFOperations.getInstance().getInvoiceSignatureFromFile(sign);
            if (image != null) {
                image.setBorder(Rectangle.NO_BORDER);
                PdfPCell pdfPCell = new PdfPCell();
                pdfPCell.setBorder(Rectangle.NO_BORDER);
                pdfPCell.setHorizontalAlignment(Element.ALIGN_LEFT);
                pdfPCell.setVerticalAlignment(Element.ALIGN_CENTER);
                pdfPCell.addElement(image);
                sig.addCell(pdfPCell);
                PdfPCell pCell = new PdfPCell();
                pCell.setBorder(Rectangle.NO_BORDER);
                sig.addCell(pCell);
                document.add(sig);
            }

            PdfPTable custTable = new PdfPTable(1);
            custTable.setWidthPercentage(100);


            PdfPCell pdfPCellSig = new PdfPCell(new Phrase("Customer's Signature:", normalFont));
            pdfPCellSig.setBorder(Rectangle.NO_BORDER);
            pdfPCellSig.setHorizontalAlignment(Element.ALIGN_LEFT);
            custTable.addCell(pdfPCellSig);
            document.add(custTable);

            addEmptySpaceLine(1);


        } catch (BadElementException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    private void addFooterToPdf() {
        try {

//            Paragraph signPara = new Paragraph("                            ");
//            document.add(signPara);//user signature
            PdfPTable custTable3 = new PdfPTable(1);
            custTable3.setWidthPercentage(100);
            PdfPCell pdf = new PdfPCell(new Phrase("Delivery Remarks : " + createInvoicePaymentDO.deliveryRemarks, boldFont));

            pdf.setBorder(Rectangle.NO_BORDER);
            pdf.setHorizontalAlignment(Element.ALIGN_LEFT);

            custTable3.addCell(pdf);

            PdfPTable custTable1 = new PdfPTable(1);
            custTable1.setWidthPercentage(100);
            PdfPCell pdf1 = new PdfPCell(new Phrase("Invoice Remarks : " + createInvoicePaymentDO.remarks, boldFont));

            pdf1.setBorder(Rectangle.NO_BORDER);
            pdf1.setHorizontalAlignment(Element.ALIGN_LEFT);

            custTable1.addCell(pdf1);
            document.add(custTable3);
            document.add(custTable1);
            PdfPTable custTable2 = new PdfPTable(1);
            custTable2.setWidthPercentage(100);
            PdfPCell pdfCe = new PdfPCell(new Phrase("this is computer generated invoice no signature or stamp required", boldFont));
//            PdfPCell pdfCe = new PdfPCell(new Phrase("THIS IS COMPUTER GENERATED DOCUMENT AND DOES NOT REQUIRE SIGNATURE", boldFont));

            pdfCe.setBorder(Rectangle.NO_BORDER);
            pdfCe.setHorizontalAlignment(Element.ALIGN_LEFT);

            custTable2.addCell(pdfCe);

            document.add(custTable2);

            addEmptySpaceLine(1);

            document.add(getBorderlessCell("", Element.ALIGN_CENTER));
            CustomerDo customerDo = StorageManager.getInstance(context).getCurrentSpotSalesCustomer(context);
            String shipmentProductsType = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "");
            if (shipmentProductsType.equalsIgnoreCase(AppConstants.FixedQuantityProduct) && customerDo.resFlag == 2) {
                addQsns1();
                addQsns2();
                addQsns3();
                addQsns4();
                addQsns5();
            }
            float regColWidth[] = {2.2f, 7};
            PdfPTable table = new PdfPTable(regColWidth);
            table.setWidthPercentage(100);

            PdfPCell regCell = new PdfPCell(new Phrase("Registered Office:", boldFont));
            regCell.setHorizontalAlignment(Rectangle.LEFT);
            regCell.setBorder(Rectangle.NO_BORDER);

            PdfPCell regCell2 = new PdfPCell(new Phrase(getAddress(false), normalFont));
            regCell2.setHorizontalAlignment(Rectangle.LEFT);
            regCell2.setBorder(Rectangle.NO_BORDER);

            table.addCell(regCell);
            table.addCell(regCell2);
            document.add(table);

            document.add(getBorderlessCell("", Element.ALIGN_CENTER));

            float regAdd[] = {5, 5};
            PdfPTable regAddTable = new PdfPTable(regAdd);
            regAddTable.setWidthPercentage(100);

            PdfPCell regAddCell = new PdfPCell();
            regAddCell.setHorizontalAlignment(Rectangle.LEFT);
            regAddCell.setBorder(Rectangle.NO_BORDER);
            regAddCell.addElement(new Phrase("T: (+971)" + createInvoicePaymentDO.siteMobile, normalFont));
            regAddCell.addElement(new Phrase("Email: " + createInvoicePaymentDO.siteEmail2, normalFont));

            PdfPCell regAddCell2 = new PdfPCell();
            regAddCell2.setHorizontalAlignment(Rectangle.ALIGN_RIGHT);
            regAddCell2.setBorder(Rectangle.NO_BORDER);
            regAddCell2.addElement(new Phrase("F: +(+971)" + createInvoicePaymentDO.siteFax, normalFont));
            regAddCell2.addElement(new Phrase("Website: " + createInvoicePaymentDO.siteWebEmail, normalFont));

            regAddTable.addCell(regAddCell);
            regAddTable.addCell(regAddCell2);

            document.add(regAddTable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void sendPDfewDoc() {
//        String email1 = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.EMAIL, "");
//        String email2 = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.EMAIL2, "");
//        String email3 = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.EMAIL3, "");
//        String email4 = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.EMAIL4, "");
//        String email5 = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.EMAIL5, "");

        SharedPreferences pref = context.getApplicationContext().getSharedPreferences("MyPref", 0);
        String email1 =pref.getString(PreferenceUtils.EMAIL, "");
        String email2 =pref.getString(PreferenceUtils.EMAIL2, "");
        String email3 = pref.getString(PreferenceUtils.EMAIL3, "");
        String email4 =pref.getString(PreferenceUtils.EMAIL4, "");
        String email5 = pref.getString(PreferenceUtils.EMAIL5, "");

        String mainEMail = "";
        if (!email1.isEmpty()) {
            mainEMail = email1+",";
        }
        if (!email2.isEmpty()) {
            mainEMail = mainEMail+email2+",";
        }
        if (!email3.isEmpty()) {
            mainEMail = mainEMail+email3+",";
        }
        if (!email4.isEmpty()) {
            mainEMail = mainEMail+email4+",";
        }
        if (!email5.isEmpty()) {
            mainEMail = mainEMail+email5;
        }
        if (mainEMail.length() > 0) {

            PDFOperations.getInstance().sendpdfMail(context,
//                    activeDeliveryMainDO.deliveryEmail,
                    mainEMail,

                    createInvoicePaymentDO.customerDescription,
                    PDFConstants.BG_INVOICE_PDF_NAME, PDFConstants.BG_INVOCE_NAME);
        } else {
            Toast.makeText(context, "Please provide email address", Toast.LENGTH_SHORT);
        }


//        if (createInvoicePaymentDO.invoiceEmail.length() > 0) {
//            PDFOperations.getInstance().sendpdfMail(context,
//                    createInvoicePaymentDO.invoiceEmail,
//                    createInvoicePaymentDO.customerDescription,
//                    PDFConstants.BG_INVOICE_PDF_NAME, PDFConstants.BG_INVOCE_NAME);
//        } else {
//            Toast.makeText(context, "Please provide email address", Toast.LENGTH_SHORT).show();
//        }


    }

    private PdfPCell getBorderlessCell(String elementName, int alignment) {
        PdfPCell cell = new PdfPCell(new Phrase(elementName, normalFont));
        cell.setHorizontalAlignment(alignment);
        cell.setBorder(Rectangle.NO_BORDER);
        return cell;
    }

    private String getAddress(boolean isCustomer) {

        String street, landMark, town, postal, city, countryName;
        if (isCustomer) {
            street = createInvoicePaymentDO.customerStreet;
            landMark = createInvoicePaymentDO.customerLandMark;
            town = createInvoicePaymentDO.customerTown;
            postal = createInvoicePaymentDO.customerPostalCode;
            city = createInvoicePaymentDO.customerCity;
            countryName = createInvoicePaymentDO.countryname;
        } else {
            street = createInvoicePaymentDO.siteAddress1;
            landMark = createInvoicePaymentDO.siteAddress2;
            town = createInvoicePaymentDO.siteAddress3;
            city = createInvoicePaymentDO.siteCity;
            postal = "";
            countryName = createInvoicePaymentDO.siteCountry;
        }
        String finalString = "";

        if (!TextUtils.isEmpty(street)) {
            finalString += street + ", ";
        }
        if (!TextUtils.isEmpty(landMark)) {
            finalString += landMark + ", ";
        }
        if (!TextUtils.isEmpty(town)) {
            finalString += town + ", ";
        }
        if (!TextUtils.isEmpty(city)) {
            finalString += city + ", ";
        }
        if (!TextUtils.isEmpty(postal)) {
            finalString += postal + ", ";
        }
//        if (!TextUtils.isEmpty(countryName)) {
//            finalString += countryName;
//        }

        return finalString;
    }

    public Image getQRCODE() {

        try {

            try {
                Bitmap qrCode = Util.encodeAsBitmap(context, createInvoicePaymentDO.invoiceNumber);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                qrCode = getResizedBitmap(qrCode, 200);

                qrCode.compress(Bitmap.CompressFormat.PNG, 30, stream);
                return Image.getInstance(stream.toByteArray());
            } catch (WriterException e) {
                e.printStackTrace();
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (BadElementException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Bitmap getResizedBitmap(Bitmap image, int size) {

        return Bitmap.createScaledBitmap(image, 250, 250, true);
    }

}
