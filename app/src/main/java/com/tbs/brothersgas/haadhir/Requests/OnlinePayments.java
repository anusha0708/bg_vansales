package com.tbs.brothersgas.haadhir.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.tbs.brothersgas.haadhir.Model.SuccessDO;
import com.tbs.brothersgas.haadhir.Model.UnPaidInvoiceDO;
import com.tbs.brothersgas.haadhir.utils.CustomXmlPullParser;
import com.tbs.brothersgas.haadhir.utils.OnlinePullParser;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;



public class OnlinePayments extends AsyncTask<String, Void, Boolean> {

    private SuccessDO successDO;
    private Context mContext;
    private String code, name, email, inputInvoiceObject, invoice;
    ArrayList<UnPaidInvoiceDO> invoices;
    private Double amount;

    public OnlinePayments(Context mContext, String invoic, ArrayList<UnPaidInvoiceDO> invoices, String code, String name, Double amount, String email) {

        this.mContext = mContext;
        this.invoices = invoices;
        this.code = code;
        this.name = name;
        this.amount = amount;
        this.email = email;
        this.invoice = invoic;
    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {

        public void onCompleted(boolean isError, SuccessDO successDO);

    }

    HttpURLConnection urlConnection;

    public boolean runRequest() {
        if (invoices.size() > 0) {
            for (int i = 0; i < invoices.size(); i++) {
                inputInvoiceObject = inputInvoiceObject+"<met:string>" + invoices.get(i) .invoiceId+ "</met:string>\n";
            }
        } else if (!invoice.isEmpty()) {
            inputInvoiceObject = "<met:string>" + invoice + "</met:string>\n";

        } else {
            inputInvoiceObject = "<met:string>" + "" + "</met:string>\n";

        }

//        email = "sarada.chintam@tema-systems.com";
//        code = "U999999";


        String URL = "http://18.141.100.91/MeterReading.asmx";

        String xmlContentPrefix = "<soapenv:Envelope\n" +
                "xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"\n" +
                "xmlns:met=\"http://mdasol.com/MeterReading\">\n" +
                "   <soapenv:Header>\n" +
                "      <met:AuthUser>\n" +
                "         <!--Optional:-->\n" +
                "         <met:Username>Web</met:Username>\n" +
                "         <!--Optional:-->\n" +
                "         <met:Password>5bs2YO!)A8RMEhcS@ADj</met:Password>\n" +
                "      </met:AuthUser>\n" +
                "   </soapenv:Header>\n" +
                "   <soapenv:Body>\n" +
                "      <met:CreateInvoice>\n" +
                "         <!--Optional:-->\n" +
                "         <met:CustomerCode>" + code + "</met:CustomerCode>\n" +
                "         <!--Optional:-->\n" +
                "         <met:CustomerName>" + name + "</met:CustomerName>\n" +
                "         <!--Optional:-->\n" +
                "         <met:Email>" + email + "</met:Email>\n" +
                "         <!--Optional:-->\n" +
                "         <met:InvoiceNumber>" + inputInvoiceObject + "</met:InvoiceNumber>\n" +
                "         <met:Amount>" + amount + "</met:Amount>\n" +
                "      </met:CreateInvoice>\n" +
                "   </soapenv:Body>\n" +
                "</soapenv:Envelope>";
        try {
            java.net.URL url = new URL(URL);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "text/xml");
//            urlConnection.setRequestProperty("SOAPAction", "http://mdasol.com/MeterReading/CreateInvoice");
            urlConnection.setRequestProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode(("Web" + ":" + "5bs2YO!)A8RMEhcS@ADj").getBytes()));
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            OutputStream outputStream = urlConnection.getOutputStream();
            outputStream.write(xmlContentPrefix.getBytes());
            outputStream.flush();
            outputStream.close();
            urlConnection.connect();
            Log.e(getClass().getName(), String.valueOf(urlConnection.getResponseCode()));
            String result = null;
            if (urlConnection.getResponseCode() == 200) {
                BufferedInputStream bis = new BufferedInputStream(urlConnection.getInputStream());
                ByteArrayOutputStream buf = new ByteArrayOutputStream();
                int result2 = bis.read();
                while (result2 != -1) {
                    buf.write((byte) result2);
                    result2 = bis.read();
                }
                result = buf.toString();
            }
            //XML parsing is done in a second class called CustomXmlPullParser.
            //Please check that.

            String resultXml = string2SoapObject(result);

            if (resultXml != null && resultXml.length() > 0) {
                return parseXML(resultXml);
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }

    }

    public String string2SoapObject(String bytes) {
        String resultXML = null;

        try {

            resultXML = OnlinePullParser.parse(bytes);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultXML;
    }

    public boolean parseXML(String xmlString) {
        boolean value=false;
        if(xmlString.equalsIgnoreCase("1")){
            try {
                successDO = new SuccessDO();
                successDO.flag = Integer.parseInt(xmlString);
                return value=true;
            } catch (Exception e) {

                return value=false;
            }
        }

        return value;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        if (onResultListener != null) {
            onResultListener.onCompleted(!result, successDO);
        }
    }
}