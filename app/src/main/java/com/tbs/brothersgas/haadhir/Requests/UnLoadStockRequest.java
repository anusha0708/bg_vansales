package com.tbs.brothersgas.haadhir.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryMainDO;
import com.tbs.brothersgas.haadhir.Model.LoadStockDO;
import com.tbs.brothersgas.haadhir.Model.LoadStockMainDO;
import com.tbs.brothersgas.haadhir.database.StorageManager;
import com.tbs.brothersgas.haadhir.utils.CalendarUtils;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;
import com.tbs.brothersgas.haadhir.utils.WebServiceConstants;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class UnLoadStockRequest extends AsyncTask<String, Void, Boolean> {

    private LoadStockMainDO loanReturnMainDO;
    private LoadStockDO loanReturnDO;
    private Context mContext;
    private String recievedSite,date,customerSite;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;
    LinkedHashMap<String, ArrayList<LoadStockDO>> returnDOs;
    public UnLoadStockRequest(String recieveSite, String datE, String site, LinkedHashMap<String, ArrayList<LoadStockDO>> returndos, Context mContext) {

        this.recievedSite = recieveSite;
        this.date = datE;
        this.customerSite = site;
        this.returnDOs = returndos;

        this.mContext = mContext;


    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        public void onCompleted(boolean isError, LoadStockMainDO loanReturnMainDO);

    }

    public boolean runRequest() {
        // System.out.println("CUSTOMER ID " + customerId);
        String NAMESPACE = "http://www.adonix.com/WSS";
        String METHOD_NAME = "run";
        String SOAP_ACTION = "CAdxWebServiceXmlCC";
//        String URL = "http://183.82.9.23:8124/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC";
        preferenceUtils = new PreferenceUtils(mContext);
        username = preferenceUtils.getStringFromPreference(PreferenceUtils.A_USER_NAME, "");
        password = preferenceUtils.getStringFromPreference(PreferenceUtils.A_PASSWORD, "");
        ip = preferenceUtils.getStringFromPreference(PreferenceUtils.IP_ADDRESS, "");
        port = preferenceUtils.getStringFromPreference(PreferenceUtils.PORT, "");
        pool = preferenceUtils.getStringFromPreference(PreferenceUtils.ALIAS, "");

        String URL = "http://" + ip + ":" + port + "/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC";
        String site  = preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID, "");
        String customer  = preferenceUtils.getStringFromPreference(PreferenceUtils.CUSTOMER, "");
        String code = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_CODE, "");
        String date =CalendarUtils.getDate();
        String nId = preferenceUtils.getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, "");
        String loc = preferenceUtils.getStringFromPreference(PreferenceUtils.LOCATION, "");

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        // Set all input params
        request.addProperty("publicName", WebServiceConstants.UNLOAD_STOCK);
        ActiveDeliveryMainDO activeDeliverySavedDo = StorageManager.getInstance(mContext).getActiveDeliveryMainDo(mContext);

        JSONObject jsonObject = new JSONObject();
        try {

//            jsonObject.put("I_YBPC", activeDeliverySavedDo.customer);
//            jsonObject.put("I_YFCY", site);
            jsonObject.put("XSRC", nId);
            jsonObject.put("XSFCY", site);
            jsonObject.put("XSDAT", date);
            jsonObject.put("XSLOCD", loc);
            jsonObject.put("XSLOC", code);

            JSONArray jsonArray =new JSONArray();
            ArrayList<String> keySet = new ArrayList<String>(returnDOs.keySet());
            for (int q=0; q<returnDOs.size(); q++) {

                ArrayList<LoadStockDO> loanReturnDos = returnDOs.get(keySet.get(q));
                if(loanReturnDos!=null && loanReturnDos.size()>0) {
                    for (int i = 0; i < loanReturnDos.size(); i++) {
                        JSONObject jsonObject1 = new JSONObject();
                        jsonObject1.put("XSITM", loanReturnDos.get(i).product);
                        jsonObject1.put("XSQTY",  loanReturnDos.get(i).quantity);


                 /*   jsonArray.put("I_YQTY",createDeliveryDoS.get(i).totalQuantity);
                    jsonArray.put("I_YITMREF",createDeliveryDoS.get(i).item);*/
                        jsonArray.put(i, jsonObject1);
                    }
                    jsonObject.put("GRP2", jsonArray);
                }

            }


        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        request.addProperty("inputXml", jsonObject.toString());

        SoapObject callcontext = new SoapObject("", "callContext");
        // Set all input params
        callcontext.addProperty("codeLang", "ENG");
        callcontext.addProperty("poolAlias", pool);
        callcontext.addProperty("poolId", "");
        callcontext.addProperty("codeUser", username);
        callcontext.addProperty("password", password);
        callcontext.addProperty("requestConfig", "adxwss.trace.on=off");

        request.addSoapObject(callcontext);


        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.setOutputSoapObject(request);

        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
        androidHttpTransport.debug = true;

        try {
            List<HeaderProperty> headerList = new ArrayList<HeaderProperty>();
            headerList.add(new HeaderProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode((username + ":" + password).getBytes())));


            androidHttpTransport.call(SOAP_ACTION, envelope, headerList);


            SoapObject response = (SoapObject) envelope.getResponse();
            String resultXML = (String) response.getProperty("resultXml");
            if (resultXML != null && resultXML.length() > 0) {
                return parseXML(resultXML);
            } else {
                return false;
            }
        } catch (SocketTimeoutException e) {
            e.printStackTrace();

            return false;
        } catch (Exception e) {

            e.printStackTrace();
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            loanReturnMainDO = new LoadStockMainDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        loanReturnMainDO.loadStockDOS = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        loanReturnDO = new LoadStockDO();
                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {
                        if (attribute.equalsIgnoreCase("XSTATUS")) {
                            if(text.length()>0){
                                loanReturnMainDO.status = Integer.parseInt(text);

                            }



                        }

                        text="";

                    }

                    if (endTag.equalsIgnoreCase("GRP")) {
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        loanReturnMainDO.loadStockDOS.add(loanReturnDO);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        if (onResultListener != null) {
            onResultListener.onCompleted(!result, loanReturnMainDO);
        }
    }
}