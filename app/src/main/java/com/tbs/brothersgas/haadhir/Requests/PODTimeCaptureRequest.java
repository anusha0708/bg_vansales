package com.tbs.brothersgas.haadhir.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.brothersgas.haadhir.Activitys.BaseActivity;
import com.tbs.brothersgas.haadhir.Model.SuccessDO;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;
import com.tbs.brothersgas.haadhir.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class PODTimeCaptureRequest extends AsyncTask<String, Void, Boolean> {

    private SuccessDO successDO;
    private Context mContext;
    private String userId, currentDate, documentType, documentNumber, arrivalDate, arrivalTime, startUnloadingDate, startUnloadingTime, cdDate,
            cdTime, endUnloadingDate, endUnloadingTime, validateDeliveryDate, validateDeliveryTime, confirmDepartureDate, confirmDepartureTime;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;
    private int flaG;

    public PODTimeCaptureRequest(String userID, String date, String documentTypE, String documentNumbeR, String arrivalDatE, String arrivalTimE,
                                 String startUnloadingDatE, String startUnloadingTimE, String cdDatE, String cdTimE,
                                 String endUnloadingDatE, String endUnloadingTimE, String validateDeliveryDatE,
                                 String validateDeliveryTimE, String confirmDepartureDatE, String confirmDepartureTimE, Context mContext) {

        this.mContext = mContext;
        this.userId = userID;
        this.currentDate = date;

        this.documentType = documentTypE;
        this.documentNumber = documentNumbeR;
        this.arrivalDate = arrivalDatE;
        this.arrivalTime = arrivalTimE;
        this.startUnloadingDate = startUnloadingDatE;
        this.startUnloadingTime = startUnloadingTimE;
        this.cdDate = cdDatE;
        this.cdTime = cdTimE;
        this.endUnloadingDate = endUnloadingDatE;
        this.endUnloadingTime = endUnloadingTimE;
        this.validateDeliveryDate = validateDeliveryDatE;
        this.validateDeliveryTime = validateDeliveryTimE;
        this.confirmDepartureDate = confirmDepartureDatE;
        this.confirmDepartureTime = confirmDepartureTimE;

    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        public void onCompleted(boolean isError, SuccessDO createInvoiceDO);

    }

    public boolean runRequest() {
        // System.out.println("CUSTOMER ID " + customerId);
        String NAMESPACE = "http://www.adonix.com/WSS";
        String METHOD_NAME = "run";
        String SOAP_ACTION = "CAdxWebServiceXmlCC";
        preferenceUtils = new PreferenceUtils(mContext);
        username = preferenceUtils.getStringFromPreference(PreferenceUtils.A_USER_NAME, "");
        password = preferenceUtils.getStringFromPreference(PreferenceUtils.A_PASSWORD, "");
        ip = preferenceUtils.getStringFromPreference(PreferenceUtils.IP_ADDRESS, "");
        port = preferenceUtils.getStringFromPreference(PreferenceUtils.PORT, "");
        pool = preferenceUtils.getStringFromPreference(PreferenceUtils.ALIAS, "");
        String URL = "http://" + ip + ":" + port + "/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC";
        String id = preferenceUtils.getStringFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID, "");

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        // Set all input params                         X10CSAINV3
        request.addProperty("publicName", WebServiceConstants.POD_TIME_CAPTURE);
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("I_X10CUSRID", userId);
            jsonObject.put("I_XDATE", currentDate);
            jsonObject.put("I_XSDHNUM", documentNumber);
            jsonObject.put("I_XPODDOCTYP", documentType);
            jsonObject.put("I_XPODDOCNUM", id);
            jsonObject.put("I_XPODCONARRDAT", arrivalDate);
            jsonObject.put("I_XPODCONARRTIM", arrivalTime);
            jsonObject.put("I_XPODSTRUNLODDAT", startUnloadingTime);
            jsonObject.put("I_XPODSTRUNLODTIM", startUnloadingDate);
            jsonObject.put("I_XPODCAPDELDAT", cdTime);
            jsonObject.put("I_XPODCAPDELTIM", cdDate);
            jsonObject.put("I_XPODENDUNLODDAT", endUnloadingTime);
            jsonObject.put("I_XPODENDUNLODTIM", endUnloadingDate);
            jsonObject.put("I_XPODVLDDELDAT", validateDeliveryTime);
            jsonObject.put("I_XPODVLDDELTIM", validateDeliveryDate);
            jsonObject.put("I_XPODCNFMDEPDAT", confirmDepartureTime);
            jsonObject.put("I_XPODCNFMDEPTIM", confirmDepartureDate);
        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        request.addProperty("inputXml", jsonObject.toString());

        SoapObject callcontext = new SoapObject("", "callContext");
        // Set all input params
        callcontext.addProperty("codeLang", "ENG");
        callcontext.addProperty("poolAlias", pool);
        callcontext.addProperty("poolId", "");
        callcontext.addProperty("codeUser", username);
        callcontext.addProperty("password", password);
        callcontext.addProperty("requestConfig", "adxwss.trace.on=off");

        request.addSoapObject(callcontext);


        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.setOutputSoapObject(request);

        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
        androidHttpTransport.debug = true;

        try {
            List<HeaderProperty> headerList = new ArrayList<HeaderProperty>();
            headerList.add(new HeaderProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode((username + ":" + password).getBytes())));


            androidHttpTransport.call(SOAP_ACTION, envelope, headerList);


            SoapObject response = (SoapObject) envelope.getResponse();
            String resultXML = (String) response.getProperty("resultXml");
            if (resultXML != null && resultXML.length() > 0) {
                return parseXML(resultXML);
            } else {
                return false;
            }
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            successDO = new SuccessDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        //    createPaymentDO.customerDetailsDos = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        //      createPaymentDO = new CustomerDetailsDo();

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {


                        if (attribute.equalsIgnoreCase("O_XFLG")) {
                            if (text.length() > 0) {

                                successDO.flag = Integer.parseInt(text);
                            }


                        }
                    }


                    if (endTag.equalsIgnoreCase("GRP")) {
                        // customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        // customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        if (onResultListener != null) {
            onResultListener.onCompleted(!result, successDO);
        }
    }
}