package com.tbs.brothersgas.haadhir.history

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context
import androidx.core.content.res.ResourcesCompat
import androidx.appcompat.app.AppCompatDialog
import androidx.recyclerview.widget.RecyclerView
import android.text.Editable
import android.text.InputFilter
import android.text.Spanned
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*

import com.tbs.brothersgas.haadhir.Activitys.BaseActivity
import com.tbs.brothersgas.haadhir.Activitys.ScheduledCaptureDeliveryActivity
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryDO
import com.tbs.brothersgas.haadhir.Model.ReasonDO
import com.tbs.brothersgas.haadhir.Model.ReasonMainDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.ActiveDeliveryRequest
import com.tbs.brothersgas.haadhir.common.AppConstants
import com.tbs.brothersgas.haadhir.listeners.LoadStockListener
import com.tbs.brothersgas.haadhir.listeners.ResultListner
import com.tbs.brothersgas.haadhir.pdfs.BulkDeliveryNotePDF
import com.tbs.brothersgas.haadhir.pdfs.CYLDeliveryNotePDF
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils

import java.util.ArrayList

class DeliverysAdapter(private val context: Context, var activeDeliveryDOS: ArrayList<ActiveDeliveryDO>?) : androidx.recyclerview.widget.RecyclerView.Adapter<DeliverysAdapter.MyViewHolder>() {

    internal var mass = 0.0
    internal var type = 0
    var fromDeliveryExecutive: Boolean = false
    var resultListner: ResultListner? = null


    private fun prepareDeliveryNoteCreation(id: String) {

        if (id.length > 0) {
            val driverListRequest = ActiveDeliveryRequest(id, context)
            driverListRequest.setOnResultListener { isError, activeDeliveryDo ->

                if (activeDeliveryDo != null) {
                    if (isError) {

                    } else {
                       if(activeDeliveryDo.productType==20){
                           BulkDeliveryNotePDF(context).createDeliveryPDF(activeDeliveryDo, "HISTORY").CreateHistoryPDF().execute()

                       }else{
                           CYLDeliveryNotePDF(context).createDeliveryPDF(activeDeliveryDo, "HISTORY").CreateHistoryPDF().execute()

                       }

                    }
                } else {
                }
            }
            driverListRequest.execute()
        }
    }

    fun refreshAdapter(activeDeliveryDOS: ArrayList<ActiveDeliveryDO>, Type: Int) {
        this.activeDeliveryDOS = activeDeliveryDOS
        this.type = Type
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.delivery_data, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val activeDeliveryDO = activeDeliveryDOS!![position]
        if (activeDeliveryDO.date.length > 0) {
            val aMonth = activeDeliveryDO.date.substring(4, 6)
            val ayear = activeDeliveryDO.date.substring(0, 4)
            val aDate = activeDeliveryDO.date.substring(Math.max(activeDeliveryDO.date.length - 2, 0))
            holder.tvShipmentId.text = "" + activeDeliveryDO.shipmentId
            holder.tvSite.text = "" + activeDeliveryDO.site
            holder.tvDeliveryDate.text = "" + aDate + "-" + aMonth + "-" + ayear
            holder.tvCustomer.text = "" + activeDeliveryDO.customer + " - " + activeDeliveryDO.customerDes
            holder.tvAddress.text = "" + activeDeliveryDO.address1 + " , " + activeDeliveryDO.address2 + " , " + activeDeliveryDO.address3

        }


        holder.itemView.setOnClickListener {
            if (!fromDeliveryExecutive) {
                prepareDeliveryNoteCreation(activeDeliveryDO.shipmentId)
            } else {
                resultListner?.onResultListner(activeDeliveryDO, true)
            }
        }


    }

    override fun getItemCount(): Int {
        return if (activeDeliveryDOS != null) activeDeliveryDOS!!.size else 0
    }

    fun deliveryExexutive(fromDeliveryExecutive: Boolean, resultListner: ResultListner) {
        this.fromDeliveryExecutive = fromDeliveryExecutive
        this.resultListner = resultListner

    }


    inner class MyViewHolder(view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
        var tvShipmentId: TextView
        var tvSite: TextView
        var tvDeliveryDate: TextView
        var tvCustomer: TextView
        var tvAddress: TextView


        init {
            tvShipmentId = view.findViewById<View>(R.id.tvShipmentId) as TextView
            tvSite = view.findViewById<View>(R.id.tvSite) as TextView
            tvDeliveryDate = view.findViewById<View>(R.id.tvDeliveryDate) as TextView
            tvCustomer = view.findViewById<View>(R.id.tvCustomer) as TextView
            tvAddress = view.findViewById<View>(R.id.tvAddress) as TextView


        }
    }


}
