package com.tbs.brothersgas.haadhir.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.tbs.brothersgas.haadhir.Activitys.BaseActivity;
import com.tbs.brothersgas.haadhir.Model.SuccessDO;
import com.tbs.brothersgas.haadhir.utils.CalendarUtils;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;
import com.tbs.brothersgas.haadhir.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CheckinTimeCaptureRequest extends AsyncTask<String, Void, Boolean> {
     long minuts=0;
    private SuccessDO successDO;
    private Context mContext;
    private String userID, currentDate, id, arrivalDate, arrivalTime, startLoadingTime, startLoadingDate, endloadingTime, endLoadingDate, vehicleInspectionDate, vehicleInspectionTime, gateInspectionDate, gateInspectionTime, checkinDate, checkinTime;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;
    private int flaG;

    public CheckinTimeCaptureRequest(String userId, String currentdate, String iD, String arrivaldate, String arrivaltime,
                                     String startloadingtime, String startloadingdate,
                                     String endloadingtime, String endloadingdate,
                                     String vehicleinspectiontime, String vehicleinspectiondate,
                                     String gateinspectiontime, String gateinspectiondate,
                                     String checkindate, String checkintime, Context mContext) {

        this.mContext = mContext;
        this.userID = userId;
        this.id = iD;

        this.currentDate = currentdate;
        this.arrivalTime = arrivaltime;
        this.arrivalDate = arrivaldate;
        this.startLoadingTime = startloadingdate;
        this.startLoadingDate = startloadingtime;
        this.endloadingTime = endloadingtime;
        this.endLoadingDate = endloadingdate;
        this.vehicleInspectionDate = vehicleinspectiondate;
        this.vehicleInspectionTime = vehicleinspectiontime;
        this.gateInspectionDate = gateinspectiondate;
        this.gateInspectionTime = gateinspectiontime;
        this.checkinDate = checkindate;
        this.checkinTime = checkintime;


    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        public void onCompleted(boolean isError, SuccessDO createInvoiceDO);

    }

    public boolean runRequest() {
        // System.out.println("CUSTOMER ID " + customerId);
        String NAMESPACE = "http://www.adonix.com/WSS";
        String METHOD_NAME = "run";
        String SOAP_ACTION = "CAdxWebServiceXmlCC";
        preferenceUtils = new PreferenceUtils(mContext);
        username = preferenceUtils.getStringFromPreference(PreferenceUtils.A_USER_NAME, "");
        password = preferenceUtils.getStringFromPreference(PreferenceUtils.A_PASSWORD, "");
        ip = preferenceUtils.getStringFromPreference(PreferenceUtils.IP_ADDRESS, "");
        port = preferenceUtils.getStringFromPreference(PreferenceUtils.PORT, "");
        pool = preferenceUtils.getStringFromPreference(PreferenceUtils.ALIAS, "");
        String URL = "http://" + ip + ":" + port + "/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC";
        String doc_number = preferenceUtils.getStringFromPreference(PreferenceUtils.DOC_NUMBER, "");

        SimpleDateFormat smf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        String depatureDate = preferenceUtils.getStringFromPreference(PreferenceUtils.CD_DATE, "");
        String departureTime = preferenceUtils.getStringFromPreference(PreferenceUtils.CD_TIME, "");
        /*String[] time = departureTime.split(":");
        String hour = time[0];
        String minute = time[1];*/

        try {
            Date finaldepDate = smf.parse(depatureDate +" "+ departureTime);
            Date finalCheckDate = smf.parse(CalendarUtils.getCurrentDate());

            long date1 = finaldepDate.getTime();
            long date2 = finalCheckDate.getTime();
            long diff;
            if (date1 > date2) {

                diff = date1 - date2;
            } else {
                diff = date2 - date1;
            }
            long seconds = diff / 1000;
            minuts = seconds / 60;
            long hours = minuts / 60;
            long days = hours / 24;

            Log.d("Seconds-->", seconds + "-->minutes" + minuts + "-->hours->" + hours + "-->days->" + days);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        // Set all input params                         X10CSAINV3
        request.addProperty("publicName", WebServiceConstants.CHECKIN_TIME_CAPTURE);
        JSONObject jsonObject = new JSONObject();
        try {
//
            jsonObject.put("I_XSRCNUM", doc_number);
            jsonObject.put("I_X10CUSRID", userID);
            jsonObject.put("I_XDATE", currentDate);
            jsonObject.put("I_XDOCNUM", id);
            jsonObject.put("I_XDSCONARRDAT", arrivalDate);
            jsonObject.put("I_XDSCONARRTIM", arrivalTime);
            jsonObject.put("I_XDSSTRLODDAT", startLoadingTime);
            jsonObject.put("I_XDSSTRLODTIM", startLoadingDate);
            jsonObject.put("I_XDSENDLODDAT", endLoadingDate);
            jsonObject.put("I_XDSENDLODTIM", endloadingTime);
            jsonObject.put("I_XDSVEHINSPDAT", vehicleInspectionDate);
            jsonObject.put("I_XDSVEHINSPTIM", vehicleInspectionTime);
            jsonObject.put("I_XDSGATINSPDAT", gateInspectionDate);
            jsonObject.put("I_XDSGATINSPTIM", gateInspectionTime);
            jsonObject.put("I_XDSCHKINDAT", checkinTime);
            jsonObject.put("I_XDSCHKINTIM", checkinDate);
            jsonObject.put("O_XTIM_DIFF", ""+minuts);


        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        request.addProperty("inputXml", jsonObject.toString());

        SoapObject callcontext = new SoapObject("", "callContext");
        // Set all input params
        callcontext.addProperty("codeLang", "ENG");
        callcontext.addProperty("poolAlias", pool);
        callcontext.addProperty("poolId", "");
        callcontext.addProperty("codeUser", username);
        callcontext.addProperty("password", password);
        callcontext.addProperty("requestConfig", "adxwss.trace.on=off");

        request.addSoapObject(callcontext);


        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.setOutputSoapObject(request);

        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
        androidHttpTransport.debug = true;

        try {
            List<HeaderProperty> headerList = new ArrayList<HeaderProperty>();
            headerList.add(new HeaderProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode((username + ":" + password).getBytes())));


            androidHttpTransport.call(SOAP_ACTION, envelope, headerList);


            SoapObject response = (SoapObject) envelope.getResponse();
            String resultXML = (String) response.getProperty("resultXml");
            if (resultXML != null && resultXML.length() > 0) {
                return parseXML(resultXML);
            } else {
                return false;
            }
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            successDO = new SuccessDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        //    createPaymentDO.customerDetailsDos = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        //      createPaymentDO = new CustomerDetailsDo();

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {


                        if (attribute.equalsIgnoreCase("O_XFLG")) {
                            if (text.length() > 0) {

                                successDO.flag = Integer.parseInt(text);
                            }


                        }
                    }


                    if (endTag.equalsIgnoreCase("GRP")) {
                        // customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        // customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
//        ((BaseActivity) mContext).showLoader();

        // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

//        ((BaseActivity) mContext).hideLoader();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, successDO);
        }
    }


}