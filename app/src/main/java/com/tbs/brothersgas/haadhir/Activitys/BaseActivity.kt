package com.tbs.brothersgas.haadhir.Activitys

import android.Manifest
import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Typeface
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.provider.MediaStore
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.util.Patterns
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.webkit.PermissionRequest
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDialog
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.single.PermissionListener
import com.tbs.brothersgas.haadhir.ActionBarDrawerToggle
import com.tbs.brothersgas.haadhir.Adapters.ExpandableListAdapter
import com.tbs.brothersgas.haadhir.DrawerArrowDrawable
import com.tbs.brothersgas.haadhir.Model.CustomerDo
import com.tbs.brothersgas.haadhir.Model.ReasonDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.LogoutTimeCaptureRequest
import com.tbs.brothersgas.haadhir.collector.TransactionListActivity
import com.tbs.brothersgas.haadhir.common.AppConstants
import com.tbs.brothersgas.haadhir.common.FireBaseOperations
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.database.TBSDatabaseHelper.DATABASE_NAME
import com.tbs.brothersgas.haadhir.listeners.ResultListner
import com.tbs.brothersgas.haadhir.listeners.SingleSelectedItemListener
import com.tbs.brothersgas.haadhir.receivers.DateChangedReceiver
import com.tbs.brothersgas.haadhir.utils.*
import java.io.File
import java.text.DateFormat
import java.text.DecimalFormat
import java.util.*


abstract class BaseActivity : AppCompatActivity() {
    private lateinit var myReceiver: DateChangedReceiver
    public val childList = HashMap<MenuModel, List<MenuModel>>()
    public val headerList = ArrayList<MenuModel>()
    private var expandableListAdapter: ExpandableListAdapter? = null
    private var expandableListView: ExpandableListView? = null
    lateinit var ivMenu: ImageView
    lateinit var ivCustomerLocation: ImageView
    lateinit var inflater: LayoutInflater
    private var spinAdapter: MySpinnerAdapter? = null

    var lattitudeFused: String? = null
    var longitudeFused: String? = null
    var addressFused: String? = null

    lateinit var ivAdd: ImageView
    private var lastExpandedPosition = -1
    lateinit var toolbar: Toolbar
    lateinit var llBody: LinearLayout
    var localCustomerDOS: ArrayList<CustomerDo> = ArrayList<CustomerDo>()
    var llFooter: LinearLayout? = null
    var calendar = Calendar.getInstance()
    var display: TextView? = null
    var tvQsnDate: TextView? = null


    var mDay: Int = 0
    var mMonth: Int = 0
    var mYear: Int = 0

    private lateinit var dialogLoader: Dialog
    private var number = ""
    lateinit var ivRefresh: ImageView

    lateinit var ivDone: ImageView
    private val TAG = BaseActivity::class.java!!.getSimpleName()

    lateinit var mDrawerLayout: DrawerLayout
    private val mDrawerList: ListView? = null
    private var context: Context? = null
    private val userId: String? = null
    private var orderCode: String? = null
    lateinit var flToolbar: FrameLayout
    private var mDrawerToggle: ActionBarDrawerToggle? = null
    private var drawerArrow: DrawerArrowDrawable? = null


    lateinit var preferenceUtils: PreferenceUtils
    lateinit var ivSearch: ImageView
    lateinit var svSearch: SearchView

    public var ivPic: ImageView? = null
    lateinit var rlAdd: RelativeLayout

    //    lateinit   var ivBack: ImageView
    lateinit var tvScreenTitle: TextView
    lateinit var tvScreenTitleTop: TextView
    lateinit var tvScreenTitleBottom: TextView
    lateinit var tvDate: TextView
    lateinit var tvDriverCode: TextView
    lateinit var tvDriverName: TextView
    lateinit var tvRoutingId: TextView
    lateinit var tvCompanyCode: TextView
    lateinit var tvCompanyName: TextView
    lateinit var ivProfileArrow: ImageView
    lateinit var ivSelectAll: ImageView
    lateinit var ivViewAll: Button
//    lateinit var ivShowAll: Button

    lateinit var tvShipments: TextView
    lateinit var tvVehicleCode: TextView
    lateinit var tvPlate: TextView
    lateinit var tvADT: TextView
    lateinit var tvDDT: TextView
    lateinit var tvStatus: TextView
    lateinit var llData: LinearLayout
    lateinit var llProfile: LinearLayout
    lateinit var llStatus: LinearLayout
    lateinit var llUserProfile: LinearLayout

    private var mLastUpdateTime: String? = null

    // location updates interval - 10sec
    private val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 10000

    // fastest updates interval - 5 sec
    // location updates will be received if another app is requesting the locations
    // than your app can handle
    private val FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS: Long = 5000

    private val REQUEST_CHECK_SETTINGS = 100


    // bunch of location related apis
    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private var mSettingsClient: SettingsClient? = null
    private var mLocationRequest: LocationRequest? = null
    private var mLocationSettingsRequest: LocationSettingsRequest? = null
    private var mLocationCallback: LocationCallback? = null
    private var mCurrentLocation: Location? = null

    // boolean flag to toggle the ui
    private var mRequestingLocationUpdates: Boolean? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.base_layout)
        context = this@BaseActivity
//        dialogLoader= Dialog(this@BaseActivity)
        //        inflater = this.getLayoutInflater();
        inflater = layoutInflater

        baseInitializeControls()

        preferenceUtils = PreferenceUtils(context)
        var id = preferenceUtils.getStringFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID, "")
        if (id.length > 0) {
            tvVehicleCode.setVisibility(View.VISIBLE)
        }

        setSupportActionBar(toolbar)
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayShowHomeEnabled(true)
            supportActionBar!!.setDisplayHomeAsUpEnabled(false)
            supportActionBar!!.setHomeButtonEnabled(true)
            supportActionBar!!.setDisplayShowTitleEnabled(false)
        }

        tvScreenTitleTop.setText("" + preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_CHECKED_ROUTE_ID, ""))
        tvScreenTitleBottom.setText("" + preferenceUtils.getStringFromPreference(PreferenceUtils.USER_NAME, ""))

        //        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //        getSupportActionBar().setHomeButtonEnabled(true);
        val data = preferenceUtils.getStringFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID, "")

        if (data.length > 0) {
            tvRoutingId.setVisibility(View.VISIBLE)
            llStatus.visibility = View.VISIBLE
            ivProfileArrow.visibility = View.VISIBLE
//            llData.setVisibility(View.VISIBLE)
//            tvDate.setVisibility(View.VISIBLE)

        } else {
            tvRoutingId.setVisibility(View.GONE)
            tvDate.setVisibility(View.GONE)
            llData.setVisibility(View.GONE)
            ivProfileArrow.visibility = View.GONE

        }
        val siteId = preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_NAME, "")
        val vehicleCheckInDo = StorageManager.getInstance(this@BaseActivity).getVehicleCheckInData(this@BaseActivity);
        if (vehicleCheckInDo.scheduledRootId!!.isEmpty()) {
            tvDate.setVisibility(View.GONE)
            tvRoutingId.setVisibility(View.GONE)
        } else {
            tvDate.setVisibility(View.VISIBLE)
            tvRoutingId.setVisibility(View.VISIBLE)
        }

        tvDate.setText("" + resources.getString(R.string.base_date) + " " + preferenceUtils.getStringFromPreference(PreferenceUtils.CHECKIN_DATE, ""))
        tvRoutingId.setText("" + resources.getString(R.string.base_vr_id) + " " + preferenceUtils.getStringFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID, ""))
        tvDriverCode.setText("" + resources.getString(R.string.base_driver) + " " + preferenceUtils.getStringFromPreference(PreferenceUtils.LOGIN_DRIVER_NAME, ""))
        tvVehicleCode.setText("" + resources.getString(R.string.base_vcode) + " " + preferenceUtils.getStringFromPreference(PreferenceUtils.CVEHICLE_CODE, ""))
        tvPlate.setText("" + resources.getString(R.string.base_name_plate) + " " + preferenceUtils.getStringFromPreference(PreferenceUtils.CV_PLATE, ""))
        tvShipments.setText("" + resources.getString(R.string.base_shipmnts) + " " + preferenceUtils.getStringFromPreference(PreferenceUtils.CN_SHIPMENTS, ""))
        tvADT.setText("" + resources.getString(R.string.base_eta) + " " + preferenceUtils.getStringFromPreference(PreferenceUtils.CA_DATE, "") + "  " + preferenceUtils.getStringFromPreference(PreferenceUtils.CA_TIME, ""))
        tvDDT.setText("" + resources.getString(R.string.base_etd) + " " + preferenceUtils.getStringFromPreference(PreferenceUtils.CD_DATE, "") + "  " + preferenceUtils.getStringFromPreference(PreferenceUtils.CD_TIME, ""))
//        tvStatus.setText("" + preferenceUtils.getStringFromPreference(PreferenceUtils.CHECK_IN_STATUS, "") )
        if (vehicleCheckInDo.checkInStatus.equals("", true)) {
            if (!vehicleCheckInDo.loadStock.equals("")) {
                tvStatus.setText(vehicleCheckInDo.loadStock)
            } else {
                tvStatus.setText("Logged in")
            }
        } else {
            tvStatus.setText(vehicleCheckInDo.checkInStatus)
        }
        tvCompanyCode.setText("" + resources.getString(R.string.company_name))
        tvCompanyName.setText("")
        if (siteId.length > 0) {
            tvDriverName.setVisibility(View.VISIBLE)
            tvDriverName.setText("Site : " + siteId)
        } else {
            tvDriverName.setVisibility(View.GONE)
        }

        val image = preferenceUtils.getStringFromPreference(PreferenceUtils.PROFILE_PICTURE, "")
        if (image.isNotEmpty()) {
            val decodedString = android.util.Base64.decode(image, android.util.Base64.DEFAULT);
            val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)

            ivPic?.setImageBitmap(decodedByte)
        } else {
            //  ivPic?.setImageDrawable(R.drawable.usericon)
        }
//        drawerArrow = DrawerArrowDrawable(this) {
//            val isLayoutRtl: Boolean
//                get() = false
//        }

        drawerArrow = object : DrawerArrowDrawable(this) {
            override fun isLayoutRtl(): Boolean {
                return false
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

        }
        toolbar.navigationIcon = drawerArrow
        mDrawerToggle = object : ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close, drawerArrow) {

            override fun onDrawerOpened(drawerView: View) {
                invalidateOptionsMenu()
                super.onDrawerOpened(drawerView)
            }

            override fun onDrawerClosed(drawerView: View) {
                this@BaseActivity.expandableListView!!.collapseGroup(this@BaseActivity.lastExpandedPosition)
                invalidateOptionsMenu()
                super.onDrawerClosed(drawerView)
            }

            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                super.onDrawerSlide(drawerView, slideOffset)
            }

            override fun onDrawerStateChanged(newState: Int) {
                super.onDrawerStateChanged(newState)
            }
        }
        mDrawerLayout.addDrawerListener(mDrawerToggle!!)
        mDrawerToggle!!.syncState()
        toolbar.setNavigationOnClickListener {
            menuOperates()
        }
        //

        initialize()
        setTypeFace(AppConstants.MONTSERRAT_REGULAR_TYPE_FACE, llBody)
    }
    /*} catch (JSONException e) {
            e.printStackTrace();
        }*/

    fun showLoader() {
        runOnUiThread(RunShowLoader("Loading..."))
    }

    //Method to show loader with text
    fun showLoader(msg: String) {
        runOnUiThread(RunShowLoader(msg))
    }

    fun showToast(strMsg: String) {
        if (!strMsg.isEmpty()) {
            Toast.makeText(this@BaseActivity, strMsg, Toast.LENGTH_SHORT).show()
        }
    }

    fun showAlert(strMsg: String) {
        if (!strMsg.isEmpty()) {
            showAppCompatAlert("Alert!", strMsg, "OK", "", "", true)
        }
    }

    fun showSnackbar(errorMsg: String) {

        val snackbar = Snackbar.make(llBody, errorMsg, Snackbar.LENGTH_SHORT)
        snackbar.show()

    }

    var alertDialog: AlertDialog.Builder? = null
    fun showAppCompatAlert(mTitle: String, mMessage: String, posButton: String, negButton: String?, from: String, isCancelable: Boolean) {
        try {
            if (alertDialog == null)
                alertDialog = AlertDialog.Builder(this@BaseActivity, R.style.AppCompatAlertDialogStyle)
            alertDialog!!.setTitle(mTitle)
            alertDialog!!.setMessage(mMessage)
            number = mMessage
            alertDialog!!.setPositiveButton(posButton) { dialog, which -> onButtonYesClick(from) }
            if (negButton != null && !negButton.equals("", ignoreCase = true)) {
                alertDialog!!.setNegativeButton(negButton) { dialog, which -> onButtonNoClick(from) }
                alertDialog!!.setCancelable(false)
            } else {
            }
            alertDialog!!.show()
        } catch (e: java.lang.Exception) {

        }

    }

    fun CalculationByDistance(StartP: LatLng, EndP: LatLng): Double {
        val Radius = 6371// radius of earth in Km
        val lat1 = StartP.latitude
        val lat2 = EndP.latitude
        val lon1 = StartP.longitude
        val lon2 = EndP.longitude
        val dLat = Math.toRadians(lat2 - lat1)
        val dLon = Math.toRadians(lon2 - lon1)
        val a = (Math.sin(dLat / 2) * Math.sin(dLat / 2) + (Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2)
                * Math.sin(dLon / 2)))
        val c = 2 * Math.asin(Math.sqrt(a))
        val valueResult = Radius * c
        val km = valueResult / 1
        val newFormat = DecimalFormat("####")
        val kmInDec = Integer.valueOf(newFormat.format(km))
        val meter = valueResult % 1000
        val meterInDec = Integer.valueOf(newFormat.format(meter))
        Log.i("Radius Value", ("" + valueResult + " KM " + kmInDec
                + " Meter " + meterInDec))
        return kmInDec.toDouble()
    }

    open fun onButtonNoClick(from: String) {

    }

    open fun onButtonYesClick(from: String) {
//
//        if ("LOGOUT".equals(from, ignoreCase = true)) {
//
//            if (context !is LoginActivity) {
//             //   preferenceUtils.saveBoolean(PreferenceUtils.IS_LOGIN, false)
//                val logoutIntent = Intent(context, LoginActivity::class.java)
//                logoutIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
//                startActivity(logoutIntent)
//                finish()
//            }
//        }


    }

    /**
     * For hiding progress dialog (Loader ).
     */

    fun hideLoader() {
        runOnUiThread {
            try {
                if (::dialogLoader.isInitialized) {
                    if (dialogLoader != null && dialogLoader.isShowing()) {
                        dialogLoader.dismiss()
                    }
                }

            } catch (e: Exception) {
                LogUtils.debug("LOADER", e.toString())
                e.printStackTrace()
            }
        }
    }


    fun hideKeyBoard(view: View) {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun showSoftKeyboard(view: View) {
        if (view.requestFocus()) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
        }
    }


    private inner class RunShowLoader(private val strMsg: String) : Runnable {

        override fun run() {
            try {
                dialogLoader = Dialog(this@BaseActivity)
                dialogLoader.requestWindowFeature(Window.FEATURE_NO_TITLE)
                dialogLoader.setCancelable(false)
                dialogLoader.getWindow()!!.setBackgroundDrawableResource(android.R.color.transparent)
                dialogLoader.setContentView(R.layout.loader_custom)
                if (!dialogLoader.isShowing())
                    dialogLoader.show()
            } catch (e: Exception) {
                // dialogLoader = null
            }

        }
    }

    fun menuOperates() {
        if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
            mDrawerLayout.closeDrawer(Gravity.LEFT)
        } else {
            mDrawerLayout.openDrawer(Gravity.LEFT)
        }
    }


    protected fun disableMenuWithBackButton() {
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, Gravity.LEFT)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
    }

    abstract fun initialize()

    abstract fun initializeControls()

    private fun baseInitializeControls() {
        flToolbar = findViewById<View>(R.id.flToolbar) as FrameLayout
        llBody = findViewById<View>(R.id.llBody) as LinearLayout
        toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        expandableListView = findViewById<View>(R.id.expandableListView) as ExpandableListView
        ivMenu = findViewById<View>(R.id.ivMenu) as ImageView
        ivAdd = findViewById<View>(R.id.ivAdd) as ImageView
        rlAdd = findViewById<View>(R.id.rlAdd) as RelativeLayout

        ivCustomerLocation = findViewById<View>(R.id.ivCustomerLocation) as ImageView

        tvScreenTitle = findViewById<View>(R.id.tvScreenTitle) as TextView
        tvDriverName = findViewById<View>(R.id.tvDriverName) as TextView
        tvDriverCode = findViewById<View>(R.id.tvDriverCode) as TextView
        tvDate = findViewById<View>(R.id.tvDate) as TextView
        tvRoutingId = findViewById<View>(R.id.tvRoutingId) as TextView
        ivSearch = findViewById<View>(R.id.ivSearch) as ImageView
        tvScreenTitleTop = findViewById<View>(R.id.tvScreenTitleTop) as TextView
        tvScreenTitleBottom = findViewById<View>(R.id.tvScreenTitleBottom) as TextView
        ivPic = findViewById<View>(R.id.ivProfilePic) as ImageView
        svSearch = findViewById<View>(R.id.svSearch) as SearchView
        tvCompanyName = findViewById<View>(R.id.tvCompanyName) as TextView
        ivProfileArrow = findViewById<View>(R.id.ivProfileArrow) as ImageView
        tvCompanyCode = findViewById<View>(R.id.tvCompanyCode) as TextView
        ivRefresh = findViewById<View>(R.id.ivRefresh) as ImageView
        ivSelectAll = findViewById<View>(R.id.ivSelectAll) as ImageView
        ivViewAll = findViewById<View>(R.id.ivViewAll) as Button
//        ivShowAll = findViewById<View>(R.id.ivShowAll) as Button

        tvVehicleCode = findViewById<View>(R.id.tvVehicleCode) as TextView
        tvPlate = findViewById<View>(R.id.tvPlate) as TextView
        tvShipments = findViewById<View>(R.id.tvShipments) as TextView
        tvADT = findViewById<View>(R.id.tvADT) as TextView
        tvDDT = findViewById<View>(R.id.tvDDT) as TextView

        tvStatus = findViewById<View>(R.id.tvStatus) as TextView
        llData = findViewById<View>(R.id.llData) as LinearLayout
        llProfile = findViewById<View>(R.id.llProfile) as LinearLayout
        llStatus = findViewById<View>(R.id.llStatus) as LinearLayout
        llUserProfile = findViewById<View>(R.id.llUserProfile) as LinearLayout


        mDrawerLayout = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        // mDrawerList          = (ListView) findViewById(R.id.left_drawer);
        prepareMenuData()
        populateExpandableList()
//        if(localCustomerDOS==null&&localCustomerDOS.size==0){
//
//            localCustomerDOS = StorageManager.getInstance(this).getSpotSalesCustomerList(this)
//        }
        llUserProfile.setOnClickListener {
            val data = preferenceUtils.getStringFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID, "")
            if (data.length > 0) {
                if (llData.getVisibility() == View.VISIBLE) {
                    llData.setVisibility(View.GONE);
                    ivProfileArrow.setRotation(270f)
                } else {
                    llData.setVisibility(View.VISIBLE);
                    ivProfileArrow.setRotation(90f)
                }
            } else {
                tvRoutingId.setVisibility(View.GONE)
                tvDate.setVisibility(View.GONE)
                llData.setVisibility(View.GONE)

            }
        }

        ivMenu.setOnClickListener {

            val popup = PopupMenu(this@BaseActivity, ivMenu)
            popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu())
            popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                override fun onMenuItemClick(item: MenuItem): Boolean {
                    if (item.title.equals("Logout")) {
                        logOut()
                    } else if (item.title.equals("Pending Orders")) {
                        val vehicleCheckInDo = StorageManager.getInstance(this@BaseActivity).getVehicleCheckInData(this@BaseActivity);
                        val status = vehicleCheckInDo.checkInStatus;
                        if (!status!!.isEmpty()) {
                            val intent = Intent(this@BaseActivity, PendingOrdersActivity::class.java)
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            startActivity(intent)
                        } else {
                            showAppCompatAlert("Alert!", "Please Finish Check-in Process", "OK", "", "FAILURE", false)
                        }
                    } else if (item.title.equals("Activity Report")) {
                        val vehicleCheckInDo = StorageManager.getInstance(this@BaseActivity).getVehicleCheckInData(this@BaseActivity);
                        val status = vehicleCheckInDo.checkInStatus;
                        if (!status!!.isEmpty()) {
                            val intent = Intent(this@BaseActivity, TransactionListActivity::class.java)
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            startActivity(intent)
                        } else {
                            showAppCompatAlert("Alert!", "Please Finish Check-in Process", "OK", "", "FAILURE", false)

                        }

                    } else if (item.title.equals("User Activity Report")) {
//                        val vehicleCheckInDo = StorageManager.getInstance(this@BaseActivity).getVehicleCheckInData(this@BaseActivity);
//                        val status = vehicleCheckInDo.checkInStatus;
//                        if (!status!!.isEmpty()) {
//                            val intent = Intent(this@BaseActivity, UserActivityReportActivity::class.java)
//                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//                            startActivity(i   ntent)
//                        } else {
//                            showAppCompatAlert("Alert!", "Please Finish Check-in Process", "OK", "", "FAILURE", false)
//
//                        }
                        val intent = Intent(this@BaseActivity, UserActivityReportActivity::class.java)
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        startActivity(intent)
                    } else if (item.title.equals("Available Stock")) {
                        val vehicleCheckInDo = StorageManager.getInstance(this@BaseActivity).getVehicleCheckInData(this@BaseActivity);
                        val status = vehicleCheckInDo.checkInStatus;
                        if (!status!!.isEmpty()) {
                            val intent = Intent(this@BaseActivity, AvailableStockActivity::class.java)
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            startActivity(intent)
                        } else {
                            showAppCompatAlert("Alert!", "Please Finish Check-in Process", "OK", "", "FAILURE", false)

                        }

                    } else if (item.title.equals("Vehicle Stock")) {
                        val vehicleCheckInDo = StorageManager.getInstance(this@BaseActivity).getVehicleCheckInData(this@BaseActivity);
                        val status = vehicleCheckInDo.checkInStatus;
                        if (!status!!.isEmpty()) {
                            val intent = Intent(this@BaseActivity, NewAvailableStockActivity::class.java)
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            startActivity(intent)
                        } else {
                            showAppCompatAlert("Alert!", "Please Finish Check-in Process", "OK", "", "FAILURE", false)

                        }

                    } else {
                        val vehicleCheckInDo = StorageManager.getInstance(this@BaseActivity).getVehicleCheckInData(this@BaseActivity);
                        val status = vehicleCheckInDo.checkInStatus;
                        if (!status!!.isEmpty()) {
                            val intent = Intent(this@BaseActivity, CurrentVanSaleStockTabActivity::class.java)
                            intent.putExtra("ScreenTitle", "Current Van Sales Stock");
                            intent.putExtra("FLAG", 1);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            startActivity(intent)
                        } else {
                            showAppCompatAlert("Alert!", "Please Finish Check-in Process", "OK", "", "FAILURE", false)

                        }
                    }
//                        else if (item.title.equals("Settings")) {
//                             /* val intent = Intent(this@BaseActivity, PrintDocumentsActivity::class.java)
//                               startActivity(intent)*/
//                        } else if (item.title.equals("Messages")) {
//                            /*val intent = Intent(this@BaseActivity, DisplayBillDetailsActivity::class.java)
//                            startActivity(intent)*/
//                        } else if (item.title.equals("Events")) {
//                            /* val intent = Intent(this@BaseActivity, TBSMapsNavigationActivity::class.java)
//                             startActivity(intent)*/
//                        } else {
//                            Toast.makeText(this@BaseActivity, "You Clicked : " + item.getTitle(), Toast.LENGTH_SHORT).show()
//                        }
                    return true

                }


            })
            popup.show()//showing popup menu

        }

//        ivBack.setOnClickListener {
//           finish()
//        }

    }

    internal inner class groupClick : ExpandableListView.OnGroupClickListener {

        override fun onGroupClick(parent: ExpandableListView, v: View, groupPosition: Int, id: Long): Boolean {
            if ((this@BaseActivity.headerList.get(groupPosition) as MenuModel).isGroup && !(this@BaseActivity.headerList.get(groupPosition) as MenuModel).hasChildren) {
                this@BaseActivity.onBackPressed()
            }
            if (groupPosition == 3) {
                val vehicleCheckInDo = StorageManager.getInstance(this@BaseActivity).getVehicleCheckInData(this@BaseActivity);
                val status = vehicleCheckInDo.checkInStatus;
                if (!status!!.isEmpty()) {
                    val intent = Intent(this@BaseActivity, CurrentVanSaleStockTabActivity::class.java)
                    intent.putExtra("ScreenTitle", "Current Van Sales Stock");
                    intent.putExtra("FLAG", 1);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                } else {
                    showAppCompatAlert("Alert!", "Please Finish Check-in Process", "OK", "", "FAILURE", false)

                }
                menuOperates()
            }
            return false
        }
    }

    /* renamed from: test.com.screensdesign.activities.BaseNavigationActivity$2 */
    internal inner class childClick : ExpandableListView.OnChildClickListener {

        override fun onChildClick(parent: ExpandableListView, v: View, groupPosition: Int, childPosition: Int, id: Long): Boolean {
            if (this@BaseActivity.childList.get(this@BaseActivity.headerList.get(groupPosition)) != null) {
                val model = this@BaseActivity.childList.get(this@BaseActivity.headerList.get(groupPosition))!![childPosition] as MenuModel
                if (model.menuName.equals("Vehicle Check in", ignoreCase = true)) {
                    val intent = Intent(this@BaseActivity, CheckinScreen::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                } else if (model.menuName.equals("Delivery/Returns", ignoreCase = true)) {
                    val vehicleCheckOutDo = StorageManager.getInstance(this@BaseActivity).getVehicleCheckOutData(this@BaseActivity)
                    if (vehicleCheckOutDo.confirmCheckOutArrival.equals("", true)) {
                        val vehicleCheckInDo = StorageManager.getInstance(this@BaseActivity).getVehicleCheckInData(this@BaseActivity);
                        if (!vehicleCheckInDo.checkInStatus!!.isEmpty()) {
                            var shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
                            if (shipmentId == null || shipmentId.equals("")) {
                                val intent = Intent(this@BaseActivity, SpotSalesCustomerActivity::class.java)
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                startActivity(intent)
                            } else {
                                showToast("Please process the current schedule " + shipmentId + " shipment")
                            }
                        } else {
                            showAppCompatAlert("Alert!", "Please Finish Check-in Process", "OK", "", "FAILURE", false)
                        }
                    } else {
                        showAppCompatAlert("", "You have arrived for CheckOut, you cannot access at this time", "OK", "", "", false)
                    }
                } else if (model.menuName.equals("Invoice", ignoreCase = true)) {
                    val vehicleCheckOutDo = StorageManager.getInstance(this@BaseActivity).getVehicleCheckOutData(this@BaseActivity)
                    if (vehicleCheckOutDo.confirmCheckOutArrival.equals("", true)) {
                        val vehicleCheckInDo = StorageManager.getInstance(this@BaseActivity).getVehicleCheckInData(this@BaseActivity);
                        if (!vehicleCheckInDo.checkInStatus!!.isEmpty()) {
                            var shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
                            if (shipmentId == null || shipmentId.equals("")) {
                                val intent = Intent(this@BaseActivity, SalesInvoiceActivity::class.java)
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                intent.putExtra("SALES", 1)
                                startActivity(intent)
                            } else {
                                showAppCompatAlert("Alert!", "Please process the current schedule " + shipmentId + " shipment", "OK", "", "FAILURE", false)
                            }

                        } else {
                            showAppCompatAlert("Alert!", "Please Finish Check-in Process", "OK", "", "FAILURE", false)
                        }
                    } else {
                        showAppCompatAlert("", "You have arrived for CheckOut, you cannot access at this time", "OK", "", "", false)
                    }
                } else if (model.menuName.equals("Payments", ignoreCase = true)) {
                    val vehicleCheckOutDo = StorageManager.getInstance(this@BaseActivity).getVehicleCheckOutData(this@BaseActivity)
                    if (vehicleCheckOutDo.confirmCheckOutArrival.equals("", true)) {
                        val vehicleCheckInDo = StorageManager.getInstance(this@BaseActivity).getVehicleCheckInData(this@BaseActivity);
                        if (!vehicleCheckInDo.checkInStatus!!.isEmpty()) {
                            var shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")

                            if (shipmentId == null || shipmentId.equals("")) {
                                val intent = Intent(this@BaseActivity, SalesPaymentsActivity::class.java)
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                intent.putExtra("SALES", 1)
                                startActivity(intent)
                            } else {
                                showToast("Please process the current schedule " + shipmentId + " shipment")
                            }
                        } else {
                            showAppCompatAlert("Alert!", "Please Finish Check-in Process", "OK", "", "FAILURE", false)
                        }
                    } else {
                        showAppCompatAlert("", "You have arrived for CheckOut, you cannot access at this time", "OK", "", "", false)
                    }
                } else if (model.menuName.equals("Alerts", ignoreCase = true)) {
                    val vehicleCheckInDo = StorageManager.getInstance(this@BaseActivity).getVehicleCheckInData(this@BaseActivity);
                    if (!vehicleCheckInDo.checkInStatus!!.isEmpty()) {
//                    val intent = Intent(this@BaseActivity, ViewImageActivity::class.java)
//                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//                    startActivity(intent)
                    } else {
                        showAppCompatAlert("Alert!", "Please Finish Check-in Process", "OK", "", "FAILURE", false)
                    }
                } else if (model.menuName.equals("Unload Van Sales Stock", ignoreCase = true)) {
                    val vehicleCheckInDo = StorageManager.getInstance(this@BaseActivity).getVehicleCheckInData(this@BaseActivity);
                    var status = vehicleCheckInDo.checkInStatus;
                    if (!status!!.isEmpty()) {
                        val intent = Intent(this@BaseActivity, CurrentVanSaleStockTabActivity::class.java)
                        intent.putExtra("ScreenTitle", "Unload Van Sales Stock");
                        intent.putExtra("FLAG", 11);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        startActivity(intent)
                    } else {
                        showAppCompatAlert("Alert!", "Please Finish Check-in Process", "OK", "", "FAILURE", false)
                    }
                } else if (model.menuName.equals("Inventory Count", ignoreCase = true)) {
                    val vehicleCheckInDo = StorageManager.getInstance(this@BaseActivity).getVehicleCheckInData(this@BaseActivity);
                    if (!vehicleCheckInDo.checkInStatus!!.isEmpty()) {
                        val intent = Intent(this@BaseActivity, CaptureReturnDetailsActivity::class.java)
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        startActivity(intent)
                    } else {
                        showAppCompatAlert("Alert!", "Please Finish Check-in Process", "OK", "", "FAILURE", false)
                    }
                } else if (model.menuName.equals("Vehicle Check Out", ignoreCase = true)) {
                    val vehicleCheckInDo = StorageManager.getInstance(this@BaseActivity).getVehicleCheckInData(this@BaseActivity);
                    if (!vehicleCheckInDo.checkInStatus!!.isEmpty()) {
                        var shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
                        if (shipmentId == null || shipmentId.equals("")) {
                            val customerDo = StorageManager.getInstance(this@BaseActivity).getCurrentSpotSalesCustomer(this@BaseActivity)
                            if (customerDo.customerId.equals("")) {
                                val intent = Intent(this@BaseActivity, CheckOutScreen::class.java)
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                startActivity(intent)
                            } else {
                                showAppCompatAlert("", "Please process the current customer " + customerDo.customerId + "'s non schedule shipment", "OK", "", "FAILURE", false)
                            }

                        } else {
                            showToast("Please process the current schedule " + shipmentId + " shipment")
                        }


                    } else {
                        showAppCompatAlert("Alert!", "Please Finish Check-in Process", "OK", "", "FAILURE", false)
                    }
                } else if (model.menuName.equals("Current Scheduled Stock", ignoreCase = true)) {
                    val vehicleCheckInDo = StorageManager.getInstance(this@BaseActivity).getVehicleCheckInData(this@BaseActivity);
                    if (!vehicleCheckInDo.checkInStatus!!.isEmpty()) {
                        val intent = Intent(this@BaseActivity, CurrentScheduleStockActivity::class.java)
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        startActivity(intent)
                    } else {
                        showAppCompatAlert("Alert!", "Please Finish Check-in Process", "OK", "", "FAILURE", false)
                    }
                } else if (model.menuName.equals("Delivery List", ignoreCase = true)) {
                    val vehicleCheckOutDo = StorageManager.getInstance(this@BaseActivity).getVehicleCheckOutData(this@BaseActivity)
                    if (vehicleCheckOutDo.confirmCheckOutArrival.equals("", true)) {
                        val vehicleCheckInDo = StorageManager.getInstance(this@BaseActivity).getVehicleCheckInData(this@BaseActivity);
                        if (!vehicleCheckInDo.checkInStatus!!.isEmpty()) {
                            val customerDo = StorageManager.getInstance(this@BaseActivity).getCurrentSpotSalesCustomer(this@BaseActivity)
                            if (customerDo.customerId.equals("")) {
                                val vehicleRoutId = preferenceUtils!!.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "")
                                if (vehicleRoutId.length > 0) {
                                    val intent = Intent(this@BaseActivity, VehicleRoutingList::class.java)
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                    startActivity(intent)
                                } else {
                                    showAppCompatAlert("Alert!", "No Shipments Found", "OK", "", "FAILURE", false)

                                }

                            } else {
                                showAppCompatAlert("", "Please process the current customer " + customerDo.customerId + "'s non schedule shipment", "OK", "", "FAILURE", false)
                            }
                        } else {
                            showAppCompatAlert("", "Please Finish Check-in Process", "OK", "", "FAILURE", false)
                        }
                    } else {
                        showAppCompatAlert("", "You have arrived for CheckOut, you cannot access at this time", "OK", "", "", false)
                    }
                } else if (model.menuName.equals("Sales Active Delivery", ignoreCase = true)) {
                    val vehicleCheckOutDo = StorageManager.getInstance(this@BaseActivity).getVehicleCheckOutData(this@BaseActivity)
                    if (vehicleCheckOutDo.confirmCheckOutArrival.equals("", true)) {
                        val vehicleCheckInDo = StorageManager.getInstance(this@BaseActivity).getVehicleCheckInData(this@BaseActivity);
                        if (!vehicleCheckInDo.checkInStatus!!.isEmpty()) {
                            var shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
                            if (shipmentId == null || shipmentId.equals("")) {
                                val customerDo = StorageManager.getInstance(this@BaseActivity).getCurrentSpotSalesCustomer(this@BaseActivity)
                                if (!customerDo.customerId.equals("")) {
                                    val intent = Intent(this@BaseActivity, ActiveDeliveryActivity::class.java)
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                    startActivity(intent)
                                } else {
                                    showAppCompatAlert("", "No active delivery", "OK", "", "", false)

                                }
                            } else {
                                showAppCompatAlert("", "Please process the current schedule " + shipmentId + " shipment", "OK", "", "FAILURE", false)
                            }

                        } else {
                            showAppCompatAlert("", "Please Finish Check-in Process", "OK", "", "FAILURE", false)
                        }
                    } else {
                        showAppCompatAlert("", "You have arrived for CheckOut, you cannot access at this time", "OK", "", "", false)
                    }
                } else if (model.menuName.equals("Load Van Sales Stock", ignoreCase = true)) {
                    val vehicleCheckInDo = StorageManager.getInstance(this@BaseActivity).getVehicleCheckInData(this@BaseActivity);
                    if (!vehicleCheckInDo.checkInStatus!!.isEmpty()) {
                        if (!vehicleCheckInDo.loadStock.equals("")) {
                            val intent = Intent(this@BaseActivity, LoadVanSaleStockTabActivity::class.java)
                            intent.putExtra("FLAG", 1);
                            intent.putExtra("ScreenTitle", "Load Van Sales Stock")
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            startActivity(intent)
                        } else {
                            showToast("Please complete the stock load in check in")
                        }
                    } else {

                        if (!vehicleCheckInDo.loadStock.equals("")) {
                            val intent = Intent(this@BaseActivity, LoadVanSaleStockTabActivity::class.java)
                            intent.putExtra("FLAG", 1);
                            intent.putExtra("ScreenTitle", "Load Van Sales Stock")
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            startActivity(intent)
                        } else {
                            showAppCompatAlert("", "Please Finish Check-in Process", "OK", "", "", false)
                        }

                    }
                } else if (model.menuName.equals("Active Delivery", ignoreCase = true)) {
                    val vehicleCheckOutDo = StorageManager.getInstance(this@BaseActivity).getVehicleCheckOutData(this@BaseActivity)
                    if (vehicleCheckOutDo.confirmCheckOutArrival.equals("", true)) {
                        val vehicleCheckInDo = StorageManager.getInstance(this@BaseActivity).getVehicleCheckInData(this@BaseActivity);
                        if (!vehicleCheckInDo.checkInStatus!!.isEmpty()) {
                            val shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
                            if (!shipmentId.isEmpty()) {
                                val intent = Intent(this@BaseActivity, ActiveDeliveryActivity::class.java)
                                intent.putExtra("SHIPMENT_ID", shipmentId);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                startActivity(intent)
                            } else {
                                showAppCompatAlert("", "No active delivery", "OK", "", "", false)
                            }
                        } else {
                            showAppCompatAlert("", "Please Finish Check-in Process", "OK", "", "", false)
                        }
                    } else {
                        showAppCompatAlert("", "You have arrived for CheckOut, you cannot access at this time", "OK", "", "", false)
                    }
                } else if (model.menuName.equals("Shops/Customers", ignoreCase = true)) {
                    val intent = Intent(this@BaseActivity, MasterDataCustomerActivity::class.java)
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                } else if (model.menuName.equals("Sites", ignoreCase = true)) {
                    val vehicleCheckInDo = StorageManager.getInstance(this@BaseActivity).getVehicleCheckInData(this@BaseActivity);
                    val intent = Intent(this@BaseActivity, SiteActivity::class.java)
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                } else if (model.menuName.equals("Products", ignoreCase = true)) {
                    val intent = Intent(this@BaseActivity, ProductActivity::class.java)
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                } else if (model.menuName.equals("Price Lists", ignoreCase = true)) {
                    val intent = Intent(this@BaseActivity, PriceActivity::class.java)
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                }
                menuOperates()
            }
            return false
        }
    }

    /* renamed from: test.com.screensdesign.activities.BaseActivity$3 */
    internal inner class groupExpand : ExpandableListView.OnGroupExpandListener {

        override fun onGroupExpand(groupPosition: Int) {
            if (!(this@BaseActivity.lastExpandedPosition == -1 || groupPosition == this@BaseActivity.lastExpandedPosition)) {
                this@BaseActivity.expandableListView!!.collapseGroup(this@BaseActivity.lastExpandedPosition)
            }
            this@BaseActivity.lastExpandedPosition = groupPosition
        }
    }

    fun populateExpandableList() {
        this.expandableListAdapter = ExpandableListAdapter(this, this.headerList, this.childList)
        this.expandableListView!!.setAdapter(this.expandableListAdapter)
        this.expandableListView!!.setOnGroupClickListener(groupClick())
        this.expandableListView!!.setOnChildClickListener(childClick())
        this.expandableListView!!.setOnGroupExpandListener(groupExpand())
    }

    fun refreshMenuAdapter() {
//        expandableListAdapter!!.notifyDataSetChanged()
        val vehicleCheckInDo = StorageManager.getInstance(this@BaseActivity).getVehicleCheckInData(this@BaseActivity);
        if (vehicleCheckInDo.checkInStatus.equals("", true)) {
            if (!vehicleCheckInDo.loadStock.equals("")) {
                tvStatus.setText(vehicleCheckInDo.loadStock)
            } else {
                tvStatus.setText("Logged In")
            }
        } else {
            tvStatus.setText(vehicleCheckInDo.checkInStatus)
        }


        expandableListAdapter!!.notifyDataSetChanged()
    }

    fun applyFont(root: View, font: String) {
        try {
            val fontName = fontStyleName(font)
            if (root is ViewGroup) {
                for (i in 0 until root.childCount)
                    applyFont(root.getChildAt(i), fontName)
            } else if (root is TextView) {
                root.typeface = Typeface.createFromAsset(assets, fontName)
            } else if (root is EditText) {
                root.typeface = Typeface.createFromAsset(assets, fontName)
            }
        } catch (e: Exception) {
//            LogUtils.error("HAADHIR", String.format("Error occured when trying to apply %s font for %s view", font, root))
            e.printStackTrace()
        }

    }

    private fun fontStyleName(name: String): String {
        var fontStyleName = ""
        when (name) {
            AppConstants.Goudy_Old_Style_Italic -> fontStyleName = "gillsansstd.otf"
            AppConstants.Goudy_Old_Style_Normal -> fontStyleName = "montserrat_regular.ttf"
            AppConstants.Goudy_Old_Style_Bold -> fontStyleName = "montserrat_bold.ttf"

            "" -> fontStyleName = "montserrat_regular.ttf"
        }
        return fontStyleName
    }

    private fun prepareMenuData() {
        val childModelsList = ArrayList<MenuModel>()
        var menuModel = MenuModel("Start Day", true, true)
        this.headerList.add(menuModel)
        childModelsList.add(MenuModel("Vehicle Check in", false, false))
        childModelsList.add(MenuModel("Load Van Sales Stock", false, false))
        if (menuModel.hasChildren) {
            this.childList.put(menuModel, childModelsList)
        }
        var childModelsList2 = ArrayList<MenuModel>()
        menuModel = MenuModel("Scheduled Sales", true, true)
        this.headerList.add(menuModel)
        childModelsList2.add(MenuModel("Delivery List", false, false))
        childModelsList2.add(MenuModel("Active Delivery", false, false))
        // childModelsList2.add(MenuModel("Alerts", false, false))
        if (menuModel.hasChildren) {
            this.childList.put(menuModel, childModelsList2)
        }
        childModelsList2 = ArrayList()
        menuModel = MenuModel("Spot Sales", true, true)
        this.headerList.add(menuModel)
        childModelsList2.add(MenuModel("Delivery/Returns", false, false))
        childModelsList2.add(MenuModel("Sales Active Delivery", false, false))

//        childModelsList2.add(MenuModel("Invoice", false, false))
//        childModelsList2.add(MenuModel("Payments", false, false))
        if (menuModel.hasChildren) {
            this.childList.put(menuModel, childModelsList2)
        }
        childModelsList2 = ArrayList()
//       menuModel = MenuModel("Credit Collection", true, true)
//       this.headerList.add(menuModel)
//        if (menuModel.hasChildren) {
//            this.childList.put(menuModel, childModelsList2)
//        }
        childModelsList2 = ArrayList()
        menuModel = MenuModel("Current Van Sales Stock", true, true)
        this.headerList.add(menuModel)
//        childModelsList2.add(MenuModel("Current Van Sales Stock", false, false))
//        childModelsList2.add(MenuModel("Current Scheduled Stock", false, false))
        if (menuModel.hasChildren) {
            this.childList.put(menuModel, childModelsList2)
        }

        childModelsList2 = ArrayList()
        menuModel = MenuModel("End Day", true, true)
        this.headerList.add(menuModel)
        //childModelsList2.add(MenuModel("Cash Remit", false, false))
        //  childModelsList2.add(MenuModel("Unload Van Sales Stock", false, false))
        //  childModelsList2.add(MenuModel("Inventory Count", false, false))
        childModelsList2.add(MenuModel("Vehicle Check Out", false, false))
        if (menuModel.hasChildren) {
            this.childList.put(menuModel, childModelsList2)
        }
        childModelsList2 = ArrayList()
        menuModel = MenuModel("Master Data", true, true)
        this.headerList.add(menuModel)
//        childModelsList2.add(MenuModel("Sites", false, false))
        childModelsList2.add(MenuModel("Shops/Customers", false, false))
        childModelsList2.add(MenuModel("Products", false, false))
        //  childModelsList2.add(MenuModel("Price Lists", false, false))
        if (menuModel.hasChildren) {
            this.childList.put(menuModel, childModelsList2)
        }
    }

    override fun onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
            mDrawerLayout.closeDrawer(Gravity.LEFT)
        } else {
            super.onBackPressed()
        }

    }

    public fun logOut() {
        var driverId = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "")
        var date = CalendarUtils.getDate()
        var time = CalendarUtils.getTime()
        logOutTimeCapture()

//        val siteListRequest = LogoutMultipleUsersRequest(driverId, date, date, time, this)
//        siteListRequest.setOnResultListener { isError, loginDo ->
//            hideLoader()
//            if (loginDo != null) {
//                if (isError) {
//                    hideLoader()
//                    showToast("you are unable to logout")
//                } else {
//                    hideLoader()
//                    if (loginDo.flag == 1) {
//                        logOutTimeCapture()
//
//
//                    } else {
//                        showToast("you are unable to logout")
//
//                    }
//
//                }
//            } else {
//                hideLoader()
//            }
//
//        }
//
//        siteListRequest.execute()
    }

    private fun logOutTimeCapture() {
        var driverId = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "")
        var date = CalendarUtils.getDate()
        var time = CalendarUtils.getTime()

        val siteListRequest = LogoutTimeCaptureRequest(driverId, date, date, time, this)
        siteListRequest.setOnResultListener { isError, loginDo ->
            hideLoader()
            if (loginDo != null) {
                if (isError) {
                    hideLoader()
                    Toast.makeText(this@BaseActivity, R.string.error_NoData, Toast.LENGTH_SHORT).show()
                } else {
                    hideLoader()
                    if (loginDo.flag == 0) {
                        showToast("You have successfully logged out!")
                        clearDataLocalData()


                    } else {
                        showToast("You are unable to logout")

                    }

                }
            } else {
                hideLoader()
            }

        }

        siteListRequest.execute()
    }

    private fun clearDataLocalData() {
        hideLoader()
        preferenceUtils.removeFromPreference(PreferenceUtils.CHECKIN_DATE)
        preferenceUtils.removeFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.CUSTOMER)
        preferenceUtils.removeFromPreference(PreferenceUtils.LATTITUDE)
        preferenceUtils.removeFromPreference(PreferenceUtils.LONGITUDE)
        preferenceUtils.removeFromPreference(PreferenceUtils.VEHICLE_CHECKED_ROUTE_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.CARRIER_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.NOTES)
        preferenceUtils.removeFromPreference(PreferenceUtils.PREVIEW_INVOICE_ID);
        preferenceUtils.removeFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER)
        preferenceUtils.removeFromPreference(PreferenceUtils.CREDIT_AMOUNT);
        preferenceUtils.removeFromPreference(PreferenceUtils.PAYMENT_ID);
        preferenceUtils.removeFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.INVOICE_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.SHIPMENT)
        preferenceUtils.removeFromPreference(PreferenceUtils.SITE_NAME)
        preferenceUtils.removeFromPreference(PreferenceUtils.VOLUME)
        preferenceUtils.removeFromPreference(PreferenceUtils.WEIGHT)
        preferenceUtils.removeFromPreference(PreferenceUtils.CV_PLATE)
        preferenceUtils.removeFromPreference(PreferenceUtils.CA_DATE)
        preferenceUtils.removeFromPreference(PreferenceUtils.CA_TIME)
        preferenceUtils.removeFromPreference(PreferenceUtils.CD_DATE)
        preferenceUtils.removeFromPreference(PreferenceUtils.CVEHICLE_CODE)
        preferenceUtils.removeFromPreference(PreferenceUtils.CD_TIME)
        preferenceUtils.removeFromPreference(PreferenceUtils.CN_SHIPMENTS)
        preferenceUtils.removeFromPreference(PreferenceUtils.CHECK_IN_STATUS)
        preferenceUtils.removeFromPreference(PreferenceUtils.TOTAL_LOAD)
        preferenceUtils.removeFromPreference(PreferenceUtils.VEHICLE_CAPACITY)
        preferenceUtils.removeFromPreference(PreferenceUtils.SELECTION)
//        preferenceUtils.removeFromPreference(PreferenceUtils.Non_Scheduled_Route_Id)
        preferenceUtils.removeFromPreference(PreferenceUtils.TOTAL_NON_SCHEDULED_STOCK)
        preferenceUtils.removeFromPreference(PreferenceUtils.DAMAGE_REASON)
        preferenceUtils.removeFromPreference(PreferenceUtils.DAMAGE_REASON_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.EMIRATES_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.COMMENT_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.PRICE_TAG)
        preferenceUtils.removeFromPreference(PreferenceUtils.SALES_ORDER_NUMBER);
        preferenceUtils.removeFromPreference(PreferenceUtils.FINANTIAL_SITE_ID);
        preferenceUtils.removeFromPreference(PreferenceUtils.CREQUESTED_NUMBER);
        preferenceUtils.removeFromPreference(PreferenceUtils.REQUESTED_NUMBER);
        preferenceUtils.removeFromPreference(PreferenceUtils.CREDIT_FLAG);

        preferenceUtils.removeFromPreference(PreferenceUtils.CONFIG_SUCCESS)
        preferenceUtils.removeFromPreference(PreferenceUtils.CAPTURE)
        preferenceUtils.removeFromPreference(PreferenceUtils.DAMAGE)
        preferenceUtils.removeFromPreference(PreferenceUtils.STOCK_COUNT)
        preferenceUtils.removeFromPreference(PreferenceUtils.BANK_REASON)
        preferenceUtils.removeFromPreference(PreferenceUtils.WEIGHT)
        preferenceUtils.removeFromPreference(PreferenceUtils.VOLUME)
        preferenceUtils.removeFromPreference(PreferenceUtils.VEHICLE_CAPACITY)
        preferenceUtils.removeFromPreference(PreferenceUtils.TOTAL_LOAD)
        preferenceUtils.removeFromPreference(PreferenceUtils.CUSTOMER)
        preferenceUtils.removeFromPreference(PreferenceUtils.CUSTOMER_NAME)
        preferenceUtils.removeFromPreference(PreferenceUtils.SITE_NAME)
        preferenceUtils.removeFromPreference(PreferenceUtils.TOTAL_NON_SCHEDULED_STOCK)
        preferenceUtils.removeFromPreference(PreferenceUtils.CREDIT_INVOICE_AMOUNT)
        preferenceUtils.removeFromPreference(PreferenceUtils.SELECTION_INVOICE_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.SHIPMENT)
        preferenceUtils.removeFromPreference(PreferenceUtils.USER_NAME)
        preferenceUtils.removeFromPreference(PreferenceUtils.PASSWORD)
        preferenceUtils.removeFromPreference(PreferenceUtils.A_USER_NAME)
        preferenceUtils.removeFromPreference(PreferenceUtils.A_PASSWORD)
        preferenceUtils.removeFromPreference(PreferenceUtils.IP_ADDRESS)
        preferenceUtils.removeFromPreference(PreferenceUtils.PORT)
        preferenceUtils.removeFromPreference(PreferenceUtils.ALIAS)
        preferenceUtils.removeFromPreference(PreferenceUtils.MESSAGE_SPOT)
        preferenceUtils.removeFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.PROFILE_PICTURE)
        preferenceUtils.removeFromPreference(PreferenceUtils.VEHICLE_CHECKED_ROUTE_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.SITE_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.SHIPMENT)
        preferenceUtils.removeFromPreference(PreferenceUtils.ShipmentType)
        preferenceUtils.removeFromPreference(PreferenceUtils.ProductsType)
        preferenceUtils.removeFromPreference(PreferenceUtils.CustomerId)
        preferenceUtils.removeFromPreference(PreferenceUtils.ShipmentProductsType)
        preferenceUtils.removeFromPreference(PreferenceUtils.PRODUCT_APPROVAL)
        preferenceUtils.removeFromPreference(PreferenceUtils.RESCHEDULE_APPROVAL)
        preferenceUtils.removeFromPreference(PreferenceUtils.DELIVERY_NOTE)
        preferenceUtils.removeFromPreference(PreferenceUtils.DELIVERY_NOTE_RESHEDULE)
        preferenceUtils.removeFromPreference(PreferenceUtils.PRODUCT_CODE)
        preferenceUtils.removeFromPreference(PreferenceUtils.PRODUCT_DESCRIPTION)
        preferenceUtils.removeFromPreference(PreferenceUtils.TOTAL_AMOUNT)
        preferenceUtils.removeFromPreference(PreferenceUtils.PAYMENT_TYPE)
        preferenceUtils.removeFromPreference(PreferenceUtils.C_LAT)
        preferenceUtils.removeFromPreference(PreferenceUtils.C_LONG)
        preferenceUtils.removeFromPreference(PreferenceUtils.B_SITE_NAME)
        preferenceUtils.removeFromPreference(PreferenceUtils.COMPANY)
        preferenceUtils.removeFromPreference(PreferenceUtils.COMPANY_DES)
        preferenceUtils.removeFromPreference(PreferenceUtils.B_SITE_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.DATE_FORMAT)
        preferenceUtils.removeFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER)
        preferenceUtils.removeFromPreference(PreferenceUtils.NONBG)
        preferenceUtils.removeFromPreference(PreferenceUtils.USER_ROLE)
        preferenceUtils.removeFromPreference(PreferenceUtils.LOGIN_EMAIL)
        preferenceUtils.removeFromPreference(PreferenceUtils.USER_AUTHORIZED)
        preferenceUtils.removeFromPreference(PreferenceUtils.Reason_CODE)
        preferenceUtils.removeFromPreference(PreferenceUtils.CARRIER_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.REASON)
        preferenceUtils.removeFromPreference(PreferenceUtils.Reason_Payment)
        preferenceUtils.removeFromPreference(PreferenceUtils.SELECTION)
        preferenceUtils.removeFromPreference(PreferenceUtils.V_PLATE)
        preferenceUtils.removeFromPreference(PreferenceUtils.D_TIME)
        preferenceUtils.removeFromPreference(PreferenceUtils.A_TIME)
        preferenceUtils.removeFromPreference(PreferenceUtils.N_SHIPMENTS)
        preferenceUtils.removeFromPreference(PreferenceUtils.D_DATE)
        preferenceUtils.removeFromPreference(PreferenceUtils.A_DATE)
        preferenceUtils.removeFromPreference(PreferenceUtils.VEHICLE_CODE)
        preferenceUtils.removeFromPreference(PreferenceUtils.LOAN_RETURN_NUMBER)
        preferenceUtils.removeFromPreference(PreferenceUtils.DOC_NUMBER)
        preferenceUtils.removeFromPreference(PreferenceUtils.NON_VEHICLE_CODE)
        preferenceUtils.removeFromPreference(PreferenceUtils.LOCATION)
        preferenceUtils.removeFromPreference(PreferenceUtils.CD_TIME)
        preferenceUtils.removeFromPreference(PreferenceUtils.CA_TIME)
        preferenceUtils.removeFromPreference(PreferenceUtils.CN_SHIPMENTS)
        preferenceUtils.removeFromPreference(PreferenceUtils.CD_DATE)
        preferenceUtils.removeFromPreference(PreferenceUtils.CA_DATE)
        preferenceUtils.removeFromPreference(PreferenceUtils.CVEHICLE_CODE)
        preferenceUtils.removeFromPreference(PreferenceUtils.STATUS)
        preferenceUtils.removeFromPreference(PreferenceUtils.SHIPMENT_DT)
        preferenceUtils.removeFromPreference(PreferenceUtils.SHIPMENT_AT)
        preferenceUtils.removeFromPreference(PreferenceUtils.NOTES)
        preferenceUtils.removeFromPreference(PreferenceUtils.DRIVER_NAME)
        preferenceUtils.removeFromPreference(PreferenceUtils.LOGIN_DRIVER_NAME)
        preferenceUtils.removeFromPreference(PreferenceUtils.DISTANCE)
        preferenceUtils.removeFromPreference(PreferenceUtils.EMAIL)
        preferenceUtils.removeFromPreference(PreferenceUtils.PHONE)
        preferenceUtils.removeFromPreference(PreferenceUtils.LONGITUDE2)
        preferenceUtils.removeFromPreference(PreferenceUtils.LATTITUDE2)
        preferenceUtils.removeFromPreference(PreferenceUtils.LONGITUDE)
        preferenceUtils.removeFromPreference(PreferenceUtils.LATTITUDE)
        preferenceUtils.removeFromPreference(PreferenceUtils.CHECKIN_DATE)
        preferenceUtils.removeFromPreference(PreferenceUtils.DRIVER_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.VR_ID)
//          preferenceUtils.removeFromPreference(PreferenceUtils.CHEQUE_DATE                   )
        preferenceUtils.removeFromPreference(PreferenceUtils.INVOICE_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.PAYMENT_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.INVOICE_AMOUNT)
        preferenceUtils.removeFromPreference(PreferenceUtils.CURRENCY)
        preferenceUtils.removeFromPreference(PreferenceUtils.INVOICE_SHIPMENT_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.PREVIEW_INVOICE_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.LOGIN_SUCCESS)
        preferenceUtils.removeFromPreference(PreferenceUtils.PAYMENT_INVOICE_ID)
        StorageManager.getInstance(this).deleteCheckInData(this)
        StorageManager.getInstance(this).deleteCompletedShipments(this)
        StorageManager.getInstance(this).deleteSkipShipmentList(this)
        StorageManager.getInstance(this).deleteVanNonScheduleProducts(this);
        StorageManager.getInstance(this).deleteVanScheduleProducts(this);
        StorageManager.getInstance(this).deleteSpotSalesCustomerList(this);
        StorageManager.getInstance(this).deleteVehicleInspectionList(this);
        StorageManager.getInstance(this).deleteGateInspectionList(this);
        StorageManager.getInstance(this).deleteSiteListData(this);
        StorageManager.getInstance(this).deleteShipmentListData(this)
        StorageManager.getInstance(this).deleteCheckOutData(this)
        StorageManager.getInstance(this).deleteScheduledNonScheduledReturnData();// clearing all tables
        StorageManager.getInstance(this).deleteDamageDeliveryItems(this)
        StorageManager.getInstance(this).deleteActiveDeliveryMainDo(this)
        StorageManager.getInstance(this).deleteDepartureData(this)
        StorageManager.getInstance(this).deleteVanScheduleProducts(this);
        StorageManager.getInstance(this).deleteVanNonScheduleProducts(this);
        StorageManager.getInstance(this).deleteCollectorCustomerList(this)
//        StorageManager.getInstance(this).deleteMasterDataCustomerList(this)

        preferenceUtils.removeFromPreference(PreferenceUtils.CustomerId);
        StorageManager.getInstance(this).deleteCurrentSpotSalesCustomer(this)
        StorageManager.getInstance(this).deleteCurrentDeliveryItems(this)
        StorageManager.getInstance(this).deleteReturnCylinders(this)
        preferenceUtils.clearPreferences()
        context!!.deleteDatabase(DATABASE_NAME);

        val intent = Intent(this@BaseActivity, LoginActivity::class.java)
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
    }

    override fun onResume() {
        refreshMenuAdapter()
        FireBaseOperations.checkingDate(this);

        if (AppPrefs.getBoolean(AppPrefs.IS_TIME_CHANGED, false)) {
            checkDateAndTime();
        }
        super.onResume()
    }

    private fun checkDateAndTime() {

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Setting Alert")
        builder.setMessage("Your phone date is inaccurate! Adjust your clock and try again")
        builder.setCancelable(false)
        //builder.setPositiveButton("OK", DialogInterface.OnClickListener(function = x))
        builder.setPositiveButton("Adjust Date") { dialog, which ->
            startActivityForResult(Intent(android.provider.Settings.ACTION_DATE_SETTINGS), 0);
        }
        builder.show()


    }

    public fun setTypeFace(fontType: String, viewGroup: ViewGroup) {
        val count = viewGroup.getChildCount()
        var v: View
        for (i in 0 until count) {
            v = viewGroup.getChildAt(i)
            if (v is TextView || v is Button || v is EditText || v is TextInputEditText)
                (v as TextView).typeface = getTypeFace(fontType)
            else if (v is ViewGroup)
                setTypeFace(fontType, v)
        }

    }

    private fun getTypeFace(fontType: String): Typeface {
        return Typeface.createFromAsset(getAssets(), fontType);
//        if(fontType.equals(AppConstants.MONTSERRAT_REGULAR_TYPE_FACE)){
//
//        }
//        else if(fontType.equals(AppConstants.MONTSERRAT_MEDIUM_TYPE_FACE)){
//
//        }
//        else if(fontType.equals(AppConstants.MONTSERRAT_BOLD_TYPE_FACE)){
//
//        }
//        else if(fontType.equals(AppConstants.MONTSERRAT_LIGHT_TYPE_FACE)){
//
//        }
    }

    fun showCommonReasonPopup(title: String, list: List<*>, listener: SingleSelectedItemListener) {
        val countryLayout = inflater.inflate(R.layout.reason_list_dialog_layout, null)
        applyFont(countryLayout, AppConstants.MONTSERRAT_REGULAR_TYPE_FACE)
        val appCompatDialog = AppCompatDialog(this@BaseActivity, R.style.NewDialog)
        appCompatDialog.supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
        appCompatDialog.setContentView(countryLayout)
        val listView = countryLayout.findViewById(R.id.lvSpinnerItems) as ListView
        val btnSubmit = countryLayout.findViewById(R.id.btnSubmit) as Button
        val etReasonText = countryLayout.findViewById(R.id.etReasonText) as EditText
        spinAdapter = MySpinnerAdapter(this, list, true)
        listView.adapter = spinAdapter
        appCompatDialog.setCanceledOnTouchOutside(true)
        appCompatDialog.setTitle(title)
        appCompatDialog.setCancelable(true)
        btnSubmit.isEnabled = false
        btnSubmit.setBackgroundResource(R.drawable.button_light_gray_bg)
        etReasonText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                if (s.toString().length > 0) {
                    btnSubmit.isEnabled = true
                    btnSubmit.setBackgroundResource(R.drawable.button_bg)
                } else {
                    btnSubmit.isEnabled = false
                    btnSubmit.setBackgroundResource(R.drawable.button_light_gray_bg)
                }
            }
        })
        btnSubmit.setOnClickListener {
            listener.setSingleSelectedItem(etReasonText.text.toString().trim { it <= ' ' })
            if (appCompatDialog.isShowing)
                appCompatDialog.dismiss()
        }
        listView.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            listener.setSingleSelectedItem(list[position])
            if (appCompatDialog.isShowing)
                appCompatDialog.dismiss()
        }
        if (!appCompatDialog.isShowing)
            appCompatDialog.show()
    }

    public class MySpinnerAdapter(private var context: Context, private var list: List<*>?, private var isForReason: Boolean) : BaseAdapter() {

        fun refresh(list: List<*>) {
            this.list = list
            notifyDataSetChanged()
        }

        override fun getCount(): Int {
            return if (list != null) list!!.size else 0
        }

        override fun getItem(position: Int): Any {
            return list!![position]!!
        }

        override fun getItemId(position: Int): Long {
            return 0
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            var convertView = convertView
            var holder: ViewHolder? = null
            if (convertView == null) {
                if (isForReason) {
                    convertView = LayoutInflater.from(context).inflate(R.layout.reason_list_item_cell, null)
                } else {
                    convertView = LayoutInflater.from(context).inflate(R.layout.spinner_list_item_cell, null)
                }
                holder = ViewHolder()
                holder!!.tvItemName = convertView!!.findViewById<View>(R.id.tvItemName) as TextView
                convertView.tag = holder
            } else {
                holder = convertView.tag as ViewHolder
            }
            if (list!![position] is ReasonDO) {
                val cityDo = list!![position] as ReasonDO
                holder!!.tvItemName!!.setText("" + cityDo.reason)

            }
            return convertView
        }
    }

    private class ViewHolder {
        var tvItemName: TextView? = null
    }

    @TargetApi(Build.VERSION_CODES.M)
    fun insertContactWrapper() {
        val permissionsNeeded = ArrayList<String>()
        val permissionsList = ArrayList<String>()
        if (!addPermission(permissionsList, android.Manifest.permission.CAMERA))
            permissionsNeeded.add("Camera")
        if (!addPermission(permissionsList, android.Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Storage")
        if (!addPermission(permissionsList, android.Manifest.permission.ACCESS_FINE_LOCATION))
            permissionsNeeded.add("Location")
        if (!addPermission(permissionsList, android.Manifest.permission.ACCESS_COARSE_LOCATION))
            permissionsNeeded.add("Location")
        if (permissionsList.size > 0) {
            requestPermissions(permissionsList.toArray(arrayOfNulls<String>(permissionsList.size)),
                    124)
            return
        }
    }

    private fun addPermission(permissionsList: MutableList<String>, permission: String): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission)
                // Check for Rationale Option
                if (!shouldShowRequestPermissionRationale(permission))
                    return false
            }
        }
        return true
    }

    fun location() {

        insertContactWrapper()

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mSettingsClient = LocationServices.getSettingsClient(this)

        mLocationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                super.onLocationResult(locationResult)
                // location is received
                mCurrentLocation = locationResult!!.lastLocation
                mLastUpdateTime = DateFormat.getTimeInstance().format(Date())

                updateLocationUI()
            }
        }

        mRequestingLocationUpdates = false

        mLocationRequest = LocationRequest()
        mLocationRequest!!.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS)
        mLocationRequest!!.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS)
        mLocationRequest!!.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)

        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(mLocationRequest!!)
        mLocationSettingsRequest = builder.build()

        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(object : PermissionListener {
                    override fun onPermissionRationaleShouldBeShown(permission: com.karumi.dexter.listener.PermissionRequest?, token: PermissionToken?) {

                    }

                    override fun onPermissionGranted(response: PermissionGrantedResponse) {
                        mRequestingLocationUpdates = true
                        startLocationUpdates()
                    }

                    override fun onPermissionDenied(response: PermissionDeniedResponse) {
                        if (response.isPermanentlyDenied()) {

                            showToast("Location Permission denied")
                            // open device settings when the permission is
                            // denied permanently
//                            openSettings()
                        }
                    }

                    fun onPermissionRationaleShouldBeShown(permission: PermissionRequest, token: PermissionToken) {
                        token.continuePermissionRequest()
                    }
                }).check()
    }

    fun updateLocationUI() {
        if (mCurrentLocation != null) {

            lattitudeFused = mCurrentLocation!!.getLatitude().toString()
            longitudeFused = mCurrentLocation!!.getLongitude().toString()
            var gcd = Geocoder(getBaseContext(), Locale.getDefault());

            try {
                var addresses = gcd.getFromLocation(mCurrentLocation!!.getLatitude(),
                        mCurrentLocation!!.getLongitude(), 1);
                if (addresses.size > 0) {
                    System.out.println(addresses.get(0).getLocality());
                    var cityName = addresses.get(0).getLocality();
                    addressFused = cityName
                }
            } catch (e: java.lang.Exception) {
                e.printStackTrace();
            }

            stopLocationUpdates()
        }

    }

    @SuppressLint("MissingPermission")
    private fun startLocationUpdates() {
        mSettingsClient!!
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this) {
                    Log.i(TAG, "All location settings are satisfied.")

//                    Toast.makeText(applicationContext, "Started location updates!", Toast.LENGTH_SHORT).show()


                    mFusedLocationClient!!.requestLocationUpdates(mLocationRequest,
                            mLocationCallback, Looper.myLooper())

                    updateLocationUI()
                }
                .addOnFailureListener(this) { e ->
                    val statusCode = (e as ApiException).statusCode
                    when (statusCode) {
                        LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                            Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " + "location settings ")
                            try {
                                // Show the dialog by calling startResolutionForResult(), and check the
                                // result in onActivityResult().
                                val rae = e as ResolvableApiException
                                rae.startResolutionForResult(this@BaseActivity, REQUEST_CHECK_SETTINGS)
                            } catch (sie: IntentSender.SendIntentException) {
                                Log.i(TAG, "PendingIntent unable to execute request.")
                            }

                        }
                        LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                            val errorMessage = "Location settings are inadequate, and cannot be " + "fixed here. Fix in Settings."
                            Log.e(TAG, errorMessage)

                            Toast.makeText(this@BaseActivity, errorMessage, Toast.LENGTH_LONG).show()
                        }
                    }

                    updateLocationUI()
                }
    }

    fun stopLocationUpdates() {
        if (mFusedLocationClient != null && mLocationCallback != null) {
            mFusedLocationClient!!
                    .removeLocationUpdates(mLocationCallback)
                    .addOnCompleteListener(this) {

                    }
        }
    }

    fun getPickImageResultUri(data: Intent?): Uri? {
        var isCamera = true
        if (data != null) {
            val action = data.action
            isCamera = action != null && action == MediaStore.ACTION_IMAGE_CAPTURE
        }


        return if (isCamera) getCaptureImageOutputUri() else data!!.data
    }

    fun getCaptureImageOutputUri(): Uri? {
        var outputFileUri: Uri? = null
        val getImage = externalCacheDir
        if (getImage != null) {
            outputFileUri = Uri.fromFile(File(getImage.path, "profile.png"))
        }
        return outputFileUri
    }

    fun getResizedMainBitmap(image: Bitmap, maxSize: Int): Bitmap {
        var width = image.width
        var height = image.height

        val bitmapRatio = width.toFloat() / height.toFloat()
        if (bitmapRatio > 0) {
            width = maxSize
            height = (width / bitmapRatio).toInt()
        } else {
            height = maxSize
            width = (height * bitmapRatio).toInt()
        }
        return Bitmap.createScaledBitmap(image, width, height, true)
    }

    open fun isValidEmail(target: CharSequence?): Boolean {
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches()
    }
    fun captureInfo(email: String, resultListner: ResultListner) {
        try {
            val dialog = AppCompatDialog(this, R.style.AppCompatAlertDialogStyle)
            dialog.supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
            val view = LayoutInflater.from(this).inflate(R.layout.email_capture, null)
            val email1 = view.findViewById<EditText>(R.id.email1)
            val email2 = view.findViewById<EditText>(R.id.email2)
            val email3 = view.findViewById<EditText>(R.id.email3)
            val email4 = view.findViewById<EditText>(R.id.email4)
            val email5 = view.findViewById<EditText>(R.id.email5)
            email1.setText(email)

            dialog.setCancelable(true)
            dialog.setCanceledOnTouchOutside(true)
            val btnSubmit = view.findViewById<Button>(R.id.btnSubmit)
//            val btnSkip = view.findViewById<Button>(R.id.btnSkip)
//            btnSkip.setOnClickListener {
//                resultListner.onResultListner(true, false)
//
//            }

            btnSubmit.setOnClickListener {
                val pref: SharedPreferences = applicationContext.getSharedPreferences("MyPref", 0)

                val editor: SharedPreferences.Editor = pref.edit()
                pref.edit().remove(PreferenceUtils.EMAIL).commit();
                pref.edit().remove(PreferenceUtils.EMAIL2).commit();
                pref.edit().remove(PreferenceUtils.EMAIL3).commit();
                pref.edit().remove(PreferenceUtils.EMAIL4).commit();
                pref.edit().remove(PreferenceUtils.EMAIL5).commit();

                preferenceUtils.removeFromPreference(PreferenceUtils.EMAIL)
                preferenceUtils.removeFromPreference(PreferenceUtils.EMAIL2)
                preferenceUtils.removeFromPreference(PreferenceUtils.EMAIL3)
                preferenceUtils.removeFromPreference(PreferenceUtils.EMAIL4)
                preferenceUtils.removeFromPreference(PreferenceUtils.EMAIL5)

                var mail1 = email1.text.toString()


//                if (!isValidEmail(mail1)) {
//                    showToast("Please enter a valid email")
//                } else {
                preferenceUtils.saveString(PreferenceUtils.EMAIL, mail1)
                editor.putString(PreferenceUtils.EMAIL, mail1);
                editor.apply()
                if (email2.text.toString().isNotEmpty()&&isValidEmail(email2.text.toString())) {
                    var mail2 = email2.text.toString()
                    preferenceUtils.saveString(PreferenceUtils.EMAIL2, mail2)
                    editor.putString(PreferenceUtils.EMAIL2, mail2);
                    editor.apply()
                }
                if (email3.text.toString().isNotEmpty()) {
                    var mail3 = email3.text.toString()
                    preferenceUtils.saveString(PreferenceUtils.EMAIL3, mail3)
                    editor.putString(PreferenceUtils.EMAIL3, mail3);
                    editor.apply()
                }
                if (email4.text.toString().isNotEmpty()) {
                    var mail4 = email4.text.toString()
                    preferenceUtils.saveString(PreferenceUtils.EMAIL4, mail4)
                    editor.putString(PreferenceUtils.EMAIL4, mail4);
                    editor.apply()
                }
                if (email5.text.toString().isNotEmpty()) {
                    var mail5 = email5.text.toString()
                    preferenceUtils.saveString(PreferenceUtils.EMAIL5, mail5)
                    editor.putString(PreferenceUtils.EMAIL5, mail5);
                    editor.apply()
                }
                dialog.dismiss()
                resultListner.onResultListner(true, true)
//                }
            }


            dialog.setContentView(view)
            if (!dialog.isShowing)
                dialog.show()
        } catch (e: Exception) {
        }

    }

    fun checkemail(resultListner: ResultListner){
        val pref = applicationContext.getSharedPreferences("MyPref", 0)
        val email1 = pref.getString(PreferenceUtils.EMAIL, "")
        val email2 = pref.getString(PreferenceUtils.EMAIL2, "")
        val email3 = pref.getString(PreferenceUtils.EMAIL3, "")
        val email4 = pref.getString(PreferenceUtils.EMAIL4, "")
        val email5 = pref.getString(PreferenceUtils.EMAIL5, "")
        var mainEMail = ""
        if (!email1!!.isEmpty()) {
            mainEMail = "$email1,"
        }
        if (!email2!!.isEmpty()) {
            mainEMail = "$mainEMail$email2,"
        }
        if (!email3!!.isEmpty()) {
            mainEMail = "$mainEMail$email3,"
        }
        if (!email4!!.isEmpty()) {
            mainEMail = "$mainEMail$email4,"
        }
        if (!email5!!.isEmpty()) {
            mainEMail = mainEMail + email5
        }
        if (mainEMail.length > 0) {
            resultListner.onResultListner(true, true)
        }
        else
        {

            showToast("Please provide email address")
            resultListner.onResultListner(true, false)
        }
    }

}

