package com.tbs.brothersgas.haadhir.Activitys

import android.content.Intent
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.brothersgas.haadhir.Adapters.InvoiceAdapter
import com.tbs.brothersgas.haadhir.Adapters.ReasonListAdapter
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryMainDO
import com.tbs.brothersgas.haadhir.Model.LoadStockDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import java.util.ArrayList
import android.widget.Toast
import com.tbs.brothersgas.haadhir.Adapters.SingleReasonListAdapter
import com.tbs.brothersgas.haadhir.Requests.*


class SkipActivity : BaseActivity() {
    lateinit var invoiceAdapter: InvoiceAdapter
    lateinit var loadStockDOs: ArrayList<LoadStockDO>
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var tvNoDataFound: TextView
    var fromId = 0
    lateinit var tvSelection: TextView
    lateinit var llSelectReason: LinearLayout
    lateinit var reasonListAdapter: SingleReasonListAdapter
    lateinit var date: String
    var reason: String = ""
    lateinit var btnConfirm: Button
    lateinit var btnRequestApproval: Button
    lateinit var tvApprovalStatus: TextView
    private var isSentForApproval: Boolean = false

    override protected fun onResume() {
        super.onResume()
    }

    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.skip_list, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()
        ivRefresh.visibility = View.VISIBLE

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            if (isSentForApproval) {
                showToast("Please process the order..!")
            } else {
                finish()
            }
        }
    }

    fun updateReqApprButton(selectedreason: String) {
        reason = selectedreason
        btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
        btnConfirm.isClickable = false
        btnConfirm.isEnabled = false
        btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_green))
        btnRequestApproval.isClickable = true
        btnRequestApproval.isEnabled = true
    }

    override fun initializeControls() {
        tvScreenTitle.setText(R.string.skip)
        recycleview = findViewById(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        recycleview.setLayoutManager(linearLayoutManager)
        tvNoDataFound = findViewById(R.id.tvNoDataFound) as TextView
        btnConfirm = findViewById(R.id.btnConfirm) as Button
        btnRequestApproval = findViewById(R.id.btnRequestApproval) as Button
        tvApprovalStatus = findViewById(R.id.tvApprovalStatus) as TextView
        btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
        btnConfirm.isClickable = false
        btnConfirm.isEnabled = false
        var tvShipment = findViewById(R.id.tvShipment) as TextView
        tvShipment.setText("Delivery - "+preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT,""))

//        invoiceAdapter = InvoiceAdapter(this@SalesInvoiceActivity,  null)
//        recycleview.setAdapter(invoiceAdapter)
//
        btnRequestApproval.setOnClickListener {
            if (reason.isEmpty()) {
                showToast("Please select reason..!")

            } else {
                approvalMechanismRequst("", reason, 5)
            }

        }
        ivRefresh.setOnClickListener {
            if (tvApprovalStatus.getText().toString().contains("Requested")) {
                preferenceUtils.saveString(PreferenceUtils.PRODUCT_APPROVAL, tvApprovalStatus.getText().toString())
//                updateStatus()
                approvalMechanismCheckStatus()
            }
        }
        btnConfirm.setOnClickListener {
            skipShipment()
        }
        if (Util.isNetworkAvailable(this)) {

            val reasonsListRequest = ReasonsListRequest(5, this@SkipActivity)
            reasonsListRequest.setOnResultListener { isError, reasonMainDo ->
                hideLoader()
                if (isError) {
                    tvNoDataFound.setVisibility(View.VISIBLE)
                    recycleview.setVisibility(View.GONE)
                    Toast.makeText(this@SkipActivity, R.string.error_NoData, Toast.LENGTH_SHORT).show()
                } else {

                    if (reasonMainDo.reasonDOS.size > 0) {
                        tvNoDataFound.setVisibility(View.GONE)
                        recycleview.setVisibility(View.VISIBLE)
                        reasonListAdapter = SingleReasonListAdapter(5, this@SkipActivity, reasonMainDo.reasonDOS)
                        recycleview!!.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this@SkipActivity)

                        recycleview!!.adapter = reasonListAdapter

                    } else {
                        tvNoDataFound.setVisibility(View.VISIBLE)
                        recycleview.setVisibility(View.GONE)
                    }


                }
            }
            reasonsListRequest.execute()
        } else {
            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "", false)

        }


    }
    private fun skipShipment() {
        if (Util.isNetworkAvailable(this)) {
            val siteListRequest = SkipShipmentRequest(this@SkipActivity)
            siteListRequest.setOnResultListener { isError, loginDo ->
                hideLoader()
                if (loginDo != null) {
                    if (isError) {
                        hideLoader()
                        showAppCompatAlert("Alert!", resources.getString(R.string.server_error), "OK", "", "",false)
                    } else {
                        hideLoader()
                        if (loginDo.flag == 2) {
                            showToast("Delivery has skipped")
//                            val skipShipmentList = StorageManager.getInstance(this).getSkipShipmentList(this)
//                            skipShipmentList.add(shipmentId)
//                            StorageManager.getInstance(this).saveSkipShipmentList(this, skipShipmentList)
                            StorageManager.getInstance(this).deleteActiveDeliveryMainDo(this)
                            StorageManager.getInstance(this).deleteDepartureData(this)
                            StorageManager.getInstance(this).deleteCurrentDeliveryItems(this)
                            StorageManager.getInstance(this).deleteReturnCylinders(this)
                            preferenceUtils.removeFromPreference(PreferenceUtils.SHIPMENT)
                            preferenceUtils.removeFromPreference(PreferenceUtils.DELIVERY_NOTE)
                            preferenceUtils.removeFromPreference(PreferenceUtils.DELIVERY_NOTE_RESHEDULE)
                            preferenceUtils.removeFromPreference(PreferenceUtils.PRODUCT_APPROVAL);
                            preferenceUtils.removeFromPreference(PreferenceUtils.RESCHEDULE_APPROVAL);
                            preferenceUtils.removeFromPreference(PreferenceUtils.INVOICE_ID);
                            preferenceUtils.removeFromPreference(PreferenceUtils.PAYMENT_ID);
//                            preferenceUtils.removeFromPreference(PreferenceUtils.SHIPMENT_ID);
                            val intent = Intent();
                            intent.putExtra("SkipShipment", "Skip")
                            setResult(52, intent)
                            finish()

                        } else {
                            showToast("Delivery not skipped")
                        }

                    }
                } else {
                    hideLoader()
                }

            }

            siteListRequest.execute()
        } else {
            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "",false)

        }


    }

    private fun approvalMechanismCheckStatus() {
        val approvalRequest = ApprovalMechanismCheckStatusRequest( this)
        approvalRequest.setOnResultListener { isError, approvalDO ->
            hideLoader()
            if (approvalDO != null) {
                if (isError) {
                    Toast.makeText(this@SkipActivity, "Approval under progress", Toast.LENGTH_SHORT).show()
                } else {
//                    approvalDO.status = "Approved"
                    if (approvalDO.flag==20) {
//                        loadDeliveryData()
                        isSentForApproval = true;
                        tvApprovalStatus.setText("Status : " + approvalDO.status)
                        btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
                        btnConfirm.isClickable = true
                        btnConfirm.isEnabled = true
                        btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        btnRequestApproval.isClickable = false
                        btnRequestApproval.isEnabled = false


                    } else if (approvalDO.flag==30) {
//                        rejectProducts()
                        isSentForApproval = false;
//                        loadDeliveryData()
                        tvApprovalStatus.setText("Status : " + approvalDO.status)
                        btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        btnConfirm.isClickable = false
                        btnConfirm.isEnabled = false
                        btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        btnRequestApproval.isClickable = false
                        btnRequestApproval.isEnabled = false
                        android.os.Handler().postDelayed({
                            finish()
                        }, 5000)

                    } else {
                        Toast.makeText(this@SkipActivity, approvalDO.status, Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
        approvalRequest.execute()
    }


    private fun approvalMechanismRequst(rescheduleDate: String, comments: String, docType: Int) {
        if (Util.isNetworkAvailable(this)) {
            val appUser = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "")
            val shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
            val stransactionID = preferenceUtils.getStringFromPreference(PreferenceUtils.DOC_NUMBER, "")
            var activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)


            val approvalMechanismRequst = ApprovalMechanismRequestedRequest(stransactionID, appUser, activeDeliverySavedDo.customer
                    , activeDeliverySavedDo.site, shipmentId, rescheduleDate, comments, docType, activeDeliverySavedDo.activeDeliveryDOS, this@SkipActivity)
            approvalMechanismRequst.setOnResultListener { isError, approveDO ->
                hideLoader()
                if (approveDO != null) {
                    if (isError) {
                        Log.e("RequestAproval", "Response : " + isError)
                    } else {
                        btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        btnRequestApproval.isClickable = false
                        btnRequestApproval.isEnabled = false
                        if (approveDO.flag == 20) {
                            tvApprovalStatus.setText("Status : Requested")
                            isSentForApproval = true;
                            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                            btnRequestApproval.isClickable = false
                            btnRequestApproval.isEnabled = false
                            reasonListAdapter!!.isClickable = false
                            preferenceUtils.saveString(PreferenceUtils.REQUESTED_NUMBER, approveDO.requestedNumber)

                        } else {
                            Toast.makeText(this@SkipActivity, approveDO.message, Toast.LENGTH_SHORT).show()
                            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_green))
                            btnRequestApproval.isClickable = true
                            btnRequestApproval.isEnabled = true
                            tvApprovalStatus.setText("Status : Request Not Sent")
                        }

                    }
                }
            }
            approvalMechanismRequst.execute()
        } else {
            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "", false)

        }


    }

}