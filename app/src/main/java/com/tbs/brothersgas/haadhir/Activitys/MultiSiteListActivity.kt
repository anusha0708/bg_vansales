package com.tbs.brothersgas.haadhir.Activitys

import android.content.Intent
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.brothersgas.haadhir.Adapters.AddressAdapter
import com.tbs.brothersgas.haadhir.Adapters.InvoiceAdapter
import com.tbs.brothersgas.haadhir.Model.CustomerDo
import com.tbs.brothersgas.haadhir.Model.LoadStockDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.AddressListRequest
import com.tbs.brothersgas.haadhir.Requests.InvoiceHistoryRequest
import com.tbs.brothersgas.haadhir.common.AppConstants
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import java.util.ArrayList
import androidx.core.os.HandlerCompat.postDelayed
import com.tbs.brothersgas.haadhir.Adapters.MultipleSiteAdapter
import com.tbs.brothersgas.haadhir.Requests.MultipleSiteListRequest
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.listeners.ResultListner
import com.tbs.brothersgas.haadhir.utils.CalendarUtils
import com.tbs.brothersgas.haadhir.utils.Constants
import kotlinx.android.synthetic.main.base_layout.*
import kotlinx.android.synthetic.main.multisite_list.*
import java.util.logging.Handler


class MultiSiteListActivity : BaseActivity() {
    lateinit var adapter: MultipleSiteAdapter
    lateinit var recycleview: RecyclerView
    lateinit var tvNoDataFound: TextView
    var fromId = 0
    lateinit var code: String
    lateinit var customer: String
    lateinit var customerDo: CustomerDo

    override protected fun onResume() {
        super.onResume()
    }

    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.multisite_list, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
    }

    override fun initializeControls() {
        tvScreenTitle.setText("Select Site")
        recycleview = findViewById(R.id.recycleview) as RecyclerView
        val linearLayoutManager = LinearLayoutManager(this)
        tvNoDataFound = findViewById(R.id.tvNoDataFound) as TextView
        recycleview.setLayoutManager(linearLayoutManager)
        recycleview.layoutManager = LinearLayoutManager(this@MultiSiteListActivity)
        tvShowAll.setOnClickListener {
            val intent = Intent()
            setResult(1, intent)
            finish()
        }




        if (Util.isNetworkAvailable(this)) {

            val request = MultipleSiteListRequest(this@MultiSiteListActivity)
            request.setOnResultListener { isError, siteMainDO ->
                hideLoader()
                if (isError) {
                    tvNoDataFound.setVisibility(View.VISIBLE)
                    recycleview.setVisibility(View.GONE)
                    tvClear.visibility = View.GONE
                    Toast.makeText(this@MultiSiteListActivity, R.string.error_NoData, Toast.LENGTH_SHORT).show()
                } else {
                    if (siteMainDO.finantialSiteDOS.size > 0) {
                        if (!intent.getBooleanExtra(Constants.IS_FROM_DELIVERY, false)) {
                            tvClear.visibility = View.VISIBLE
                        }
                        adapter = MultipleSiteAdapter(this@MultiSiteListActivity, siteMainDO.finantialSiteDOS, ResultListner { `object`, isSuccess ->

                            val intent = getIntent();
                            intent.putExtra("site_id", `object`.toString());
                            setResult(RESULT_OK, intent);
                            finish();
                        })
                        recycleview.adapter = adapter

                        tvClear.setOnClickListener {
                            val intent = getIntent();
                            intent.putExtra("site_id", "clear");
                            setResult(RESULT_OK, intent);
                            finish();
                        }
                    } else {
                        tvNoDataFound.setVisibility(View.VISIBLE)
                        recycleview.setVisibility(View.GONE)
                        tvClear.visibility = View.GONE
                    }
                }
            }
            request.execute()
        } else {
            showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 63 && resultCode == 63) {
            finish()
        } else if (requestCode == 63 && resultCode == 19) {
            if (data != null && data.hasExtra("CapturedReturns") && data.getBooleanExtra("CapturedReturns", false)) {

                val intent = Intent()
                intent.putExtra("CapturedReturns", AppConstants.CapturedReturns)
                setResult(19, intent)
                finish()
                // uncomment if required
            }
        }
    }

    override fun onButtonYesClick(from: String) {

        if ("SUCCESS".equals(from, ignoreCase = true)) {
            finish()

        } else if ("CREDIT_SUCCESS".equals(from, ignoreCase = true)) {
            StorageManager.getInstance(this).deleteActiveDeliveryMainDo(this)
            StorageManager.getInstance(this).deleteDepartureData(this)
            (this).preferenceUtils.removeFromPreference(PreferenceUtils.CustomerId)
            StorageManager.getInstance(this).deleteCurrentSpotSalesCustomer(this)
            StorageManager.getInstance(this).deleteCurrentDeliveryItems(this)
            StorageManager.getInstance(this).deleteReturnCylinders(this)

            if (StorageManager.getInstance(this).saveCurrentSpotSalesCustomer(this, customerDo)) {
                val intent = Intent(this, ActiveDeliveryActivity::class.java)
                preferenceUtils.saveString(PreferenceUtils.ProductsType, AppConstants.FixedQuantityProduct)
                preferenceUtils.saveString(PreferenceUtils.ShipmentType, getResources().getString(R.string.checkin_non_scheduled))
                preferenceUtils.saveString(PreferenceUtils.CREDIT_FLAG, "CREDIT_CUSTOMER")
                startActivityForResult(intent, 63)
            }
        } else
            if ("FAILURE".equals(from, ignoreCase = true)) {
            }

    }

    override fun onButtonNoClick(from: String) {
        if ("CREDIT_SUCCESS".equals(from, ignoreCase = true)) {
            finish()
        }
        super.onButtonNoClick(from)
    }
}