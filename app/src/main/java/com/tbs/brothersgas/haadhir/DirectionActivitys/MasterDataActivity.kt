package com.tbs.brothersgas.pod.Activitys

import android.annotation.TargetApi
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.ScrollView
import android.widget.TextView
import com.tbs.brothersgas.haadhir.Activitys.*
import com.tbs.brothersgas.haadhir.R


class MasterDataActivity : BaseActivity() {


    override fun initialize() {
      var  llCategories = getLayoutInflater().inflate(R.layout.master_data_layout, null) as ScrollView
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        disableMenuWithBackButton()
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            finish()
//            overridePendingTransition(R.anim.exit, R.anim.enter)
        }
        initializeControls()

    }


    override fun initializeControls() {

        tvScreenTitle.setText("Master Data")
        var  llProducts = findViewById<View>(R.id.llProducts) as LinearLayout
        var  llCustomers = findViewById<View>(R.id.llCustomers) as LinearLayout
        var  llLocations = findViewById<View>(R.id.llLocations) as LinearLayout


        llCustomers.setOnClickListener {
            val intent = Intent(this@MasterDataActivity, MasterDataCustomerActivity::class.java)
            startActivity(intent)
        }
        llLocations.setOnClickListener {
            val intent = Intent(this@MasterDataActivity, MRLocationsActivity::class.java)
            startActivity(intent)
        }
        llProducts.setOnClickListener {
            val intent = Intent(this@MasterDataActivity, ProductActivity::class.java)
            startActivity(intent)
        }

    }
    override  protected fun onResume() {
        super.onResume()
    }


}