//package com.tbs.brothersgas.haadhir.Activitys;
//
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//
//import com.tbs.brothersgas.haadhir.Adapters.CustomerAdapter;
//import com.tbs.brothersgas.haadhir.Model.CustomerDo;
//import com.tbs.brothersgas.haadhir.R;
//import com.tbs.brothersgas.haadhir.Requests.CustomerNewRequest;
//import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;
//import com.tbs.brothersgas.haadhir.utils.Util;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class CustomerActivity extends BaseActivity {
//    private String userId, orderCode;
//    private PreferenceUtils preferenceUtils;
//    private RecyclerView recycleview;
//    private CustomerAdapter customerAdapter;
//    private RelativeLayout llOrderHistory;
//    private TextView tvNoOrders;
//    private List<CustomerDo> customerDos;
//
//
//    @Override
//    public void initialize() {
//        llOrderHistory = (RelativeLayout) getLayoutInflater().inflate(R.layout.customer_screen, null);
//        llBody.addView(llOrderHistory, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
//        initializeControls();
//        toolbar.setNavigationIcon(R.drawable.back);
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//            }
//        });
//
//        preferenceUtils = new PreferenceUtils(CustomerActivity.this);
//        recycleview.setLayoutManager(new LinearLayoutManager(CustomerActivity.this));
//        if (Util.isNetworkAvailable(this)) {
//            CustomerNewRequest driverListRequest = new CustomerNewRequest(CustomerActivity.this);
//            driverListRequest.setOnResultListener(new CustomerNewRequest.OnResultListener() {
//                @Override
//                public void onCompleted(boolean isError, ArrayList<CustomerDo> customerDOs) {
//                    if (isError) {
//                        Toast.makeText(CustomerActivity.this, R.string.error_customer_list, Toast.LENGTH_SHORT).show();
//                    } else {
//                        customerDos = customerDOs;
//                        customerAdapter = new CustomerAdapter(CustomerActivity.this, customerDos);
//                        recycleview.setAdapter(customerAdapter);
//
//                    }
//                }
//            });
//            driverListRequest.execute();
//        } else {
//            showAppCompatAlert("Alert!", getResources().getString(R.string.internet_connection), "OK", "", "",false);
//
//        }
//
//
//
//    }
//
//    @Override
//    public void initializeControls() {
//        recycleview = (RecyclerView) findViewById(R.id.recycleview);
//        tvNoOrders = (TextView) findViewById(R.id.tvNoOrders);
//        tvScreenTitle.setText(R.string.customer_list);
//        ivSearch.setVisibility(View.VISIBLE);
//
//    }
//
//}
