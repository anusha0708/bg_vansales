package com.tbs.brothersgas.haadhir.Activitys

import android.app.Activity
import android.app.DatePickerDialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.brothersgas.haadhir.Adapters.*
import com.tbs.brothersgas.haadhir.Model.*
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.UserActivityReportRequest
import com.tbs.brothersgas.haadhir.common.AppConstants
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.listeners.ResultListner
import com.tbs.brothersgas.haadhir.pdfs.PaymentReceiptPDF
import com.tbs.brothersgas.haadhir.prints.PrinterConnection
import com.tbs.brothersgas.haadhir.prints.PrinterConstants
import com.tbs.brothersgas.haadhir.utils.CalendarUtils
import com.tbs.brothersgas.haadhir.utils.Constants
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import kotlinx.android.synthetic.main.base_layout.*
import kotlinx.android.synthetic.main.include_loan_issue.*
import kotlinx.android.synthetic.main.include_loan_receipt.*
import kotlinx.android.synthetic.main.include_sales_summary.*
import kotlinx.android.synthetic.main.new_load_vansales_tab.*
import kotlinx.android.synthetic.main.user_aactivity_report.*
import java.util.*
import kotlin.collections.ArrayList


class UserActivityReportActivity : BaseActivity() {
    lateinit var invoiceAdapter: InvoiceAdapter
    lateinit var loadStockDOs: ArrayList<LoadStockDO>
    lateinit var sdRecycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var siRecycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var cashRecycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var chequeRecycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var productsRecycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var stockRecycleview: androidx.recyclerview.widget.RecyclerView

    lateinit var btnPrint: Button

    private var userActivityReportMainDO: UserActivityReportMainDO = UserActivityReportMainDO()

    lateinit var tvNoDataFound: TextView
    var fromId = 0
    var startDate = ""
    var endDate = ""
    var from = ""
    var to = ""
    lateinit var tvCashTotalAmount: TextView
    lateinit var tvSdTotalAmount: TextView
    lateinit var tvTotalSIAmount: TextView
    lateinit var tvTotalChequeAmount: TextView
    lateinit var tvTotalProductQuantity: TextView
    lateinit var tvTotalStockQuantity: TextView

    lateinit var tvSpotDeliveries: TextView
    lateinit var tvSalesInvoices: TextView
    lateinit var tvCashReciepts: TextView
    lateinit var tvChequeReciepts: TextView
    lateinit var tvProducts: TextView
    lateinit var tvStock: TextView

    private var cday: Int = 0
    private var cmonth: Int = 0
    private var cyear: Int = 0
    lateinit var tvTransactionDate: TextView
    lateinit var tvUserId: TextView
    lateinit var tvUserName: TextView
    lateinit var llSpotDeliveryItem: LinearLayout
    lateinit var llSalesInvoicesItem: LinearLayout
    lateinit var llCashItem: LinearLayout
    lateinit var llChequeItem: LinearLayout
    lateinit var llProducts: LinearLayout
    lateinit var llStock: LinearLayout

    lateinit var llSdQuantity: LinearLayout
    lateinit var llSIAmount: LinearLayout
    lateinit var llProductQuantity: LinearLayout
    lateinit var llStockQuantity: LinearLayout

    lateinit var llCashAmount: LinearLayout
    lateinit var llChequeAmount: LinearLayout
    lateinit var llUserId: LinearLayout
    lateinit var llName: LinearLayout
    lateinit var view1: View
    lateinit var view2: View
    lateinit var view3: View
    lateinit var view4: View
    lateinit var view5: View
    lateinit var view6: View
    lateinit var view7: View
    lateinit var view8: View
    lateinit var view9: View
    lateinit var view10: View
    lateinit var view11: View
    lateinit var view12: View
    lateinit var view13: View
    lateinit var view14: View
    lateinit var view15: View
    lateinit var view16: View
    private var site: String = ""

    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.user_aactivity_report, null) as View
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        site = preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID, "")

        initializeControls()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
    }

    override fun initializeControls() {
        val name = preferenceUtils.getStringFromPreference(PreferenceUtils.LOGIN_DRIVER_NAME, "")
        val id = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "")
        val svCode = preferenceUtils.getStringFromPreference(PreferenceUtils.CV_PLATE, "")
        val nvCode = preferenceUtils.getStringFromPreference(PreferenceUtils.NON_VEHICLE_CODE, "")
        sdRecycleview = findViewById(R.id.sdRecycleview) as androidx.recyclerview.widget.RecyclerView
        siRecycleview = findViewById(R.id.siRecycleview) as androidx.recyclerview.widget.RecyclerView
        cashRecycleview = findViewById(R.id.cashRecycleview) as androidx.recyclerview.widget.RecyclerView
        chequeRecycleview = findViewById(R.id.chequeRecycleview) as androidx.recyclerview.widget.RecyclerView
        productsRecycleview = findViewById(R.id.productsRecycleview) as androidx.recyclerview.widget.RecyclerView
        stockRecycleview = findViewById(R.id.stockRecycleview) as androidx.recyclerview.widget.RecyclerView
        view1 = findViewById(R.id.view1) as View
        view2 = findViewById(R.id.view2) as View
        view3 = findViewById(R.id.view3) as View
        view4 = findViewById(R.id.view4) as View
        view5 = findViewById(R.id.view5) as View
        view6 = findViewById(R.id.view6) as View
        view7 = findViewById(R.id.view7) as View
        view8 = findViewById(R.id.view8) as View
        view9 = findViewById(R.id.view9) as View
        view10 = findViewById(R.id.view10) as View
        view11 = findViewById(R.id.view11) as View
        view12 = findViewById(R.id.view12) as View
        view13 = findViewById(R.id.view13) as View
        view14 = findViewById(R.id.view14) as View
        view15 = findViewById(R.id.view15) as View
        view16 = findViewById(R.id.view16) as View
        tvNoDataFound = findViewById(R.id.tvNoData) as TextView
        tvTransactionDate = findViewById(R.id.tvTransactionDate) as TextView
        tvSdTotalAmount = findViewById(R.id.tvSdTotalAmount) as TextView
        tvTotalSIAmount = findViewById(R.id.tvTotalSIAmount) as TextView
        tvCashTotalAmount = findViewById(R.id.tvCashTotalAmount) as TextView
        tvTotalChequeAmount = findViewById(R.id.tvTotalChequeAmount) as TextView
        tvTotalProductQuantity = findViewById(R.id.tvTotalProductQuantity) as TextView
        tvTotalStockQuantity = findViewById(R.id.tvTotalStockQuantity) as TextView
        tvUserId = findViewById(R.id.tvUserId) as TextView
        tvUserName = findViewById(R.id.tvUserName) as TextView
        tvSpotDeliveries = findViewById(R.id.tvSpotDeliveries) as TextView
        tvSalesInvoices = findViewById(R.id.tvSalesInvoices) as TextView
        tvCashReciepts = findViewById(R.id.tvCashReciepts) as TextView
        tvChequeReciepts = findViewById(R.id.tvChequeReciepts) as TextView
        tvProducts = findViewById(R.id.tvProducts) as TextView
        tvStock = findViewById(R.id.tvStock) as TextView
        llUserId = findViewById(R.id.llUserId) as LinearLayout
        llName = findViewById(R.id.llName) as LinearLayout
        llSpotDeliveryItem = findViewById(R.id.llSpotDeliveryItem) as LinearLayout
        llSalesInvoicesItem = findViewById(R.id.llSalesInvoicesItem) as LinearLayout
        llCashItem = findViewById(R.id.llCashItem) as LinearLayout
        llChequeItem = findViewById(R.id.llChequeItem) as LinearLayout
        llProducts = findViewById(R.id.llProducts) as LinearLayout
        llStock = findViewById(R.id.llStock) as LinearLayout
        llSdQuantity = findViewById(R.id.llSdQuantity) as LinearLayout
        llSIAmount = findViewById(R.id.llSIAmount) as LinearLayout
        llCashAmount = findViewById(R.id.llCashAmount) as LinearLayout
        llChequeAmount = findViewById(R.id.llChequeAmount) as LinearLayout
        llProductQuantity = findViewById(R.id.llProductQuantity) as LinearLayout
        llStockQuantity = findViewById(R.id.llStockQuantity) as LinearLayout
        var linearLayoutManager1 = androidx.recyclerview.widget.LinearLayoutManager(this)
        var linearLayoutManager2 = androidx.recyclerview.widget.LinearLayoutManager(this)
        var linearLayoutManager3 = androidx.recyclerview.widget.LinearLayoutManager(this)
        var linearLayoutManager4 = androidx.recyclerview.widget.LinearLayoutManager(this)
        var linearLayoutManager5 = androidx.recyclerview.widget.LinearLayoutManager(this)
        var linearLayoutManager6 = androidx.recyclerview.widget.LinearLayoutManager(this)
        var linearLayoutManager7 = androidx.recyclerview.widget.LinearLayoutManager(this)
        var linearLayoutManager8 = androidx.recyclerview.widget.LinearLayoutManager(this)
        var linearLayoutManager9 = androidx.recyclerview.widget.LinearLayoutManager(this)

        tvScreenTitle.maxLines = 2
        tvScreenTitle.setSingleLine(false)
        if (svCode.isNotEmpty()) {
            tvScreenTitle.setText(name + " \n Vehicle Number - " + svCode)
        } else {
            tvScreenTitle.setText(name + " \n Vehicle Number - " + nvCode)
        }
        tvUserId.setText(id)
        tvUserName.setText(name)
        var from = CalendarUtils.getDate()
        val aMonth = from.substring(4, 6)
        val ayear = from.substring(0, 4)
        val aDate = from.substring(Math.max(from.length - 2, 0))


        tvTransactionDate.setText("" + aDate + " - " + aMonth + " - " + ayear)
        startDate = ayear + aMonth + aDate
        endDate = ayear + aMonth + aDate

        sdRecycleview.setNestedScrollingEnabled(false);
        siRecycleview.setNestedScrollingEnabled(false);
        cashRecycleview.setNestedScrollingEnabled(false);
        chequeRecycleview.setNestedScrollingEnabled(false);
        productsRecycleview.setNestedScrollingEnabled(false);
        stockRecycleview.setNestedScrollingEnabled(false);
        salesRecycleview.setNestedScrollingEnabled(false);
        issueRecycleview.setNestedScrollingEnabled(false);
        recieptRecycleview.setNestedScrollingEnabled(false);



        sdRecycleview.setLayoutManager(linearLayoutManager1)
        siRecycleview.setLayoutManager(linearLayoutManager2)
        cashRecycleview.setLayoutManager(linearLayoutManager3)
        chequeRecycleview.setLayoutManager(linearLayoutManager4)
        productsRecycleview.setLayoutManager(linearLayoutManager5)
        stockRecycleview.setLayoutManager(linearLayoutManager6)
        salesRecycleview.setLayoutManager(linearLayoutManager7)
        issueRecycleview.setLayoutManager(linearLayoutManager8)
        recieptRecycleview.setLayoutManager(linearLayoutManager9)

        btnPrint = findViewById(R.id.btnPrint) as Button
        val btnSearch = findViewById(R.id.btnSearch) as Button

        btnSearch.setOnClickListener {
            userActivityReportMainDO.spotDeliveryDOS.clear()
            userActivityReportMainDO.salesInvoiceDOS.clear()
            userActivityReportMainDO.cashDOS.clear()
            userActivityReportMainDO.chequeDOS.clear()

            var frm = tvTransactionDate.text.toString().trim();
            if (frm.isNotEmpty()) {
                selectActivityList(0)
            } else {
                showToast("Please select From and To dates")
            }

        }
        val c = Calendar.getInstance()
        val cyear = c.get(Calendar.YEAR)
        val cmonth = c.get(Calendar.MONTH)
        val cday = c.get(Calendar.DAY_OF_MONTH)
        tvTransactionDate.setOnClickListener {
            val datePicker = DatePickerDialog(this@UserActivityReportActivity, datePickerListener, cyear, cmonth, cday)

            datePicker.show()
        }



        btnPrint.setOnClickListener {

            prepareActivityReport()
        }


        ll_fillter.setOnClickListener {
            val intent = Intent(this, MultiSiteListActivity::class.java)
            intent.putExtra(Constants.IS_FROM_DELIVERY, false)
            startActivityForResult(intent, 10)

        }

    }

    private fun selectActivityList(flag: Int) {

        if (flag == 0) {
            site = ""
        }
        if (Util.isNetworkAvailable(this)) {
            var userActivityReportActivity = UserActivityReportRequest(startDate, this@UserActivityReportActivity, site)

            userActivityReportActivity.setOnResultListener { isError, userActivityMainDO ->
                hideLoader()

                if (isError) {
                    tvSpotDeliveries.visibility = View.GONE
                    llSpotDeliveryItem.visibility = View.GONE
                    sdRecycleview.visibility = View.GONE
                    llSdQuantity.visibility = View.GONE
                    view1.visibility = View.GONE
                    view2.visibility = View.GONE
                    tvSalesInvoices.visibility = View.GONE
                    llSalesInvoicesItem.visibility = View.GONE
                    siRecycleview.visibility = View.GONE
                    llSIAmount.visibility = View.GONE
                    view3.visibility = View.GONE
                    view4.visibility = View.GONE
                    tvCashReciepts.visibility = View.GONE
                    llCashItem.visibility = View.GONE
                    cashRecycleview.visibility = View.GONE
                    llCashAmount.visibility = View.GONE
                    view5.visibility = View.GONE
                    view6.visibility = View.GONE
                    tvChequeReciepts.visibility = View.GONE
                    llChequeAmount.visibility = View.GONE
                    llChequeItem.visibility = View.GONE
                    chequeRecycleview.visibility = View.GONE
                    view7.visibility = View.GONE
                    view8.visibility = View.GONE
                    view9.visibility = View.GONE
                    view10.visibility = View.GONE
                    tvProducts.visibility = View.GONE
                    llProductQuantity.visibility = View.GONE
                    llProducts.visibility = View.GONE
                    productsRecycleview.visibility = View.GONE
                    view11.visibility = View.GONE
                    view12.visibility = View.GONE
                    view13.visibility = View.GONE
                    tvStock.visibility = View.GONE
                    llStockQuantity.visibility = View.GONE
                    llStock.visibility = View.GONE
                    stockRecycleview.visibility = View.GONE
                    view14.visibility = View.GONE
                    view15.visibility = View.GONE
                    view16.visibility = View.GONE
                    btnPrint.visibility = View.GONE
                    llLoanIssue.visibility = View.GONE
                    llLoanReciept.visibility = View.GONE
                    llSaleSummary.visibility = View.GONE

                    tvNoDataFound.visibility = View.VISIBLE
                    Toast.makeText(this@UserActivityReportActivity, resources.getString(R.string.server_error), Toast.LENGTH_SHORT).show()
                } else {
                    if (preferenceUtils.getbooleanFromPreference(PreferenceUtils.IS_MULTI_SITE, false)) {
                        ll_fillter.visibility = View.VISIBLE
//                        btnShowAll.visibility = View.VISIBLE
                    } else {
                        ll_fillter.visibility = View.GONE
                    }

                    userActivityReportMainDO = userActivityMainDO
                    if (userActivityMainDO != null) {
                        if (userActivityReportMainDO.spotDeliveryDOS.size > 0 || userActivityReportMainDO.salesInvoiceDOS.size > 0 ||
                                userActivityReportMainDO.cashDOS.size > 0 || userActivityReportMainDO.chequeDOS.size > 0 ||
                                userActivityReportMainDO.productDOS.size > 0 || userActivityReportMainDO.stockDOS.size > 0 ||
                                userActivityReportMainDO.salesDOS.size > 0 || userActivityReportMainDO.issueDOS.size > 0 ||
                                userActivityReportMainDO.recieptDOS.size > 0
                        ) {
                            tvNoDataFound.visibility = View.GONE

                            if (userActivityReportMainDO.spotDeliveryDOS != null && userActivityReportMainDO.spotDeliveryDOS.size > 0) {
                                tvSpotDeliveries.visibility = View.VISIBLE
                                llSpotDeliveryItem.visibility = View.VISIBLE
                                sdRecycleview.visibility = View.VISIBLE
                                llSdQuantity.visibility = View.VISIBLE
                                view1.visibility = View.VISIBLE
                                view2.visibility = View.VISIBLE
                                btnPrint.visibility = View.VISIBLE
                                var siteAdapter = UserActivityDeliveryAdapter(this@UserActivityReportActivity, userActivityReportMainDO.spotDeliveryDOS)
                                sdRecycleview.setAdapter(siteAdapter)
                                tvSdTotalAmount.setText("Total : " + userActivityMainDO.deliveryTotalQty + " " + userActivityReportMainDO.spotDeliveryDOS.get(0).productUnit)

                            } else {
                                tvSpotDeliveries.visibility = View.GONE
                                llSpotDeliveryItem.visibility = View.GONE
                                sdRecycleview.visibility = View.GONE
                                llSdQuantity.visibility = View.GONE
                                view1.visibility = View.GONE
                                view2.visibility = View.GONE

                            }

                            if (userActivityReportMainDO.salesInvoiceDOS != null && userActivityReportMainDO.salesInvoiceDOS.size > 0) {
                                tvSalesInvoices.visibility = View.VISIBLE
                                llSalesInvoicesItem.visibility = View.VISIBLE
                                siRecycleview.visibility = View.VISIBLE
                                llSIAmount.visibility = View.VISIBLE
                                view3.visibility = View.VISIBLE
                                view4.visibility = View.VISIBLE
                                view5.visibility = View.VISIBLE
                                btnPrint.visibility = View.VISIBLE
                                tvTotalSIAmount.setText("Total : " + userActivityMainDO.invoiceTotalAmount + " AED")

                                var siteAdapter = UserActivityInvoiceAdapter(this@UserActivityReportActivity, userActivityReportMainDO.salesInvoiceDOS)
                                siRecycleview.setAdapter(siteAdapter)
                            } else {
                                tvSalesInvoices.visibility = View.GONE
                                llSalesInvoicesItem.visibility = View.GONE
                                siRecycleview.visibility = View.GONE
                                llSIAmount.visibility = View.GONE
                                view3.visibility = View.GONE
                                view4.visibility = View.GONE
                                view5.visibility = View.GONE
                            }
                            if (userActivityReportMainDO.cashDOS != null && userActivityReportMainDO.cashDOS.size > 0) {
                                tvCashReciepts.visibility = View.VISIBLE
                                llCashItem.visibility = View.VISIBLE
                                cashRecycleview.visibility = View.VISIBLE
                                llCashAmount.visibility = View.VISIBLE
                                view6.visibility = View.VISIBLE
                                view7.visibility = View.VISIBLE
                                btnPrint.visibility = View.VISIBLE
                                tvCashTotalAmount.setText("Total : " + userActivityMainDO.cashTotalAmount + " AED")

                                var siteAdapter = UserActivityCashAdapter(this@UserActivityReportActivity, userActivityReportMainDO.cashDOS)
                                cashRecycleview.setAdapter(siteAdapter)

                            } else {
                                tvCashReciepts.visibility = View.GONE
                                llCashItem.visibility = View.GONE
                                cashRecycleview.visibility = View.GONE
                                llCashAmount.visibility = View.GONE
                                view6.visibility = View.GONE
                                view7.visibility = View.GONE

                            }
                            if (userActivityReportMainDO.chequeDOS != null && userActivityReportMainDO.chequeDOS.size > 0) {
                                tvChequeReciepts.visibility = View.VISIBLE
                                llChequeAmount.visibility = View.VISIBLE
                                llChequeItem.visibility = View.VISIBLE
                                chequeRecycleview.visibility = View.VISIBLE
                                view8.visibility = View.VISIBLE
                                view9.visibility = View.VISIBLE
                                view10.visibility = View.VISIBLE
                                btnPrint.visibility = View.VISIBLE
                                tvTotalChequeAmount.setText("Total : " + userActivityMainDO.chequeTotalAmount + " AED")

                                var siteAdapter = UserActivityChequeAdapter(this@UserActivityReportActivity, userActivityReportMainDO.chequeDOS)
                                chequeRecycleview.setAdapter(siteAdapter)

                            } else {
                                tvChequeReciepts.visibility = View.GONE
                                llChequeAmount.visibility = View.GONE
                                llChequeItem.visibility = View.GONE
                                chequeRecycleview.visibility = View.GONE
                                view8.visibility = View.GONE
                                view9.visibility = View.GONE
                                view10.visibility = View.GONE

                            }


                            if (userActivityReportMainDO.productDOS != null && userActivityReportMainDO.productDOS.size > 0) {
                                tvTotalProductQuantity.visibility = View.VISIBLE
                                tvProducts.visibility = View.VISIBLE
                                llProductQuantity.visibility = View.VISIBLE
                                llProducts.visibility = View.VISIBLE
                                productsRecycleview.visibility = View.VISIBLE
                                view11.visibility = View.VISIBLE
                                view12.visibility = View.VISIBLE
                                view13.visibility = View.VISIBLE
                                btnPrint.visibility = View.VISIBLE
                                tvTotalProductQuantity.setText("Total : " + userActivityMainDO.productsTotalQty + " " + userActivityReportMainDO.productDOS.get(0).stockUnit)

                                var siteAdapter = UserActivityProductAdapter(this@UserActivityReportActivity, userActivityReportMainDO.productDOS)
                                productsRecycleview.setAdapter(siteAdapter)

                            } else {
                                tvTotalProductQuantity.visibility = View.GONE
                                tvProducts.visibility = View.GONE
                                llProductQuantity.visibility = View.GONE
                                llProducts.visibility = View.GONE
                                productsRecycleview.visibility = View.GONE
                                view11.visibility = View.GONE
                                view12.visibility = View.GONE
                                view13.visibility = View.GONE

                            }

                            if (userActivityReportMainDO.stockDOS != null && userActivityReportMainDO.stockDOS.size > 0) {
                                tvStock.visibility = View.VISIBLE
                                llStockQuantity.visibility = View.VISIBLE
                                llStock.visibility = View.VISIBLE
                                stockRecycleview.visibility = View.VISIBLE
                                view14.visibility = View.VISIBLE
                                view15.visibility = View.VISIBLE
                                view16.visibility = View.VISIBLE
                                btnPrint.visibility = View.VISIBLE
                                tvTotalStockQuantity.setText("Total : " + userActivityMainDO.stockTotalQty + " " + userActivityReportMainDO.stockDOS.get(0).stockUnit)

                                var siteAdapter = UserActivityProductAdapter(this@UserActivityReportActivity, userActivityReportMainDO.stockDOS)
                                stockRecycleview.setAdapter(siteAdapter)

                            } else {
                                tvStock.visibility = View.GONE
                                llStockQuantity.visibility = View.GONE
                                llStock.visibility = View.GONE
                                stockRecycleview.visibility = View.GONE
                                view14.visibility = View.GONE
                                view15.visibility = View.GONE
                                view16.visibility = View.GONE
                            }

                            if (userActivityReportMainDO.salesDOS != null && userActivityReportMainDO.salesDOS.size > 0) {
                                llSaleSummary.visibility = View.VISIBLE
                                btnPrint.visibility = View.VISIBLE
                                tvTotalSalesQuantity.setText("Total : " + userActivityMainDO.salesTotalQty + " " + userActivityReportMainDO.salesDOS.get(0).stockUnit)

                                var siteAdapter = UserActivityProductAdapter(this@UserActivityReportActivity, userActivityReportMainDO.salesDOS)
                                salesRecycleview.setAdapter(siteAdapter)

                            } else {
                                llSaleSummary.visibility = View.GONE
                            }
                            if (userActivityReportMainDO.issueDOS != null && userActivityReportMainDO.issueDOS.size > 0) {
                                llLoanIssue.visibility = View.VISIBLE
                                btnPrint.visibility = View.VISIBLE
                                tvTotalIssueQuantity.setText("Total : " + userActivityMainDO.issueTotalQty + " " + userActivityReportMainDO.issueDOS.get(0).stockUnit)

                                var siteAdapter = UserActivityLoanAdapter(this@UserActivityReportActivity, userActivityReportMainDO.issueDOS)
                                issueRecycleview.setAdapter(siteAdapter)

                            } else {
                                llLoanIssue.visibility = View.GONE
                            }
                            if (userActivityReportMainDO.recieptDOS != null && userActivityReportMainDO.recieptDOS.size > 0) {
                                llLoanReciept.visibility = View.VISIBLE
                                btnPrint.visibility = View.VISIBLE
                                tvTotalRecieptQuantity.setText("Total : " + userActivityMainDO.recieptTotalQty + " " + userActivityReportMainDO.recieptDOS.get(0).stockUnit)

                                var siteAdapter = UserActivityLoanAdapter(this@UserActivityReportActivity, userActivityReportMainDO.recieptDOS)
                                recieptRecycleview.setAdapter(siteAdapter)

                            } else {
                                llLoanReciept.visibility = View.GONE
                            }
                        } else {
                            tvNoDataFound.visibility = View.VISIBLE
                            tvSpotDeliveries.visibility = View.GONE
                            llSpotDeliveryItem.visibility = View.GONE
                            sdRecycleview.visibility = View.GONE
                            llSdQuantity.visibility = View.GONE
                            view1.visibility = View.GONE
                            view2.visibility = View.GONE
                            tvSpotDeliveries.visibility = View.GONE

                            tvSalesInvoices.visibility = View.GONE
                            llSalesInvoicesItem.visibility = View.GONE
                            siRecycleview.visibility = View.GONE
                            llSIAmount.visibility = View.GONE
                            view3.visibility = View.GONE
                            view4.visibility = View.GONE

                            tvCashReciepts.visibility = View.GONE
                            llCashItem.visibility = View.GONE
                            cashRecycleview.visibility = View.GONE
                            llCashAmount.visibility = View.GONE
                            view5.visibility = View.GONE
                            view6.visibility = View.GONE

                            tvChequeReciepts.visibility = View.GONE
                            llChequeAmount.visibility = View.GONE
                            llChequeItem.visibility = View.GONE
                            chequeRecycleview.visibility = View.GONE
                            view7.visibility = View.GONE
                            view8.visibility = View.GONE
                            view9.visibility = View.GONE
                            view10.visibility = View.GONE


                            tvProducts.visibility = View.GONE
                            llProductQuantity.visibility = View.GONE
                            llProducts.visibility = View.GONE
                            productsRecycleview.visibility = View.GONE
                            view11.visibility = View.GONE
                            view12.visibility = View.GONE
                            view13.visibility = View.GONE

                            tvStock.visibility = View.GONE
                            llStockQuantity.visibility = View.GONE
                            llStock.visibility = View.GONE
                            stockRecycleview.visibility = View.GONE
                            view14.visibility = View.GONE
                            view15.visibility = View.GONE
                            view16.visibility = View.GONE


                            btnPrint.visibility = View.GONE
                            tvNoDataFound.visibility = View.VISIBLE
                            Toast.makeText(this@UserActivityReportActivity, resources.getString(R.string.no_data), Toast.LENGTH_SHORT).show()

                        }

                    } else {
                        tvSpotDeliveries.visibility = View.GONE
                        llSpotDeliveryItem.visibility = View.GONE
                        sdRecycleview.visibility = View.GONE
                        llSdQuantity.visibility = View.GONE
                        view1.visibility = View.GONE
                        view2.visibility = View.GONE

                        tvSalesInvoices.visibility = View.GONE
                        llSalesInvoicesItem.visibility = View.GONE
                        siRecycleview.visibility = View.GONE
                        llSIAmount.visibility = View.GONE
                        view3.visibility = View.GONE
                        view4.visibility = View.GONE

                        tvCashReciepts.visibility = View.GONE
                        llCashItem.visibility = View.GONE
                        cashRecycleview.visibility = View.GONE
                        llCashAmount.visibility = View.GONE
                        view5.visibility = View.GONE
                        view6.visibility = View.GONE

                        tvChequeReciepts.visibility = View.GONE
                        llChequeAmount.visibility = View.GONE
                        llChequeItem.visibility = View.GONE
                        chequeRecycleview.visibility = View.GONE
                        view7.visibility = View.GONE
                        view8.visibility = View.GONE
                        view9.visibility = View.GONE
                        view10.visibility = View.GONE

                        tvProducts.visibility = View.GONE
                        llProductQuantity.visibility = View.GONE
                        llProducts.visibility = View.GONE
                        productsRecycleview.visibility = View.GONE
                        view11.visibility = View.GONE
                        view12.visibility = View.GONE
                        view13.visibility = View.GONE

                        tvStock.visibility = View.GONE
                        llStockQuantity.visibility = View.GONE
                        llStock.visibility = View.GONE
                        stockRecycleview.visibility = View.GONE
                        view14.visibility = View.GONE
                        view15.visibility = View.GONE
                        view16.visibility = View.GONE

                        btnPrint.visibility = View.GONE
                        tvNoDataFound.visibility = View.VISIBLE
                        Toast.makeText(this@UserActivityReportActivity, resources.getString(R.string.no_data), Toast.LENGTH_SHORT).show()

                    }


                }


            }

            userActivityReportActivity.execute()
        } else {
//            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "",false)
            showToast("" + resources.getString(R.string.internet_connection))
        }

//


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == 10) {
            if (data != null) {
                site = data.getStringExtra("site_id")!!;

                selectActivityList(1)
            }
        } else {
            selectActivityList(0)

        }
    }


    private val datePickerListener: DatePickerDialog.OnDateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        cday = dayOfMonth
        cmonth = monthOfYear + 1
        cyear = year
        var monthString = cmonth.toString()
        if (monthString.length == 1) {
            monthString = "0$monthString"
        } else {
            monthString = cmonth.toString()
        }

        var dayString = cday.toString()
        if (dayString.length == 1) {
            dayString = "0$dayString"
        } else {
            dayString = cday.toString()
        }
        startDate = "" + cyear + monthString + dayString;
        tvTransactionDate.setText("" + dayString + " - " + monthString + " - " + cyear)
        from = tvTransactionDate.text.toString()

    }


    private fun prepareActivityReport() {

//        val objLlist = ArrayList<Serializable>();
//        objLlist.add(transactionCashMainDO);
//        objLlist.add(transactionChequeMainDO)
        AppConstants.Trans_Date = tvTransactionDate.text.toString().trim().replace("-", "/");
        printDocument(userActivityReportMainDO, PrinterConstants.PrintUserActivityReport)

    }


    private fun printDocument(obj: Any, from: Int) {
        val printConnection = PrinterConnection.getInstance(this@UserActivityReportActivity)
        if (printConnection.isBluetoothEnabled()) {
            printConnection.connectToPrinter(obj, from)
        } else {
//            PrinterConstants.PRINTER_MAC_ADDRESS = ""
            showToast("Please enable your mobile Bluetooth.")
//            showAppCompatAlert("", "Please enable your mobile Bluetooth.", "Enable", "Cancel", "EnableBluetooth", false)
        }
    }

    private fun pairedDevices() {
        val pairedDevices = BluetoothAdapter.getDefaultAdapter().getBondedDevices()
        if (pairedDevices.size > 0) {
            for (device in pairedDevices) {
                val deviceName = device.getName()
                val mac = device.getAddress() // MAC address
//                if (StorageManager.getInstance(this).getPrinterMac(this).equals("")) {
                StorageManager.getInstance(this).savePrinterMac(this, mac);
//                }
                Log.e("Bluetooth", "Name : " + deviceName + " Mac : " + mac)
                break
            }
        }
    }

    override fun onResume() {
        super.onResume()
        pairedDevices()
    }

    override fun onStart() {
        super.onStart()
        val filter = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        registerReceiver(mReceiver, filter)
//        val filter = IntentFilter(Intent.PAIRIN.ACTION_FOUND Intent. "android.bluetooth.device.action.PAIRING_REQUEST");
//        registerReceiver(mReceiver, filter)
//        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
//        mBluetoothAdapter.startDiscovery()
//        val filter2 = IntentFilter( "android.bluetooth.device.action.PAIRING_REQUEST")
//        registerReceiver(mReceiver, filter2)

    }

    override fun onDestroy() {
        unregisterReceiver(mReceiver)
//        PrinterConstants.PRINTER_MAC_ADDRESS = ""
        super.onDestroy()
    }

    private val mReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            if (BluetoothDevice.ACTION_FOUND == action) {
                val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE);
                PrinterConstants.bluetoothDevices.add(device)
                Log.e("Bluetooth", "Discovered => name : " + device!!.name + ", Mac : " + device.address)
            } else if (action.equals(BluetoothDevice.ACTION_ACL_CONNECTED)) {
                Log.e("Bluetooth", "status : ACTION_ACL_CONNECTED")
                pairedDevices()
            } else if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                val state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);

                if (state == BluetoothAdapter.STATE_OFF) {
                    Log.e("Bluetooth", "status : STATE_OFF")
                } else if (state == BluetoothAdapter.STATE_TURNING_OFF) {
                    Log.e("Bluetooth", "status : STATE_TURNING_OFF")
                } else if (state == BluetoothAdapter.STATE_ON) {
                    Log.e("Bluetooth", "status : STATE_ON")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_TURNING_ON) {
                    Log.e("Bluetooth", "status : STATE_TURNING_ON")
                } else if (state == BluetoothAdapter.STATE_CONNECTING) {
                    Log.e("Bluetooth", "status : STATE_CONNECTING")
                } else if (state == BluetoothAdapter.STATE_CONNECTED) {
                    Log.e("Bluetooth", "status : STATE_CONNECTED")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_DISCONNECTED) {
                    Log.e("Bluetooth", "status : STATE_DISCONNECTED")
                }
            }
        }
    }

    private fun createPaymentPDF(createPDFInvoiceDO: PaymentPdfDO) {
        if (createPDFInvoiceDO != null) {
            captureInfo(createPDFInvoiceDO.mail, ResultListner { `object`, isSuccess ->
                if (isSuccess) {
                    PaymentReceiptPDF(this).createReceiptPDF(createPDFInvoiceDO, "Email")
                }
            })
            return

        } else {
            //Display error message
        }
    }


}