package com.tbs.brothersgas.haadhir.Model;
/*
 * Created by developer on 5/3/18.
 */


import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;

/**
 * The type User current time.
 */
public class UserCurrentTime {
    @ServerTimestamp
    public Date currentTime;

    /**
     * Gets current time.
     *
     * @return the current time
     */
    public Date getCurrentTime() {
        return currentTime;
    }

    /**
     * Sets current time.
     *
     * @param currentTime the current time
     */
    public void setCurrentTime(Date currentTime) {
        this.currentTime = currentTime;
    }
}
