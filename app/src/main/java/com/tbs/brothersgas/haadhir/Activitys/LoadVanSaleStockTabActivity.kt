package com.tbs.brothersgas.haadhir.Activitys


import com.google.android.material.tabs.TabLayout
import androidx.viewpager.widget.ViewPager
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.brothersgas.haadhir.Adapters.LoadVanSaleStockPagerAdapter
import com.tbs.brothersgas.haadhir.Model.LoadStockDO
import com.tbs.brothersgas.haadhir.R
//import com.tbs.brothersgas.haadhir.Requests.LoadVanSaleRequest
import com.tbs.brothersgas.haadhir.Requests.NewScheduledLoadVanSaleRequest
import com.tbs.brothersgas.haadhir.Requests.NonScheduledLoadVanSaleRequest
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.utils.CalendarUtils
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import java.util.ArrayList

class LoadVanSaleStockTabActivity : BaseActivity() {
    private  var rlPartyActivity: LinearLayout? = null
    lateinit var adapter: LoadVanSaleStockPagerAdapter
    lateinit var viewPager: ViewPager
    lateinit var btnLoadVehicle: Button
    lateinit var tabLayout: TabLayout
    lateinit var tvCapacity: TextView
    lateinit var tvScheduledStock: TextView
    lateinit var tvNonScheduledStock: TextView
    lateinit var tvBalanceStock: TextView
    lateinit var tvReturnStock: TextView
    lateinit var llReturnStock: LinearLayout
    lateinit var btnLoadReject: Button
    private  var scheduledRootId : String = ""
    private  var nonScheduledRootId : String = ""

    //    lateinit var loadStockMainDO : LoadStockMainDO
    var flag:Int = 0

    private var scheduleDos: ArrayList<LoadStockDO> = ArrayList()
    private var nonScheduleDos: ArrayList<LoadStockDO> = ArrayList()
    private var availableStockDos: ArrayList<LoadStockDO> = ArrayList()

    override fun initialize() {
        rlPartyActivity = layoutInflater.inflate(R.layout.load_vansales_tab, null) as LinearLayout
        llBody.addView(rlPartyActivity, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        disableMenuWithBackButton()
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
        initializeControls()
        if(intent.hasExtra("ScreenTitle")){
            tvScreenTitle.setText(intent.extras.getString("ScreenTitle"))

        }
        else{
            tvScreenTitle.text = "Check Load";
        }
//        tabLayout.addTab(tabLayout.newTab().setText("Available Stock"))
        tabLayout.addTab(tabLayout.newTab().setText("Scheduled Stock"))
        tabLayout.addTab(tabLayout.newTab().setText("Non-Scheduled Stock"))

        tabLayout.tabGravity = TabLayout.GRAVITY_FILL
        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })
        if (intent.hasExtra("FLAG")) {
            flag = intent.extras.getInt("FLAG")
        }
        if(flag == 1){
            btnLoadVehicle.setVisibility(View.GONE)
            btnLoadReject.setVisibility(View.GONE)

        }else  if(flag == 2){
            btnLoadVehicle.setVisibility(View.VISIBLE)
            llReturnStock.setVisibility(View.VISIBLE)
            btnLoadVehicle.setText("UnLoad Vehicle")
            btnLoadReject.setVisibility(View.GONE)

        }
        else {
            btnLoadReject.setVisibility(View.GONE)
            btnLoadVehicle.setVisibility(View.VISIBLE)
        }
        val vehicleCheckInDo = StorageManager.getInstance(this@LoadVanSaleStockTabActivity).getVehicleCheckInData(this@LoadVanSaleStockTabActivity);
        scheduledRootId = vehicleCheckInDo.scheduledRootId!!
        nonScheduledRootId = vehicleCheckInDo.nonScheduledRootId!!

        if(!scheduledRootId.equals("", true)){
            scheduleDos = StorageManager.getInstance(this).getVanScheduleProducts(this);
        }
        if(!nonScheduledRootId.equals("", true)){
            nonScheduleDos = StorageManager.getInstance(this).getVanNonScheduleProducts(this);
        }
//        var scheduleTotal = 0.0;
//        var nonScheduleTotal = 0.0;
//        if(scheduleDos != null && scheduleDos!!.size>0){
//            for (j in scheduleDos!!.indices) {
//                scheduleTotal = scheduleDos!!.get(j).totalQuantity
//            }
//        }
//        if(nonScheduleDos != null && nonScheduleDos!!.size>0){
//            for (j in nonScheduleDos!!.indices) {
//                nonScheduleTotal = nonScheduleDos!!.get(j).totalQuantity
//            }
//        }
//        var totalCapacity = "";
//        var unit = preferenceUtils.getStringFromPreference(PreferenceUtils.UNIT_CAPACITY, "")
//
//        totalCapacity = preferenceUtils.getStringFromPreference(PreferenceUtils.TOTAL_VEHICLE_CAPACITY, "")
//
//        if (!totalCapacity.equals("")) {
//            var total = totalCapacity.toDouble()
//            var scheduledStock = preferenceUtils.getStringFromPreference(PreferenceUtils.TOTAL_SCHEDULED_STOCK, "")
//            var nonScheduledStock = preferenceUtils.getStringFromPreference(PreferenceUtils.TOTAL_NON_SCHEDULED_STOCK, "")
//            scheduleTotal = scheduledStock.toDouble()
//            if(nonScheduledStock!=null  && nonScheduledStock.length>0){
//
//                nonScheduleTotal = nonScheduledStock.toDouble()
//                tvCapacity.setText(totalCapacity +" " +unit)
//                tvScheduledStock.setText("" + scheduleTotal + " " + unit)
//                tvNonScheduledStock.setText("" + nonScheduleTotal + " " + unit)
//                tvBalanceStock.setText("" + (total - scheduleTotal - nonScheduleTotal) + " " + unit)
//            }else{
//                tvCapacity.setText(totalCapacity +" " +unit)
//                tvScheduledStock.setText("" + scheduleTotal + " " + unit)
//                tvNonScheduledStock.setText("" + 0 + " " + unit)
//                tvBalanceStock.setText("" + (total - scheduleTotal - 0) + " " + unit)
//            }
//
//
//        } else {
//            //tvBalanceStock.setText("" + (scheduleTotal - nonScheduleTotal) + " " + unit)
//
//        }

        if(scheduleDos == null || scheduleDos!!.size<1){
            loadVehicleStockData()
        }
        else if(nonScheduleDos == null || nonScheduleDos!!.size<1){
            loadNonScheduledVehicleStockData()
        }
        else{
//            var isProductExisted = false;
//            availableStockDos=nonScheduleDos
//            for (i in scheduleDos.indices) {
//                for (k in availableStockDos.indices) {
//                    if (scheduleDos.get(i).product.equals(availableStockDos!!.get(k).product,  true)) {
//                        availableStockDos!!.get(k).quantity= availableStockDos!!.get(k).quantity+scheduleDos.get(i).quantity
//                        isProductExisted = true
//                        break
//                    }
//                }
//                if (isProductExisted) {
//                    isProductExisted = false
//                    continue
//                } else {
//                    availableStockDos!!.add(scheduleDos.get(i))
//                }
//            }
            adapter = LoadVanSaleStockPagerAdapter(this, scheduleDos!!, nonScheduleDos!!, supportFragmentManager)
            viewPager.adapter = adapter
        }
        btnLoadVehicle.setOnClickListener {
            var id = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "")
            var nID = preferenceUtils.getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, "")

            if(id.isEmpty()&&nID.isEmpty()){
              showAlert("Stock Not Assigned to Driver")
              }else{

                  showAppCompatAlert("", "Are You Sure You Want to \n confirm load?", "OK", "Cancel", "SUCCESS", true)
              }


        }
    }

    override fun initializeControls() {
        tabLayout               = findViewById<View>(R.id.tab_layout) as TabLayout
        viewPager               = findViewById<View>(R.id.pager) as ViewPager
        btnLoadVehicle          = findViewById<Button>(R.id.btnLoadVehicle) as Button
        tvCapacity              = findViewById<View>(R.id.tvCapacity) as TextView
        tvScheduledStock        = findViewById<View>(R.id.tvScheduledStock) as TextView
        tvNonScheduledStock     = findViewById<View>(R.id.tvNonScheduledStock) as TextView
        tvBalanceStock          = findViewById<View>(R.id.tvBalanceStock) as TextView
        tvReturnStock          = findViewById<View>(R.id.tvReturnStock) as TextView
        llReturnStock          = findViewById<View>(R.id.llReturnedStock) as LinearLayout
        btnLoadReject          = findViewById<Button>(R.id.btnLoadReject) as Button
           btnLoadReject.setOnClickListener {
               setResult(10, null)
               finish()
           }
    }

    private fun loadVehicleStockData(){
        if(Util.isNetworkAvailable(this)) {
            if(!scheduledRootId.equals("", true)){
                val loadVanSaleRequest = NewScheduledLoadVanSaleRequest(scheduledRootId, this@LoadVanSaleStockTabActivity)
                showLoader()
                loadVanSaleRequest.setOnResultListener { isError, loadStockMainDo ->
                    hideLoader()
                    if (isError) {
                        showToast("No vehicle data found.")
                    }
                    else {
                        scheduleDos = loadStockMainDo.loadStockDOS;

                    }
                    loadNonScheduledVehicleStockData()
                }
                loadVanSaleRequest.execute()
            }
            else{
                loadNonScheduledVehicleStockData()
            }
        }
        else {
            showToast("No Internet connection, please try again later")
        }
    }

    private fun loadNonScheduledVehicleStockData(){
        if(Util.isNetworkAvailable(this)){

            var nonScheduledRootId = preferenceUtils.getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, "")

            if(!nonScheduledRootId.equals("", true)){
                val loadVanSaleRequest = NonScheduledLoadVanSaleRequest(nonScheduledRootId, this@LoadVanSaleStockTabActivity)
                showLoader()
                loadVanSaleRequest.setOnResultListener { isError, loadStockMainDo ->
                    hideLoader()
                    if (isError) {
                        showToast("No vehicle data found.")
                    }
                    else {
                        nonScheduleDos = loadStockMainDo.loadStockDOS;
                        adapter = LoadVanSaleStockPagerAdapter(this, scheduleDos, nonScheduleDos, supportFragmentManager)
                        viewPager.adapter = adapter
//                        var unit =preferenceUtils.getStringFromPreference(PreferenceUtils.UNIT_CAPACITY, "")
//
//                        tvCapacity.setText(""+preferenceUtils.getStringFromPreference(PreferenceUtils.TOTAL_VEHICLE_CAPACITY, "")+ " "+unit)
//
//                      //  tvNonScheduledStock.setText("Non Scheduled Stock : "+loadStockMainDo.totalNonScheduledStock +" "+loadStockMainDo.vehicleCapacityUnit)
//                        var scheduleTotal = 0.0;
//                        var nonScheduleTotal = 0.0;

//                        var scheduleStock =preferenceUtils.getStringFromPreference(PreferenceUtils.TOTAL_SCHEDULED_STOCK, "")
//                        var nonScheduleStock =loadStockMainDo.totalNonScheduledStock
//                        tvScheduledStock.setText(""+scheduleStock+" "+loadStockMainDo.vehicleCapacityUnit)
//                        tvNonScheduledStock.setText(""+nonScheduleStock+" "+loadStockMainDo.vehicleCapacityUnit)
//
//                        var total = 0.0
//                        tvCapacity.setText(preferenceUtils.getStringFromPreference(PreferenceUtils.TOTAL_VEHICLE_CAPACITY, "")+" "+unit)
//                        var  capacity=preferenceUtils.getStringFromPreference(PreferenceUtils.TOTAL_VEHICLE_CAPACITY,"")
//                           total=capacity.toDouble()
//                        scheduleTotal=scheduleStock.toDouble()
//                        preferenceUtils.saveString(PreferenceUtils.TOTAL_NON_SCHEDULED_STOCK, loadStockMainDo.totalNonScheduledStock)
//
//                        nonScheduleTotal=nonScheduleStock.toDouble()
//                        tvBalanceStock.setText(""+(total-scheduleTotal-nonScheduleTotal)+" "+ loadStockMainDo.vehicleCapacityUnit)

                    }
                }
                loadVanSaleRequest.execute()
            }
            else{
//                tvNonScheduledStock.setText("0")
                adapter = LoadVanSaleStockPagerAdapter(this, scheduleDos, nonScheduleDos, supportFragmentManager)
                viewPager.adapter = adapter
            }
        }
        else{
            showToast("No Internet connection, please try again later")
        }
    }
    override fun onButtonNoClick(from: String) {

        if ("SUCCESS".equals(from, ignoreCase = true)) {
//            finish()

        }
    }

    override fun onButtonYesClick(from: String) {

        if ("SUCCESS".equals(from, ignoreCase = true)) {
            StorageManager.getInstance(this).saveScheduledVehicleStockInLocal(scheduleDos!!)
            StorageManager.getInstance(this).saveVanScheduleProducts(this@LoadVanSaleStockTabActivity, scheduleDos)
            StorageManager.getInstance(this).saveNonScheduledVehicleStockInLocal(nonScheduleDos!!)
            StorageManager.getInstance(this).saveVanNonScheduleProducts(this@LoadVanSaleStockTabActivity, nonScheduleDos)

            var  vehicleCheckInDo = StorageManager.getInstance(this).getVehicleCheckInData(this)
            vehicleCheckInDo.loadStock = resources.getString(R.string.loaded_stock)
            vehicleCheckInDo.checkinTimeCaptureStockLoadingTime= CalendarUtils.getTime()
            vehicleCheckInDo.checkinTimeCaptureStockLoadingDate= CalendarUtils.getDate()

            if(StorageManager.getInstance(this).insertCheckInData(this,vehicleCheckInDo)){
                refreshMenuAdapter()
                setResult(4, null)
                finish()
            }
            else{
                showToast("Unable to load stock")
            }

        } else
            if ("FAILURE".equals(from, ignoreCase = true)) {


                //finish()
            }

    }
}