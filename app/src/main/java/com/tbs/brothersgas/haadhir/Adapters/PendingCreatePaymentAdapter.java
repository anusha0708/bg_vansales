package com.tbs.brothersgas.haadhir.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.tbs.brothersgas.haadhir.Activitys.CreatePaymentActivity;
import com.tbs.brothersgas.haadhir.Activitys.InvoiceListActivity;
import com.tbs.brothersgas.haadhir.Activitys.PendingInvoiceListActivity;
import com.tbs.brothersgas.haadhir.Model.UnPaidInvoiceDO;
import com.tbs.brothersgas.haadhir.R;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;

import java.util.ArrayList;

public class PendingCreatePaymentAdapter extends RecyclerView.Adapter<PendingCreatePaymentAdapter.MyViewHolder> {

    private ArrayList<UnPaidInvoiceDO> siteDOS;
    private Context context;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvSiteName, tvSiteId;
        private LinearLayout llDetails;

        public MyViewHolder(View view) {
            super(view);
            llDetails = (LinearLayout) view.findViewById(R.id.llDetails);
            tvSiteName = (TextView) view.findViewById(R.id.tvInvoiceNumber);
            tvSiteId = (TextView) view.findViewById(R.id.tvSiteId);


        }
    }


    public PendingCreatePaymentAdapter(Context context, ArrayList<UnPaidInvoiceDO> siteDOS) {
        this.context = context;
        this.siteDOS = siteDOS;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.invoice_data, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final UnPaidInvoiceDO unPaidInvoiceDO = siteDOS.get(position);
        PreferenceUtils preferenceUtils = new PreferenceUtils(context);


        double invoicedAmount = Double.parseDouble(String.format("%.2f", unPaidInvoiceDO.paidAmount+unPaidInvoiceDO.outstandingAmount));
        preferenceUtils.saveString(PreferenceUtils.CURRENCY, "" + unPaidInvoiceDO.outstandingAmountCurrency);

        if(unPaidInvoiceDO.accountingDate.length()>0){
            String aMonth = unPaidInvoiceDO.accountingDate.substring(4, 6);
            String ayear = unPaidInvoiceDO.accountingDate.substring(0, 4);
            String aDate = unPaidInvoiceDO.accountingDate.substring(Math.max(unPaidInvoiceDO.accountingDate.length() - 2, 0));
            holder.tvSiteName.setText("Invoiced Id            :  " + unPaidInvoiceDO.invoiceId + "\n" + "Accounting Date  :  " + aDate + "-" + aMonth + "-" + ayear
                    + "\n" + "Invoiced Amount :  " + invoicedAmount + " " + unPaidInvoiceDO.currency
                    + "\n" + "Paid Amount        :  " + String.format("%.2f",unPaidInvoiceDO.paidAmount) + " " + unPaidInvoiceDO.paidAmountCurrency
                    + "\n" + "Balance Amount  :  " + String.format("%.2f",unPaidInvoiceDO.outstandingAmount) + " " + unPaidInvoiceDO.outstandingAmountCurrency
                    + "\n" + "Due Days               :  " + unPaidInvoiceDO.dueDays

            );
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = holder.getAdapterPosition();
                PreferenceUtils preferenceUtils = new PreferenceUtils(context);
                preferenceUtils.saveString(PreferenceUtils.INVOICE_ID, String.valueOf(siteDOS.get(pos).invoiceId));
                preferenceUtils.saveString(PreferenceUtils.INVOICE_AMOUNT, "" + unPaidInvoiceDO.outstandingAmount);
                preferenceUtils.saveString(PreferenceUtils.CURRENCY, "" + unPaidInvoiceDO.outstandingAmountCurrency);
//                ((CreatePaymentActivity) context).etAmount.setText("" + unPaidInvoiceDO.outstandingAmount);
//                ((CreatePaymentActivity) context).tvSelection.setText("" + preferenceUtils.getStringFromPreference(PreferenceUtils.INVOICE_ID, ""));
//                ((CreatePaymentActivity) context).btnPayments.setBackgroundColor(ContextCompat.getColor(context, R.color.md_green));
//                ((CreatePaymentActivity) context).btnPayments.setClickable(true);
//                ((CreatePaymentActivity) context).btnPayments.setEnabled(true);
//                ((CreatePaymentActivity) context).dialog.dismiss();

                Intent intent = new Intent(context, CreatePaymentActivity.class);
                intent.putExtra("INVOICE_ID",String.valueOf(siteDOS.get(pos).invoiceId));
                intent.putExtra("INVOICE_AMOUNT","" + unPaidInvoiceDO.outstandingAmount);
                intent.putExtra("CURRENCY","" + unPaidInvoiceDO.outstandingAmountCurrency);
                intent.putExtra("SINGLE","PAY");

                ((PendingInvoiceListActivity) context).startActivityForResult(intent,1);


            }
        });


    }
    public void refreshAdapter(ArrayList<UnPaidInvoiceDO> siteDoS) {
        for (int i = 0; i < siteDoS.size(); i++) {

        }
        siteDOS = siteDoS;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return siteDOS.size();
    }

}
