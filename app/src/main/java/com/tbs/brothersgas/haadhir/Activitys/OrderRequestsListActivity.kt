package com.tbs.brothersgas.haadhir.Activitys

import android.graphics.*
import com.google.android.material.snackbar.Snackbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.ItemTouchHelper
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.brothersgas.haadhir.Adapters.OrderRequestsAdapter
import com.tbs.brothersgas.haadhir.Model.OrderRequestsDO
import com.tbs.brothersgas.haadhir.R
import java.util.ArrayList
import android.graphics.Movie


//
class OrderRequestsListActivity : BaseActivity() {
    private var tvNoBookings: TextView? = null
    private var rvBookingList: androidx.recyclerview.widget.RecyclerView? = null
     var orderRequestDOs: ArrayList<OrderRequestsDO> = ArrayList()

    lateinit var orderRequestAdapter: OrderRequestsAdapter
    private val p = Paint()

    //    private var orderRequestDOs = arrayOf("Code1", "Code2", "Code3", "Code4", "Code5", "Code6", "Code7", "Code8", "Code9", "Code10")
    override protected fun onResume() {
        super.onResume()
    }

    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.booking_list_layout, null) as LinearLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        ivMenu.visibility = View.VISIBLE
        initializeControls()
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }


        ivMenu.setOnClickListener {

            val popup = PopupMenu(this@OrderRequestsListActivity, ivMenu)
            popup.getMenuInflater().inflate(R.menu.superviser_menu, popup.getMenu())
            popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                override fun onMenuItemClick(item: MenuItem): Boolean {
                    if (item.title.equals("Users")) {


                    } else if (item.title.equals("Transaction Type")) {


                    }


                    return true

                }


            })
            popup.show()

        }
    }

    override fun initializeControls() {
        tvScreenTitle.setText("Requests")
        tvNoBookings = llBody.findViewById<View>(R.id.tvNoBookings) as TextView
        rvBookingList = llBody.findViewById<View>(R.id.rvBookingList) as androidx.recyclerview.widget.RecyclerView
        prepareData()
        rvBookingList!!.setLayoutManager(androidx.recyclerview.widget.LinearLayoutManager(this@OrderRequestsListActivity, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false))
        orderRequestAdapter = OrderRequestsAdapter(this@OrderRequestsListActivity, orderRequestDOs)
        rvBookingList!!.setAdapter(orderRequestAdapter)
        enableSwipe()
    }

    private fun enableSwipe() {
        val simpleItemTouchCallback = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {

            override fun onMove(recyclerView: androidx.recyclerview.widget.RecyclerView, viewHolder: androidx.recyclerview.widget.RecyclerView.ViewHolder, target: androidx.recyclerview.widget.RecyclerView.ViewHolder): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: androidx.recyclerview.widget.RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.adapterPosition

                if (direction == ItemTouchHelper.LEFT) {
                    val deletedModel = orderRequestDOs.get(position)
                    orderRequestAdapter.removeItem(position)
                    // showing snack bar with Undo option
                    val snackbar = Snackbar.make(window.decorView.rootView, "Rejected Successfully...", Snackbar.LENGTH_LONG)

//                    snackbar.setAction("UNDO") {
//                        // undo is selected, restore the deleted item
//                        orderRequestAdapter.restoreItem(deletedModel, position)
//                    }
//                    snackbar.setActionTextColor(Color.YELLOW)
                    snackbar.show()
                } else {
                    val deletedModel = orderRequestDOs.get(position)
                    orderRequestAdapter.removeItem(position)
                    val snackbar = Snackbar.make(window.decorView.rootView, "Accepted Successfully...", Snackbar.LENGTH_LONG)

                    // showing snack bar with Undo option
//                    snackbar.setAction("UNDO") {
//                        // undo is selected, restore the deleted item
//                        orderRequestAdapter.restoreItem(deletedModel, position)
//                    }
//                    snackbar.setActionTextColor(Color.YELLOW)
                    snackbar.show()
                }
            }

            override fun onChildDraw(c: Canvas, recyclerView: androidx.recyclerview.widget.RecyclerView, viewHolder: androidx.recyclerview.widget.RecyclerView.ViewHolder, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {

                val icon: Bitmap
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {

                    val itemView = viewHolder.itemView
                    val height = itemView.bottom.toFloat() - itemView.top.toFloat()
                    val width = height / 3

                    if (dX > 0) {
                        p.setColor(Color.parseColor("#388E3C"))
                        val background = RectF(itemView.left.toFloat(), itemView.top.toFloat(), dX, itemView.bottom.toFloat())
                        c.drawRect(background, p)
                        icon = BitmapFactory.decodeResource(resources, R.drawable.accept)
                        val icon_dest = RectF(itemView.left.toFloat() + width, itemView.top.toFloat() + width, itemView.left.toFloat() + 2 * width, itemView.bottom.toFloat() - width)
                        c.drawBitmap(icon, null, icon_dest, p)
                    } else {
                        p.setColor(Color.parseColor("#D32F2F"))
                        val background = RectF(itemView.right.toFloat() + dX, itemView.top.toFloat(), itemView.right.toFloat(), itemView.bottom.toFloat())
                        c.drawRect(background, p)
                        icon = BitmapFactory.decodeResource(resources, R.drawable.reject)
                        val icon_dest = RectF(itemView.right.toFloat() - 2 * width, itemView.top.toFloat() + width, itemView.right.toFloat() - width, itemView.bottom.toFloat() - width)
                        c.drawBitmap(icon, null, icon_dest, p)
                    }
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
            }
        }
        val itemTouchHelper = ItemTouchHelper(simpleItemTouchCallback)
        itemTouchHelper.attachToRecyclerView(rvBookingList)
    }

    private fun prepareData() {
        var orderRequestsDO = OrderRequestsDO()
        orderRequestDOs.add(orderRequestsDO)

        orderRequestsDO = OrderRequestsDO()
        orderRequestDOs.add(orderRequestsDO)
        orderRequestsDO = OrderRequestsDO()
        orderRequestDOs.add(orderRequestsDO)
        orderRequestsDO = OrderRequestsDO()
        orderRequestDOs.add(orderRequestsDO)
        orderRequestsDO = OrderRequestsDO()
        orderRequestDOs.add(orderRequestsDO)
        orderRequestsDO = OrderRequestsDO()
        orderRequestDOs.add(orderRequestsDO)
        orderRequestsDO = OrderRequestsDO()
        orderRequestDOs.add(orderRequestsDO)
        orderRequestsDO = OrderRequestsDO()
        orderRequestDOs.add(orderRequestsDO)
        orderRequestsDO = OrderRequestsDO()
        orderRequestDOs.add(orderRequestsDO)
        orderRequestsDO = OrderRequestsDO()
        orderRequestDOs.add(orderRequestsDO)
        orderRequestsDO = OrderRequestsDO()
        orderRequestDOs.add(orderRequestsDO)

        orderRequestsDO = OrderRequestsDO()
        orderRequestDOs.add(orderRequestsDO)
        orderRequestsDO = OrderRequestsDO()
        orderRequestDOs.add(orderRequestsDO)
        orderRequestsDO = OrderRequestsDO()
        orderRequestDOs.add(orderRequestsDO)
        orderRequestsDO = OrderRequestsDO()
        orderRequestDOs.add(orderRequestsDO)
        orderRequestsDO = OrderRequestsDO()
        orderRequestDOs.add(orderRequestsDO)
        orderRequestsDO = OrderRequestsDO()
        orderRequestDOs.add(orderRequestsDO)
        orderRequestsDO = OrderRequestsDO()
        orderRequestDOs.add(orderRequestsDO)
        orderRequestsDO = OrderRequestsDO()
        orderRequestDOs.add(orderRequestsDO)
        orderRequestsDO = OrderRequestsDO()
        orderRequestDOs.add(orderRequestsDO)
        orderRequestsDO = OrderRequestsDO()
        orderRequestDOs.add(orderRequestsDO)

        orderRequestsDO = OrderRequestsDO()
        orderRequestDOs.add(orderRequestsDO)
        orderRequestsDO = OrderRequestsDO()
        orderRequestDOs.add(orderRequestsDO)
        orderRequestsDO = OrderRequestsDO()
        orderRequestDOs.add(orderRequestsDO)
        orderRequestsDO = OrderRequestsDO()
        orderRequestDOs.add(orderRequestsDO)
        orderRequestsDO = OrderRequestsDO()
        orderRequestDOs.add(orderRequestsDO)
        orderRequestsDO = OrderRequestsDO()
        orderRequestDOs.add(orderRequestsDO)
        orderRequestsDO = OrderRequestsDO()
        orderRequestDOs.add(orderRequestsDO)
        orderRequestsDO = OrderRequestsDO()
        orderRequestDOs.add(orderRequestsDO)
        orderRequestsDO = OrderRequestsDO()
        orderRequestDOs.add(orderRequestsDO)
        orderRequestsDO = OrderRequestsDO()
        orderRequestDOs.add(orderRequestsDO)
    }

}