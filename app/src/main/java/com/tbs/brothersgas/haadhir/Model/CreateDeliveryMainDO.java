package com.tbs.brothersgas.haadhir.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class CreateDeliveryMainDO implements Serializable {

    public String customerId = "";
    public String site = "";
    public String deliveryNumber = "";
    public String message = "";
    public int status = 0;
    public int productGroupType = 0;
    public String salesOrderNumber = "";
    public String miscNumber = "";
    public int miscStatus = 0;

    public ArrayList<CreateDeliveryDO> createDeliveryDOS = new ArrayList<>();


}
