package com.tbs.brothersgas.pod.Activitys

import android.annotation.TargetApi
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.view.View
import android.view.ViewGroup
import com.tbs.brothersgas.haadhir.Activitys.BaseActivity
import com.tbs.brothersgas.haadhir.Activitys.CheckinScreen
import com.tbs.brothersgas.haadhir.Activitys.LoadVanSaleStockTabActivity
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.itextpdf.text.pdf.qrcode.WriterException
import android.R.id
import android.graphics.Bitmap
import android.graphics.Color
import android.opengl.ETC1.getHeight
import android.widget.*
import com.itextpdf.text.pdf.qrcode.BitMatrix
import com.itextpdf.text.pdf.qrcode.QRCodeWriter
import com.tbs.brothersgas.haadhir.Activitys.NewLoadVanSaleStockTabActivity


class StartDayActivity : BaseActivity() {


    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.start_day_layout, null) as ScrollView
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        disableMenuWithBackButton()
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            finish()
//            overridePendingTransition(R.anim.exit, R.anim.enter)
        }
        initializeControls()


    }


    override fun initializeControls() {

        tvScreenTitle.setText("Start Day")
        var llCheckIn = findViewById<View>(R.id.llCheckIn) as LinearLayout
        var llLoadStock = findViewById<View>(R.id.llLoadStock) as LinearLayout
        llCheckIn.setOnClickListener {
            val intent = Intent(this@StartDayActivity, CheckinScreen::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
        }

        llLoadStock.setOnClickListener {
            val vehicleCheckInDo = StorageManager.getInstance(this@StartDayActivity).getVehicleCheckInData(this@StartDayActivity);
            if (!vehicleCheckInDo.checkInStatus!!.isEmpty()) {
                if (!vehicleCheckInDo.loadStock.equals("")) {
                    val intent = Intent(this@StartDayActivity, NewLoadVanSaleStockTabActivity::class.java)
                    intent.putExtra("FLAG", 1);
                    intent.putExtra("ScreenTitle", "Load Van Sales Stock")
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                } else {
                    showToast("Please complete the stock load in check in screen")
                }
            } else {

                if (!vehicleCheckInDo.loadStock.equals("")) {
                    val intent = Intent(this@StartDayActivity, NewLoadVanSaleStockTabActivity::class.java)
                    intent.putExtra("FLAG", 1);
                    intent.putExtra("ScreenTitle", "Load Van Sales Stock")
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                } else {
                    showAppCompatAlert("", "Please Finish Check-in Process", "OK", "", "", false)
                }

            }
        }





    }

    override protected fun onResume() {
        super.onResume()
    }


}