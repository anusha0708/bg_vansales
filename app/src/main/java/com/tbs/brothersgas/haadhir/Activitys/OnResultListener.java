package com.tbs.brothersgas.haadhir.Activitys;

import com.tbs.brothersgas.haadhir.Model.CylinderIssueMainDO;

public interface OnResultListener {
        public void onCompleted(boolean isError, CylinderIssueMainDO cylinderIssueMainDO);

    }