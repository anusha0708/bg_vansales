package com.tbs.brothersgas.haadhir.Activitys

import android.content.Intent
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.brothersgas.haadhir.Adapters.AddressAdapter
import com.tbs.brothersgas.haadhir.Adapters.InvoiceAdapter
import com.tbs.brothersgas.haadhir.Model.CustomerDo
import com.tbs.brothersgas.haadhir.Model.LoadStockDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.AddressListRequest
import com.tbs.brothersgas.haadhir.Requests.InvoiceHistoryRequest
import com.tbs.brothersgas.haadhir.common.AppConstants
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import java.util.ArrayList
import androidx.core.os.HandlerCompat.postDelayed
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.utils.CalendarUtils
import java.util.logging.Handler


class AddressListActivity : BaseActivity() {
    lateinit var invoiceAdapter: AddressAdapter
    lateinit var loadStockDOs: ArrayList<LoadStockDO>
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var tvNoDataFound: TextView
    var fromId = 0
    lateinit var code: String
    lateinit var customer: String
    lateinit var customerDo: CustomerDo
    lateinit var tvColorFlag : TextView

    override protected fun onResume() {
        initializeControls()
        super.onResume()
    }

    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.address_list, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
//        initializeControls()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
    }

    override fun initializeControls() {
        tvScreenTitle.setText("Address List")
        recycleview = findViewById(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        tvNoDataFound = findViewById(R.id.tvNoDataFound) as TextView
        tvColorFlag = findViewById(R.id.tvColorFlag) as TextView
        recycleview.setLayoutManager(linearLayoutManager)
        recycleview!!.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this@AddressListActivity)
        if (intent.hasExtra("CODE")) {
            customer = intent.extras!!.getString("CODE")!!
        } else {
            customer = ""
        }
        if (intent.hasExtra("CUSTOMERDO")) {
            customerDo = intent.extras!!.getSerializable("CUSTOMERDO") as CustomerDo
        }
        if (Util.isNetworkAvailable(this)) {

            val addressListRequest = AddressListRequest(customer, this@AddressListActivity)
            addressListRequest.setOnResultListener { isError, invoiceHistoryMainDO ->
                hideLoader()
                if (isError) {
                    tvNoDataFound.setVisibility(View.VISIBLE)
                    recycleview.setVisibility(View.GONE)
                    Toast.makeText(this@AddressListActivity, R.string.error_NoData, Toast.LENGTH_SHORT).show()
                } else {
                    if (invoiceHistoryMainDO.addressDOS.size > 0) {
                        if (intent.hasExtra("MASTER")) {
                            code = intent.extras!!.getString("MASTER")!!
                        } else {
                            code = ""
                        }
                        tvNoDataFound.setVisibility(View.GONE)
                        recycleview.setVisibility(View.VISIBLE)
                        if(invoiceHistoryMainDO.percentage.isNotEmpty()&&code.isEmpty()){
                            tvColorFlag.setVisibility(View.VISIBLE)
                        }else{
                            tvColorFlag.setVisibility(View.GONE)
                        }
                        if(invoiceHistoryMainDO.colorFlag==30){
                            tvColorFlag.setBackgroundDrawable(resources.getDrawable(R.drawable.card_red))
                            tvColorFlag.setTextColor(resources.getColor(R.color.white))


                        }else if(invoiceHistoryMainDO.colorFlag==20){
                            tvColorFlag.setBackgroundDrawable(resources.getDrawable(R.drawable.card_orange))
                            tvColorFlag.setTextColor(resources.getColor(R.color.white))

                        }
                        else if(invoiceHistoryMainDO.colorFlag==10){
                            tvColorFlag.setBackgroundDrawable(resources.getDrawable(R.drawable.card_white))
                            tvColorFlag.setTextColor(resources.getColor(R.color.black))


                        }else{
                            tvColorFlag.setBackgroundDrawable(resources.getDrawable(R.drawable.card_white))
                            tvColorFlag.setTextColor(resources.getColor(R.color.black))

                        }
                        tvColorFlag.setText(invoiceHistoryMainDO.percentage)

                        if (code.length > 0) {
                            invoiceAdapter = AddressAdapter(this@AddressListActivity, invoiceHistoryMainDO.addressDOS, code, customer, customerDo)
                            recycleview!!.adapter = invoiceAdapter
                        } else {

                            if (invoiceHistoryMainDO.flag == 3) {
                                showAppCompatAlert("Alert!", "Customer held", "OK", "", "SUCCESS", false)

                                android.os.Handler().postDelayed({
                                    finish()
                                }, 6000)


                            } else {
                                invoiceAdapter = AddressAdapter(this@AddressListActivity, invoiceHistoryMainDO.addressDOS, "", customer, customerDo)

                                recycleview!!.adapter = invoiceAdapter
                            }

                        }
                    } else {
                        tvNoDataFound.setVisibility(View.VISIBLE)
                        recycleview.setVisibility(View.GONE)
                    }
                }
            }
            addressListRequest.execute()
        } else {
            showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 63 && resultCode == 63) {
            finish()
        }
        if (requestCode == 63 && resultCode == 52) {
            finish()
        }
        else if (requestCode == 63 && resultCode == 19) {
            if (data != null && data.hasExtra("CapturedReturns") && data.getBooleanExtra("CapturedReturns", false)) {

                val intent = Intent()
                intent.putExtra("CapturedReturns", AppConstants.CapturedReturns)
                setResult(19, intent)
                finish()
                // uncomment if required
            }
        }
    }

    override fun onButtonYesClick(from: String) {

        if ("SUCCESS".equals(from, ignoreCase = true)) {
            finish()

        } else if ("CREDIT_SUCCESS".equals(from, ignoreCase = true)) {
            StorageManager.getInstance(this).deleteActiveDeliveryMainDo(this)
            StorageManager.getInstance(this).deleteDepartureData(this)
            (this).preferenceUtils.removeFromPreference(PreferenceUtils.CustomerId)
            StorageManager.getInstance(this).deleteCurrentSpotSalesCustomer(this)
            StorageManager.getInstance(this).deleteCurrentDeliveryItems(this)
            StorageManager.getInstance(this).deleteReturnCylinders(this)

            if (StorageManager.getInstance(this).saveCurrentSpotSalesCustomer(this, customerDo)) {
                val intent = Intent(this, ActiveDeliveryActivity::class.java)
                preferenceUtils.saveString(PreferenceUtils.ProductsType, AppConstants.FixedQuantityProduct)
                preferenceUtils.saveString(PreferenceUtils.ShipmentType, getResources().getString(R.string.checkin_non_scheduled))
                preferenceUtils.saveString(PreferenceUtils.CREDIT_FLAG, "CREDIT_CUSTOMER")
                startActivityForResult(intent, 63)
            }
        } else
            if ("FAILURE".equals(from, ignoreCase = true)) {
            }

    }

    override fun onButtonNoClick(from: String) {
        if ("CREDIT_SUCCESS".equals(from, ignoreCase = true)) {
            finish()
        }
        super.onButtonNoClick(from)
    }
}