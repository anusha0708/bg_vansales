package com.tbs.brothersgas.haadhir.Activitys

import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.appcompat.app.AppCompatDialog
import com.tbs.brothersgas.haadhir.Adapters.LoadStockAdapter
import com.tbs.brothersgas.haadhir.Adapters.ProductAdapter
import com.tbs.brothersgas.haadhir.Adapters.ScheduledProductsAdapter
import com.tbs.brothersgas.haadhir.Adapters.SiteAdapter
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryDO
import com.tbs.brothersgas.haadhir.Model.CustomerDo
import com.tbs.brothersgas.haadhir.Model.NonScheduledProductMainDO
import com.tbs.brothersgas.haadhir.Model.ProductDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.CurrentNonScheduledStockRequest
import com.tbs.brothersgas.haadhir.common.AppConstants
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.listeners.DBProductsListener
import com.tbs.brothersgas.haadhir.utils.CalendarUtils
import com.tbs.brothersgas.haadhir.utils.Constants
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import kotlinx.android.synthetic.main.questions_residential.*
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*


class ScheduledAddProductActivity : BaseActivity(), DBProductsListener {
    override fun updateData() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    var selectedDate = ""
    var x3Date = ""

    lateinit var nonScheduledProductMainDO: NonScheduledProductMainDO
    private var cday: Int = 0
    private var cmonth: Int = 0
    private var cyear: Int = 0
    lateinit var tvSelection: TextView
    lateinit var tvNoDataFound: TextView
    lateinit var dialog: Dialog
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var llOrderHistory: RelativeLayout
    lateinit var customerId: String
    private lateinit var btnAdd: Button
    var loadStockAdapter: ScheduledProductsAdapter = ScheduledProductsAdapter(this, ArrayList(), "AddProducts", "", 0)
    lateinit var tvScreenTitles: TextView
    var tvNoOrders: TextView? = null
    var loadStockDoS: ArrayList<ActiveDeliveryDO> = ArrayList()
    lateinit var llSearch: LinearLayout
    lateinit var etSearch: EditText
    lateinit var ivClearSearch: ImageView
    lateinit var ivSearchs: ImageView
    lateinit var ivGoBack: ImageView
    lateinit var routeId: String
    lateinit var productDOs: ArrayList<ActiveDeliveryDO>
    lateinit var fromm: String
    lateinit var shipmentProductsType: String
    lateinit var customerDo: CustomerDo
    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.scheduled_add_product_screen, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        flToolbar.visibility = View.GONE
        disableMenuWithBackButton()
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
        disableMenuWithBackButton()
        routeId = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "")
        if (intent.hasExtra("CustomerId")) {
            customerId = intent.extras.getString("CustomerId")
        }

        initializeControls()
        if (intent.hasExtra("FROM")) {
            fromm = intent.extras.getString("FROM")
        }
        loadNonScheduleProducts()
        btnAdd.setOnClickListener {
            var isProductsAdded: Boolean? = false
            if (loadStockAdapter != null && loadStockAdapter.getSelectedactiveDeliveryDOs() != null && loadStockAdapter.getSelectedactiveDeliveryDOs().size > 0) {
                for (i in loadStockAdapter.getSelectedactiveDeliveryDOs().indices) {
                    if (loadStockAdapter.getSelectedactiveDeliveryDOs().get(i).isProductAdded && loadStockAdapter.getSelectedactiveDeliveryDOs().get(i).orderedQuantity > 0) {
                        isProductsAdded = true
                        break
                    }
                }
            }
            if (isProductsAdded!!) {
                showAppCompatAlert("", "Are You Sure You Want to \n Add Products?", "OK", "Cancel", "SUCCESS", true)


            } else {
                showToast("Please select any product")
            }
        }

        ivGoBack.setOnClickListener { finish() }
        ivSearch.setOnClickListener {
            //                if(llSearch.visibility == View.VISIBLE){
            //                llSearch.visibility = View.INVISIBLE
            //            }
            //            else{
            //            }
            tvScreenTitles.visibility = View.GONE
            llSearch.visibility = View.VISIBLE
        }

        ivClearSearch.setOnClickListener {
            etSearch.setText("")
            if (loadStockDoS != null && loadStockDoS.size > 0) {
                loadStockAdapter = ScheduledProductsAdapter(this@ScheduledAddProductActivity, loadStockDoS, "AddProducts", "", 0)
                recycleview.setAdapter(loadStockAdapter)
                tvNoDataFound.setVisibility(View.GONE)
                recycleview.visibility = View.VISIBLE
            } else {
                tvNoDataFound.setVisibility(View.VISIBLE)
                recycleview.visibility = View.GONE
            }
        }

        etSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                try {
                    if (etSearch.text.toString().equals("", true)) {
                        if (loadStockDoS != null && loadStockDoS.size > 0) {
                            loadStockAdapter = ScheduledProductsAdapter(this@ScheduledAddProductActivity, loadStockDoS, "AddProducts", "", 0)
                            recycleview.setAdapter(loadStockAdapter)
                            tvNoDataFound.setVisibility(View.GONE)
                            recycleview.visibility = View.VISIBLE
                        } else {
                            tvNoDataFound.setVisibility(View.VISIBLE)
                            recycleview.visibility = View.GONE
                        }
                    } else if (etSearch.text.toString().length > 2) {

                        filter(etSearch.text.toString())
                    }
                } catch (e: Exception) {

                }

            }
        })

    }

    private fun filter(filtered: String): ArrayList<ActiveDeliveryDO> {
        productDOs = ArrayList()
        for (i in loadStockDoS.indices) {
            if (loadStockDoS.get(i).product.contains(filtered, true) || loadStockDoS.get(i).productDescription.contains(filtered, true)) {
                productDOs.add(loadStockDoS.get(i))
            }
        }
        if (productDOs.size > 0) {
            loadStockAdapter = ScheduledProductsAdapter(this@ScheduledAddProductActivity, loadStockDoS, "AddProducts", "", 0)
            recycleview.setAdapter(loadStockAdapter)
            //            productAdapter.refreshAdapter(productDOs);
            tvNoDataFound.setVisibility(View.GONE)
            recycleview.visibility = View.VISIBLE
        } else {
            tvNoDataFound.setVisibility(View.VISIBLE)
            recycleview.visibility = View.GONE
        }
        return productDOs
    }

    private fun loadNonScheduleProducts() {
        var routeId = preferenceUtils.getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, "")

        if (Util.isNetworkAvailable(this)) {

            val loadVanSaleRequest = CurrentNonScheduledStockRequest(routeId, this@ScheduledAddProductActivity)
            showLoader()
            loadVanSaleRequest.setOnResultListener { isError, loadStockMainDo ->
                hideLoader()
                if (isError) {
                    showToast("No vehicle data found.")
                    recycleview.visibility = GONE
                    tvNoDataFound.visibility = VISIBLE
                    btnAdd.visibility = GONE
                } else {
                    hideLoader()
//                        StorageManager.getInstance(this).saveNonScheduledVehicleStockInLocal(loadStockMainDo.loadStockDOS!!)

                    if (loadStockMainDo.loadStockDOS != null && loadStockMainDo.loadStockDOS.size > 0) {
                        recycleview.visibility = VISIBLE
                        tvNoDataFound.visibility = GONE
                        btnAdd.visibility = VISIBLE
                        loadStockDoS = ArrayList<ActiveDeliveryDO>()
                        for (i in loadStockMainDo.loadStockDOS.indices) {
                            val loadStockDO = loadStockMainDo.loadStockDOS.get(i)
                            val activeDeliveryDO = ActiveDeliveryDO();
                            activeDeliveryDO.shipmentId = loadStockDO.shipmentNumber
                            activeDeliveryDO.product = loadStockDO.product
                            activeDeliveryDO.site = loadStockDO.site

                            activeDeliveryDO.productDescription = loadStockDO.productDescription
                            activeDeliveryDO.stockUnit = loadStockDO.stockUnit
                            activeDeliveryDO.weightUnit = loadStockDO.weightUnit
                            activeDeliveryDO.quantity = loadStockDO.quantity
                            activeDeliveryDO.weightUnit = "" + loadStockDO.productWeight
                            activeDeliveryDO.unit = loadStockDO.totalStockUnit
                            activeDeliveryDO.totalQuantity = loadStockDO.quantity
                            activeDeliveryDO.orderedQuantity = loadStockDO.quantity
                            activeDeliveryDO.unit = loadStockDO.productUnit
                            activeDeliveryDO.percentage = loadStockDO.percentage
                            activeDeliveryDO.price = loadStockDO.price

                            if (loadStockDO.productGroupType == 20) {
                                activeDeliveryDO.shipmentProductType = AppConstants.MeterReadingProduct
                                preferenceUtils.saveString(PreferenceUtils.ShipmentProductsType, AppConstants.MeterReadingProduct)

                            } else {
                                activeDeliveryDO.shipmentProductType = AppConstants.FixedQuantityProduct
                                preferenceUtils.saveString(PreferenceUtils.ShipmentProductsType, AppConstants.FixedQuantityProduct)

                            }
//                                activeDeliveryDO.status                        = loadStockDO.status
                            loadStockDoS.add(activeDeliveryDO)
                        }
                        loadStockAdapter = ScheduledProductsAdapter(this@ScheduledAddProductActivity, loadStockDoS, "AddProducts", "", 0)
                        recycleview.setAdapter(loadStockAdapter)
                    } else {
                        recycleview.visibility = GONE
                        tvNoDataFound.visibility = VISIBLE
                        btnAdd.visibility = GONE
                    }

                }
            }
            loadVanSaleRequest.execute()
        } else {
            showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

        }


    }

    override fun initializeControls() {

        recycleview = findViewById<View>(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        tvNoDataFound = findViewById<View>(R.id.tvNoDataFound) as TextView
        btnAdd = findViewById<View>(R.id.btnAdd) as Button
        tvScreenTitles = findViewById<View>(R.id.tvScreenTitles) as TextView
        llSearch = findViewById<LinearLayout>(R.id.llSearch)
        etSearch = findViewById<EditText>(R.id.etSearch)
        ivClearSearch = findViewById<ImageView>(R.id.ivClearSearch)
        ivGoBack = findViewById<ImageView>(R.id.ivGoBack)
        ivSearch = findViewById(R.id.ivSearchs)
        recycleview.setLayoutManager(androidx.recyclerview.widget.LinearLayoutManager(this, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false))
        recycleview.setNestedScrollingEnabled(false);

    }

    override fun onButtonYesClick(from: String) {

        if ("SUCCESS".equals(from, ignoreCase = true)) {
            customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)
            shipmentProductsType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "")
            if (shipmentProductsType.equals(AppConstants.FixedQuantityProduct, true) && customerDo.resFlag == 2) {
                captureAnswers()
            } else {
                val loadStockDos = loadStockAdapter!!.getSelectedactiveDeliveryDOs();
                val intent = Intent();
                intent.putExtra("AddedProducts", loadStockDos);
                setResult(1, intent)
                finish()
            }

        } else
            if ("FAILURE".equals(from, ignoreCase = true)) {

            }
    }

    fun captureAnswers() {
        try {
            val dialog = AppCompatDialog(this, R.style.AppCompatAlertDialogStyle)
            dialog.supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
            val view = LayoutInflater.from(this).inflate(R.layout.questions_residential, null)


            dialog.setCancelable(true)
            dialog.setCanceledOnTouchOutside(true)
            val btnSubmit = view.findViewById<Button>(R.id.btnConfirm)
            val btnBack = view.findViewById<Button>(R.id.btnBack)

            tvQsnDate = view.findViewById<TextView>(R.id.tvDate) as TextView
            var rbYes2 = view.findViewById<RadioButton>(R.id.rbYes2) as RadioButton
            var rbYes3 = view.findViewById<RadioButton>(R.id.rbYes3) as RadioButton
            var rbYes4 = view.findViewById<RadioButton>(R.id.rbYes4) as RadioButton
            var rbYes5 = view.findViewById<RadioButton>(R.id.rbYes5) as RadioButton
            var rbNo2 = view.findViewById<RadioButton>(R.id.rbNo2) as RadioButton
            var rbNo3 = view.findViewById<RadioButton>(R.id.rbNo3) as RadioButton
            var rbNo4 = view.findViewById<RadioButton>(R.id.rbNo4) as RadioButton
            var rbNo5 = view.findViewById<RadioButton>(R.id.rbNo5) as RadioButton
            val c = Calendar.getInstance()
            val cyear = c.get(Calendar.YEAR)
            val cmonth = c.get(Calendar.MONTH)
            val cday = c.get(Calendar.DAY_OF_MONTH)
            tvQsnDate!!.setOnClickListener {
                val datePicker = DatePickerDialog(this, datePickerListener, cyear, cmonth, cday)
                datePicker.show()
            }
            btnBack.setOnClickListener {
                dialog.dismiss()
            }

            btnSubmit.setOnClickListener {

               if(selectedDate.isNotEmpty()){
                   var sdf = SimpleDateFormat("dd/MM/yyyy");
                   var strDate = sdf.parse(selectedDate);
                   var date1 = sdf.parse(selectedDate);
                   var date2 = sdf.parse(CalendarUtils.getPDFDate());
                   if (!strDate.after(Date()) && !date1.equals(date2)) {
                       showToast("You cannot proceed further")
                   } else if (rbNo2.isChecked || rbNo3.isChecked || rbNo4.isChecked || rbNo5.isChecked) {
                       showToast("You cannot proceed further")
                   } else {
                       preferenceUtils.saveString(PreferenceUtils.EXPIRY_DATE, x3Date)
                       preferenceUtils.saveInt(PreferenceUtils.QSN_2, 2)
                       preferenceUtils.saveInt(PreferenceUtils.QSN_3, 2)
                       preferenceUtils.saveInt(PreferenceUtils.QSN_4, 2)
                       preferenceUtils.saveInt(PreferenceUtils.QSN_5, 2)

                       val loadStockDos = loadStockAdapter!!.getSelectedactiveDeliveryDOs();
                       val intent = Intent();
                       intent.putExtra("AddedProducts", loadStockDos);
                       setResult(1, intent)
                       finish()
                   }
               }else{
                   showToast("Please Select Date")

               }

            }
            dialog.setContentView(view)
            if (!dialog.isShowing) {
                dialog.show()
            }
        } catch (e: Exception) {
        }

    }


    private val datePickerListener: DatePickerDialog.OnDateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        cday = dayOfMonth
        cmonth = monthOfYear + 1
        cyear = year
        var monthString = cmonth.toString()
        if (monthString.length == 1) {
            monthString = "0$monthString"
        } else {
            monthString = cmonth.toString()
        }

        var dayString = cday.toString()
        if (dayString.length == 1) {
            dayString = "0$dayString"
        } else {
            dayString = cday.toString()
        }
        x3Date = "" + cyear + monthString+ dayString;

        selectedDate = "" + dayString + "/" + monthString + "/" + cyear;
        tvQsnDate!!.setText("" + dayString + " - " + monthString + " - " + cyear)


    }

}