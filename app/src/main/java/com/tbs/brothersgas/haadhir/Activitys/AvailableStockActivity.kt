package com.tbs.brothersgas.haadhir.Activitys

import android.app.Activity
import android.content.Intent
import android.util.Log
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.brothersgas.haadhir.Adapters.LoadStockAdapter
import com.tbs.brothersgas.haadhir.Adapters.LoadVanSaleStockPagerAdapter
import com.tbs.brothersgas.haadhir.Adapters.ScheduledProductsAdapter
import com.tbs.brothersgas.haadhir.Model.LoadStockDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.*
import com.tbs.brothersgas.haadhir.utils.Constants
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import kotlinx.android.synthetic.main.available_stock.*
import kotlinx.android.synthetic.main.base_layout.*
import kotlinx.android.synthetic.main.new_load_vansales_tab.*
import java.util.ArrayList

//
class AvailableStockActivity : BaseActivity() {
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    private var scheduleDos: ArrayList<LoadStockDO> = ArrayList()
    private var nonScheduleDos: ArrayList<LoadStockDO> = ArrayList()
    private var availableStockDos: ArrayList<LoadStockDO> = ArrayList()
    lateinit var tvTotal: TextView
    var nonPercntage = 0.0
    var percentage: Double = 0.0
    override protected fun onResume() {
        super.onResume()
    }

    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.available_stock, null) as ScrollView
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()
        disableMenuWithBackButton()
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            setResult(14, null)
            finish()
        }

        if (preferenceUtils.getbooleanFromPreference(PreferenceUtils.IS_MULTI_SITE, false)) {
            floating_action_button_available.show()
        } else {
            floating_action_button_available.hide()
        }

        floating_action_button_available.setOnClickListener {
            val intent = Intent(this, MultiSiteListActivity::class.java)
            intent.putExtra(Constants.IS_FROM_DELIVERY, false)
            startActivityForResult(intent, 10)

        }
    }

    override fun initializeControls() {
        tvScreenTitle.setText("Available Stock")
        recycleview = findViewById<View>(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        recycleview.setLayoutManager(androidx.recyclerview.widget.LinearLayoutManager(this))
        tvTotal = findViewById<TextView>(R.id.tvTotal) as TextView
        val typeface = ResourcesCompat.getFont(this!!, R.font.bgs)
        tvTotal.setTypeface(typeface)

        if (scheduleDos == null || scheduleDos!!.size < 1) {
            loadVehicleStockData()
        } else if (nonScheduleDos == null || nonScheduleDos!!.size < 1) {
            loadNonVehicleStockData()
        } else {
            var isProductExisted = false;
            availableStockDos = nonScheduleDos
            for (i in scheduleDos.indices) {
                for (k in availableStockDos.indices) {
                    if (scheduleDos.get(i).product.equals(availableStockDos!!.get(k).product, true)) {
                        availableStockDos!!.get(k).quantity = availableStockDos!!.get(k).quantity + scheduleDos.get(i).quantity
                        isProductExisted = true
                        break
                    }
                }
                if (isProductExisted) {
                    isProductExisted = false
                    continue
                } else {
                    availableStockDos!!.add(scheduleDos.get(i))
                }
            }

            if (availableStockDos != null && availableStockDos.size > 0) {
                var loadStockAdapter = LoadStockAdapter(this@AvailableStockActivity, availableStockDos, "Shipments")
                recycleview.setAdapter(loadStockAdapter)
            }

        }


    }

    private fun loadVehicleStockData() {
        if (Util.isNetworkAvailable(this)) {
            var scheduledRootId = "" + preferenceUtils.getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, "")

//            var scheduledRootId = "" + preferenceUtils.getStringFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID, "")
            if (scheduledRootId.length > 0) {
                val loadVanSaleRequest = MultiCurrentScheduledStockRequest(scheduledRootId, this@AvailableStockActivity)
                showLoader()

                loadVanSaleRequest.setOnResultListener { isError, loadStockMainDo ->
                    hideLoader()
                    if (isError) {
                        showToast("No vehicle data found.")
                    } else {
                        hideLoader()

                        scheduleDos = loadStockMainDo.loadStockDOS;
                        for (k in scheduleDos.indices) {
                            percentage = percentage + scheduleDos.get(k).percentage

                        }
                    }
                    loadNonVehicleStockData()
                }
                loadVanSaleRequest.execute()
            } else {
                loadNonVehicleStockData()

            }


        } else {
            showToast("No Internet connection, please try again later")
        }
    }

    private fun loadNonVehicleStockData() {
        if (Util.isNetworkAvailable(this)) {

            var nonScheduledRootId = "" + preferenceUtils.getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, "")
            if (nonScheduledRootId.length > 0) {
                val loadVanSaleRequest = MultiCurrentNonScheduledStockRequest(nonScheduledRootId, this@AvailableStockActivity)
                showLoader()
                loadVanSaleRequest.setOnResultListener { isError, loadStockMainDo ->
                    hideLoader()
                    if (isError) {
                        showToast("No vehicle data found.")
                    } else {
                        hideLoader()

                        nonScheduleDos = loadStockMainDo.loadStockDOS;
                        var isProductExisted = false;
                        availableStockDos = nonScheduleDos
                        for (i in scheduleDos.indices) {
                            for (k in availableStockDos.indices) {
                                if (scheduleDos.get(i).product.equals(availableStockDos!!.get(k).product, true)&&scheduleDos.get(i).site.equals(availableStockDos!!.get(k).site, true)) {
                                    availableStockDos!!.get(k).quantity = availableStockDos!!.get(k).quantity + scheduleDos.get(i).quantity
                                    isProductExisted = true
                                    break
                                }
                            }
                            if (isProductExisted) {
                                isProductExisted = false
                                continue
                            } else {
                                availableStockDos!!.add(scheduleDos.get(i))
                            }

                        }
                        for (k in nonScheduleDos.indices) {
                            nonPercntage = nonPercntage + nonScheduleDos.get(k).percentage

                        }
                        var totalPer = percentage + nonPercntage
                        if (totalPer != 0.0) {
                            tvTotal.visibility = View.VISIBLE

                            tvTotal.setText("" + totalPer + "%")

                        }
//                        availableStockDos.addAll(nonScheduleDos)

                        var loadStockAdapter = LoadStockAdapter(this@AvailableStockActivity, availableStockDos, "Shipments")
                        recycleview.setAdapter(loadStockAdapter)

                    }
                }
                loadVanSaleRequest.execute()
            } else {

                var loadStockAdapter = LoadStockAdapter(this@AvailableStockActivity, availableStockDos, "Shipments")
                recycleview.setAdapter(loadStockAdapter)
                if (percentage != 0.0) {
                    tvTotal.visibility = View.VISIBLE
                    tvTotal.setText("" + percentage + "%")

                }
            }


        } else {
            showToast("No Internet connection, please try again later")
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == 10) {
            if (data != null) {
                val site = data.getStringExtra("site_id");
                if (site.equals("clear", true)) {
                    tv_no_data.visibility = View.GONE
                    recycleview.visibility = View.VISIBLE
                    val loadStockAdapter = LoadStockAdapter(this, availableStockDos, "Shipments")
                    recycleview.setAdapter(loadStockAdapter)
                } else {
                    //Toast.makeText(this, site, Toast.LENGTH_LONG).show()
                    val availableStockDosLocal: ArrayList<LoadStockDO> = ArrayList()
                    Log.d("site--->", availableStockDos.size.toString())

                    for (items in availableStockDos) {
                        Log.d("site--->", items.site.toString())
                        if (items.site.equals(site, true)) {
                            availableStockDosLocal.add(items)

                        }
                    }
                    if (availableStockDosLocal.size > 0) {
                        tv_no_data.visibility = View.GONE
                        recycleview.visibility = View.VISIBLE
                        val loadStockAdapter = LoadStockAdapter(this, availableStockDosLocal, "Shipments")
                        recycleview.setAdapter(loadStockAdapter)
                    } else {
                        tv_no_data.visibility = View.VISIBLE
                        recycleview.visibility = View.GONE
                        tv_no_data.text = "No data found for site " + site
                    }
                }
            };
        }
    }
}