package com.tbs.brothersgas.haadhir.Activitys

import android.app.Dialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.brothersgas.haadhir.Adapters.CreateProductAdapter
import com.tbs.brothersgas.haadhir.Model.*
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.*
import com.tbs.brothersgas.haadhir.listeners.DBProductsListener


class CreateProductActivity : BaseActivity() ,DBProductsListener {
    override fun updateData() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    lateinit var tvSelection: TextView
    lateinit var dialog: Dialog
    lateinit var pickDos: ArrayList<PickUpDo>
    lateinit var createInvoiceDO: CreateInvoiceDO
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var llOrderHistory: RelativeLayout
    lateinit var productDOS: List<ProductDO>
    lateinit var DBProductsListener :DBProductsListener
    public  lateinit var btnAdd:Button
    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.create_product_screen, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()

    }

    override fun initializeControls() {
        tvScreenTitle.setText(R.string.create_product)
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
        recycleview = findViewById<View>(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        recycleview.setLayoutManager(androidx.recyclerview.widget.LinearLayoutManager(this))
        btnAdd = findViewById<View>(R.id.btnAdd) as Button
          DBProductsListener = com.tbs.brothersgas.haadhir.listeners.DBProductsListener {

          }

        val siteDO = SiteDO()
        val siteListRequest = ProductListRequest(this@CreateProductActivity)
        showLoader()
        siteListRequest.setOnResultListener { isError, productDoS ->
            hideLoader()
            productDOS = productDoS
            if (isError) {
                Toast.makeText(this@CreateProductActivity, R.string.error_product_list, Toast.LENGTH_SHORT).show()
            } else {
                if (productDOS.size > 0) {
                    val driverAdapter = CreateProductAdapter(this@CreateProductActivity, productDOS)
                    recycleview.setAdapter(driverAdapter)
                } else {
                    showAlert("" + R.string.error_NoData)
                }
            }
        }

        siteListRequest.execute()

    }


}