package com.tbs.brothersgas.haadhir.Activitys

import android.app.Activity
import android.view.ViewGroup
import android.widget.*
import com.tbs.brothersgas.haadhir.R
import android.content.Intent
import com.tbs.brothersgas.haadhir.database.StorageManager


//
class DeliveryNotesActivity : BaseActivity() {

    lateinit var btnSave : Button
    lateinit var etNotes : EditText

    override protected fun onResume() {
        super.onResume()
    }

    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.notes_screen, null) as LinearLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        disableMenuWithBackButton()
        tvScreenTitle.setText("Delivery Remarks")
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
        initializeControls()
        var podDo = StorageManager.getInstance(this).getDepartureData(this);
        etNotes.setText(podDo.deliveryNotes)
        btnSave.setOnClickListener {
            if(etNotes.text.toString().isNotEmpty()){
                var data = etNotes.text.toString()
                val intent = Intent()
                intent.putExtra("DELIVERY_REMARKS", data)
                setResult(Activity.RESULT_OK, intent)
                finish()
            }
            else{
                showToast("Please enter remarks")
            }
        }
    }

    override fun initializeControls() {
        etNotes         = findViewById(R.id.etNotes) as EditText
        btnSave         = findViewById(R.id.btnSave) as Button
    }
}