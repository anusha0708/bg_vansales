package com.tbs.brothersgas.haadhir.Activitys

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.location.*
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import android.util.Log
import android.view.ViewGroup
import android.widget.*
import com.tbs.brothersgas.haadhir.Model.*
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.*
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.listeners.ResultListner
import com.tbs.brothersgas.haadhir.pdfs.BGInvoicePdf
import com.tbs.brothersgas.haadhir.pdfs.PreInvoicePdf
import com.tbs.brothersgas.haadhir.print.FileHelper
import com.tbs.brothersgas.haadhir.prints.PrinterConnection
import com.tbs.brothersgas.haadhir.prints.PrinterConstants
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import java.lang.Exception
import java.util.*


class CreateInvoiceActivity : BaseActivity() {
    lateinit var tvSelection: TextView
    lateinit var dialog: Dialog
    lateinit var deliveryDO: DeliveryDO
    lateinit var deliveryMainDO: DeliveryMainDO
    lateinit var data: String
    lateinit var podDo: PodDo
    lateinit var btnInvoice: Button
    lateinit var btnPreInvoice: Button

    private val permissionRequestCode = 34
    val TAG = FileHelper::class.java.name
    lateinit var createPDFInvoiceDo: CreateInvoicePaymentDO
    lateinit var createInvoiceDO: CreateInvoiceDO
    lateinit var locationManager: LocationManager
    var lattitude: String = ""
    var longitude: String = ""
    var address: String = ""

    var fromId = 0
    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.create_invoice_screen, null) as LinearLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()
        if (intent.hasExtra("Sales")) {
            fromId = intent.extras.getInt("Sales")
        }

        getSystemService(Context.LOCATION_SERVICE);
        val ShipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, getResources().getString(R.string.checkin_non_scheduled))
        if (ShipmentType.equals(getResources().getString(R.string.checkin_non_scheduled), true)) {
            var shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER, "")
            if (shipmentId.length > 0) {

                tvSelection.setText(shipmentId)
            } else {
                tvSelection.setHint("Delivery Number")
                btnInvoice.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnInvoice.setClickable(false)
                btnInvoice.setEnabled(false)
            }
        } else {
            var shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
            tvSelection.setText(shipmentId)
        }

        if (!podDo.invoice.equals("")) {

            btnInvoice.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnInvoice.setClickable(false)
            btnInvoice.setEnabled(false)
            var selectedId = tvSelection.text.toString().trim()

            var deliveryId = preferenceUtils.getStringFromPreference(PreferenceUtils.INVOICE_SHIPMENT_ID, "")
            if (deliveryId.length > 0 && selectedId.equals(deliveryId)) {
                btnInvoice.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnInvoice.setClickable(false)
                btnInvoice.setEnabled(false)

            } else {
                btnInvoice.setBackgroundColor(resources.getColor(R.color.md_green))
                btnInvoice.setClickable(true)
                btnInvoice.setEnabled(true)
            }
        } else {
            btnInvoice.setBackgroundColor(resources.getColor(R.color.md_green))
            btnInvoice.setClickable(true)
            btnInvoice.setEnabled(true)
        }
    }

    @SuppressLint("MissingPermission")
    override fun initializeControls() {
        tvScreenTitle.setText(R.string.invoice)
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
        btnInvoice = findViewById(R.id.btnInvoice) as Button
        tvSelection = findViewById(R.id.tvSelection) as TextView
        btnPreInvoice = findViewById(R.id.btnPreInvoice) as Button
        podDo = StorageManager.getInstance(this).getDepartureData(this);
        locationManager = (getSystemService(LOCATION_SERVICE) as LocationManager?)!!
        locationManager?.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0L, 0f, locationListener);

        btnPreInvoice.setOnClickListener {
            prePrepareInvoiceCreation()

        }





        btnInvoice.setOnClickListener {

            var shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
            var spotID = preferenceUtils.getStringFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER, "")

            if (shipmentId.length > 0) {
                val customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)
                var activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)

                val ShipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, getResources().getString(R.string.checkin_non_scheduled))
                if (ShipmentType.equals(getResources().getString(R.string.checkin_non_scheduled), true)) {
                    captureInfo(customerDo.invoiceEmail,ResultListner { `object`, isSuccess ->
                        if (isSuccess) {
                            customerManagement(customerDo.customerId, 2)
                        }
                    })
                } else {
                    captureInfo(activeDeliverySavedDo.invoiceEmail, ResultListner { `object`, isSuccess ->
                        if (isSuccess) {
                            customerManagement(activeDeliverySavedDo.customer, 2)
                        }
                    })

                }
//                if (ShipmentType.equals(getResources().getString(R.string.checkin_non_scheduled), true)) {
//
//                    customerManagement(customerDo.customerId, 2)
//                } else {
//                    customerManagement(activeDeliverySavedDo.customer, 2)
//
//                }
            } else if (spotID.length > 0) {
                val customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)
                captureInfo(customerDo.invoiceEmail, ResultListner { `object`, isSuccess ->
                    if (isSuccess) {
                        customerManagement(customerDo.customerId, 2)
                    }
                })
//                customerManagement(customerDo.customerId, 2)


            } else {
                showToast("Delivery not Found..")

            }


        }

        tvSelection.setOnClickListener {
            //            dialog = Dialog(this, R.style.NewDialog)
//            dialog.setCancelable(true)
//            dialog.setCanceledOnTouchOutside(true)
//            dialog.setContentView(R.layout.invoice_list_dialog)
//            val window = dialog.getWindow()
//            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)
//            var recyclerView = dialog.findViewById(R.id.recycleview) as RecyclerView
//            recyclerView.setLayoutManager(LinearLayoutManager(this));
//            preferenceUtils = PreferenceUtils(this@CreateInvoiceActivity)
//            var id = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "");
//            val siteListRequest = UnInvoicedDeliveryListRequest(this@CreateInvoiceActivity)
//            showLoader()
//            siteListRequest.setOnResultListener { isError, deliveryDOS ->
//                hideLoader()
//                deliveryMainDO = deliveryDOS
//                if (isError) {
//                    Toast.makeText(this@CreateInvoiceActivity, R.string.failed_load, Toast.LENGTH_SHORT).show()
//                } else {
//                    if (deliveryMainDO.deliveryDOS != null && deliveryMainDO.deliveryDOS.size > 0) {
//                        var siteAdapter = CreateInvoiceAdapter(this@CreateInvoiceActivity, deliveryMainDO.deliveryDOS)
//                        recyclerView.setAdapter(siteAdapter)
//                    } else {
//                    }
//
//                }
//            }
//            siteListRequest.execute()
//            dialog.show()


        }

    }

    val locationListener: LocationListener = object : LocationListener {
        override fun onLocationChanged(location: Location) {
            lattitude = location.latitude.toString()
            longitude = location.longitude.toString()

            var gcd = Geocoder(getBaseContext(), Locale.getDefault());

            try {
                var addresses = gcd.getFromLocation(location.getLatitude(),
                        location.getLongitude(), 1);
                if (addresses.size > 0) {
                    System.out.println(addresses.get(0).getLocality());
                    var cityName = addresses.get(0).getLocality();
                    address = cityName
                }
            } catch (e: Exception) {
                e.printStackTrace();
            }
        }

        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
        override fun onProviderEnabled(provider: String) {}
        override fun onProviderDisabled(provider: String) {}
    }

    override fun onButtonYesClick(from: String) {

        if ("SUCCESS".equals(from, ignoreCase = true)) {
//            val intent = Intent(this@CreateInvoiceActivity, DashBoardActivity::class.java)
//            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//
//            startActivity(intent)


        }
    }

    override fun onButtonNoClick(from: String) {

        if ("SUCCESS".equals(from, ignoreCase = true)) {

            finish()
        }
    }

    private fun customerManagement(id: String, type: Int) {
        if (Util.isNetworkAvailable(this)) {
            val request = CustomerManagementRequest(id, type, this)
            request.setOnResultListener { isError, customerDo ->
                hideLoader()
                if (customerDo != null) {
                    if (isError) {
                        showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
                    } else {
                        if (customerDo.flag.equals("Customer authorized for this transaction", true)) {
                            invoiceCreation()
                        } else {
                            showAppCompatAlert("Alert!", "Customer not authorized for this transaction", "Ok", "", "", false)

                        }
                    }
                } else {
                    showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
                }
            }
            request.execute()
        } else {
            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "FAILURE", false)

        }

    }

    private fun invoiceCreation() {
        if (Util.isNetworkAvailable(this)) {

            data = tvSelection.text.toString()
            if (data.length > 0) {
                val siteListRequest = CreateInvoiceRequest("",podDo.invoiceNotes, data, lattitude, longitude, address, this@CreateInvoiceActivity)
                siteListRequest.setOnResultListener { isError, createPaymentDO ->
                    hideLoader()
                    if (createPaymentDO != null) {


                        createInvoiceDO = createPaymentDO
                        if (isError) {
                            showAppCompatAlert("Error", "Invoice Not Created", "Ok", "", "SUCCESS", false)
                        } else {
                            if (createInvoiceDO.flag == 2) {
                                showToast("Invoice Created - " + createInvoiceDO.salesInvoiceNumber)

//                            showAppCompatAlert("Success", "Invoice Created - " + createInvoiceDO.salesInvoiceNumber, "Ok", "", "SUCCESS", false)
                                //Toast.makeText(this@CreateInvoiceActivity, "Invoice Created", Toast.LENGTH_SHORT).show()
                                preferenceUtils.saveString(PreferenceUtils.INVOICE_ID, createPaymentDO.salesInvoiceNumber)
                                preferenceUtils.saveString(PreferenceUtils.INVOICE_AMOUNT, createPaymentDO.amount)
                                preferenceUtils.saveString(PreferenceUtils.INVOICE_SHIPMENT_ID, createPaymentDO.o_shipmentNumber)
                                preferenceUtils.saveString(PreferenceUtils.PREVIEW_INVOICE_ID, createPaymentDO.salesInvoiceNumber)



                                podDo.invoice = createPaymentDO.salesInvoiceNumber;
                                StorageManager.getInstance(this).saveDepartureData(this, podDo)

                                btnInvoice.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                                btnInvoice.setClickable(false)
                                btnInvoice.setEnabled(false)

                                prepareInvoiceCreation()


                            } else if (createInvoiceDO.flag == 4) {
                                if (createInvoiceDO.message.length > 0) {
                                    showAppCompatAlert("Info!", "" + createInvoiceDO.message, "Ok", "", "SUCCESS", false)

                                } else {
                                    showAppCompatAlert("Error", "Invoice Not Created", "Ok", "", "SUCCESS", false)

                                }

                            } else {
                                if (createInvoiceDO.message.length > 0) {
                                    showAppCompatAlert("Info!", "" + createInvoiceDO.message, "Ok", "", "SUCCESS", false)

                                } else {
                                    showAppCompatAlert("Error", "Invoice Not Created", "Ok", "", "SUCCESS", false)

                                }

                            }

                        }
                    } else {
                        showAppCompatAlert("Error", "Invoice Not Created", "Ok", "", "SUCCESS", false)


                    }
                }

                siteListRequest.execute()
            } else {
                //   btnInvoice.isClickable=false
            }
        } else {
            showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

        }

    }

    private fun prepareInvoiceCreation() {
        var id = preferenceUtils.getStringFromPreference(PreferenceUtils.INVOICE_ID, "")
//        var id="CDC-U101-19000091"//
        if (id.length > 0) {
            if (Util.isNetworkAvailable(this)) {
                val siteListRequest = InvoiceDetailsRequest(id, this)
                siteListRequest.setOnResultListener { isError, createPDFInvoiceDO ->
                    hideLoader()
                    if (createPDFInvoiceDO != null) {
                        if (isError) {
                            showToast("Unable to send Email");
//                        showAppCompatAlert("Error", "Unable to send Email", "Ok", "", "", false)
                        } else {
                            createPDFInvoiceDo = createPDFInvoiceDO
//                        if(createPDFInvoiceDo.email==10){
//                            createPaymentPDF(createPDFInvoiceDO)
//                        }
//                        if(createPaymentDo.print==10){
//                            printDocument(createPDFpaymentDO, PrinterConstants.PrintPaymentReport)
//                        }
                            if (createInvoiceDO.email == 10) {
                                createInvoicePDF(createPDFInvoiceDO)
                            }
                            if (createInvoiceDO.print == 10) {
                                if (checkFilePermission()) {
                                    printDocument(createPDFInvoiceDO, PrinterConstants.PrintInvoiceReport);
                                }
                            }


                        }
                    } else {
                        showToast("Unable to send Email");

//                    showAppCompatAlert("Error", "Unable to send Email!!", "Ok", "", "", false)
                    }
                }
                siteListRequest.execute()
            } else {
//            showAppCompatAlert("Error", "Unable to send Email", "Ok", "", "Failure", false)
            }
        } else {
            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "", false)

        }


    }

    private fun checkFilePermission(): Boolean {
        if (ContextCompat.checkSelfPermission(this@CreateInvoiceActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this@CreateInvoiceActivity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), permissionRequestCode)
        } else {
            return true
        }
        return false
    }

    private fun createInvoicePDF(createPDFInvoiceDO: CreateInvoicePaymentDO) {
        BGInvoicePdf.getBuilder(this).build(createPDFInvoiceDO, "Email");
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == permissionRequestCode) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                createInvoicePDF(createPDFInvoiceDo)
            } else {
                Log.e(TAG, "WRITE_PERMISSION_DENIED")
            }
        }

    }

    private fun printDocument(obj: Any, from: Int) {
        val printConnection = PrinterConnection.getInstance(this@CreateInvoiceActivity)
        if (printConnection.isBluetoothEnabled()) {
            printConnection.connectToPrinter(obj, from)
        } else {
//            PrinterConstants.PRINTER_MAC_ADDRESS = ""
            showToast("Please enable your mobile Bluetooth.")
//            showAppCompatAlert("", "Please enable your mobile Bluetooth.", "Enable", "Cancel", "EnableBluetooth", false)
        }
    }

    private fun pairedDevices() {
        val pairedDevices = BluetoothAdapter.getDefaultAdapter().getBondedDevices()
        if (pairedDevices.size > 0) {
            for (device in pairedDevices) {
                val deviceName = device.getName()
                val mac = device.getAddress() // MAC address
//                if (StorageManager.getInstance(this).getPrinterMac(this).equals("")) {
                StorageManager.getInstance(this).savePrinterMac(this, mac);
//                }
                Log.e("Bluetooth", "Name : " + deviceName + " Mac : " + mac)
                break
            }
        }
    }

    override fun onResume() {
        super.onResume()
        pairedDevices()
    }

    override fun onStart() {
        super.onStart()
        val filter = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        registerReceiver(mReceiver, filter)
//        val filter = IntentFilter(Intent.PAIRIN.ACTION_FOUND Intent. "android.bluetooth.device.action.PAIRING_REQUEST");
//        registerReceiver(mReceiver, filter)
//        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
//        mBluetoothAdapter.startDiscovery()
//        val filter2 = IntentFilter( "android.bluetooth.device.action.PAIRING_REQUEST")
//        registerReceiver(mReceiver, filter2)

    }

    override fun onDestroy() {
        unregisterReceiver(mReceiver)
//        PrinterConstants.PRINTER_MAC_ADDRESS = ""
        super.onDestroy()
    }

    private val mReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            if (BluetoothDevice.ACTION_FOUND == action) {
                val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE);
                PrinterConstants.bluetoothDevices.add(device)
                Log.e("Bluetooth", "Discovered => name : " + device!!.name + ", Mac : " + device.address)
            } else if (action.equals(BluetoothDevice.ACTION_ACL_CONNECTED)) {
                Log.e("Bluetooth", "status : ACTION_ACL_CONNECTED")
                pairedDevices()
            } else if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                val state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);

                if (state == BluetoothAdapter.STATE_OFF) {
                    Log.e("Bluetooth", "status : STATE_OFF")
                } else if (state == BluetoothAdapter.STATE_TURNING_OFF) {
                    Log.e("Bluetooth", "status : STATE_TURNING_OFF")
                } else if (state == BluetoothAdapter.STATE_ON) {
                    Log.e("Bluetooth", "status : STATE_ON")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_TURNING_ON) {
                    Log.e("Bluetooth", "status : STATE_TURNING_ON")
                } else if (state == BluetoothAdapter.STATE_CONNECTING) {
                    Log.e("Bluetooth", "status : STATE_CONNECTING")
                } else if (state == BluetoothAdapter.STATE_CONNECTED) {
                    Log.e("Bluetooth", "status : STATE_CONNECTED")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_DISCONNECTED) {
                    Log.e("Bluetooth", "status : STATE_DISCONNECTED")
                }
            }
        }
    }

    private fun prePrepareInvoiceCreation() {
        var id = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
        if (id.isEmpty()) {
            id = preferenceUtils.getStringFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER, "")

        }
//        var id="CNV-U102-19000144"
        if (id.length > 0) {
            if (Util.isNetworkAvailable(this)) {
                val siteListRequest = PreInvoiceDetailsRequest(id, this)
                siteListRequest.setOnResultListener { isError, createPDFInvoiceDO ->
                    hideLoader()
                    if (createPDFInvoiceDO != null) {
                        if (isError) {
                            showAppCompatAlert("Error", "Invoice pdf api error", "Ok", "", "", false)
                        } else {
                            createPDFInvoiceDo = createPDFInvoiceDO

                            if (checkFilePermission()) {
                                PreInvoicePdf.getBuilder(this).createPDF(tvSelection.text.toString().trim(), createPDFInvoiceDO, "Preview");
                            }

                        }
                    } else {
                        showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
                    }
                }
                siteListRequest.execute()
            } else {
                showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "", false)

            }

        } else {
            showAppCompatAlert("Error", "No Invoice Id Found", "Ok", "", "Failure", false)

        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == 10) {
            btnPreInvoice.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnPreInvoice.setClickable(false)
            btnPreInvoice.setEnabled(false)
            val intent = Intent();
            intent.putExtra("INVOICE", data)
            setResult(10, intent)
            finish()
        }

    }

}