package com.tbs.brothersgas.haadhir.Activitys

import android.content.Intent
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.brothersgas.haadhir.Adapters.PaymentAdapter
import com.tbs.brothersgas.haadhir.Model.LoadStockDO
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.CustomerManagementRequest
import com.tbs.brothersgas.haadhir.Requests.PaymentHistoryRequest
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import com.tbs.brothersgas.haadhir.utils.Util
import java.util.ArrayList


class SalesPaymentsActivity : BaseActivity() {
    lateinit var invoiceAdapter: PaymentAdapter
    lateinit var loadStockDOs: ArrayList<LoadStockDO>
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var tvNoDataFound: TextView
    var fromId = 0

    override protected fun onResume() {
        super.onResume()
    }

    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.framgment_sales_payments_list, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
    }

    override fun initializeControls() {
        tvScreenTitle.setText(R.string.sales_payments)
        recycleview = findViewById(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        tvNoDataFound = findViewById(R.id.tvNoDataFound) as TextView

        recycleview.setLayoutManager(linearLayoutManager)
        if (intent.hasExtra("Sales")) {
            fromId = intent.extras.getInt("Sales")
        }
        val btnCreate = findViewById(R.id.btnCreate) as Button
        if (Util.isNetworkAvailable(this)) {

            val driverListRequest = PaymentHistoryRequest("", this@SalesPaymentsActivity)
            driverListRequest.setOnResultListener { isError, invoiceHistoryMainDO ->
                hideLoader()
                if (isError) {
                    tvNoDataFound.setVisibility(View.VISIBLE)
                    recycleview.setVisibility(View.GONE)
                    Toast.makeText(this@SalesPaymentsActivity, resources.getString(R.string.error_NoData), Toast.LENGTH_SHORT).show()
                } else {

                    if (invoiceHistoryMainDO.paymentHistoryDOS.size > 0) {
                        tvNoDataFound.setVisibility(View.GONE)
                        recycleview.setVisibility(View.VISIBLE)
                        invoiceAdapter = PaymentAdapter(this@SalesPaymentsActivity, invoiceHistoryMainDO.paymentHistoryDOS)
                        recycleview.setAdapter(invoiceAdapter)
                    } else {
                        tvNoDataFound.setVisibility(View.VISIBLE)
                        recycleview.setVisibility(View.GONE)
                    }


                }
            }
            driverListRequest.execute()
        } else {
            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "",false)

        }

        btnCreate.setOnClickListener {
            val customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)
            var activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)
            val ShipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, getResources().getString(R.string.checkin_non_scheduled))
            if (ShipmentType.equals(getResources().getString(R.string.checkin_non_scheduled), true)) {

                customerManagement(customerDo.customerId, 3)
            } else {
                customerManagement(activeDeliverySavedDo.customer, 3)

            }

        }


    }

    private fun customerManagement(id: String, type: Int) {
        if (Util.isNetworkAvailable(this)) {

            val request = CustomerManagementRequest(id, type, this)
            request.setOnResultListener { isError, customerDo ->
                hideLoader()
                if (customerDo != null) {
                    if (isError) {
                        showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
                    } else {
                        if (customerDo.flag.equals("Customer authorized for this transaction", true)) {
                            val intent = Intent(this@SalesPaymentsActivity, InvoiceListActivity::class.java);
                            intent.putExtra("Sales", fromId)
                            startActivity(intent);
                        } else {
                            showAppCompatAlert("Alert!", "Customer not authorized for this transaction", "Ok", "", "", false)

                        }
                    }
                } else {
                    showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
                }
            }
            request.execute()
        } else {
            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "", false)

        }

    }

}