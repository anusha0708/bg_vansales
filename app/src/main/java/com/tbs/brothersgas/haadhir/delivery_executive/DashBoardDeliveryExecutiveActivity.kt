package com.tbs.brothersgas.haadhir.delivery_executive

import android.annotation.TargetApi
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.brothersgas.haadhir.Activitys.BaseActivity
import com.tbs.brothersgas.haadhir.Model.CustomerDo
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.collector.CollectorCustomerAdapter
import com.tbs.brothersgas.haadhir.collector.CollectorCustomerNewRequest
import com.tbs.brothersgas.haadhir.collector.CollectorPreviewActivity
import com.tbs.brothersgas.haadhir.collector.TransactionListActivity
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.pdfs.utils.PDFConstants
import com.tbs.brothersgas.haadhir.utils.Constants
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils
import kotlinx.android.synthetic.main.activity_dashboard_delivery_executive.*

class DashBoardDeliveryExecutiveActivity : BaseActivity() {
    lateinit var userId: String
    lateinit var orderCode: String
    lateinit var llOrderHistory: View
    lateinit var customerDos: ArrayList<CustomerDo>
    private lateinit var btnAddMore: Button
    lateinit var llSearch: LinearLayout
    lateinit var etSearch: EditText
    var REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 200
    override fun initialize() {
        llOrderHistory = layoutInflater.inflate(R.layout.activity_dashboard_delivery_executive, null)
        llBody.addView(llOrderHistory, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            finishAffinity()
        }
        disableMenuWithBackButton()
        initializeControls()

        tvScreenTitle.setText("Delivery Executive")
        ivMenu.visibility = View.VISIBLE

    }

    override fun initializeControls() {
        insertDummyContactWrapper()
        ivMenu.setOnClickListener {
            //
            val popup = PopupMenu(this@DashBoardDeliveryExecutiveActivity, ivMenu)
            popup.getMenuInflater().inflate(R.menu.popup_menu_delivery_executive, popup.getMenu())
            popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                override fun onMenuItemClick(item: MenuItem): Boolean {
                    if (item.title.equals("Logout")) {
                        logOut()
                    }

                    return true

                }


            })
            popup.show()//showing popup menu

        }


        val intent = Intent(this, DeliveryExecutiveListActivity::class.java)
        ll_todo.setOnClickListener {
            intent.putExtra(Constants.SCREEN_TYPE, 1)
            startActivity(intent)
        }
        ll_completed.setOnClickListener {
            intent.putExtra(Constants.SCREEN_TYPE, 2)
            startActivity(intent)
        }
        ll_activity.setOnClickListener {
            val intent = Intent(this, DeliveryExecutiveActivityReportActivity::class.java)
            startActivity(intent)
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private fun insertDummyContactWrapper() {
        val permissionsNeeded = ArrayList<String>()
        val permissionsList = ArrayList<String>()
        if (!addPermission(permissionsList, android.Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Storage")
        if (permissionsList.size > 0) {
            requestPermissions(permissionsList.toArray(arrayOfNulls<String>(permissionsList.size)),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS)
            return
        }
    }

    private fun addPermission(permissionsList: MutableList<String>, permission: String): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission)
                // Check for Rationale Option
                if (!shouldShowRequestPermissionRationale(permission))
                    return false
            }
        }
        return true
    }

    private fun setIvMenu() {
        val popup = PopupMenu(this, ivMenu)
        popup.getMenuInflater().inflate(R.menu.popup_menu_delivery_executive, popup.getMenu())

        popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
            override fun onMenuItemClick(item: MenuItem): Boolean {
                if (item.title.equals("Logout")) {
                    logOut()
                }
                return true

            }
        })
        popup.show()//showing popup menu

    }
}

