package com.tbs.brothersgas.haadhir.Activitys

import android.app.Activity
import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.PixelFormat
import android.location.Location
import android.location.LocationListener
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.provider.Settings
import android.text.InputFilter
import android.text.Spanned
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityCompat.startActivityForResult
import androidx.core.content.ContextCompat.startActivity
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.AppCompatDialog
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryMainDO
import com.tbs.brothersgas.haadhir.Model.CustomerDo
import com.tbs.brothersgas.haadhir.R
import com.tbs.brothersgas.haadhir.Requests.ActiveDeliveryRequest
import com.tbs.brothersgas.haadhir.Requests.ActiveDeliveryTimeCaptureRequest
import com.tbs.brothersgas.haadhir.Requests.SkipShipmentRequest
import com.tbs.brothersgas.haadhir.common.AppConstants
import com.tbs.brothersgas.haadhir.database.StorageManager
import com.tbs.brothersgas.haadhir.utils.*
import kotlinx.android.synthetic.main.new_load_vansales_tab.*

//
class ActiveDeliveryActivity : BaseActivity(), OnMapReadyCallback, LocationListener {

    lateinit var googleMap: GoogleMap
    private var shipmentId: String = ""
    private var shipmentType: String = ""
    lateinit var tvDateTime: TextView
    lateinit var tvEmail: TextView
    lateinit var tvName: TextView
    lateinit var tvPhoneNumber: TextView
    lateinit var activeDeliveryDo: ActiveDeliveryMainDO
    lateinit var customerDo: CustomerDo

    lateinit var btnReturnDetails: Button
    lateinit var btnPod: LinearLayout
    lateinit var tvShipmentList: TextView
    lateinit var btnStartRoute: LinearLayout
    lateinit var btnCancelRoute: LinearLayout
    lateinit var ivNavigate: ImageView
    lateinit var itemName: String
    var itemSite: Int = 0
    lateinit var itemNumber: String

    lateinit var btnSkip: LinearLayout
    //    lateinit var mGetServiceClick: GetFloatingIconClick
    var distance: Double = 0.0

    override fun initialize() {
        val llCategories = getLayoutInflater().inflate(R.layout.new_active_delivery, null) as ScrollView
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        disableMenuWithBackButton()
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            val intent = Intent()
            intent.putExtra("CapturedReturns", AppConstants.CapturedReturns)
            setResult(19, intent)
            finish()
        }
//        mGetServiceClick = GetFloatingIconClick()

        initializeControls()
        customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)

        activeDeliveryDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)
        tvName.setText(activeDeliveryDo.customerDescription)
        tvPhoneNumber.setText(activeDeliveryDo.mobile)
        tvEmail.setText(activeDeliveryDo.email)
        tvDateTime.setText(activeDeliveryDo.customerStreet + activeDeliveryDo.customerLandMark + activeDeliveryDo.customerTown + " " + activeDeliveryDo.customerCity + " " + activeDeliveryDo.countryName)

        var podDo = StorageManager.getInstance(this).getDepartureData(this);

        shipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, "")
        if (shipmentType.equals(getResources().getString(R.string.checkin_scheduled), true)) {
            btnCancelRoute.setVisibility(View.GONE)
            btnSkip.setVisibility(View.VISIBLE)
            tvShipmentList.setVisibility(View.VISIBLE)
        } else {
            btnCancelRoute.setVisibility(View.VISIBLE)
            btnCancelRoute.setBackgroundColor(resources.getColor(R.color.white))
            btnCancelRoute.setClickable(true)
            btnCancelRoute.setEnabled(true)
            btnSkip.setVisibility(View.GONE)
            tvShipmentList.setVisibility(View.GONE)

        }
        if (podDo != null && !podDo.podTimeCaptureCaptureDeliveryTime.equals("")) {
            btnCancelRoute.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnCancelRoute.setClickable(false)
            btnCancelRoute.setEnabled(false)
        }

        if (!activeDeliveryDo.podTime.equals("")) {
            btnPod.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnPod.setClickable(false)
            btnPod.setEnabled(false)
        }
        if (!activeDeliveryDo.CaptureReturnsTime.equals("")) {
            btnReturnDetails.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnReturnDetails.setClickable(false)
            btnReturnDetails.setEnabled(false)

        }

        if (intent.hasExtra("SHIPMENT_ID")) {
            shipmentId = intent.extras.getString("SHIPMENT_ID")
        }


        if (shipmentType.equals(getResources().getString(R.string.checkin_scheduled), true) && shipmentId.length > 0) {
            if (Util.isNetworkAvailable(this)) {
                val driverListRequest = ActiveDeliveryRequest(shipmentId, this@ActiveDeliveryActivity)
                driverListRequest.setOnResultListener { isError, activeDeliveryMainDo ->
                    hideLoader()
                    if (isError) {
                        Toast.makeText(this@ActiveDeliveryActivity, R.string.error_NoData, Toast.LENGTH_SHORT).show()
                    } else {
                        if (intent.hasExtra("Scheduled_customer")) {
                            intent.getExtras().getString("Scheduled_customer")
                        }
                        activeDeliveryDo = activeDeliveryMainDo;
                        val activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)
                        activeDeliveryDo.startRouteTime = activeDeliverySavedDo.startRouteTime;
                        activeDeliveryDo.podTime = activeDeliverySavedDo.podTime;
                        activeDeliveryDo.CaptureReturnsTime = activeDeliverySavedDo.CaptureReturnsTime;
//                    activeDeliveryDo.customer = activeDeliverySavedDo.customer;
//                    activeDeliveryDo.customerDescription = activeDeliverySavedDo.customerDescription;

                        StorageManager.getInstance(this).saveActiveDeliveryMainDo(this, activeDeliveryDo)

                        tvName.setText(activeDeliveryDo.customerDescription)
                        tvPhoneNumber.setText(activeDeliveryDo.mobile)
                        tvEmail.setText(activeDeliveryDo.email)
                        tvDateTime.setText(activeDeliveryDo.customerStreet + activeDeliveryDo.customerLandMark + activeDeliveryDo.customerTown + " " + activeDeliveryDo.customerCity + " " + activeDeliveryDo.countryName)

                        val arrivalTime = activeDeliveryDo.arrivalTime.substring(Math.max(activeDeliveryDo.arrivalTime.length - 2, 0))
                        val departureTime = activeDeliveryDo.departureTime.substring(Math.max(activeDeliveryDo.departureTime.length - 2, 0))
                        println(arrivalTime)
                        println(departureTime)
//                    if (arrivalTime.length > 0) {
//
//                        arrivalTime2 = activeDeliveryDo.arrivalTime.substring(0, 2)
//                        departureTime2 = activeDeliveryDo.departureTime.substring(0, 2)
//                      //  holder.tvTime.setText("ETA : $arrivalTime2:$arrivalTime  ETD : $departureTime2:$departureTime")
//                    }
                        val arrivalTime2: String
                        val departureTime2: String
                        val aMonth: String
                        val ayear: String
                        val aDate: String
                        val dMonth: String
                        val dyear: String
                        val dDate: String
                        try {
                            arrivalTime2 = activeDeliveryDo.arrivalTime.substring(0, 2)
                            departureTime2 = activeDeliveryDo.departureTime.substring(0, 2)
                            aMonth = activeDeliveryDo.arraivalDate.substring(4, 6)
                            ayear = activeDeliveryDo.arraivalDate.substring(0, 4)
                            aDate = activeDeliveryDo.arraivalDate.substring(Math.max(activeDeliveryDo.arraivalDate.length - 2, 0))
                            dMonth = activeDeliveryDo.departureDate.substring(4, 6)
                            dyear = activeDeliveryDo.departureDate.substring(0, 4)
                            dDate = activeDeliveryDo.departureDate.substring(Math.max(activeDeliveryDo.departureDate.length - 2, 0))
                            preferenceUtils.saveString(PreferenceUtils.SHIPMENT_DT, dDate + "-" + dMonth + "-" + dyear + "  " + departureTime2 + ":" + departureTime)
                            preferenceUtils.saveString(PreferenceUtils.SHIPMENT_AT, aDate + "-" + aMonth + "-" + ayear + "  " + arrivalTime2 + ":" + arrivalTime)

                        } catch (e: Exception) {

                        }


                        /*if(!activeDeliveryDo.startRouteTime.equals("")){
                            btnStartRoute.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                            btnStartRoute.setClickable(false)
                            btnStartRoute.setEnabled(false)
                        }*/
                        if (!activeDeliveryDo.podTime.equals("")) {
                            btnPod.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                            btnPod.setClickable(false)
                            btnPod.setEnabled(false)

                        }
                        if (!activeDeliveryDo.CaptureReturnsTime.equals("")) {
                            btnReturnDetails.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                            btnReturnDetails.setClickable(false)
                            btnReturnDetails.setEnabled(false)
                        }

                    }
                }
                driverListRequest.execute()
            } else {
                showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.internet_connection), "" + resources.getString(R.string.ok), "", "", false)

            }


        } else {
            val customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)
            tvName.setText("" + customerDo.customerId)
            tvEmail.setText("" + customerDo.email)
            tvPhoneNumber.setText("" + customerDo.mobile)
            tvDateTime.setText(customerDo.city + customerDo.landmark + customerDo.town + " " + customerDo.businessLine + " " + customerDo.countryName)

        }

        btnCancelRoute.setOnClickListener {
            var podDo = StorageManager.getInstance(this).getDepartureData(this);
            if (podDo != null && !podDo.arrivalTime.equals("")) {
                showToast("You Arrived for pod so cannot use now..")
            } else {
                StorageManager.getInstance(this@ActiveDeliveryActivity).deleteCurrentSpotSalesCustomer(this@ActiveDeliveryActivity)
                setResult(63)
                finish()
//                showToast("you can use this function now...")

            }


        }

    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 11 && resultCode == 11) {//CaptureReturnDetailsActivity
            activeDeliveryDo.CaptureReturnsTime = CalendarUtils.getCurrentDate();
//            AppConstants.CapturedReturns= true
//            btnReturnDetails.setBackgroundColor(resources.getColor(R.color.md_gray_light))
//            btnReturnDetails.setClickable(false)
//            btnReturnDetails.setEnabled(false)
//            StorageManager.getInstance(this).saveActiveDeliveryMainDo(this, activeDeliveryDo)
        } else if (requestCode == 13 && resultCode == 13) {
            activeDeliveryDo.startRouteTime = CalendarUtils.getCurrentDate();
            /*btnStartRoute.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnStartRoute.setClickable(false)
            btnStartRoute.setEnabled(false)*/
            StorageManager.getInstance(this).saveActiveDeliveryMainDo(this, activeDeliveryDo)
        } else if (requestCode == 12 && resultCode == 12) {
            if (data != null && data.hasExtra("CapturedReturns") && data.getBooleanExtra("CapturedReturns", false)) {
                AppConstants.CapturedReturns = true
                btnPod.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnPod.setClickable(false)
                btnPod.setEnabled(false)
                btnStartRoute.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnStartRoute.setClickable(false)
                btnStartRoute.setEnabled(false)
                btnReturnDetails.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnReturnDetails.setClickable(false)
                btnReturnDetails.setEnabled(false)
                btnCancelRoute.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnCancelRoute.setClickable(false)
                btnCancelRoute.setEnabled(false)
                val intent = Intent()
                intent.putExtra("CapturedReturns", AppConstants.CapturedReturns)
                setResult(19, intent)
                finish()
                // uncomment if required
            }
        } else if (requestCode == 201 && resultCode == 52) {
            val intent = Intent();
            intent.putExtra("SkipShipment", "Skip")
            setResult(52, intent)
            finish()
        }
        else if (requestCode == 12 && resultCode == 52) {
            val intent = Intent();
            intent.putExtra("SkipShipment", "Skip")
            setResult(52, intent)
            finish()
        }
        else if (requestCode == 12 && resultCode == 53) {
            val intent = Intent();
            intent.putExtra("SkipShipment", "Cancel")
            setResult(resultCode, intent)
            finish()
        } else if (requestCode == 201 && resultCode == 201) {
            finish()
        } else if (resultCode == Activity.RESULT_OK && requestCode == 10) {
            if (data != null) {

                val site = data.getStringExtra("site_id");
                preferenceUtils.saveString(PreferenceUtils.B_SITE_ID, site);
                startPodActivity()
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }


    override fun initializeControls() {
        toolbar.setNavigationIcon(R.drawable.back)

        tvScreenTitle.setText(R.string.active_delivery)
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment!!.getMapAsync(this@ActiveDeliveryActivity)
        tvName = findViewById(R.id.tvName) as TextView
        tvEmail = findViewById(R.id.tvEmail) as TextView
        tvPhoneNumber = findViewById(R.id.tvPhoneNumber) as TextView
        tvDateTime = findViewById(R.id.tvDateTime) as TextView
        tvShipmentList = findViewById(R.id.tvShipmentList) as TextView
        ivNavigate = findViewById(R.id.ivNavigate) as ImageView
        btnCancelRoute = findViewById(R.id.btnCancelRoute) as LinearLayout
        btnStartRoute = findViewById(R.id.btnStartRoute) as LinearLayout
        btnSkip = findViewById(R.id.btnSkip) as LinearLayout
        btnPod = findViewById(R.id.btnPod) as LinearLayout
        btnReturnDetails = findViewById(R.id.btnReturnDetails) as Button
        var podDo = StorageManager.getInstance(this).getDepartureData(this);
        if (podDo != null && !podDo.podTimeCaptureCaptureDeliveryTime.equals("")) {
            btnCancelRoute.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnCancelRoute.setClickable(false)
            btnCancelRoute.setEnabled(false)
        } else {
            btnCancelRoute.setBackgroundColor(resources.getColor(R.color.white))
            btnCancelRoute.setClickable(true)
            btnCancelRoute.setEnabled(true)

        }
        btnSkip.setOnClickListener {
            val podDo = StorageManager.getInstance(this).getDepartureData(this);
            if (podDo == null || podDo.arrivalTime.equals("")) {
                val intent = Intent(this@ActiveDeliveryActivity, SkipActivity::class.java);
                startActivityForResult(intent, 201);
//                skipShipment()
            } else {
                showToast("You cannot use this function now")

            }
        }
        btnStartRoute.setOnClickListener {
            if (shipmentType.equals(getResources().getString(R.string.checkin_scheduled), true) && shipmentId.length > 0) {

                if (intent.hasExtra("DISTANCE")) {
                    distance = intent.getExtras().getDouble("DISTANCE")
                }
//            var distance  = preferenceUtils.getDoubleFromPreference(PreferenceUtils.DISTANCE, 0.0)
                if (distance.equals(0.0)) {

                    activeDeliveryDo.startRouteTime = CalendarUtils.getCurrentDate();
                    StorageManager.getInstance(this).saveActiveDeliveryMainDo(this, activeDeliveryDo)

                    showAlert("Destination Reached Already..")

                } else {
                    activeDeliveryDo.startRouteTime = CalendarUtils.getCurrentDate();
                    activeDeliveryDo.timeCaptureStartRouteDate = CalendarUtils.getDate()

                    activeDeliveryDo.timeCaptureStartRouteTime = CalendarUtils.getTime()

                    activeDeliveryTimeCapture()

                    StorageManager.getInstance(this).saveActiveDeliveryMainDo(this, activeDeliveryDo)
                    if (intent.hasExtra("CustomerId")) {
                        preferenceUtils.saveString(PreferenceUtils.CustomerId, intent.getExtras().getString("CustomerId"));
                    }
                    val latitude = activeDeliveryDo.longitude//"28.7041"
                    val longitude = activeDeliveryDo.lattitude//"77.1025"
                    val gmmIntentUri = "google.navigation:q=" + latitude + "," + longitude + "&mode=d"
                    val mapIntent = Intent(Intent.ACTION_VIEW, Uri.parse(gmmIntentUri))
                    mapIntent.setPackage("com.google.android.apps.maps")
                    try {
                        if (mapIntent.resolveActivity(packageManager) != null) {
                            startActivityForResult(mapIntent, 1542)
                        }
                    } catch (e: Exception) {
                        Log.e("Map Loading", "onClick: NullPointerException: Couldn't open map." + e.message)
                        Toast.makeText(this, "Couldn't open map", Toast.LENGTH_SHORT).show()
                    }
                }

            } else {
                val customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)
                if (intent.hasExtra("DISTANCE")) {
                    distance = intent.getExtras().getDouble("DISTANCE")
                }
//            var distance  = preferenceUtils.getDoubleFromPreference(PreferenceUtils.DISTANCE, 0.0)
//                if (distance.equals(0.0)) {
//
//                    activeDeliveryDo.startRouteTime = CalendarUtils.getCurrentDate();
//                    StorageManager.getInstance(this).saveActiveDeliveryMainDo(this, activeDeliveryDo)
//                    showAlert("Destination Reached Already..")
//
//                } else {
                activeDeliveryDo.startRouteTime = CalendarUtils.getCurrentDate();
                activeDeliveryDo.timeCaptureStartRouteDate = CalendarUtils.getDate()
                activeDeliveryDo.timeCaptureStartRouteTime = CalendarUtils.getTime()
                activeDeliveryTimeCapture()
                StorageManager.getInstance(this).saveActiveDeliveryMainDo(this, activeDeliveryDo)
                if (intent.hasExtra("CustomerId")) {
                    preferenceUtils.saveString(PreferenceUtils.CustomerId, intent.getExtras().getString("CustomerId"));
                }
                val latitude = customerDo.longitude//"28.7041"
                val longitude = customerDo.lattitude//"77.1025"
                if (latitude.isNotEmpty() && longitude.isNotEmpty()) {
                    val gmmIntentUri = "google.navigation:q=" + latitude + "," + longitude + "&mode=d"
                    val mapIntent = Intent(Intent.ACTION_VIEW, Uri.parse(gmmIntentUri))
                    mapIntent.setPackage("com.google.android.apps.maps")
                    try {
                        if (mapIntent.resolveActivity(packageManager) != null) {
                            startActivityForResult(mapIntent, 1542)
                        }
                    } catch (e: Exception) {
                        Log.e("Map Loading", "onClick: NullPointerException: Couldn't open map." + e.message)
                        Toast.makeText(this, "Couldn't open map", Toast.LENGTH_SHORT).show()
                    }
                } else {
                    val gmmIntentUri = "google.navigation:q=" + latitude + " " + longitude + "&mode=d"
                    val mapIntent = Intent(Intent.ACTION_VIEW, Uri.parse(gmmIntentUri))
                    mapIntent.setPackage("com.google.android.apps.maps")
                    try {
                        if (mapIntent.resolveActivity(packageManager) != null) {
                            startActivityForResult(mapIntent, 1542)
                        }
                    } catch (e: Exception) {
                        Log.e("Map Loading", "onClick: NullPointerException: Couldn't open map." + e.message)
                        Toast.makeText(this, "Couldn't open map", Toast.LENGTH_SHORT).show()
                    }
                }

//                }


            }

//            var lon = "17.4484823";
//            var lat = "78.3719473";
//            var type = "drive";
//            var str = "com.sygic.aura://coordinate|" + lat + "|" + lon + "|" + type;
//            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(str)));
        }
        ivNavigate.setOnClickListener {
            //            askDrawOverPermission();
            //            val intent = Intent(this@ActiveDeliveryActivity, TBSMapsNavigationActivity::class.java);
//            startActivity(intent);

            val latitude = activeDeliveryDo.lattitude//"28.7041"
            val longitude = activeDeliveryDo.longitude//"77.1025"
            val gmmIntentUri = "google.navigation:q=" + latitude + "," + longitude + "&mode=d"
            val mapIntent = Intent(Intent.ACTION_VIEW, Uri.parse(gmmIntentUri))
            mapIntent.setPackage("com.google.android.apps.maps")
            try {
                if (mapIntent.resolveActivity(packageManager) != null) {
                    startActivityForResult(mapIntent, 1542)
                    onUserLeaveHint()
                }
            } catch (e: Exception) {
                Log.e("Map Loading", "onClick: NullPointerException: Couldn't open map." + e.message)
                Toast.makeText(this, "Couldn't open map", Toast.LENGTH_SHORT).show()
            }
        }
        btnPod.setOnClickListener {
            //            startPodMActivity()
            if (shipmentType.equals(getResources().getString(R.string.checkin_scheduled), true)) {
                if (preferenceUtils.getStringFromPreference(PreferenceUtils.POD_NUMBER, "").isEmpty()
                        || preferenceUtils.getStringFromPreference(PreferenceUtils.POD_NAME, "").isEmpty()) {
                    captureInfo(1)

                } else {
                    startPodActivity()

                }
            } else {
                if (preferenceUtils.getbooleanFromPreference(PreferenceUtils.IS_MULTI_SITE, false)) {
                    if (preferenceUtils.getStringFromPreference(PreferenceUtils.POD_NUMBER, "").isEmpty()
                            || preferenceUtils.getStringFromPreference(PreferenceUtils.POD_NAME, "").isEmpty()) {
                        captureInfo(2)

                    } else {
                        startPodActivity()

                    }

                } else {
                    if (preferenceUtils.getStringFromPreference(PreferenceUtils.POD_NUMBER, "").isEmpty()
                            || preferenceUtils.getStringFromPreference(PreferenceUtils.POD_NAME, "").isEmpty()) {
                        captureInfo(1)

                    } else {
                        startPodActivity()

                    }
                }


            }
        }

        tvShipmentList.setOnClickListener {
            var shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
            if (!shipmentId.equals("")) {
                val intent = Intent(this@ActiveDeliveryActivity, ShipmentDetailsActivity::class.java);
                startActivity(intent);
            }
        }

        btnReturnDetails.setOnClickListener {
            if (activeDeliveryDo.startRouteTime.equals("")) {
                showToast("You have to start the route first.")
            } else {
                val intent = Intent(this@ActiveDeliveryActivity, CaptureReturnDetailsActivity::class.java);
                startActivityForResult(intent, 11);
            }
        }
    }

    private fun startPodActivity() {

        if (activeDeliveryDo.startRouteTime.equals("")) {
            if (intent.hasExtra("DISTANCE")) {
                distance = intent.getExtras().getDouble("DISTANCE")
            }
//            var distance  = preferenceUtils.getDoubleFromPreference(PreferenceUtils.DISTANCE, 0.0)
            if (distance.equals(0.0)) {
                if (preferenceUtils.getStringFromPreference(PreferenceUtils.POD_NUMBER, "").isEmpty()
                        || preferenceUtils.getStringFromPreference(PreferenceUtils.POD_NAME, "").isEmpty()) {
                    captureInfo(1)

                } else {
                    if (customerDo.customerId.equals(resources.getString(R.string.u109))) {
                        MRpod()
                    } else {
                        val intent = Intent(this@ActiveDeliveryActivity, PODActivity::class.java);
                        intent.putExtra("DISTANCE", distance)
                        startActivityForResult(intent, 12);
                    }
                }


            } else {
                showToast("You have to start the route first.")

            }
        } else if (!activeDeliveryDo.podTime.equals("")) {
            showToast("You have already done the pod.")
        } else {
            if (preferenceUtils.getStringFromPreference(PreferenceUtils.POD_NUMBER, "").isEmpty()
                    || preferenceUtils.getStringFromPreference(PreferenceUtils.POD_NAME, "").isEmpty()) {
                captureInfo(1)

            } else {
                if (customerDo.customerId.equals(resources.getString(R.string.u109))) {
                    MRpod()
                } else {
                    val intent = Intent(this@ActiveDeliveryActivity, PODActivity::class.java);
                    intent.putExtra("DISTANCE", distance)
                    startActivityForResult(intent, 12);
                }
            }


        }
    }

    fun captureInfo(site: Int) {
        try {
            val dialog = AppCompatDialog(this, R.style.AppCompatAlertDialogStyle)
            dialog.supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
            val view = LayoutInflater.from(this).inflate(R.layout.info_capture, null)
            val etName = view.findViewById<EditText>(R.id.etName)
            val etNumber = view.findViewById<EditText>(R.id.etNumber)

            dialog.setCancelable(true)
            dialog.setCanceledOnTouchOutside(true)
            val btnSubmit = view.findViewById<Button>(R.id.btnSubmit)

            btnSubmit.setOnClickListener {
                itemSite = site
                itemName = etName.text.toString()
                itemNumber = etNumber.text.toString()

                if (itemName.isEmpty()) {
                    showToast("Please enter name")
                }
//                 else if (itemName.length<2) {
//                    showToast("Please enter valid name")
//
//                }
                else if (itemNumber.isEmpty()) {
                    showToast("Please enter mobile number")

                } else {
//                     val REG = "^\\+[0-9]{10,14}\$"
//                    var PATTERN: java.util.regex.Pattern = java.util.regex.Pattern.compile(REG)
//                    fun CharSequence.isPhoneNumber() : Boolean = PATTERN.matcher(this).find()
//                    if (!itemNumber.isPhoneNumber()) {
//                        Toast.makeText(this, "Please enter " + "" + " valid phone number", Toast.LENGTH_SHORT).show();
//                    } else {
                    try {
                        val dialog2 = AppCompatDialog(this, R.style.AppCompatAlertDialogStyle)
                        dialog2.supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
                        val view = LayoutInflater.from(this).inflate(R.layout.confirm_info_capture, null)
                        val etName1 = view.findViewById<TextView>(R.id.etName)
                        val etNumber2 = view.findViewById<TextView>(R.id.etNumber)
                        etName1.setText(itemName)
                        etNumber2.setText(itemNumber)
                        dialog2.setCancelable(true)
                        dialog2.setCanceledOnTouchOutside(true)
                        val btnSubmit = view.findViewById<Button>(R.id.btnConfirm)
                        val btnBack = view.findViewById<Button>(R.id.btnBack)
                        btnBack.setOnClickListener {
                            dialog2.dismiss()
                        }

                        btnSubmit.setOnClickListener {
                            itemSite = site
                            var itemName1 = etName1.text.toString()
                            var itemNumber1 = etNumber2.text.toString()

                            if (itemNumber1.isEmpty()) {
                                showToast("Please enter mobile number")
                            } else if (itemName1.isEmpty()) {
                                showToast("Please enter name")
                            } else {
                                dialog.dismiss()
                                dialog2.dismiss()
                                preferenceUtils.saveString(PreferenceUtils.POD_NAME, itemName)
                                preferenceUtils.saveString(PreferenceUtils.POD_NUMBER, itemNumber)
                                if (itemSite == 1) {
                                    if (shipmentType.equals(getResources().getString(R.string.checkin_scheduled), true)) {
                                        val intent = Intent(this@ActiveDeliveryActivity, PODActivity::class.java);
                                        intent.putExtra("DISTANCE", distance)
                                        startActivityForResult(intent, 12);
                                    }else{
                                        if (customerDo.customerId.equals(resources.getString(R.string.u109))) {
                                            MRpod()
                                        } else {
                                            val intent = Intent(this@ActiveDeliveryActivity, PODActivity::class.java);
                                            intent.putExtra("DISTANCE", distance)
                                            startActivityForResult(intent, 12);
                                        }
                                    }
                                } else {
                                    val intent = Intent(this, MultiSiteListActivity::class.java)
                                    intent.putExtra(Constants.IS_FROM_DELIVERY, true)
                                    startActivityForResult(intent, 10)
                                }
                            }
                        }


                        dialog2.setContentView(view)
                        if (!dialog2.isShowing)
                            dialog2.show()
                    } catch (e: Exception) {
//                        }
                    }
                }
            }


            dialog.setContentView(view)
            if (!dialog.isShowing)
                dialog.show()
        } catch (e: Exception) {
        }

    }

    override fun onBackPressed() {

        super.onBackPressed()
    }

    fun MRpod(){
        val mrCode = preferenceUtils.getStringFromPreference(PreferenceUtils.MR_CODE, "")
        if(mrCode.isEmpty()){
            val intent = Intent(this@ActiveDeliveryActivity, PODMRLocationsActivity::class.java);
            intent.putExtra("DISTANCE", distance)
            intent.putExtra(Constants.IS_FROM_POD, false)

            startActivityForResult(intent, 12);
        }else{
            val intent = Intent(this@ActiveDeliveryActivity, PODActivity::class.java);
            intent.putExtra("DISTANCE", distance)
            startActivityForResult(intent, 12);
        }

    }
    private fun activeDeliveryTimeCapture() {
        var driverId = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "")
        var date = CalendarUtils.getDate()
        var time = CalendarUtils.getTime()
        var doctype = "";
        var activeDeliveryDO = StorageManager.getInstance(this).getActiveDeliveryMainDo(this);
        var startDate = activeDeliveryDO.timeCaptureStartRouteDate
        var startTime = activeDeliveryDO.timeCaptureStartRouteTime
        var docNumber = activeDeliveryDO.timeCaptureStartRouteDate
        if (docNumber.isEmpty()) {
            doctype = "0"
        } else {
            doctype = "4"

        }
        if (Util.isNetworkAvailable(this)) {

            val siteListRequest = ActiveDeliveryTimeCaptureRequest(driverId, date, doctype, docNumber, startDate, startTime, this@ActiveDeliveryActivity)
            siteListRequest.setOnResultListener { isError, loginDo ->
                hideLoader()
                if (loginDo != null) {
                    if (isError) {
                        hideLoader()
//                    Toast.makeText(this@ActiveDeliveryActivity, "Failed", Toast.LENGTH_SHORT).show()
                    } else {
                        hideLoader()
                        if (loginDo.flag == 1) {
                            showToast("Success")

                        } else {
                            showToast("Success")

                        }

                    }
                } else {
                    hideLoader()
                }

            }

            siteListRequest.execute()
        } else {
            showAppCompatAlert("Alert!", "Please Check Your Internet Connection", "OK", "", "FAILURE", false)

        }

    }

    override fun onMapReady(p0: GoogleMap?) {
        googleMap = p0!!
        // googleMap.setMyLocationEnabled(true)
        if (googleMap == null) {
            Toast.makeText(applicationContext, R.string.map_error, Toast.LENGTH_SHORT).show()
        } else {
            googleMap.getUiSettings().setZoomControlsEnabled(true)

        }
        showLocation()
        try {

        } catch (e: Exception) {

        }


    }

    override fun onProviderEnabled(provider: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onProviderDisabled(provider: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onLocationChanged(location: Location?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun showLocation() {
        try {

            val markerOptions = MarkerOptions()
            preferenceUtils = PreferenceUtils(this@ActiveDeliveryActivity)
            if (shipmentType.equals(getResources().getString(R.string.checkin_scheduled), true) && shipmentId.length > 0) {
                val lat = preferenceUtils.getDoubleFromPreference(PreferenceUtils.LATTITUDE, 0.0)
                val lng = preferenceUtils.getDoubleFromPreference(PreferenceUtils.LONGITUDE, 0.0)
                markerOptions.position(LatLng(lng, lat))
                googleMap.addMarker(markerOptions)
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lng, lat), 16f))
            } else {
                var customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)
                var lat = java.lang.Double.parseDouble(customerDo.lattitude)
                var lng = java.lang.Double.parseDouble(customerDo.longitude)
                markerOptions.position(LatLng(lng, lat))
                googleMap.addMarker(markerOptions)
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lng, lat), 16f))
            }


        } catch (e: Exception) {
            // Log.e(TAG, R.string.fail_location.toString(), e)
        }

    }

    override protected fun onResume() {
        super.onResume()
    }


}