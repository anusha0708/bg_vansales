package com.tbs.brothersgas.haadhir.pdfs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.tbs.brothersgas.haadhir.Activitys.BaseActivity;
import com.tbs.brothersgas.haadhir.Activitys.PrintDocumentsActivity;
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryDO;
import com.tbs.brothersgas.haadhir.Model.ActiveDeliveryMainDO;
import com.tbs.brothersgas.haadhir.Model.CustomerDo;
import com.tbs.brothersgas.haadhir.Model.PodDo;
import com.tbs.brothersgas.haadhir.database.StorageManager;
import com.tbs.brothersgas.haadhir.pdfs.utils.PDFConstants;
import com.tbs.brothersgas.haadhir.pdfs.utils.PDFOperations;
import com.tbs.brothersgas.haadhir.pdfs.utils.PreviewActivity;
import com.tbs.brothersgas.haadhir.prints.PrinterConstants;
import com.tbs.brothersgas.haadhir.utils.CalendarUtils;
import com.tbs.brothersgas.haadhir.utils.LogUtils;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;
import com.tbs.brothersgas.haadhir.utils.Util;

import java.io.File;
import java.io.FileOutputStream;

/*
 * Created by developer on 25/1/19.
 */
public class CYLDeliveryNotePDF {

    private Context context;
    private ActiveDeliveryMainDO activeDeliveryMainDO;
    private PdfPTable parentTable;
    private Font normalFont;
    private Font boldFont;
    private Document document;
    private boolean isFromSignature;
    private String type;

    public CYLDeliveryNotePDF(Context context) {
        this.context = context;
    }


    public CYLDeliveryNotePDF createDeliveryPDF(ActiveDeliveryMainDO activeDeliveryMainDO, String typE) {
        this.activeDeliveryMainDO = activeDeliveryMainDO;
        this.type = typE;

        Log.d("ActiveDeliveryMainDO-->", activeDeliveryMainDO + "");

        boldFont = new Font(Font.FontFamily.COURIER, 10.0f, Font.BOLD, BaseColor.BLACK);
        normalFont = new Font(Font.FontFamily.COURIER, 10.0f, Font.BOLD, BaseColor.BLACK);
        if (type.equalsIgnoreCase("Preview")) {
            normalFont = boldFont;
        }


//        new CreateDeliveryPDF().execute();

        return this;
    }

    public void isFromDeliverySignatureToFinishActivity(boolean isFromSignature) {
        this.isFromSignature = isFromSignature;

    }

    public void startCreatePDF() {
        new CreateDeliveryPDF().execute();
    }

    @SuppressLint("StaticFieldLeak")
    public class CreateDeliveryPDF extends AsyncTask<Void, Void, Void> {

        ProgressDialog progressDialog = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Generating Delivery Note Please wait...");
            if (!type.equalsIgnoreCase("AUTO")) {
                if (!((Activity) context).isFinishing()) {
                    try {
                        progressDialog.show();

                    } catch (Exception e) {
                        Log.d("Try...", String.valueOf(e));
                    }
                }
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {

            try {
                preparePDF();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (progressDialog.isShowing()) {
                progressDialog.cancel();

            }
            sendPDfewDoc();
        }
    }

    public class CreatePDF extends AsyncTask<Void, Void, Void> {

        ProgressDialog progressDialog = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Preparing Delivery Note Please wait...");
            if (!type.equalsIgnoreCase("AUTO")) {
                progressDialog.show();
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {

            try {
                preparePDF();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (progressDialog.isShowing()) {
                progressDialog.cancel();
            }
            Intent intent = new Intent(context.getApplicationContext(), PreviewActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("KEY", PDFConstants.CYL_DELIVERY_NOTE_PDF_NAME);
            context.startActivity(intent);
        }
    }

    public class CreateHistoryPDF extends AsyncTask<Void, Void, Void> {

        ProgressDialog progressDialog = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Preparing Delivery Note Please wait...");
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            try {
                preparePDF();
            } catch (Exception e) {
                e.printStackTrace();


            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressDialog.cancel();
            Intent intent = new Intent(context.getApplicationContext(), com.tbs.brothersgas.haadhir.history.PreviewActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("KEY", PDFConstants.CYL_DELIVERY_NOTE_PDF_NAME);
            if (isFromSignature) {
                intent.putExtra("CONSTANT", 9);
            }
            intent.putExtra("ID", activeDeliveryMainDO.shipmentNumber);
            context.startActivity(intent);
            if (isFromSignature) {
                ((BaseActivity) context).finish();
            }

        }
    }

    //Pre pare pdf
    public void preparePDF() {
        parentTable = new PdfPTable(1);
        parentTable.setWidthPercentage(100);
        initFile();

        addHeaderLogo();
        addHeaderLabel();
//
        if (isFromSignature) {
            addExecutiveDeliveryDetails();
        } else {
            addDeliveryDetails();

        }

        addProductsDetailsTableLables();
        addProductsDetailsTableValues();
        addSignature(activeDeliveryMainDO.signature);
        addFooterToPdf();
        document.close();
    }

    private void addHeaderLogo() {

        try {
            document.add(PDFOperations.getInstance().
                    getHeadrLogoIOSCertificateLogo(context, document, activeDeliveryMainDO.companyCode));
        } catch (DocumentException e) {
            e.printStackTrace();
        }

    }


    //Create file and document

    private void initFile() {
        File file = new File(Util.getAppPath(context) + PDFConstants.CYL_DELIVERY_NOTE_PDF_NAME);
        if (file.exists()) {
            file.delete();
        }
        try {
            file.createNewFile();
            document = new Document();
            // Location to save
            PdfWriter.getInstance(document, new FileOutputStream(file));
            // Open to write
            document.open();
            // Document Settings
            document.setPageSize(PageSize.A4);
            document.addCreationDate();
            document.addAuthor(PDFConstants.AUTHOR);
            document.addCreator(PDFConstants.CREATOR);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void addHeaderLabel() {
        try {
            Font headerLabelFont = new Font(Font.FontFamily.COURIER, 11.0f, Font.BOLD, BaseColor.BLACK);
//            Paragraph headerLabel = new Paragraph(PDFConstants.HEADER_1, headerLabelFont);
            Paragraph headerLabel = new Paragraph(activeDeliveryMainDO.companyDescription, headerLabelFont);

            headerLabel.setAlignment(Element.ALIGN_CENTER);
            headerLabel.setPaddingTop(10);
            document.add(headerLabel);
//            Paragraph headerLabel1 = new Paragraph(PDFConstants.HEADER_2, headerLabelFont);

            Paragraph headerLabel1 = new Paragraph(PDFConstants.HEADER_2, headerLabelFont);
            headerLabel1.setAlignment(Element.ALIGN_CENTER);
            headerLabel1.setPaddingTop(10);
            document.add(headerLabel1);
            addEmptySpaceLine(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addDeliveryDetails() {
        try {

            PdfPTable headerParentTable = new PdfPTable(1);
            headerParentTable.setPaddingTop(10f);
            headerParentTable.setWidthPercentage(100);

            float[] columnWidths = {2.3f, 0.3f, 6, 2.2f, 0.3f, 3f};
            PdfPTable headerTable = new PdfPTable(columnWidths);
            headerTable.setWidthPercentage(100);
            PdfPCell cellOne = new PdfPCell();
            cellOne.setBorder(Rectangle.NO_BORDER);
            PdfPCell cellOne1 = new PdfPCell();
            cellOne1.setBorder(Rectangle.NO_BORDER);
            PdfPCell cellTwo = new PdfPCell();
            cellTwo.setBorder(Rectangle.NO_BORDER);
            PdfPCell cellThree = new PdfPCell();
            cellThree.setBorder(Rectangle.NO_BORDER);
            PdfPCell cellThree1 = new PdfPCell();
            cellThree1.setBorder(Rectangle.NO_BORDER);
            PdfPCell cellFour = new PdfPCell();
            cellFour.setBorder(Rectangle.NO_BORDER);


            cellOne.addElement(new Phrase(PDFConstants.D_R_NO, boldFont));
            cellOne.addElement(new Phrase(PDFConstants.DELIVERY_TO, boldFont));
            cellOne.addElement(new Phrase(PDFConstants.PAYMENT_TERM, boldFont));
            cellOne.addElement(new Phrase(PDFConstants.VEHICLE_CODE, boldFont));
            cellOne.addElement(new Phrase(PDFConstants.RECIEVER_NAME, boldFont));

            cellOne.addElement(new Phrase(PDFConstants.ADDRESS, boldFont));

            cellOne1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellOne1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellOne1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellOne1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellOne1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));

            cellTwo.addElement(new Phrase(activeDeliveryMainDO.shipmentNumber, normalFont));
            cellTwo.addElement(new Phrase(activeDeliveryMainDO.customerDescription, normalFont));
            cellTwo.addElement(new Phrase(activeDeliveryMainDO.paymentTerm, normalFont));
            PreferenceUtils preferenceUtils = new PreferenceUtils(context);
            String vehicleCode = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_CODE, "");
            cellTwo.addElement(new Phrase(vehicleCode, normalFont));
            cellTwo.addElement(new Phrase(activeDeliveryMainDO.capturedName, normalFont));

            cellTwo.addElement(new Phrase(getAddress(), normalFont));

            cellThree.addElement(new Phrase(PDFConstants.DATE, boldFont));
            cellThree.addElement(new Phrase(PDFConstants.TIME, boldFont));
            cellThree.addElement(new Phrase(PDFConstants.USER_ID, boldFont));
            cellThree.addElement(new Phrase(PDFConstants.USER_NAME, boldFont));
            cellThree.addElement(new Phrase(PDFConstants.VEHICLE_NUMBER, boldFont));
            cellThree.addElement(new Phrase(PDFConstants.RECIEVER_NUMBER, boldFont));


            cellThree1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellThree1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellThree1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellThree1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            if (activeDeliveryMainDO.createdDate.length() > 0) {
                String dMonth = activeDeliveryMainDO.createdDate.substring(4, 6);
                String dyear = activeDeliveryMainDO.createdDate.substring(0, 4);
                String dDate = activeDeliveryMainDO.createdDate.substring(Math.max(activeDeliveryMainDO.createdDate.length() - 2, 0));
                cellFour.addElement(new Phrase(dDate + "-" + dMonth + "-" + dyear, normalFont));
            } else {
                cellFour.addElement(new Phrase("", normalFont));
            }
            cellFour.addElement(new Phrase(activeDeliveryMainDO.createdTime, normalFont));
            String vehicleNumber = preferenceUtils.getStringFromPreference(PreferenceUtils.CV_PLATE, "");
            cellFour.addElement(new Phrase(activeDeliveryMainDO.createUserID, normalFont));
            cellFour.addElement(new Phrase(activeDeliveryMainDO.createUserName, normalFont));

            cellFour.addElement(new Phrase(vehicleNumber, normalFont));
            cellFour.addElement(new Phrase(activeDeliveryMainDO.capturedNumber, normalFont));

            headerTable.addCell(cellOne);
            headerTable.addCell(cellOne1);
            headerTable.addCell(cellTwo);
            headerTable.addCell(cellThree);
            headerTable.addCell(cellThree1);
            headerTable.addCell(cellFour);

            PdfPCell pdfPCell = new PdfPCell();
            pdfPCell.addElement(headerTable);
            headerParentTable.addCell(pdfPCell);
            document.add(headerParentTable);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    private void addExecutiveDeliveryDetails() {
        try {

            PdfPTable headerParentTable = new PdfPTable(1);
            headerParentTable.setPaddingTop(10f);
            headerParentTable.setWidthPercentage(100);

            float[] columnWidths = {2.3f, 0.3f, 6, 2.2f, 0.3f, 3f};
            PdfPTable headerTable = new PdfPTable(columnWidths);
            headerTable.setWidthPercentage(100);
            PdfPCell cellOne = new PdfPCell();
            cellOne.setBorder(Rectangle.NO_BORDER);
            PdfPCell cellOne1 = new PdfPCell();
            cellOne1.setBorder(Rectangle.NO_BORDER);
            PdfPCell cellTwo = new PdfPCell();
            cellTwo.setBorder(Rectangle.NO_BORDER);
            PdfPCell cellThree = new PdfPCell();
            cellThree.setBorder(Rectangle.NO_BORDER);
            PdfPCell cellThree1 = new PdfPCell();
            cellThree1.setBorder(Rectangle.NO_BORDER);
            PdfPCell cellFour = new PdfPCell();
            cellFour.setBorder(Rectangle.NO_BORDER);


            cellOne.addElement(new Phrase(PDFConstants.D_R_NO, boldFont));
            cellOne.addElement(new Phrase(PDFConstants.DELIVERY_TO, boldFont));
            cellOne.addElement(new Phrase(PDFConstants.CUSTOMER_ID, boldFont));
            cellOne.addElement(new Phrase(PDFConstants.PAYMENT_TERM, boldFont));
            cellOne.addElement(new Phrase(PDFConstants.VEHICLE_CODE, boldFont));
            cellOne.addElement(new Phrase(PDFConstants.RECIEVER_NAME, boldFont));
            cellOne.addElement(new Phrase(PDFConstants.SALES_NUMBER, boldFont));
            cellOne.addElement(new Phrase(PDFConstants.LPO_NUMBER, boldFont));

            cellOne.addElement(new Phrase(PDFConstants.ADDRESS, boldFont));

            cellOne1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellOne1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellOne1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellOne1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellOne1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellOne1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellOne1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellOne1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellOne1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));


            cellTwo.addElement(new Phrase(activeDeliveryMainDO.shipmentNumber, normalFont));
            if (activeDeliveryMainDO.customerDescription.isEmpty()) {
                cellTwo.addElement(new Phrase("      ", normalFont));
            } else {
//                activeDeliveryMainDO.customerDescription = activeDeliveryMainDO.customerDescription.substring(0, Math.min(activeDeliveryMainDO.customerDescription.length(), 20));

                cellTwo.addElement(new Phrase(activeDeliveryMainDO.customerDescription, normalFont));
            }
            if (activeDeliveryMainDO.customer.isEmpty()) {
                cellTwo.addElement(new Phrase("      ", normalFont));
            } else {

                cellTwo.addElement(new Phrase(activeDeliveryMainDO.customer, normalFont));
            }
            cellTwo.addElement(new Phrase(activeDeliveryMainDO.paymentTerm, normalFont));
            PreferenceUtils preferenceUtils = new PreferenceUtils(context);
            String vehicleCode = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_CODE, "");
            if (vehicleCode.isEmpty()) {
                cellTwo.addElement(new Phrase("      ", normalFont));

            } else {
                cellTwo.addElement(new Phrase(vehicleCode, normalFont));

            }
            if (activeDeliveryMainDO.capturedName.isEmpty()) {
                cellTwo.addElement(new Phrase("      ", normalFont));

            } else {
                cellTwo.addElement(new Phrase(activeDeliveryMainDO.capturedName, normalFont));

            }
            if (activeDeliveryMainDO.salesOrderNumber.isEmpty()) {
                cellTwo.addElement(new Phrase("    ", normalFont));

            } else {
                cellTwo.addElement(new Phrase(activeDeliveryMainDO.salesOrderNumber, normalFont));

            }

            if (activeDeliveryMainDO.LPONumber.isEmpty()) {
                cellTwo.addElement(new Phrase("    ", normalFont));

            } else {
                cellTwo.addElement(new Phrase(activeDeliveryMainDO.LPONumber, normalFont));

            }

            cellTwo.addElement(new Phrase(getAddress(), normalFont));

            cellThree.addElement(new Phrase(PDFConstants.DATE, boldFont));
            cellThree.addElement(new Phrase(PDFConstants.TIME, boldFont));
            cellThree.addElement(new Phrase(PDFConstants.USER_ID, boldFont));
            cellThree.addElement(new Phrase(PDFConstants.USER_NAME, boldFont));
            cellThree.addElement(new Phrase(PDFConstants.VEHICLE_NUMBER, boldFont));
            cellThree.addElement(new Phrase(PDFConstants.RECIEVER_NUMBER, boldFont));

            cellThree.addElement(new Phrase(PDFConstants.SIGN_DATE, boldFont));
            cellThree.addElement(new Phrase(PDFConstants.SIGN_TIME, boldFont));
            cellThree1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellThree1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellThree1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellThree1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellThree1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellThree1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellThree1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellThree1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            if (activeDeliveryMainDO.createdDate.length() > 0) {
                String dMonth = activeDeliveryMainDO.createdDate.substring(4, 6);
                String dyear = activeDeliveryMainDO.createdDate.substring(0, 4);
                String dDate = activeDeliveryMainDO.createdDate.substring(Math.max(activeDeliveryMainDO.createdDate.length() - 2, 0));
                cellFour.addElement(new Phrase(dDate + "-" + dMonth + "-" + dyear, normalFont));
            } else {
                cellFour.addElement(new Phrase("", normalFont));
            }
            cellFour.addElement(new Phrase(activeDeliveryMainDO.createdTime, normalFont));
            String vehicleNumber = preferenceUtils.getStringFromPreference(PreferenceUtils.CV_PLATE, "");
            cellFour.addElement(new Phrase(activeDeliveryMainDO.createUserID, normalFont));
            if (activeDeliveryMainDO.createUserName.isEmpty()) {
                cellFour.addElement(new Phrase("      ", normalFont));
            } else {
                activeDeliveryMainDO.createUserName = activeDeliveryMainDO.createUserName.substring(0, Math.min(activeDeliveryMainDO.createUserName.length(), 13));

                cellFour.addElement(new Phrase(activeDeliveryMainDO.createUserName, normalFont));
            }
            if (vehicleNumber.isEmpty()) {
                cellFour.addElement(new Phrase("   ", normalFont));
            } else {
                cellFour.addElement(new Phrase(vehicleNumber, normalFont));
            }
            if (activeDeliveryMainDO.capturedNumber.isEmpty()) {
                cellFour.addElement(new Phrase("   ", normalFont));
            } else {
                cellFour.addElement(new Phrase(activeDeliveryMainDO.capturedNumber, normalFont));
            }

            if (activeDeliveryMainDO.signatureDate.length() > 0) {
                String dMonth = activeDeliveryMainDO.signatureDate.substring(4, 6);
                String dyear = activeDeliveryMainDO.signatureDate.substring(0, 4);
                String dDate = activeDeliveryMainDO.signatureDate.substring(Math.max(activeDeliveryMainDO.signatureDate.length() - 2, 0));
                cellFour.addElement(new Phrase(dDate + "-" + dMonth + "-" + dyear, normalFont));
            } else {
                cellFour.addElement(new Phrase(" ", normalFont));
            }

            if (activeDeliveryMainDO.signatureTime.length() > 0) {
                cellFour.addElement(new Phrase(activeDeliveryMainDO.signatureTime, normalFont));
            } else {
                cellFour.addElement(new Phrase("   ", normalFont));
            }


            headerTable.addCell(cellOne);
            headerTable.addCell(cellOne1);
            headerTable.addCell(cellTwo);
            headerTable.addCell(cellThree);
            headerTable.addCell(cellThree1);
            headerTable.addCell(cellFour);

            PdfPCell pdfPCell = new PdfPCell();
            pdfPCell.addElement(headerTable);
            headerParentTable.addCell(pdfPCell);
            document.add(headerParentTable);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    private void addProductsDetailsTableLables() {
        try {
            float[] productColumnsWidth = {2.5f, 10, 3};
            PdfPTable productsTable = new PdfPTable(productColumnsWidth);
            productsTable.setPaddingTop(10);
            productsTable.setWidthPercentage(100);

            PdfPCell cellOne = new PdfPCell();
            PdfPCell cellTwo = new PdfPCell();
            PdfPCell cellThree = new PdfPCell();

            Paragraph pOne = new Paragraph(PDFConstants.SR_NO, boldFont);
            pOne.setAlignment(Element.ALIGN_CENTER);
            cellOne.addElement(pOne);

            Paragraph pTwo = new Paragraph(PDFConstants.ITEM, boldFont);
            pTwo.setAlignment(Element.ALIGN_LEFT);
            cellTwo.addElement(pTwo);

            Paragraph pThree = new Paragraph(PDFConstants.QUANTITY, boldFont);
            pThree.setAlignment(Element.ALIGN_CENTER);
            cellThree.addElement(pThree);

            productsTable.addCell(cellOne);
            productsTable.addCell(cellTwo);
            productsTable.addCell(cellThree);

            document.add(productsTable);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    private void addProductsDetailsTableValues() {
        try {
            float[] productColumnsWidth = {2.5f, 10, 3};
            PdfPTable productsTable = new PdfPTable(productColumnsWidth);
            productsTable.setWidthPercentage(100);
            LogUtils.INSTANCE.info("activeDeliveryMainDO", activeDeliveryMainDO.activeDeliveryDOS.size() + "--->");
            if (activeDeliveryMainDO.activeDeliveryDOS != null && activeDeliveryMainDO.activeDeliveryDOS.size() > 0) {
                for (int i = 0; i < activeDeliveryMainDO.activeDeliveryDOS.size(); i++) {
                    ActiveDeliveryDO actDelDo = activeDeliveryMainDO.activeDeliveryDOS.get(i);
                    PdfPCell cellOne = new PdfPCell();
                    PdfPCell cellTwo = new PdfPCell();
                    PdfPCell cellThree = new PdfPCell();

                    cellOne.setHorizontalAlignment(Element.ALIGN_CENTER);
                    Paragraph prOne = new Paragraph("" + (i + 1), normalFont);
                    prOne.setAlignment(Element.ALIGN_CENTER);
                    cellOne.addElement(prOne);

                    cellTwo.setHorizontalAlignment(Element.ALIGN_LEFT);
                    if (isFromSignature) {
                        cellTwo.addElement(new Phrase(actDelDo.product + "\n" + actDelDo.productDescription, normalFont));

                    } else {
                        cellTwo.addElement(new Phrase("" + actDelDo.productDescription, normalFont));

                    }

                    cellThree.setHorizontalAlignment(Element.ALIGN_CENTER);
                    Paragraph prThree = new Paragraph("" + actDelDo.totalQuantity + " " + actDelDo.unit, normalFont);
                    prThree.setAlignment(Element.ALIGN_CENTER);
                    cellThree.addElement(prThree);

                    productsTable.addCell(cellOne);
                    productsTable.addCell(cellTwo);
                    productsTable.addCell(cellThree);
                }
                document.add(productsTable);
                PdfPTable referenceTable = new PdfPTable(1);
                referenceTable.setWidthPercentage(100);


                PdfPCell referenceCell = new PdfPCell(new Phrase("Delivery Reference : " + activeDeliveryMainDO.referenceID, boldFont));
//                PdfPCell noteCell = new PdfPCell(new Phrase("AED : Eighteen Thousand Eight Hundred Forty Seven and 50/100 Only-", boldFont));
                referenceCell.setHorizontalAlignment(Element.ALIGN_LEFT);
                referenceCell.setPadding(10);
                referenceTable.addCell(referenceCell);

                document.add(referenceTable);
                PdfPTable noteTable = new PdfPTable(1);
                noteTable.setWidthPercentage(100);
                PdfPCell noteCell = new PdfPCell(new Phrase("Remarks : " + activeDeliveryMainDO.remarks, boldFont));
//                PdfPCell noteCell = new PdfPCell(new Phrase("AED : Eighteen Thousand Eight Hundred Forty Seven and 50/100 Only-", boldFont));
                noteCell.setHorizontalAlignment(Element.ALIGN_LEFT);
                noteCell.setPadding(10);
                noteTable.addCell(noteCell);

                document.add(noteTable);
//                PdfPTable noteTable2 = new PdfPTable(1);
//                noteTable2.setWidthPercentage(100);
//                PdfPCell noteCell2 = new PdfPCell(new Phrase("Starting Reading :   " + activeDeliveryMainDO.startReading +"       Ending Reading :   " + activeDeliveryMainDO.endReading, boldFont));
////                PdfPCell noteCell = new PdfPCell(new Phrase("AED : Eighteen Thousand Eight Hundred Forty Seven and 50/100 Only-", boldFont));
//                noteCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
//                noteCell2.setPadding(10);
//                noteTable2.addCell(noteCell2);
//
//                document.add(noteTable2);
            }
//            addEmptySpaceLine(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addSignature(String sign) {
        try {
//
            PdfPTable custTable = new PdfPTable(1);
            custTable.setWidthPercentage(100);

            Image image = PDFOperations.getInstance().getSignatureFromFile(sign);
            if (image != null) {
                document.add(image);
            }

            PdfPCell pdfPCell = new PdfPCell(new Phrase("Customer's Signature:", normalFont));
            pdfPCell.setBorder(Rectangle.NO_BORDER);
            pdfPCell.setHorizontalAlignment(Element.ALIGN_LEFT);
            custTable.addCell(pdfPCell);
            document.add(custTable);


        } catch (BadElementException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }

    }

    private void addFooterToPdf() {
        try {
            document.add(PDFOperations.getInstance().addFooterImage(context, document));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addEmptySpaceLine(int noOfLines) {
        try {
            PdfPTable emptyTable = new PdfPTable(1);
            emptyTable.setWidthPercentage(100);
            for (int i = 0; i < noOfLines; i++) {
                PdfPCell emptyCell = new PdfPCell();
                emptyCell.setBorder(Rectangle.NO_BORDER);
                emptyCell.addElement(new Paragraph("\n"));
                emptyTable.addCell(emptyCell);
            }
            document.add(emptyTable);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    private PdfPCell getBorderlessCell(String elementName, int alignment) {
        PdfPCell cell = new PdfPCell(new Phrase(elementName, normalFont));
        cell.setHorizontalAlignment(alignment);
        cell.setBorder(Rectangle.NO_BORDER);
        return cell;
    }

    private void sendPDfewDoc() {
//        String email1 = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.EMAIL, "");
//        String email2 = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.EMAIL2, "");
//        String email3 = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.EMAIL3, "");
//        String email4 = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.EMAIL4, "");
//        String email5 = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.EMAIL5, "");
        SharedPreferences pref = context.getApplicationContext().getSharedPreferences("MyPref", 0);
        String email1 =pref.getString(PreferenceUtils.EMAIL, "");
        String email2 =pref.getString(PreferenceUtils.EMAIL2, "");
        String email3 = pref.getString(PreferenceUtils.EMAIL3, "");
        String email4 =pref.getString(PreferenceUtils.EMAIL4, "");
        String email5 = pref.getString(PreferenceUtils.EMAIL5, "");
        String mainEMail = "";
        if (!email1.isEmpty()) {
            mainEMail = email1+",";
        }
        if (!email2.isEmpty()) {
            mainEMail = mainEMail+email2+",";
        }
        if (!email3.isEmpty()) {
            mainEMail = mainEMail+email3+",";
        }
        if (!email4.isEmpty()) {
            mainEMail = mainEMail+email4+",";
        }
        if (!email5.isEmpty()) {
            mainEMail = mainEMail+email5;
        }
        if (mainEMail.length() > 0) {

            PDFOperations.getInstance().sendpdfMail(context,
//                    activeDeliveryMainDO.deliveryEmail,
                    mainEMail,

                    activeDeliveryMainDO.customerDescription,
                    PDFConstants.CYL_DELIVERY_NOTE_PDF_NAME, PDFConstants.CYL_NAME);
        } else {
            Toast.makeText(context, "Please provide email address", Toast.LENGTH_SHORT);
        }
    }


    private String getAddress() {


        String street = activeDeliveryMainDO.customerStreet;
        String landMark = activeDeliveryMainDO.customerLandMark;
        String town = activeDeliveryMainDO.customerTown;
        String city = activeDeliveryMainDO.customerCity;
        String postal = activeDeliveryMainDO.customerPostalCode;
        String countryName = activeDeliveryMainDO.countryName;

        String finalString = "";

        if (!TextUtils.isEmpty(street)) {
            finalString += street + ", ";
        }
        if (!TextUtils.isEmpty(landMark)) {
            finalString += landMark + ", ";
        }
        if (!TextUtils.isEmpty(town)) {
            finalString += town + ", ";
        }
        if (!TextUtils.isEmpty(city)) {
            finalString += city + ", ";
        }
        if (!TextUtils.isEmpty(postal)) {
            finalString += postal + ", ";
        }
//        if (!TextUtils.isEmpty(countryName)) {
//            finalString += countryName;
//        }

        return finalString;
    }
}
