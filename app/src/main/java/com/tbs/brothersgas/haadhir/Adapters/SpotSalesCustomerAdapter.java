package com.tbs.brothersgas.haadhir.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import android.content.Intent;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tbs.brothersgas.haadhir.Activitys.AddressListActivity;
import com.tbs.brothersgas.haadhir.Activitys.BaseActivity;
import com.tbs.brothersgas.haadhir.Activitys.SpotSalesCustomerActivity;
import com.tbs.brothersgas.haadhir.Model.CustomerDo;
import com.tbs.brothersgas.haadhir.R;
import com.tbs.brothersgas.haadhir.common.AppConstants;
import com.tbs.brothersgas.haadhir.decanting.DecantingAddressListActivity;
import com.tbs.brothersgas.haadhir.utils.PreferenceUtils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class SpotSalesCustomerAdapter extends RecyclerView.Adapter<SpotSalesCustomerAdapter.MyViewHolder> {

    private ArrayList<CustomerDo> customerDos;
    private Context context;
    private String from;


    public void refreshAdapter(@NotNull ArrayList<CustomerDo> customerDos) {
        this.customerDos = customerDos;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvCustomerName, tvPaymentTerm, tvCustomerCode, tvBuisinessLine, tvAddressCode, tvDescription, tvCity, tvEmirates, tvAddressLine;
        public TextView btnPay;
        private LinearLayout llDetails;
        public TextView tvOrderId;

        public MyViewHolder(View view) {
            super(view);
            llDetails = (LinearLayout) view.findViewById(R.id.llDetails);
            tvCustomerName = itemView.findViewById(R.id.tvCustomerName);
            tvCustomerCode = itemView.findViewById(R.id.tvCustomerCode);
            tvBuisinessLine = itemView.findViewById(R.id.tvBuisinessLine);
            tvAddressCode = itemView.findViewById(R.id.tvAddressCode);
            tvAddressLine = itemView.findViewById(R.id.tvAddressLine);
            tvPaymentTerm = itemView.findViewById(R.id.tvPaymentTerm);


        }
    }


    public SpotSalesCustomerAdapter(Context context, ArrayList<CustomerDo> listOrderDos, String froM) {
        this.context = context;
        this.customerDos = listOrderDos;
        this.from = froM;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.spotsales_customer_data, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        final CustomerDo customerDo = customerDos.get(position);
        if (from.equalsIgnoreCase("DECANTING")) {
            holder.tvCustomerCode.setText(customerDo.customerId);
            holder.tvCustomerName.setText(customerDo.customerName);
            holder.tvBuisinessLine.setText(customerDo.businessLine);
            holder.tvPaymentTerm.setText(customerDo.paymentTerm);
            holder.tvAddressLine.setText(customerDo.postBox);
            holder.tvAddressCode.setText(customerDo.amount);
        } else {
            holder.tvCustomerCode.setText(customerDo.customerId);
            holder.tvCustomerName.setText(customerDo.customerName);
            holder.tvBuisinessLine.setText(customerDo.businessLine);
            holder.tvPaymentTerm.setText(customerDo.paymentTerm);
            holder.tvAddressLine.setText(customerDo.postBox);
            holder.tvAddressCode.setText(customerDo.amount);
            if (customerDo.isDelivered != null && customerDo.isDelivered.equalsIgnoreCase("Delivered")) {
                holder.llDetails.setBackgroundColor(context.getResources().getColor(R.color.gray_new));
                holder.llDetails.setClickable(false);
                holder.llDetails.setEnabled(false);
            } else {
                holder.llDetails.setClickable(true);
                holder.llDetails.setEnabled(true);
            }
        }


        holder.llDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (customerDo.fax.equals(context.getResources().getString(R.string.app_version))) {
                    if (!from.isEmpty() && from.equalsIgnoreCase("MASTER")) {
                        Intent intent = new Intent(context, AddressListActivity.class);
                        intent.putExtra("CODE", customerDo.customerId);
                        intent.putExtra("MASTER", "MASTER");
                        intent.putExtra("CUSTOMERDO", customerDo);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        context.startActivity(intent);
                    } else if (!from.isEmpty() && from.equalsIgnoreCase("DECANTING")) {
                        Intent intent = new Intent(context, DecantingAddressListActivity.class);
                        intent.putExtra("CODE", customerDo.customerId);
                        intent.putExtra("DECANTING", "DECANTING");
                        intent.putExtra("CUSTOMERDO", customerDo);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        context.startActivity(intent);
                    } else {
                        Intent intent = new Intent(context, AddressListActivity.class);
                        intent.putExtra("CODE", customerDo.customerId);
                        intent.putExtra("CUSTOMERDO", customerDo);
                        ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.CURRENCY, "" + customerDo.currency);
                        ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.ProductsType, AppConstants.FixedQuantityProduct);
                        ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.ShipmentType, context.getResources().getString(R.string.checkin_non_scheduled));
                        context.startActivity(intent);
                    }
                } else {
                    ((BaseActivity) context).showAppCompatAlert("" + context.getResources().getString(R.string.login_info), "" + context.getResources().getString(R.string.playstore_messsage), "" + context.getResources().getString(R.string.ok), "Cancel", "PLAYSTORE", true);
                }


            }
        });


    }

    @Override
    public int getItemCount() {
//        if (filterList != null && filterList.size() > 0) {
//            return filterList.size();
//
//        } else {
//        }
        return customerDos != null ? customerDos.size() : 0;

    }

}
